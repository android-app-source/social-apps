.class public Lcom/facebook/nearby/model/MapTileSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/nearby/model/MapTile;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2416372
    const-class v0, Lcom/facebook/nearby/model/MapTile;

    new-instance v1, Lcom/facebook/nearby/model/MapTileSerializer;

    invoke-direct {v1}, Lcom/facebook/nearby/model/MapTileSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2416373
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2416374
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/nearby/model/MapTile;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2416375
    if-nez p0, :cond_0

    .line 2416376
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2416377
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2416378
    invoke-static {p0, p1, p2}, Lcom/facebook/nearby/model/MapTileSerializer;->b(Lcom/facebook/nearby/model/MapTile;LX/0nX;LX/0my;)V

    .line 2416379
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2416380
    return-void
.end method

.method private static b(Lcom/facebook/nearby/model/MapTile;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2416381
    const-string v0, "id"

    iget-object v1, p0, Lcom/facebook/nearby/model/MapTile;->id:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2416382
    const-string v0, "creationTime"

    iget-wide v2, p0, Lcom/facebook/nearby/model/MapTile;->creationTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 2416383
    const-string v0, "timeToLiveInSeconds"

    iget-wide v2, p0, Lcom/facebook/nearby/model/MapTile;->timeToLiveInSeconds:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 2416384
    const-string v0, "bounds"

    iget-object v1, p0, Lcom/facebook/nearby/model/MapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2416385
    const-string v0, "minZoom"

    iget v1, p0, Lcom/facebook/nearby/model/MapTile;->minZoom:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 2416386
    const-string v0, "maxZoom"

    iget v1, p0, Lcom/facebook/nearby/model/MapTile;->maxZoom:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 2416387
    const-string v0, "places"

    iget-object v1, p0, Lcom/facebook/nearby/model/MapTile;->places:Ljava/util/List;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 2416388
    const-string v0, "backgroundPlaces"

    iget-object v1, p0, Lcom/facebook/nearby/model/MapTile;->backgroundPlaces:Ljava/util/List;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 2416389
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2416390
    check-cast p1, Lcom/facebook/nearby/model/MapTile;

    invoke-static {p1, p2, p3}, Lcom/facebook/nearby/model/MapTileSerializer;->a(Lcom/facebook/nearby/model/MapTile;LX/0nX;LX/0my;)V

    return-void
.end method
