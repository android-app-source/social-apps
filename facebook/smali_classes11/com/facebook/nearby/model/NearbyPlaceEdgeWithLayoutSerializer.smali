.class public Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayoutSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2416453
    const-class v0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;

    new-instance v1, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayoutSerializer;

    invoke-direct {v1}, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayoutSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2416454
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2416455
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2416456
    if-nez p0, :cond_0

    .line 2416457
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2416458
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2416459
    invoke-static {p0, p1, p2}, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayoutSerializer;->b(Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;LX/0nX;LX/0my;)V

    .line 2416460
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2416461
    return-void
.end method

.method private static b(Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2416462
    const-string v0, "placeEdge"

    iget-object v1, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;->placeEdge:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2416463
    const-string v0, "layout"

    iget-object v1, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;->layout:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2416464
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2416465
    check-cast p1, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;

    invoke-static {p1, p2, p3}, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayoutSerializer;->a(Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;LX/0nX;LX/0my;)V

    return-void
.end method
