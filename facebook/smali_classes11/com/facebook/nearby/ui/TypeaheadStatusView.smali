.class public Lcom/facebook/nearby/ui/TypeaheadStatusView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Landroid/widget/ProgressBar;

.field private c:Landroid/widget/TextView;

.field private d:LX/0hx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:J

.field private f:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2421460
    const-class v0, Lcom/facebook/nearby/ui/TypeaheadStatusView;

    sput-object v0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2421461
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/nearby/ui/TypeaheadStatusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2421462
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2421463
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/nearby/ui/TypeaheadStatusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2421464
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2421465
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2421466
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->e:J

    .line 2421467
    const v0, 0x7f030bd4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2421468
    const v0, 0x7f0d1d77

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->c:Landroid/widget/TextView;

    .line 2421469
    const v0, 0x7f0d1d76

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->b:Landroid/widget/ProgressBar;

    .line 2421470
    const-class v0, Lcom/facebook/nearby/ui/TypeaheadStatusView;

    invoke-static {v0, p0}, Lcom/facebook/nearby/ui/TypeaheadStatusView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2421471
    return-void
.end method

.method private static a(Lcom/facebook/nearby/ui/TypeaheadStatusView;LX/0hx;LX/0So;)V
    .locals 0

    .prologue
    .line 2421472
    iput-object p1, p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->d:LX/0hx;

    iput-object p2, p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->f:LX/0So;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/nearby/ui/TypeaheadStatusView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;

    invoke-static {v1}, LX/0hx;->a(LX/0QB;)LX/0hx;

    move-result-object v0

    check-cast v0, LX/0hx;

    invoke-static {v1}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-static {p0, v0, v1}, Lcom/facebook/nearby/ui/TypeaheadStatusView;->a(Lcom/facebook/nearby/ui/TypeaheadStatusView;LX/0hx;LX/0So;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2421473
    iget-object v0, p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->b:Landroid/widget/ProgressBar;

    invoke-static {v0}, LX/0hx;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2421474
    iget-object v0, p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->f:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->e:J

    .line 2421475
    iget-object v0, p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->d:LX/0hx;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0hx;->a(Z)V

    .line 2421476
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->b:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2421477
    iget-object v0, p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2421478
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 2421479
    iget-object v0, p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2421480
    iget-object v0, p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2421481
    iget-wide v0, p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->e:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->d:LX/0hx;

    iget-object v1, p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->f:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->e:J

    sub-long/2addr v2, v4

    iget-object v1, p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2, v3, v1}, LX/0hx;->a(JLandroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2421482
    iput-wide v6, p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->e:J

    .line 2421483
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/ui/TypeaheadStatusView;->b:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2421484
    return-void
.end method
