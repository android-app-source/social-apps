.class public Lcom/facebook/nearby/search/NearbySearchFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H1k;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/H3P;

.field private f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H1r;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/view/inputmethod/InputMethodManager;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0y2;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field public l:Landroid/widget/ListView;

.field public m:Landroid/widget/EditText;

.field public n:Lcom/facebook/nearby/ui/TypeaheadStatusView;

.field private o:LX/H3R;

.field public p:Landroid/location/Location;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;

.field private r:Lcom/facebook/nearby/protocol/SearchArea;

.field private s:Z

.field private t:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/common/SearchSuggestion;",
            ">;"
        }
    .end annotation
.end field

.field private u:LX/1MF;

.field private v:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H3b;",
            ">;"
        }
    .end annotation
.end field

.field private w:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private x:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BVS;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2421214
    const-class v0, Lcom/facebook/nearby/search/NearbySearchFragment;

    sput-object v0, Lcom/facebook/nearby/search/NearbySearchFragment;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2421188
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2421189
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2421190
    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->b:LX/0Ot;

    .line 2421191
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2421192
    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->c:LX/0Ot;

    .line 2421193
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2421194
    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->d:LX/0Ot;

    .line 2421195
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2421196
    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->f:LX/0Ot;

    .line 2421197
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2421198
    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->g:LX/0Ot;

    .line 2421199
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2421200
    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->h:LX/0Ot;

    .line 2421201
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2421202
    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->i:LX/0Ot;

    .line 2421203
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2421204
    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->j:LX/0Ot;

    .line 2421205
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2421206
    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->k:LX/0Ot;

    .line 2421207
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->q:Ljava/lang/String;

    .line 2421208
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2421209
    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->v:LX/0Ot;

    .line 2421210
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2421211
    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->w:LX/0Ot;

    .line 2421212
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2421213
    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->x:LX/0Ot;

    return-void
.end method

.method private a(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2421215
    new-instance v0, LX/H3Y;

    invoke-direct {v0, p0}, LX/H3Y;-><init>(Lcom/facebook/nearby/search/NearbySearchFragment;)V

    invoke-static {p1, v0}, LX/0Ph;->c(Ljava/lang/Iterable;LX/0Rl;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private a(ILcom/facebook/nearby/common/SearchSuggestion;)V
    .locals 9

    .prologue
    .line 2421216
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->l:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2421217
    sget-object v2, LX/H1j;->HISTORY_SUGGESTION:LX/H1j;

    .line 2421218
    :goto_0
    iget-object v0, p2, Lcom/facebook/nearby/common/SearchSuggestion;->d:Lcom/facebook/nearby/common/NearbyTopic;

    if-eqz v0, :cond_1

    .line 2421219
    iget-object v0, p2, Lcom/facebook/nearby/common/SearchSuggestion;->d:Lcom/facebook/nearby/common/NearbyTopic;

    iget-object v0, v0, Lcom/facebook/nearby/common/NearbyTopic;->b:LX/0Rf;

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v3

    .line 2421220
    :goto_1
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H1k;

    iget-object v1, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->m:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->q:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->p:Landroid/location/Location;

    iget-object v8, p2, Lcom/facebook/nearby/common/SearchSuggestion;->c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-virtual/range {v0 .. v8}, LX/H1k;->a(Ljava/lang/String;LX/H1j;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Landroid/location/Location;Lcom/facebook/graphql/model/GraphQLGeoRectangle;)V

    .line 2421221
    return-void

    .line 2421222
    :cond_0
    sget-object v2, LX/H1j;->TYPEAHEAD_SUGGESTION:LX/H1j;

    goto :goto_0

    .line 2421223
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    goto :goto_1
.end method

.method private a(Lcom/facebook/nearby/model/TypeaheadPlace;I)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2421224
    sget-object v0, LX/0ax;->aE:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/nearby/model/TypeaheadPlace;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 2421225
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/facebook/nearby/model/TypeaheadPlace;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/03l;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2421226
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H1k;

    iget-object v1, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->m:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/H1j;->PAGE:LX/H1j;

    new-array v3, v11, [Ljava/lang/Long;

    iget-object v5, p1, Lcom/facebook/nearby/model/TypeaheadPlace;->a:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v10

    invoke-static {v3}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v5, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->q:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->p:Landroid/location/Location;

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, LX/H1k;->a(Ljava/lang/String;LX/H1j;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Landroid/location/Location;Lcom/facebook/graphql/model/GraphQLGeoRectangle;)V

    .line 2421227
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2421228
    iget-object v0, p1, Lcom/facebook/nearby/model/TypeaheadPlace;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/nearby/model/TypeaheadPlace;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/nearby/model/TypeaheadPlace;->d:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, LX/5ve;->b(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2421229
    const-string v0, "extra_user_location"

    iget-object v2, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->p:Landroid/location/Location;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2421230
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2, v9, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2421231
    sget-object v0, Lcom/facebook/nearby/search/NearbySearchFragment;->a:Ljava/lang/Class;

    const-string v1, "Failed navigating to page id = %s "

    new-array v2, v11, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/facebook/nearby/model/TypeaheadPlace;->a:Ljava/lang/String;

    aput-object v3, v2, v10

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2421232
    :goto_0
    return-void

    .line 2421233
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "via_nearby_result"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    goto :goto_0
.end method

.method private static a(Lcom/facebook/nearby/search/NearbySearchFragment;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/nearby/search/NearbySearchFragment;",
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/H1k;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/H1r;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/view/inputmethod/InputMethodManager;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0y2;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/H3b;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BVS;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2421234
    iput-object p1, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->b:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->c:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->d:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->f:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->g:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->h:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->i:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->j:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->k:LX/0Ot;

    iput-object p10, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->v:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->w:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->x:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/nearby/search/NearbySearchFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 14

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v12

    move-object v0, p0

    check-cast v0, Lcom/facebook/nearby/search/NearbySearchFragment;

    const/16 v1, 0x542

    invoke-static {v12, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x2eb

    invoke-static {v12, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x2a93

    invoke-static {v12, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2a96

    invoke-static {v12, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x271

    invoke-static {v12, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x41

    invoke-static {v12, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xc7e

    invoke-static {v12, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x97

    invoke-static {v12, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2e3

    invoke-static {v12, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2aa3

    invoke-static {v12, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x259

    invoke-static {v12, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v13, 0x3881

    invoke-static {v12, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static/range {v0 .. v12}, Lcom/facebook/nearby/search/NearbySearchFragment;->a(Lcom/facebook/nearby/search/NearbySearchFragment;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/nearby/search/NearbySearchFragment;I)V
    .locals 4

    .prologue
    .line 2421235
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->e:LX/H3P;

    invoke-virtual {v0, p1}, LX/H3P;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 2421236
    instance-of v1, v0, Lcom/facebook/nearby/common/SearchSuggestion;

    if-eqz v1, :cond_0

    .line 2421237
    check-cast v0, Lcom/facebook/nearby/common/SearchSuggestion;

    .line 2421238
    invoke-direct {p0, p1, v0}, Lcom/facebook/nearby/search/NearbySearchFragment;->a(ILcom/facebook/nearby/common/SearchSuggestion;)V

    .line 2421239
    invoke-static {p0, v0}, Lcom/facebook/nearby/search/NearbySearchFragment;->a$redex0(Lcom/facebook/nearby/search/NearbySearchFragment;Lcom/facebook/nearby/common/SearchSuggestion;)V

    .line 2421240
    :goto_0
    return-void

    .line 2421241
    :cond_0
    instance-of v1, v0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;

    if-eqz v1, :cond_1

    .line 2421242
    check-cast v0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;

    iget-object v0, v0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->typeaheadPlace:Lcom/facebook/nearby/model/TypeaheadPlace;

    invoke-direct {p0, v0, p1}, Lcom/facebook/nearby/search/NearbySearchFragment;->a(Lcom/facebook/nearby/model/TypeaheadPlace;I)V

    goto :goto_0

    .line 2421243
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected object type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static a$redex0(Lcom/facebook/nearby/search/NearbySearchFragment;Lcom/facebook/nearby/common/SearchSuggestion;)V
    .locals 8

    .prologue
    .line 2421244
    invoke-static {p0}, Lcom/facebook/nearby/search/NearbySearchFragment;->n(Lcom/facebook/nearby/search/NearbySearchFragment;)V

    .line 2421245
    iget-object v0, p1, Lcom/facebook/nearby/common/SearchSuggestion;->d:Lcom/facebook/nearby/common/NearbyTopic;

    if-eqz v0, :cond_0

    .line 2421246
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H1k;

    iget-object v1, p1, Lcom/facebook/nearby/common/SearchSuggestion;->d:Lcom/facebook/nearby/common/NearbyTopic;

    iget-object v1, v1, Lcom/facebook/nearby/common/NearbyTopic;->b:LX/0Rf;

    iget-object v2, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->q:Ljava/lang/String;

    .line 2421247
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2421248
    iget-object v3, v0, LX/H1k;->a:LX/0Zb;

    const-string v4, "category_selected"

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 2421249
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2421250
    const-string v3, "places_search"

    invoke-virtual {v4, v3}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string v6, "topic_ids"

    invoke-interface {v1}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v3

    const/4 v7, 0x0

    aget-object v3, v3, v7

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v5, v6, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v3

    const-string v5, "session_id"

    invoke-virtual {v3, v5, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2421251
    invoke-virtual {v4}, LX/0oG;->d()V

    .line 2421252
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H1r;

    .line 2421253
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2421254
    invoke-virtual {v4, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421255
    const/4 v2, 0x1

    .line 2421256
    iget-object v1, v0, LX/H1r;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v3, v1

    :goto_0
    if-ge v3, v5, :cond_1

    iget-object v1, v0, LX/H1r;->d:LX/0Px;

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/common/SearchSuggestion;

    .line 2421257
    invoke-virtual {v1, p1}, Lcom/facebook/nearby/common/SearchSuggestion;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 2421258
    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421259
    add-int/lit8 v1, v2, 0x1

    .line 2421260
    const/16 v2, 0xa

    if-eq v1, v2, :cond_1

    .line 2421261
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_0

    .line 2421262
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/H1r;->d:LX/0Px;

    .line 2421263
    iget-object v1, v0, LX/H1r;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-static {v0}, LX/H1r;->c(LX/H1r;)LX/0Tn;

    move-result-object v2

    iget-object v3, v0, LX/H1r;->c:LX/H1q;

    iget-object v4, v0, LX/H1r;->d:LX/0Px;

    invoke-virtual {v3, v4}, LX/H1q;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2421264
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H1r;

    .line 2421265
    iget-object v1, v0, LX/H1r;->d:LX/0Px;

    move-object v0, v1

    .line 2421266
    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->t:LX/0Px;

    .line 2421267
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2421268
    const-string v1, "result_place_search_suggestion"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2421269
    const-string v1, "result_search_area"

    iget-object v2, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->r:Lcom/facebook/nearby/protocol/SearchArea;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2421270
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2421271
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2421272
    return-void

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/nearby/search/NearbySearchFragment;Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;)V
    .locals 4

    .prologue
    .line 2421178
    iget-object v0, p1, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->q:Ljava/lang/String;

    .line 2421179
    iget-object v0, p1, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;->d:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/facebook/nearby/search/NearbySearchFragment;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 2421180
    iget-object v1, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->m:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2421181
    iget-object v2, p1, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2421182
    :goto_0
    return-void

    .line 2421183
    :cond_0
    iget-object v1, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->e:LX/H3P;

    iget-object v2, p1, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;->c:Ljava/util/List;

    iget-object v3, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->p:Landroid/location/Location;

    invoke-virtual {v1, v2, v0, v3}, LX/H3P;->a(Ljava/util/List;Ljava/util/List;Landroid/location/Location;)V

    .line 2421184
    iget-object v1, p1, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2421185
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->n:Lcom/facebook/nearby/ui/TypeaheadStatusView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/ui/TypeaheadStatusView;->setVisibility(I)V

    .line 2421186
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->n:Lcom/facebook/nearby/ui/TypeaheadStatusView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0820c9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/ui/TypeaheadStatusView;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2421187
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->n:Lcom/facebook/nearby/ui/TypeaheadStatusView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/ui/TypeaheadStatusView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/nearby/search/NearbySearchFragment;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2421273
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 2421274
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0y2;

    const-wide/32 v4, 0x1b7740

    invoke-virtual {v0, v4, v5}, LX/0y2;->a(J)Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    .line 2421275
    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->p:Landroid/location/Location;

    .line 2421276
    iget-boolean v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->s:Z

    if-nez v0, :cond_0

    .line 2421277
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->p:Landroid/location/Location;

    if-eqz v0, :cond_0

    .line 2421278
    new-instance v0, Lcom/facebook/nearby/protocol/SearchArea;

    iget-object v4, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->p:Landroid/location/Location;

    iget-object v5, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->r:Lcom/facebook/nearby/protocol/SearchArea;

    iget v5, v5, Lcom/facebook/nearby/protocol/SearchArea;->b:I

    invoke-direct {v0, v4, v5}, Lcom/facebook/nearby/protocol/SearchArea;-><init>(Landroid/location/Location;I)V

    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->r:Lcom/facebook/nearby/protocol/SearchArea;

    .line 2421279
    :cond_0
    new-instance v0, LX/H3L;

    iget-object v4, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->r:Lcom/facebook/nearby/protocol/SearchArea;

    invoke-direct {v0, v4, p1}, LX/H3L;-><init>(Lcom/facebook/nearby/protocol/SearchArea;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->p:Landroid/location/Location;

    .line 2421280
    iput-object v4, v0, LX/H3L;->c:Landroid/location/Location;

    .line 2421281
    move-object v4, v0

    .line 2421282
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->p:Landroid/location/Location;

    if-nez v0, :cond_3

    move v0, v1

    .line 2421283
    :goto_1
    iput v0, v4, LX/H3L;->d:F

    .line 2421284
    move-object v4, v4

    .line 2421285
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->p:Landroid/location/Location;

    if-nez v0, :cond_4

    move v0, v1

    .line 2421286
    :goto_2
    iput v0, v4, LX/H3L;->f:F

    .line 2421287
    move-object v0, v4

    .line 2421288
    iget-object v4, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->p:Landroid/location/Location;

    if-nez v4, :cond_5

    .line 2421289
    :goto_3
    iput v1, v0, LX/H3L;->h:F

    .line 2421290
    move-object v0, v0

    .line 2421291
    new-instance v1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;

    invoke-direct {v1, v0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;-><init>(LX/H3L;)V

    move-object v0, v1

    .line 2421292
    new-instance v1, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsParams;

    new-instance v4, Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutParams;

    invoke-direct {v4}, Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutParams;-><init>()V

    invoke-direct {v1, v0, v4}, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsParams;-><init>(Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutParams;)V

    .line 2421293
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2421294
    const-string v0, "searchNearbyPlacesAndLayoutsParams"

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2421295
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->u:LX/1MF;

    if-eqz v0, :cond_1

    .line 2421296
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->u:LX/1MF;

    invoke-interface {v0}, LX/1MF;->dispose()V

    .line 2421297
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0aG;

    const-string v1, "search_nearby_places_and_layout"

    const v5, -0x3b66f32

    invoke-static {v0, v1, v4, v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->u:LX/1MF;

    .line 2421298
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->e:LX/H3P;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->p:Landroid/location/Location;

    invoke-virtual {v0, v1, v4, v5}, LX/H3P;->a(Ljava/util/List;Ljava/util/List;Landroid/location/Location;)V

    .line 2421299
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->n:Lcom/facebook/nearby/ui/TypeaheadStatusView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/ui/TypeaheadStatusView;->setVisibility(I)V

    .line 2421300
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->n:Lcom/facebook/nearby/ui/TypeaheadStatusView;

    invoke-virtual {v0}, Lcom/facebook/nearby/ui/TypeaheadStatusView;->a()V

    .line 2421301
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    iget-object v1, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->u:LX/1MF;

    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    new-instance v4, LX/H3X;

    invoke-direct {v4, p0, p1, v2, v3}, LX/H3X;-><init>(Lcom/facebook/nearby/search/NearbySearchFragment;Ljava/lang/String;J)V

    invoke-virtual {v0, v1, v4}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2421302
    return-void

    .line 2421303
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v0

    goto/16 :goto_0

    .line 2421304
    :cond_3
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->p:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAltitude()D

    move-result-wide v6

    double-to-float v0, v6

    goto/16 :goto_1

    :cond_4
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->p:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getBearing()F

    move-result v0

    goto/16 :goto_2

    :cond_5
    iget-object v1, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->p:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getSpeed()F

    move-result v1

    goto/16 :goto_3
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2421124
    const v0, 0x7f0d1d6f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->m:Landroid/widget/EditText;

    .line 2421125
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->m:Landroid/widget/EditText;

    new-instance v1, LX/H3T;

    invoke-direct {v1, p0}, LX/H3T;-><init>(Lcom/facebook/nearby/search/NearbySearchFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2421126
    return-void
.end method

.method public static d$redex0(Lcom/facebook/nearby/search/NearbySearchFragment;)V
    .locals 6

    .prologue
    .line 2421127
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->m:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v1

    .line 2421128
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->m:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2421129
    if-lez v1, :cond_0

    .line 2421130
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    new-instance v3, Lcom/facebook/nearby/search/NearbySearchFragment$2;

    invoke-direct {v3, p0, v2}, Lcom/facebook/nearby/search/NearbySearchFragment$2;-><init>(Lcom/facebook/nearby/search/NearbySearchFragment;Ljava/lang/String;)V

    const-wide/16 v4, 0x12c

    invoke-virtual {v0, v3, v4, v5}, LX/0Sh;->b(Ljava/lang/Runnable;J)V

    .line 2421131
    :cond_0
    if-lez v1, :cond_1

    .line 2421132
    invoke-direct {p0}, Lcom/facebook/nearby/search/NearbySearchFragment;->e()V

    .line 2421133
    :goto_0
    return-void

    .line 2421134
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->n:Lcom/facebook/nearby/ui/TypeaheadStatusView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/ui/TypeaheadStatusView;->setVisibility(I)V

    .line 2421135
    invoke-direct {p0}, Lcom/facebook/nearby/search/NearbySearchFragment;->k()V

    goto :goto_0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 2421136
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->l:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2421137
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->l:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->o:LX/H3R;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeHeaderView(Landroid/view/View;)Z

    .line 2421138
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->e:LX/H3P;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->p:Landroid/location/Location;

    invoke-virtual {v0, v1, v2, v3}, LX/H3P;->a(Ljava/util/List;Ljava/util/List;Landroid/location/Location;)V

    .line 2421139
    return-void
.end method

.method private k()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2421140
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->l:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 2421141
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->l:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2421142
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->l:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->o:LX/H3R;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 2421143
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->l:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->e:LX/H3P;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2421144
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->e:LX/H3P;

    iget-object v1, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->t:LX/0Px;

    invoke-virtual {v0, v1}, LX/H3P;->a(Ljava/util/List;)V

    .line 2421145
    return-void
.end method

.method private l()V
    .locals 5

    .prologue
    .line 2421146
    const v0, 0x7f0d1d71

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/ui/TypeaheadStatusView;

    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->n:Lcom/facebook/nearby/ui/TypeaheadStatusView;

    .line 2421147
    const v0, 0x7f0d1d70

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->l:Landroid/widget/ListView;

    .line 2421148
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->l:Landroid/widget/ListView;

    new-instance v1, LX/H3U;

    invoke-direct {v1, p0}, LX/H3U;-><init>(Lcom/facebook/nearby/search/NearbySearchFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2421149
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->l:Landroid/widget/ListView;

    new-instance v1, LX/H3V;

    invoke-direct {v1, p0}, LX/H3V;-><init>(Lcom/facebook/nearby/search/NearbySearchFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2421150
    invoke-direct {p0}, Lcom/facebook/nearby/search/NearbySearchFragment;->m()LX/H3R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->o:LX/H3R;

    .line 2421151
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->l:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->o:LX/H3R;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 2421152
    new-instance v3, LX/H3P;

    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H3b;

    iget-object v1, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->x:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BVS;

    iget-object v2, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->w:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v0, v1, v2, v4}, LX/H3P;-><init>(LX/H3b;LX/BVS;LX/03V;Landroid/content/Context;)V

    iput-object v3, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->e:LX/H3P;

    .line 2421153
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->e:LX/H3P;

    iget-object v1, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->t:LX/0Px;

    invoke-virtual {v0, v1}, LX/H3P;->a(Ljava/util/List;)V

    .line 2421154
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->l:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->e:LX/H3P;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2421155
    return-void
.end method

.method private m()LX/H3R;
    .locals 5

    .prologue
    .line 2421156
    new-instance v0, LX/H3R;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/H3R;-><init>(Landroid/content/Context;)V

    .line 2421157
    new-instance v1, LX/H3W;

    invoke-direct {v1, p0}, LX/H3W;-><init>(Lcom/facebook/nearby/search/NearbySearchFragment;)V

    .line 2421158
    iget-object v2, v0, LX/H3R;->a:LX/0P1;

    invoke-virtual {v2}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v2

    invoke-virtual {v2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 2421159
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v3}, LX/H3R;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance p0, LX/H3Q;

    invoke-direct {p0, v0, v1, v2}, LX/H3Q;-><init>(LX/H3R;LX/H3W;Ljava/util/Map$Entry;)V

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2421160
    :cond_0
    return-object v0
.end method

.method public static n(Lcom/facebook/nearby/search/NearbySearchFragment;)V
    .locals 3

    .prologue
    .line 2421161
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->l:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2421162
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2421163
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2421164
    const-class v0, Lcom/facebook/nearby/search/NearbySearchFragment;

    invoke-static {v0, p0}, Lcom/facebook/nearby/search/NearbySearchFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2421165
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x76bef703

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2421166
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2421167
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "search_area"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "Missing search area info in intent"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2421168
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "search_area"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/SearchArea;

    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->r:Lcom/facebook/nearby/protocol/SearchArea;

    .line 2421169
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "user_location"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->p:Landroid/location/Location;

    .line 2421170
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "user_defined_search_location"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->s:Z

    .line 2421171
    iget-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H1r;

    .line 2421172
    iget-object v2, v0, LX/H1r;->d:LX/0Px;

    move-object v0, v2

    .line 2421173
    iput-object v0, p0, Lcom/facebook/nearby/search/NearbySearchFragment;->t:LX/0Px;

    .line 2421174
    invoke-direct {p0}, Lcom/facebook/nearby/search/NearbySearchFragment;->c()V

    .line 2421175
    invoke-direct {p0}, Lcom/facebook/nearby/search/NearbySearchFragment;->l()V

    .line 2421176
    const/16 v0, 0x2b

    const v2, -0x1939d31

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x3503ab7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2421177
    const v1, 0x7f030bcf

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x646ec38f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method
