.class public Lcom/facebook/nearby/cluster/NearbyPlaceCluster;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/cluster/NearbyPlaceCluster;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/graphics/RectF;

.field public final c:Landroid/location/Location;

.field public final d:Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2415877
    new-instance v0, LX/H1n;

    invoke-direct {v0}, LX/H1n;-><init>()V

    sput-object v0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2415856
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2415857
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2415858
    :goto_0
    if-eqz v0, :cond_2

    .line 2415859
    new-instance v2, LX/0cA;

    invoke-direct {v2}, LX/0cA;-><init>()V

    .line 2415860
    const-class v0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, [Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;

    check-cast v0, [Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;

    .line 2415861
    array-length v3, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v0, v1

    .line 2415862
    invoke-virtual {v2, v4}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2415863
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 2415864
    goto :goto_0

    .line 2415865
    :cond_1
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->a:Ljava/util/Set;

    .line 2415866
    :goto_2
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;

    iput-object v0, p0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->d:Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;

    .line 2415867
    iget-object v0, p0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->d:Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;

    .line 2415868
    iget-object v5, v0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->h:Landroid/location/Location;

    if-nez v5, :cond_3

    .line 2415869
    invoke-virtual {v0}, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->a()D

    move-result-wide v5

    invoke-virtual {v0}, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->c()D

    move-result-wide v7

    invoke-static {v5, v6, v7, v8}, LX/6aA;->a(DD)Landroid/location/Location;

    move-result-object v5

    iput-object v5, v0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->h:Landroid/location/Location;

    .line 2415870
    :goto_3
    iget-object v5, v0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->h:Landroid/location/Location;

    move-object v0, v5

    .line 2415871
    iput-object v0, p0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->c:Landroid/location/Location;

    .line 2415872
    const-class v0, Landroid/graphics/RectF;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->b:Landroid/graphics/RectF;

    .line 2415873
    return-void

    .line 2415874
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->a:Ljava/util/Set;

    goto :goto_2

    .line 2415875
    :cond_3
    iget-object v5, v0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->h:Landroid/location/Location;

    invoke-virtual {v0}, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->a()D

    move-result-wide v7

    invoke-virtual {v5, v7, v8}, Landroid/location/Location;->setLatitude(D)V

    .line 2415876
    iget-object v5, v0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->h:Landroid/location/Location;

    invoke-virtual {v0}, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->c()D

    move-result-wide v7

    invoke-virtual {v5, v7, v8}, Landroid/location/Location;->setLongitude(D)V

    goto :goto_3
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2415855
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2415838
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;

    if-nez v1, :cond_1

    .line 2415839
    :cond_0
    :goto_0
    return v0

    .line 2415840
    :cond_1
    check-cast p1, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;

    .line 2415841
    iget-object v1, p0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->a:Ljava/util/Set;

    move-object v1, v1

    .line 2415842
    iget-object v2, p1, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->a:Ljava/util/Set;

    move-object v2, v2

    .line 2415843
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2415844
    iget-object v1, p0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->c:Landroid/location/Location;

    move-object v1, v1

    .line 2415845
    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 2415846
    iget-object v2, p1, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->c:Landroid/location/Location;

    move-object v2, v2

    .line 2415847
    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2415848
    iget-object v1, p0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->c:Landroid/location/Location;

    move-object v1, v1

    .line 2415849
    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 2415850
    iget-object v2, p1, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->c:Landroid/location/Location;

    move-object v2, v2

    .line 2415851
    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2415852
    iget-object v1, p0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->d:Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;

    move-object v1, v1

    .line 2415853
    iget-object v2, p1, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->d:Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;

    move-object v2, v2

    .line 2415854
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 2415837
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->a:Ljava/util/Set;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->d:Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->c:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->c:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 2415825
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2415826
    iget-object v0, p0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;

    .line 2415827
    invoke-virtual {v0}, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->a()D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->c()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->d()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2415828
    :cond_0
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2415829
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2415830
    iget-object v0, p0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->a:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 2415831
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2415832
    :goto_0
    iget-object v0, p0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->d:Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2415833
    iget-object v0, p0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->b:Landroid/graphics/RectF;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2415834
    return-void

    .line 2415835
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2415836
    iget-object v0, p0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->a:Ljava/util/Set;

    iget-object v1, p0, Lcom/facebook/nearby/cluster/NearbyPlaceCluster;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    goto :goto_0
.end method
