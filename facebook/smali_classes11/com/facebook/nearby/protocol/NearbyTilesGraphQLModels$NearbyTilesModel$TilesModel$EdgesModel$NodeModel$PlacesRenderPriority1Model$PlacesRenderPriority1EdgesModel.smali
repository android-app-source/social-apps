.class public final Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6fc4cfac
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:D

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2417737
    const-class v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2417736
    const-class v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2417734
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2417735
    return-void
.end method

.method private a()Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2417732
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->e:Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->e:Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;

    .line 2417733
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->e:Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;

    return-object v0
.end method

.method private j()Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getResultDecoration"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2417730
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->f:Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->f:Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    .line 2417731
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->f:Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    return-object v0
.end method

.method private k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSocialContext"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2417738
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2417739
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 2417719
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2417720
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->a()Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2417721
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->j()Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2417722
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x4f52b9db

    invoke-static {v3, v2, v4}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2417723
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2417724
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2417725
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2417726
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->g:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2417727
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2417728
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2417729
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2417699
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2417700
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->a()Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2417701
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->a()Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;

    .line 2417702
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->a()Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2417703
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;

    .line 2417704
    iput-object v0, v1, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->e:Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;

    .line 2417705
    :cond_0
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->j()Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2417706
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->j()Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    .line 2417707
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->j()Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2417708
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;

    .line 2417709
    iput-object v0, v1, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->f:Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    .line 2417710
    :cond_1
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2417711
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x4f52b9db

    invoke-static {v2, v0, v3}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2417712
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2417713
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;

    .line 2417714
    iput v3, v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->h:I

    move-object v1, v0

    .line 2417715
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2417716
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 2417717
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object p0, v1

    .line 2417718
    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2417695
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2417696
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->g:D

    .line 2417697
    const/4 v0, 0x3

    const v1, -0x4f52b9db

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;->h:I

    .line 2417698
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2417692
    new-instance v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;

    invoke-direct {v0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel;-><init>()V

    .line 2417693
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2417694
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2417691
    const v0, 0x530ef2e4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2417690
    const v0, -0x31a9b2c7

    return v0
.end method
