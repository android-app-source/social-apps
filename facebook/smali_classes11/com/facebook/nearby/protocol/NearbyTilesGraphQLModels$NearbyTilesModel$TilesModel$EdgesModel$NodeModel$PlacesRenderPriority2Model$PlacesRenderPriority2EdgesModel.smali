.class public final Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x13365a21
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel$PlacesRenderPriority2EdgesNodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:D

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2418023
    const-class v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2418022
    const-class v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2418020
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2418021
    return-void
.end method

.method private a()Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel$PlacesRenderPriority2EdgesNodeModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2418018
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->e:Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel$PlacesRenderPriority2EdgesNodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel$PlacesRenderPriority2EdgesNodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel$PlacesRenderPriority2EdgesNodeModel;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->e:Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel$PlacesRenderPriority2EdgesNodeModel;

    .line 2418019
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->e:Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel$PlacesRenderPriority2EdgesNodeModel;

    return-object v0
.end method

.method private j()Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getResultDecoration"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2418016
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->f:Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->f:Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    .line 2418017
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->f:Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    return-object v0
.end method

.method private k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSocialContext"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2418024
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2418025
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 2418005
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2418006
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->a()Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel$PlacesRenderPriority2EdgesNodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2418007
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->j()Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2418008
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x5321705

    invoke-static {v3, v2, v4}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2418009
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2418010
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2418011
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2418012
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->g:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2418013
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2418014
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2418015
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2417985
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2417986
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->a()Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel$PlacesRenderPriority2EdgesNodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2417987
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->a()Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel$PlacesRenderPriority2EdgesNodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel$PlacesRenderPriority2EdgesNodeModel;

    .line 2417988
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->a()Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel$PlacesRenderPriority2EdgesNodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2417989
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;

    .line 2417990
    iput-object v0, v1, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->e:Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel$PlacesRenderPriority2EdgesNodeModel;

    .line 2417991
    :cond_0
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->j()Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2417992
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->j()Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    .line 2417993
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->j()Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2417994
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;

    .line 2417995
    iput-object v0, v1, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->f:Lcom/facebook/nearby/protocol/FetchNearbyPlacesGraphQLFragmentsModels$NearbyFacepilesFragmentModel;

    .line 2417996
    :cond_1
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2417997
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x5321705

    invoke-static {v2, v0, v3}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2417998
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2417999
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;

    .line 2418000
    iput v3, v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->h:I

    move-object v1, v0

    .line 2418001
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2418002
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 2418003
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object p0, v1

    .line 2418004
    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2417976
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2417977
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->g:D

    .line 2417978
    const/4 v0, 0x3

    const v1, 0x5321705

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;->h:I

    .line 2417979
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2417982
    new-instance v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;

    invoke-direct {v0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority2Model$PlacesRenderPriority2EdgesModel;-><init>()V

    .line 2417983
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2417984
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2417981
    const v0, -0x591e1d2b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2417980
    const v0, -0x31a9b2c7

    return v0
.end method
