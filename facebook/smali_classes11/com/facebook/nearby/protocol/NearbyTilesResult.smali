.class public Lcom/facebook/nearby/protocol/NearbyTilesResult;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/protocol/NearbyTilesResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMapTile;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/facebook/nearby/protocol/NearbyTilesParams;

.field public final f:Lcom/facebook/graphql/model/GraphQLGeoRectangle;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2419401
    new-instance v0, LX/H2w;

    invoke-direct {v0}, LX/H2w;-><init>()V

    sput-object v0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0ta;JLjava/util/List;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;Lcom/facebook/graphql/model/GraphQLGeoRectangle;Lcom/facebook/nearby/protocol/NearbyTilesParams;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ta;",
            "J",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMapTile;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLGeoRectangle;",
            "Lcom/facebook/nearby/protocol/NearbyTilesParams;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2419392
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 2419393
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2419394
    iput-object p4, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->a:Ljava/util/List;

    .line 2419395
    iput-object p5, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->b:Ljava/lang/String;

    .line 2419396
    invoke-static {p6}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->c:LX/0Rf;

    .line 2419397
    invoke-static {p7}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->d:LX/0Rf;

    .line 2419398
    iput-object p8, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->f:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 2419399
    iput-object p9, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->e:Lcom/facebook/nearby/protocol/NearbyTilesParams;

    .line 2419400
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2419368
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 2419369
    const-class v0, Lcom/facebook/graphql/model/GraphQLMapTileNode;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->a:Ljava/util/List;

    .line 2419370
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->b:Ljava/lang/String;

    .line 2419371
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2419372
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->c:LX/0Rf;

    .line 2419373
    const-class v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->d:LX/0Rf;

    .line 2419374
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->f:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 2419375
    const-class v0, Lcom/facebook/nearby/protocol/NearbyTilesParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/NearbyTilesParams;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->e:Lcom/facebook/nearby/protocol/NearbyTilesParams;

    .line 2419376
    return-void

    .line 2419377
    :cond_0
    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2419391
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2419390
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->c:LX/0Rf;

    return-object v0
.end method

.method public final d()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2419389
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->d:LX/0Rf;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2419388
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/facebook/graphql/model/GraphQLGeoRectangle;
    .locals 1

    .prologue
    .line 2419387
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->f:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    return-object v0
.end method

.method public final f()Lcom/facebook/nearby/protocol/NearbyTilesParams;
    .locals 1

    .prologue
    .line 2419386
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->e:Lcom/facebook/nearby/protocol/NearbyTilesParams;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2419378
    invoke-super {p0, p1, p2}, Lcom/facebook/fbservice/results/BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2419379
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2419380
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2419381
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->c:LX/0Rf;

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2419382
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->d:LX/0Rf;

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2419383
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->f:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2419384
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesResult;->e:Lcom/facebook/nearby/protocol/NearbyTilesParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2419385
    return-void
.end method
