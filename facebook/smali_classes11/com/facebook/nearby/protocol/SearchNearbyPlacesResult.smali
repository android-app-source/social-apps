.class public Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/common/SearchSuggestion;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/model/TypeaheadPlace;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2420864
    new-instance v0, LX/H3M;

    invoke-direct {v0}, LX/H3M;-><init>()V

    sput-object v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0ta;JLjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ta;",
            "J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/common/SearchSuggestion;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/model/TypeaheadPlace;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2420865
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 2420866
    iput-object p4, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;->a:Ljava/lang/String;

    .line 2420867
    iput-object p5, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;->b:Ljava/lang/String;

    .line 2420868
    iput-object p6, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;->c:Ljava/util/List;

    .line 2420869
    iput-object p7, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;->d:Ljava/util/List;

    .line 2420870
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2420871
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 2420872
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;->a:Ljava/lang/String;

    .line 2420873
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;->b:Ljava/lang/String;

    .line 2420874
    const-class v0, Lcom/facebook/nearby/common/SearchSuggestion;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;->c:Ljava/util/List;

    .line 2420875
    const-class v0, Lcom/facebook/nearby/model/TypeaheadPlace;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;->d:Ljava/util/List;

    .line 2420876
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2420877
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2420878
    invoke-super {p0, p1, p2}, Lcom/facebook/fbservice/results/BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2420879
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2420880
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2420881
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;->c:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2420882
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;->d:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2420883
    return-void
.end method
