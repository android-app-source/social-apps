.class public final Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xc61f176
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:I

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:D

.field private n:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Z

.field private t:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2417655
    const-class v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2417656
    const-class v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2417657
    const/16 v0, 0x13

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2417658
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 2417659
    iput-boolean p1, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->g:Z

    .line 2417660
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2417661
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2417662
    if-eqz v0, :cond_0

    .line 2417663
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 2417664
    :cond_0
    return-void
.end method

.method private j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAddress"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2417665
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2417666
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private k()Z
    .locals 2

    .prologue
    .line 2417667
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2417668
    iget-boolean v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->g:Z

    return v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2417681
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->h:Ljava/lang/String;

    .line 2417682
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2417669
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 2417670
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2417671
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->l:Ljava/lang/String;

    .line 2417672
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method private o()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageLikers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 2417673
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2417674
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->n:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private p()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageVisits"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2417675
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2417676
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->o:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private q()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPhoneNumber"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2417677
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2417678
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->p:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2417653
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->q:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->q:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 2417654
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->q:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    return-object v0
.end method

.method private s()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2417679
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2417680
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->r:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private t()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRaters"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2417534
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2417535
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->t:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private u()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2417538
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->u:Ljava/util/List;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->u:Ljava/util/List;

    .line 2417539
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->u:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2417536
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->v:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->v:Ljava/lang/String;

    .line 2417537
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->v:Ljava/lang/String;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2417540
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->w:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->w:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2417541
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->w:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 17

    .prologue
    .line 2417542
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2417543
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->j()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x32eb491f    # -1.5593832E8f

    invoke-static {v3, v2, v4}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2417544
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->l()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2417545
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2417546
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->n()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2417547
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->o()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v8, 0x26518cd

    invoke-static {v7, v6, v8}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2417548
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->p()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v9, -0x212ad9fb

    invoke-static {v7, v6, v9}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2417549
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->q()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v10, -0x4a50b857

    invoke-static {v7, v6, v10}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 2417550
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->r()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    .line 2417551
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->s()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v12, -0x7b228f91

    invoke-static {v7, v6, v12}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 2417552
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->t()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v13, -0x62f8dc94

    invoke-static {v7, v6, v13}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 2417553
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->u()LX/0Px;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/util/List;)I

    move-result v14

    .line 2417554
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->v()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 2417555
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->w()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v16

    .line 2417556
    const/16 v6, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 2417557
    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v2}, LX/186;->b(II)V

    .line 2417558
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->f:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->a(IZ)V

    .line 2417559
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->g:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->a(IZ)V

    .line 2417560
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2417561
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->i:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2417562
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2417563
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->k:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 2417564
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2417565
    const/16 v3, 0x8

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->m:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 2417566
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2417567
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2417568
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2417569
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2417570
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2417571
    const/16 v2, 0xe

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->s:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2417572
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2417573
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 2417574
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2417575
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2417576
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2417577
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2417578
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2417579
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2417580
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x32eb491f    # -1.5593832E8f

    invoke-static {v2, v0, v3}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2417581
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2417582
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;

    .line 2417583
    iput v3, v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->e:I

    move-object v1, v0

    .line 2417584
    :cond_0
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2417585
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 2417586
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2417587
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;

    .line 2417588
    iput-object v0, v1, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 2417589
    :cond_1
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->o()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2417590
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->o()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x26518cd

    invoke-static {v2, v0, v3}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2417591
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->o()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2417592
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;

    .line 2417593
    iput v3, v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->n:I

    move-object v1, v0

    .line 2417594
    :cond_2
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->p()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 2417595
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->p()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x212ad9fb

    invoke-static {v2, v0, v3}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2417596
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->p()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2417597
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;

    .line 2417598
    iput v3, v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->o:I

    move-object v1, v0

    .line 2417599
    :cond_3
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->q()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 2417600
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->q()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x4a50b857

    invoke-static {v2, v0, v3}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2417601
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->q()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2417602
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;

    .line 2417603
    iput v3, v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->p:I

    move-object v1, v0

    .line 2417604
    :cond_4
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->s()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_5

    .line 2417605
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->s()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x7b228f91

    invoke-static {v2, v0, v3}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 2417606
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->s()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2417607
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;

    .line 2417608
    iput v3, v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->r:I

    move-object v1, v0

    .line 2417609
    :cond_5
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->t()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    .line 2417610
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->t()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x62f8dc94

    invoke-static {v2, v0, v3}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 2417611
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->t()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2417612
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;

    .line 2417613
    iput v3, v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->t:I

    move-object v1, v0

    .line 2417614
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2417615
    if-nez v1, :cond_7

    :goto_0
    return-object p0

    .line 2417616
    :catchall_0
    move-exception v0

    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v0

    .line 2417617
    :catchall_1
    move-exception v0

    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v0

    .line 2417618
    :catchall_2
    move-exception v0

    :try_start_8
    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v0

    .line 2417619
    :catchall_3
    move-exception v0

    :try_start_9
    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    throw v0

    .line 2417620
    :catchall_4
    move-exception v0

    :try_start_a
    monitor-exit v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    throw v0

    .line 2417621
    :catchall_5
    move-exception v0

    :try_start_b
    monitor-exit v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    throw v0

    :cond_7
    move-object p0, v1

    .line 2417622
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2417623
    new-instance v0, LX/H2U;

    invoke-direct {v0, p1}, LX/H2U;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2417624
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2417625
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2417626
    const v0, -0x32eb491f    # -1.5593832E8f

    invoke-static {p1, p2, v2, v0}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->e:I

    .line 2417627
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->f:Z

    .line 2417628
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->g:Z

    .line 2417629
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->i:Z

    .line 2417630
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->k:I

    .line 2417631
    const/16 v0, 0x8

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->m:D

    .line 2417632
    const/16 v0, 0x9

    const v1, 0x26518cd

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->n:I

    .line 2417633
    const/16 v0, 0xa

    const v1, -0x212ad9fb

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->o:I

    .line 2417634
    const/16 v0, 0xb

    const v1, -0x4a50b857

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->p:I

    .line 2417635
    const/16 v0, 0xd

    const v1, -0x7b228f91

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->r:I

    .line 2417636
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->s:Z

    .line 2417637
    const/16 v0, 0xf

    const v1, -0x62f8dc94

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->t:I

    .line 2417638
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2417639
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2417640
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2417641
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2417642
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 2417643
    :goto_0
    return-void

    .line 2417644
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2417645
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2417646
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;->a(Z)V

    .line 2417647
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2417648
    new-instance v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;

    invoke-direct {v0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel$NodeModel$PlacesRenderPriority1Model$PlacesRenderPriority1EdgesModel$PlacesRenderPriority1EdgesNodeModel;-><init>()V

    .line 2417649
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2417650
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2417651
    const v0, 0x75154661

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2417652
    const v0, 0x25d6af

    return v0
.end method
