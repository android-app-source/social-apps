.class public final Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x696ed4dc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Z

.field private k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:I

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:D

.field private o:Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2419854
    const-class v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2419853
    const-class v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2419851
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2419852
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 4

    .prologue
    .line 2419756
    iput-object p1, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->q:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2419757
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2419758
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2419759
    if-eqz v0, :cond_0

    .line 2419760
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0xc

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSavedState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2419761
    :cond_0
    return-void

    .line 2419762
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2419848
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2419849
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2419850
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAddress"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2419846
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2419847
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private l()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2419844
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->g:Ljava/util/List;

    .line 2419845
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2419842
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->i:Ljava/lang/String;

    .line 2419843
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2419840
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 2419841
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2419838
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->m:Ljava/lang/String;

    .line 2419839
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2419836
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->o:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->o:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 2419837
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->o:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2419834
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2419835
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2419832
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->q:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->q:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2419833
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->q:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 2419806
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2419807
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2419808
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x4aff4445    # 8364578.5f

    invoke-static {v2, v1, v3}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2419809
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->l()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 2419810
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2419811
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2419812
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2419813
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->p()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 2419814
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2419815
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->r()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 2419816
    const/16 v9, 0xd

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2419817
    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 2419818
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2419819
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2419820
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2419821
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2419822
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->j:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2419823
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2419824
    const/4 v0, 0x7

    iget v1, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->l:I

    invoke-virtual {p1, v0, v1, v10}, LX/186;->a(III)V

    .line 2419825
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2419826
    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->n:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2419827
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2419828
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2419829
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2419830
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2419831
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2419786
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2419787
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2419788
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x4aff4445    # 8364578.5f

    invoke-static {v2, v0, v3}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2419789
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2419790
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;

    .line 2419791
    iput v3, v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->f:I

    move-object v1, v0

    .line 2419792
    :cond_0
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2419793
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 2419794
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2419795
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;

    .line 2419796
    iput-object v0, v1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 2419797
    :cond_1
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2419798
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2419799
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2419800
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;

    .line 2419801
    iput-object v0, v1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2419802
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2419803
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 2419804
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object p0, v1

    .line 2419805
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2419785
    new-instance v0, LX/H34;

    invoke-direct {v0, p1}, LX/H34;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2419784
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2419777
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2419778
    const/4 v0, 0x1

    const v1, 0x4aff4445    # 8364578.5f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->f:I

    .line 2419779
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->h:Z

    .line 2419780
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->j:Z

    .line 2419781
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->l:I

    .line 2419782
    const/16 v0, 0x9

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->n:D

    .line 2419783
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2419771
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2419772
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->r()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2419773
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2419774
    const/16 v0, 0xc

    iput v0, p2, LX/18L;->c:I

    .line 2419775
    :goto_0
    return-void

    .line 2419776
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2419768
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2419769
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-direct {p0, p2}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;->a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 2419770
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2419765
    new-instance v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;

    invoke-direct {v0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$EdgesModel$NodeModel$ResultsModel$ResultsEdgesModel$ResultsEdgesNodeModel;-><init>()V

    .line 2419766
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2419767
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2419764
    const v0, 0x22013a60

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2419763
    const v0, 0x252222

    return v0
.end method
