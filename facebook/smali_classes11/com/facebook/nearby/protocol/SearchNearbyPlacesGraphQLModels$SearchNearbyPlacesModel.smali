.class public final Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7dcde344
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2420308
    const-class v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2420307
    const-class v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2420305
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2420306
    return-void
.end method

.method private a()Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getResultSections"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2420303
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel;->e:Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel;->e:Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel;

    .line 2420304
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel;->e:Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2420301
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel;->f:Ljava/lang/String;

    .line 2420302
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2420293
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2420294
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel;->a()Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2420295
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2420296
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2420297
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2420298
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2420299
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2420300
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2420280
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2420281
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel;->a()Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2420282
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel;->a()Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel;

    .line 2420283
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel;->a()Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2420284
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel;

    .line 2420285
    iput-object v0, v1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel;->e:Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel;

    .line 2420286
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2420287
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2420290
    new-instance v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel;

    invoke-direct {v0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel;-><init>()V

    .line 2420291
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2420292
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2420289
    const v0, 0x3e6fef51

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2420288
    const v0, 0x66d593e1

    return v0
.end method
