.class public final Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2419638
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2419639
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2419583
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2419584
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 2419623
    if-nez p1, :cond_0

    .line 2419624
    :goto_0
    return v0

    .line 2419625
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 2419626
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2419627
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2419628
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2419629
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2419630
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2419631
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2419632
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2419633
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2419634
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2419635
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2419636
    invoke-virtual {p3, v6, v3}, LX/186;->b(II)V

    .line 2419637
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4aff4445
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2419622
    new-instance v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2419619
    packed-switch p0, :pswitch_data_0

    .line 2419620
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2419621
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x4aff4445
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2419618
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2419616
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$DraculaImplementation;->b(I)V

    .line 2419617
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2419611
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2419612
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2419613
    :cond_0
    iput-object p1, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2419614
    iput p2, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$DraculaImplementation;->b:I

    .line 2419615
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2419610
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2419609
    new-instance v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2419606
    iget v0, p0, LX/1vt;->c:I

    .line 2419607
    move v0, v0

    .line 2419608
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2419603
    iget v0, p0, LX/1vt;->c:I

    .line 2419604
    move v0, v0

    .line 2419605
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2419600
    iget v0, p0, LX/1vt;->b:I

    .line 2419601
    move v0, v0

    .line 2419602
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2419597
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2419598
    move-object v0, v0

    .line 2419599
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2419588
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2419589
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2419590
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2419591
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2419592
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2419593
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2419594
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2419595
    invoke-static {v3, v9, v2}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2419596
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2419585
    iget v0, p0, LX/1vt;->c:I

    .line 2419586
    move v0, v0

    .line 2419587
    return v0
.end method
