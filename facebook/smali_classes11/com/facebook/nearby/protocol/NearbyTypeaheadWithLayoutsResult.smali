.class public Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/common/SearchSuggestion;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2419506
    const-class v0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;

    sput-object v0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;->e:Ljava/lang/Class;

    .line 2419507
    new-instance v0, LX/H30;

    invoke-direct {v0}, LX/H30;-><init>()V

    sput-object v0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2419508
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 2419509
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;->a:Ljava/lang/String;

    .line 2419510
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;->b:Ljava/lang/String;

    .line 2419511
    const-class v0, Lcom/facebook/nearby/common/SearchSuggestion;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;->c:Ljava/util/List;

    .line 2419512
    const-class v0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;->d:Ljava/util/List;

    .line 2419513
    return-void
.end method

.method public constructor <init>(Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutResult;)V
    .locals 12

    .prologue
    .line 2419514
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 2419515
    iget-wide v4, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v2, v4

    .line 2419516
    invoke-direct {p0, v0, v2, v3}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 2419517
    iget-object v0, p1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;->a:Ljava/lang/String;

    .line 2419518
    iget-object v0, p1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;->b:Ljava/lang/String;

    .line 2419519
    iget-object v0, p1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;->c:Ljava/util/List;

    .line 2419520
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2419521
    if-nez p2, :cond_1

    .line 2419522
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    move-object v2, v0

    .line 2419523
    :goto_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2419524
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    move v3, v4

    .line 2419525
    :goto_1
    iget-object v0, p1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/model/TypeaheadPlace;

    .line 2419526
    iget-object v1, v0, Lcom/facebook/nearby/model/TypeaheadPlace;->a:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2419527
    if-eqz v3, :cond_0

    if-nez v1, :cond_0

    .line 2419528
    sget-object v8, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;->e:Ljava/lang/Class;

    const-string v9, "missing layout for place id = %s"

    new-array v10, v4, [Ljava/lang/Object;

    iget-object v11, v0, Lcom/facebook/nearby/model/TypeaheadPlace;->a:Ljava/lang/String;

    aput-object v11, v10, v5

    invoke-static {v8, v9, v10}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2419529
    :cond_0
    new-instance v8, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;

    invoke-direct {v8, v0, v1}, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;-><init>(Lcom/facebook/nearby/model/TypeaheadPlace;Ljava/lang/String;)V

    invoke-virtual {v6, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 2419530
    :cond_1
    iget-object v0, p2, Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutResult;->a:LX/0P1;

    move-object v0, v0

    .line 2419531
    move-object v2, v0

    goto :goto_0

    :cond_2
    move v3, v5

    .line 2419532
    goto :goto_1

    .line 2419533
    :cond_3
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2419534
    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;->d:Ljava/util/List;

    .line 2419535
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2419536
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2419537
    invoke-super {p0, p1, p2}, Lcom/facebook/fbservice/results/BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2419538
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2419539
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2419540
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;->c:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2419541
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;->d:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2419542
    return-void
.end method
