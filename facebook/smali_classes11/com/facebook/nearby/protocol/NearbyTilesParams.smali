.class public Lcom/facebook/nearby/protocol/NearbyTilesParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/protocol/NearbyTilesParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:F

.field public final d:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

.field public final e:F

.field public final f:Landroid/location/Location;

.field public final g:F

.field public final h:I

.field public final i:F

.field public final j:F

.field public final k:F

.field public final l:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2419346
    new-instance v0, LX/H2v;

    invoke-direct {v0}, LX/H2v;-><init>()V

    sput-object v0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2419332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2419333
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->d:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 2419334
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->e:F

    .line 2419335
    const-class v0, Landroid/location/Location;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->f:Landroid/location/Location;

    .line 2419336
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->g:F

    .line 2419337
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->h:I

    .line 2419338
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->i:F

    .line 2419339
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->j:F

    .line 2419340
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->k:F

    .line 2419341
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->l:F

    .line 2419342
    const-class v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->a:Ljava/util/List;

    .line 2419343
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->b:LX/0Px;

    .line 2419344
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->c:F

    .line 2419345
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLGeoRectangle;
    .locals 1

    .prologue
    .line 2419363
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->d:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    return-object v0
.end method

.method public final b()F
    .locals 1

    .prologue
    .line 2419362
    iget v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->e:F

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2419364
    const/4 v0, 0x0

    return v0
.end method

.method public final j()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2419361
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->a:Ljava/util/List;

    return-object v0
.end method

.method public final k()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2419360
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->b:LX/0Px;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2419347
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->d:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2419348
    iget v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->e:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2419349
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->f:Landroid/location/Location;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2419350
    iget v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->g:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2419351
    iget v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2419352
    iget v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->i:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2419353
    iget v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->j:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2419354
    iget v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->k:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2419355
    iget v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->l:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2419356
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2419357
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2419358
    iget v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesParams;->c:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2419359
    return-void
.end method
