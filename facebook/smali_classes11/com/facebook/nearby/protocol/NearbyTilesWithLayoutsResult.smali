.class public Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/model/MapTile;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/nearby/protocol/NearbyTilesParams;

.field private final g:Lcom/facebook/graphql/model/GraphQLGeoRectangle;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2419485
    const-class v0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;

    sput-object v0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->a:Ljava/lang/Class;

    .line 2419486
    new-instance v0, LX/H2y;

    invoke-direct {v0}, LX/H2y;-><init>()V

    sput-object v0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0ta;JLjava/util/List;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;Lcom/facebook/graphql/model/GraphQLGeoRectangle;Lcom/facebook/nearby/protocol/NearbyTilesParams;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ta;",
            "J",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/model/MapTile;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLGeoRectangle;",
            "Lcom/facebook/nearby/protocol/NearbyTilesParams;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2419476
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 2419477
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2419478
    iput-object p4, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->b:Ljava/util/List;

    .line 2419479
    iput-object p5, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->c:Ljava/lang/String;

    .line 2419480
    invoke-static {p6}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->d:LX/0Rf;

    .line 2419481
    invoke-static {p7}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->e:LX/0Rf;

    .line 2419482
    iput-object p8, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->g:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 2419483
    iput-object p9, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->f:Lcom/facebook/nearby/protocol/NearbyTilesParams;

    .line 2419484
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2419421
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 2419422
    const-class v0, Lcom/facebook/graphql/model/GraphQLMapTileNode;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->b:Ljava/util/List;

    .line 2419423
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->c:Ljava/lang/String;

    .line 2419424
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2419425
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->d:LX/0Rf;

    .line 2419426
    const-class v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->e:LX/0Rf;

    .line 2419427
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->g:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 2419428
    const-class v0, Lcom/facebook/nearby/protocol/NearbyTilesParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/NearbyTilesParams;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->f:Lcom/facebook/nearby/protocol/NearbyTilesParams;

    .line 2419429
    return-void

    .line 2419430
    :cond_0
    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/nearby/protocol/NearbyTilesResult;Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutResult;)V
    .locals 6

    .prologue
    .line 2419456
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 2419457
    iget-wide v4, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v2, v4

    .line 2419458
    invoke-direct {p0, v0, v2, v3}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 2419459
    iget-object v0, p1, Lcom/facebook/nearby/protocol/NearbyTilesResult;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2419460
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2419461
    iget-object v0, p1, Lcom/facebook/nearby/protocol/NearbyTilesResult;->a:Ljava/util/List;

    move-object v1, v0

    .line 2419462
    if-nez p2, :cond_0

    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    :goto_0
    invoke-static {v1, v0}, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->a(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->b:Ljava/util/List;

    .line 2419463
    iget-object v0, p1, Lcom/facebook/nearby/protocol/NearbyTilesResult;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2419464
    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->c:Ljava/lang/String;

    .line 2419465
    iget-object v0, p1, Lcom/facebook/nearby/protocol/NearbyTilesResult;->c:LX/0Rf;

    move-object v0, v0

    .line 2419466
    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->d:LX/0Rf;

    .line 2419467
    iget-object v0, p1, Lcom/facebook/nearby/protocol/NearbyTilesResult;->d:LX/0Rf;

    move-object v0, v0

    .line 2419468
    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->e:LX/0Rf;

    .line 2419469
    iget-object v0, p1, Lcom/facebook/nearby/protocol/NearbyTilesResult;->f:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-object v0, v0

    .line 2419470
    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->g:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 2419471
    iget-object v0, p1, Lcom/facebook/nearby/protocol/NearbyTilesResult;->e:Lcom/facebook/nearby/protocol/NearbyTilesParams;

    move-object v0, v0

    .line 2419472
    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->f:Lcom/facebook/nearby/protocol/NearbyTilesParams;

    .line 2419473
    return-void

    .line 2419474
    :cond_0
    iget-object v0, p2, Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutResult;->a:LX/0P1;

    move-object v0, v0

    .line 2419475
    goto :goto_0
.end method

.method private static a(Ljava/util/Map;Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;)LX/0Px;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 2419445
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2419446
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_1

    move v4, v5

    .line 2419447
    :goto_0
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2419448
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v2, v3

    :goto_1
    if-ge v2, v8, :cond_2

    invoke-virtual {v7, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    .line 2419449
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2419450
    if-eqz v4, :cond_0

    if-nez v1, :cond_0

    .line 2419451
    sget-object v9, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->a:Ljava/lang/Class;

    const-string v10, "missing layout for place id = %s"

    new-array v11, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v3

    invoke-static {v9, v10, v11}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2419452
    :cond_0
    new-instance v9, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;

    invoke-direct {v9, v0, v1}, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;-><init>(Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;Ljava/lang/String;)V

    invoke-virtual {v6, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2419453
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move v4, v3

    .line 2419454
    goto :goto_0

    .line 2419455
    :cond_2
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMapTile;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/model/MapTile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2419441
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 2419442
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/facebook/graphql/model/GraphQLMapTile;

    .line 2419443
    new-instance v0, Lcom/facebook/nearby/model/MapTile;

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLMapTile;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLMapTile;->b()J

    move-result-wide v2

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLMapTile;->c()J

    move-result-wide v4

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLMapTile;->e()F

    move-result v6

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLMapTile;->f()F

    move-result v7

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLMapTile;->d()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v8

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLMapTile;->g()Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;

    move-result-object v9

    invoke-static {p1, v9}, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->a(Ljava/util/Map;Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;)LX/0Px;

    move-result-object v9

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLMapTile;->h()Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;

    move-result-object v10

    invoke-static {p1, v10}, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->a(Ljava/util/Map;Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;)LX/0Px;

    move-result-object v10

    invoke-direct/range {v0 .. v10}, Lcom/facebook/nearby/model/MapTile;-><init>(Ljava/lang/String;JJFFLcom/facebook/graphql/model/GraphQLGeoRectangle;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v11, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2419444
    :cond_0
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/model/MapTile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2419440
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->b:Ljava/util/List;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2419439
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2419431
    invoke-super {p0, p1, p2}, Lcom/facebook/fbservice/results/BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2419432
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->b:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2419433
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2419434
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->d:LX/0Rf;

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2419435
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->e:LX/0Rf;

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2419436
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->g:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2419437
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->f:Lcom/facebook/nearby/protocol/NearbyTilesParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2419438
    return-void
.end method
