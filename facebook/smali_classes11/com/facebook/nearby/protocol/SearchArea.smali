.class public Lcom/facebook/nearby/protocol/SearchArea;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/protocol/SearchArea;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/location/Location;

.field public final b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2419558
    new-instance v0, LX/H31;

    invoke-direct {v0}, LX/H31;-><init>()V

    sput-object v0, Lcom/facebook/nearby/protocol/SearchArea;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/location/Location;I)V
    .locals 0

    .prologue
    .line 2419546
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2419547
    iput-object p1, p0, Lcom/facebook/nearby/protocol/SearchArea;->a:Landroid/location/Location;

    .line 2419548
    iput p2, p0, Lcom/facebook/nearby/protocol/SearchArea;->b:I

    .line 2419549
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2419554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2419555
    const-class v0, Landroid/location/Location;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchArea;->a:Landroid/location/Location;

    .line 2419556
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/protocol/SearchArea;->b:I

    .line 2419557
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2419553
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2419550
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchArea;->a:Landroid/location/Location;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2419551
    iget v0, p0, Lcom/facebook/nearby/protocol/SearchArea;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2419552
    return-void
.end method
