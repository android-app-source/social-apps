.class public final Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2417399
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2417400
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2417397
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2417398
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 12

    .prologue
    .line 2417306
    if-nez p1, :cond_0

    .line 2417307
    const/4 v0, 0x0

    .line 2417308
    :goto_0
    return v0

    .line 2417309
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2417310
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2417311
    :sswitch_0
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2417312
    const/4 v0, 0x1

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2417313
    const/4 v0, 0x2

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v8

    .line 2417314
    const/4 v0, 0x3

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v10

    .line 2417315
    const/4 v0, 0x4

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    .line 2417316
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2417317
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v6

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2417318
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2417319
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v10

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2417320
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2417321
    :sswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2417322
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2417323
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2417324
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2417325
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2417326
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2417327
    const/4 v3, 0x3

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2417328
    const/4 v3, 0x0

    invoke-virtual {p3, v3, v0}, LX/186;->b(II)V

    .line 2417329
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2417330
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 2417331
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2417332
    :sswitch_2
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2417333
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2417334
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v0, v2}, LX/186;->a(III)V

    .line 2417335
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2417336
    :sswitch_3
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2417337
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2417338
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v0, v2}, LX/186;->a(III)V

    .line 2417339
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2417340
    :sswitch_4
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2417341
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2417342
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2417343
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2417344
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2417345
    :sswitch_5
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2417346
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2417347
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2417348
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2417349
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2417350
    :sswitch_6
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2417351
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2417352
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v0, v2}, LX/186;->a(III)V

    .line 2417353
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2417354
    :sswitch_7
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2417355
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2417356
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2417357
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2417358
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2417359
    :sswitch_8
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2417360
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2417361
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2417362
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2417363
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2417364
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2417365
    const/4 v3, 0x3

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2417366
    const/4 v3, 0x0

    invoke-virtual {p3, v3, v0}, LX/186;->b(II)V

    .line 2417367
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2417368
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 2417369
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2417370
    :sswitch_9
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2417371
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2417372
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v0, v2}, LX/186;->a(III)V

    .line 2417373
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2417374
    :sswitch_a
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2417375
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2417376
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v0, v2}, LX/186;->a(III)V

    .line 2417377
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2417378
    :sswitch_b
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2417379
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2417380
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2417381
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2417382
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2417383
    :sswitch_c
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2417384
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2417385
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2417386
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2417387
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2417388
    :sswitch_d
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2417389
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2417390
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v0, v2}, LX/186;->a(III)V

    .line 2417391
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2417392
    :sswitch_e
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2417393
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2417394
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2417395
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2417396
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7b228f91 -> :sswitch_5
        -0x70ee37fe -> :sswitch_a
        -0x62f8dc94 -> :sswitch_6
        -0x4f52b9db -> :sswitch_7
        -0x4b0dad63 -> :sswitch_8
        -0x4a50b857 -> :sswitch_4
        -0x32eb491f -> :sswitch_1
        -0x212ad9fb -> :sswitch_3
        0x26518cd -> :sswitch_2
        0x5321705 -> :sswitch_e
        0xf666f46 -> :sswitch_b
        0x32bc24d0 -> :sswitch_c
        0x40fea11a -> :sswitch_d
        0x4e830a12 -> :sswitch_0
        0x67ab8a51 -> :sswitch_9
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2417305
    new-instance v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2417302
    sparse-switch p0, :sswitch_data_0

    .line 2417303
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2417304
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x7b228f91 -> :sswitch_0
        -0x70ee37fe -> :sswitch_0
        -0x62f8dc94 -> :sswitch_0
        -0x4f52b9db -> :sswitch_0
        -0x4b0dad63 -> :sswitch_0
        -0x4a50b857 -> :sswitch_0
        -0x32eb491f -> :sswitch_0
        -0x212ad9fb -> :sswitch_0
        0x26518cd -> :sswitch_0
        0x5321705 -> :sswitch_0
        0xf666f46 -> :sswitch_0
        0x32bc24d0 -> :sswitch_0
        0x40fea11a -> :sswitch_0
        0x4e830a12 -> :sswitch_0
        0x67ab8a51 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2417301
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2417299
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->b(I)V

    .line 2417300
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2417268
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2417269
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2417270
    :cond_0
    iput-object p1, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2417271
    iput p2, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->b:I

    .line 2417272
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2417298
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2417297
    new-instance v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2417294
    iget v0, p0, LX/1vt;->c:I

    .line 2417295
    move v0, v0

    .line 2417296
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2417291
    iget v0, p0, LX/1vt;->c:I

    .line 2417292
    move v0, v0

    .line 2417293
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2417288
    iget v0, p0, LX/1vt;->b:I

    .line 2417289
    move v0, v0

    .line 2417290
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2417285
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2417286
    move-object v0, v0

    .line 2417287
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2417276
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2417277
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2417278
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2417279
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2417280
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2417281
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2417282
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2417283
    invoke-static {v3, v9, v2}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2417284
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2417273
    iget v0, p0, LX/1vt;->c:I

    .line 2417274
    move v0, v0

    .line 2417275
    return v0
.end method
