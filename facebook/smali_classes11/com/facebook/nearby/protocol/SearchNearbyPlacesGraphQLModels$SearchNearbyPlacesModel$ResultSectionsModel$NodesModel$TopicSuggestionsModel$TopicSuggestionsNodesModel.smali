.class public final Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2e8da1e3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel$TopicModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2420167
    const-class v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2420166
    const-class v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2420164
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2420165
    return-void
.end method

.method private a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getBounds"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2420162
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2420163
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2420160
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2420161
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2420111
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2420112
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private l()Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel$TopicModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2420158
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->h:Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel$TopicModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel$TopicModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel$TopicModel;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->h:Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel$TopicModel;

    .line 2420159
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->h:Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel$TopicModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2420146
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2420147
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x4e830a12

    invoke-static {v1, v0, v2}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2420148
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2420149
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2420150
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->l()Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel$TopicModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2420151
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2420152
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2420153
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2420154
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2420155
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2420156
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2420157
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2420121
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2420122
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2420123
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x4e830a12

    invoke-static {v2, v0, v3}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2420124
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2420125
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;

    .line 2420126
    iput v3, v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->e:I

    move-object v1, v0

    .line 2420127
    :cond_0
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2420128
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2420129
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2420130
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;

    .line 2420131
    iput-object v0, v1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2420132
    :cond_1
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2420133
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2420134
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2420135
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;

    .line 2420136
    iput-object v0, v1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2420137
    :cond_2
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->l()Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel$TopicModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2420138
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->l()Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel$TopicModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel$TopicModel;

    .line 2420139
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->l()Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel$TopicModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2420140
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;

    .line 2420141
    iput-object v0, v1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->h:Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel$TopicModel;

    .line 2420142
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2420143
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    .line 2420144
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    move-object p0, v1

    .line 2420145
    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2420118
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2420119
    const/4 v0, 0x0

    const v1, 0x4e830a12

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;->e:I

    .line 2420120
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2420115
    new-instance v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;

    invoke-direct {v0}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel$ResultSectionsModel$NodesModel$TopicSuggestionsModel$TopicSuggestionsNodesModel;-><init>()V

    .line 2420116
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2420117
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2420114
    const v0, 0x661574fe

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2420113
    const v0, 0x457b568b

    return v0
.end method
