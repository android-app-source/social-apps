.class public Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/nearby/protocol/NearbyTilesParams;

.field public final b:Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutParams;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2419417
    new-instance v0, LX/H2x;

    invoke-direct {v0}, LX/H2x;-><init>()V

    sput-object v0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2419405
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2419406
    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsParams;->a:Lcom/facebook/nearby/protocol/NearbyTilesParams;

    .line 2419407
    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsParams;->b:Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutParams;

    .line 2419408
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2419409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2419410
    const-class v0, Lcom/facebook/nearby/protocol/NearbyTilesParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/NearbyTilesParams;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsParams;->a:Lcom/facebook/nearby/protocol/NearbyTilesParams;

    .line 2419411
    const-class v0, Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutParams;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsParams;->b:Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutParams;

    .line 2419412
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2419413
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2419414
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsParams;->a:Lcom/facebook/nearby/protocol/NearbyTilesParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2419415
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsParams;->b:Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2419416
    return-void
.end method
