.class public Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;

.field public final b:Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutParams;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2419502
    new-instance v0, LX/H2z;

    invoke-direct {v0}, LX/H2z;-><init>()V

    sput-object v0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2419498
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2419499
    const-class v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsParams;->a:Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;

    .line 2419500
    const-class v0, Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutParams;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsParams;->b:Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutParams;

    .line 2419501
    return-void
.end method

.method public constructor <init>(Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutParams;)V
    .locals 0

    .prologue
    .line 2419494
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2419495
    iput-object p1, p0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsParams;->a:Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;

    .line 2419496
    iput-object p2, p0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsParams;->b:Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutParams;

    .line 2419497
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2419490
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2419491
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsParams;->a:Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2419492
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsParams;->b:Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2419493
    return-void
.end method
