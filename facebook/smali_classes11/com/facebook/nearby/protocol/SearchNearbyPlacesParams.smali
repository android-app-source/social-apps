.class public Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/nearby/protocol/SearchArea;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/location/Location;

.field public final d:F

.field public final e:F

.field public final f:F

.field public final g:F

.field public final h:F

.field public final i:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2420860
    new-instance v0, LX/H3K;

    invoke-direct {v0}, LX/H3K;-><init>()V

    sput-object v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/H3L;)V
    .locals 1

    .prologue
    .line 2420827
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2420828
    iget-object v0, p1, LX/H3L;->a:Lcom/facebook/nearby/protocol/SearchArea;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->a:Lcom/facebook/nearby/protocol/SearchArea;

    .line 2420829
    iget-object v0, p1, LX/H3L;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->b:Ljava/lang/String;

    .line 2420830
    iget-object v0, p1, LX/H3L;->c:Landroid/location/Location;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->c:Landroid/location/Location;

    .line 2420831
    iget v0, p1, LX/H3L;->d:F

    iput v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->d:F

    .line 2420832
    iget v0, p1, LX/H3L;->e:F

    iput v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->e:F

    .line 2420833
    iget v0, p1, LX/H3L;->f:F

    iput v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->f:F

    .line 2420834
    iget v0, p1, LX/H3L;->g:F

    iput v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->g:F

    .line 2420835
    iget v0, p1, LX/H3L;->h:F

    iput v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->h:F

    .line 2420836
    iget v0, p1, LX/H3L;->i:I

    iput v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->i:I

    .line 2420837
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2420838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2420839
    const-class v0, Lcom/facebook/nearby/protocol/SearchArea;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/SearchArea;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->a:Lcom/facebook/nearby/protocol/SearchArea;

    .line 2420840
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->b:Ljava/lang/String;

    .line 2420841
    const-class v0, Landroid/location/Location;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->c:Landroid/location/Location;

    .line 2420842
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->d:F

    .line 2420843
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->e:F

    .line 2420844
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->f:F

    .line 2420845
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->g:F

    .line 2420846
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->h:F

    .line 2420847
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->i:I

    .line 2420848
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2420849
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2420850
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->a:Lcom/facebook/nearby/protocol/SearchArea;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2420851
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2420852
    iget-object v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->c:Landroid/location/Location;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2420853
    iget v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->d:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2420854
    iget v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->e:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2420855
    iget v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->f:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2420856
    iget v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->g:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2420857
    iget v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->h:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2420858
    iget v0, p0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2420859
    return-void
.end method
