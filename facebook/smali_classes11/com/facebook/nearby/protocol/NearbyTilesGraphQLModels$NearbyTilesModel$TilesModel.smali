.class public final Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x97fc845
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2418210
    const-class v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2418209
    const-class v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2418207
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2418208
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2418205
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->e:Ljava/lang/String;

    .line 2418206
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDisplayRegionHint"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2418164
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2418165
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEdges"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2418203
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->g:Ljava/util/List;

    .line 2418204
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2418201
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->h:Ljava/lang/String;

    .line 2418202
    iget-object v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2418189
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2418190
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2418191
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x4e830a12

    invoke-static {v2, v1, v3}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2418192
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->k()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 2418193
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2418194
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2418195
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2418196
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2418197
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2418198
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2418199
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2418200
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2418174
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2418175
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2418176
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x4e830a12

    invoke-static {v2, v0, v3}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2418177
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2418178
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;

    .line 2418179
    iput v3, v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->f:I

    .line 2418180
    :goto_0
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2418181
    invoke-direct {p0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2418182
    if-eqz v1, :cond_0

    .line 2418183
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;

    .line 2418184
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->g:Ljava/util/List;

    .line 2418185
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2418186
    if-nez v0, :cond_1

    :goto_1
    return-object p0

    .line 2418187
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move-object p0, v0

    .line 2418188
    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2418171
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2418172
    const/4 v0, 0x1

    const v1, 0x4e830a12

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;->f:I

    .line 2418173
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2418168
    new-instance v0, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;

    invoke-direct {v0}, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel$TilesModel;-><init>()V

    .line 2418169
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2418170
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2418167
    const v0, -0x557946c9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2418166
    const v0, -0x4ac1e5c9

    return v0
.end method
