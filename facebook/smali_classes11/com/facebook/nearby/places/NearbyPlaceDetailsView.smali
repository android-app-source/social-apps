.class public Lcom/facebook/nearby/places/NearbyPlaceDetailsView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/saved/gating/annotations/IsSavedForLaterEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/nearby/annotations/IsFacepileInNearbyPlaceResultsEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/6aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final e:F

.field private final f:F

.field private final g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final h:Landroid/widget/TextView;

.field private final i:Lcom/facebook/fbui/facepile/FacepileView;

.field private final j:Landroid/widget/TextView;

.field private final k:Landroid/widget/TextView;

.field private final l:Landroid/widget/FrameLayout;

.field private final m:Landroid/widget/RatingBar;

.field private final n:Landroid/widget/ImageView;

.field private final o:Landroid/widget/TextView;

.field private final p:Landroid/widget/TextView;

.field private q:Landroid/text/style/ForegroundColorSpan;

.field private final r:Landroid/widget/TextView;

.field private final s:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2416787
    const-class v0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;

    const-string v1, "nearby_places"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2416785
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2416786
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2416783
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2416784
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 2416764
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2416765
    const v0, 0x7f030bb4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2416766
    const-class v0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;

    invoke-static {v0, p0}, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2416767
    const v0, 0x7f0d162b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->h:Landroid/widget/TextView;

    .line 2416768
    const v0, 0x7f0d094c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2416769
    const v0, 0x7f0d1d0f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->i:Lcom/facebook/fbui/facepile/FacepileView;

    .line 2416770
    const v0, 0x7f0d1d17

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->j:Landroid/widget/TextView;

    .line 2416771
    const v0, 0x7f0d1d16

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->k:Landroid/widget/TextView;

    .line 2416772
    const v0, 0x7f0d1d0e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->r:Landroid/widget/TextView;

    .line 2416773
    const v0, 0x7f0d1d11

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->l:Landroid/widget/FrameLayout;

    .line 2416774
    const v0, 0x7f0d1d12

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->m:Landroid/widget/RatingBar;

    .line 2416775
    const v0, 0x7f0d1d15

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->p:Landroid/widget/TextView;

    .line 2416776
    const v0, 0x7f0d1d13

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->n:Landroid/widget/ImageView;

    .line 2416777
    const v0, 0x7f0d1d14

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->o:Landroid/widget/TextView;

    .line 2416778
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0577

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->q:Landroid/text/style/ForegroundColorSpan;

    .line 2416779
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->s:Landroid/content/res/Resources;

    .line 2416780
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->s:Landroid/content/res/Resources;

    const v1, 0x7f0b004e

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->e:F

    .line 2416781
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->s:Landroid/content/res/Resources;

    const v1, 0x7f0b0050

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->f:F

    .line 2416782
    return-void
.end method

.method private static a(Lcom/facebook/nearby/places/NearbyPlaceDetailsView;LX/0Or;LX/0Or;LX/6aG;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/nearby/places/NearbyPlaceDetailsView;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/6aG;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2416763
    iput-object p1, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->c:LX/6aG;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;

    const/16 v1, 0x35f

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x33a

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {v0}, LX/6aG;->b(LX/0QB;)LX/6aG;

    move-result-object v0

    check-cast v0, LX/6aG;

    invoke-static {p0, v1, v2, v0}, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->a(Lcom/facebook/nearby/places/NearbyPlaceDetailsView;LX/0Or;LX/0Or;LX/6aG;)V

    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 2416762
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method

.method private getPermanentlyClosedText()Landroid/text/SpannableStringBuilder;
    .locals 5

    .prologue
    .line 2416717
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2416718
    iget-object v1, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->s:Landroid/content/res/Resources;

    const v2, 0x7f081797

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2416719
    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2416720
    iget-object v2, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->q:Landroid/text/style/ForegroundColorSpan;

    const/4 v3, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v4, 0x21

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2416721
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/nearby/model/TypeaheadPlace;Landroid/location/Location;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/16 v6, 0x8

    .line 2416722
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->h:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/facebook/nearby/model/TypeaheadPlace;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2416723
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p1, Lcom/facebook/nearby/model/TypeaheadPlace;->d:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2416724
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->m:Landroid/widget/RatingBar;

    invoke-virtual {v0, v6}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 2416725
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2416726
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2416727
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2416728
    invoke-direct {p0}, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/nearby/model/TypeaheadPlace;->k:Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v0, v1, :cond_1

    .line 2416729
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2416730
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2416731
    :cond_0
    :goto_0
    iget-boolean v0, p1, Lcom/facebook/nearby/model/TypeaheadPlace;->j:Z

    if-eqz v0, :cond_2

    .line 2416732
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->k:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->getPermanentlyClosedText()Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2416733
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->k:Landroid/widget/TextView;

    iget v1, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->f:F

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 2416734
    :goto_1
    if-nez p2, :cond_4

    .line 2416735
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2416736
    :goto_2
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2416737
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->i:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v6}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 2416738
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2416739
    return-void

    .line 2416740
    :cond_1
    iget-wide v0, p1, Lcom/facebook/nearby/model/TypeaheadPlace;->c:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 2416741
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->m:Landroid/widget/RatingBar;

    invoke-virtual {v0, v4}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 2416742
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->m:Landroid/widget/RatingBar;

    iget-wide v2, p1, Lcom/facebook/nearby/model/TypeaheadPlace;->c:D

    double-to-float v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setRating(F)V

    goto :goto_0

    .line 2416743
    :cond_2
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->k:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/facebook/nearby/model/TypeaheadPlace;->f:Ljava/util/List;

    iget-object v2, p1, Lcom/facebook/nearby/model/TypeaheadPlace;->g:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 2416744
    const-string v7, ""

    .line 2416745
    const-string v5, ""

    .line 2416746
    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 2416747
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v7, v3

    .line 2416748
    :cond_3
    if-eqz v2, :cond_8

    .line 2416749
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStreetAddress;->l()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 2416750
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStreetAddress;->l()Ljava/lang/String;

    move-result-object v3

    .line 2416751
    :goto_3
    invoke-static {v7}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2416752
    :goto_4
    move-object v1, v3

    .line 2416753
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2416754
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->k:Landroid/widget/TextView;

    iget v1, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->e:F

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    goto :goto_1

    .line 2416755
    :cond_4
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2416756
    iget-object v0, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->r:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->c:LX/6aG;

    iget-object v2, p1, Lcom/facebook/nearby/model/TypeaheadPlace;->e:Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v2

    iget-object v4, p1, Lcom/facebook/nearby/model/TypeaheadPlace;->e:Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, LX/6aA;->a(DD)Landroid/location/Location;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, LX/6aG;->a(Landroid/location/Location;Landroid/location/Location;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 2416757
    :cond_5
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStreetAddress;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 2416758
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStreetAddress;->a()Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 2416759
    :cond_6
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    move-object v3, v7

    .line 2416760
    goto :goto_4

    .line 2416761
    :cond_7
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " \u00b7 "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    :cond_8
    move-object v3, v5

    goto :goto_3
.end method
