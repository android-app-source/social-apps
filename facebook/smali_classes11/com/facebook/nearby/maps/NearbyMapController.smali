.class public Lcom/facebook/nearby/maps/NearbyMapController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final c:LX/H1o;

.field private final d:LX/1Uo;

.field private final e:LX/1Ad;

.field private final f:LX/H23;

.field private final g:LX/CIt;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2416284
    const-class v0, Lcom/facebook/nearby/maps/NearbyMapController;

    sput-object v0, Lcom/facebook/nearby/maps/NearbyMapController;->a:Ljava/lang/Class;

    .line 2416285
    const-class v0, Lcom/facebook/nearby/maps/NearbyMapController;

    const-string v1, "nearby_map"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/nearby/maps/NearbyMapController;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1Ad;Landroid/content/Context;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2416275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2416276
    new-instance v0, LX/CIt;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/CIt;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/nearby/maps/NearbyMapController;->g:LX/CIt;

    .line 2416277
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/nearby/maps/NearbyMapController;->h:Ljava/util/List;

    .line 2416278
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/nearby/maps/NearbyMapController;->i:Ljava/util/List;

    .line 2416279
    new-instance v0, LX/H23;

    invoke-direct {v0}, LX/H23;-><init>()V

    iput-object v0, p0, Lcom/facebook/nearby/maps/NearbyMapController;->f:LX/H23;

    .line 2416280
    new-instance v0, LX/H1o;

    invoke-direct {v0}, LX/H1o;-><init>()V

    iput-object v0, p0, Lcom/facebook/nearby/maps/NearbyMapController;->c:LX/H1o;

    .line 2416281
    new-instance v0, LX/1Uo;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/nearby/maps/NearbyMapController;->d:LX/1Uo;

    .line 2416282
    iput-object p1, p0, Lcom/facebook/nearby/maps/NearbyMapController;->e:LX/1Ad;

    .line 2416283
    return-void
.end method
