.class public final Lcom/facebook/selfupdate/SelfUpdateManager$DownloadErrorEventHandler$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/EmW;

.field public final synthetic b:LX/Fjm;


# direct methods
.method public constructor <init>(LX/Fjm;LX/EmW;)V
    .locals 0

    .prologue
    .line 2277633
    iput-object p1, p0, Lcom/facebook/selfupdate/SelfUpdateManager$DownloadErrorEventHandler$1;->b:LX/Fjm;

    iput-object p2, p0, Lcom/facebook/selfupdate/SelfUpdateManager$DownloadErrorEventHandler$1;->a:LX/EmW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    .line 2277634
    iget-object v0, p0, Lcom/facebook/selfupdate/SelfUpdateManager$DownloadErrorEventHandler$1;->b:LX/Fjm;

    iget-object v0, v0, LX/Fjm;->a:LX/Fjp;

    iget-object v1, p0, Lcom/facebook/selfupdate/SelfUpdateManager$DownloadErrorEventHandler$1;->a:LX/EmW;

    const/4 v9, 0x0

    .line 2277635
    iget-object v2, v1, LX/EmW;->c:LX/EmV;

    move-object v3, v2

    .line 2277636
    iget-object v2, v0, LX/Fjp;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/Fjg;->r:LX/0Tn;

    const-string v5, ""

    invoke-interface {v2, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2277637
    iget-object v2, v0, LX/Fjp;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fjj;

    const-string v5, "selfupdate_download_failed_with_error"

    const-string v6, "error"

    invoke-virtual {v3}, LX/EmV;->name()Ljava/lang/String;

    move-result-object v7

    const-string v8, "source"

    invoke-static {v6, v7, v8, v4}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, LX/Fjj;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2277638
    sget-object v2, LX/EmV;->ERROR_NO_FREE_SPACE_TO_DOWNLOAD:LX/EmV;

    if-ne v3, v2, :cond_1

    .line 2277639
    iget-object v2, v0, LX/Fjp;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fjj;

    const-string v3, "selfupdate_no_free_space_to_download"

    invoke-virtual {v2, v3, v9}, LX/Fjj;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2277640
    :cond_0
    :goto_0
    return-void

    .line 2277641
    :cond_1
    sget-object v2, LX/EmV;->ERROR_FAILED_TO_REMOVE_DOWNLOAD_ID:LX/EmV;

    if-ne v3, v2, :cond_2

    .line 2277642
    iget-object v2, v0, LX/Fjp;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fjj;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to remove download ID from DownloadManager after making a copy: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2277643
    iget-wide v10, v1, LX/EmW;->e:J

    move-wide v4, v10

    .line 2277644
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2277645
    iget-object v4, v1, LX/EmW;->d:Ljava/lang/Exception;

    move-object v4, v4

    .line 2277646
    invoke-virtual {v2, v3, v4}, LX/Fjj;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2277647
    :cond_2
    sget-object v2, LX/EmV;->ERROR_DOWNLOAD_MANAGER_COMPLETION_EXCEPTION:LX/EmV;

    if-ne v3, v2, :cond_3

    .line 2277648
    iget-object v2, v0, LX/Fjp;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fjj;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Download Manager complete exception for ID "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2277649
    iget-wide v10, v1, LX/EmW;->e:J

    move-wide v4, v10

    .line 2277650
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2277651
    iget-object v4, v1, LX/EmW;->d:Ljava/lang/Exception;

    move-object v4, v4

    .line 2277652
    invoke-virtual {v2, v3, v4}, LX/Fjj;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2277653
    iget-object v2, v0, LX/Fjp;->r:LX/0lC;

    invoke-virtual {v2}, LX/0lC;->e()LX/0m9;

    move-result-object v3

    .line 2277654
    const-string v2, "error_arg"

    .line 2277655
    iget-wide v10, v1, LX/EmW;->e:J

    move-wide v4, v10

    .line 2277656
    invoke-virtual {v3, v2, v4, v5}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 2277657
    const-string v2, "error_exception"

    .line 2277658
    iget-object v4, v1, LX/EmW;->d:Ljava/lang/Exception;

    move-object v4, v4

    .line 2277659
    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2277660
    iget-object v2, v0, LX/Fjp;->j:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fjr;

    const-string v4, "downloadmanager_completion_exception"

    invoke-virtual {v2, v4, v3}, LX/Fjr;->a(Ljava/lang/String;LX/0m9;)V

    goto :goto_0

    .line 2277661
    :cond_3
    sget-object v2, LX/EmV;->ERROR_DOWNLOAD_MANAGER_FAILURE:LX/EmV;

    if-ne v3, v2, :cond_0

    .line 2277662
    iget-object v2, v0, LX/Fjp;->j:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fjr;

    const-string v3, "downloadmanager_failure"

    invoke-virtual {v2, v3, v9}, LX/Fjr;->a(Ljava/lang/String;LX/0m9;)V

    goto/16 :goto_0
.end method
