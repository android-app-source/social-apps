.class public Lcom/facebook/selfupdate/SelfUpdateFetchService;
.super LX/1ZN;
.source ""


# static fields
.field private static final m:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/Fjj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/12x;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/EmO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/EmQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/1sf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/2V1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0s1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/1sd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0ka;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2277537
    const-class v0, Lcom/facebook/selfupdate/SelfUpdateFetchService;

    sput-object v0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->m:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2277535
    const-string v0, "SelfUpdateFetchService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 2277536
    return-void
.end method

.method private a(Ljava/lang/String;IJLjava/lang/String;Z)J
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 2277516
    invoke-static {}, LX/EmL;->a()LX/EmL;

    move-result-object v0

    sget-object v1, LX/EmM;->APP_UPDATE:LX/EmM;

    .line 2277517
    iput-object v1, v0, LX/EmL;->a:LX/EmM;

    .line 2277518
    move-object v0, v0

    .line 2277519
    iput-object p1, v0, LX/EmL;->b:Ljava/lang/String;

    .line 2277520
    move-object v0, v0

    .line 2277521
    invoke-static {p2}, LX/EmO;->a(I)LX/EmK;

    move-result-object v1

    .line 2277522
    iput-object v1, v0, LX/EmL;->c:LX/EmK;

    .line 2277523
    move-object v0, v0

    .line 2277524
    iput-object p5, v0, LX/EmL;->d:Ljava/lang/String;

    .line 2277525
    move-object v0, v0

    .line 2277526
    iput-wide p3, v0, LX/EmL;->e:J

    .line 2277527
    move-object v0, v0

    .line 2277528
    iput-boolean p6, v0, LX/EmL;->f:Z

    .line 2277529
    move-object v0, v0

    .line 2277530
    iget-object v1, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->j:LX/0s1;

    invoke-interface {v1}, LX/0s1;->d()Ljava/lang/String;

    move-result-object v1

    .line 2277531
    iput-object v1, v0, LX/EmL;->h:Ljava/lang/String;

    .line 2277532
    move-object v0, v0

    .line 2277533
    invoke-virtual {v0}, LX/EmL;->b()LX/EmN;

    move-result-object v0

    .line 2277534
    iget-object v1, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->g:LX/EmQ;

    invoke-virtual {v1, v0}, LX/EmQ;->a(LX/EmN;)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(ZLjava/lang/String;Ljava/lang/String;)J
    .locals 18

    .prologue
    .line 2277492
    new-instance v16, Ljava/util/HashMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashMap;-><init>()V

    .line 2277493
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->i:LX/2V1;

    move/from16 v0, p1

    invoke-virtual {v2, v0}, LX/2V1;->a(Z)LX/Fjz;

    move-result-object v17

    .line 2277494
    if-nez v17, :cond_0

    .line 2277495
    const-string v2, "response_null"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2277496
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->a:LX/Fjj;

    const-string v3, "selfupdate_skip_fql_download"

    move-object/from16 v0, v16

    invoke-virtual {v2, v3, v0}, LX/Fjj;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2277497
    const-wide/32 v2, 0x2932e00

    .line 2277498
    :goto_0
    return-wide v2

    .line 2277499
    :cond_0
    const-string v2, "response_release"

    move-object/from16 v0, v17

    iget v3, v0, LX/Fjz;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2277500
    const-string v2, "response_updatecheckms"

    const-wide/32 v4, 0x2932e00

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5}, LX/Fjz;->a(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2277501
    const-string v2, "update_available"

    invoke-virtual/range {v17 .. v17}, LX/Fjz;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2277502
    const-string v2, "force_download"

    invoke-static/range {p1 .. p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2277503
    const-string v2, "download_pending"

    invoke-direct/range {p0 .. p0}, Lcom/facebook/selfupdate/SelfUpdateFetchService;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2277504
    const-string v2, "update_critical"

    invoke-virtual/range {v17 .. v17}, LX/Fjz;->b()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2277505
    invoke-virtual/range {v17 .. v17}, LX/Fjz;->b()Z

    move-result v2

    move-object/from16 v0, v17

    iget v3, v0, LX/Fjz;->d:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/facebook/selfupdate/SelfUpdateFetchService;->a(ZI)Z

    move-result v2

    .line 2277506
    const-string v3, "download_postponed"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2277507
    invoke-virtual/range {v17 .. v17}, LX/Fjz;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez p1, :cond_1

    invoke-direct/range {p0 .. p0}, Lcom/facebook/selfupdate/SelfUpdateFetchService;->a()Z

    move-result v3

    if-nez v3, :cond_2

    if-nez v2, :cond_2

    .line 2277508
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->k:LX/1sd;

    invoke-virtual {v2}, LX/1sd;->c()Z

    move-result v2

    .line 2277509
    const-string v3, "preload_sdk_present"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2277510
    move-object/from16 v0, v17

    iget-object v4, v0, LX/Fjz;->f:Ljava/lang/String;

    move-object/from16 v0, v17

    iget v5, v0, LX/Fjz;->j:I

    move-object/from16 v0, v17

    iget-wide v6, v0, LX/Fjz;->l:J

    move-object/from16 v3, p0

    move-object/from16 v8, p3

    move/from16 v9, p1

    invoke-direct/range {v3 .. v9}, Lcom/facebook/selfupdate/SelfUpdateFetchService;->a(Ljava/lang/String;IJLjava/lang/String;Z)J

    move-result-wide v12

    .line 2277511
    move-object/from16 v0, v17

    iget v3, v0, LX/Fjz;->d:I

    move-object/from16 v0, v17

    iget-object v4, v0, LX/Fjz;->f:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-object v5, v0, LX/Fjz;->g:Ljava/lang/String;

    invoke-virtual/range {v17 .. v17}, LX/Fjz;->b()Z

    move-result v6

    move-object/from16 v0, v17

    iget-object v7, v0, LX/Fjz;->i:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-object v8, v0, LX/Fjz;->a:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-object v9, v0, LX/Fjz;->k:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-wide v10, v0, LX/Fjz;->l:J

    const-string v15, "fql"

    move-object/from16 v2, p0

    move-object/from16 v14, p2

    invoke-direct/range {v2 .. v15}, Lcom/facebook/selfupdate/SelfUpdateFetchService;->a(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V

    .line 2277512
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->a:LX/Fjj;

    const-string v3, "selfupdate_queue_download_from_fql"

    move-object/from16 v0, v16

    invoke-virtual {v2, v3, v0}, LX/Fjj;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2277513
    const-wide/32 v2, 0x2932e00

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, LX/Fjz;->a(J)J

    move-result-wide v2

    goto/16 :goto_0

    .line 2277514
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->a:LX/Fjj;

    const-string v3, "selfupdate_skip_fql_download"

    move-object/from16 v0, v16

    invoke-virtual {v2, v3, v0}, LX/Fjj;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2277515
    const-wide/32 v2, 0x2932e00

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, LX/Fjz;->a(J)J

    move-result-wide v2

    goto/16 :goto_0
.end method

.method private static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2277487
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 2277488
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2277489
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    .line 2277490
    :catch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2277491
    :cond_0
    const-string v0, ""

    goto :goto_1
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2277484
    const-wide/16 v2, -0x1

    cmp-long v2, p10, v2

    if-eqz v2, :cond_0

    .line 2277485
    iget-object v2, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/Fjg;->d:LX/0Tn;

    invoke-interface {v2, v3, p1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v2

    sget-object v3, LX/Fjg;->g:LX/0Tn;

    move-wide/from16 v0, p10

    invoke-interface {v2, v3, v0, v1}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    sget-object v3, LX/Fjg;->e:LX/0Tn;

    invoke-interface {v2, v3, p2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v3, LX/Fjg;->f:LX/0Tn;

    invoke-interface {v2, v3, p3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v3, LX/Fjg;->i:LX/0Tn;

    invoke-interface {v2, v3, p4}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    sget-object v3, LX/Fjg;->j:LX/0Tn;

    move-object/from16 v0, p12

    invoke-interface {v2, v3, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v3, LX/Fjg;->k:LX/0Tn;

    invoke-interface {v2, v3, p5}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v3, LX/Fjg;->n:LX/0Tn;

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v2

    sget-object v3, LX/Fjg;->o:LX/0Tn;

    invoke-interface {v2, v3, p6}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v3, LX/Fjg;->p:LX/0Tn;

    invoke-interface {v2, v3, p7}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v3, LX/Fjg;->q:LX/0Tn;

    invoke-interface {v2, v3, p8, p9}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    sget-object v3, LX/Fjg;->r:LX/0Tn;

    move-object/from16 v0, p13

    invoke-interface {v2, v3, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 2277486
    :cond_0
    return-void
.end method

.method private a(J)V
    .locals 7

    .prologue
    const-wide/32 v2, 0xf731400

    const-wide/32 v0, 0x493e0

    const/4 v6, 0x0

    .line 2277538
    cmp-long v4, p1, v2

    if-lez v4, :cond_1

    .line 2277539
    :goto_0
    cmp-long v4, v2, v0

    if-gez v4, :cond_0

    .line 2277540
    :goto_1
    iget-object v2, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    add-long/2addr v2, v0

    .line 2277541
    iget-object v4, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    sget-object v5, LX/Fjg;->b:LX/0Tn;

    invoke-interface {v4, v5, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v4

    sget-object v5, LX/Fjg;->c:LX/0Tn;

    invoke-interface {v4, v5, v0, v1}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2277542
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->e:Landroid/content/Context;

    const-class v4, Lcom/facebook/selfupdate/SelfUpdateFetchService;

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2277543
    const-string v1, "force_update"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2277544
    iget-object v1, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->e:Landroid/content/Context;

    invoke-static {v1, v6, v0, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 2277545
    iget-object v1, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->c:LX/12x;

    const/4 v4, 0x1

    .line 2277546
    invoke-virtual {v1, v4, v2, v3, v0}, LX/12x;->c(IJLandroid/app/PendingIntent;)V

    .line 2277547
    return-void

    :cond_0
    move-wide v0, v2

    goto :goto_1

    :cond_1
    move-wide v2, p1

    goto :goto_0
.end method

.method private static a(Lcom/facebook/selfupdate/SelfUpdateFetchService;LX/Fjj;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/12x;LX/0SG;Landroid/content/Context;LX/EmO;LX/EmQ;LX/1sf;LX/2V1;LX/0s1;LX/1sd;LX/0ka;)V
    .locals 0

    .prologue
    .line 2277483
    iput-object p1, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->a:LX/Fjj;

    iput-object p2, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p3, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->c:LX/12x;

    iput-object p4, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->d:LX/0SG;

    iput-object p5, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->e:Landroid/content/Context;

    iput-object p6, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->f:LX/EmO;

    iput-object p7, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->g:LX/EmQ;

    iput-object p8, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->h:LX/1sf;

    iput-object p9, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->i:LX/2V1;

    iput-object p10, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->j:LX/0s1;

    iput-object p11, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->k:LX/1sd;

    iput-object p12, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->l:LX/0ka;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 13

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v12

    move-object v0, p0

    check-cast v0, Lcom/facebook/selfupdate/SelfUpdateFetchService;

    invoke-static {v12}, LX/Fjj;->b(LX/0QB;)LX/Fjj;

    move-result-object v1

    check-cast v1, LX/Fjj;

    invoke-static {v12}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v12}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v3

    check-cast v3, LX/12x;

    invoke-static {v12}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    const-class v5, Landroid/content/Context;

    invoke-interface {v12, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v12}, LX/EmO;->a(LX/0QB;)LX/EmO;

    move-result-object v6

    check-cast v6, LX/EmO;

    invoke-static {v12}, LX/EmQ;->a(LX/0QB;)LX/EmQ;

    move-result-object v7

    check-cast v7, LX/EmQ;

    invoke-static {v12}, LX/1sf;->b(LX/0QB;)LX/1sf;

    move-result-object v8

    check-cast v8, LX/1sf;

    invoke-static {v12}, LX/2V1;->b(LX/0QB;)LX/2V1;

    move-result-object v9

    check-cast v9, LX/2V1;

    invoke-static {v12}, LX/0s0;->a(LX/0QB;)LX/0s0;

    move-result-object v10

    check-cast v10, LX/0s1;

    invoke-static {v12}, LX/1sb;->b(LX/0QB;)LX/1sd;

    move-result-object v11

    check-cast v11, LX/1sd;

    invoke-static {v12}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v12

    check-cast v12, LX/0ka;

    invoke-static/range {v0 .. v12}, Lcom/facebook/selfupdate/SelfUpdateFetchService;->a(Lcom/facebook/selfupdate/SelfUpdateFetchService;LX/Fjj;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/12x;LX/0SG;Landroid/content/Context;LX/EmO;LX/EmQ;LX/1sf;LX/2V1;LX/0s1;LX/1sd;LX/0ka;)V

    return-void
.end method

.method private a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2277481
    iget-object v1, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Fjg;->n:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    .line 2277482
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private a(ZI)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2277475
    if-eqz p1, :cond_1

    .line 2277476
    :cond_0
    :goto_0
    return v0

    .line 2277477
    :cond_1
    iget-object v1, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Fjg;->l:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    .line 2277478
    if-ne v1, p2, :cond_0

    .line 2277479
    iget-object v1, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Fjg;->m:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 2277480
    iget-object v1, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    cmp-long v1, v4, v2

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x0

    const/16 v0, 0x24

    const v1, -0x2cdb5807

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2277462
    if-nez p1, :cond_0

    .line 2277463
    const/16 v1, 0x25

    const v2, 0xbb9cc22

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2277464
    :goto_0
    return-void

    .line 2277465
    :cond_0
    const-string v1, "force_update"

    invoke-virtual {p1, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 2277466
    if-nez v1, :cond_1

    iget-object v2, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->h:LX/1sf;

    invoke-virtual {v2}, LX/1sf;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2277467
    :cond_1
    iget-object v2, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/facebook/selfupdate/SelfUpdateFetchService;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 2277468
    const v3, 0x7f0832d6

    invoke-virtual {p0, v3}, Lcom/facebook/selfupdate/SelfUpdateFetchService;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2277469
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2277470
    iget-object v4, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->a:LX/Fjj;

    const-string v5, "selfupdate_start_fetching_from_fql"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, LX/Fjj;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2277471
    invoke-direct {p0, v1, v2, v3}, Lcom/facebook/selfupdate/SelfUpdateFetchService;->a(ZLjava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    .line 2277472
    iget-object v1, p0, Lcom/facebook/selfupdate/SelfUpdateFetchService;->h:LX/1sf;

    invoke-virtual {v1}, LX/1sf;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2277473
    invoke-direct {p0, v2, v3}, Lcom/facebook/selfupdate/SelfUpdateFetchService;->a(J)V

    .line 2277474
    :cond_2
    const v1, 0x58f47632

    invoke-static {v1, v0}, LX/02F;->d(II)V

    goto :goto_0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0xa661daf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2277457
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 2277458
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/facebook/selfupdate/SelfUpdateFetchService;->setIntentRedelivery(Z)V

    .line 2277459
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 2277460
    invoke-static {p0, p0}, Lcom/facebook/selfupdate/SelfUpdateFetchService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2277461
    const/16 v1, 0x25

    const v2, -0x555b4908

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
