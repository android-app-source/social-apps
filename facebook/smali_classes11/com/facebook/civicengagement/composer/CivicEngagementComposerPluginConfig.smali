.class public final Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/88f;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfigDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfigSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final mComposerHint:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_hint"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2360182
    const-class v0, Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfigDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2360181
    const-class v0, Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfigSerializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2360178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2360179
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;->mComposerHint:Ljava/lang/String;

    .line 2360180
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2360174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2360175
    iput-object p1, p0, Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;->mComposerHint:Ljava/lang/String;

    .line 2360176
    invoke-virtual {p0}, Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;->a()V

    .line 2360177
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;
    .locals 1

    .prologue
    .line 2360173
    new-instance v0, Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;

    invoke-direct {v0, p0}, Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2360171
    invoke-virtual {p0}, Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2360172
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2360170
    const-string v0, "CivicEngagementComposerPluginConfig"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2360169
    iget-object v0, p0, Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;->mComposerHint:Ljava/lang/String;

    return-object v0
.end method
