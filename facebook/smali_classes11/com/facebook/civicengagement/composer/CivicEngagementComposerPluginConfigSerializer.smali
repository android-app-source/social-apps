.class public Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2360204
    const-class v0, Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;

    new-instance v1, Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2360205
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2360206
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2360207
    if-nez p0, :cond_0

    .line 2360208
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2360209
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2360210
    invoke-static {p0, p1, p2}, Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfigSerializer;->b(Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;LX/0nX;LX/0my;)V

    .line 2360211
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2360212
    return-void
.end method

.method private static b(Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2360213
    const-string v0, "composer_hint"

    iget-object v1, p0, Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;->mComposerHint:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2360214
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2360215
    check-cast p1, Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfigSerializer;->a(Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;LX/0nX;LX/0my;)V

    return-void
.end method
