.class public Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;
.super Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->GROUPS_GRID_FRAGMENT:LX/0cQ;
.end annotation


# static fields
.field public static final D:Ljava/lang/String;


# instance fields
.field public A:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private E:LX/GkE;

.field public F:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "LX/GkB;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/GlY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/Gkk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/Gkj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/GkM;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/DPS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/GkD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/DPA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/3mF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2389415
    const-class v0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->D:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2389416
    invoke-direct {p0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;-><init>()V

    .line 2389417
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->F:Ljava/util/HashSet;

    return-void
.end method

.method private static a(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;Ljava/lang/String;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 2389418
    new-instance v0, LX/GkV;

    invoke-direct {v0, p0, p1}, LX/GkV;-><init>(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;Ljava/lang/String;)V

    return-object v0
.end method

.method private static a(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;Lcom/facebook/content/SecureContextHelper;LX/GlY;LX/Gkk;LX/Gkj;LX/0Ot;LX/0Ot;LX/DPS;LX/GkD;LX/17W;LX/0Uh;LX/DPA;LX/1Ck;LX/3mF;Landroid/content/Context;Landroid/content/res/Resources;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/GlY;",
            "LX/Gkk;",
            "LX/Gkj;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/GkM;",
            ">;",
            "LX/DPS;",
            "LX/GkD;",
            "LX/17W;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/groups/groupactions/GroupActionsHelper;",
            "LX/1Ck;",
            "LX/3mF;",
            "Landroid/content/Context;",
            "Landroid/content/res/Resources;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2389419
    iput-object p1, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->m:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->n:LX/GlY;

    iput-object p3, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->o:LX/Gkk;

    iput-object p4, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->p:LX/Gkj;

    iput-object p5, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->q:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->r:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->s:LX/DPS;

    iput-object p8, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->t:LX/GkD;

    iput-object p9, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->u:LX/17W;

    iput-object p10, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->v:LX/0Uh;

    iput-object p11, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->w:LX/DPA;

    iput-object p12, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->x:LX/1Ck;

    iput-object p13, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->y:LX/3mF;

    iput-object p14, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->z:Landroid/content/Context;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->A:Landroid/content/res/Resources;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->B:LX/0Ot;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->C:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 21

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v19

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    invoke-static/range {v19 .. v19}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    const-class v4, LX/GlY;

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/GlY;

    invoke-static/range {v19 .. v19}, LX/Gl6;->a(LX/0QB;)LX/Gl6;

    move-result-object v5

    check-cast v5, LX/Gkk;

    invoke-static/range {v19 .. v19}, LX/Gkj;->a(LX/0QB;)LX/Gkj;

    move-result-object v6

    check-cast v6, LX/Gkj;

    const/16 v7, 0x259

    move-object/from16 v0, v19

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x23a7

    move-object/from16 v0, v19

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static/range {v19 .. v19}, LX/DPS;->a(LX/0QB;)LX/DPS;

    move-result-object v9

    check-cast v9, LX/DPS;

    invoke-static/range {v19 .. v19}, LX/GkD;->a(LX/0QB;)LX/GkD;

    move-result-object v10

    check-cast v10, LX/GkD;

    invoke-static/range {v19 .. v19}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v11

    check-cast v11, LX/17W;

    invoke-static/range {v19 .. v19}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-static/range {v19 .. v19}, LX/DPB;->a(LX/0QB;)LX/DPB;

    move-result-object v13

    check-cast v13, LX/DPA;

    invoke-static/range {v19 .. v19}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v14

    check-cast v14, LX/1Ck;

    invoke-static/range {v19 .. v19}, LX/3mF;->a(LX/0QB;)LX/3mF;

    move-result-object v15

    check-cast v15, LX/3mF;

    const-class v16, Landroid/content/Context;

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/content/Context;

    invoke-static/range {v19 .. v19}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v17

    check-cast v17, Landroid/content/res/Resources;

    const/16 v18, 0x12c4

    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v20, 0x259

    invoke-static/range {v19 .. v20}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    invoke-static/range {v2 .. v19}, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->a(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;Lcom/facebook/content/SecureContextHelper;LX/GlY;LX/Gkk;LX/Gkj;LX/0Ot;LX/0Ot;LX/DPS;LX/GkD;LX/17W;LX/0Uh;LX/DPA;LX/1Ck;LX/3mF;Landroid/content/Context;Landroid/content/res/Resources;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLGroupAdminType;)Z
    .locals 1

    .prologue
    .line 2389420
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-eq v0, p0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->MODERATOR:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-ne v0, p0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;Landroid/content/Context;Ljava/lang/String;Z)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 2389421
    new-instance v0, LX/GkR;

    invoke-direct {v0, p0, p2, p1, p3}, LX/GkR;-><init>(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;Ljava/lang/String;Landroid/content/Context;Z)V

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;I)V
    .locals 3

    .prologue
    .line 2389422
    iget-object v0, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->A:Landroid/content/res/Resources;

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2389423
    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;LX/Gkq;ILcom/facebook/widget/recyclerview/BetterRecyclerView;LX/GkS;)V
    .locals 12

    .prologue
    .line 2389424
    new-instance v11, LX/5OM;

    invoke-virtual {p3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v11, v0}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 2389425
    invoke-virtual {v11}, LX/5OM;->c()LX/5OG;

    move-result-object v6

    .line 2389426
    invoke-virtual {p1}, LX/Gkq;->e()Z

    move-result v0

    if-nez v0, :cond_3

    const v0, 0x7f08372c

    :goto_0
    invoke-virtual {v6, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    .line 2389427
    new-instance v1, LX/Gkc;

    invoke-direct {v1, p0, p1, p2}, LX/Gkc;-><init>(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;LX/Gkq;I)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2389428
    invoke-virtual {p1}, LX/Gkq;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2389429
    const v0, 0x7f08372d

    invoke-virtual {v6, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    .line 2389430
    new-instance v1, LX/Gkd;

    invoke-direct {v1, p0, p3, p2, p1}, LX/Gkd;-><init>(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;Lcom/facebook/widget/recyclerview/BetterRecyclerView;ILX/Gkq;)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2389431
    :cond_0
    invoke-virtual {p1}, LX/Gkq;->o()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    move-result-object v3

    .line 2389432
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->OFF:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    if-eq v3, v0, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    if-eq v3, v0, :cond_1

    invoke-virtual {p1}, LX/Gkq;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2389433
    const v0, 0x7f08372f

    invoke-virtual {v6, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v7

    .line 2389434
    new-instance v0, LX/Gke;

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v4, p4

    move v5, p2

    invoke-direct/range {v0 .. v5}, LX/Gke;-><init>(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;LX/Gkq;Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;LX/GkS;I)V

    invoke-virtual {v7, v0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2389435
    :cond_1
    invoke-virtual {p1}, LX/Gkq;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, LX/Gkq;->e()Z

    move-result v0

    if-nez v0, :cond_4

    const v0, 0x7f083731

    :goto_1
    invoke-virtual {v6, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    .line 2389436
    new-instance v1, LX/Gkf;

    invoke-direct {v1, p0, p1, p2}, LX/Gkf;-><init>(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;LX/Gkq;I)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2389437
    iget-object v0, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->v:LX/0Uh;

    const/16 v1, 0x4f3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, LX/Gkq;->k()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->a(Lcom/facebook/graphql/enums/GraphQLGroupAdminType;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2389438
    invoke-virtual {p1}, LX/Gkq;->n()Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f08306b

    :goto_2
    invoke-virtual {v6, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v2

    .line 2389439
    invoke-virtual {p1}, LX/Gkq;->n()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, LX/Gkq;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->b(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;Ljava/lang/String;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    .line 2389440
    :goto_3
    invoke-virtual {p1}, LX/Gkq;->n()Z

    move-result v1

    if-nez v1, :cond_7

    const/4 v1, 0x1

    .line 2389441
    :goto_4
    new-instance v4, LX/Gkg;

    invoke-direct {v4, p0, v1, v0}, LX/Gkg;-><init>(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;ZLandroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v2, v4}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2389442
    :cond_2
    const v0, 0x7f083732

    invoke-virtual {v6, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    .line 2389443
    new-instance v4, LX/Gki;

    move-object v5, p0

    move-object v6, p3

    move-object v7, p1

    move-object v8, v3

    move-object/from16 v9, p4

    move v10, p2

    invoke-direct/range {v4 .. v10}, LX/Gki;-><init>(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;Lcom/facebook/widget/recyclerview/BetterRecyclerView;LX/Gkq;Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;LX/GkS;I)V

    invoke-virtual {v0, v4}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2389444
    const v0, 0x7f08372b

    invoke-virtual {v11, v0}, LX/5OM;->c(I)V

    .line 2389445
    invoke-virtual {v11, p3}, LX/0ht;->a(Landroid/view/View;)V

    .line 2389446
    return-void

    .line 2389447
    :cond_3
    const v0, 0x7f08372e

    goto/16 :goto_0

    .line 2389448
    :cond_4
    const v0, 0x7f083730

    goto :goto_1

    .line 2389449
    :cond_5
    const v0, 0x7f08306a

    goto :goto_2

    .line 2389450
    :cond_6
    invoke-virtual {p1}, LX/Gkq;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->a(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;Ljava/lang/String;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    goto :goto_3

    .line 2389451
    :cond_7
    const/4 v1, 0x0

    goto :goto_4
.end method

.method public static b(LX/1OM;I)LX/GkS;
    .locals 1

    .prologue
    .line 2389452
    new-instance v0, LX/GkS;

    invoke-direct {v0, p0, p1}, LX/GkS;-><init>(LX/1OM;I)V

    return-object v0
.end method

.method private static b(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;Ljava/lang/String;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 2389453
    new-instance v0, LX/GkY;

    invoke-direct {v0, p0, p1}, LX/GkY;-><init>(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/Glk;LX/Gld;)LX/GlX;
    .locals 8

    .prologue
    .line 2389339
    iget-object v0, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->n:LX/GlY;

    .line 2389340
    new-instance v1, LX/GlX;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/DZB;->a(LX/0QB;)LX/DZB;

    move-result-object v4

    check-cast v4, LX/DZB;

    invoke-static {v0}, LX/GkM;->b(LX/0QB;)LX/GkM;

    move-result-object v6

    check-cast v6, LX/GkM;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v7}, LX/GlX;-><init>(LX/Glk;Lcom/facebook/content/SecureContextHelper;LX/DZB;LX/Gld;LX/GkM;Landroid/content/res/Resources;)V

    .line 2389341
    move-object v0, v1

    .line 2389342
    return-object v0
.end method

.method public final a(LX/Glh;ZI)V
    .locals 4

    .prologue
    .line 2389407
    invoke-virtual {p0, p1}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->a(LX/Glh;)V

    .line 2389408
    if-nez p2, :cond_2

    .line 2389409
    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->g:LX/Glj;

    iget-object v1, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->l:LX/GlX;

    invoke-virtual {v1}, LX/GlX;->d()LX/Gkm;

    move-result-object v1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2389410
    iget-object p0, v0, LX/Glj;->c:LX/0Zb;

    const-string p1, "groups_grid_loaded"

    invoke-interface {p0, p1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object p0

    .line 2389411
    invoke-virtual {p0}, LX/0oG;->a()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 2389412
    const-string p1, "groups_grid"

    invoke-virtual {p0, p1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object p0

    iget-object p1, v0, LX/Glj;->d:LX/0kv;

    iget-object p2, v0, LX/Glj;->b:Landroid/content/Context;

    invoke-virtual {p1, p2}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p1, "loaded_items"

    invoke-virtual {p0, p1, p3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object p0

    const-string p1, "ordering"

    invoke-virtual {v1}, LX/Gkm;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p1, "is_first_load"

    iget-boolean p2, v0, LX/Glj;->a:Z

    if-nez p2, :cond_0

    move v2, v3

    :cond_0
    invoke-virtual {p0, p1, v2}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 2389413
    :cond_1
    iput-boolean v3, v0, LX/Glj;->a:Z

    .line 2389414
    :cond_2
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2389334
    invoke-super {p0, p1}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->a(Landroid/os/Bundle;)V

    .line 2389335
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2389336
    new-instance v0, LX/GkZ;

    invoke-direct {v0, p0}, LX/GkZ;-><init>(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->E:LX/GkE;

    .line 2389337
    iget-object v0, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->o:LX/Gkk;

    sget-object v1, LX/Gkn;->FAVORITES_SECTION:LX/Gkn;

    sget-object v2, LX/Gkn;->RECENTLY_JOINED_SECTION:LX/Gkn;

    sget-object v3, LX/Gkn;->FILTERED_GROUPS_SECTION:LX/Gkn;

    sget-object v4, LX/Gkn;->HIDDEN_GROUPS_SECTION:LX/Gkn;

    invoke-static {v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/Gkk;->a(LX/0Px;)V

    .line 2389338
    return-void
.end method

.method public final b()LX/1OZ;
    .locals 1

    .prologue
    .line 2389343
    new-instance v0, LX/Gka;

    invoke-direct {v0, p0}, LX/Gka;-><init>(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;)V

    return-object v0
.end method

.method public final c()LX/1Oa;
    .locals 1

    .prologue
    .line 2389344
    new-instance v0, LX/Gkb;

    invoke-direct {v0, p0}, LX/Gkb;-><init>(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;)V

    return-object v0
.end method

.method public final d()LX/0fu;
    .locals 1

    .prologue
    .line 2389345
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()LX/Gkk;
    .locals 1

    .prologue
    .line 2389346
    iget-object v0, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->o:LX/Gkk;

    return-object v0
.end method

.method public final k()LX/Gkj;
    .locals 1

    .prologue
    .line 2389347
    iget-object v0, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->p:LX/Gkj;

    return-object v0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x76d5c0a7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2389348
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->F:Ljava/util/HashSet;

    .line 2389349
    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->o:LX/Gkk;

    invoke-interface {v1}, LX/Gkk;->a()V

    .line 2389350
    invoke-super {p0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->onDestroy()V

    .line 2389351
    const/16 v1, 0x2b

    const v2, -0x565e9d5f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x21affa9f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2389352
    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->t:LX/GkD;

    iget-object v2, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->E:LX/GkE;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2389353
    invoke-super {p0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->onDestroyView()V

    .line 2389354
    const/16 v1, 0x2b

    const v2, -0x1ec95ad7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xce6b952

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2389355
    invoke-super {p0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->onResume()V

    .line 2389356
    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->F:Ljava/util/HashSet;

    if-eqz v1, :cond_1

    .line 2389357
    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->F:Ljava/util/HashSet;

    sget-object v2, LX/GkB;->STATUS_CHANGE:LX/GkB;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2389358
    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->o:LX/Gkk;

    check-cast v1, LX/Gl6;

    invoke-virtual {v1}, LX/Gl6;->b()V

    .line 2389359
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->F:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 2389360
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x2c80e494

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2389361
    :cond_2
    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->F:Ljava/util/HashSet;

    sget-object v2, LX/GkB;->REORDER:LX/GkB;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2389362
    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->o:LX/Gkk;

    check-cast v1, LX/Gl6;

    sget-object v2, LX/Gkn;->FAVORITES_SECTION:LX/Gkn;

    invoke-virtual {p0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->n()LX/GkK;

    move-result-object v4

    .line 2389363
    iget-object v5, v1, LX/Gl6;->g:Ljava/util/HashMap;

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Gkp;

    .line 2389364
    if-eqz v5, :cond_3

    instance-of v6, v5, LX/Gkp;

    if-eqz v6, :cond_3

    .line 2389365
    check-cast v5, LX/Gkp;

    .line 2389366
    invoke-virtual {v5}, LX/Gkp;->b()I

    move-result v6

    .line 2389367
    invoke-virtual {v5}, LX/Gkp;->j()V

    .line 2389368
    invoke-static {v1}, LX/Gl6;->g(LX/Gl6;)V

    .line 2389369
    invoke-static {v1, v5, v6, v4}, LX/Gl6;->a(LX/Gl6;LX/Gkp;ILX/GkK;)Z

    .line 2389370
    :cond_3
    goto :goto_0
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x639d83fc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2389371
    invoke-super {p0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->onStart()V

    .line 2389372
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2389373
    const-string v2, "doNotSetTitleBar"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2389374
    :cond_0
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x78cdcab6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2389375
    :cond_1
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2389376
    if-eqz v1, :cond_0

    .line 2389377
    const v2, 0x7f08371f

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2389378
    invoke-super {p0, p1, p2}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2389379
    invoke-virtual {p0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->l()V

    .line 2389380
    check-cast p1, Landroid/view/ViewGroup;

    const/4 v2, 0x1

    .line 2389381
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2389382
    const v3, 0x7f030866

    move v1, v3

    .line 2389383
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2389384
    const v1, 0x7f0d15df

    move v0, v1

    .line 2389385
    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2389386
    iput-boolean v2, v0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 2389387
    iget-object v1, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->j:LX/Glk;

    iget-object v2, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->s:LX/Gld;

    invoke-virtual {p0, v1, v2}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->a(LX/Glk;LX/Gld;)LX/GlX;

    move-result-object v1

    .line 2389388
    iput-object v1, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->l:LX/GlX;

    .line 2389389
    invoke-virtual {p0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->b()LX/1OZ;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setOnItemClickListener(LX/1OZ;)V

    .line 2389390
    invoke-virtual {p0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->c()LX/1Oa;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setOnItemLongClickListener(LX/1Oa;)V

    .line 2389391
    invoke-virtual {p0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->d()LX/0fu;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2389392
    invoke-virtual {p0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->d()LX/0fu;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->a(LX/0fu;)V

    .line 2389393
    :cond_0
    iput-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->h:Landroid/support/v7/widget/RecyclerView;

    .line 2389394
    invoke-static {p0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->q(Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;)LX/3wu;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2389395
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2389396
    iget-object v1, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->j:LX/Glk;

    .line 2389397
    iget v2, v1, LX/Glk;->d:I

    move v1, v2

    .line 2389398
    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->j:LX/Glk;

    .line 2389399
    iget p2, v3, LX/Glk;->d:I

    move v3, p2

    .line 2389400
    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getPaddingBottom()I

    move-result p2

    invoke-virtual {v0, v1, v2, v3, p2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setPadding(IIII)V

    .line 2389401
    new-instance v1, LX/Gle;

    invoke-direct {v1, p0}, LX/Gle;-><init>(Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2389402
    invoke-virtual {p0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->o()V

    .line 2389403
    sget-object v0, LX/Glh;->LOADING:LX/Glh;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->i:LX/Glh;

    .line 2389404
    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->i:LX/Glh;

    invoke-virtual {p0, v0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->a(LX/Glh;)V

    .line 2389405
    iget-object v0, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->t:LX/GkD;

    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->E:LX/GkE;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2389406
    return-void
.end method
