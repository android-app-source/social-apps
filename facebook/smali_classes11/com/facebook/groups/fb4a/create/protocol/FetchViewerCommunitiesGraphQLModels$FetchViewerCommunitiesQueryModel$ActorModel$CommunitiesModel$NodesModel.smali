.class public final Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x63a7e14
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2198200
    const-class v0, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2198199
    const-class v0, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2198197
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2198198
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2198194
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2198195
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2198196
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V
    .locals 4

    .prologue
    .line 2198187
    iput-object p1, p0, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 2198188
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2198189
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2198190
    if-eqz v0, :cond_0

    .line 2198191
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x2

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2198192
    :cond_0
    return-void

    .line 2198193
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2198181
    iput-object p1, p0, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->f:Ljava/lang/String;

    .line 2198182
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2198183
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2198184
    if-eqz v0, :cond_0

    .line 2198185
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 2198186
    :cond_0
    return-void
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2198179
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 2198180
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2198169
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2198170
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2198171
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2198172
    invoke-direct {p0}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->l()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 2198173
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2198174
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2198175
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2198176
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2198177
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2198178
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2198201
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2198202
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2198203
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2198168
    new-instance v0, LX/F5C;

    invoke-direct {v0, p1}, LX/F5C;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2198167
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2198157
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2198158
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2198159
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2198160
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 2198161
    :goto_0
    return-void

    .line 2198162
    :cond_0
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2198163
    invoke-direct {p0}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->l()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2198164
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2198165
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2198166
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2198152
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2198153
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->a(Ljava/lang/String;)V

    .line 2198154
    :cond_0
    :goto_0
    return-void

    .line 2198155
    :cond_1
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2198156
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-direct {p0, p2}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2198149
    new-instance v0, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;-><init>()V

    .line 2198150
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2198151
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2198148
    const v0, 0x11213806

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2198143
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2198146
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->e:Ljava/lang/String;

    .line 2198147
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2198144
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->f:Ljava/lang/String;

    .line 2198145
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method
