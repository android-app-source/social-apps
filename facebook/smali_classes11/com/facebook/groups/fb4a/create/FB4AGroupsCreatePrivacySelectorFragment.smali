.class public Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Ljava/util/Observer;


# instance fields
.field public a:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/F53;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

.field private d:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

.field private e:Lcom/facebook/fbui/widget/contentview/CheckedContentView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2197822
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/fbui/widget/contentview/CheckedContentView;)V
    .locals 3

    .prologue
    .line 2197812
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2197813
    invoke-virtual {p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->getTitleText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 2197814
    invoke-static {v1}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->a(Ljava/lang/StringBuilder;)V

    .line 2197815
    const-string v0, " "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->getSubtitleText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 2197816
    invoke-static {v1}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->a(Ljava/lang/StringBuilder;)V

    .line 2197817
    const-string v0, " "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f082f75

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2197818
    invoke-static {v1}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->a(Ljava/lang/StringBuilder;)V

    .line 2197819
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2197820
    return-void

    .line 2197821
    :cond_0
    const v0, 0x7f082f76

    goto :goto_0
.end method

.method private static a(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;Ljava/lang/Boolean;LX/F53;)V
    .locals 0

    .prologue
    .line 2197811
    iput-object p1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->a:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->b:LX/F53;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;

    invoke-static {v1}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-static {v1}, LX/F53;->a(LX/0QB;)LX/F53;

    move-result-object v1

    check-cast v1, LX/F53;

    invoke-static {p0, v0, v1}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->a(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;Ljava/lang/Boolean;LX/F53;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2197803
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->b:LX/F53;

    .line 2197804
    iget-object v1, v0, LX/F53;->d:Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;

    move-object v0, v1

    .line 2197805
    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 2197806
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->e:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082f6f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2197807
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->e:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const v1, 0x7f082f6b

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(I)V

    .line 2197808
    :goto_0
    return-void

    .line 2197809
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->e:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082f70

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2197810
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->e:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const v1, 0x7f082f6c

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    const/16 v1, 0x2e

    .line 2197771
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    if-eq v0, v1, :cond_0

    .line 2197772
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2197773
    :cond_0
    return-void
.end method

.method private b()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2197802
    new-instance v0, LX/F4v;

    invoke-direct {v0, p0}, LX/F4v;-><init>(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;)V

    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 2197798
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->c:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-direct {p0, v0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->a(Lcom/facebook/fbui/widget/contentview/CheckedContentView;)V

    .line 2197799
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->e:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-direct {p0, v0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->a(Lcom/facebook/fbui/widget/contentview/CheckedContentView;)V

    .line 2197800
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->d:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-direct {p0, v0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->a(Lcom/facebook/fbui/widget/contentview/CheckedContentView;)V

    .line 2197801
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2197795
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2197796
    const-class v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;

    invoke-static {v0, p0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2197797
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x7597490d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2197794
    const v1, 0x7f0305ff

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x67f5ae9b

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2197782
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2197783
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->b:LX/F53;

    invoke-virtual {v0, p0}, LX/F53;->addObserver(Ljava/util/Observer;)V

    .line 2197784
    const v0, 0x7f0d1077

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->c:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 2197785
    const v0, 0x7f0d1076

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->e:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 2197786
    const v0, 0x7f0d1078

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->d:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 2197787
    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->e:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f082f6c

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(I)V

    .line 2197788
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->c:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-direct {p0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->b()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2197789
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->e:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-direct {p0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->b()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2197790
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->d:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-direct {p0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->b()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2197791
    invoke-direct {p0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->c()V

    .line 2197792
    return-void

    .line 2197793
    :cond_0
    const v0, 0x7f082f6b

    goto :goto_0
.end method

.method public final update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2197774
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->b:LX/F53;

    invoke-virtual {v0}, LX/F53;->k()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->a(Ljava/lang/String;)V

    .line 2197775
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->b:LX/F53;

    .line 2197776
    iget-object v1, v0, LX/F53;->h:Ljava/lang/String;

    move-object v0, v1

    .line 2197777
    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->c:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const-string v2, "CLOSED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 2197778
    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->e:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const-string v2, "OPEN"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 2197779
    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->d:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const-string v2, "SECRET"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 2197780
    invoke-direct {p0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->c()V

    .line 2197781
    return-void
.end method
