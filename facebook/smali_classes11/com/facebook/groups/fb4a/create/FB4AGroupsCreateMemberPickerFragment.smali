.class public Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateMemberPickerFragment;
.super Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;
.source ""


# instance fields
.field public u:LX/F4q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/1g8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/F53;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/DVw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2197729
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 12

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateMemberPickerFragment;

    new-instance v4, LX/F4q;

    invoke-static {v3}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v5

    check-cast v5, Landroid/view/inputmethod/InputMethodManager;

    const-class v6, Landroid/content/Context;

    invoke-interface {v3, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    const-class v7, LX/8vM;

    invoke-interface {v3, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/8vM;

    const-class v8, LX/8tF;

    invoke-interface {v3, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/8tF;

    const-class v9, LX/F4W;

    invoke-interface {v3, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/F4W;

    invoke-static {v3}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    invoke-static {v3}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-direct/range {v4 .. v11}, LX/F4q;-><init>(Landroid/view/inputmethod/InputMethodManager;Landroid/content/Context;LX/8vM;LX/8tF;LX/F4W;Ljava/lang/Boolean;LX/0ad;)V

    move-object v0, v4

    check-cast v0, LX/F4q;

    invoke-static {v3}, LX/1g8;->a(LX/0QB;)LX/1g8;

    move-result-object v1

    check-cast v1, LX/1g8;

    invoke-static {v3}, LX/F53;->a(LX/0QB;)LX/F53;

    move-result-object v2

    check-cast v2, LX/F53;

    invoke-static {v3}, LX/DVw;->a(LX/0QB;)LX/DVw;

    move-result-object v3

    check-cast v3, LX/DVw;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateMemberPickerFragment;->u:LX/F4q;

    iput-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateMemberPickerFragment;->v:LX/1g8;

    iput-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateMemberPickerFragment;->w:LX/F53;

    iput-object v3, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateMemberPickerFragment;->x:LX/DVw;

    return-void
.end method


# virtual methods
.method public final D()V
    .locals 2

    .prologue
    .line 2197723
    invoke-super {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->D()V

    .line 2197724
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2197725
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateMemberPickerFragment;->x:LX/DVw;

    invoke-virtual {v0, p0}, LX/DVw;->b(Landroid/support/v4/app/Fragment;)V

    .line 2197726
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateMemberPickerFragment;->w:LX/F53;

    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    .line 2197727
    iput-object v1, v0, LX/F53;->m:Ljava/util/List;

    .line 2197728
    return-void
.end method

.method public final a(Lcom/facebook/user/model/User;Ljava/lang/String;)Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;
    .locals 2

    .prologue
    .line 2197718
    new-instance v0, Lcom/facebook/groups/memberpicker/MemberPickerToken;

    invoke-direct {v0, p1}, Lcom/facebook/groups/memberpicker/MemberPickerToken;-><init>(Lcom/facebook/user/model/User;)V

    .line 2197719
    iget-object v1, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 2197720
    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Ljava/lang/String;)Z

    move-result v1

    .line 2197721
    iput-boolean v1, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->i:Z

    .line 2197722
    return-object v0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 2197704
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(I)V

    .line 2197705
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    invoke-virtual {v0, p1}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 2197706
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2197707
    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {p0, v0, v2}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2197708
    iget-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateMemberPickerFragment;->v:LX/1g8;

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 2197709
    :goto_0
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "groups_member_picker_action"

    invoke-direct {v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "add_member"

    .line 2197710
    iput-object v3, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2197711
    move-object v1, v1

    .line 2197712
    const-string v3, "position"

    invoke-virtual {v1, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v3, "item_type"

    const-string p0, "profile"

    invoke-virtual {v1, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p0, "selection_source"

    if-eqz v0, :cond_2

    const-string v1, "search"

    :goto_1
    invoke-virtual {v3, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v3, "source"

    const-string p0, "create"

    invoke-virtual {v1, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2197713
    iget-object v3, v2, LX/1g8;->a:LX/0Zb;

    invoke-interface {v3, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2197714
    invoke-static {v2, v1}, LX/1g8;->a(LX/1g8;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2197715
    :cond_0
    return-void

    .line 2197716
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2197717
    :cond_2
    const-string v1, "alphabetical"

    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2197701
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Landroid/os/Bundle;)V

    .line 2197702
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateMemberPickerFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2197703
    return-void
.end method

.method public final b()LX/8tB;
    .locals 1

    .prologue
    .line 2197700
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateMemberPickerFragment;->u:LX/F4q;

    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x20fac7f1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2197698
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2197699
    const/16 v1, 0x2b

    const v2, 0x52b280ab

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2197685
    const/16 v0, 0x457f

    if-eq p1, v0, :cond_0

    .line 2197686
    :goto_0
    return-void

    .line 2197687
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateMemberPickerFragment;->x:LX/DVw;

    invoke-virtual {v0, p1, p2, p3}, LX/DVw;->a(IILandroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 2197688
    invoke-static {v0, p0}, LX/DVw;->a(Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 2197689
    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateMemberPickerFragment;->w:LX/F53;

    .line 2197690
    iput-object v0, v1, LX/F53;->i:Ljava/lang/String;

    .line 2197691
    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x1ac264a7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2197694
    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateMemberPickerFragment;->w:LX/F53;

    .line 2197695
    iget-object v2, v1, LX/F53;->n:Ljava/lang/String;

    move-object v1, v2

    .line 2197696
    invoke-static {v1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->d(Ljava/lang/String;)LX/0Rf;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateMemberPickerFragment;->p:LX/0Rf;

    .line 2197697
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x781d56ab

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2197692
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateMemberPickerFragment;->x:LX/DVw;

    invoke-virtual {v0, p0}, LX/DVw;->a(Landroid/support/v4/app/Fragment;)V

    .line 2197693
    return-void
.end method
