.class public Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements Ljava/util/Observer;


# static fields
.field public static final a:Ljava/lang/Class;

.field public static final r:[Ljava/lang/String;

.field public static final s:[Ljava/lang/String;

.field private static final t:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:Lcom/facebook/ui/media/attachments/MediaResource;

.field public B:Lcom/facebook/groups/create/GroupCreationProgressDialogFragment;

.field public C:Z

.field public D:Ljava/lang/String;

.field public E:Ljava/lang/String;

.field public F:Ljava/lang/String;

.field public G:Ljava/lang/String;

.field public H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field public J:Z

.field public K:LX/0i5;

.field private L:LX/F4z;

.field public final M:LX/F4e;

.field public b:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/2Ib;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/DKa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/1g8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/DKO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/F53;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/F51;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/F4u;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Lcom/facebook/fig/textinput/FigEditText;

.field public v:Landroid/widget/ImageView;

.field public w:Lcom/facebook/drawee/view/DraweeView;

.field private x:Lcom/facebook/resources/ui/FbTextView;

.field public y:Landroid/net/Uri;

.field public z:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2197322
    const-class v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    sput-object v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->a:Ljava/lang/Class;

    .line 2197323
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.CAMERA"

    aput-object v1, v0, v3

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->r:[Ljava/lang/String;

    .line 2197324
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->s:[Ljava/lang/String;

    .line 2197325
    const-class v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    const-string v1, "group_creation"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->t:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2197319
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2197320
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->J:Z

    .line 2197321
    new-instance v0, LX/F4e;

    invoke-direct {v0, p0}, LX/F4e;-><init>(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->M:LX/F4e;

    return-void
.end method

.method private a(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2197618
    const-string v0, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2197619
    :cond_0
    :goto_0
    return-object p1

    .line 2197620
    :cond_1
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 2197621
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2197622
    if-eqz v0, :cond_2

    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_4

    .line 2197623
    :cond_2
    sget-object v1, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->a:Ljava/lang/Class;

    const-string v2, "Cannot resolve real pic uri in group creation"

    invoke-static {v1, v2}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2197624
    if-eqz v0, :cond_3

    .line 2197625
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object p1, v6

    goto :goto_0

    .line 2197626
    :cond_4
    :try_start_2
    const-string v1, "_data"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 2197627
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2197628
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2197629
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "file://"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object p1

    .line 2197630
    if-eqz v0, :cond_0

    .line 2197631
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2197632
    :catch_0
    move-object v0, v6

    :goto_1
    if-eqz v0, :cond_5

    .line 2197633
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_5
    move-object p1, v6

    goto :goto_0

    .line 2197634
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v6, :cond_6

    .line 2197635
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 2197636
    :catchall_1
    move-exception v1

    move-object v6, v0

    move-object v0, v1

    goto :goto_2

    :catch_1
    goto :goto_1
.end method

.method private static a(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;LX/0kL;Landroid/view/inputmethod/InputMethodManager;LX/2Ib;LX/1Ad;Lcom/facebook/content/SecureContextHelper;LX/DKa;LX/1Ck;LX/1g8;LX/0i4;Ljava/lang/Boolean;LX/DKO;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;LX/0iA;LX/F53;LX/F51;LX/F4u;)V
    .locals 1

    .prologue
    .line 2197617
    iput-object p1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->b:LX/0kL;

    iput-object p2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->c:Landroid/view/inputmethod/InputMethodManager;

    iput-object p3, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->d:LX/2Ib;

    iput-object p4, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->e:LX/1Ad;

    iput-object p5, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->f:Lcom/facebook/content/SecureContextHelper;

    iput-object p6, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->g:LX/DKa;

    iput-object p7, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->h:LX/1Ck;

    iput-object p8, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->i:LX/1g8;

    iput-object p9, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->j:LX/0i4;

    iput-object p10, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->k:Ljava/lang/Boolean;

    iput-object p11, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->l:LX/DKO;

    iput-object p12, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->m:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    iput-object p13, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->n:LX/0iA;

    iput-object p14, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->p:LX/F51;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->q:LX/F4u;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 18

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v17

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    invoke-static/range {v17 .. v17}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v2

    check-cast v2, LX/0kL;

    invoke-static/range {v17 .. v17}, LX/10d;->a(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    invoke-static/range {v17 .. v17}, LX/2Ib;->a(LX/0QB;)LX/2Ib;

    move-result-object v4

    check-cast v4, LX/2Ib;

    invoke-static/range {v17 .. v17}, LX/1Ad;->a(LX/0QB;)LX/1Ad;

    move-result-object v5

    check-cast v5, LX/1Ad;

    invoke-static/range {v17 .. v17}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v17 .. v17}, LX/DKa;->a(LX/0QB;)LX/DKa;

    move-result-object v7

    check-cast v7, LX/DKa;

    invoke-static/range {v17 .. v17}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    invoke-static/range {v17 .. v17}, LX/1g8;->a(LX/0QB;)LX/1g8;

    move-result-object v9

    check-cast v9, LX/1g8;

    const-class v10, LX/0i4;

    move-object/from16 v0, v17

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/0i4;

    invoke-static/range {v17 .. v17}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v11

    check-cast v11, Ljava/lang/Boolean;

    invoke-static/range {v17 .. v17}, LX/DKO;->a(LX/0QB;)LX/DKO;

    move-result-object v12

    check-cast v12, LX/DKO;

    invoke-static/range {v17 .. v17}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(LX/0QB;)Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    move-result-object v13

    check-cast v13, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    invoke-static/range {v17 .. v17}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v14

    check-cast v14, LX/0iA;

    invoke-static/range {v17 .. v17}, LX/F53;->a(LX/0QB;)LX/F53;

    move-result-object v15

    check-cast v15, LX/F53;

    invoke-static/range {v17 .. v17}, LX/F51;->a(LX/0QB;)LX/F51;

    move-result-object v16

    check-cast v16, LX/F51;

    invoke-static/range {v17 .. v17}, LX/F4u;->a(LX/0QB;)LX/F4u;

    move-result-object v17

    check-cast v17, LX/F4u;

    invoke-static/range {v1 .. v17}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->a(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;LX/0kL;Landroid/view/inputmethod/InputMethodManager;LX/2Ib;LX/1Ad;Lcom/facebook/content/SecureContextHelper;LX/DKa;LX/1Ck;LX/1g8;LX/0i4;Ljava/lang/Boolean;LX/DKO;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;LX/0iA;LX/F53;LX/F51;LX/F4u;)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 2197603
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    invoke-virtual {v0}, LX/F53;->k()Ljava/lang/String;

    move-result-object v1

    .line 2197604
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->x:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setEnabled(Z)V

    .line 2197605
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197606
    iget-object v2, v0, LX/F53;->r:Ljava/lang/String;

    move-object v0, v2

    .line 2197607
    if-eqz v0, :cond_0

    .line 2197608
    const v0, 0x7f0d1070

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2197609
    const v2, 0x7f083184

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2197610
    :cond_0
    iget-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->x:Lcom/facebook/resources/ui/FbTextView;

    if-nez v1, :cond_2

    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197611
    iget-object v1, v0, LX/F53;->p:Ljava/lang/String;

    move-object v0, v1

    .line 2197612
    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f083185

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2197613
    return-void

    .line 2197614
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197615
    iget-object v1, v0, LX/F53;->p:Ljava/lang/String;

    move-object v0, v1

    .line 2197616
    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public static b$redex0(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;)V
    .locals 3

    .prologue
    .line 2197599
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->c:Landroid/view/inputmethod/InputMethodManager;

    .line 2197600
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2197601
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2197602
    return-void
.end method

.method private n()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 2197589
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197590
    iget-boolean v3, v0, LX/F53;->s:Z

    move v0, v3

    .line 2197591
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->u:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0}, Lcom/facebook/fig/textinput/FigEditText;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2197592
    :goto_0
    const v3, 0x7f0d1072

    invoke-virtual {p0, v3}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2197593
    const v2, 0x7f0d106a

    invoke-virtual {p0, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2197594
    const v2, 0x7f0d106f

    invoke-virtual {p0, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2197595
    const v0, 0x7f0d1073

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2197596
    const v0, 0x7f0d1074

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2197597
    return-void

    :cond_0
    move v0, v2

    .line 2197598
    goto :goto_0
.end method

.method private o()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2197585
    const v0, 0x7f0d1072

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2197586
    const v0, 0x7f0d106a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2197587
    const v0, 0x7f0d106f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2197588
    return-void
.end method

.method public static t(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;)V
    .locals 1

    .prologue
    .line 2197581
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->B:Lcom/facebook/groups/create/GroupCreationProgressDialogFragment;

    if-eqz v0, :cond_0

    .line 2197582
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->B:Lcom/facebook/groups/create/GroupCreationProgressDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2197583
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->B:Lcom/facebook/groups/create/GroupCreationProgressDialogFragment;

    .line 2197584
    :cond_0
    return-void
.end method

.method private u()V
    .locals 4

    .prologue
    .line 2197567
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2197568
    if-eqz v0, :cond_0

    .line 2197569
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2197570
    const v1, 0x7f082f59

    invoke-interface {v0, v1}, LX/1ZF;->x_(I)V

    .line 2197571
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082f68

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2197572
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2197573
    move-object v1, v1

    .line 2197574
    const/4 v2, -0x2

    .line 2197575
    iput v2, v1, LX/108;->h:I

    .line 2197576
    move-object v1, v1

    .line 2197577
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2197578
    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2197579
    new-instance v1, LX/F4l;

    invoke-direct {v1, p0}, LX/F4l;-><init>(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;)V

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    .line 2197580
    :cond_0
    return-void
.end method

.method private w()V
    .locals 4

    .prologue
    .line 2197549
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2197550
    if-eqz v0, :cond_0

    .line 2197551
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2197552
    const v1, 0x7f082f59

    invoke-interface {v0, v1}, LX/1ZF;->x_(I)V

    .line 2197553
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082f68

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2197554
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2197555
    move-object v1, v1

    .line 2197556
    const/4 v2, -0x2

    .line 2197557
    iput v2, v1, LX/108;->h:I

    .line 2197558
    move-object v1, v1

    .line 2197559
    iget-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197560
    iget-boolean v3, v2, LX/F53;->k:Z

    move v2, v3

    .line 2197561
    iput-boolean v2, v1, LX/108;->d:Z

    .line 2197562
    move-object v1, v1

    .line 2197563
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2197564
    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2197565
    new-instance v1, LX/F4X;

    invoke-direct {v1, p0}, LX/F4X;-><init>(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;)V

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    .line 2197566
    :cond_0
    return-void
.end method

.method private y()V
    .locals 4

    .prologue
    .line 2197539
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 2197540
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1bX;->a(Z)LX/1bX;

    move-result-object v0

    new-instance v1, LX/1o9;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-direct {v1, v2, v3}, LX/1o9;-><init>(II)V

    .line 2197541
    iput-object v1, v0, LX/1bX;->c:LX/1o9;

    .line 2197542
    move-object v0, v0

    .line 2197543
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    .line 2197544
    iget-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->w:Lcom/facebook/drawee/view/DraweeView;

    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->e:LX/1Ad;

    iget-object v3, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->w:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v3}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v3, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->t:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2197545
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->v:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2197546
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->w:Lcom/facebook/drawee/view/DraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setVisibility(I)V

    .line 2197547
    :goto_0
    return-void

    .line 2197548
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->b:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082f61

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0
.end method


# virtual methods
.method public final S_()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2197508
    iget-boolean v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->C:Z

    if-eqz v2, :cond_0

    .line 2197509
    :goto_0
    return v0

    .line 2197510
    :cond_0
    iget-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197511
    iget-object v3, v2, LX/F53;->f:LX/F52;

    move-object v2, v3

    .line 2197512
    sget-object v3, LX/F52;->Privacy:LX/F52;

    if-ne v2, v3, :cond_1

    .line 2197513
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    sget-object v2, LX/F52;->Invite:LX/F52;

    invoke-virtual {v0, v2}, LX/F53;->a(LX/F52;)V

    .line 2197514
    move v0, v1

    .line 2197515
    goto :goto_0

    .line 2197516
    :cond_1
    iget-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197517
    iget-object v3, v2, LX/F53;->f:LX/F52;

    move-object v2, v3

    .line 2197518
    sget-object v3, LX/F52;->Community:LX/F52;

    if-ne v2, v3, :cond_2

    .line 2197519
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    sget-object v2, LX/F52;->Invite:LX/F52;

    invoke-virtual {v0, v2}, LX/F53;->a(LX/F52;)V

    .line 2197520
    move v0, v1

    .line 2197521
    goto :goto_0

    .line 2197522
    :cond_2
    iget-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197523
    iget-object v3, v2, LX/F53;->f:LX/F52;

    move-object v2, v3

    .line 2197524
    sget-object v3, LX/F52;->Invite:LX/F52;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    invoke-virtual {v2}, LX/F53;->f()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2197525
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    sget-object v2, LX/F52;->Type:LX/F52;

    invoke-virtual {v0, v2}, LX/F53;->a(LX/F52;)V

    move v0, v1

    .line 2197526
    goto :goto_0

    .line 2197527
    :cond_3
    iget-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->u:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v2}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 2197528
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    invoke-virtual {v2}, LX/F53;->d()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->y:Landroid/net/Uri;

    if-eqz v2, :cond_5

    .line 2197529
    :cond_4
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2197530
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/0ju;->a(Z)LX/0ju;

    .line 2197531
    const v2, 0x7f082f71

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 2197532
    const v2, 0x7f082f72

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2197533
    const v2, 0x7f082f73

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/F4i;

    invoke-direct {v3, p0}, LX/F4i;-><init>(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;)V

    invoke-virtual {v0, v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2197534
    const v2, 0x7f082f74

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/F4j;

    invoke-direct {v3, p0}, LX/F4j;-><init>(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;)V

    invoke-virtual {v0, v2, v3}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2197535
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2197536
    move v0, v1

    .line 2197537
    goto/16 :goto_0

    .line 2197538
    :cond_5
    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->i:LX/1g8;

    invoke-virtual {v1}, LX/1g8;->b()V

    goto/16 :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2197505
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2197506
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2197507
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2197476
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2197477
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2197478
    packed-switch p1, :pswitch_data_0

    .line 2197479
    :cond_0
    :goto_0
    return-void

    .line 2197480
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->z:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->y:Landroid/net/Uri;

    .line 2197481
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->z:Landroid/net/Uri;

    .line 2197482
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->y:Landroid/net/Uri;

    .line 2197483
    iput-object v1, v0, LX/5zn;->b:Landroid/net/Uri;

    .line 2197484
    move-object v0, v0

    .line 2197485
    sget-object v1, LX/2MK;->PHOTO:LX/2MK;

    .line 2197486
    iput-object v1, v0, LX/5zn;->c:LX/2MK;

    .line 2197487
    move-object v0, v0

    .line 2197488
    sget-object v1, LX/5zj;->CAMERA:LX/5zj;

    .line 2197489
    iput-object v1, v0, LX/5zn;->d:LX/5zj;

    .line 2197490
    move-object v0, v0

    .line 2197491
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2197492
    invoke-direct {p0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->y()V

    goto :goto_0

    .line 2197493
    :pswitch_1
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->y:Landroid/net/Uri;

    .line 2197494
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->y:Landroid/net/Uri;

    .line 2197495
    iput-object v1, v0, LX/5zn;->b:Landroid/net/Uri;

    .line 2197496
    move-object v0, v0

    .line 2197497
    sget-object v1, LX/2MK;->PHOTO:LX/2MK;

    .line 2197498
    iput-object v1, v0, LX/5zn;->c:LX/2MK;

    .line 2197499
    move-object v0, v0

    .line 2197500
    sget-object v1, LX/5zj;->GALLERY:LX/5zj;

    .line 2197501
    iput-object v1, v0, LX/5zn;->d:LX/5zj;

    .line 2197502
    move-object v0, v0

    .line 2197503
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2197504
    invoke-direct {p0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->y()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3f2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x6774d2c8

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2197458
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2197459
    iget-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    const-string v3, "group_members"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2197460
    iput-object v1, v2, LX/F53;->n:Ljava/lang/String;

    .line 2197461
    const v1, 0x7f0305fe

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2197462
    iget-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->p:LX/F51;

    .line 2197463
    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    new-instance v4, LX/F50;

    invoke-direct {v4, v2, v1}, LX/F50;-><init>(LX/F51;Landroid/view/View;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2197464
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    .line 2197465
    const/4 p1, 0x1

    .line 2197466
    new-instance v3, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateMemberPickerFragment;

    invoke-direct {v3}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateMemberPickerFragment;-><init>()V

    .line 2197467
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2197468
    const-string p0, "is_show_caspian_style"

    invoke-virtual {v4, p0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2197469
    const-string p0, "is_sticky_header_off"

    invoke-virtual {v4, p0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2197470
    const-string p0, "hide_caspian_send_button"

    invoke-virtual {v4, p0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2197471
    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2197472
    move-object v3, v3

    .line 2197473
    const v4, 0x7f0d1072

    invoke-virtual {v2, v4, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 2197474
    invoke-virtual {v2}, LX/0hH;->b()I

    .line 2197475
    const/16 v2, 0x2b

    const v3, 0x2a9a112

    invoke-static {v5, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2e3c24fa

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2197454
    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    invoke-virtual {v1}, LX/F53;->deleteObservers()V

    .line 2197455
    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->h:LX/1Ck;

    sget-object v2, LX/F4n;->TASK_CREATE_GROUP:LX/F4n;

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2197456
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2197457
    const/16 v1, 0x2b

    const v2, 0x254cf50e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2197451
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2197452
    const-string v0, "tmp_image"

    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->z:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2197453
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2197362
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2197363
    invoke-direct {p0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->u()V

    .line 2197364
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->j:LX/0i4;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->K:LX/0i5;

    .line 2197365
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 2197366
    const-string v0, "ref"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2197367
    if-nez v0, :cond_0

    .line 2197368
    sget-object v2, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->a:Ljava/lang/Class;

    const-string v3, "Group creation source is not set"

    invoke-static {v2, v3}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2197369
    :cond_0
    iget-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    invoke-virtual {v2, v0}, LX/F53;->c(Ljava/lang/String;)V

    .line 2197370
    const-string v0, "suggestion_category"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->D:Ljava/lang/String;

    .line 2197371
    const-string v0, "suggestion_identifier"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->E:Ljava/lang/String;

    .line 2197372
    const-string v0, "trackingcode_item"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->F:Ljava/lang/String;

    .line 2197373
    const-string v0, "trackingcode_unit"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->G:Ljava/lang/String;

    .line 2197374
    const-string v0, "parent_group_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->I:Ljava/lang/String;

    .line 2197375
    const-string v0, "cache_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->H:Ljava/lang/String;

    .line 2197376
    const-string v0, "quick_return"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->J:Z

    .line 2197377
    const-string v0, "page_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2197378
    iget-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197379
    iput-object v0, v2, LX/F53;->r:Ljava/lang/String;

    .line 2197380
    const-string v0, "parent_group_or_page_name"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2197381
    const v0, 0x7f0d106e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->u:Lcom/facebook/fig/textinput/FigEditText;

    .line 2197382
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->u:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0}, Lcom/facebook/fig/textinput/FigEditText;->requestFocus()Z

    .line 2197383
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->u:Lcom/facebook/fig/textinput/FigEditText;

    new-instance v3, LX/F4f;

    invoke-direct {v3, p0}, LX/F4f;-><init>(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/fig/textinput/FigEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2197384
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->u:Lcom/facebook/fig/textinput/FigEditText;

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/text/InputFilter;

    new-instance v4, Landroid/text/InputFilter$LengthFilter;

    const/16 v5, 0x4b

    invoke-direct {v4, v5}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v4, v3, v6

    invoke-virtual {v0, v3}, Lcom/facebook/fig/textinput/FigEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 2197385
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->u:Lcom/facebook/fig/textinput/FigEditText;

    const-string v3, "group_name"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2197386
    const v0, 0x7f0d106c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->v:Landroid/widget/ImageView;

    .line 2197387
    const v0, 0x7f0d106d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/DraweeView;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->w:Lcom/facebook/drawee/view/DraweeView;

    .line 2197388
    const v0, 0x7f0d1071

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->x:Lcom/facebook/resources/ui/FbTextView;

    .line 2197389
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v0, v3}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v3

    .line 2197390
    iput-object v3, v0, LX/1Uo;->u:LX/4Ab;

    .line 2197391
    move-object v0, v0

    .line 2197392
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2197393
    iget-object v3, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->w:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v3, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2197394
    if-eqz p2, :cond_1

    const-string v0, "tmp_image"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2197395
    const-string v0, "tmp_image"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->z:Landroid/net/Uri;

    .line 2197396
    :cond_1
    if-eqz v2, :cond_2

    .line 2197397
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    invoke-virtual {v0, v2}, LX/F53;->g(Ljava/lang/String;)V

    .line 2197398
    :cond_2
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197399
    iget-object v2, v0, LX/F53;->r:Ljava/lang/String;

    move-object v0, v2

    .line 2197400
    if-eqz v0, :cond_3

    .line 2197401
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->q:LX/F4u;

    iget-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197402
    iget-object v3, v2, LX/F53;->r:Ljava/lang/String;

    move-object v2, v3

    .line 2197403
    iget-object v4, v0, LX/F4u;->d:LX/0tX;

    .line 2197404
    new-instance v3, LX/F54;

    invoke-direct {v3}, LX/F54;-><init>()V

    move-object v3, v3

    .line 2197405
    const-string v5, "page_id"

    invoke-virtual {v3, v5, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/F54;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    invoke-static {v3}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2197406
    iget-object v4, v0, LX/F4u;->b:LX/1Ck;

    sget-object v5, LX/F4t;->FETCH_PAGE_NAME:LX/F4t;

    new-instance p2, LX/F4r;

    invoke-direct {p2, v0}, LX/F4r;-><init>(LX/F4u;)V

    invoke-virtual {v4, v5, v3, p2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2197407
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->q:LX/F4u;

    iget-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197408
    iget-object v3, v2, LX/F53;->r:Ljava/lang/String;

    move-object v2, v3

    .line 2197409
    iget-object v3, v0, LX/F4u;->e:LX/3iH;

    invoke-virtual {v3, v2}, LX/3iH;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2197410
    iget-object v4, v0, LX/F4u;->b:LX/1Ck;

    sget-object v5, LX/F4t;->FETCH_PAGE_VC:LX/F4t;

    new-instance p2, LX/F4s;

    invoke-direct {p2, v0}, LX/F4s;-><init>(LX/F4u;)V

    invoke-virtual {v4, v5, v3, p2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2197411
    :cond_3
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    const-string v2, "group_visibility"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/F53;->e(Ljava/lang/String;)V

    .line 2197412
    const v0, 0x7f0d106b

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 2197413
    new-instance v1, LX/F4g;

    invoke-direct {v1, p0, v0}, LX/F4g;-><init>(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2197414
    invoke-direct {p0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->n()V

    .line 2197415
    const v0, 0x7f0d106f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2197416
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197417
    iget-object v1, v0, LX/F53;->r:Ljava/lang/String;

    move-object v0, v1

    .line 2197418
    if-nez v0, :cond_4

    .line 2197419
    const v0, 0x7f0d1071

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/F4h;

    invoke-direct {v1, p0}, LX/F4h;-><init>(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2197420
    :cond_4
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197421
    iget-object v1, v0, LX/F53;->p:Ljava/lang/String;

    move-object v0, v1

    .line 2197422
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2197423
    invoke-direct {p0, v6}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->a(Z)V

    .line 2197424
    :goto_0
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->I:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2197425
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->I:Ljava/lang/String;

    .line 2197426
    iput-object v1, v0, LX/F53;->l:Ljava/lang/String;

    .line 2197427
    :cond_5
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    invoke-virtual {v0}, LX/F53;->f()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2197428
    invoke-direct {p0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->w()V

    .line 2197429
    new-instance v0, LX/F4z;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/F4z;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->L:LX/F4z;

    .line 2197430
    invoke-direct {p0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o()V

    .line 2197431
    const v0, 0x7f0d1069

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2197432
    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->L:LX/F4z;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2197433
    :cond_6
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->i:LX/1g8;

    .line 2197434
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "creation_entered"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "group_creation"

    .line 2197435
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2197436
    move-object v1, v1

    .line 2197437
    iget-object v2, v0, LX/1g8;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2197438
    invoke-static {v0, v1}, LX/1g8;->a(LX/1g8;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2197439
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    invoke-virtual {v0, p0}, LX/F53;->addObserver(Ljava/util/Observer;)V

    .line 2197440
    invoke-virtual {p0, v7, v7}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 2197441
    return-void

    .line 2197442
    :cond_7
    const v0, 0x7f0d1071

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 2197443
    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->n:LX/0iA;

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_CREATE_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v3, LX/F4R;

    invoke-virtual {v1, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    check-cast v1, LX/F4R;

    .line 2197444
    if-nez v1, :cond_8

    .line 2197445
    :goto_1
    goto :goto_0

    .line 2197446
    :cond_8
    new-instance v2, LX/0hs;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2197447
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f083186

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2197448
    sget-object v3, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v2, v3}, LX/0ht;->a(LX/3AV;)V

    .line 2197449
    invoke-virtual {v2, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 2197450
    iget-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->n:LX/0iA;

    invoke-virtual {v2}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v2

    invoke-virtual {v1}, LX/F4R;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 2197326
    const v0, 0x7f0d1073

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2197327
    const v0, 0x7f0d1074

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2197328
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->L:LX/F4z;

    if-eqz v0, :cond_0

    .line 2197329
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->L:LX/F4z;

    invoke-virtual {v0, v1}, LX/F4z;->setVisibility(I)V

    .line 2197330
    :cond_0
    sget-object v0, LX/F4d;->a:[I

    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197331
    iget-object p1, v1, LX/F53;->f:LX/F52;

    move-object v1, p1

    .line 2197332
    invoke-virtual {v1}, LX/F52;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2197333
    :cond_1
    :goto_0
    return-void

    .line 2197334
    :pswitch_0
    invoke-direct {p0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->u()V

    .line 2197335
    invoke-direct {p0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->n()V

    .line 2197336
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->x:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->isEnabled()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->a(Z)V

    goto :goto_0

    .line 2197337
    :pswitch_1
    invoke-direct {p0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o()V

    .line 2197338
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2197339
    if-eqz v0, :cond_2

    .line 2197340
    const v1, 0x7f082f5b

    invoke-interface {v0, v1}, LX/1ZF;->x_(I)V

    .line 2197341
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2197342
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2197343
    :cond_2
    const v0, 0x7f0d1074

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2197344
    :pswitch_2
    invoke-direct {p0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o()V

    .line 2197345
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2197346
    if-eqz v0, :cond_3

    .line 2197347
    const v1, 0x7f082f5a

    invoke-interface {v0, v1}, LX/1ZF;->x_(I)V

    .line 2197348
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f082f67

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 2197349
    iput-object p1, v1, LX/108;->g:Ljava/lang/String;

    .line 2197350
    move-object v1, v1

    .line 2197351
    const/4 p1, -0x2

    .line 2197352
    iput p1, v1, LX/108;->h:I

    .line 2197353
    move-object v1, v1

    .line 2197354
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2197355
    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2197356
    new-instance v1, LX/F4m;

    invoke-direct {v1, p0}, LX/F4m;-><init>(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;)V

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    .line 2197357
    :cond_3
    const v0, 0x7f0d1073

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2197358
    :pswitch_3
    invoke-direct {p0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o()V

    .line 2197359
    invoke-direct {p0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->w()V

    .line 2197360
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->L:LX/F4z;

    if-eqz v0, :cond_1

    .line 2197361
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->L:LX/F4z;

    invoke-virtual {v0, v2}, LX/F4z;->setVisibility(I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
