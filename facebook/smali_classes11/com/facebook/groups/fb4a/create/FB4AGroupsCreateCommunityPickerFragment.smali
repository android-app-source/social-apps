.class public Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/F53;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/widget/LinearLayout;

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/fbui/widget/contentview/CheckedContentView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2197004
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2197005
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->e:Ljava/util/Map;

    .line 2197006
    return-void
.end method

.method private static a(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;LX/1Ck;LX/0tX;LX/F53;)V
    .locals 0

    .prologue
    .line 2197003
    iput-object p1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->a:LX/1Ck;

    iput-object p2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->b:LX/0tX;

    iput-object p3, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->c:LX/F53;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;

    invoke-static {v2}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-static {v2}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {v2}, LX/F53;->a(LX/0QB;)LX/F53;

    move-result-object v2

    check-cast v2, LX/F53;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->a(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;LX/1Ck;LX/0tX;LX/F53;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;Z)V
    .locals 3

    .prologue
    .line 2196991
    new-instance v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;-><init>(Landroid/content/Context;)V

    .line 2196992
    invoke-virtual {p1}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2196993
    invoke-virtual {v0, p2}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 2196994
    if-eqz p2, :cond_0

    .line 2196995
    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->c:LX/F53;

    .line 2196996
    iput-object p1, v1, LX/F53;->e:Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;

    .line 2196997
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setFocusable(Z)V

    .line 2196998
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0206fc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2196999
    new-instance v1, LX/F4T;

    invoke-direct {v1, p0, p1}, LX/F4T;-><init>(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2197000
    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->e:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2197001
    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2197002
    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2196988
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2196989
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    goto :goto_0

    .line 2196990
    :cond_0
    return-void
.end method

.method public static b$redex0(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;)V
    .locals 3

    .prologue
    .line 2197007
    iget-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->c:LX/F53;

    .line 2197008
    iget-object v1, v0, LX/F53;->d:Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;

    move-object v0, v1

    .line 2197009
    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->c:LX/F53;

    .line 2197010
    iget-object v2, v1, LX/F53;->l:Ljava/lang/String;

    move-object v1, v2

    .line 2197011
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->a$redex0(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;Z)V

    .line 2197012
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2196985
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2196986
    const-class v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;

    invoke-static {v0, p0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2196987
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6694682a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2196984
    const v1, 0x7f0305fd

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x74596d3f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x11a533bb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2196981
    iget-object v1, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->a:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2196982
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2196983
    const/16 v1, 0x2b

    const v2, -0x6d1431dd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x4ee134f5

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2196975
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2196976
    iget-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->b:LX/0tX;

    .line 2196977
    new-instance v0, LX/F59;

    invoke-direct {v0}, LX/F59;-><init>()V

    move-object v0, v0

    .line 2196978
    const-string v3, "community_limit"

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/F59;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2196979
    iget-object v2, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->a:LX/1Ck;

    sget-object v3, LX/F4U;->TASK_FETCH_AVAILABLE_COMMUNITIES:LX/F4U;

    new-instance v4, LX/F4S;

    invoke-direct {v4, p0}, LX/F4S;-><init>(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;)V

    invoke-virtual {v2, v3, v0, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2196980
    const/16 v0, 0x2b

    const v2, -0x5d127402

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2196972
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2196973
    const v0, 0x7f0d1068

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->d:Landroid/widget/LinearLayout;

    .line 2196974
    return-void
.end method
