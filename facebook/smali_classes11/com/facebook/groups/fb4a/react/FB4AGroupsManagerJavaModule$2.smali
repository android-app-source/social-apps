.class public final Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/F5T;


# direct methods
.method public constructor <init>(LX/F5T;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2198510
    iput-object p1, p0, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$2;->b:LX/F5T;

    iput-object p2, p0, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$2;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 2198511
    iget-object v0, p0, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$2;->b:LX/F5T;

    iget-object v0, v0, LX/F5T;->d:Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;

    iget-object v1, p0, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$2;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$2;->b:LX/F5T;

    invoke-static {v2}, LX/F5T;->a(LX/F5T;)Landroid/app/Activity;

    move-result-object v2

    .line 2198512
    new-instance v3, LX/9OZ;

    invoke-direct {v3}, LX/9OZ;-><init>()V

    .line 2198513
    iput-object v1, v3, LX/9OZ;->A:Ljava/lang/String;

    .line 2198514
    const-string v4, ""

    iput-object v4, v3, LX/9OZ;->C:Ljava/lang/String;

    .line 2198515
    new-instance v4, LX/9OQ;

    invoke-direct {v4}, LX/9OQ;-><init>()V

    .line 2198516
    iput-object v1, v4, LX/9OQ;->n:Ljava/lang/String;

    .line 2198517
    invoke-virtual {v3}, LX/9OZ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    .line 2198518
    iput-object v3, v4, LX/9OQ;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    .line 2198519
    invoke-virtual {v4}, LX/9OQ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v3

    const/4 v5, 0x0

    iget-object v6, v0, Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;->b:LX/8ht;

    iget-object v7, v0, Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;->g:LX/0Uh;

    iget-object v8, v0, Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;->h:LX/0ad;

    move-object v4, v1

    invoke-static/range {v3 .. v8}, LX/DOI;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Ljava/lang/String;ZLX/8ht;LX/0Uh;LX/0ad;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v4

    .line 2198520
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    iget-object v3, v0, Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;->d:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ComponentName;

    invoke-virtual {v5, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v3

    const-string v5, "target_fragment"

    sget-object v6, LX/0cQ;->SEARCH_FRAGMENT:LX/0cQ;

    invoke-virtual {v6}, LX/0cQ;->ordinal()I

    move-result v6

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v5

    .line 2198521
    if-eqz v4, :cond_0

    .line 2198522
    const-string v3, "initial_typeahead_query"

    invoke-virtual {v5, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2198523
    :cond_0
    iget-object v3, v0, Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;->f:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Cvs;

    invoke-interface {v3}, LX/Cvs;->b()V

    .line 2198524
    iget-object v3, v0, Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;->e:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2SV;

    sget-object v4, Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;->a:Lcom/facebook/common/callercontext/CallerContext;

    sget-object v6, LX/EPu;->MEMORY:LX/EPu;

    invoke-virtual {v3, v4, v6}, LX/2SP;->a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V

    .line 2198525
    iget-object v3, v0, Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v5, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2198526
    return-void
.end method
