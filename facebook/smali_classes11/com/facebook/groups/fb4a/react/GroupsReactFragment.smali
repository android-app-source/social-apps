.class public Lcom/facebook/groups/fb4a/react/GroupsReactFragment;
.super Lcom/facebook/fbreact/fragment/FbReactFragment;
.source ""


# instance fields
.field public f:LX/33u;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2198978
    invoke-direct {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;-><init>()V

    .line 2198979
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/groups/fb4a/react/GroupsReactFragment;

    invoke-static {p0}, LX/33u;->a(LX/0QB;)LX/33u;

    move-result-object v1

    check-cast v1, LX/33u;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object p0

    check-cast p0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object v1, p1, Lcom/facebook/groups/fb4a/react/GroupsReactFragment;->f:LX/33u;

    iput-object p0, p1, Lcom/facebook/groups/fb4a/react/GroupsReactFragment;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    return-void
.end method

.method public static m()LX/F5b;
    .locals 1

    .prologue
    .line 2198980
    new-instance v0, LX/F5b;

    invoke-direct {v0}, LX/F5b;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2198970
    const-class v0, Lcom/facebook/groups/fb4a/react/GroupsReactFragment;

    invoke-static {v0, p0}, Lcom/facebook/groups/fb4a/react/GroupsReactFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2198971
    invoke-super {p0, p1}, Lcom/facebook/fbreact/fragment/FbReactFragment;->a(Landroid/os/Bundle;)V

    .line 2198972
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2198973
    const-string v1, "warm_perf_marker"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 2198974
    const-string v2, "react_context_perf_marker"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 2198975
    new-instance v2, LX/F5a;

    invoke-direct {v2, p0, v0, v1}, LX/F5a;-><init>(Lcom/facebook/groups/fb4a/react/GroupsReactFragment;II)V

    .line 2198976
    sput-object v2, Lcom/facebook/react/bridge/ReactMarker;->a:LX/5pc;

    .line 2198977
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2198964
    invoke-super {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->c()V

    .line 2198965
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2198966
    const-string v1, "cold_perf_marker"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 2198967
    if-eqz v0, :cond_0

    .line 2198968
    iget-object v1, p0, Lcom/facebook/groups/fb4a/react/GroupsReactFragment;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2198969
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2198957
    invoke-super {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->d()V

    .line 2198958
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2198959
    const-string v1, "cold_perf_marker"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 2198960
    if-eqz v0, :cond_0

    .line 2198961
    iget-object v1, p0, Lcom/facebook/groups/fb4a/react/GroupsReactFragment;->f:LX/33u;

    invoke-virtual {v1}, LX/33u;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2198962
    iget-object v1, p0, Lcom/facebook/groups/fb4a/react/GroupsReactFragment;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 2198963
    :cond_0
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x30e0315

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2198955
    invoke-super {p0, p1}, Lcom/facebook/fbreact/fragment/FbReactFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2198956
    const/16 v1, 0x2b

    const v2, -0x7ab9716c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
