.class public Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/8ht;

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2SV;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Cvs;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Uh;

.field public final h:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2198918
    const-class v0, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;

    const-string v1, "search"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/8ht;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/0Or;LX/0Or;LX/0Uh;LX/0ad;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8ht;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2SV;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Cvs;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2198919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2198920
    iput-object p1, p0, Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;->b:LX/8ht;

    .line 2198921
    iput-object p2, p0, Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2198922
    iput-object p3, p0, Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;->d:LX/0Or;

    .line 2198923
    iput-object p4, p0, Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;->e:LX/0Or;

    .line 2198924
    iput-object p5, p0, Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;->f:LX/0Or;

    .line 2198925
    iput-object p6, p0, Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;->g:LX/0Uh;

    .line 2198926
    iput-object p7, p0, Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;->h:LX/0ad;

    .line 2198927
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;
    .locals 9

    .prologue
    .line 2198928
    new-instance v1, Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;

    invoke-static {p0}, LX/8ht;->a(LX/0QB;)LX/8ht;

    move-result-object v2

    check-cast v2, LX/8ht;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    const/16 v4, 0xc

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x11b2

    invoke-static {p0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x32da

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v1 .. v8}, Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;-><init>(LX/8ht;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/0Or;LX/0Or;LX/0Uh;LX/0ad;)V

    .line 2198929
    move-object v0, v1

    .line 2198930
    return-object v0
.end method
