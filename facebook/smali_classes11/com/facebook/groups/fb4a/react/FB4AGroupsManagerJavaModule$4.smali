.class public final Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/5pG;

.field public final synthetic b:LX/F5T;


# direct methods
.method public constructor <init>(LX/F5T;LX/5pG;)V
    .locals 0

    .prologue
    .line 2198540
    iput-object p1, p0, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$4;->b:LX/F5T;

    iput-object p2, p0, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$4;->a:LX/5pG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2198541
    iget-object v0, p0, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$4;->b:LX/F5T;

    invoke-static {v0}, LX/F5T;->h(LX/F5T;)LX/1ZF;

    move-result-object v3

    .line 2198542
    if-nez v3, :cond_1

    .line 2198543
    const-string v0, "RKTreehouseManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get title bar in activity:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$4;->b:LX/F5T;

    invoke-static {v2}, LX/F5T;->e(LX/F5T;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198544
    :cond_0
    :goto_0
    return-void

    .line 2198545
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$4;->a:LX/5pG;

    if-nez v0, :cond_2

    .line 2198546
    invoke-interface {v3, v4}, LX/1ZF;->setCustomTitle(Landroid/view/View;)V

    .line 2198547
    invoke-interface {v3, v4}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2198548
    invoke-interface {v3, v2}, LX/1ZF;->k_(Z)V

    goto :goto_0

    .line 2198549
    :cond_2
    iget-object v0, p0, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$4;->a:LX/5pG;

    const-string v4, "type"

    invoke-interface {v0, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v0, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_3
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 2198550
    const-string v0, "RKTreehouseManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown search bar type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$4;->a:LX/5pG;

    const-string v3, "type"

    invoke-interface {v2, v3}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2198551
    :sswitch_0
    const-string v5, "editable"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v0, v1

    goto :goto_1

    :sswitch_1
    const-string v5, "clickable"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v0, v2

    goto :goto_1

    :sswitch_2
    const-string v5, "text"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v0, 0x2

    goto :goto_1

    .line 2198552
    :pswitch_0
    new-instance v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    iget-object v1, p0, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$4;->b:LX/F5T;

    invoke-static {v1}, LX/F5T;->f(LX/F5T;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;-><init>(Landroid/content/Context;)V

    .line 2198553
    new-instance v1, LX/F5Q;

    invoke-direct {v1, p0}, LX/F5Q;-><init>(Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$4;)V

    .line 2198554
    iput-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->o:LX/F5P;

    .line 2198555
    invoke-interface {v3, v0}, LX/1ZF;->setCustomTitle(Landroid/view/View;)V

    .line 2198556
    invoke-interface {v3, v2}, LX/1ZF;->k_(Z)V

    .line 2198557
    iget-object v1, p0, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$4;->a:LX/5pG;

    const-string v2, "searchHint"

    invoke-interface {v1, v2}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2198558
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v1, v1

    .line 2198559
    iget-object v2, p0, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$4;->a:LX/5pG;

    const-string v3, "searchHint"

    invoke-interface {v2, v3}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ui/search/SearchEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2198560
    :cond_4
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 2198561
    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->b()Z

    goto/16 :goto_0

    .line 2198562
    :pswitch_1
    new-instance v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    iget-object v4, p0, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$4;->b:LX/F5T;

    invoke-static {v4}, LX/F5T;->g(LX/F5T;)Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;-><init>(Landroid/content/Context;)V

    .line 2198563
    iget-object v4, p0, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$4;->a:LX/5pG;

    const-string v5, "searchHint"

    invoke-interface {v4, v5}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 2198564
    iget-object v4, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v4, v4

    .line 2198565
    iget-object v5, p0, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$4;->a:LX/5pG;

    const-string v6, "searchHint"

    invoke-interface {v5, v6}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/ui/search/SearchEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2198566
    :cond_5
    iget-object v4, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v4, v4

    .line 2198567
    invoke-virtual {v4, v1}, Lcom/facebook/ui/search/SearchEditText;->setFocusable(Z)V

    .line 2198568
    new-instance v1, LX/F5R;

    invoke-direct {v1, p0}, LX/F5R;-><init>(Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$4;)V

    invoke-virtual {v0, v1}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a(Landroid/view/View$OnClickListener;)V

    .line 2198569
    invoke-interface {v3, v0}, LX/1ZF;->setCustomTitle(Landroid/view/View;)V

    .line 2198570
    invoke-interface {v3, v2}, LX/1ZF;->k_(Z)V

    goto/16 :goto_0

    .line 2198571
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$4;->a:LX/5pG;

    const-string v1, "text"

    invoke-interface {v0, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2198572
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2198573
    invoke-interface {v3, v0}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2198574
    invoke-interface {v3, v2}, LX/1ZF;->k_(Z)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x751aa91e -> :sswitch_1
        0x36452d -> :sswitch_2
        0x5f82ee64 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
