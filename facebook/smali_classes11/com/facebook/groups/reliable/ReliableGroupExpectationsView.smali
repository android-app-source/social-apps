.class public Lcom/facebook/groups/reliable/ReliableGroupExpectationsView;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field public a:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2200425
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2200426
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/groups/reliable/ReliableGroupExpectationsView;->a(Landroid/util/AttributeSet;I)V

    .line 2200427
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2200419
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2200420
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/facebook/groups/reliable/ReliableGroupExpectationsView;->a(Landroid/util/AttributeSet;I)V

    .line 2200421
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2200422
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2200423
    invoke-direct {p0, p2, p3}, Lcom/facebook/groups/reliable/ReliableGroupExpectationsView;->a(Landroid/util/AttributeSet;I)V

    .line 2200424
    return-void
.end method

.method private a(Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 2200408
    const-class v1, Lcom/facebook/groups/reliable/ReliableGroupExpectationsView;

    invoke-static {v1, p0}, Lcom/facebook/groups/reliable/ReliableGroupExpectationsView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2200409
    iget-object v1, p0, Lcom/facebook/groups/reliable/ReliableGroupExpectationsView;->a:LX/0ad;

    sget-short v2, LX/F6S;->b:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2200410
    :cond_0
    :goto_0
    return-void

    .line 2200411
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/groups/reliable/ReliableGroupExpectationsView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0311c5

    invoke-static {v1, v2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2200412
    invoke-virtual {p0, v3}, Lcom/facebook/groups/reliable/ReliableGroupExpectationsView;->setOrientation(I)V

    .line 2200413
    if-eqz p1, :cond_0

    .line 2200414
    invoke-virtual {p0}, Lcom/facebook/groups/reliable/ReliableGroupExpectationsView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/03r;->ReliableGroupExpectationsView:[I

    invoke-virtual {v1, p1, v2, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 2200415
    const/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2200416
    :goto_1
    const v2, 0x7f0d29b9

    invoke-virtual {p0, v2}, Lcom/facebook/groups/reliable/ReliableGroupExpectationsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2200417
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    .line 2200418
    :cond_2
    const/16 v0, 0x8

    goto :goto_1
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/groups/reliable/ReliableGroupExpectationsView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/groups/reliable/ReliableGroupExpectationsView;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v0, p0, Lcom/facebook/groups/reliable/ReliableGroupExpectationsView;->a:LX/0ad;

    return-void
.end method
