.class public Lcom/facebook/groups/editsettings/fragment/GroupPurposeItemView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public l:Lcom/facebook/resources/ui/FbTextView;

.field public m:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2194168
    const-class v0, Lcom/facebook/groups/editsettings/fragment/GroupPurposeItemView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/editsettings/fragment/GroupPurposeItemView;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2194169
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 2194170
    const v0, 0x7f030829

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2194171
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/fragment/GroupPurposeItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b20f9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2194172
    new-instance v1, LX/1a3;

    const/4 p1, -0x1

    invoke-direct {v1, p1, v0}, LX/1a3;-><init>(II)V

    .line 2194173
    invoke-virtual {p0, v1}, Lcom/facebook/groups/editsettings/fragment/GroupPurposeItemView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2194174
    const v0, 0x7f0d1556

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupPurposeItemView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2194175
    const v0, 0x7f0d1557

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupPurposeItemView;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 2194176
    const v0, 0x7f0d1558

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupPurposeItemView;->m:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2194177
    return-void
.end method
