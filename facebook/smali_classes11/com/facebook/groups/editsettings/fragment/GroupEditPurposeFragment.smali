.class public Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/F2L;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/DZ5;
    .annotation runtime Lcom/facebook/groups/editsettings/annotation/EditGroupPurposeNavigationHandler;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

.field public g:Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel;

.field public h:LX/F3T;

.field public i:Landroid/support/v7/widget/RecyclerView;

.field public final j:LX/F3A;

.field private final k:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2193893
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2193894
    new-instance v0, LX/F3A;

    invoke-direct {v0, p0}, LX/F3A;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->j:LX/F3A;

    .line 2193895
    new-instance v0, LX/F3B;

    invoke-direct {v0, p0}, LX/F3B;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->k:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 2193896
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2193897
    invoke-virtual {v0, p1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->a:Landroid/content/res/Resources;

    const v2, 0x7f080016

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2193898
    return-void
.end method

.method public static c(Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;)Z
    .locals 1

    .prologue
    .line 2193885
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->f:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->h:LX/F3T;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->h:LX/F3T;

    .line 2193886
    iget-object p0, v0, LX/F3T;->b:Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    move-object v0, p0

    .line 2193887
    if-nez v0, :cond_1

    .line 2193888
    :cond_0
    const/4 v0, 0x0

    .line 2193889
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2193890
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2193891
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/F2L;->b(LX/0QB;)LX/F2L;

    move-result-object v5

    check-cast v5, LX/F2L;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p1

    check-cast p1, LX/1Ck;

    invoke-static {v0}, LX/DZ5;->b(LX/0QB;)LX/DZ5;

    move-result-object v0

    check-cast v0, LX/DZ5;

    iput-object v3, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->a:Landroid/content/res/Resources;

    iput-object v4, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->b:LX/0tX;

    iput-object v5, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->c:LX/F2L;

    iput-object p1, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->d:LX/1Ck;

    iput-object v0, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->e:LX/DZ5;

    .line 2193892
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6cf54a4a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2193859
    const v1, 0x7f0303f5

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x10aaa43f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6e4d14de

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2193860
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2193861
    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->d:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2193862
    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->c:LX/F2L;

    invoke-virtual {v1}, LX/F2L;->a()V

    .line 2193863
    const/16 v1, 0x2b

    const v2, 0x21919422

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2193864
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2193865
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2193866
    const-string v3, "group_edit_name_description_data"

    invoke-static {v0, v3}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->f:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    .line 2193867
    const v0, 0x7f0d0c36

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->i:Landroid/support/v7/widget/RecyclerView;

    .line 2193868
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->i:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, LX/62X;

    iget-object v4, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->a:Landroid/content/res/Resources;

    const v5, 0x7f0a05ed

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    iget-object v5, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->a:Landroid/content/res/Resources;

    const v6, 0x7f0b0033

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-direct {v3, v4, v5}, LX/62X;-><init>(II)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2193869
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v0, v3}, LX/1P1;-><init>(Landroid/content/Context;)V

    .line 2193870
    iget-object v3, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->i:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2193871
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->f:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->p()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2193872
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->f:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->p()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2193873
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_5

    .line 2193874
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->f:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->p()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2193875
    if-eqz v0, :cond_3

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 2193876
    :goto_4
    if-eqz v1, :cond_6

    const v0, 0x7f0831a1

    move v1, v0

    .line 2193877
    :goto_5
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->e:LX/DZ5;

    iget-object v2, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->a:Landroid/content/res/Resources;

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->a:Landroid/content/res/Resources;

    const v3, 0x7f0831a2

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->k:Landroid/view/View$OnClickListener;

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, LX/DZ5;->a(Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 2193878
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->d:LX/1Ck;

    const-string v1, "fetch_group_possible_purposes"

    new-instance v2, LX/F3C;

    invoke-direct {v2, p0}, LX/F3C;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;)V

    new-instance v3, LX/F3D;

    invoke-direct {v3, p0}, LX/F3D;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2193879
    return-void

    .line 2193880
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2193881
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_1

    .line 2193882
    :cond_3
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2193883
    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_3

    .line 2193884
    :cond_6
    const v0, 0x7f0831a0

    move v1, v0

    goto :goto_5

    :cond_7
    move v1, v2

    goto :goto_4
.end method
