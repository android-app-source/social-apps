.class public Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/F2L;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/DZ5;
    .annotation runtime Lcom/facebook/groups/editsettings/annotation/EditGroupTagsNavigationHandler;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/F4Q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/F31;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/F30;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;

.field public k:Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;

.field public l:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

.field public m:Landroid/widget/ListView;

.field public n:Lcom/facebook/widget/text/BetterTextView;

.field public o:Landroid/widget/ProgressBar;

.field public p:Ljava/lang/String;

.field public q:Z

.field public r:Z

.field public s:I

.field public final t:Landroid/os/Handler;

.field public u:Ljava/lang/Runnable;

.field public v:LX/F3H;

.field private final w:Landroid/view/View$OnClickListener;

.field private final x:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2194125
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2194126
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->t:Landroid/os/Handler;

    .line 2194127
    new-instance v0, LX/F3H;

    invoke-direct {v0, p0}, LX/F3H;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->v:LX/F3H;

    .line 2194128
    new-instance v0, LX/F3I;

    invoke-direct {v0, p0}, LX/F3I;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->w:Landroid/view/View$OnClickListener;

    .line 2194129
    new-instance v0, LX/F3J;

    invoke-direct {v0, p0}, LX/F3J;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->x:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public static a(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2194130
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2194131
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    .line 2194132
    invoke-virtual {v0, p1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 2194133
    invoke-virtual {v0, p2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2194134
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2194135
    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2194090
    if-eqz p1, :cond_0

    .line 2194091
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->b:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2194092
    :cond_0
    return-void
.end method

.method public static n$redex0(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V
    .locals 2

    .prologue
    .line 2194123
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->o:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2194124
    return-void
.end method

.method public static r(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V
    .locals 1

    .prologue
    .line 2194120
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->l:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {p0, v0}, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->a$redex0(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;Landroid/view/View;)V

    .line 2194121
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 2194122
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 2194136
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2194117
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2194118
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v4

    check-cast v4, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/F2L;->b(LX/0QB;)LX/F2L;

    move-result-object v6

    check-cast v6, LX/F2L;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static {v0}, LX/DZ5;->b(LX/0QB;)LX/DZ5;

    move-result-object v8

    check-cast v8, LX/DZ5;

    invoke-static {v0}, LX/F4Q;->b(LX/0QB;)LX/F4Q;

    move-result-object v9

    check-cast v9, LX/F4Q;

    invoke-static {v0}, LX/F31;->b(LX/0QB;)LX/F31;

    move-result-object p1

    check-cast p1, LX/F31;

    new-instance v1, LX/F30;

    invoke-static {v0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object p0

    check-cast p0, Landroid/view/LayoutInflater;

    invoke-direct {v1, p0}, LX/F30;-><init>(Landroid/view/LayoutInflater;)V

    move-object v0, v1

    check-cast v0, LX/F30;

    iput-object v3, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->a:Landroid/content/res/Resources;

    iput-object v4, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->b:Landroid/view/inputmethod/InputMethodManager;

    iput-object v5, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->c:LX/0tX;

    iput-object v6, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->d:LX/F2L;

    iput-object v7, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->e:LX/1Ck;

    iput-object v8, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->f:LX/DZ5;

    iput-object v9, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->g:LX/F4Q;

    iput-object p1, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->h:LX/F31;

    iput-object v0, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->i:LX/F30;

    .line 2194119
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x69f8f61e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2194115
    const v2, 0x7f0303f7

    move v1, v2

    .line 2194116
    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x54164fe3

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2c243bc5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2194112
    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->d:LX/F2L;

    invoke-virtual {v1}, LX/F2L;->a()V

    .line 2194113
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2194114
    const/16 v1, 0x2b

    const v2, -0x691f94fd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1c5a4acf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2194109
    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->t:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->u:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2194110
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2194111
    const/16 v1, 0x2b

    const v2, -0x76bf157c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2194093
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2194094
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2194095
    const-string v1, "edit_tags_group_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->p:Ljava/lang/String;

    .line 2194096
    const v0, 0x7f0d0c3a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->l:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 2194097
    const v0, 0x7f0d0c3d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->m:Landroid/widget/ListView;

    .line 2194098
    const v0, 0x7f0d0c3b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->n:Lcom/facebook/widget/text/BetterTextView;

    .line 2194099
    const v0, 0x7f0d0c3c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->o:Landroid/widget/ProgressBar;

    .line 2194100
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->l:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setEnabled(Z)V

    .line 2194101
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->l:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v1, LX/F3N;

    invoke-direct {v1, p0}, LX/F3N;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2194102
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->l:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v1, LX/F3O;

    invoke-direct {v1, p0}, LX/F3O;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2194103
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->m:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->i:LX/F30;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2194104
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->i:LX/F30;

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->v:LX/F3H;

    .line 2194105
    iput-object v1, v0, LX/F30;->b:LX/F3H;

    .line 2194106
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->f:LX/DZ5;

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->a:Landroid/content/res/Resources;

    const v2, 0x7f083196

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->a:Landroid/content/res/Resources;

    const v3, 0x7f080031

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->x:Landroid/view/View$OnClickListener;

    iget-object v5, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->w:Landroid/view/View$OnClickListener;

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, LX/DZ5;->a(Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 2194107
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->e:LX/1Ck;

    sget-object v1, LX/F3Q;->GET_GROUP_TOPIC_TAGS:LX/F3Q;

    new-instance v2, LX/F3K;

    invoke-direct {v2, p0}, LX/F3K;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V

    new-instance v3, LX/F3L;

    invoke-direct {v3, p0}, LX/F3L;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2194108
    return-void
.end method
