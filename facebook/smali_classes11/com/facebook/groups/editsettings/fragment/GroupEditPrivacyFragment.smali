.class public Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/F2V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DZ7;
    .annotation runtime Lcom/facebook/groups/editsettings/annotation/EditGroupPrivacyNavigationHandler;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/F2U;

.field private e:Lcom/facebook/widget/listview/BetterListView;

.field private f:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

.field private final g:LX/F39;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2193783
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2193784
    new-instance v0, LX/F39;

    invoke-direct {v0, p0}, LX/F39;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;->g:LX/F39;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;

    const-class v1, LX/F2V;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/F2V;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-static {p0}, LX/DZ8;->a(LX/0QB;)LX/DZ8;

    move-result-object p0

    check-cast p0, LX/DZ7;

    iput-object v1, p1, Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;->a:LX/F2V;

    iput-object v2, p1, Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;->b:Landroid/content/res/Resources;

    iput-object p0, p1, Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;->c:LX/DZ7;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2193778
    const-class v0, Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;

    invoke-static {v0, p0}, Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2193779
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2193780
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2193781
    const-string v1, "group_edit_privacy_data"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;->f:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    .line 2193782
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x2ca2beb9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2193777
    const v1, 0x7f030803

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x5945026f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x709b1ca0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2193757
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2193758
    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;->d:LX/F2U;

    .line 2193759
    iget-object v2, v1, LX/F2U;->a:LX/F2L;

    const/4 p0, 0x0

    .line 2193760
    iput-object p0, v2, LX/F2L;->j:LX/F39;

    .line 2193761
    iget-object v2, v1, LX/F2U;->a:LX/F2L;

    invoke-virtual {v2}, LX/F2L;->a()V

    .line 2193762
    const/16 v1, 0x2b

    const v2, 0x54c90d4c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2193763
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2193764
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;->c:LX/DZ7;

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;->b:Landroid/content/res/Resources;

    const v2, 0x7f0831c6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, p0, v1, v2}, LX/DZ7;->a(Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;LX/DLO;)V

    .line 2193765
    const v0, 0x7f0d151c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;->e:Lcom/facebook/widget/listview/BetterListView;

    .line 2193766
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;->a:LX/F2V;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;->g:LX/F39;

    .line 2193767
    new-instance v3, LX/F2U;

    invoke-static {v0}, LX/F4L;->b(LX/0QB;)LX/F4L;

    move-result-object v6

    check-cast v6, LX/F4L;

    invoke-static {v0}, LX/F2L;->b(LX/0QB;)LX/F2L;

    move-result-object v7

    check-cast v7, LX/F2L;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v9

    check-cast v9, LX/0W9;

    move-object v4, v1

    move-object v5, v2

    invoke-direct/range {v3 .. v9}, LX/F2U;-><init>(LX/0gc;LX/F39;LX/F4L;LX/F2L;Landroid/content/res/Resources;LX/0W9;)V

    .line 2193768
    move-object v0, v3

    .line 2193769
    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;->d:LX/F2U;

    .line 2193770
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;->d:LX/F2U;

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;->f:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    .line 2193771
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->B()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2193772
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;->e:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditPrivacyFragment;->d:LX/F2U;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2193773
    return-void

    .line 2193774
    :cond_1
    iput-object v1, v0, LX/F2U;->i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    .line 2193775
    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->B()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/F2U;->h:Ljava/lang/String;

    .line 2193776
    invoke-static {v0}, LX/F2U;->b(LX/F2U;)V

    goto :goto_0
.end method
