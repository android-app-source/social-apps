.class public final Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment$8$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/F3O;


# direct methods
.method public constructor <init>(LX/F3O;)V
    .locals 0

    .prologue
    .line 2194044
    iput-object p1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment$8$1;->a:LX/F3O;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2194045
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment$8$1;->a:LX/F3O;

    iget-object v0, v0, LX/F3O;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-boolean v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->q:Z

    if-nez v0, :cond_1

    .line 2194046
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment$8$1;->a:LX/F3O;

    iget-object v0, v0, LX/F3O;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    const/4 v1, 0x1

    .line 2194047
    iput-boolean v1, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->q:Z

    .line 2194048
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment$8$1;->a:LX/F3O;

    iget-object v0, v0, LX/F3O;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    .line 2194049
    iget-boolean v1, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->q:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->i:LX/F30;

    invoke-virtual {v1}, LX/F30;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2194050
    iget-object v1, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->o:Landroid/widget/ProgressBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2194051
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment$8$1;->a:LX/F3O;

    iget-object v0, v0, LX/F3O;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment$8$1;->a:LX/F3O;

    iget-object v1, v1, LX/F3O;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-object v1, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->l:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2194052
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2194053
    iget-object v2, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->i:LX/F30;

    const/4 v3, 0x0

    .line 2194054
    iput-object v3, v2, LX/F30;->a:Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;

    .line 2194055
    iget-object v2, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->i:LX/F30;

    const v3, 0xed10c34

    invoke-static {v2, v3}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2194056
    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->q:Z

    .line 2194057
    invoke-static {v0}, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->n$redex0(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V

    .line 2194058
    :cond_1
    :goto_0
    return-void

    .line 2194059
    :cond_2
    new-instance v2, LX/F46;

    invoke-direct {v2}, LX/F46;-><init>()V

    move-object v2, v2

    .line 2194060
    iget-object v3, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->a:Landroid/content/res/Resources;

    const v4, 0x7f0b20fe

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2194061
    const-string v4, "query"

    invoke-virtual {v2, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2194062
    const-string v4, "num_results"

    const/4 v5, 0x6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2194063
    const-string v4, "width"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2194064
    const-string v4, "height"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2194065
    iget-object v3, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->e:LX/1Ck;

    sget-object v4, LX/F3Q;->GET_GROUP_TAG_SUGGESTIONS:LX/F3Q;

    iget-object v5, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->c:LX/0tX;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object p0, LX/0zS;->d:LX/0zS;

    invoke-virtual {v2, p0}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    const/4 p0, 0x1

    .line 2194066
    iput-boolean p0, v2, LX/0zO;->p:Z

    .line 2194067
    move-object v2, v2

    .line 2194068
    invoke-virtual {v5, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    new-instance v5, LX/F3M;

    invoke-direct {v5, v0}, LX/F3M;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V

    invoke-virtual {v3, v4, v2, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method
