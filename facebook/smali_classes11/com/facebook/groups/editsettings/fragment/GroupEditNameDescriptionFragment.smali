.class public Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/F2L;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/DZ5;
    .annotation runtime Lcom/facebook/groups/editsettings/annotation/EditGroupNameNavigationHandler;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/F4Q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/F31;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;

.field public l:Lcom/facebook/widget/text/BetterEditTextView;

.field public m:Lcom/facebook/widget/text/BetterTextView;

.field public n:Lcom/facebook/widget/text/BetterEditTextView;

.field public o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

.field private p:Z

.field public q:Ljava/lang/String;

.field private final r:Landroid/view/View$OnClickListener;

.field private final s:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2193748
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2193749
    new-instance v0, LX/F32;

    invoke-direct {v0, p0}, LX/F32;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->r:Landroid/view/View$OnClickListener;

    .line 2193750
    new-instance v0, LX/F33;

    invoke-direct {v0, p0}, LX/F33;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->s:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2193745
    if-eqz p1, :cond_0

    .line 2193746
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->c:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2193747
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2193732
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    if-eqz v0, :cond_0

    .line 2193733
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2193734
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->l:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterEditTextView;->setFocusable(Z)V

    .line 2193735
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->l:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a0038

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setTextColor(I)V

    .line 2193736
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->l:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2193737
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->l:Lcom/facebook/widget/text/BetterEditTextView;

    new-array v1, v3, [Landroid/text/InputFilter;

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/16 v3, 0x4b

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setFilters([Landroid/text/InputFilter;)V

    .line 2193738
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 2193739
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->n:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a05ee

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setHintTextColor(I)V

    .line 2193740
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->n:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a:Landroid/content/res/Resources;

    const v2, 0x7f0831bc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 2193741
    :goto_1
    return-void

    .line 2193742
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->l:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterEditTextView;->setFocusable(Z)V

    .line 2193743
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->l:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a05ef

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setTextColor(I)V

    goto :goto_0

    .line 2193744
    :cond_2
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->n:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public static d(Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;)V
    .locals 3

    .prologue
    .line 2193728
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->l:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->n:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2193729
    :cond_0
    invoke-static {p0}, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->m(Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;)V

    .line 2193730
    :goto_0
    return-void

    .line 2193731
    :cond_1
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f08319d

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f08319f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f08319e

    new-instance v2, LX/F36;

    invoke-direct {v2, p0}, LX/F36;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    goto :goto_0
.end method

.method public static m(Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;)V
    .locals 1

    .prologue
    .line 2193723
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->n:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-direct {p0, v0}, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a(Landroid/view/View;)V

    .line 2193724
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->l:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-direct {p0, v0}, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a(Landroid/view/View;)V

    .line 2193725
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->p:Z

    .line 2193726
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 2193727
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 2193719
    iget-boolean v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->p:Z

    if-nez v0, :cond_0

    .line 2193720
    invoke-static {p0}, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->d(Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;)V

    .line 2193721
    const/4 v0, 0x1

    .line 2193722
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    .line 2193716
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2193717
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/F2L;->b(LX/0QB;)LX/F2L;

    move-result-object v4

    check-cast v4, LX/F2L;

    invoke-static {v0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v5

    check-cast v5, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v7

    check-cast v7, LX/17Y;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v9

    check-cast v9, LX/1Ck;

    invoke-static {v0}, LX/DZ5;->b(LX/0QB;)LX/DZ5;

    move-result-object v10

    check-cast v10, LX/DZ5;

    invoke-static {v0}, LX/F4Q;->b(LX/0QB;)LX/F4Q;

    move-result-object p1

    check-cast p1, LX/F4Q;

    invoke-static {v0}, LX/F31;->b(LX/0QB;)LX/F31;

    move-result-object v0

    check-cast v0, LX/F31;

    iput-object v3, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a:Landroid/content/res/Resources;

    iput-object v4, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->b:LX/F2L;

    iput-object v5, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->c:Landroid/view/inputmethod/InputMethodManager;

    iput-object v6, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iput-object v7, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->e:LX/17Y;

    iput-object v8, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->f:LX/0tX;

    iput-object v9, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->g:LX/1Ck;

    iput-object v10, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->h:LX/DZ5;

    iput-object p1, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->i:LX/F4Q;

    iput-object v0, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->j:LX/F31;

    .line 2193718
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x3f916f5a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2193670
    const v2, 0x7f0303f4

    move v1, v2

    .line 2193671
    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x4b89a22

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3f8b063

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2193713
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2193714
    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->g:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2193715
    const/16 v1, 0x2b

    const v2, 0x2bd4f606

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7538ce67

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2193709
    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->n:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-direct {p0, v1}, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a(Landroid/view/View;)V

    .line 2193710
    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->l:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-direct {p0, v1}, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a(Landroid/view/View;)V

    .line 2193711
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2193712
    const/16 v1, 0x2b

    const v2, 0x64ce8740

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2193705
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2193706
    const-string v0, "group_edit_name_state"

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->l:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2193707
    const-string v0, "group_edit_description_state"

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->n:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2193708
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2193672
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2193673
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2193674
    const-string v1, "has_no_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2193675
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2193676
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2193677
    const-string v1, "group_edit_name_description_data"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    .line 2193678
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->b:LX/F2L;

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/F2L;->a(Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;LX/F2f;)V

    .line 2193679
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->q:Ljava/lang/String;

    .line 2193680
    :goto_0
    const v0, 0x7f0d0c32

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->l:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2193681
    const v0, 0x7f0d0c33

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->m:Lcom/facebook/widget/text/BetterTextView;

    .line 2193682
    const v0, 0x7f0d0c34

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->n:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2193683
    if-eqz p2, :cond_3

    .line 2193684
    const-string v0, "group_edit_name_state"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "group_edit_description_state"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a$redex0(Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 2193685
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2193686
    const/16 p2, 0x21

    const/4 p1, 0x0

    .line 2193687
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a:Landroid/content/res/Resources;

    const v1, 0x7f0831c0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2193688
    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a:Landroid/content/res/Resources;

    const v2, 0x7f0831c1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2193689
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object v0, v2, p1

    const/4 v3, 0x1

    const-string v4, " "

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object v1, v2, v3

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2193690
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2193691
    new-instance v3, LX/F37;

    invoke-direct {v3, p0}, LX/F37;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;)V

    .line 2193692
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5, p2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2193693
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    iget-object v4, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a:Landroid/content/res/Resources;

    const v5, 0x7f0a05ee

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 2193694
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v2, v3, v0, v1, p2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2193695
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2193696
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2193697
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2193698
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->invalidate()V

    .line 2193699
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->h:LX/DZ5;

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a:Landroid/content/res/Resources;

    const v2, 0x7f083195

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a:Landroid/content/res/Resources;

    const v3, 0x7f080031

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->s:Landroid/view/View$OnClickListener;

    iget-object v5, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->r:Landroid/view/View$OnClickListener;

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, LX/DZ5;->a(Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 2193700
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->g:LX/1Ck;

    sget-object v1, LX/F38;->GET_GROUP_NAME_DESCRIPTION:LX/F38;

    new-instance v2, LX/F34;

    invoke-direct {v2, p0}, LX/F34;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;)V

    new-instance v3, LX/F35;

    invoke-direct {v3, p0}, LX/F35;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2193701
    return-void

    .line 2193702
    :cond_2
    const-string v1, "group_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 2193703
    :cond_3
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    if-eqz v0, :cond_0

    .line 2193704
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->s()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a$redex0(Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
