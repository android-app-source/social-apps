.class public Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public a:LX/F2s;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/F2N;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/DZ7;
    .annotation runtime Lcom/facebook/groups/editsettings/annotation/EditGroupSettingsNavigationHandler;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DKH;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/F4Q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

.field public k:LX/F2r;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2193972
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 2193968
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mDetached:Z

    move v0, v0

    .line 2193969
    if-nez v0, :cond_0

    .line 2193970
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->e:LX/1Ck;

    const-string v1, "FETCH_GROUP_SETTINGS"

    new-instance v2, LX/F3F;

    invoke-direct {v2, p0}, LX/F3F;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;)V

    new-instance v3, LX/F3G;

    invoke-direct {v3, p0}, LX/F3G;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2193971
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2193967
    const-string v0, "group_info_settings"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2193964
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2193965
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;

    const-class v3, LX/F2s;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/F2s;

    invoke-static {v0}, LX/F2N;->b(LX/0QB;)LX/F2N;

    move-result-object v4

    check-cast v4, LX/F2N;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-static {v0}, LX/DZ8;->a(LX/0QB;)LX/DZ8;

    move-result-object v9

    check-cast v9, LX/DZ7;

    const/16 p1, 0x2390

    invoke-static {v0, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-static {v0}, LX/F4Q;->b(LX/0QB;)LX/F4Q;

    move-result-object v0

    check-cast v0, LX/F4Q;

    iput-object v3, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->a:LX/F2s;

    iput-object v4, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->b:LX/F2N;

    iput-object v5, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->c:Lcom/facebook/content/SecureContextHelper;

    iput-object v6, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->d:LX/0tX;

    iput-object v7, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->e:LX/1Ck;

    iput-object v8, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->f:Landroid/content/res/Resources;

    iput-object v9, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->g:LX/DZ7;

    iput-object p1, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->h:LX/0Ot;

    iput-object v0, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->i:LX/F4Q;

    .line 2193966
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2193932
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2193933
    const/16 v0, 0x7ac

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2193934
    :cond_0
    :goto_0
    return-void

    .line 2193935
    :cond_1
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 2193936
    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/DKH;

    .line 2193937
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2193938
    const-string v3, "group_feed_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2193939
    iget-object v3, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v0, v3

    .line 2193940
    invoke-virtual {v1, v2, v0}, LX/DKH;->a(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x74ff8dc4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2193962
    const v2, 0x7f0303f6

    move v1, v2

    .line 2193963
    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x44e2e144

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1b9ec7da

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2193954
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2193955
    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->e:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2193956
    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->k:LX/F2r;

    .line 2193957
    iget-object v2, v1, LX/F2r;->a:LX/F2L;

    iget-object v4, v1, LX/F2r;->r:LX/F2f;

    .line 2193958
    iget-object p0, v2, LX/F2L;->i:LX/F2f;

    if-ne p0, v4, :cond_0

    .line 2193959
    const/4 p0, 0x0

    iput-object p0, v2, LX/F2L;->i:LX/F2f;

    .line 2193960
    :cond_0
    iget-object v2, v1, LX/F2r;->a:LX/F2L;

    invoke-virtual {v2}, LX/F2L;->a()V

    .line 2193961
    const/16 v1, 0x2b

    const v2, -0x40c084f9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x23b558de

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2193951
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2193952
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->b()V

    .line 2193953
    const/16 v1, 0x2b

    const v2, 0x11af481c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2193941
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2193942
    iget-object v0, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->g:LX/DZ7;

    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->f:Landroid/content/res/Resources;

    const v2, 0x7f0831a4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, p0, v1, v2}, LX/DZ7;->a(Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;LX/DLO;)V

    .line 2193943
    const v0, 0x7f0d0c38

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    .line 2193944
    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->a:LX/F2s;

    new-instance v2, LX/F3E;

    invoke-direct {v2, p0}, LX/F3E;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;)V

    .line 2193945
    new-instance v3, LX/F2r;

    invoke-static {v1}, LX/F2L;->b(LX/0QB;)LX/F2L;

    move-result-object v5

    check-cast v5, LX/F2L;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v1}, LX/F2N;->b(LX/0QB;)LX/F2N;

    move-result-object v7

    check-cast v7, LX/F2N;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v1}, LX/11T;->a(LX/0QB;)LX/11T;

    move-result-object v9

    check-cast v9, LX/11T;

    invoke-static {v1}, LX/F4L;->b(LX/0QB;)LX/F4L;

    move-result-object v10

    check-cast v10, LX/F4L;

    invoke-static {v1}, LX/F4Q;->b(LX/0QB;)LX/F4Q;

    move-result-object v11

    check-cast v11, LX/F4Q;

    move-object v4, v2

    invoke-direct/range {v3 .. v11}, LX/F2r;-><init>(LX/F3E;LX/F2L;Landroid/content/res/Resources;LX/F2N;Lcom/facebook/content/SecureContextHelper;LX/11T;LX/F4L;LX/F4Q;)V

    .line 2193946
    move-object v1, v3

    .line 2193947
    iput-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->k:LX/F2r;

    .line 2193948
    iget-object v1, p0, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->k:LX/F2r;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2193949
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->b()V

    .line 2193950
    return-void
.end method
