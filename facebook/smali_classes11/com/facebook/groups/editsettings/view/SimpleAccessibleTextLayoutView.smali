.class public Lcom/facebook/groups/editsettings/view/SimpleAccessibleTextLayoutView;
.super LX/BtI;
.source ""


# instance fields
.field public a:LX/1nq;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2196915
    invoke-direct {p0, p1}, LX/BtI;-><init>(Landroid/content/Context;)V

    .line 2196916
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2196917
    invoke-direct {p0, p1, p2}, LX/BtI;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2196918
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 2196919
    iget-object v0, p0, Lcom/facebook/groups/editsettings/view/SimpleAccessibleTextLayoutView;->a:LX/1nq;

    if-nez v0, :cond_0

    .line 2196920
    invoke-super {p0, p1, p2}, LX/BtI;->onMeasure(II)V

    .line 2196921
    :goto_0
    return-void

    .line 2196922
    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/view/SimpleAccessibleTextLayoutView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/view/SimpleAccessibleTextLayoutView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 2196923
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/Layout;->getWidth()I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 2196924
    :cond_1
    iget-object v1, p0, Lcom/facebook/groups/editsettings/view/SimpleAccessibleTextLayoutView;->a:LX/1nq;

    invoke-virtual {v1, v0}, LX/1nq;->a(I)LX/1nq;

    move-result-object v1

    invoke-virtual {v1}, LX/1nq;->c()Landroid/text/Layout;

    move-result-object v1

    .line 2196925
    iput-object v1, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    .line 2196926
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/view/SimpleAccessibleTextLayoutView;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/view/SimpleAccessibleTextLayoutView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/view/SimpleAccessibleTextLayoutView;->getPaddingBottom()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/view/SimpleAccessibleTextLayoutView;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/groups/editsettings/view/SimpleAccessibleTextLayoutView;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public setLayoutBuilder(LX/1nq;)V
    .locals 0

    .prologue
    .line 2196927
    iput-object p1, p0, Lcom/facebook/groups/editsettings/view/SimpleAccessibleTextLayoutView;->a:LX/1nq;

    .line 2196928
    return-void
.end method
