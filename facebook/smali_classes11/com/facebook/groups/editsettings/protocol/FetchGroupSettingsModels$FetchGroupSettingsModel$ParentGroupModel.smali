.class public final Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3578b80
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2195428
    const-class v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2195384
    const-class v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2195425
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2195426
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2195419
    iput-object p1, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;->g:Ljava/lang/String;

    .line 2195420
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2195421
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2195422
    if-eqz v0, :cond_0

    .line 2195423
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 2195424
    :cond_0
    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2195417
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;->f:Ljava/lang/String;

    .line 2195418
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2195407
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2195408
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2195409
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2195410
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2195411
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2195412
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2195413
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2195414
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2195415
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2195416
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2195404
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2195405
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2195406
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2195403
    new-instance v0, LX/F3u;

    invoke-direct {v0, p1}, LX/F3u;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2195427
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2195397
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2195398
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2195399
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2195400
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 2195401
    :goto_0
    return-void

    .line 2195402
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2195394
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2195395
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;->a(Ljava/lang/String;)V

    .line 2195396
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2195391
    new-instance v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;

    invoke-direct {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;-><init>()V

    .line 2195392
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2195393
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2195390
    const v0, -0x325b3197    # -3.4562384E8f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2195389
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2195387
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 2195388
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2195385
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;->g:Ljava/lang/String;

    .line 2195386
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;->g:Ljava/lang/String;

    return-object v0
.end method
