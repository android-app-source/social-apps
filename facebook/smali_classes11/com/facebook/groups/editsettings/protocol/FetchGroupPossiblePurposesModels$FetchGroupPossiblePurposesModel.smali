.class public final Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6320ed60
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2195056
    const-class v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2195055
    const-class v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2195053
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2195054
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2195047
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2195048
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2195049
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2195050
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2195051
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2195052
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2195045
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel;->e:Ljava/util/List;

    .line 2195046
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2195057
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2195058
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2195059
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2195060
    if-eqz v1, :cond_0

    .line 2195061
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel;

    .line 2195062
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel;->e:Ljava/util/List;

    .line 2195063
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2195064
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2195044
    new-instance v0, LX/F3n;

    invoke-direct {v0, p1}, LX/F3n;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2195042
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2195043
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2195041
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2195036
    new-instance v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel;

    invoke-direct {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel;-><init>()V

    .line 2195037
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2195038
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2195040
    const v0, -0x235da6af

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2195039
    const v0, 0x41e065f

    return v0
.end method
