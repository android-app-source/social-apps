.class public final Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2195205
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2195206
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2195134
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2195135
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 2195136
    if-nez p1, :cond_0

    move v0, v1

    .line 2195137
    :goto_0
    return v0

    .line 2195138
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2195139
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2195140
    :sswitch_0
    const-class v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$CoverPhotoModel$PhotoModel;

    .line 2195141
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2195142
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2195143
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2195144
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2195145
    :sswitch_1
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2195146
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2195147
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 2195148
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2195149
    :sswitch_2
    const-class v0, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 2195150
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2195151
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2195152
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2195153
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2195154
    :sswitch_3
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2195155
    invoke-virtual {p0, p1, v5}, LX/15i;->p(II)I

    move-result v2

    .line 2195156
    const v3, 0x264ce754

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v2

    .line 2195157
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2195158
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 2195159
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2195160
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2195161
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2195162
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2195163
    invoke-virtual {p0, p1, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    move-result-object v2

    .line 2195164
    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 2195165
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2195166
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2195167
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2195168
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2195169
    :sswitch_5
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2195170
    invoke-virtual {p0, p1, v5}, LX/15i;->p(II)I

    move-result v2

    .line 2195171
    const v3, -0x184212d0

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v2

    .line 2195172
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2195173
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 2195174
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2195175
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2195176
    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2195177
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2195178
    invoke-virtual {p0, p1, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    move-result-object v2

    .line 2195179
    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 2195180
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2195181
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2195182
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2195183
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2195184
    :sswitch_7
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2195185
    const v2, 0x37a88ba0

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 2195186
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2195187
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2195188
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2195189
    :sswitch_8
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2195190
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2195191
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2195192
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2195193
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v3

    .line 2195194
    invoke-virtual {p3, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 2195195
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2195196
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2195197
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2195198
    invoke-virtual {p3, v6, v3}, LX/186;->b(II)V

    .line 2195199
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x31b8e1a6 -> :sswitch_3
        -0x2030d109 -> :sswitch_2
        -0x184212d0 -> :sswitch_6
        -0x166dc189 -> :sswitch_7
        -0x12e352ce -> :sswitch_0
        0x264ce754 -> :sswitch_4
        0x2b805edb -> :sswitch_1
        0x37a88ba0 -> :sswitch_8
        0x5bce65ca -> :sswitch_5
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2195200
    new-instance v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2195201
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 2195202
    if-eqz v0, :cond_0

    .line 2195203
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2195204
    :cond_0
    return-void
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2195224
    if-eqz p0, :cond_0

    .line 2195225
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2195226
    if-eq v0, p0, :cond_0

    .line 2195227
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2195228
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2195114
    sparse-switch p2, :sswitch_data_0

    .line 2195115
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2195116
    :sswitch_0
    const-class v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$CoverPhotoModel$PhotoModel;

    .line 2195117
    invoke-static {v0, p3}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 2195118
    :goto_0
    :sswitch_1
    return-void

    .line 2195119
    :sswitch_2
    const-class v0, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 2195120
    invoke-static {v0, p3}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    .line 2195121
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2195122
    const v1, 0x264ce754

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2195123
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2195124
    const v1, -0x184212d0

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2195125
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2195126
    const v1, 0x37a88ba0

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x31b8e1a6 -> :sswitch_3
        -0x2030d109 -> :sswitch_2
        -0x184212d0 -> :sswitch_1
        -0x166dc189 -> :sswitch_5
        -0x12e352ce -> :sswitch_0
        0x264ce754 -> :sswitch_1
        0x2b805edb -> :sswitch_1
        0x37a88ba0 -> :sswitch_1
        0x5bce65ca -> :sswitch_4
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2195207
    if-nez p1, :cond_0

    move v0, v1

    .line 2195208
    :goto_0
    return v0

    .line 2195209
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 2195210
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2195211
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2195212
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 2195213
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 2195214
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2195215
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2195216
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 2195217
    if-eqz p1, :cond_0

    .line 2195218
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 2195219
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2195220
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 2195221
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2195222
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2195223
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2195127
    if-eqz p1, :cond_0

    .line 2195128
    invoke-static {p0, p1, p2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;

    move-result-object v1

    .line 2195129
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;

    .line 2195130
    if-eq v0, v1, :cond_0

    .line 2195131
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2195132
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2195133
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2195081
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2195082
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2195083
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2195084
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2195085
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->a:LX/15i;

    .line 2195086
    iput p2, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->b:I

    .line 2195087
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2195088
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2195089
    new-instance v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2195090
    iget v0, p0, LX/1vt;->c:I

    .line 2195091
    move v0, v0

    .line 2195092
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2195093
    iget v0, p0, LX/1vt;->c:I

    .line 2195094
    move v0, v0

    .line 2195095
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2195096
    iget v0, p0, LX/1vt;->b:I

    .line 2195097
    move v0, v0

    .line 2195098
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2195099
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2195100
    move-object v0, v0

    .line 2195101
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2195102
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2195103
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2195104
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2195105
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2195106
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2195107
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2195108
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2195109
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2195110
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2195111
    iget v0, p0, LX/1vt;->c:I

    .line 2195112
    move v0, v0

    .line 2195113
    return v0
.end method
