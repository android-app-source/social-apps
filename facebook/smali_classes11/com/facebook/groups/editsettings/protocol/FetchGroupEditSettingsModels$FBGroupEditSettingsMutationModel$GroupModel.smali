.class public final Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1a717cd4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel$GroupTopicTagsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Z

.field private m:Z

.field private n:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2194709
    const-class v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2194710
    const-class v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2194711
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2194712
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;)V
    .locals 4

    .prologue
    .line 2194693
    iput-object p1, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->i:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    .line 2194694
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2194695
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2194696
    if-eqz v0, :cond_0

    .line 2194697
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x4

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2194698
    :cond_0
    return-void

    .line 2194699
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V
    .locals 4

    .prologue
    .line 2194713
    iput-object p1, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->n:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2194714
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2194715
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2194716
    if-eqz v0, :cond_0

    .line 2194717
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x9

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2194718
    :cond_0
    return-void

    .line 2194719
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;)V
    .locals 4

    .prologue
    .line 2194720
    iput-object p1, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    .line 2194721
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2194722
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2194723
    if-eqz v0, :cond_0

    .line 2194724
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x6

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2194725
    :cond_0
    return-void

    .line 2194726
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V
    .locals 4

    .prologue
    .line 2194727
    iput-object p1, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->p:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 2194728
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2194729
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2194730
    if-eqz v0, :cond_0

    .line 2194731
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0xb

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2194732
    :cond_0
    return-void

    .line 2194733
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2194734
    iput-object p1, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->f:Ljava/lang/String;

    .line 2194735
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2194736
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2194737
    if-eqz v0, :cond_0

    .line 2194738
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 2194739
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 2194760
    iput-boolean p1, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->l:Z

    .line 2194761
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2194762
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2194763
    if-eqz v0, :cond_0

    .line 2194764
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 2194765
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2194740
    iput-object p1, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->j:Ljava/lang/String;

    .line 2194741
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2194742
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2194743
    if-eqz v0, :cond_0

    .line 2194744
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 2194745
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 2194746
    iput-boolean p1, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->m:Z

    .line 2194747
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2194748
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2194749
    if-eqz v0, :cond_0

    .line 2194750
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 2194751
    :cond_0
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2194752
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->f:Ljava/lang/String;

    .line 2194753
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel$GroupTopicTagsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2194754
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel$GroupTopicTagsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->g:Ljava/util/List;

    .line 2194755
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2194756
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->h:Ljava/lang/String;

    .line 2194757
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2194758
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->i:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->i:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    .line 2194759
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->i:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2194705
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->j:Ljava/lang/String;

    .line 2194706
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2194707
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    .line 2194708
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    return-object v0
.end method

.method private p()Z
    .locals 2

    .prologue
    .line 2194580
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2194581
    iget-boolean v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->l:Z

    return v0
.end method

.method private q()Z
    .locals 2

    .prologue
    .line 2194582
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2194583
    iget-boolean v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->m:Z

    return v0
.end method

.method private r()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2194584
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->n:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->n:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2194585
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->n:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2194586
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->o:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->o:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 2194587
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->o:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2194588
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->p:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->p:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 2194589
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->p:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method

.method private u()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getVisibilitySentence"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2194590
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2194591
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->q:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    .line 2194592
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2194593
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2194594
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->k()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 2194595
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2194596
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->m()Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 2194597
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2194598
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->o()Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 2194599
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 2194600
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->s()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 2194601
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->t()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 2194602
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->u()LX/1vs;

    move-result-object v9

    iget-object v10, v9, LX/1vs;->a:LX/15i;

    iget v9, v9, LX/1vs;->b:I

    const v11, 0x371012a0

    invoke-static {v10, v9, v11}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$DraculaImplementation;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2194603
    const/16 v10, 0xd

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2194604
    const/4 v10, 0x0

    iget-boolean v11, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->e:Z

    invoke-virtual {p1, v10, v11}, LX/186;->a(IZ)V

    .line 2194605
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 2194606
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2194607
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2194608
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2194609
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2194610
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2194611
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2194612
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->m:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2194613
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2194614
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2194615
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2194616
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2194617
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2194618
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2194619
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2194620
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2194621
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2194622
    if-eqz v1, :cond_2

    .line 2194623
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;

    .line 2194624
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->g:Ljava/util/List;

    move-object v1, v0

    .line 2194625
    :goto_0
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->u()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2194626
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->u()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x371012a0

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2194627
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->u()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2194628
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;

    .line 2194629
    iput v3, v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->q:I

    move-object v1, v0

    .line 2194630
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2194631
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    .line 2194632
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move-object p0, v1

    .line 2194633
    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2194634
    new-instance v0, LX/F3f;

    invoke-direct {v0, p1}, LX/F3f;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2194635
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2194636
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2194637
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->e:Z

    .line 2194638
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->l:Z

    .line 2194639
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->m:Z

    .line 2194640
    const/16 v0, 0xc

    const v1, 0x371012a0

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->q:I

    .line 2194641
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2194642
    const-string v0, "description"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2194643
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2194644
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2194645
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 2194646
    :goto_0
    return-void

    .line 2194647
    :cond_0
    const-string v0, "join_approval_setting"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2194648
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->m()Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2194649
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2194650
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2194651
    :cond_1
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2194652
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2194653
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2194654
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2194655
    :cond_2
    const-string v0, "post_permission_setting"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2194656
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->o()Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2194657
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2194658
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2194659
    :cond_3
    const-string v0, "requires_admin_membership_approval"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2194660
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->p()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2194661
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2194662
    const/4 v0, 0x7

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2194663
    :cond_4
    const-string v0, "requires_post_approval"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2194664
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->q()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2194665
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2194666
    const/16 v0, 0x8

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 2194667
    :cond_5
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2194668
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2194669
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2194670
    const/16 v0, 0x9

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 2194671
    :cond_6
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2194672
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->t()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2194673
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2194674
    const/16 v0, 0xb

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 2194675
    :cond_7
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2194676
    const-string v0, "description"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2194677
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->a(Ljava/lang/String;)V

    .line 2194678
    :cond_0
    :goto_0
    return-void

    .line 2194679
    :cond_1
    const-string v0, "join_approval_setting"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2194680
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    invoke-direct {p0, p2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;)V

    goto :goto_0

    .line 2194681
    :cond_2
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2194682
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 2194683
    :cond_3
    const-string v0, "post_permission_setting"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2194684
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    invoke-direct {p0, p2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;)V

    goto :goto_0

    .line 2194685
    :cond_4
    const-string v0, "requires_admin_membership_approval"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2194686
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->a(Z)V

    goto :goto_0

    .line 2194687
    :cond_5
    const-string v0, "requires_post_approval"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2194688
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->b(Z)V

    goto :goto_0

    .line 2194689
    :cond_6
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2194690
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-direct {p0, p2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V

    goto :goto_0

    .line 2194691
    :cond_7
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2194692
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-direct {p0, p2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2194700
    new-instance v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;

    invoke-direct {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;-><init>()V

    .line 2194701
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2194702
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2194703
    const v0, 0x76881a5

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2194704
    const v0, 0x41e065f

    return v0
.end method
