.class public final Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel$GroupTopicTagsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x46ac8a11
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel$GroupTopicTagsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel$GroupTopicTagsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2194292
    const-class v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel$GroupTopicTagsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2194276
    const-class v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel$GroupTopicTagsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2194290
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2194291
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2194282
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2194283
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel$GroupTopicTagsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2194284
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel$GroupTopicTagsModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2194285
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2194286
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2194287
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2194288
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2194289
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2194279
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2194280
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2194281
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2194278
    new-instance v0, LX/F3X;

    invoke-direct {v0, p1}, LX/F3X;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2194277
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel$GroupTopicTagsModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2194293
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2194294
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2194269
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2194266
    new-instance v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel$GroupTopicTagsModel;

    invoke-direct {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel$GroupTopicTagsModel;-><init>()V

    .line 2194267
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2194268
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2194270
    const v0, 0x3306dc86

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2194271
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2194272
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel$GroupTopicTagsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel$GroupTopicTagsModel;->e:Ljava/lang/String;

    .line 2194273
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel$GroupTopicTagsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2194274
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel$GroupTopicTagsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel$GroupTopicTagsModel;->f:Ljava/lang/String;

    .line 2194275
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel$GroupTopicTagsModel;->f:Ljava/lang/String;

    return-object v0
.end method
