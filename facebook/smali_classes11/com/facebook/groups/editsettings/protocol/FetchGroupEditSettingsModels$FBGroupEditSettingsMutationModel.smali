.class public final Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x129bd99d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2194803
    const-class v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2194802
    const-class v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2194800
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2194801
    return-void
.end method

.method private a()Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroup"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2194779
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel;->e:Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;

    iput-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel;->e:Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;

    .line 2194780
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel;->e:Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2194794
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2194795
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel;->a()Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2194796
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2194797
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2194798
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2194799
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2194786
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2194787
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel;->a()Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2194788
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel;->a()Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;

    .line 2194789
    invoke-direct {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel;->a()Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2194790
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel;

    .line 2194791
    iput-object v0, v1, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel;->e:Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel$GroupModel;

    .line 2194792
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2194793
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2194783
    new-instance v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel;

    invoke-direct {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel;-><init>()V

    .line 2194784
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2194785
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2194782
    const v0, 0x43851522    # 266.1651f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2194781
    const v0, -0x681df23c

    return v0
.end method
