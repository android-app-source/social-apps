.class public final Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xfe46e79
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2196391
    const-class v0, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2196392
    const-class v0, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2196393
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2196394
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2196370
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2196371
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2196372
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2196373
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2196374
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2196375
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEdges"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2196389
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;->e:Ljava/util/List;

    .line 2196390
    iget-object v0, p0, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2196381
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2196382
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2196383
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2196384
    if-eqz v1, :cond_0

    .line 2196385
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;

    .line 2196386
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;->e:Ljava/util/List;

    .line 2196387
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2196388
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2196378
    new-instance v0, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;

    invoke-direct {v0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;-><init>()V

    .line 2196379
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2196380
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2196377
    const v0, 0x3fa75456

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2196376
    const v0, 0x3b3e41d2

    return v0
.end method
