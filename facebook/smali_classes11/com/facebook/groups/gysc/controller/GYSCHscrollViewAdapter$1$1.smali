.class public final Lcom/facebook/groups/gysc/controller/GYSCHscrollViewAdapter$1$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/DKN;

.field public final synthetic b:LX/F5i;


# direct methods
.method public constructor <init>(LX/F5i;LX/DKN;)V
    .locals 0

    .prologue
    .line 2199123
    iput-object p1, p0, Lcom/facebook/groups/gysc/controller/GYSCHscrollViewAdapter$1$1;->b:LX/F5i;

    iput-object p2, p0, Lcom/facebook/groups/gysc/controller/GYSCHscrollViewAdapter$1$1;->a:LX/DKN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2199124
    iget-object v0, p0, Lcom/facebook/groups/gysc/controller/GYSCHscrollViewAdapter$1$1;->b:LX/F5i;

    iget-object v0, v0, LX/F5i;->a:LX/F5k;

    iget-object v1, p0, Lcom/facebook/groups/gysc/controller/GYSCHscrollViewAdapter$1$1;->a:LX/DKN;

    iget-object v1, v1, LX/DKN;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/groups/gysc/controller/GYSCHscrollViewAdapter$1$1;->a:LX/DKN;

    iget-object v2, v2, LX/DKN;->c:Ljava/lang/String;

    .line 2199125
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 2199126
    :cond_0
    :goto_0
    return-void

    .line 2199127
    :cond_1
    const/4 v3, 0x0

    move v4, v3

    :goto_1
    iget-object v3, v0, LX/F5k;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v4, v3, :cond_0

    .line 2199128
    iget-object v3, v0, LX/F5k;->d:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/F5o;

    .line 2199129
    iget-object v5, v3, LX/F5o;->b:Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;

    move-object v3, v5

    .line 2199130
    if-eqz v3, :cond_2

    .line 2199131
    iget-object v3, v0, LX/F5k;->d:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/F5o;

    .line 2199132
    iget-object v5, v3, LX/F5o;->b:Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;

    move-object v3, v5

    .line 2199133
    invoke-virtual {v3}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;->m()Ljava/lang/String;

    move-result-object v5

    .line 2199134
    iget-object v3, v0, LX/F5k;->d:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/F5o;

    .line 2199135
    iget-object p0, v3, LX/F5o;->b:Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;

    move-object v3, p0

    .line 2199136
    invoke-virtual {v3}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;->n()Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->name()Ljava/lang/String;

    move-result-object v3

    .line 2199137
    if-eqz v5, :cond_2

    if-eqz v3, :cond_2

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2199138
    iget-object v3, v0, LX/F5k;->d:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/F5o;

    .line 2199139
    const/4 v5, 0x1

    iput-boolean v5, v3, LX/F5o;->a:Z

    .line 2199140
    invoke-virtual {v0, v4}, LX/1OM;->i_(I)V

    goto :goto_0

    .line 2199141
    :cond_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1
.end method
