.class public Lcom/facebook/groups/gysc/GYSCRowView;
.super Lcom/facebook/widget/recyclerview/BetterRecyclerView;
.source ""


# instance fields
.field private i:LX/1P1;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2199037
    invoke-direct {p0, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;)V

    .line 2199038
    invoke-direct {p0}, Lcom/facebook/groups/gysc/GYSCRowView;->o()V

    .line 2199039
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2199040
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2199041
    invoke-direct {p0}, Lcom/facebook/groups/gysc/GYSCRowView;->o()V

    .line 2199042
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2199043
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2199044
    invoke-direct {p0}, Lcom/facebook/groups/gysc/GYSCRowView;->o()V

    .line 2199045
    return-void
.end method

.method private o()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2199046
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, Lcom/facebook/groups/gysc/GYSCRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P1;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/groups/gysc/GYSCRowView;->i:LX/1P1;

    .line 2199047
    iget-object v0, p0, Lcom/facebook/groups/gysc/GYSCRowView;->i:LX/1P1;

    invoke-virtual {v0, v2}, LX/1P1;->b(I)V

    .line 2199048
    iget-object v0, p0, Lcom/facebook/groups/gysc/GYSCRowView;->i:LX/1P1;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2199049
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    move-object v0, v0

    .line 2199050
    iput-boolean v2, v0, LX/1Of;->g:Z

    .line 2199051
    return-void
.end method
