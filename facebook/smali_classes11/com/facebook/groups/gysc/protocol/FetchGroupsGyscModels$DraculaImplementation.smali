.class public final Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2199281
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2199282
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2199251
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2199252
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 2199253
    if-nez p1, :cond_0

    .line 2199254
    :goto_0
    return v0

    .line 2199255
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2199256
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2199257
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2199258
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2199259
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2199260
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2199261
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2199262
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2199263
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2199264
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2199265
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2199266
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2199267
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2199268
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2199269
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2199270
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2199271
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2199272
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2199273
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2199274
    invoke-virtual {p0, p1, v4}, LX/15i;->b(II)Z

    move-result v2

    .line 2199275
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2199276
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2199277
    invoke-virtual {p3, v4, v2}, LX/186;->a(IZ)V

    .line 2199278
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x3598d6c1 -> :sswitch_0
        -0x2b3cc39d -> :sswitch_3
        0x29d9dc70 -> :sswitch_2
        0x7a673e8b -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2199279
    new-instance v0, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2199283
    sparse-switch p0, :sswitch_data_0

    .line 2199284
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2199285
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x3598d6c1 -> :sswitch_0
        -0x2b3cc39d -> :sswitch_0
        0x29d9dc70 -> :sswitch_0
        0x7a673e8b -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2199280
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2199244
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$DraculaImplementation;->b(I)V

    .line 2199245
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2199246
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2199247
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2199248
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$DraculaImplementation;->a:LX/15i;

    .line 2199249
    iput p2, p0, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$DraculaImplementation;->b:I

    .line 2199250
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2199243
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2199242
    new-instance v0, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2199218
    iget v0, p0, LX/1vt;->c:I

    .line 2199219
    move v0, v0

    .line 2199220
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2199239
    iget v0, p0, LX/1vt;->c:I

    .line 2199240
    move v0, v0

    .line 2199241
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2199236
    iget v0, p0, LX/1vt;->b:I

    .line 2199237
    move v0, v0

    .line 2199238
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2199233
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2199234
    move-object v0, v0

    .line 2199235
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2199224
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2199225
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2199226
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2199227
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2199228
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2199229
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2199230
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2199231
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2199232
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2199221
    iget v0, p0, LX/1vt;->c:I

    .line 2199222
    move v0, v0

    .line 2199223
    return v0
.end method
