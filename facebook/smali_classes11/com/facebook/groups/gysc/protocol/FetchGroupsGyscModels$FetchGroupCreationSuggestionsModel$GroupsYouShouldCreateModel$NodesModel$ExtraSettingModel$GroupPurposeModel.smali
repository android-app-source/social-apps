.class public final Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3c1eb32
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2199396
    const-class v0, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2199395
    const-class v0, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2199393
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2199394
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2199383
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2199384
    invoke-virtual {p0}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2199385
    invoke-virtual {p0}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2199386
    invoke-virtual {p0}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2199387
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2199388
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2199389
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2199390
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2199391
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2199392
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2199380
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2199381
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2199382
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2199397
    iget-object v0, p0, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel;->e:Ljava/lang/String;

    .line 2199398
    iget-object v0, p0, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2199377
    new-instance v0, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel;

    invoke-direct {v0}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel;-><init>()V

    .line 2199378
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2199379
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2199376
    const v0, -0xd6a0231

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2199375
    const v0, -0x182c8a41

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2199373
    iget-object v0, p0, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    iput-object v0, p0, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 2199374
    iget-object v0, p0, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2199371
    iget-object v0, p0, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel;->g:Ljava/lang/String;

    .line 2199372
    iget-object v0, p0, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel;->g:Ljava/lang/String;

    return-object v0
.end method
