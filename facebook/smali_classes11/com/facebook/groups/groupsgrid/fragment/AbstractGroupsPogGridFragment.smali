.class public abstract Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# static fields
.field public static final m:Ljava/lang/String;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/tablet/IsTablet;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/00H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0gy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Glj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Landroid/support/v7/widget/RecyclerView;

.field public i:LX/Glh;

.field public j:LX/Glk;

.field public k:Landroid/view/View;

.field public l:LX/GlX;

.field private n:Landroid/view/View;

.field public o:LX/Gkj;

.field private p:LX/GkK;

.field private q:LX/Gli;

.field private final r:Landroid/view/View$OnClickListener;

.field public final s:LX/Gld;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2389333
    const-class v0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2389329
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2389330
    new-instance v0, LX/Glc;

    invoke-direct {v0, p0}, LX/Glc;-><init>(Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->r:Landroid/view/View$OnClickListener;

    .line 2389331
    new-instance v0, LX/Gld;

    invoke-direct {v0, p0}, LX/Gld;-><init>(Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->s:LX/Gld;

    .line 2389332
    return-void
.end method

.method public static q(Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;)LX/3wu;
    .locals 4

    .prologue
    .line 2389321
    new-instance v0, LX/3wu;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->j:LX/Glk;

    .line 2389322
    iget v3, v2, LX/Glk;->b:I

    move v2, v3

    .line 2389323
    invoke-direct {v0, v1, v2}, LX/3wu;-><init>(Landroid/content/Context;I)V

    .line 2389324
    new-instance v1, LX/GlN;

    iget-object v2, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->j:LX/Glk;

    .line 2389325
    iget v3, v2, LX/Glk;->b:I

    move v2, v3

    .line 2389326
    iget-object v3, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->l:LX/GlX;

    invoke-direct {v1, v2, v3}, LX/GlN;-><init>(ILX/1OM;)V

    .line 2389327
    iput-object v1, v0, LX/3wu;->h:LX/3wr;

    .line 2389328
    return-object v0
.end method

.method public static r(Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;)I
    .locals 2

    .prologue
    .line 2389312
    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2389313
    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->j:LX/Glk;

    .line 2389314
    iget v1, v0, LX/Glk;->b:I

    move v0, v1

    .line 2389315
    iget-object v1, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->j:LX/Glk;

    .line 2389316
    iget p0, v1, LX/Glk;->c:I

    move v1, p0

    .line 2389317
    add-int/lit8 v1, v1, 0x2

    mul-int/2addr v0, v1

    .line 2389318
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->j:LX/Glk;

    .line 2389319
    iget v1, v0, LX/Glk;->b:I

    iget p0, v0, LX/Glk;->c:I

    mul-int/2addr v1, p0

    move v0, v1

    .line 2389320
    mul-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method


# virtual methods
.method public abstract a(LX/Glk;LX/Gld;)LX/GlX;
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2389311
    const-string v0, "groups_grid"

    return-object v0
.end method

.method public final a(LX/Glh;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 2389282
    iput-object p1, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->i:LX/Glh;

    .line 2389283
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2389284
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->h:Landroid/support/v7/widget/RecyclerView;

    if-nez v0, :cond_1

    .line 2389285
    :cond_0
    :goto_0
    return-void

    .line 2389286
    :cond_1
    iget-object v1, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->h:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->i:LX/Glh;

    sget-object v4, LX/Glh;->LOADED:LX/Glh;

    if-ne v0, v4, :cond_5

    move v0, v2

    :goto_1
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2389287
    const v0, 0x7f0d15b5

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->i:LX/Glh;

    sget-object v4, LX/Glh;->LOADING:LX/Glh;

    if-ne v0, v4, :cond_6

    move v0, v2

    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2389288
    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->i:LX/Glh;

    sget-object v1, LX/Glh;->EMPTY:LX/Glh;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->k:Landroid/view/View;

    if-nez v0, :cond_2

    .line 2389289
    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 2389290
    const v4, 0x7f030867

    move v4, v4

    .line 2389291
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2389292
    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v0, v4, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2389293
    const v1, 0x7f0d15e0

    move v0, v1

    .line 2389294
    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->k:Landroid/view/View;

    .line 2389295
    :cond_2
    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->k:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 2389296
    iget-object v1, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->k:Landroid/view/View;

    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->i:LX/Glh;

    sget-object v4, LX/Glh;->EMPTY:LX/Glh;

    if-ne v0, v4, :cond_7

    move v0, v2

    :goto_3
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2389297
    :cond_3
    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->i:LX/Glh;

    sget-object v1, LX/Glh;->FAILED:LX/Glh;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->n:Landroid/view/View;

    if-nez v0, :cond_4

    .line 2389298
    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 2389299
    const v4, 0x7f030868

    move v4, v4

    .line 2389300
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2389301
    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v0, v4, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2389302
    const v1, 0x7f0d15e1

    move v0, v1

    .line 2389303
    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->n:Landroid/view/View;

    .line 2389304
    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->n:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->r:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2389305
    :cond_4
    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->n:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2389306
    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->n:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->i:LX/Glh;

    sget-object v4, LX/Glh;->FAILED:LX/Glh;

    if-ne v1, v4, :cond_8

    :goto_4
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_5
    move v0, v3

    .line 2389307
    goto/16 :goto_1

    :cond_6
    move v0, v3

    .line 2389308
    goto :goto_2

    :cond_7
    move v0, v3

    .line 2389309
    goto :goto_3

    :cond_8
    move v2, v3

    .line 2389310
    goto :goto_4
.end method

.method public abstract a(LX/Glh;ZI)V
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2389279
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2389280
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;

    const/16 v3, 0x259

    invoke-static {v9, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v9}, LX/0rr;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    const-class v5, LX/00H;

    invoke-interface {v9, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/00H;

    invoke-static {v9}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v9}, LX/0gy;->a(LX/0QB;)LX/0gy;

    move-result-object v7

    check-cast v7, LX/0gy;

    const/16 v8, 0x3c

    invoke-static {v9, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    new-instance v1, LX/Glj;

    const-class p1, Landroid/content/Context;

    invoke-interface {v9, p1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    invoke-static {v9}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {v9}, LX/0kv;->a(LX/0QB;)LX/0kv;

    move-result-object p0

    check-cast p0, LX/0kv;

    invoke-direct {v1, p1, v0, p0}, LX/Glj;-><init>(Landroid/content/Context;LX/0Zb;LX/0kv;)V

    move-object v9, v1

    check-cast v9, LX/Glj;

    iput-object v3, v2, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->a:LX/0Ot;

    iput-object v4, v2, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->b:Ljava/lang/Boolean;

    iput-object v5, v2, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->c:LX/00H;

    iput-object v6, v2, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->d:Landroid/content/res/Resources;

    iput-object v7, v2, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->e:LX/0gy;

    iput-object v8, v2, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->f:LX/0Ot;

    iput-object v9, v2, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->g:LX/Glj;

    .line 2389281
    return-void
.end method

.method public abstract b()LX/1OZ;
.end method

.method public abstract c()LX/1Oa;
.end method

.method public abstract d()LX/0fu;
.end method

.method public abstract e()LX/Gkk;
.end method

.method public abstract k()LX/Gkj;
.end method

.method public final l()V
    .locals 3

    .prologue
    .line 2389267
    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->c:LX/00H;

    .line 2389268
    iget-object v1, v0, LX/00H;->j:LX/01T;

    move-object v0, v1

    .line 2389269
    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_0

    .line 2389270
    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->e:LX/0gy;

    sget-object v1, LX/0cQ;->GROUPS_TAB_FRAGMENT:LX/0cQ;

    invoke-virtual {v0, v1}, LX/0gy;->b(LX/0cQ;)LX/0gz;

    move-result-object v0

    .line 2389271
    iget v1, v0, LX/0gz;->a:I

    move v1, v1

    .line 2389272
    iget v2, v0, LX/0gz;->c:I

    move v2, v2

    .line 2389273
    sub-int/2addr v1, v2

    .line 2389274
    iget v2, v0, LX/0gz;->d:I

    move v0, v2

    .line 2389275
    sub-int v0, v1, v0

    .line 2389276
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->d:Landroid/content/res/Resources;

    invoke-static {v0, v1, v2}, LX/Glk;->a(Ljava/lang/Integer;Landroid/content/Context;Landroid/content/res/Resources;)LX/Glk;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->j:LX/Glk;

    .line 2389277
    :goto_0
    return-void

    .line 2389278
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->d:Landroid/content/res/Resources;

    invoke-static {v0, v1, v2}, LX/Glk;->a(Ljava/lang/Integer;Landroid/content/Context;Landroid/content/res/Resources;)LX/Glk;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->j:LX/Glk;

    goto :goto_0
.end method

.method public final n()LX/GkK;
    .locals 1

    .prologue
    .line 2389264
    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->p:LX/GkK;

    if-nez v0, :cond_0

    .line 2389265
    new-instance v0, LX/Glf;

    invoke-direct {v0, p0}, LX/Glf;-><init>(Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->p:LX/GkK;

    .line 2389266
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->p:LX/GkK;

    return-object v0
.end method

.method public final o()V
    .locals 4

    .prologue
    .line 2389262
    invoke-virtual {p0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->e()LX/Gkk;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->l:LX/GlX;

    invoke-virtual {v1}, LX/GlX;->d()LX/Gkm;

    move-result-object v1

    invoke-static {p0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->r(Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;)I

    move-result v2

    new-instance v3, LX/Glg;

    invoke-direct {v3, p0}, LX/Glg;-><init>(Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;)V

    invoke-interface {v0, v1, v2, v3}, LX/Gkk;->a(LX/Gkm;ILX/GkH;)V

    .line 2389263
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0xe44c949

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2389261
    const v1, 0x7f030853

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x13b0ca2

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x8454b71

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2389258
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2389259
    iget-object v1, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->q:LX/Gli;

    invoke-virtual {v1}, LX/Gli;->disable()V

    .line 2389260
    const/16 v1, 0x2b

    const v2, 0x742c86b2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x733d94ff

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2389255
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2389256
    iget-object v1, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->q:LX/Gli;

    invoke-virtual {v1}, LX/Gli;->enable()V

    .line 2389257
    const/16 v1, 0x2b

    const v2, -0x423b22f2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2389251
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2389252
    invoke-virtual {p0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->k()LX/Gkj;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->o:LX/Gkj;

    .line 2389253
    new-instance v0, LX/Gli;

    iget-object v1, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->d:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {v0, p0, v1}, LX/Gli;-><init>(Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;I)V

    iput-object v0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->q:LX/Gli;

    .line 2389254
    return-void
.end method
