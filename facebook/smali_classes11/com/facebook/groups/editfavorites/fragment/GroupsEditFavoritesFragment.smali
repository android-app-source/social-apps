.class public Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# static fields
.field public static final f:Ljava/lang/String;


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Gk7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Gkk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DZ7;
    .annotation runtime Lcom/facebook/groups/editfavorites/annotation/GroupsEditFavoritesNavigationHandler;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/GkD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:Lcom/facebook/groups/editfavorites/view/GroupsEditFavoritesDragSortListView;

.field public h:LX/GkA;

.field public i:LX/GkK;

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2388978
    const-class v0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2389012
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    new-instance v4, LX/Gk7;

    new-instance p1, LX/GkA;

    new-instance v5, LX/Gk6;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {v5, v3}, LX/Gk6;-><init>(Landroid/content/res/Resources;)V

    move-object v3, v5

    check-cast v3, LX/Gk6;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {p1, v3, v5}, LX/GkA;-><init>(LX/Gk6;Landroid/content/res/Resources;)V

    move-object v3, p1

    check-cast v3, LX/GkA;

    invoke-direct {v4, v3}, LX/Gk7;-><init>(LX/GkA;)V

    move-object v3, v4

    check-cast v3, LX/Gk7;

    invoke-static {p0}, LX/Gl6;->b(LX/0QB;)LX/Gl6;

    move-result-object v4

    check-cast v4, LX/Gkk;

    invoke-static {p0}, LX/GkN;->a(LX/0QB;)LX/GkN;

    move-result-object v5

    check-cast v5, LX/DZ7;

    invoke-static {p0}, LX/GkD;->a(LX/0QB;)LX/GkD;

    move-result-object p0

    check-cast p0, LX/GkD;

    iput-object v2, v1, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->a:Landroid/content/res/Resources;

    iput-object v3, v1, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->b:LX/Gk7;

    iput-object v4, v1, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->c:LX/Gkk;

    iput-object v5, v1, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->d:LX/DZ7;

    iput-object p0, v1, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->e:LX/GkD;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2389011
    const-string v0, "group_edit_favorites"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2389007
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2389008
    const-class v0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;

    invoke-static {v0, p0}, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2389009
    iget-object v0, p0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->c:LX/Gkk;

    sget-object v1, LX/Gkn;->FAVORITES_SECTION:LX/Gkn;

    sget-object v2, LX/Gkn;->FILTERED_GROUPS_SECTION:LX/Gkn;

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/Gkk;->a(LX/0Px;)V

    .line 2389010
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x7f478ed9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2389003
    const v0, 0x7f03083d

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2389004
    const v0, 0x7f0d1583

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editfavorites/view/GroupsEditFavoritesDragSortListView;

    iput-object v0, p0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->g:Lcom/facebook/groups/editfavorites/view/GroupsEditFavoritesDragSortListView;

    .line 2389005
    iget-object v0, p0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->g:Lcom/facebook/groups/editfavorites/view/GroupsEditFavoritesDragSortListView;

    new-instance v3, LX/GkF;

    invoke-direct {v3, p0}, LX/GkF;-><init>(Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/groups/editfavorites/view/GroupsEditFavoritesDragSortListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2389006
    const/16 v0, 0x2b

    const v3, 0x63c166d8

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x6f207ecf

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2388997
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2388998
    iget-object v1, p0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->c:LX/Gkk;

    invoke-interface {v1}, LX/Gkk;->a()V

    .line 2388999
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->c:LX/Gkk;

    .line 2389000
    iget-boolean v1, p0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->j:Z

    if-eqz v1, :cond_0

    .line 2389001
    iget-object v1, p0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->e:LX/GkD;

    new-instance v2, LX/GkC;

    sget-object v3, LX/GkB;->EDIT_DONE:LX/GkB;

    invoke-direct {v2, v3}, LX/GkC;-><init>(LX/GkB;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2389002
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x5eac4cfe

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5f6a7ff5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2388993
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2388994
    iget-object v1, p0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->g:Lcom/facebook/groups/editfavorites/view/GroupsEditFavoritesDragSortListView;

    invoke-virtual {v1, v2}, Lcom/facebook/groups/editfavorites/view/GroupsEditFavoritesDragSortListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2388995
    iput-object v2, p0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->g:Lcom/facebook/groups/editfavorites/view/GroupsEditFavoritesDragSortListView;

    .line 2388996
    const/16 v1, 0x2b

    const v2, 0x20df21a4    # 3.779993E-19f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2388979
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2388980
    iget-object v0, p0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->d:LX/DZ7;

    iget-object v1, p0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->a:Landroid/content/res/Resources;

    const v2, 0x7f083739

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, p0, v1, v2}, LX/DZ7;->a(Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;LX/DLO;)V

    .line 2388981
    const v0, 0x7f0d1583

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editfavorites/view/GroupsEditFavoritesDragSortListView;

    iput-object v0, p0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->g:Lcom/facebook/groups/editfavorites/view/GroupsEditFavoritesDragSortListView;

    .line 2388982
    iget-object v0, p0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->b:LX/Gk7;

    new-instance v1, LX/GkG;

    invoke-direct {v1, p0}, LX/GkG;-><init>(Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;)V

    .line 2388983
    iget-object v2, v0, LX/Gk7;->a:LX/GkA;

    .line 2388984
    iput-object v1, v2, LX/GkA;->d:LX/GkG;

    .line 2388985
    iget-object v2, v0, LX/Gk7;->a:LX/GkA;

    move-object v0, v2

    .line 2388986
    iput-object v0, p0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->h:LX/GkA;

    .line 2388987
    iget-object v0, p0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->c:LX/Gkk;

    sget-object v1, LX/Gkm;->ALPHABETICAL:LX/Gkm;

    const/16 v2, 0xa

    new-instance v3, LX/GkI;

    invoke-direct {v3, p0}, LX/GkI;-><init>(Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;)V

    invoke-interface {v0, v1, v2, v3}, LX/Gkk;->a(LX/Gkm;ILX/GkH;)V

    .line 2388988
    iget-object v0, p0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->g:Lcom/facebook/groups/editfavorites/view/GroupsEditFavoritesDragSortListView;

    iget-object v1, p0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->h:LX/GkA;

    invoke-virtual {v0, v1}, Lcom/facebook/groups/editfavorites/view/GroupsEditFavoritesDragSortListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2388989
    iget-object v0, p0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->g:Lcom/facebook/groups/editfavorites/view/GroupsEditFavoritesDragSortListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setStickyHeaderEnabled(Z)V

    .line 2388990
    iget-object v0, p0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->g:Lcom/facebook/groups/editfavorites/view/GroupsEditFavoritesDragSortListView;

    new-instance v1, LX/GkJ;

    invoke-direct {v1, p0}, LX/GkJ;-><init>(Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;)V

    .line 2388991
    iput-object v1, v0, LX/620;->m:LX/61y;

    .line 2388992
    return-void
.end method
