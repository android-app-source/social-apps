.class public Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/widget/ImageView;

.field public d:Lcom/facebook/fbui/glyph/GlyphView;

.field public e:Lcom/facebook/widget/text/BetterTextView;

.field public f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public g:LX/GkG;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2389063
    const-class v0, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;

    const-string v1, "group_edit_favorites"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2389064
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2389065
    const-class p1, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;

    invoke-static {p1, p0}, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2389066
    const p1, 0x7f030801

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2389067
    const p1, 0x7f0d1516

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->c:Landroid/widget/ImageView;

    .line 2389068
    const p1, 0x7f0d1519

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object p1, p0, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2389069
    const p1, 0x7f0d1518

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2389070
    const p1, 0x7f0d1517

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2389071
    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->setClickable(Z)V

    .line 2389072
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object p0

    check-cast p0, Landroid/content/res/Resources;

    iput-object p0, p1, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->a:Landroid/content/res/Resources;

    return-void
.end method
