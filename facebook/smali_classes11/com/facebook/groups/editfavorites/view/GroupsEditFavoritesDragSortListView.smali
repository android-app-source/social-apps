.class public Lcom/facebook/groups/editfavorites/view/GroupsEditFavoritesDragSortListView;
.super LX/620;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2389073
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/620;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2389074
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2389075
    invoke-direct {p0, p1, p2, p3}, LX/620;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2389076
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2389077
    invoke-virtual {p0, p1}, Lcom/facebook/groups/editfavorites/view/GroupsEditFavoritesDragSortListView;->b(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2389078
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2389079
    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2389080
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2389081
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2389082
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;III)V
    .locals 1

    .prologue
    .line 2389083
    invoke-virtual {p0, p1}, Lcom/facebook/groups/editfavorites/view/GroupsEditFavoritesDragSortListView;->b(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2389084
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2389085
    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2389086
    invoke-virtual {p1, p4}, Landroid/view/View;->setVisibility(I)V

    .line 2389087
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2389088
    check-cast p1, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;

    iget v0, p0, LX/620;->a:I

    .line 2389089
    new-instance p0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 p2, -0x1

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 2389090
    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->getChildAt(I)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2, p0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2389091
    :cond_0
    return-void
.end method

.method public final b(Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2389092
    if-nez p1, :cond_1

    .line 2389093
    :cond_0
    :goto_0
    return v0

    .line 2389094
    :cond_1
    iget v1, p0, LX/620;->c:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2389095
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
