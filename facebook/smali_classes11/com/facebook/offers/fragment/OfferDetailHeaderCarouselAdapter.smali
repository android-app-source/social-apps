.class public Lcom/facebook/offers/fragment/OfferDetailHeaderCarouselAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1OO;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/H62;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/1OO",
        "<",
        "LX/H62;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Z

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/offers/graphql/OfferQueriesInterfaces$PhotoData$;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2425783
    const-class v0, Lcom/facebook/offers/fragment/OfferDetailHeaderCarouselAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/offers/fragment/OfferDetailHeaderCarouselAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 2425784
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2425785
    iput-object p1, p0, Lcom/facebook/offers/fragment/OfferDetailHeaderCarouselAdapter;->b:Landroid/content/Context;

    .line 2425786
    iput-boolean p2, p0, Lcom/facebook/offers/fragment/OfferDetailHeaderCarouselAdapter;->c:Z

    .line 2425787
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2425788
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030c52

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2425789
    iget-boolean v1, p0, Lcom/facebook/offers/fragment/OfferDetailHeaderCarouselAdapter;->c:Z

    if-eqz v1, :cond_0

    .line 2425790
    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailHeaderCarouselAdapter;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LX/1Uo;->a(Landroid/content/res/Resources;)LX/1Uo;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailHeaderCarouselAdapter;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02115d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v1

    sget-object v2, LX/1Up;->g:LX/1Up;

    invoke-virtual {v1, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v1

    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2425791
    :cond_0
    new-instance v1, LX/H62;

    invoke-direct {v1, p0, v0}, LX/H62;-><init>(Lcom/facebook/offers/fragment/OfferDetailHeaderCarouselAdapter;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2425792
    check-cast p1, LX/H62;

    .line 2425793
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailHeaderCarouselAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    .line 2425794
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    iget-object p0, p1, LX/H62;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 p2, 0x0

    invoke-virtual {v2, v1, p2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/offers/fragment/OfferDetailHeaderCarouselAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2425795
    return-void
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2425796
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailHeaderCarouselAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final ii_()I
    .locals 1

    .prologue
    .line 2425797
    const/4 v0, 0x1

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2425798
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailHeaderCarouselAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
