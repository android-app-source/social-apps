.class public final Lcom/facebook/offers/fragment/OfferRenderingUtils$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/H83;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:LX/H6T;


# direct methods
.method public constructor <init>(LX/H6T;LX/H83;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2426640
    iput-object p1, p0, Lcom/facebook/offers/fragment/OfferRenderingUtils$5;->c:LX/H6T;

    iput-object p2, p0, Lcom/facebook/offers/fragment/OfferRenderingUtils$5;->a:LX/H83;

    iput-object p3, p0, Lcom/facebook/offers/fragment/OfferRenderingUtils$5;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 2426630
    :try_start_0
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferRenderingUtils$5;->c:LX/H6T;

    iget-object v0, v0, LX/H6T;->l:LX/H60;

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferRenderingUtils$5;->a:LX/H83;

    invoke-virtual {v1}, LX/H83;->w()LX/H72;

    move-result-object v1

    invoke-interface {v1}, LX/H71;->me_()Ljava/lang/String;

    move-result-object v1

    .line 2426631
    iget-object v2, v0, LX/H60;->b:LX/18V;

    iget-object v3, v0, LX/H60;->c:LX/H5z;

    new-instance v4, LX/H5y;

    invoke-direct {v4}, LX/H5y;-><init>()V

    .line 2426632
    iput-object v1, v4, LX/H5y;->a:Ljava/lang/String;

    .line 2426633
    move-object v4, v4

    .line 2426634
    new-instance v0, Lcom/facebook/offers/fetcher/OfferResendEmailApiMethod$Params;

    invoke-direct {v0, v4}, Lcom/facebook/offers/fetcher/OfferResendEmailApiMethod$Params;-><init>(LX/H5y;)V

    move-object v4, v0

    .line 2426635
    invoke-virtual {v2, v3, v4}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2426636
    :goto_0
    return-void

    .line 2426637
    :catch_0
    move-exception v0

    .line 2426638
    const-string v1, "OfferRenderingUtils"

    const-string v2, "Error resending email"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2426639
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferRenderingUtils$5;->c:LX/H6T;

    iget-object v0, v0, LX/H6T;->m:LX/4nT;

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferRenderingUtils$5;->b:Landroid/content/Context;

    const v2, 0x7f081ce8

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4nT;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
