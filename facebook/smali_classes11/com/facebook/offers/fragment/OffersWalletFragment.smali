.class public Lcom/facebook/offers/fragment/OffersWalletFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->OFFERS_WALLET_FRAGMENT:LX/0cQ;
.end annotation


# instance fields
.field public a:LX/H60;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/offers/fragment/OffersWalletAdapter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/H82;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/4nT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field public j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public k:Lcom/facebook/resources/ui/FbTextView;

.field public l:I

.field public m:Z

.field public n:LX/1P0;

.field private o:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public p:Z

.field public q:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2427182
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2427183
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->m:Z

    .line 2427184
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->p:Z

    .line 2427185
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->o:Ljava/util/Set;

    .line 2427186
    return-void
.end method

.method public static a$redex0(Lcom/facebook/offers/fragment/OffersWalletFragment;II)V
    .locals 5

    .prologue
    .line 2427229
    :goto_0
    if-gt p1, p2, :cond_1

    .line 2427230
    iget-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->c:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    .line 2427231
    iget-object v1, v0, Lcom/facebook/offers/fragment/OffersWalletAdapter;->b:Ljava/util/List;

    move-object v0, v1

    .line 2427232
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->c:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    .line 2427233
    iget-object v1, v0, Lcom/facebook/offers/fragment/OffersWalletAdapter;->b:Ljava/util/List;

    move-object v0, v1

    .line 2427234
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->c:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    .line 2427235
    iget-object v1, v0, Lcom/facebook/offers/fragment/OffersWalletAdapter;->b:Ljava/util/List;

    move-object v0, v1

    .line 2427236
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2427237
    iget-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->c:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    invoke-virtual {v0, p1}, Lcom/facebook/offers/fragment/OffersWalletAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H83;

    .line 2427238
    iget-object v1, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->o:Ljava/util/Set;

    invoke-virtual {v0}, LX/H83;->m()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2427239
    const-string v1, "viewed_offer"

    .line 2427240
    iget-object v2, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->d:LX/0Zb;

    iget-object v3, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->e:LX/H82;

    const-string v4, "wallet"

    invoke-virtual {v3, v1, v0, v4}, LX/H82;->a(Ljava/lang/String;LX/H83;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2427241
    iget-object v1, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->o:Ljava/util/Set;

    invoke-virtual {v0}, LX/H83;->m()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2427242
    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 2427243
    :cond_1
    return-void
.end method

.method public static a$redex0(Lcom/facebook/offers/fragment/OffersWalletFragment;ZZ)V
    .locals 8

    .prologue
    .line 2427217
    iget-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->k:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2427218
    iget-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->k:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2427219
    iget-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->a:LX/H60;

    iget v1, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->l:I

    iget-boolean v2, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->m:Z

    .line 2427220
    sget-object v3, LX/0zS;->a:LX/0zS;

    .line 2427221
    if-eqz p1, :cond_0

    .line 2427222
    sget-object v3, LX/0zS;->d:LX/0zS;

    .line 2427223
    :cond_0
    new-instance v4, LX/H6t;

    invoke-direct {v4}, LX/H6t;-><init>()V

    move-object v4, v4

    .line 2427224
    const-string v5, "count"

    const/16 v6, 0x1e

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "active"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v4

    const-string v5, "profile_pic_width"

    const/16 v6, 0x64

    div-int/lit8 v7, v1, 0x4

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "creative_img_size"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    const-wide/16 v5, 0x258

    invoke-virtual {v3, v5, v6}, LX/0zO;->a(J)LX/0zO;

    move-result-object v3

    .line 2427225
    iget-object v4, v0, LX/H60;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    move-object v0, v3

    .line 2427226
    new-instance v1, LX/H6g;

    invoke-direct {v1, p0, p2}, LX/H6g;-><init>(Lcom/facebook/offers/fragment/OffersWalletFragment;Z)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2427227
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/offers/fragment/OffersWalletFragment;->a(Z)V

    .line 2427228
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2427216
    const-string v0, "offers_wallet"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2427244
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/offers/fragment/OffersWalletFragment;

    invoke-static {v0}, LX/H60;->a(LX/0QB;)LX/H60;

    move-result-object v3

    check-cast v3, LX/H60;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    new-instance v7, Lcom/facebook/offers/fragment/OffersWalletAdapter;

    invoke-static {v0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    invoke-static {v0}, LX/H6T;->a(LX/0QB;)LX/H6T;

    move-result-object v6

    check-cast v6, LX/H6T;

    invoke-direct {v7, v5, v6}, Lcom/facebook/offers/fragment/OffersWalletAdapter;-><init>(Landroid/app/Activity;LX/H6T;)V

    move-object v5, v7

    check-cast v5, Lcom/facebook/offers/fragment/OffersWalletAdapter;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v0}, LX/H82;->b(LX/0QB;)LX/H82;

    move-result-object v7

    check-cast v7, LX/H82;

    const/16 v8, 0x15e7

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/4nT;->b(LX/0QB;)LX/4nT;

    move-result-object v9

    check-cast v9, LX/4nT;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object v3, v2, Lcom/facebook/offers/fragment/OffersWalletFragment;->a:LX/H60;

    iput-object v4, v2, Lcom/facebook/offers/fragment/OffersWalletFragment;->b:LX/0Sh;

    iput-object v5, v2, Lcom/facebook/offers/fragment/OffersWalletFragment;->c:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iput-object v6, v2, Lcom/facebook/offers/fragment/OffersWalletFragment;->d:LX/0Zb;

    iput-object v7, v2, Lcom/facebook/offers/fragment/OffersWalletFragment;->e:LX/H82;

    iput-object v8, v2, Lcom/facebook/offers/fragment/OffersWalletFragment;->f:LX/0Or;

    iput-object v9, v2, Lcom/facebook/offers/fragment/OffersWalletFragment;->g:LX/4nT;

    iput-object v0, v2, Lcom/facebook/offers/fragment/OffersWalletFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 2427245
    iget-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x8b0001

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2427246
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2427247
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 2427248
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 2427249
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2427250
    iget v0, v1, Landroid/graphics/Point;->x:I

    iput v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->l:I

    .line 2427251
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 2427214
    iget-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->i:Lcom/facebook/widget/FbSwipeRefreshLayout;

    new-instance v1, Lcom/facebook/offers/fragment/OffersWalletFragment$7;

    invoke-direct {v1, p0, p1}, Lcom/facebook/offers/fragment/OffersWalletFragment$7;-><init>(Lcom/facebook/offers/fragment/OffersWalletFragment;Z)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbSwipeRefreshLayout;->post(Ljava/lang/Runnable;)Z

    .line 2427215
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x6d433a1c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2427192
    const v0, 0x7f030c55

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2427193
    const v0, 0x7f0d1e56

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2427194
    new-instance v3, LX/H6Y;

    invoke-direct {v3, p0, v0}, LX/H6Y;-><init>(Lcom/facebook/offers/fragment/OffersWalletFragment;Lcom/facebook/resources/ui/FbTextView;)V

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2427195
    const v0, 0x7f0d1e58

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->i:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2427196
    iget-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->i:Lcom/facebook/widget/FbSwipeRefreshLayout;

    new-instance v3, LX/H6Z;

    invoke-direct {v3, p0}, LX/H6Z;-><init>(Lcom/facebook/offers/fragment/OffersWalletFragment;)V

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2427197
    const v0, 0x7f0d1e59

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2427198
    iget-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v3, 0x1

    .line 2427199
    iput-boolean v3, v0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 2427200
    new-instance v0, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/1P0;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->n:LX/1P0;

    .line 2427201
    iget-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->n:LX/1P0;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2427202
    iget-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->c:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2427203
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2427204
    if-eqz v0, :cond_0

    .line 2427205
    const v3, 0x7f081ca5

    invoke-interface {v0, v3}, LX/1ZF;->x_(I)V

    .line 2427206
    const/4 v3, 0x1

    invoke-interface {v0, v3}, LX/1ZF;->k_(Z)V

    .line 2427207
    :cond_0
    const v0, 0x7f0d1e57

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->k:Lcom/facebook/resources/ui/FbTextView;

    .line 2427208
    iget-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v3, LX/H6a;

    invoke-direct {v3, p0}, LX/H6a;-><init>(Lcom/facebook/offers/fragment/OffersWalletFragment;)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2427209
    iget-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->c:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    new-instance v3, LX/H6c;

    invoke-direct {v3, p0}, LX/H6c;-><init>(Lcom/facebook/offers/fragment/OffersWalletFragment;)V

    .line 2427210
    iput-object v3, v0, Lcom/facebook/offers/fragment/OffersWalletAdapter;->e:LX/H6c;

    .line 2427211
    iget-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->c:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    new-instance v3, LX/H6e;

    invoke-direct {v3, p0}, LX/H6e;-><init>(Lcom/facebook/offers/fragment/OffersWalletFragment;)V

    .line 2427212
    iput-object v3, v0, Lcom/facebook/offers/fragment/OffersWalletAdapter;->f:LX/H6e;

    .line 2427213
    const/16 v0, 0x2b

    const v3, -0x4d9a726

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x64a880ac

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2427187
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2427188
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2427189
    const-string v2, "source"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/offers/fragment/OffersWalletFragment;->q:Ljava/lang/String;

    .line 2427190
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/facebook/offers/fragment/OffersWalletFragment;->a$redex0(Lcom/facebook/offers/fragment/OffersWalletFragment;ZZ)V

    .line 2427191
    const/16 v1, 0x2b

    const v2, -0x294adb4b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
