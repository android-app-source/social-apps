.class public Lcom/facebook/offers/fragment/OffersWalletAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1OO;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/H6W;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/1OO",
        "<",
        "LX/H6W;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/H83;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/app/Activity;

.field public final d:LX/H6T;

.field public e:LX/H6c;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/H6e;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2426962
    const-class v0, Lcom/facebook/offers/fragment/OffersWalletAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/offers/fragment/OffersWalletAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;LX/H6T;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2426956
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2426957
    iput-object p1, p0, Lcom/facebook/offers/fragment/OffersWalletAdapter;->c:Landroid/app/Activity;

    .line 2426958
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletAdapter;->b:Ljava/util/List;

    .line 2426959
    iput-object p2, p0, Lcom/facebook/offers/fragment/OffersWalletAdapter;->d:LX/H6T;

    .line 2426960
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/1OM;->a(Z)V

    .line 2426961
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2426955
    iget-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletAdapter;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H83;

    invoke-virtual {v0}, LX/H83;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2426953
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030c56

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2426954
    new-instance v1, LX/H6W;

    invoke-direct {v1, p0, v0}, LX/H6W;-><init>(Lcom/facebook/offers/fragment/OffersWalletAdapter;Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 13

    .prologue
    .line 2426963
    check-cast p1, LX/H6W;

    .line 2426964
    iget-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletAdapter;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H83;

    const/4 v6, 0x0

    const/16 v5, 0x8

    .line 2426965
    iput-object v0, p1, LX/H6W;->m:LX/H83;

    .line 2426966
    iget-object v1, p1, LX/H6W;->m:LX/H83;

    invoke-virtual {v1}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->m()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2426967
    iget-object v3, p1, LX/H6W;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v1, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/offers/fragment/OffersWalletAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2426968
    iget-object v1, p1, LX/H6W;->o:Landroid/widget/TextView;

    iget-object v2, p1, LX/H6W;->m:LX/H83;

    invoke-virtual {v2}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2426969
    iget-object v1, p1, LX/H6W;->p:Lcom/facebook/offers/views/OfferExpirationTimerView;

    iget-object v2, p1, LX/H6W;->m:LX/H83;

    invoke-virtual {v2}, LX/H83;->h()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Lcom/facebook/offers/views/OfferExpirationTimerView;->setExpiresOn(J)V

    .line 2426970
    iget-object v1, p1, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OffersWalletAdapter;->d:LX/H6T;

    iget-object v2, p1, LX/H6W;->p:Lcom/facebook/offers/views/OfferExpirationTimerView;

    iget-object v3, p1, LX/H6W;->m:LX/H83;

    invoke-virtual {v3}, LX/H83;->h()J

    move-result-wide v3

    const/4 v11, 0x0

    .line 2426971
    const-wide/16 v7, 0x3e8

    mul-long/2addr v7, v3

    iget-object v9, v1, LX/H6T;->a:LX/0SG;

    invoke-interface {v9}, LX/0SG;->a()J

    move-result-wide v9

    sub-long/2addr v7, v9

    .line 2426972
    const-wide/32 v9, 0x5265c00

    cmp-long v9, v7, v9

    if-gez v9, :cond_2

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-lez v7, :cond_2

    .line 2426973
    iget-object v7, v1, LX/H6T;->d:LX/0wM;

    const v8, 0x7f0207fe

    const v9, -0x4d4d4e

    invoke-virtual {v7, v8, v9}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v2, v7, v11, v11, v11}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2426974
    const/4 v7, 0x5

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 2426975
    :goto_0
    iget-object v1, p1, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OffersWalletAdapter;->d:LX/H6T;

    iget-object v2, p1, LX/H6W;->o:Landroid/widget/TextView;

    iget-object v3, p1, LX/H6W;->m:LX/H83;

    invoke-virtual {v1, v2, v3}, LX/H6T;->a(Landroid/view/View;LX/H83;)V

    .line 2426976
    iget-object v1, p1, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OffersWalletAdapter;->d:LX/H6T;

    iget-object v2, p1, LX/H6W;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v3, p1, LX/H6W;->m:LX/H83;

    invoke-virtual {v1, v2, v3}, LX/H6T;->a(Landroid/view/View;LX/H83;)V

    .line 2426977
    iget-object v1, p1, LX/H6W;->m:LX/H83;

    invoke-virtual {v1}, LX/H83;->d()I

    move-result v1

    if-lez v1, :cond_0

    .line 2426978
    const/4 v4, 0x1

    .line 2426979
    iget-object v1, p1, LX/H6W;->A:Landroid/widget/TextView;

    iget-object v2, p1, LX/H6W;->m:LX/H83;

    invoke-virtual {v2}, LX/H83;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2426980
    iget-object v1, p1, LX/H6W;->B:Landroid/widget/TextView;

    iget-object v2, p1, LX/H6W;->m:LX/H83;

    invoke-virtual {v2}, LX/H83;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2426981
    iget-object v1, p1, LX/H6W;->z:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v2, p1, LX/H6W;->m:LX/H83;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v4}, LX/H83;->a(IZ)LX/2pa;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 2426982
    iget-object v1, p1, LX/H6W;->z:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 2426983
    iget-object v1, p1, LX/H6W;->z:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v1, v4, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 2426984
    iget-object v1, p1, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OffersWalletAdapter;->d:LX/H6T;

    iget-object v2, p1, LX/H6W;->m:LX/H83;

    invoke-virtual {v1, v2}, LX/H6T;->a(LX/H83;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2426985
    iget-object v1, p1, LX/H6W;->C:Lcom/facebook/resources/ui/FbButton;

    const v2, 0x7f081cda

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2426986
    iget-object v1, p1, LX/H6W;->C:Lcom/facebook/resources/ui/FbButton;

    iget-object v2, p1, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v2, v2, Lcom/facebook/offers/fragment/OffersWalletAdapter;->c:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00a4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setTextColor(I)V

    .line 2426987
    :goto_1
    iget-object v1, p1, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OffersWalletAdapter;->d:LX/H6T;

    iget-object v2, p1, LX/H6W;->C:Lcom/facebook/resources/ui/FbButton;

    iget-object v3, p1, LX/H6W;->m:LX/H83;

    const-string v4, "wallet"

    invoke-virtual {v1, v2, v3, v4}, LX/H6T;->a(Landroid/view/View;LX/H83;Ljava/lang/String;)V

    .line 2426988
    iget-object v1, p1, LX/H6W;->x:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2426989
    iget-object v1, p1, LX/H6W;->D:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v5}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2426990
    iget-object v1, p1, LX/H6W;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2426991
    :goto_2
    return-void

    .line 2426992
    :cond_0
    iget-object v1, p1, LX/H6W;->m:LX/H83;

    invoke-virtual {v1}, LX/H83;->c()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    .line 2426993
    invoke-static {p1}, LX/H6W;->x(LX/H6W;)V

    .line 2426994
    iget-object v1, p1, LX/H6W;->D:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v6}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2426995
    iget-object v1, p1, LX/H6W;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2426996
    iget-object v1, p1, LX/H6W;->x:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2

    .line 2426997
    :cond_1
    iget-object v1, p1, LX/H6W;->m:LX/H83;

    invoke-virtual {v1}, LX/H83;->a()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_5

    .line 2426998
    iget-object v1, p1, LX/H6W;->m:LX/H83;

    invoke-virtual {v1}, LX/H83;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    iget-object v3, p1, LX/H6W;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/offers/fragment/OffersWalletAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2426999
    :goto_3
    iget-object v1, p1, LX/H6W;->u:Landroid/widget/TextView;

    iget-object v2, p1, LX/H6W;->m:LX/H83;

    invoke-virtual {v2}, LX/H83;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2427000
    iget-object v1, p1, LX/H6W;->v:Landroid/widget/TextView;

    iget-object v2, p1, LX/H6W;->m:LX/H83;

    invoke-virtual {v2}, LX/H83;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2427001
    iget-object v1, p1, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OffersWalletAdapter;->d:LX/H6T;

    iget-object v2, p1, LX/H6W;->m:LX/H83;

    invoke-virtual {v1, v2}, LX/H6T;->a(LX/H83;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2427002
    iget-object v1, p1, LX/H6W;->w:Lcom/facebook/resources/ui/FbButton;

    const v2, 0x7f081cda

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2427003
    :goto_4
    iget-object v1, p1, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OffersWalletAdapter;->d:LX/H6T;

    iget-object v2, p1, LX/H6W;->w:Lcom/facebook/resources/ui/FbButton;

    iget-object v3, p1, LX/H6W;->m:LX/H83;

    const-string v4, "wallet"

    invoke-virtual {v1, v2, v3, v4}, LX/H6T;->a(Landroid/view/View;LX/H83;Ljava/lang/String;)V

    .line 2427004
    iget-object v1, p1, LX/H6W;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2427005
    iget-object v1, p1, LX/H6W;->D:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v5}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2427006
    iget-object v1, p1, LX/H6W;->x:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_2

    .line 2427007
    :cond_2
    invoke-virtual {v2, v11, v11, v11, v11}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2427008
    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    goto/16 :goto_0

    .line 2427009
    :cond_3
    iget-object v1, p1, LX/H6W;->m:LX/H83;

    invoke-virtual {v1}, LX/H83;->q()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2427010
    iget-object v1, p1, LX/H6W;->C:Lcom/facebook/resources/ui/FbButton;

    const v2, 0x7f081cc0

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2427011
    iget-object v1, p1, LX/H6W;->C:Lcom/facebook/resources/ui/FbButton;

    iget-object v2, p1, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v2, v2, Lcom/facebook/offers/fragment/OffersWalletAdapter;->c:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00a4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setTextColor(I)V

    goto/16 :goto_1

    .line 2427012
    :cond_4
    iget-object v1, p1, LX/H6W;->C:Lcom/facebook/resources/ui/FbButton;

    const v2, 0x7f081cc0

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2427013
    iget-object v1, p1, LX/H6W;->C:Lcom/facebook/resources/ui/FbButton;

    iget-object v2, p1, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v2, v2, Lcom/facebook/offers/fragment/OffersWalletAdapter;->c:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setTextColor(I)V

    goto/16 :goto_1

    .line 2427014
    :cond_5
    iget-object v1, p1, LX/H6W;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x0

    sget-object v3, Lcom/facebook/offers/fragment/OffersWalletAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_3

    .line 2427015
    :cond_6
    iget-object v1, p1, LX/H6W;->w:Lcom/facebook/resources/ui/FbButton;

    const v2, 0x7f081cc0

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    goto :goto_4
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2426952
    iget-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletAdapter;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final ii_()I
    .locals 1

    .prologue
    .line 2426951
    const/4 v0, 0x1

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2426950
    iget-object v0, p0, Lcom/facebook/offers/fragment/OffersWalletAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
