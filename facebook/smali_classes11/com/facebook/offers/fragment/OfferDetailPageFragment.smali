.class public Lcom/facebook/offers/fragment/OfferDetailPageFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/H6M;
.implements LX/63S;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->OFFERS_DETAIL_PAGE_FRAGMENT:LX/0cQ;
.end annotation


# static fields
.field public static final l:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:Lcom/facebook/offers/views/OfferExpirationTimerView;

.field public B:Lcom/facebook/offers/views/OffersTabbedBar;

.field public C:Landroid/view/View;

.field public D:Lcom/facebook/resources/ui/FbTextView;

.field public E:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public F:Lcom/facebook/resources/ui/FbTextView;

.field public G:Lcom/facebook/resources/ui/FbTextView;

.field public H:Lcom/facebook/resources/ui/FbTextView;

.field public I:Lcom/facebook/resources/ui/FbButton;

.field public J:Landroid/view/ViewGroup;

.field public K:Lcom/facebook/resources/ui/FbTextView;

.field public L:Lcom/facebook/resources/ui/FbTextView;

.field public M:Lcom/facebook/resources/ui/FbTextView;

.field public N:Landroid/view/View;

.field public O:Landroid/view/ViewGroup;

.field public P:Lcom/facebook/resources/ui/FbTextView;

.field public Q:Lcom/facebook/resources/ui/FbTextView;

.field public R:Lcom/facebook/resources/ui/FbButton;

.field public S:Landroid/view/ViewGroup;

.field public T:Lcom/facebook/resources/ui/FbButton;

.field public U:Landroid/view/View;

.field public V:Landroid/view/ViewGroup;

.field public W:Lcom/facebook/resources/ui/FbTextView;

.field public X:Lcom/facebook/resources/ui/FbButton;

.field public Y:Lcom/facebook/resources/ui/FbButton;

.field public Z:Landroid/view/View;

.field public a:LX/H60;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public aA:Z

.field public aB:Z

.field private aC:Z

.field public aD:LX/63Q;

.field public aE:LX/0h5;

.field public aa:Landroid/widget/LinearLayout;

.field public ab:LX/HM4;

.field public ac:LX/HLz;

.field public ad:Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;

.field public ae:Lcom/facebook/resources/ui/FbTextView;

.field public af:Landroid/view/ViewGroup;

.field public ag:Lcom/facebook/resources/ui/FbTextView;

.field public ah:Lcom/facebook/resources/ui/FbTextView;

.field public ai:Landroid/view/View;

.field public aj:Lcom/facebook/resources/ui/FbTextView;

.field public ak:Ljava/lang/String;

.field public al:Ljava/lang/String;

.field public am:Z

.field public an:Ljava/lang/String;

.field public ao:Ljava/lang/String;

.field public ap:Ljava/lang/String;

.field public aq:Ljava/lang/String;

.field public ar:Ljava/lang/String;

.field public as:Z

.field public at:I

.field public au:Z

.field public av:LX/H83;

.field public aw:Z

.field public ax:Ljava/lang/String;

.field private ay:Z

.field public az:Z

.field public b:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/4nT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/H6T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/H82;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field public n:LX/1af;

.field public o:Landroid/view/View;

.field public p:Lcom/facebook/offers/activity/OffersCoverHeaderView;

.field public q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public s:Lcom/facebook/offers/fragment/OfferDetailHeaderCarouselAdapter;

.field public t:Lcom/facebook/video/player/RichVideoPlayer;

.field public u:Landroid/view/ViewGroup;

.field public v:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public w:Lcom/facebook/resources/ui/FbTextView;

.field public x:Lcom/facebook/resources/ui/FbTextView;

.field public y:Lcom/facebook/resources/ui/FbTextView;

.field public z:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2426404
    const-class v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->l:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2426331
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2426332
    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ak:Ljava/lang/String;

    .line 2426333
    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->al:Ljava/lang/String;

    .line 2426334
    iput-boolean v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->am:Z

    .line 2426335
    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->an:Ljava/lang/String;

    .line 2426336
    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ao:Ljava/lang/String;

    .line 2426337
    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ap:Ljava/lang/String;

    .line 2426338
    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aq:Ljava/lang/String;

    .line 2426339
    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ar:Ljava/lang/String;

    .line 2426340
    iput-boolean v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->as:Z

    .line 2426341
    iput-boolean v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->au:Z

    .line 2426342
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aw:Z

    .line 2426343
    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ax:Ljava/lang/String;

    .line 2426344
    iput-boolean v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ay:Z

    .line 2426345
    iput-boolean v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->az:Z

    .line 2426346
    iput-boolean v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aA:Z

    .line 2426347
    iput-boolean v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aB:Z

    .line 2426348
    iput-boolean v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aC:Z

    return-void
.end method

.method public static B(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)LX/0TF;
    .locals 1

    .prologue
    .line 2426349
    new-instance v0, LX/H68;

    invoke-direct {v0, p0}, LX/H68;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    return-object v0
.end method

.method public static C(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V
    .locals 3

    .prologue
    .line 2426350
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->e:LX/4nT;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f081ce4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/4nT;->a(Ljava/lang/String;I)V

    .line 2426351
    return-void
.end method

.method private E()V
    .locals 12
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 2426352
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->H()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 2426353
    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    const/4 v2, 0x0

    .line 2426354
    invoke-virtual {v1}, LX/H83;->C()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2426355
    invoke-virtual {v1}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->j()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    .line 2426356
    if-eqz v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    move v1, v2

    .line 2426357
    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->c:LX/0Uh;

    const/16 v2, 0x59a

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v1}, LX/H83;->C()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->f:LX/H6T;

    .line 2426358
    iget-object v2, v1, LX/H6T;->o:LX/0y3;

    invoke-virtual {v2}, LX/0y3;->a()LX/0yG;

    move-result-object v2

    sget-object v3, LX/0yG;->OKAY:LX/0yG;

    if-ne v2, v3, :cond_a

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 2426359
    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v1}, LX/H83;->N()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    move v0, v0

    .line 2426360
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->I()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->B:Lcom/facebook/offers/views/OffersTabbedBar;

    sget-object v1, LX/H86;->IN_STORE:LX/H86;

    invoke-virtual {v0, v1}, Lcom/facebook/offers/views/OffersTabbedBar;->a(LX/H86;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2426361
    :cond_3
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aa:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2426362
    :goto_1
    return-void

    .line 2426363
    :cond_4
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ab:LX/HM4;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ac:LX/HLz;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ad:Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;

    if-nez v0, :cond_6

    .line 2426364
    :cond_5
    new-instance v0, LX/HM4;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/HM4;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ab:LX/HM4;

    .line 2426365
    new-instance v0, LX/HLz;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/HLz;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ac:LX/HLz;

    .line 2426366
    new-instance v0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ad:Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;

    .line 2426367
    :cond_6
    const/4 v5, 0x0

    const/4 v11, 0x2

    const/4 v10, 0x0

    .line 2426368
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v2}, LX/H83;->P()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2426369
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v2}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2426370
    iget-object v4, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v4}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->k()LX/1vs;

    move-result-object v4

    iget-object v6, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2426371
    iget-object v7, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ab:LX/HM4;

    invoke-virtual {v3, v2, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v4, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v4}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->d()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v4

    invoke-virtual {v7, v2, v3, v4}, LX/HM4;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;)V

    .line 2426372
    :cond_7
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2426373
    new-instance v2, Lcom/facebook/android/maps/model/LatLng;

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v3}, LX/H83;->A()D

    move-result-wide v6

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v3}, LX/H83;->B()D

    move-result-wide v8

    invoke-direct {v2, v6, v7, v8, v9}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2426374
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ac:LX/HLz;

    const/4 v3, 0x1

    const/16 v6, 0xf

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLPlaceType;->PLACE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual/range {v2 .. v8}, LX/HLz;->a(ZLjava/util/ArrayList;Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;ILcom/facebook/graphql/enums/GraphQLPlaceType;Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)V

    .line 2426375
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ac:LX/HLz;

    new-instance v3, LX/H69;

    invoke-direct {v3, p0}, LX/H69;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    invoke-virtual {v2, v3}, LX/HLz;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2426376
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v2}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->mi_()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2426377
    iget-object v4, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v4}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->mi_()LX/1vs;

    move-result-object v4

    iget-object v6, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2426378
    iget-object v7, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ad:Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;

    invoke-virtual {v3, v2, v11}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v4, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v5, v2, v3}, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2426379
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ad:Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;

    new-instance v3, LX/H6A;

    invoke-direct {v3, p0}, LX/H6A;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2426380
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aa:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v2, v11, :cond_9

    .line 2426381
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aa:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2426382
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v2}, LX/H83;->P()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2426383
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aa:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ab:LX/HM4;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2426384
    :cond_8
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aa:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ac:LX/HLz;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2426385
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aa:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ad:Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2426386
    :cond_9
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aa:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public static N(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V
    .locals 2

    .prologue
    .line 2426387
    const v0, 0x7f081ce0

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x61

    invoke-direct {p0, v0, v1}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a(Ljava/lang/String;S)V

    .line 2426388
    return-void
.end method

.method public static O(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V
    .locals 2

    .prologue
    .line 2426389
    const v0, 0x7f081ce1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xa8

    invoke-direct {p0, v0, v1}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a(Ljava/lang/String;S)V

    .line 2426390
    return-void
.end method

.method public static Q(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V
    .locals 5

    .prologue
    .line 2426391
    iget-boolean v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aB:Z

    if-eqz v0, :cond_0

    .line 2426392
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->h:LX/0Zb;

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->i:LX/H82;

    const-string v2, "opened_link"

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    const-string v4, "pages"

    invoke-virtual {v1, v2, v3, v4}, LX/H82;->a(Ljava/lang/String;LX/H83;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2426393
    :cond_0
    const-string v0, "viewed_offer"

    .line 2426394
    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v1}, LX/H83;->M()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2426395
    const-string v0, "viewed_coupon"

    .line 2426396
    :cond_1
    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->i:LX/H82;

    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    const-string v3, "details"

    invoke-virtual {v1, v0, v2, v3}, LX/H82;->a(Ljava/lang/String;LX/H83;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2426397
    const-string v1, "offer_location"

    const-string v2, "details"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2426398
    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->h:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2426399
    return-void
.end method

.method private a(Ljava/lang/String;S)V
    .locals 2

    .prologue
    .line 2426400
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 2426401
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->b:LX/0Sh;

    new-instance v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment$21;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/offers/fragment/OfferDetailPageFragment$21;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;Ljava/lang/String;S)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2426402
    return-void

    .line 2426403
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/offers/fragment/OfferDetailPageFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2426110
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "OfferView is null"

    invoke-static {v0, v1}, LX/0Tp;->b(ZLjava/lang/String;)V

    .line 2426111
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    const-string v1, "ShareId is null"

    invoke-static {v0, v1}, LX/0Tp;->b(ZLjava/lang/String;)V

    .line 2426112
    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a:LX/H60;

    iget v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->at:I

    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, p1, p2, v2, v0}, LX/H60;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    .line 2426113
    new-instance v0, LX/H6F;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, LX/H6F;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-static {v7, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2426114
    if-eqz p7, :cond_0

    .line 2426115
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a(Z)V

    .line 2426116
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->o:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2426117
    :cond_0
    return-void

    .line 2426118
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2426119
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/offers/fragment/OfferDetailPageFragment;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 2426405
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "OfferView id is empty"

    invoke-static {v0, v1}, LX/0Tp;->b(ZLjava/lang/String;)V

    .line 2426406
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a:LX/H60;

    iget v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->at:I

    invoke-virtual {v0, v1, p2, p1}, LX/H60;->a(IZLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2426407
    new-instance v1, LX/H6E;

    invoke-direct {v1, p0}, LX/H6E;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2426408
    return-void

    .line 2426409
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/offers/fragment/OfferDetailPageFragment;ZZ)V
    .locals 3

    .prologue
    .line 2426410
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a:LX/H60;

    iget v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->at:I

    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v2}, LX/H83;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, LX/H60;->a(IZLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2426411
    new-instance v1, LX/H6D;

    invoke-direct {v1, p0, p2}, LX/H6D;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;Z)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2426412
    if-eqz p2, :cond_0

    .line 2426413
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a(Z)V

    .line 2426414
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->o:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2426415
    :cond_0
    return-void
.end method

.method public static l(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V
    .locals 10
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 2426416
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    if-nez v0, :cond_1

    .line 2426417
    const-string v0, "OfferDetailPageFragment"

    const-string v1, "Tried to rebind Detail view with null Offer"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2426418
    invoke-static {p0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->N(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    .line 2426419
    :cond_0
    :goto_0
    return-void

    .line 2426420
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ay:Z

    if-nez v0, :cond_0

    .line 2426421
    invoke-direct {p0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->m()V

    .line 2426422
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 2426423
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->c()I

    move-result v0

    .line 2426424
    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v1}, LX/H83;->d()I

    move-result v1

    .line 2426425
    if-le v0, v5, :cond_5

    .line 2426426
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->s:Lcom/facebook/offers/fragment/OfferDetailHeaderCarouselAdapter;

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v1}, LX/H83;->b()Ljava/util/List;

    move-result-object v1

    .line 2426427
    iput-object v1, v0, Lcom/facebook/offers/fragment/OfferDetailHeaderCarouselAdapter;->d:Ljava/util/List;

    .line 2426428
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2426429
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2426430
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->t:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, v3}, Lcom/facebook/video/player/RichVideoPlayer;->setVisibility(I)V

    .line 2426431
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->t:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v5, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 2426432
    :goto_1
    const/4 v3, 0x0

    .line 2426433
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2426434
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->v:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2426435
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->w:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v1}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2426436
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 2426437
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 2426438
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2426439
    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->x:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2426440
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->x:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2426441
    :goto_2
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->f:LX/H6T;

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->u:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0, v1, v2}, LX/H6T;->a(Landroid/view/View;LX/H83;)V

    .line 2426442
    const/4 v5, 0x0

    .line 2426443
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->y:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v1}, LX/H83;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2426444
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->p()I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_9

    .line 2426445
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->z:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f081cae

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v4}, LX/H83;->p()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2426446
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->z:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2426447
    :goto_3
    invoke-direct {p0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->q()V

    .line 2426448
    iget-boolean v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aA:Z

    if-eqz v0, :cond_a

    .line 2426449
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->H:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2426450
    :goto_4
    const/4 v0, 0x0

    .line 2426451
    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v1}, LX/H83;->I()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->c:LX/0Uh;

    const/16 v2, 0x599

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    move v0, v0

    .line 2426452
    if-eqz v0, :cond_4

    .line 2426453
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->I()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2426454
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->B:Lcom/facebook/offers/views/OffersTabbedBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/offers/views/OffersTabbedBar;->setVisibility(I)V

    .line 2426455
    const/4 v2, 0x1

    .line 2426456
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->B:Lcom/facebook/offers/views/OffersTabbedBar;

    sget-object v1, LX/H86;->ONLINE:LX/H86;

    invoke-virtual {v0, v1}, Lcom/facebook/offers/views/OffersTabbedBar;->a(LX/H86;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2426457
    sget-object v0, LX/H86;->ONLINE:LX/H86;

    invoke-virtual {p0, v0, v2}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a(LX/H86;Z)V

    .line 2426458
    :cond_3
    :goto_5
    const v8, -0x4d4d4e

    const v7, -0xbd984e

    const/4 v1, 0x0

    const/4 v6, 0x0

    const/16 v2, 0x8

    .line 2426459
    iget-boolean v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aA:Z

    if-eqz v0, :cond_d

    .line 2426460
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->J:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2426461
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->N:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2426462
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->S:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2426463
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->V:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2426464
    :goto_6
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->A:Lcom/facebook/offers/views/OfferExpirationTimerView;

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v3}, LX/H83;->h()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/facebook/offers/views/OfferExpirationTimerView;->setExpiresOn(J)V

    .line 2426465
    invoke-direct {p0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->E()V

    .line 2426466
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ae:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v1}, LX/H83;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2426467
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->o()Ljava/lang/String;

    move-result-object v0

    .line 2426468
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 2426469
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->af:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2426470
    :goto_7
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->o:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 2426471
    :cond_4
    invoke-direct {p0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->w()V

    .line 2426472
    invoke-direct {p0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->x()V

    goto :goto_5

    .line 2426473
    :cond_5
    if-ne v0, v5, :cond_6

    .line 2426474
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2426475
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2426476
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2426477
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->t:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, v3}, Lcom/facebook/video/player/RichVideoPlayer;->setVisibility(I)V

    .line 2426478
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->t:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v5, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    goto/16 :goto_1

    .line 2426479
    :cond_6
    if-lez v1, :cond_7

    .line 2426480
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->t:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, v4}, Lcom/facebook/video/player/RichVideoPlayer;->setVisibility(I)V

    .line 2426481
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->t:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v1, v4, v5}, LX/H83;->a(IZ)LX/2pa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 2426482
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->t:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 2426483
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->t:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v4, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 2426484
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2426485
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2426486
    :cond_7
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2426487
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2426488
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2426489
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->t:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, v3}, Lcom/facebook/video/player/RichVideoPlayer;->setVisibility(I)V

    .line 2426490
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->t:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v5, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    goto/16 :goto_1

    .line 2426491
    :cond_8
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->x:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 2426492
    :cond_9
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->z:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 2426493
    :cond_a
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->H:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 2426494
    :cond_b
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->B:Lcom/facebook/offers/views/OffersTabbedBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/offers/views/OffersTabbedBar;->setVisibility(I)V

    goto/16 :goto_5

    .line 2426495
    :cond_c
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->B:Lcom/facebook/offers/views/OffersTabbedBar;

    sget-object v1, LX/H86;->IN_STORE:LX/H86;

    invoke-virtual {v0, v1}, Lcom/facebook/offers/views/OffersTabbedBar;->a(LX/H86;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2426496
    sget-object v0, LX/H86;->IN_STORE:LX/H86;

    invoke-virtual {p0, v0, v2}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a(LX/H86;Z)V

    goto/16 :goto_5

    .line 2426497
    :cond_d
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->u()Z

    move-result v0

    if-eqz v0, :cond_11

    iget-boolean v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->am:Z

    if-eqz v0, :cond_11

    .line 2426498
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->K:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->d:LX/0wM;

    const v4, 0x7f020781

    invoke-virtual {v3, v4, v7}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v6, v3, v6, v6}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2426499
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->K:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a008a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2426500
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->K:Lcom/facebook/resources/ui/FbTextView;

    const v3, 0x7f081cbb

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2426501
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->G()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->M()Z

    move-result v0

    if-nez v0, :cond_f

    .line 2426502
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->X:Lcom/facebook/resources/ui/FbButton;

    new-instance v3, LX/H65;

    invoke-direct {v3, p0}, LX/H65;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2426503
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->W:Lcom/facebook/resources/ui/FbTextView;

    const v3, 0x7f081cce

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    .line 2426504
    invoke-virtual {v5}, LX/H83;->L()Z

    move-result v9

    if-eqz v9, :cond_17

    .line 2426505
    iget-object v9, v5, LX/H83;->b:LX/H70;

    invoke-interface {v9}, LX/H6y;->z()Ljava/lang/String;

    move-result-object v9

    .line 2426506
    :goto_8
    move-object v5, v9

    .line 2426507
    aput-object v5, v4, v1

    invoke-virtual {p0, v3, v4}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2426508
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->S:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2426509
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->V:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2426510
    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->Z:Landroid/view/View;

    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->F()Z

    move-result v0

    if-eqz v0, :cond_e

    move v0, v1

    :goto_9
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2426511
    :goto_a
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 2426512
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->L:Lcom/facebook/resources/ui/FbTextView;

    new-instance v3, LX/H66;

    invoke-direct {v3, p0}, LX/H66;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2426513
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->L:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2426514
    :goto_b
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->q()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 2426515
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->L:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->d:LX/0wM;

    const v4, 0x7f0207d6

    invoke-virtual {v3, v4, v7}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v6, v3, v6, v6}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2426516
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->L:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a008a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2426517
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->L:Lcom/facebook/resources/ui/FbTextView;

    const v3, 0x7f081cc2

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2426518
    :goto_c
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 2426519
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->w()LX/H72;

    move-result-object v0

    invoke-interface {v0}, LX/H72;->I()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    .line 2426520
    invoke-virtual {v0}, LX/H83;->J()Z

    move-result v3

    if-eqz v3, :cond_19

    .line 2426521
    invoke-virtual {v0}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->d()Z

    move-result v3

    .line 2426522
    :goto_d
    move v0, v3

    .line 2426523
    if-nez v0, :cond_14

    .line 2426524
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->f:LX/H6T;

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->M:Lcom/facebook/resources/ui/FbTextView;

    iget-object v4, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    const-string v5, "details"

    .line 2426525
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 2426526
    new-instance v7, LX/H6R;

    invoke-direct {v7, v0, v6, v4, v5}, LX/H6R;-><init>(LX/H6T;Landroid/content/Context;LX/H83;Ljava/lang/String;)V

    move-object v6, v7

    .line 2426527
    invoke-virtual {v3, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2426528
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->M:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2426529
    :goto_e
    iget-boolean v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->am:Z

    if-eqz v0, :cond_15

    .line 2426530
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->N:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2426531
    :goto_f
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->J:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_6

    :cond_e
    move v0, v2

    .line 2426532
    goto/16 :goto_9

    .line 2426533
    :cond_f
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->S:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2426534
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->V:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2426535
    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->U:Landroid/view/View;

    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->F()Z

    move-result v0

    if-eqz v0, :cond_10

    move v0, v1

    :goto_10
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_a

    :cond_10
    move v0, v2

    goto :goto_10

    .line 2426536
    :cond_11
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->K:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->d:LX/0wM;

    const v4, 0x7f020781

    invoke-virtual {v3, v4, v8}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v6, v3, v6, v6}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2426537
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->K:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00a4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2426538
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->K:Lcom/facebook/resources/ui/FbTextView;

    const v3, 0x7f081cba

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2426539
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->S:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2426540
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->V:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_a

    .line 2426541
    :cond_12
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->L:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_b

    .line 2426542
    :cond_13
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->L:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->d:LX/0wM;

    const v4, 0x7f0207d6

    invoke-virtual {v3, v4, v8}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v6, v3, v6, v6}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2426543
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->L:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00a4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2426544
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->L:Lcom/facebook/resources/ui/FbTextView;

    const v3, 0x7f081cc3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto/16 :goto_c

    .line 2426545
    :cond_14
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->M:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_e

    .line 2426546
    :cond_15
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->N:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_f

    .line 2426547
    :cond_16
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->N:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2426548
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->J:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_6

    .line 2426549
    :cond_17
    invoke-virtual {v5}, LX/H83;->u()Z

    move-result v9

    if-eqz v9, :cond_18

    .line 2426550
    iget-object v9, v5, LX/H83;->a:LX/H72;

    invoke-interface {v9}, LX/H72;->K()LX/H6z;

    move-result-object v9

    invoke-interface {v9}, LX/H6x;->z()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_8

    .line 2426551
    :cond_18
    const/4 v9, 0x0

    goto/16 :goto_8

    :cond_19
    const/4 v3, 0x0

    goto/16 :goto_d

    .line 2426552
    :cond_1a
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->af:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2426553
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ag:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v1}, LX/H83;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2426554
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->af:Landroid/view/ViewGroup;

    new-instance v1, LX/H6B;

    invoke-direct {v1, p0}, LX/H6B;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2426555
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ag:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 2426556
    new-instance v1, LX/H6C;

    invoke-direct {v1, p0}, LX/H6C;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto/16 :goto_7
.end method

.method private m()V
    .locals 5
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 2426557
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2426558
    const v0, 0x7f081cab

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v3}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2426559
    :goto_0
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2426560
    if-eqz v0, :cond_0

    .line 2426561
    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2426562
    invoke-interface {v0, v4}, LX/1ZF;->k_(Z)V

    .line 2426563
    :cond_0
    return-void

    .line 2426564
    :cond_1
    const v0, 0x7f081cac

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method

.method private q()V
    .locals 3
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 2426565
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->f:LX/H6T;

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0, v1}, LX/H6T;->a(LX/H83;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->q()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aA:Z

    if-eqz v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2426566
    if-eqz v0, :cond_1

    .line 2426567
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->I:Lcom/facebook/resources/ui/FbButton;

    const v1, 0x7f081cad

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2426568
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->f:LX/H6T;

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->I:Lcom/facebook/resources/ui/FbButton;

    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0, v1, v2}, LX/H6T;->a(Landroid/view/View;LX/H83;)V

    .line 2426569
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->I:Lcom/facebook/resources/ui/FbButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2426570
    :goto_1
    return-void

    .line 2426571
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->am:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->L()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->K()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aA:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2426572
    if-eqz v0, :cond_2

    .line 2426573
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->I:Lcom/facebook/resources/ui/FbButton;

    const v1, 0x7f081cb3

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2426574
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->I:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/H63;

    invoke-direct {v1, p0}, LX/H63;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 2426575
    :cond_2
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->E()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 2426576
    if-eqz v0, :cond_3

    .line 2426577
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->I:Lcom/facebook/resources/ui/FbButton;

    const v1, 0x7f081cb9

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2426578
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->f:LX/H6T;

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->I:Lcom/facebook/resources/ui/FbButton;

    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    .line 2426579
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 2426580
    new-instance p0, LX/H6Q;

    invoke-direct {p0, v0, v1, v2}, LX/H6Q;-><init>(LX/H6T;Landroid/view/View;LX/H83;)V

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2426581
    goto :goto_1

    .line 2426582
    :cond_3
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->I:Lcom/facebook/resources/ui/FbButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private w()V
    .locals 3
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const/16 v2, 0x8

    .line 2426285
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->G()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->q()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->f:LX/H6T;

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0, v1}, LX/H6T;->a(LX/H83;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2426286
    :cond_0
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->O:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2426287
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->R:Lcom/facebook/resources/ui/FbButton;

    const v1, 0x7f081cd2

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2426288
    :goto_0
    return-void

    .line 2426289
    :cond_1
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2426290
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->l()Ljava/lang/String;

    move-result-object v0

    const-string v1, "unique-code"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2426291
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->Q:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f081cd1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2426292
    :goto_1
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->P:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v1}, LX/H83;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2426293
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->O:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 2426294
    :cond_2
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->Q:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f081cd0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto :goto_1

    .line 2426295
    :cond_3
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->O:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method private x()V
    .locals 6
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/16 v5, 0x8

    const/4 v2, 0x0

    .line 2426296
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->H()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aA:Z

    if-eqz v0, :cond_1

    .line 2426297
    :cond_0
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->C:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2426298
    :goto_0
    return-void

    .line 2426299
    :cond_1
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->G()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2426300
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->F:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2426301
    :goto_1
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2426302
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->x()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 2426303
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->E:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v3}, LX/H83;->x()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2426304
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->E:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->at:I

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setMinimumWidth(I)V

    .line 2426305
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->E:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2426306
    const-string v0, "QR"

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    .line 2426307
    invoke-virtual {v3}, LX/H83;->L()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 2426308
    iget-object v4, v3, LX/H83;->b:LX/H70;

    invoke-interface {v4}, LX/H6y;->v()Ljava/lang/String;

    move-result-object v4

    .line 2426309
    :goto_2
    move-object v3, v4

    .line 2426310
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2426311
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->E:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1314

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setMinimumHeight(I)V

    .line 2426312
    :goto_3
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->E:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->invalidate()V

    .line 2426313
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->E:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v3, LX/H64;

    invoke-direct {v3, p0}, LX/H64;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2426314
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->z()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2426315
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->G:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v3}, LX/H83;->z()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2426316
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->G:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    :cond_2
    move v0, v1

    .line 2426317
    :goto_4
    invoke-direct {p0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->E()V

    .line 2426318
    :goto_5
    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v3}, LX/H83;->G()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v3}, LX/H83;->I()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->B:Lcom/facebook/offers/views/OffersTabbedBar;

    sget-object v4, LX/H86;->IN_STORE:LX/H86;

    invoke-virtual {v3, v4}, Lcom/facebook/offers/views/OffersTabbedBar;->a(LX/H86;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2426319
    :cond_3
    :goto_6
    if-eqz v1, :cond_7

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2426320
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->D:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v1}, LX/H83;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2426321
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->E:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2426322
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->D:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2426323
    :goto_7
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->C:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 2426324
    :cond_4
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->F:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2426325
    :cond_5
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->E:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1313

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setMinimumHeight(I)V

    goto/16 :goto_3

    :cond_6
    move v1, v2

    .line 2426326
    goto :goto_6

    .line 2426327
    :cond_7
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->D:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_7

    :cond_8
    move v0, v2

    goto :goto_4

    :cond_9
    move v0, v2

    goto :goto_5

    .line 2426328
    :cond_a
    invoke-virtual {v3}, LX/H83;->K()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 2426329
    invoke-virtual {v3}, LX/H83;->w()LX/H72;

    move-result-object v4

    invoke-interface {v4}, LX/H72;->G()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->j()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_2

    .line 2426330
    :cond_b
    const/4 v4, 0x0

    goto/16 :goto_2
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2426109
    const-string v0, "offers_detail_page"

    return-object v0
.end method

.method public final a(LX/H86;Z)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2426120
    if-nez p2, :cond_1

    .line 2426121
    :cond_0
    :goto_0
    return-void

    .line 2426122
    :cond_1
    sget-object v0, LX/H86;->ONLINE:LX/H86;

    if-ne v0, p1, :cond_2

    .line 2426123
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->C:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2426124
    invoke-direct {p0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->w()V

    .line 2426125
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aa:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 2426126
    :cond_2
    sget-object v0, LX/H86;->IN_STORE:LX/H86;

    if-ne v0, p1, :cond_0

    .line 2426127
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->O:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2426128
    invoke-direct {p0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->x()V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    .line 2426129
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v0}, LX/H60;->a(LX/0QB;)LX/H60;

    move-result-object v3

    check-cast v3, LX/H60;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v6

    check-cast v6, LX/0wM;

    invoke-static {v0}, LX/4nT;->b(LX/0QB;)LX/4nT;

    move-result-object v7

    check-cast v7, LX/4nT;

    invoke-static {v0}, LX/H6T;->a(LX/0QB;)LX/H6T;

    move-result-object v8

    check-cast v8, LX/H6T;

    const/16 v9, 0x15e7

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v10

    check-cast v10, LX/0Zb;

    invoke-static {v0}, LX/H82;->b(LX/0QB;)LX/H82;

    move-result-object v11

    check-cast v11, LX/H82;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v12

    check-cast v12, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v0

    check-cast v0, LX/0zG;

    iput-object v3, v2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a:LX/H60;

    iput-object v4, v2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->b:LX/0Sh;

    iput-object v5, v2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->c:LX/0Uh;

    iput-object v6, v2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->d:LX/0wM;

    iput-object v7, v2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->e:LX/4nT;

    iput-object v8, v2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->f:LX/H6T;

    iput-object v9, v2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->g:LX/0Or;

    iput-object v10, v2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->h:LX/0Zb;

    iput-object v11, v2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->i:LX/H82;

    iput-object v12, v2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->j:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object v0, v2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->k:LX/0zG;

    .line 2426130
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->j:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x8b0002

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2426131
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2426132
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 2426133
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 2426134
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2426135
    iget v0, v1, Landroid/graphics/Point;->x:I

    iput v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->at:I

    .line 2426136
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 2426137
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->m:Lcom/facebook/widget/FbSwipeRefreshLayout;

    new-instance v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment$20;

    invoke-direct {v1, p0, p1}, Lcom/facebook/offers/fragment/OfferDetailPageFragment$20;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;Z)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbSwipeRefreshLayout;->post(Ljava/lang/Runnable;)Z

    .line 2426138
    if-eqz p1, :cond_0

    .line 2426139
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ai:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2426140
    :cond_0
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 2426141
    invoke-virtual {p0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->mK_()LX/63R;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()I
    .locals 2

    .prologue
    .line 2426142
    invoke-virtual {p0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->mK_()LX/63R;

    move-result-object v0

    .line 2426143
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 2426144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aC:Z

    .line 2426145
    return-void
.end method

.method public final mK_()LX/63R;
    .locals 1

    .prologue
    .line 2426146
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->p:Lcom/facebook/offers/activity/OffersCoverHeaderView;

    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x400a4192

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2426147
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2426148
    if-eqz p1, :cond_0

    .line 2426149
    const-string v1, "offer_claim_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ak:Ljava/lang/String;

    .line 2426150
    const-string v1, "coupon_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->an:Ljava/lang/String;

    .line 2426151
    const-string v1, "is_offer_claimed"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->am:Z

    .line 2426152
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x5692e4ae

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2426153
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/content/Context;)V

    .line 2426154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ay:Z

    .line 2426155
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x2000d855

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2426156
    const v1, 0x7f030c53

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2426157
    const v2, 0x7f0d1e26

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->m:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2426158
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->m:Lcom/facebook/widget/FbSwipeRefreshLayout;

    new-instance v3, LX/H6G;

    invoke-direct {v3, p0}, LX/H6G;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2426159
    const v2, 0x7f0d1e27

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->o:Landroid/view/View;

    .line 2426160
    const/4 p3, 0x0

    .line 2426161
    const v2, 0x7f0d1e28

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/offers/activity/OffersCoverHeaderView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->p:Lcom/facebook/offers/activity/OffersCoverHeaderView;

    .line 2426162
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->p:Lcom/facebook/offers/activity/OffersCoverHeaderView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/facebook/offers/activity/OffersCoverHeaderView;->setVisibility(I)V

    .line 2426163
    const v2, 0x7f0d1e22

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2426164
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2426165
    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->n:LX/1af;

    if-nez v3, :cond_0

    .line 2426166
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, LX/1Uo;->a(Landroid/content/res/Resources;)LX/1Uo;

    move-result-object v3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f02115d

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {v3, p1}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v3

    sget-object p1, LX/1Up;->g:LX/1Up;

    invoke-virtual {v3, p1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v3

    invoke-virtual {v3}, LX/1Uo;->u()LX/1af;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->n:LX/1af;

    .line 2426167
    :cond_0
    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->n:LX/1af;

    move-object v3, v3

    .line 2426168
    invoke-virtual {v2, v3}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2426169
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v3, 0x3ff47ae1    # 1.91f

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2426170
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->at:I

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setMinimumWidth(I)V

    .line 2426171
    const v2, 0x7f0d1e29

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2426172
    new-instance v2, Lcom/facebook/offers/fragment/OfferDetailHeaderCarouselAdapter;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 p1, 0x1

    invoke-direct {v2, v3, p1}, Lcom/facebook/offers/fragment/OfferDetailHeaderCarouselAdapter;-><init>(Landroid/content/Context;Z)V

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->s:Lcom/facebook/offers/fragment/OfferDetailHeaderCarouselAdapter;

    .line 2426173
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->s:Lcom/facebook/offers/fragment/OfferDetailHeaderCarouselAdapter;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2426174
    const v2, 0x7f0d1e2a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->t:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2426175
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->t:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->f:LX/H6T;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    sget-object p2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, p1, p2}, LX/H6T;->a(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    .line 2426176
    new-instance v2, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, p3, p3}, LX/1P0;-><init>(Landroid/content/Context;IZ)V

    .line 2426177
    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2426178
    const v2, 0x7f0d1e2b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->u:Landroid/view/ViewGroup;

    .line 2426179
    const v2, 0x7f0d1e2c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->v:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2426180
    const v2, 0x7f0d1e2d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->w:Lcom/facebook/resources/ui/FbTextView;

    .line 2426181
    const v2, 0x7f0d1e2e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->x:Lcom/facebook/resources/ui/FbTextView;

    .line 2426182
    const v2, 0x7f0d1e2f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->y:Lcom/facebook/resources/ui/FbTextView;

    .line 2426183
    const v2, 0x7f0d1e30

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->z:Lcom/facebook/resources/ui/FbTextView;

    .line 2426184
    const v2, 0x7f0d1e31

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/offers/views/OfferExpirationTimerView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->A:Lcom/facebook/offers/views/OfferExpirationTimerView;

    .line 2426185
    const v2, 0x7f0d1e33

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/offers/views/OffersTabbedBar;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->B:Lcom/facebook/offers/views/OffersTabbedBar;

    .line 2426186
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->B:Lcom/facebook/offers/views/OffersTabbedBar;

    const/4 v3, 0x2

    new-array v3, v3, [LX/H86;

    const/4 p1, 0x0

    sget-object p2, LX/H86;->ONLINE:LX/H86;

    aput-object p2, v3, p1

    const/4 p1, 0x1

    sget-object p2, LX/H86;->IN_STORE:LX/H86;

    aput-object p2, v3, p1

    invoke-virtual {v2, v3}, Lcom/facebook/offers/views/OffersTabbedBar;->setTabTypes([LX/H86;)V

    .line 2426187
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->B:Lcom/facebook/offers/views/OffersTabbedBar;

    sget-object v3, LX/H86;->ONLINE:LX/H86;

    invoke-virtual {v2, v3}, Lcom/facebook/offers/views/OffersTabbedBar;->setSelected(LX/H86;)V

    .line 2426188
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->B:Lcom/facebook/offers/views/OffersTabbedBar;

    .line 2426189
    iput-object p0, v2, Lcom/facebook/offers/views/OffersTabbedBar;->b:LX/H6M;

    .line 2426190
    const v2, 0x7f0d1e38

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->C:Landroid/view/View;

    .line 2426191
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->C:Landroid/view/View;

    const v3, 0x7f0d1e39

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->D:Lcom/facebook/resources/ui/FbTextView;

    .line 2426192
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->C:Landroid/view/View;

    const v3, 0x7f0d1e3a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->E:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2426193
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->C:Landroid/view/View;

    const v3, 0x7f0d1e3c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->F:Lcom/facebook/resources/ui/FbTextView;

    .line 2426194
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->C:Landroid/view/View;

    const v3, 0x7f0d1e3b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->G:Lcom/facebook/resources/ui/FbTextView;

    .line 2426195
    const v2, 0x7f0d1e32

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbButton;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->I:Lcom/facebook/resources/ui/FbButton;

    .line 2426196
    const v2, 0x7f0d1e34

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->O:Landroid/view/ViewGroup;

    .line 2426197
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->O:Landroid/view/ViewGroup;

    const v3, 0x7f0d1e35

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->P:Lcom/facebook/resources/ui/FbTextView;

    .line 2426198
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->O:Landroid/view/ViewGroup;

    const v3, 0x7f0d1e36

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->Q:Lcom/facebook/resources/ui/FbTextView;

    .line 2426199
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->O:Landroid/view/ViewGroup;

    const v3, 0x7f0d1e37

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbButton;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->R:Lcom/facebook/resources/ui/FbButton;

    .line 2426200
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->R:Lcom/facebook/resources/ui/FbButton;

    const v3, 0x7f081cd2

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2426201
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->R:Lcom/facebook/resources/ui/FbButton;

    new-instance v3, LX/H6H;

    invoke-direct {v3, p0}, LX/H6H;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2426202
    const v2, 0x7f0d1e3d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->H:Lcom/facebook/resources/ui/FbTextView;

    .line 2426203
    const/4 p3, 0x0

    .line 2426204
    const v2, 0x7f0d1e3e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->J:Landroid/view/ViewGroup;

    .line 2426205
    const v2, 0x7f0d1e42

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->N:Landroid/view/View;

    .line 2426206
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->J:Landroid/view/ViewGroup;

    const v3, 0x7f0d1e40

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->L:Lcom/facebook/resources/ui/FbTextView;

    .line 2426207
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->J:Landroid/view/ViewGroup;

    const v3, 0x7f0d1e3f

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->K:Lcom/facebook/resources/ui/FbTextView;

    .line 2426208
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->K:Lcom/facebook/resources/ui/FbTextView;

    new-instance v3, LX/H6J;

    invoke-direct {v3, p0}, LX/H6J;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2426209
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->J:Landroid/view/ViewGroup;

    const v3, 0x7f0d1e41

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->M:Lcom/facebook/resources/ui/FbTextView;

    .line 2426210
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->M:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->d:LX/0wM;

    const p1, 0x7f0209c5

    const p2, -0x4d4d4e

    invoke-virtual {v3, p1, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, p3, v3, p3, p3}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2426211
    const v2, 0x7f0d1e43

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->S:Landroid/view/ViewGroup;

    .line 2426212
    const v2, 0x7f0d1e44

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    .line 2426213
    const v3, 0x7f081ccd

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2426214
    const v2, 0x7f0d1e45

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbButton;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->T:Lcom/facebook/resources/ui/FbButton;

    .line 2426215
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->T:Lcom/facebook/resources/ui/FbButton;

    new-instance v3, LX/H6K;

    invoke-direct {v3, p0}, LX/H6K;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2426216
    const v2, 0x7f0d1e46

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->U:Landroid/view/View;

    .line 2426217
    const v2, 0x7f0d1e47

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->V:Landroid/view/ViewGroup;

    .line 2426218
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->V:Landroid/view/ViewGroup;

    const v3, 0x7f0d1e4a

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbButton;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->Y:Lcom/facebook/resources/ui/FbButton;

    .line 2426219
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->V:Landroid/view/ViewGroup;

    const v3, 0x7f0d1e49

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbButton;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->X:Lcom/facebook/resources/ui/FbButton;

    .line 2426220
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->V:Landroid/view/ViewGroup;

    const v3, 0x7f0d1e48

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->W:Lcom/facebook/resources/ui/FbTextView;

    .line 2426221
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->Y:Lcom/facebook/resources/ui/FbButton;

    new-instance v3, LX/H6L;

    invoke-direct {v3, p0}, LX/H6L;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2426222
    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->X:Lcom/facebook/resources/ui/FbButton;

    const v3, 0x7f081cb1

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2426223
    const v2, 0x7f0d1e4b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->Z:Landroid/view/View;

    .line 2426224
    const v2, 0x7f0d1e4c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aa:Landroid/widget/LinearLayout;

    .line 2426225
    const v2, 0x7f0d1e4d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ae:Lcom/facebook/resources/ui/FbTextView;

    .line 2426226
    const v2, 0x7f0d1e4e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->af:Landroid/view/ViewGroup;

    .line 2426227
    const v2, 0x7f0d1e50

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ag:Lcom/facebook/resources/ui/FbTextView;

    .line 2426228
    const v2, 0x7f0d1e51

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ah:Lcom/facebook/resources/ui/FbTextView;

    .line 2426229
    const v2, 0x7f0d1e52

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ai:Landroid/view/View;

    .line 2426230
    const v2, 0x7f0d1e53

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aj:Lcom/facebook/resources/ui/FbTextView;

    .line 2426231
    invoke-direct {p0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->m()V

    .line 2426232
    const/16 v2, 0x2b

    const v3, 0x70e89209

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDetach()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x103e7df2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2426233
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDetach()V

    .line 2426234
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ay:Z

    .line 2426235
    const/16 v1, 0x2b

    const v2, 0x4849af85

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 12

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7cc9d03e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2426236
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2426237
    iget-boolean v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->az:Z

    if-eqz v1, :cond_0

    .line 2426238
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->az:Z

    .line 2426239
    const/16 v1, 0x2b

    const v2, -0x301fb277

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2426240
    :goto_0
    return-void

    .line 2426241
    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2426242
    const-string v2, "offer_claim_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ak:Ljava/lang/String;

    .line 2426243
    const-string v2, "offer_view_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->al:Ljava/lang/String;

    .line 2426244
    const-string v2, "coupon_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->an:Ljava/lang/String;

    .line 2426245
    const-string v2, "share_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2426246
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2426247
    const/4 v2, 0x0

    .line 2426248
    :cond_1
    :goto_1
    move-object v2, v2

    .line 2426249
    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ax:Ljava/lang/String;

    .line 2426250
    const-string v2, "notif_trigger"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ao:Ljava/lang/String;

    .line 2426251
    const-string v2, "notif_medium"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ap:Ljava/lang/String;

    .line 2426252
    const-string v2, "rule"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aq:Ljava/lang/String;

    .line 2426253
    const-string v2, "claim_type"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ar:Ljava/lang/String;

    .line 2426254
    const-string v2, "1"

    const-string v3, "redirect"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->as:Z

    .line 2426255
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2426256
    const-string v2, "offer_should_claim"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_2
    iput-boolean v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aB:Z

    .line 2426257
    const/4 v5, 0x0

    const/4 v11, 0x1

    .line 2426258
    iget-object v4, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ak:Ljava/lang/String;

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->an:Ljava/lang/String;

    if-eqz v4, :cond_6

    .line 2426259
    :cond_2
    invoke-static {p0, v5, v11}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a$redex0(Lcom/facebook/offers/fragment/OfferDetailPageFragment;ZZ)V

    .line 2426260
    :goto_3
    const v1, 0x727e519c

    invoke-static {v1, v0}, LX/02F;->f(II)V

    goto :goto_0

    .line 2426261
    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    .line 2426262
    :cond_4
    const-string v3, "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$"

    invoke-virtual {v2, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "^[0-9]+$"

    invoke-virtual {v2, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    const/4 v3, 0x1

    :goto_4
    move v3, v3

    .line 2426263
    if-eqz v3, :cond_1

    .line 2426264
    invoke-static {v2}, LX/H6T;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_5
    const/4 v3, 0x0

    goto :goto_4

    .line 2426265
    :cond_6
    iget-boolean v4, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->am:Z

    if-nez v4, :cond_7

    .line 2426266
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2426267
    const-string v6, "offer_should_claim"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2426268
    const-string v6, "0"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    const/4 v4, 0x1

    :goto_5
    move v4, v4

    .line 2426269
    if-eqz v4, :cond_8

    .line 2426270
    :cond_7
    iget-object v5, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->al:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ax:Ljava/lang/String;

    iget-object v7, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ao:Ljava/lang/String;

    iget-object v8, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ap:Ljava/lang/String;

    iget-object v9, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aq:Ljava/lang/String;

    iget-boolean v10, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->as:Z

    move-object v4, p0

    invoke-static/range {v4 .. v11}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a$redex0(Lcom/facebook/offers/fragment/OfferDetailPageFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto :goto_3

    .line 2426271
    :cond_8
    iput-boolean v5, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->am:Z

    .line 2426272
    iget-object v4, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->al:Ljava/lang/String;

    invoke-static {p0, v4, v11}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a$redex0(Lcom/facebook/offers/fragment/OfferDetailPageFragment;Ljava/lang/String;Z)V

    goto :goto_3

    :cond_9
    const/4 v4, 0x0

    goto :goto_5
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2426273
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2426274
    const-string v0, "offer_claim_id"

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ak:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2426275
    const-string v0, "coupon_id"

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->an:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2426276
    const-string v0, "is_offer_claimed"

    iget-boolean v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->am:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2426277
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2426278
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->k:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aC:Z

    if-nez v0, :cond_0

    .line 2426279
    const-class v1, LX/63U;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/63U;

    .line 2426280
    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aD:LX/63Q;

    if-nez v1, :cond_0

    if-eqz v5, :cond_0

    .line 2426281
    new-instance v1, LX/63Q;

    invoke-direct {v1}, LX/63Q;-><init>()V

    iput-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aD:LX/63Q;

    .line 2426282
    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->k:LX/0zG;

    invoke-interface {v1}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0h5;

    iput-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aE:LX/0h5;

    .line 2426283
    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aD:LX/63Q;

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aE:LX/0h5;

    check-cast v3, LX/63T;

    new-instance v4, LX/2iI;

    new-instance v2, Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v2, v6}, Lcom/facebook/widget/listview/BetterListView;-><init>(Landroid/content/Context;)V

    invoke-direct {v4, v2}, LX/2iI;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v2, p0

    invoke-virtual/range {v1 .. v7}, LX/63Q;->a(LX/63S;LX/63T;LX/0g9;LX/63U;ZZ)V

    .line 2426284
    :cond_0
    return-void
.end method
