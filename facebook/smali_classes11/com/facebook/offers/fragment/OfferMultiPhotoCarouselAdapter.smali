.class public Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1OO;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/H6N;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/1OO",
        "<",
        "LX/H6N;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/H6T;

.field public final d:Z

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/offers/graphql/OfferQueriesInterfaces$PhotoData$;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/H83;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2426602
    const-class v0, Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/H6T;Z)V
    .locals 0

    .prologue
    .line 2426597
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2426598
    iput-object p1, p0, Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;->b:Landroid/content/Context;

    .line 2426599
    iput-object p2, p0, Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;->c:LX/H6T;

    .line 2426600
    iput-boolean p3, p0, Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;->d:Z

    .line 2426601
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2426595
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030c51

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2426596
    new-instance v1, LX/H6N;

    invoke-direct {v1, p0, v0}, LX/H6N;-><init>(Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2426603
    check-cast p1, LX/H6N;

    .line 2426604
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;->e:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    .line 2426605
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    iget-object p0, p1, LX/H6N;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 p2, 0x0

    invoke-virtual {v2, v1, p2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2426606
    iget-object v1, p1, LX/H6N;->m:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p1, LX/H6N;->p:Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;

    iget-object v2, v2, Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;->f:LX/H83;

    invoke-virtual {v2}, LX/H83;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2426607
    iget-object v1, p1, LX/H6N;->n:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p1, LX/H6N;->p:Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;

    iget-object v2, v2, Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;->f:LX/H83;

    invoke-virtual {v2}, LX/H83;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2426608
    iget-object v1, p1, LX/H6N;->p:Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;->c:LX/H6T;

    iget-object v2, p1, LX/H6N;->p:Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;

    iget-object v2, v2, Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;->f:LX/H83;

    invoke-virtual {v1, v2}, LX/H6T;->a(LX/H83;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2426609
    iget-object v1, p1, LX/H6N;->o:Lcom/facebook/resources/ui/FbButton;

    const v2, 0x7f081cda

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2426610
    :goto_0
    iget-object v1, p1, LX/H6N;->p:Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;->c:LX/H6T;

    iget-object v2, p1, LX/H6N;->o:Lcom/facebook/resources/ui/FbButton;

    iget-object p0, p1, LX/H6N;->p:Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;

    iget-object p0, p0, Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;->f:LX/H83;

    const-string p2, "wallet"

    invoke-virtual {v1, v2, p0, p2}, LX/H6T;->a(Landroid/view/View;LX/H83;Ljava/lang/String;)V

    .line 2426611
    return-void

    .line 2426612
    :cond_0
    iget-object v1, p1, LX/H6N;->o:Lcom/facebook/resources/ui/FbButton;

    const v2, 0x7f081cc0

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2426594
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final ii_()I
    .locals 1

    .prologue
    .line 2426593
    const/4 v0, 0x1

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2426592
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
