.class public final Lcom/facebook/offers/fragment/OfferDetailPageFragment$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;Z)V
    .locals 0

    .prologue
    .line 2426041
    iput-object p1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment$4;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iput-boolean p2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment$4;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2426042
    iget-boolean v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment$4;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment$4;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-boolean v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aw:Z

    if-eqz v0, :cond_0

    .line 2426043
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment$4;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    .line 2426044
    iput-boolean v8, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aw:Z

    .line 2426045
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment$4;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->f:LX/H6T;

    iget-object v1, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment$4;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment$4;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v2, v2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v2}, LX/H83;->i()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment$4;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v3, v3, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v3}, LX/H83;->j()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment$4;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v4, v4, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v4}, LX/H83;->f()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment$4;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v5, v5, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v5}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->m()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment$4;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v6, v6, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v6}, LX/H83;->w()LX/H72;

    move-result-object v6

    invoke-interface {v6}, LX/H71;->me_()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment$4;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v7, v7, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-static {v7}, LX/H6T;->b(LX/H83;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, LX/H6T;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2426046
    :goto_0
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment$4;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-virtual {v0, v8}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a(Z)V

    .line 2426047
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment$4;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->j:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x8b0002

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2426048
    return-void

    .line 2426049
    :cond_0
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment$4;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->Q(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    .line 2426050
    iget-object v0, p0, Lcom/facebook/offers/fragment/OfferDetailPageFragment$4;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->l(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    goto :goto_0
.end method
