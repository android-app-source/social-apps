.class public final Lcom/facebook/offers/fetcher/OfferResendEmailApiMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/offers/fetcher/OfferResendEmailApiMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2425654
    new-instance v0, LX/H5x;

    invoke-direct {v0}, LX/H5x;-><init>()V

    sput-object v0, Lcom/facebook/offers/fetcher/OfferResendEmailApiMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/H5y;)V
    .locals 1

    .prologue
    .line 2425655
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2425656
    iget-object v0, p1, LX/H5y;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/offers/fetcher/OfferResendEmailApiMethod$Params;->a:Ljava/lang/String;

    .line 2425657
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2425658
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2425659
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/fetcher/OfferResendEmailApiMethod$Params;->a:Ljava/lang/String;

    .line 2425660
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2425661
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2425662
    iget-object v0, p0, Lcom/facebook/offers/fetcher/OfferResendEmailApiMethod$Params;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2425663
    return-void
.end method
