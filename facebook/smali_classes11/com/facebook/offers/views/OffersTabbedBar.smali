.class public Lcom/facebook/offers/views/OffersTabbedBar;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private a:[Lcom/facebook/resources/ui/FbTextView;

.field public b:LX/H6M;

.field private c:LX/H86;

.field private d:[LX/H86;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2432973
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2432974
    invoke-direct {p0}, Lcom/facebook/offers/views/OffersTabbedBar;->a()V

    .line 2432975
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2432979
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2432980
    invoke-direct {p0}, Lcom/facebook/offers/views/OffersTabbedBar;->a()V

    .line 2432981
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2432976
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2432977
    invoke-direct {p0}, Lcom/facebook/offers/views/OffersTabbedBar;->a()V

    .line 2432978
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 2432955
    const v0, 0x7f030c54

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2432956
    const v0, 0x7f0d1e54

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2432957
    const v1, 0x7f0d1e55

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2432958
    const/4 v3, 0x2

    new-array v3, v3, [Lcom/facebook/resources/ui/FbTextView;

    aput-object v0, v3, v2

    aput-object v1, v3, v4

    iput-object v3, p0, Lcom/facebook/offers/views/OffersTabbedBar;->a:[Lcom/facebook/resources/ui/FbTextView;

    .line 2432959
    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setSelected(Z)V

    .line 2432960
    new-instance v1, LX/H85;

    invoke-direct {v1, p0}, LX/H85;-><init>(Lcom/facebook/offers/views/OffersTabbedBar;)V

    .line 2432961
    iget-object v3, p0, Lcom/facebook/offers/views/OffersTabbedBar;->a:[Lcom/facebook/resources/ui/FbTextView;

    array-length v4, v3

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v2, v3, v0

    .line 2432962
    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2432963
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2432964
    :cond_0
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2432965
    iget-object v0, p0, Lcom/facebook/offers/views/OffersTabbedBar;->a:[Lcom/facebook/resources/ui/FbTextView;

    array-length v0, v0

    iget-object v2, p0, Lcom/facebook/offers/views/OffersTabbedBar;->d:[LX/H86;

    array-length v2, v2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "The number of TabTypes should equal the number of tabs!"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2432966
    :goto_1
    iget-object v0, p0, Lcom/facebook/offers/views/OffersTabbedBar;->a:[Lcom/facebook/resources/ui/FbTextView;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 2432967
    iget-object v0, p0, Lcom/facebook/offers/views/OffersTabbedBar;->a:[Lcom/facebook/resources/ui/FbTextView;

    aget-object v0, v0, v1

    .line 2432968
    iget-object v2, p0, Lcom/facebook/offers/views/OffersTabbedBar;->d:[LX/H86;

    aget-object v2, v2, v1

    invoke-virtual {v2}, LX/H86;->getTabNameStringRes()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2432969
    iget-object v2, p0, Lcom/facebook/offers/views/OffersTabbedBar;->d:[LX/H86;

    aget-object v2, v2, v1

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setTag(Ljava/lang/Object;)V

    .line 2432970
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 2432971
    goto :goto_0

    .line 2432972
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(LX/H86;)Z
    .locals 1

    .prologue
    .line 2432982
    iget-object v0, p0, Lcom/facebook/offers/views/OffersTabbedBar;->c:LX/H86;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelectedTabType()LX/H86;
    .locals 1

    .prologue
    .line 2432943
    iget-object v0, p0, Lcom/facebook/offers/views/OffersTabbedBar;->c:LX/H86;

    return-object v0
.end method

.method public setOnTabChangeListener(LX/H6M;)V
    .locals 0

    .prologue
    .line 2432944
    iput-object p1, p0, Lcom/facebook/offers/views/OffersTabbedBar;->b:LX/H6M;

    .line 2432945
    return-void
.end method

.method public setSelected(LX/H86;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2432946
    iget-object v3, p0, Lcom/facebook/offers/views/OffersTabbedBar;->a:[Lcom/facebook/resources/ui/FbTextView;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 2432947
    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v5, v0}, Landroid/view/View;->setSelected(Z)V

    .line 2432948
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 2432949
    goto :goto_1

    .line 2432950
    :cond_1
    iput-object p1, p0, Lcom/facebook/offers/views/OffersTabbedBar;->c:LX/H86;

    .line 2432951
    return-void
.end method

.method public setTabTypes([LX/H86;)V
    .locals 0

    .prologue
    .line 2432952
    iput-object p1, p0, Lcom/facebook/offers/views/OffersTabbedBar;->d:[LX/H86;

    .line 2432953
    invoke-direct {p0}, Lcom/facebook/offers/views/OffersTabbedBar;->b()V

    .line 2432954
    return-void
.end method
