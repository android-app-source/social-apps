.class public Lcom/facebook/offers/views/OfferExpirationTimerView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# instance fields
.field public a:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/11T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Ljava/text/SimpleDateFormat;

.field private d:Landroid/os/CountDownTimer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2432923
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 2432924
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->e:J

    .line 2432925
    invoke-direct {p0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->a()V

    .line 2432926
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2432919
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2432920
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->e:J

    .line 2432921
    invoke-direct {p0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->a()V

    .line 2432922
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2432915
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2432916
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->e:J

    .line 2432917
    invoke-direct {p0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->a()V

    .line 2432918
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2432912
    const-class v0, Lcom/facebook/offers/views/OfferExpirationTimerView;

    invoke-static {v0, p0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2432913
    iget-object v0, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->b:LX/11T;

    invoke-virtual {v0}, LX/11T;->h()Ljava/text/SimpleDateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->c:Ljava/text/SimpleDateFormat;

    .line 2432914
    return-void
.end method

.method private static a(Lcom/facebook/offers/views/OfferExpirationTimerView;LX/0SG;LX/11T;)V
    .locals 0

    .prologue
    .line 2432911
    iput-object p1, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->a:LX/0SG;

    iput-object p2, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->b:LX/11T;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/offers/views/OfferExpirationTimerView;

    invoke-static {v1}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {v1}, LX/11T;->a(LX/0QB;)LX/11T;

    move-result-object v1

    check-cast v1, LX/11T;

    invoke-static {p0, v0, v1}, Lcom/facebook/offers/views/OfferExpirationTimerView;->a(Lcom/facebook/offers/views/OfferExpirationTimerView;LX/0SG;LX/11T;)V

    return-void
.end method

.method public static b(Lcom/facebook/offers/views/OfferExpirationTimerView;)V
    .locals 10

    .prologue
    .line 2432898
    iget-wide v0, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->e:J

    iget-object v2, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 2432899
    const-wide/32 v2, 0x36ee80

    div-long v2, v0, v2

    .line 2432900
    const-wide/32 v4, 0x36ee80

    mul-long/2addr v4, v2

    sub-long v4, v0, v4

    const-wide/32 v6, 0xea60

    div-long/2addr v4, v6

    .line 2432901
    const-wide/32 v6, 0x36ee80

    mul-long/2addr v6, v2

    sub-long/2addr v0, v6

    const-wide/32 v6, 0xea60

    mul-long/2addr v6, v4

    sub-long/2addr v0, v6

    const-wide/16 v6, 0x3e8

    div-long/2addr v0, v6

    .line 2432902
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-lez v6, :cond_0

    .line 2432903
    invoke-virtual {p0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f081cdb

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v8, v9

    const/4 v2, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v8, v2

    const/4 v2, 0x2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->setText(Ljava/lang/CharSequence;)V

    .line 2432904
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->invalidate()V

    .line 2432905
    return-void

    .line 2432906
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, v4, v2

    if-lez v2, :cond_1

    .line 2432907
    invoke-virtual {p0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f081cdc

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v6, v7

    const/4 v4, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v6, v4

    invoke-virtual {v2, v3, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2432908
    :cond_1
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 2432909
    invoke-virtual {p0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f081cdd

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2432910
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f081cda

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x690dc0e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2432874
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->onDetachedFromWindow()V

    .line 2432875
    iget-object v1, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->d:Landroid/os/CountDownTimer;

    if-eqz v1, :cond_0

    .line 2432876
    iget-object v1, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->d:Landroid/os/CountDownTimer;

    invoke-virtual {v1}, Landroid/os/CountDownTimer;->cancel()V

    .line 2432877
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->d:Landroid/os/CountDownTimer;

    .line 2432878
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x2daef561

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setExpiresOn(J)V
    .locals 13

    .prologue
    const-wide/32 v10, 0x5265c00

    const-wide/16 v4, 0x3e8

    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 2432879
    mul-long v0, p1, v4

    iput-wide v0, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->e:J

    .line 2432880
    iget-wide v0, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->e:J

    iget-object v2, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long v2, v0, v2

    .line 2432881
    iget-object v0, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->d:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 2432882
    iget-object v0, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->d:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 2432883
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->d:Landroid/os/CountDownTimer;

    .line 2432884
    const-wide/32 v0, 0x240c8400

    cmp-long v0, v2, v0

    if-lez v0, :cond_1

    .line 2432885
    invoke-virtual {p0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f081cd6

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->c:Ljava/text/SimpleDateFormat;

    new-instance v4, Ljava/util/Date;

    iget-wide v6, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->e:J

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->setText(Ljava/lang/CharSequence;)V

    .line 2432886
    :goto_0
    return-void

    .line 2432887
    :cond_1
    const-wide/32 v0, 0x1ee62800

    cmp-long v0, v2, v0

    if-lez v0, :cond_2

    .line 2432888
    invoke-virtual {p0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f081cd8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2432889
    :cond_2
    const-wide/32 v0, 0xa4cb800

    cmp-long v0, v2, v0

    if-lez v0, :cond_3

    .line 2432890
    div-long v0, v2, v10

    .line 2432891
    invoke-virtual {p0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f081cd7

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v8

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2432892
    :cond_3
    cmp-long v0, v2, v10

    if-lez v0, :cond_4

    .line 2432893
    invoke-virtual {p0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f081cd9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2432894
    :cond_4
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-lez v0, :cond_5

    .line 2432895
    new-instance v0, LX/H84;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/H84;-><init>(Lcom/facebook/offers/views/OfferExpirationTimerView;JJ)V

    iput-object v0, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->d:Landroid/os/CountDownTimer;

    .line 2432896
    iget-object v0, p0, Lcom/facebook/offers/views/OfferExpirationTimerView;->d:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    goto :goto_0

    .line 2432897
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f081cda

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
