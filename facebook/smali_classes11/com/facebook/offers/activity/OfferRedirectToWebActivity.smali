.class public Lcom/facebook/offers/activity/OfferRedirectToWebActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field public p:LX/H5w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/H6T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2425530
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/offers/activity/OfferRedirectToWebActivity;LX/H5w;LX/H6T;)V
    .locals 0

    .prologue
    .line 2425549
    iput-object p1, p0, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;->p:LX/H5w;

    iput-object p2, p0, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;->q:LX/H6T;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;

    invoke-static {v1}, LX/H5w;->b(LX/0QB;)LX/H5w;

    move-result-object v0

    check-cast v0, LX/H5w;

    invoke-static {v1}, LX/H6T;->a(LX/0QB;)LX/H6T;

    move-result-object v1

    check-cast v1, LX/H6T;

    invoke-static {p0, v0, v1}, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;->a(Lcom/facebook/offers/activity/OfferRedirectToWebActivity;LX/H5w;LX/H6T;)V

    return-void
.end method

.method private b()V
    .locals 8

    .prologue
    .line 2425547
    iget-object v0, p0, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;->q:LX/H6T;

    iget-object v2, p0, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;->t:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;->u:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;->v:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;->r:Ljava/lang/String;

    iget-object v7, p0, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;->s:Ljava/lang/String;

    move-object v1, p0

    invoke-virtual/range {v0 .. v7}, LX/H6T;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2425548
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2425546
    const-string v0, "offers_web_redirect_page"

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2425531
    invoke-static {p0, p0}, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2425532
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2425533
    invoke-virtual {p0}, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 2425534
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 2425535
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2425536
    invoke-virtual {p0}, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2425537
    const-string v1, "offer_view_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;->r:Ljava/lang/String;

    .line 2425538
    const-string v1, "share_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;->s:Ljava/lang/String;

    .line 2425539
    const-string v1, "site_uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;->t:Ljava/lang/String;

    .line 2425540
    const-string v1, "offer_code"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;->u:Ljava/lang/String;

    .line 2425541
    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;->v:Ljava/lang/String;

    .line 2425542
    iget-object v1, p0, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;->p:LX/H5w;

    invoke-virtual {v1, p0, v0}, LX/H5w;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    .line 2425543
    invoke-direct {p0}, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;->b()V

    .line 2425544
    invoke-virtual {p0}, Lcom/facebook/offers/activity/OfferRedirectToWebActivity;->finish()V

    .line 2425545
    return-void
.end method
