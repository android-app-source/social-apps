.class public Lcom/facebook/offers/activity/OffersCoverHeaderView;
.super LX/Ban;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public n:LX/Bas;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private o:LX/H83;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2425577
    const-class v0, Lcom/facebook/offers/activity/OffersCoverHeaderView;

    const-string v1, "offers_detail_page"

    const-string v2, "cover_photo"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/offers/activity/OffersCoverHeaderView;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2425574
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Ban;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2425575
    invoke-direct {p0}, Lcom/facebook/offers/activity/OffersCoverHeaderView;->b()V

    .line 2425576
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2425571
    invoke-direct {p0, p1, p2, p3}, LX/Ban;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2425572
    invoke-direct {p0}, Lcom/facebook/offers/activity/OffersCoverHeaderView;->b()V

    .line 2425573
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/offers/activity/OffersCoverHeaderView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/offers/activity/OffersCoverHeaderView;

    invoke-static {v0}, LX/Bas;->b(LX/0QB;)LX/Bas;

    move-result-object v0

    check-cast v0, LX/Bas;

    iput-object v0, p0, Lcom/facebook/offers/activity/OffersCoverHeaderView;->n:LX/Bas;

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2425561
    const-class v0, Lcom/facebook/offers/activity/OffersCoverHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/offers/activity/OffersCoverHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2425562
    sget-object v0, LX/Bam;->NARROW:LX/Bam;

    iput-object v0, p0, Lcom/facebook/offers/activity/OffersCoverHeaderView;->d:LX/Bam;

    .line 2425563
    sget-object v0, LX/Bap;->IMAGE:LX/Bap;

    invoke-virtual {p0, v0}, LX/Ban;->setCoverType(LX/Bap;)V

    .line 2425564
    iget-object v0, p0, LX/Ban;->l:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->setVisibility(I)V

    .line 2425565
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {p0}, Lcom/facebook/offers/activity/OffersCoverHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b132d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setMinimumHeight(I)V

    .line 2425566
    iget-object v0, p0, LX/Ban;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 2425567
    iget-object v0, p0, LX/Ban;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2425568
    invoke-virtual {p0}, LX/Ban;->f()V

    .line 2425569
    invoke-virtual {p0}, LX/Ban;->e()V

    .line 2425570
    return-void
.end method

.method private g()V
    .locals 15

    .prologue
    const/4 v3, 0x1

    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 2425556
    new-instance v6, Landroid/graphics/PointF;

    invoke-direct {v6, v0, v0}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2425557
    invoke-virtual {p0}, LX/Ban;->f()V

    .line 2425558
    iget-object v0, p0, Lcom/facebook/offers/activity/OffersCoverHeaderView;->o:LX/H83;

    invoke-virtual {v0}, LX/H83;->w()LX/H72;

    move-result-object v0

    invoke-interface {v0}, LX/H72;->H()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;->a()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v8, v0, LX/1vs;->b:I

    .line 2425559
    iget-object v0, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {p0}, LX/Ban;->getScreenWidth()I

    move-result v1

    iget v2, p0, LX/Ban;->c:I

    invoke-virtual {v5, v8, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v5

    sget-object v10, Lcom/facebook/offers/activity/OffersCoverHeaderView;->m:Lcom/facebook/common/callercontext/CallerContext;

    move v8, v7

    move-object v9, v4

    move-object v11, v4

    move-object v12, v4

    move v13, v7

    move v14, v3

    invoke-virtual/range {v0 .. v14}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a(IIZLX/1bf;LX/1bf;Landroid/graphics/PointF;ZZ[Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;LX/1cC;ZZ)V

    .line 2425560
    return-void
.end method


# virtual methods
.method public final a(II)I
    .locals 2

    .prologue
    .line 2425555
    invoke-virtual {p0}, Lcom/facebook/offers/activity/OffersCoverHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1332

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2425550
    invoke-super {p0, p1}, LX/Ban;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2425551
    invoke-virtual {p0}, Lcom/facebook/offers/activity/OffersCoverHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/facebook/offers/activity/OffersCoverHeaderView;->b:I

    .line 2425552
    iget-object v0, p0, Lcom/facebook/offers/activity/OffersCoverHeaderView;->o:LX/H83;

    if-eqz v0, :cond_0

    .line 2425553
    invoke-direct {p0}, Lcom/facebook/offers/activity/OffersCoverHeaderView;->g()V

    .line 2425554
    :cond_0
    return-void
.end method
