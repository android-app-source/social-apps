.class public final Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5baeb8a6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2427415
    const-class v0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2427416
    const-class v0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2427417
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2427418
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2427419
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2427420
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2427421
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2427422
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2427423
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2427424
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2427425
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2427426
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2427427
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    .line 2427428
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2427429
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel;

    .line 2427430
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel;->e:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    .line 2427431
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2427432
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOfferClaim"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2427433
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel;->e:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel;->e:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    .line 2427434
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel;->e:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2427435
    new-instance v0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel;

    invoke-direct {v0}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel;-><init>()V

    .line 2427436
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2427437
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2427438
    const v0, -0x60ed568a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2427439
    const v0, 0x6fcf72b1

    return v0
.end method
