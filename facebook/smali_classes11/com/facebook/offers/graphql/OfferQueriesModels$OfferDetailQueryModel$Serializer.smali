.class public final Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2428773
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    new-instance v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2428774
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2428772
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;LX/0nX;LX/0my;)V
    .locals 9

    .prologue
    .line 2428623
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2428624
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v8, 0x5

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    .line 2428625
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2428626
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 2428627
    if-eqz v2, :cond_0

    .line 2428628
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428629
    invoke-static {v1, v0, v5, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2428630
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2428631
    if-eqz v2, :cond_1

    .line 2428632
    const-string v3, "__typename"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428633
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2428634
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2428635
    if-eqz v2, :cond_2

    .line 2428636
    const-string v3, "app_data_for_offer"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428637
    invoke-static {v1, v2, p1, p2}, LX/H7V;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2428638
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2428639
    if-eqz v2, :cond_3

    .line 2428640
    const-string v3, "claim_status"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428641
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2428642
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 2428643
    cmp-long v4, v2, v6

    if-eqz v4, :cond_4

    .line 2428644
    const-string v4, "claim_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428645
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 2428646
    :cond_4
    invoke-virtual {v1, v0, v8}, LX/15i;->g(II)I

    move-result v2

    .line 2428647
    if-eqz v2, :cond_5

    .line 2428648
    const-string v2, "coupon_claim_location"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428649
    invoke-virtual {v1, v0, v8}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2428650
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2428651
    if-eqz v2, :cond_6

    .line 2428652
    const-string v3, "discount_barcode_image"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428653
    invoke-static {v1, v2, p1, p2}, LX/H7x;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2428654
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2428655
    if-eqz v2, :cond_7

    .line 2428656
    const-string v3, "discount_barcode_type"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428657
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2428658
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2428659
    if-eqz v2, :cond_8

    .line 2428660
    const-string v3, "discount_barcode_value"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428661
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2428662
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 2428663
    cmp-long v4, v2, v6

    if-eqz v4, :cond_9

    .line 2428664
    const-string v4, "expiration_date"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428665
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 2428666
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 2428667
    if-eqz v2, :cond_a

    .line 2428668
    const-string v3, "filtered_claim_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428669
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 2428670
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2428671
    if-eqz v2, :cond_b

    .line 2428672
    const-string v3, "has_viewer_claimed"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428673
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2428674
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2428675
    if-eqz v2, :cond_c

    .line 2428676
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428677
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2428678
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2428679
    if-eqz v2, :cond_d

    .line 2428680
    const-string v3, "instore_offer_code"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428681
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2428682
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2428683
    if-eqz v2, :cond_e

    .line 2428684
    const-string v3, "is_active"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428685
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2428686
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2428687
    if-eqz v2, :cond_f

    .line 2428688
    const-string v3, "is_expired"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428689
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2428690
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2428691
    if-eqz v2, :cond_10

    .line 2428692
    const-string v3, "is_stopped"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428693
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2428694
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2428695
    if-eqz v2, :cond_11

    .line 2428696
    const-string v3, "is_used"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428697
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2428698
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2428699
    if-eqz v2, :cond_12

    .line 2428700
    const-string v3, "message"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428701
    invoke-static {v1, v2, p1}, LX/H7W;->a(LX/15i;ILX/0nX;)V

    .line 2428702
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2428703
    if-eqz v2, :cond_13

    .line 2428704
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428705
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2428706
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2428707
    if-eqz v2, :cond_14

    .line 2428708
    const-string v3, "notification_email"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428709
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2428710
    :cond_14
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2428711
    if-eqz v2, :cond_15

    .line 2428712
    const-string v3, "notifications_enabled"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428713
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2428714
    :cond_15
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2428715
    if-eqz v2, :cond_16

    .line 2428716
    const-string v3, "offer"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428717
    invoke-static {v1, v2, p1, p2}, LX/H7c;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2428718
    :cond_16
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2428719
    if-eqz v2, :cond_17

    .line 2428720
    const-string v3, "offer_view"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428721
    invoke-static {v1, v2, p1, p2}, LX/H7r;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2428722
    :cond_17
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2428723
    if-eqz v2, :cond_18

    .line 2428724
    const-string v3, "online_offer_code"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428725
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2428726
    :cond_18
    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2428727
    if-eqz v2, :cond_19

    .line 2428728
    const-string v3, "owning_page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428729
    invoke-static {v1, v2, p1, p2}, LX/H7l;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2428730
    :cond_19
    const/16 v2, 0x1a

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2428731
    if-eqz v2, :cond_1a

    .line 2428732
    const-string v3, "photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428733
    invoke-static {v1, v2, p1, p2}, LX/H7X;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2428734
    :cond_1a
    const/16 v2, 0x1b

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2428735
    if-eqz v2, :cond_1b

    .line 2428736
    const-string v3, "photos"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428737
    invoke-static {v1, v2, p1, p2}, LX/H7x;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2428738
    :cond_1b
    const/16 v2, 0x1c

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2428739
    if-eqz v2, :cond_1c

    .line 2428740
    const-string v3, "redemption_code"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428741
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2428742
    :cond_1c
    const/16 v2, 0x1d

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2428743
    if-eqz v2, :cond_1d

    .line 2428744
    const-string v3, "redemption_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428745
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2428746
    :cond_1d
    const/16 v2, 0x1e

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2428747
    if-eqz v2, :cond_1e

    .line 2428748
    const-string v3, "root_share_story"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428749
    invoke-static {v1, v2, p1, p2}, LX/H7q;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2428750
    :cond_1e
    const/16 v2, 0x1f

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2428751
    if-eqz v2, :cond_1f

    .line 2428752
    const-string v3, "share_story"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428753
    invoke-static {v1, v2, p1, p2}, LX/H7q;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2428754
    :cond_1f
    const/16 v2, 0x20

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2428755
    if-eqz v2, :cond_20

    .line 2428756
    const-string v3, "terms"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428757
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2428758
    :cond_20
    const/16 v2, 0x21

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2428759
    if-eqz v2, :cond_21

    .line 2428760
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428761
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2428762
    :cond_21
    const/16 v2, 0x22

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2428763
    if-eqz v2, :cond_22

    .line 2428764
    const-string v3, "videos"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428765
    invoke-static {v1, v2, p1, p2}, LX/H80;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2428766
    :cond_22
    const/16 v2, 0x23

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2428767
    if-eqz v2, :cond_23

    .line 2428768
    const-string v3, "viewer_claim"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2428769
    invoke-static {v1, v2, p1, p2}, LX/H7b;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2428770
    :cond_23
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2428771
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2428622
    check-cast p1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel$Serializer;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
