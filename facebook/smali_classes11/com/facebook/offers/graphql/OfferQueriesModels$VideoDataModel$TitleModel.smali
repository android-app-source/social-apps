.class public final Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5e4765a0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2430619
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2430618
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2430616
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2430617
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2430613
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2430614
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2430615
    return-void
.end method

.method public static a(Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;
    .locals 9

    .prologue
    .line 2430590
    if-nez p0, :cond_0

    .line 2430591
    const/4 p0, 0x0

    .line 2430592
    :goto_0
    return-object p0

    .line 2430593
    :cond_0
    instance-of v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;

    if-eqz v0, :cond_1

    .line 2430594
    check-cast p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;

    goto :goto_0

    .line 2430595
    :cond_1
    new-instance v0, LX/H7T;

    invoke-direct {v0}, LX/H7T;-><init>()V

    .line 2430596
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H7T;->a:Ljava/lang/String;

    .line 2430597
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H7T;->b:Ljava/lang/String;

    .line 2430598
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 2430599
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 2430600
    iget-object v3, v0, LX/H7T;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2430601
    iget-object v5, v0, LX/H7T;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2430602
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 2430603
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 2430604
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 2430605
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 2430606
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 2430607
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 2430608
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2430609
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2430610
    new-instance v3, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;

    invoke-direct {v3, v2}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;-><init>(LX/15i;)V

    .line 2430611
    move-object p0, v3

    .line 2430612
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2430582
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2430583
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2430584
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2430585
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2430586
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2430587
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2430588
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2430589
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2430579
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2430580
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2430581
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430577
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;->e:Ljava/lang/String;

    .line 2430578
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2430570
    new-instance v0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;

    invoke-direct {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;-><init>()V

    .line 2430571
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2430572
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430575
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;->f:Ljava/lang/String;

    .line 2430576
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2430574
    const v0, -0x492716d7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2430573
    const v0, -0x726d476c

    return v0
.end method
