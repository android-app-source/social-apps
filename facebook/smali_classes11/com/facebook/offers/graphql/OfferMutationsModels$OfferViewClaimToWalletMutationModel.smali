.class public final Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5baeb8a6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2427495
    const-class v0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2427494
    const-class v0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2427492
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2427493
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2427486
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2427487
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2427488
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2427489
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2427490
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2427491
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2427496
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2427497
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2427498
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    .line 2427499
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2427500
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;

    .line 2427501
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;->e:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    .line 2427502
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2427503
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOfferClaim"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2427484
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;->e:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;->e:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    .line 2427485
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;->e:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2427481
    new-instance v0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;

    invoke-direct {v0}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;-><init>()V

    .line 2427482
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2427483
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2427480
    const v0, 0xc43c96e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2427479
    const v0, -0x174ab722

    return v0
.end method
