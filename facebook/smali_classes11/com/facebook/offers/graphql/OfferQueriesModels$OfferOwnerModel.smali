.class public final Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/H74;
.implements LX/H73;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x50e9b25b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2429483
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2429482
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2429480
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2429481
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2429477
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2429478
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2429479
    return-void
.end method

.method public static a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2429455
    if-nez p0, :cond_0

    .line 2429456
    const/4 p0, 0x0

    .line 2429457
    :goto_0
    return-object p0

    .line 2429458
    :cond_0
    instance-of v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    if-eqz v0, :cond_1

    .line 2429459
    check-cast p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    goto :goto_0

    .line 2429460
    :cond_1
    new-instance v0, LX/H7E;

    invoke-direct {v0}, LX/H7E;-><init>()V

    .line 2429461
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->mi_()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, v0, LX/H7E;->a:LX/15i;

    iput v1, v0, LX/H7E;->b:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2429462
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->mj_()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    move-result-object v1

    iput-object v1, v0, LX/H7E;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    .line 2429463
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H7E;->d:Ljava/lang/String;

    .line 2429464
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iput-object v2, v0, LX/H7E;->e:LX/15i;

    iput v1, v0, LX/H7E;->f:I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2429465
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H7E;->g:Ljava/lang/String;

    .line 2429466
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_2
    iput-object v2, v0, LX/H7E;->h:LX/15i;

    iput v1, v0, LX/H7E;->i:I

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2429467
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->l()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_3
    iput-object v2, v0, LX/H7E;->j:LX/15i;

    iput v1, v0, LX/H7E;->k:I

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2429468
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->d()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v1

    iput-object v1, v0, LX/H7E;->l:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 2429469
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->m()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_4
    iput-object v2, v0, LX/H7E;->m:LX/15i;

    iput v1, v0, LX/H7E;->n:I

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 2429470
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H7E;->o:Ljava/lang/String;

    .line 2429471
    invoke-virtual {v0}, LX/H7E;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object p0

    goto/16 :goto_0

    .line 2429472
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v0

    .line 2429473
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 2429474
    :catchall_2
    move-exception v0

    :try_start_7
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v0

    .line 2429475
    :catchall_3
    move-exception v0

    :try_start_8
    monitor-exit v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    throw v0

    .line 2429476
    :catchall_4
    move-exception v0

    :try_start_9
    monitor-exit v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    throw v0
.end method

.method private n()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getChildLocations"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429453
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->f:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->f:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    .line 2429454
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->f:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 2429429
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2429430
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->mi_()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x614b33f6

    invoke-static {v1, v0, v2}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2429431
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->n()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2429432
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2429433
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->j()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x7f68b3a0

    invoke-static {v4, v3, v5}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2429434
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2429435
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->k()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    const v7, -0xea5f3c

    invoke-static {v6, v5, v7}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2429436
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->l()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v8, 0x469d319

    invoke-static {v7, v6, v8}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2429437
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->d()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 2429438
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->m()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    const v10, 0xb8750f7

    invoke-static {v9, v8, v10}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2429439
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->e()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 2429440
    const/16 v10, 0xa

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2429441
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 2429442
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2429443
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2429444
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2429445
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2429446
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2429447
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2429448
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2429449
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2429450
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2429451
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2429452
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2429390
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2429391
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->mi_()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2429392
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->mi_()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x614b33f6

    invoke-static {v2, v0, v3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2429393
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->mi_()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2429394
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    .line 2429395
    iput v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->e:I

    move-object v1, v0

    .line 2429396
    :cond_0
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->n()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2429397
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->n()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    .line 2429398
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->n()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2429399
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    .line 2429400
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->f:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    .line 2429401
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2429402
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x7f68b3a0

    invoke-static {v2, v0, v3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2429403
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2429404
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    .line 2429405
    iput v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->h:I

    move-object v1, v0

    .line 2429406
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 2429407
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0xea5f3c

    invoke-static {v2, v0, v3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2429408
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2429409
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    .line 2429410
    iput v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->j:I

    move-object v1, v0

    .line 2429411
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 2429412
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x469d319

    invoke-static {v2, v0, v3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2429413
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2429414
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    .line 2429415
    iput v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->k:I

    move-object v1, v0

    .line 2429416
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_5

    .line 2429417
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0xb8750f7

    invoke-static {v2, v0, v3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 2429418
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2429419
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    .line 2429420
    iput v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->m:I

    move-object v1, v0

    .line 2429421
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2429422
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    .line 2429423
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v0

    .line 2429424
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 2429425
    :catchall_2
    move-exception v0

    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v0

    .line 2429426
    :catchall_3
    move-exception v0

    :try_start_8
    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    throw v0

    .line 2429427
    :catchall_4
    move-exception v0

    :try_start_9
    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    throw v0

    :cond_6
    move-object p0, v1

    .line 2429428
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2429389
    new-instance v0, LX/H7H;

    invoke-direct {v0, p1}, LX/H7H;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429388
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2429381
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2429382
    const/4 v0, 0x0

    const v1, 0x614b33f6

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->e:I

    .line 2429383
    const/4 v0, 0x3

    const v1, -0x7f68b3a0

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->h:I

    .line 2429384
    const/4 v0, 0x5

    const v1, -0xea5f3c

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->j:I

    .line 2429385
    const/4 v0, 0x6

    const v1, 0x469d319

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->k:I

    .line 2429386
    const/16 v0, 0x8

    const v1, 0xb8750f7

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->m:I

    .line 2429387
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2429379
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2429380
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2429484
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2429355
    new-instance v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    invoke-direct {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;-><init>()V

    .line 2429356
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2429357
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429358
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->g:Ljava/lang/String;

    .line 2429359
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429360
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->i:Ljava/lang/String;

    .line 2429361
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429362
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->l:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->l:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 2429363
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->l:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2429364
    const v0, 0x6e33cbd3

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429365
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->n:Ljava/lang/String;

    .line 2429366
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2429367
    const v0, 0x25d6af

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLocation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429368
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2429369
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPlaceCurrentOpenHours"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429370
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2429371
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->j:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPlaceOpenStatus"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429372
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2429373
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->k:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429374
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2429375
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->m:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final mi_()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAddress"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2429376
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2429377
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic mj_()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getChildLocations"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429378
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->n()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    move-result-object v0

    return-object v0
.end method
