.class public final Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2e458655
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2429867
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2429868
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2429869
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2429870
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2429871
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2429872
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2429873
    return-void
.end method

.method public static a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
    .locals 12
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2429874
    if-nez p0, :cond_0

    .line 2429875
    const/4 p0, 0x0

    .line 2429876
    :goto_0
    return-object p0

    .line 2429877
    :cond_0
    instance-of v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    if-eqz v0, :cond_1

    .line 2429878
    check-cast p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    goto :goto_0

    .line 2429879
    :cond_1
    new-instance v2, LX/H7M;

    invoke-direct {v2}, LX/H7M;-><init>()V

    .line 2429880
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2429881
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2429882
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;

    invoke-static {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2429883
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2429884
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/H7M;->a:LX/0Px;

    .line 2429885
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/H7M;->b:Ljava/lang/String;

    .line 2429886
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->c()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;

    move-result-object v0

    iput-object v0, v2, LX/H7M;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;

    .line 2429887
    const/4 v8, 0x1

    const/4 v11, 0x0

    const/4 v6, 0x0

    .line 2429888
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 2429889
    iget-object v5, v2, LX/H7M;->a:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 2429890
    iget-object v7, v2, LX/H7M;->b:Ljava/lang/String;

    invoke-virtual {v4, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2429891
    iget-object v9, v2, LX/H7M;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;

    invoke-static {v4, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2429892
    const/4 v10, 0x3

    invoke-virtual {v4, v10}, LX/186;->c(I)V

    .line 2429893
    invoke-virtual {v4, v11, v5}, LX/186;->b(II)V

    .line 2429894
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 2429895
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v9}, LX/186;->b(II)V

    .line 2429896
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 2429897
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 2429898
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 2429899
    invoke-virtual {v5, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2429900
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2429901
    new-instance v5, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    invoke-direct {v5, v4}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;-><init>(LX/15i;)V

    .line 2429902
    move-object p0, v5

    .line 2429903
    goto/16 :goto_0
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2429904
    iput-object p1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->e:Ljava/util/List;

    .line 2429905
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2429906
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2429907
    if-eqz v0, :cond_0

    .line 2429908
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/util/List;)V

    .line 2429909
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429910
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;

    .line 2429911
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2429912
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2429913
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->d()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2429914
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2429915
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2429916
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2429917
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2429918
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2429919
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2429920
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2429921
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2429853
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2429854
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->d()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2429855
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->d()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2429856
    if-eqz v1, :cond_2

    .line 2429857
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2429858
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 2429859
    :goto_0
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2429860
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;

    .line 2429861
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2429862
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2429863
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;

    .line 2429864
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2429865
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2429836
    new-instance v0, LX/H7N;

    invoke-direct {v0, p1}, LX/H7N;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429866
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2429837
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2429838
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2429839
    const-string v0, "attachments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2429840
    check-cast p2, Ljava/util/List;

    invoke-direct {p0, p2}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->a(Ljava/util/List;)V

    .line 2429841
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2429842
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2429843
    new-instance v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    invoke-direct {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;-><init>()V

    .line 2429844
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2429845
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429846
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->f:Ljava/lang/String;

    .line 2429847
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429848
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAttachments"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2429849
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->e:Ljava/util/List;

    .line 2429850
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2429851
    const v0, -0x7b6a833e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2429852
    const v0, 0x4c808d5

    return v0
.end method
