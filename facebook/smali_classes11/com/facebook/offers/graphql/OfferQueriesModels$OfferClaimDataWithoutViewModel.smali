.class public final Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/H6z;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xd205135
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J

.field private g:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Z

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Z

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2428401
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2428376
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2428377
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2428378
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2428379
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2428380
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2428381
    return-void
.end method

.method public static a(LX/H6z;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2428382
    if-nez p0, :cond_0

    .line 2428383
    const/4 p0, 0x0

    .line 2428384
    :goto_0
    return-object p0

    .line 2428385
    :cond_0
    instance-of v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    if-eqz v0, :cond_1

    .line 2428386
    check-cast p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    goto :goto_0

    .line 2428387
    :cond_1
    new-instance v0, LX/H79;

    invoke-direct {v0}, LX/H79;-><init>()V

    .line 2428388
    invoke-interface {p0}, LX/H6x;->t()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H79;->a:Ljava/lang/String;

    .line 2428389
    invoke-interface {p0}, LX/H6x;->u()J

    move-result-wide v2

    iput-wide v2, v0, LX/H79;->b:J

    .line 2428390
    invoke-interface {p0}, LX/H6z;->C()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v1

    iput-object v1, v0, LX/H79;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    .line 2428391
    invoke-interface {p0}, LX/H6x;->v()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H79;->d:Ljava/lang/String;

    .line 2428392
    invoke-interface {p0}, LX/H6x;->w()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H79;->e:Ljava/lang/String;

    .line 2428393
    invoke-interface {p0}, LX/H6x;->me_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H79;->f:Ljava/lang/String;

    .line 2428394
    invoke-interface {p0}, LX/H6x;->x()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H79;->g:Ljava/lang/String;

    .line 2428395
    invoke-interface {p0}, LX/H6x;->y()Z

    move-result v1

    iput-boolean v1, v0, LX/H79;->h:Z

    .line 2428396
    invoke-interface {p0}, LX/H6x;->z()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H79;->i:Ljava/lang/String;

    .line 2428397
    invoke-interface {p0}, LX/H6x;->A()Z

    move-result v1

    iput-boolean v1, v0, LX/H79;->j:Z

    .line 2428398
    invoke-interface {p0}, LX/H6x;->B()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H79;->k:Ljava/lang/String;

    .line 2428399
    invoke-interface {p0}, LX/H6z;->E()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/H79;->l:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2428400
    invoke-virtual {v0}, LX/H79;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    move-result-object p0

    goto :goto_0
.end method

.method private j()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDiscountBarcodeImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428360
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    .line 2428361
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    return-object v0
.end method

.method private k()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getShareStory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428402
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->p:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->p:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2428403
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->p:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final A()Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 2428404
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428405
    iget-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->n:Z

    return v0
.end method

.method public final B()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428406
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->o:Ljava/lang/String;

    .line 2428407
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic C()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDiscountBarcodeImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428408
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic E()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getShareStory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428409
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->k()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 14

    .prologue
    .line 2428410
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2428411
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->t()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2428412
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2428413
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2428414
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->w()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2428415
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->me_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 2428416
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 2428417
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->z()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 2428418
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->B()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 2428419
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->k()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 2428420
    const/16 v1, 0xc

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2428421
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2428422
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->f:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2428423
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2428424
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2428425
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2428426
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2428427
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 2428428
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2428429
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 2428430
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->n:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2428431
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 2428432
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v13}, LX/186;->b(II)V

    .line 2428433
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2428434
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2428362
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2428363
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2428364
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    .line 2428365
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2428366
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    .line 2428367
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    .line 2428368
    :cond_0
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->k()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2428369
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->k()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2428370
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->k()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2428371
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    .line 2428372
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->p:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2428373
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2428374
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428375
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->me_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2428334
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2428335
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->f:J

    .line 2428336
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->l:Z

    .line 2428337
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->n:Z

    .line 2428338
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2428339
    new-instance v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    invoke-direct {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;-><init>()V

    .line 2428340
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2428341
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2428342
    const v0, -0x2c6255d0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2428343
    const v0, 0x7ee69360

    return v0
.end method

.method public final me_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428344
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->j:Ljava/lang/String;

    .line 2428345
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428346
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->e:Ljava/lang/String;

    .line 2428347
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final u()J
    .locals 2

    .prologue
    .line 2428348
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428349
    iget-wide v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->f:J

    return-wide v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428350
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->h:Ljava/lang/String;

    .line 2428351
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final w()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428352
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->i:Ljava/lang/String;

    .line 2428353
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final x()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428354
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->k:Ljava/lang/String;

    .line 2428355
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final y()Z
    .locals 2

    .prologue
    .line 2428356
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428357
    iget-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->l:Z

    return v0
.end method

.method public final z()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428358
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->m:Ljava/lang/String;

    .line 2428359
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->m:Ljava/lang/String;

    return-object v0
.end method
