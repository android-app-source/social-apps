.class public final Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2427466
    const-class v0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;

    new-instance v1, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2427467
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2427468
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2427469
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2427470
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2427471
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2427472
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2427473
    if-eqz v2, :cond_0

    .line 2427474
    const-string p0, "offer_claim"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2427475
    invoke-static {v1, v2, p1, p2}, LX/H7a;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2427476
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2427477
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2427478
    check-cast p1, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel$Serializer;->a(Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
