.class public final Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2428101
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2428102
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2428099
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2428100
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    const/4 v6, 0x3

    const-wide/16 v4, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 2427997
    if-nez p1, :cond_0

    .line 2427998
    :goto_0
    return v1

    .line 2427999
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2428000
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2428001
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2428002
    const v2, -0x167a7a84

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2428003
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2428004
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2428005
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 2428006
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2428007
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 2428008
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2428009
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2428010
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2428011
    invoke-virtual {p0, p1, v8}, LX/15i;->p(II)I

    move-result v2

    .line 2428012
    const v3, 0xb8750f7

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2428013
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 2428014
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2428015
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 2428016
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2428017
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2428018
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2428019
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2428020
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2428021
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2428022
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2428023
    const v2, 0xb8750f7

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2428024
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2428025
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2428026
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2428027
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2428028
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2428029
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2428030
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2428031
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2428032
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2428033
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2428034
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2428035
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2428036
    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2428037
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2428038
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2428039
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2428040
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 2428041
    invoke-virtual {p3, v9, v3}, LX/186;->b(II)V

    .line 2428042
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2428043
    :sswitch_6
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2428044
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2428045
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    move-object v0, p3

    .line 2428046
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 2428047
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2428048
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2428049
    :sswitch_7
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2428050
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2428051
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2428052
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2428053
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2428054
    :sswitch_8
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2428055
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2428056
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2428057
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2428058
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2428059
    :sswitch_9
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2428060
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2428061
    invoke-virtual {p0, p1, v8}, LX/15i;->b(II)Z

    move-result v2

    .line 2428062
    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2428063
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2428064
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2428065
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2428066
    invoke-virtual {p3, v8, v2}, LX/186;->a(IZ)V

    .line 2428067
    invoke-virtual {p3, v9, v3}, LX/186;->b(II)V

    .line 2428068
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2428069
    :sswitch_a
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2428070
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2428071
    invoke-virtual {p0, p1, v8}, LX/15i;->b(II)Z

    move-result v2

    .line 2428072
    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2428073
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2428074
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2428075
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2428076
    invoke-virtual {p3, v8, v2}, LX/186;->a(IZ)V

    .line 2428077
    invoke-virtual {p3, v9, v3}, LX/186;->b(II)V

    .line 2428078
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2428079
    :sswitch_b
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2428080
    const v2, -0x48514013

    const/4 v4, 0x0

    .line 2428081
    if-nez v0, :cond_1

    move v3, v4

    .line 2428082
    :goto_1
    move v0, v3

    .line 2428083
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2428084
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2428085
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2428086
    :sswitch_c
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2428087
    const v2, 0xb8750f7

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2428088
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2428089
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2428090
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2428091
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v5

    .line 2428092
    if-nez v5, :cond_2

    const/4 v3, 0x0

    .line 2428093
    :goto_2
    if-ge v4, v5, :cond_3

    .line 2428094
    invoke-virtual {p0, v0, v4}, LX/15i;->q(II)I

    move-result v6

    .line 2428095
    invoke-static {p0, v6, v2, p3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v6

    aput v6, v3, v4

    .line 2428096
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 2428097
    :cond_2
    new-array v3, v5, [I

    goto :goto_2

    .line 2428098
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p3, v3, v4}, LX/186;->a([IZ)I

    move-result v3

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x7f68b3a0 -> :sswitch_6
        -0x48514013 -> :sswitch_c
        -0x167a7a84 -> :sswitch_1
        -0x75ba5ba -> :sswitch_9
        -0xea5f3c -> :sswitch_7
        0x469d319 -> :sswitch_8
        0x68ed5f3 -> :sswitch_0
        0xb8750f7 -> :sswitch_4
        0x25ccafee -> :sswitch_b
        0x420c3d46 -> :sswitch_3
        0x57feaeeb -> :sswitch_2
        0x5ee5c6ae -> :sswitch_a
        0x614b33f6 -> :sswitch_5
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2427996
    new-instance v0, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const v1, 0xb8750f7

    const/4 v0, 0x0

    .line 2427954
    sparse-switch p2, :sswitch_data_0

    .line 2427955
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2427956
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2427957
    const v1, -0x167a7a84

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2427958
    :goto_0
    :sswitch_1
    return-void

    .line 2427959
    :sswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2427960
    invoke-static {p0, v0, v1, p3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2427961
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2427962
    invoke-static {p0, v0, v1, p3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2427963
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2427964
    const v1, -0x48514013

    .line 2427965
    if-eqz v0, :cond_0

    .line 2427966
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2427967
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 2427968
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2427969
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2427970
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2427971
    :cond_0
    goto :goto_0

    .line 2427972
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2427973
    invoke-static {p0, v0, v1, p3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7f68b3a0 -> :sswitch_1
        -0x48514013 -> :sswitch_5
        -0x167a7a84 -> :sswitch_2
        -0x75ba5ba -> :sswitch_1
        -0xea5f3c -> :sswitch_1
        0x469d319 -> :sswitch_1
        0x68ed5f3 -> :sswitch_0
        0xb8750f7 -> :sswitch_1
        0x25ccafee -> :sswitch_4
        0x420c3d46 -> :sswitch_3
        0x57feaeeb -> :sswitch_1
        0x5ee5c6ae -> :sswitch_1
        0x614b33f6 -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2427990
    if-eqz p1, :cond_0

    .line 2427991
    invoke-static {p0, p1, p2}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v1

    .line 2427992
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    .line 2427993
    if-eq v0, v1, :cond_0

    .line 2427994
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2427995
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2427989
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2427987
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2427988
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2427982
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2427983
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2427984
    :cond_0
    iput-object p1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a:LX/15i;

    .line 2427985
    iput p2, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->b:I

    .line 2427986
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2427981
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2427980
    new-instance v0, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2427977
    iget v0, p0, LX/1vt;->c:I

    .line 2427978
    move v0, v0

    .line 2427979
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2427974
    iget v0, p0, LX/1vt;->c:I

    .line 2427975
    move v0, v0

    .line 2427976
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2427951
    iget v0, p0, LX/1vt;->b:I

    .line 2427952
    move v0, v0

    .line 2427953
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2427948
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2427949
    move-object v0, v0

    .line 2427950
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2427939
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2427940
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2427941
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2427942
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2427943
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2427944
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2427945
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2427946
    invoke-static {v3, v9, v2}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2427947
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2427936
    iget v0, p0, LX/1vt;->c:I

    .line 2427937
    move v0, v0

    .line 2427938
    return v0
.end method
