.class public final Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1c5d8f0c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2430354
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2430353
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2430351
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2430352
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430348
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2430349
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2430350
    :cond_0
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private l()Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getClaimedCoupons"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430346
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->f:Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->f:Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;

    .line 2430347
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->f:Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430344
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->h:Ljava/lang/String;

    .line 2430345
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2430332
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2430333
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2430334
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->l()Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2430335
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2430336
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2430337
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2430338
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2430339
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2430340
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2430341
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2430342
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2430343
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2430319
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2430320
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->l()Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2430321
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->l()Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;

    .line 2430322
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->l()Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2430323
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;

    .line 2430324
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->f:Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;

    .line 2430325
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2430326
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel;

    .line 2430327
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2430328
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;

    .line 2430329
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel;

    .line 2430330
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2430331
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2430318
    new-instance v0, LX/H7Q;

    invoke-direct {v0, p1}, LX/H7Q;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430307
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2430316
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2430317
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2430315
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2430312
    new-instance v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;

    invoke-direct {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;-><init>()V

    .line 2430313
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2430314
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2430311
    const v0, -0x55b977db

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2430310
    const v0, 0x3c2b9d5

    return v0
.end method

.method public final j()Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getClaimedOffers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430308
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel;

    .line 2430309
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel;

    return-object v0
.end method
