.class public final Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1749137f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:J

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Z

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2428547
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2428519
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2428520
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2428521
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2428522
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2428523
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2428524
    return-void
.end method

.method public static a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2428525
    if-nez p0, :cond_0

    .line 2428526
    const/4 p0, 0x0

    .line 2428527
    :goto_0
    return-object p0

    .line 2428528
    :cond_0
    instance-of v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    if-eqz v0, :cond_1

    .line 2428529
    check-cast p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    goto :goto_0

    .line 2428530
    :cond_1
    new-instance v0, LX/H7A;

    invoke-direct {v0}, LX/H7A;-><init>()V

    .line 2428531
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->b()I

    move-result v1

    iput v1, v0, LX/H7A;->a:I

    .line 2428532
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H7A;->b:Ljava/lang/String;

    .line 2428533
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->d()Z

    move-result v1

    iput-boolean v1, v0, LX/H7A;->c:Z

    .line 2428534
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H7A;->d:Ljava/lang/String;

    .line 2428535
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->mg_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H7A;->e:Ljava/lang/String;

    .line 2428536
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->mh_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H7A;->f:Ljava/lang/String;

    .line 2428537
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->q()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v1

    iput-object v1, v0, LX/H7A;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    .line 2428538
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H7A;->h:Ljava/lang/String;

    .line 2428539
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H7A;->i:Ljava/lang/String;

    .line 2428540
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->l()J

    move-result-wide v2

    iput-wide v2, v0, LX/H7A;->j:J

    .line 2428541
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H7A;->k:Ljava/lang/String;

    .line 2428542
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->n()Z

    move-result v1

    iput-boolean v1, v0, LX/H7A;->l:Z

    .line 2428543
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->o()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H7A;->m:Ljava/lang/String;

    .line 2428544
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->r()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v1

    iput-object v1, v0, LX/H7A;->n:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    .line 2428545
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->p()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H7A;->o:Ljava/lang/String;

    .line 2428546
    invoke-virtual {v0}, LX/H7A;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object p0

    goto :goto_0
.end method

.method private s()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDiscountBarcodeImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428548
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->k:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->k:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    .line 2428549
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->k:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    return-object v0
.end method

.method private t()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOwner"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428605
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->r:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->r:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    .line 2428606
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->r:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 2428550
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2428551
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2428552
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2428553
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->mg_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2428554
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->mh_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2428555
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->s()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2428556
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2428557
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2428558
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->m()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2428559
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2428560
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->t()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2428561
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->p()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 2428562
    const/16 v11, 0xf

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2428563
    iget v11, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->e:I

    invoke-virtual {p1, v12, v11, v12}, LX/186;->a(III)V

    .line 2428564
    const/4 v11, 0x1

    invoke-virtual {p1, v11, v0}, LX/186;->b(II)V

    .line 2428565
    const/4 v0, 0x2

    iget-boolean v11, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->g:Z

    invoke-virtual {p1, v0, v11}, LX/186;->a(IZ)V

    .line 2428566
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2428567
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2428568
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2428569
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2428570
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2428571
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2428572
    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->n:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2428573
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2428574
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->p:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2428575
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2428576
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2428577
    const/16 v0, 0xe

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 2428578
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2428579
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2428580
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2428581
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->s()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2428582
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->s()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    .line 2428583
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->s()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2428584
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    .line 2428585
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->k:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    .line 2428586
    :cond_0
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->t()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2428587
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->t()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    .line 2428588
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->t()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2428589
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    .line 2428590
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->r:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    .line 2428591
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2428592
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428593
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2428594
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2428595
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->e:I

    .line 2428596
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->g:Z

    .line 2428597
    const/16 v0, 0x9

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->n:J

    .line 2428598
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->p:Z

    .line 2428599
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2428600
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428601
    iget v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->e:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2428602
    new-instance v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    invoke-direct {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;-><init>()V

    .line 2428603
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2428604
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428515
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->f:Ljava/lang/String;

    .line 2428516
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 2428517
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428518
    iget-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->g:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2428514
    const v0, 0x38dc7c8

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428512
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->h:Ljava/lang/String;

    .line 2428513
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2428511
    const v0, 0x4892a3c

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428509
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->l:Ljava/lang/String;

    .line 2428510
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428507
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->m:Ljava/lang/String;

    .line 2428508
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final l()J
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2428505
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428506
    iget-wide v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->n:J

    return-wide v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428503
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->o:Ljava/lang/String;

    .line 2428504
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final mg_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428501
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->i:Ljava/lang/String;

    .line 2428502
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final mh_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428499
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->j:Ljava/lang/String;

    .line 2428500
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 2428491
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428492
    iget-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->p:Z

    return v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428497
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->q:Ljava/lang/String;

    .line 2428498
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428495
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->s:Ljava/lang/String;

    .line 2428496
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic q()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDiscountBarcodeImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428494
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->s()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic r()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOwner"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428493
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->t()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    return-object v0
.end method
