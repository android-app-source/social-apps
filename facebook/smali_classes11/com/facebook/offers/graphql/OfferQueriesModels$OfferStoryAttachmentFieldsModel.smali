.class public final Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2b556152
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$ActionLinksModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2429729
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2429728
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2429726
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2429727
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2429723
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2429724
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2429725
    return-void
.end method

.method public static a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;
    .locals 12
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2429693
    if-nez p0, :cond_0

    .line 2429694
    const/4 p0, 0x0

    .line 2429695
    :goto_0
    return-object p0

    .line 2429696
    :cond_0
    instance-of v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;

    if-eqz v0, :cond_1

    .line 2429697
    check-cast p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;

    goto :goto_0

    .line 2429698
    :cond_1
    new-instance v2, LX/H7J;

    invoke-direct {v2}, LX/H7J;-><init>()V

    .line 2429699
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2429700
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2429701
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$ActionLinksModel;

    invoke-static {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$ActionLinksModel;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$ActionLinksModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$ActionLinksModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2429702
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2429703
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/H7J;->a:LX/0Px;

    .line 2429704
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->c()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;

    move-result-object v0

    iput-object v0, v2, LX/H7J;->b:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;

    .line 2429705
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/H7J;->c:Ljava/lang/String;

    .line 2429706
    const/4 v8, 0x1

    const/4 v11, 0x0

    const/4 v6, 0x0

    .line 2429707
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 2429708
    iget-object v5, v2, LX/H7J;->a:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 2429709
    iget-object v7, v2, LX/H7J;->b:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;

    invoke-static {v4, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2429710
    iget-object v9, v2, LX/H7J;->c:Ljava/lang/String;

    invoke-virtual {v4, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 2429711
    const/4 v10, 0x3

    invoke-virtual {v4, v10}, LX/186;->c(I)V

    .line 2429712
    invoke-virtual {v4, v11, v5}, LX/186;->b(II)V

    .line 2429713
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 2429714
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v9}, LX/186;->b(II)V

    .line 2429715
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 2429716
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 2429717
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 2429718
    invoke-virtual {v5, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2429719
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2429720
    new-instance v5, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;

    invoke-direct {v5, v4}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;-><init>(LX/15i;)V

    .line 2429721
    move-object p0, v5

    .line 2429722
    goto/16 :goto_0
.end method

.method private j()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMedia"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429691
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->f:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->f:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;

    .line 2429692
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->f:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2429730
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2429731
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2429732
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2429733
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2429734
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2429735
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2429736
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2429737
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2429738
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2429739
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$ActionLinksModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2429689
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$ActionLinksModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->e:Ljava/util/List;

    .line 2429690
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2429676
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2429677
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2429678
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2429679
    if-eqz v1, :cond_2

    .line 2429680
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;

    .line 2429681
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 2429682
    :goto_0
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2429683
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;

    .line 2429684
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2429685
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;

    .line 2429686
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->f:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;

    .line 2429687
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2429688
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2429673
    new-instance v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;

    invoke-direct {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;-><init>()V

    .line 2429674
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2429675
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429671
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->g:Ljava/lang/String;

    .line 2429672
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMedia"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429670
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2429669
    const v0, 0x40ec18ca

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2429668
    const v0, -0x4b900828

    return v0
.end method
