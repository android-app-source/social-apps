.class public final Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/H6w;
.implements LX/H6v;
.implements LX/H70;
.implements LX/H6y;
.implements LX/H72;
.implements LX/H71;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1cf6c9a6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel$Serializer;
.end annotation


# instance fields
.field private A:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private I:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private J:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private K:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private L:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private M:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private N:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:J

.field private j:Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:J

.field private o:I

.field private p:Z

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2428829
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2428830
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2428831
    const/16 v0, 0x24

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2428832
    return-void
.end method

.method private M()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428833
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->f:Ljava/lang/String;

    .line 2428834
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private N()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDiscountBarcodeImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428835
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->k:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->k:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    .line 2428836
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->k:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    return-object v0
.end method

.method private O()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOffer"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428837
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->A:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->A:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    .line 2428838
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->A:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    return-object v0
.end method

.method private P()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOfferView"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428839
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->B:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->B:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    .line 2428840
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->B:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    return-object v0
.end method

.method private Q()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOwningPage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428841
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->D:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->D:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    .line 2428842
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->D:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    return-object v0
.end method

.method private R()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRootShareStory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428843
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->I:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->I:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2428844
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->I:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    return-object v0
.end method

.method private S()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getShareStory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428845
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->J:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->J:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2428846
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->J:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    return-object v0
.end method

.method private T()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewerClaim"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428847
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->N:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->N:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    .line 2428848
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->N:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    return-object v0
.end method


# virtual methods
.method public final A()Z
    .locals 2

    .prologue
    .line 2428849
    const/4 v0, 0x2

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428850
    iget-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->z:Z

    return v0
.end method

.method public final B()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428851
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->C:Ljava/lang/String;

    const/16 v1, 0x18

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->C:Ljava/lang/String;

    .line 2428852
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->C:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic C()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDiscountBarcodeImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428856
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->N()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic D()LX/H72;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOfferView"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429016
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->P()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic E()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getShareStory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429015
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->S()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final F()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAppDataForOffer"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429013
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2429014
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic G()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOffer"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429012
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->O()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    return-object v0
.end method

.method public final H()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPhotos"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2429010
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->F:Ljava/util/List;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->F:Ljava/util/List;

    .line 2429011
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->F:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic I()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRootShareStory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429009
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->R()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final J()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getVideos"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2429007
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->M:Ljava/util/List;

    const/16 v1, 0x22

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->M:Ljava/util/List;

    .line 2429008
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->M:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic K()LX/H6z;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewerClaim"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429006
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->T()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    move-result-object v0

    return-object v0
.end method

.method public final L()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428853
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2428854
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2428855
    :cond_0
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 31

    .prologue
    .line 2428939
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2428940
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->L()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2428941
    invoke-direct/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->M()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2428942
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->F()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, 0x68ed5f3

    invoke-static {v5, v4, v6}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2428943
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->t()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2428944
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->b()Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 2428945
    invoke-direct/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->N()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2428946
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->v()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 2428947
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->w()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 2428948
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->me_()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 2428949
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->x()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 2428950
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->q()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v14, 0x57feaeeb

    invoke-static {v7, v6, v14}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 2428951
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->l()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 2428952
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->z()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 2428953
    invoke-direct/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->O()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 2428954
    invoke-direct/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->P()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 2428955
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->B()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 2428956
    invoke-direct/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->Q()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 2428957
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->s()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v21, 0x420c3d46

    move/from16 v0, v21

    invoke-static {v7, v6, v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 2428958
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->H()LX/0Px;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v22

    .line 2428959
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->m()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 2428960
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->n()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    .line 2428961
    invoke-direct/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->R()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 2428962
    invoke-direct/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->S()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 2428963
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    .line 2428964
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->p()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 2428965
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->J()LX/0Px;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v29

    .line 2428966
    invoke-direct/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->T()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 2428967
    const/16 v6, 0x24

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 2428968
    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v2}, LX/186;->b(II)V

    .line 2428969
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2428970
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2428971
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2428972
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->i:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2428973
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2428974
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2428975
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2428976
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2428977
    const/16 v3, 0x9

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->n:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2428978
    const/16 v2, 0xa

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->o:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 2428979
    const/16 v2, 0xb

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->p:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2428980
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2428981
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2428982
    const/16 v2, 0xe

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->s:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2428983
    const/16 v2, 0xf

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->t:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2428984
    const/16 v2, 0x10

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->u:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2428985
    const/16 v2, 0x11

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->v:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2428986
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 2428987
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2428988
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2428989
    const/16 v2, 0x15

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->z:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2428990
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2428991
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2428992
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2428993
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2428994
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2428995
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2428996
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2428997
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2428998
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2428999
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2429000
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2429001
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2429002
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2429003
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2429004
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2429005
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2428872
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2428873
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->F()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2428874
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->F()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x68ed5f3

    invoke-static {v2, v0, v3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2428875
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->F()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2428876
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    .line 2428877
    iput v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->g:I

    move-object v1, v0

    .line 2428878
    :cond_0
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->N()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2428879
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->N()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    .line 2428880
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->N()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2428881
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    .line 2428882
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->k:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    .line 2428883
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->q()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2428884
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->q()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x57feaeeb

    invoke-static {v2, v0, v3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2428885
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->q()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2428886
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    .line 2428887
    iput v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->w:I

    move-object v1, v0

    .line 2428888
    :cond_2
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->O()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2428889
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->O()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    .line 2428890
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->O()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2428891
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    .line 2428892
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->A:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    .line 2428893
    :cond_3
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->P()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2428894
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->P()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    .line 2428895
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->P()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 2428896
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    .line 2428897
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->B:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    .line 2428898
    :cond_4
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->Q()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2428899
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->Q()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    .line 2428900
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->Q()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 2428901
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    .line 2428902
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->D:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    .line 2428903
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->s()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    .line 2428904
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->s()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x420c3d46

    invoke-static {v2, v0, v3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2428905
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->s()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2428906
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    .line 2428907
    iput v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->E:I

    move-object v1, v0

    .line 2428908
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->H()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 2428909
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->H()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 2428910
    if-eqz v2, :cond_7

    .line 2428911
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    .line 2428912
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->F:Ljava/util/List;

    move-object v1, v0

    .line 2428913
    :cond_7
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->R()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 2428914
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->R()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2428915
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->R()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 2428916
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    .line 2428917
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->I:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2428918
    :cond_8
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->S()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 2428919
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->S()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2428920
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->S()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 2428921
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    .line 2428922
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->J:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2428923
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->J()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 2428924
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->J()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 2428925
    if-eqz v2, :cond_a

    .line 2428926
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    .line 2428927
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->M:Ljava/util/List;

    move-object v1, v0

    .line 2428928
    :cond_a
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->T()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 2428929
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->T()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    .line 2428930
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->T()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 2428931
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    .line 2428932
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->N:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    .line 2428933
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2428934
    if-nez v1, :cond_c

    :goto_0
    return-object p0

    .line 2428935
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 2428936
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 2428937
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_c
    move-object p0, v1

    .line 2428938
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2428871
    new-instance v0, LX/H7B;

    invoke-direct {v0, p1}, LX/H7B;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428828
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->me_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2428857
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2428858
    const/4 v0, 0x2

    const v1, 0x68ed5f3

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->g:I

    .line 2428859
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->i:J

    .line 2428860
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->n:J

    .line 2428861
    const/16 v0, 0xa

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->o:I

    .line 2428862
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->p:Z

    .line 2428863
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->s:Z

    .line 2428864
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->t:Z

    .line 2428865
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->u:Z

    .line 2428866
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->v:Z

    .line 2428867
    const/16 v0, 0x12

    const v1, 0x57feaeeb

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->w:I

    .line 2428868
    const/16 v0, 0x15

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->z:Z

    .line 2428869
    const/16 v0, 0x1a

    const v1, 0x420c3d46

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->E:I

    .line 2428870
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2428775
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2428776
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2428779
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428777
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->j:Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->j:Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    .line 2428778
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->j:Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2428780
    new-instance v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    invoke-direct {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;-><init>()V

    .line 2428781
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2428782
    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2428783
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428784
    iget-wide v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->n:J

    return-wide v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 2428785
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428786
    iget v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->o:I

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2428787
    const v0, -0x4246ae9d

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 2428788
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428789
    iget-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->p:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2428790
    const v0, 0x252222

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 2428791
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428792
    iget-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->t:Z

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 2428793
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428794
    iget-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->u:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428795
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->x:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->x:Ljava/lang/String;

    .line 2428796
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428797
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->G:Ljava/lang/String;

    const/16 v1, 0x1c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->G:Ljava/lang/String;

    .line 2428798
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->G:Ljava/lang/String;

    return-object v0
.end method

.method public final me_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428799
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->q:Ljava/lang/String;

    .line 2428800
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final mf_()Z
    .locals 2

    .prologue
    .line 2428801
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428802
    iget-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->s:Z

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428803
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->H:Ljava/lang/String;

    const/16 v1, 0x1d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->H:Ljava/lang/String;

    .line 2428804
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->H:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428805
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->K:Ljava/lang/String;

    const/16 v1, 0x20

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->K:Ljava/lang/String;

    .line 2428806
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->K:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428807
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->L:Ljava/lang/String;

    const/16 v1, 0x21

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->L:Ljava/lang/String;

    .line 2428808
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->L:Ljava/lang/String;

    return-object v0
.end method

.method public final q()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMessage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x2

    .line 2428809
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428810
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->w:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic r()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOwningPage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428811
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->Q()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    return-object v0
.end method

.method public final s()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428812
    const/4 v0, 0x3

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428813
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->E:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428814
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->h:Ljava/lang/String;

    .line 2428815
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final u()J
    .locals 2

    .prologue
    .line 2428816
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428817
    iget-wide v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->i:J

    return-wide v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428818
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->l:Ljava/lang/String;

    .line 2428819
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final w()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428820
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->m:Ljava/lang/String;

    .line 2428821
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final x()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428822
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->r:Ljava/lang/String;

    .line 2428823
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final y()Z
    .locals 2

    .prologue
    .line 2428824
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428825
    iget-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->v:Z

    return v0
.end method

.method public final z()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428826
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->y:Ljava/lang/String;

    const/16 v1, 0x14

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->y:Ljava/lang/String;

    .line 2428827
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->y:Ljava/lang/String;

    return-object v0
.end method
