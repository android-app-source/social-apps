.class public final Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5e26ebb0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2430172
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2430171
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2430169
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2430170
    return-void
.end method

.method private a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEdges"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2430142
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;->e:Ljava/util/List;

    .line 2430143
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430167
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2430168
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2430173
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2430174
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2430175
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x75ba5ba

    invoke-static {v2, v1, v3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2430176
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2430177
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2430178
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2430179
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2430180
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2430152
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2430153
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2430154
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2430155
    if-eqz v1, :cond_2

    .line 2430156
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;

    .line 2430157
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 2430158
    :goto_0
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2430159
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x75ba5ba

    invoke-static {v2, v0, v3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2430160
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2430161
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;

    .line 2430162
    iput v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;->f:I

    move-object v1, v0

    .line 2430163
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2430164
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    .line 2430165
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move-object p0, v1

    .line 2430166
    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2430149
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2430150
    const/4 v0, 0x1

    const v1, -0x75ba5ba

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;->f:I

    .line 2430151
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2430146
    new-instance v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;

    invoke-direct {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedCouponsModel;-><init>()V

    .line 2430147
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2430148
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2430145
    const v0, 0x2dc392a5

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2430144
    const v0, -0x56e3b3fa

    return v0
.end method
