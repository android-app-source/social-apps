.class public final Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/H72;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x722d26ec
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2430076
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2430075
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2430073
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2430074
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2430070
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2430071
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2430072
    return-void
.end method

.method public static a(LX/H72;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2429968
    if-nez p0, :cond_0

    .line 2429969
    const/4 p0, 0x0

    .line 2429970
    :goto_0
    return-object p0

    .line 2429971
    :cond_0
    instance-of v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    if-eqz v0, :cond_1

    .line 2429972
    check-cast p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    goto :goto_0

    .line 2429973
    :cond_1
    new-instance v3, LX/H7P;

    invoke-direct {v3}, LX/H7P;-><init>()V

    .line 2429974
    invoke-interface {p0}, LX/H72;->F()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    iput-object v1, v3, LX/H7P;->a:LX/15i;

    iput v0, v3, LX/H7P;->b:I

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2429975
    invoke-interface {p0}, LX/H71;->me_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/H7P;->c:Ljava/lang/String;

    .line 2429976
    invoke-interface {p0}, LX/H72;->G()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    iput-object v0, v3, LX/H7P;->d:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    .line 2429977
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v2

    .line 2429978
    :goto_1
    invoke-interface {p0}, LX/H72;->H()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2429979
    invoke-interface {p0}, LX/H72;->H()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    invoke-static {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2429980
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2429981
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2429982
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/H7P;->e:LX/0Px;

    .line 2429983
    invoke-interface {p0}, LX/H72;->I()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    iput-object v0, v3, LX/H7P;->f:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2429984
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2429985
    :goto_2
    invoke-interface {p0}, LX/H72;->J()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 2429986
    invoke-interface {p0}, LX/H72;->J()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;

    invoke-static {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2429987
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 2429988
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/H7P;->g:LX/0Px;

    .line 2429989
    invoke-interface {p0}, LX/H72;->K()LX/H6z;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;->a(LX/H6z;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    move-result-object v0

    iput-object v0, v3, LX/H7P;->h:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    .line 2429990
    invoke-virtual {v3}, LX/H7P;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    move-result-object p0

    goto/16 :goto_0
.end method

.method private j()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOffer"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430068
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    .line 2430069
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    return-object v0
.end method

.method private k()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRootShareStory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430066
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->i:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->i:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2430067
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->i:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    return-object v0
.end method

.method private l()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewerClaim"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430064
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->k:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->k:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    .line 2430065
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->k:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    return-object v0
.end method


# virtual methods
.method public final F()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAppDataForOffer"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2430062
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2430063
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic G()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOffer"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430061
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    return-object v0
.end method

.method public final H()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPhotos"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2430059
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->h:Ljava/util/List;

    .line 2430060
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic I()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRootShareStory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430058
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->k()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final J()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getVideos"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2430056
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->j:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->j:Ljava/util/List;

    .line 2430057
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic K()LX/H6z;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewerClaim"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430055
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->l()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 2430037
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2430038
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->F()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x68ed5f3

    invoke-static {v1, v0, v2}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2430039
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->me_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2430040
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2430041
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->H()LX/0Px;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 2430042
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->k()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2430043
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->J()LX/0Px;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 2430044
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->l()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2430045
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2430046
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 2430047
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2430048
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2430049
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2430050
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2430051
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2430052
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2430053
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2430054
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2430002
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2430003
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->F()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2430004
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->F()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x68ed5f3

    invoke-static {v2, v0, v3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2430005
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->F()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2430006
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    .line 2430007
    iput v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->e:I

    move-object v1, v0

    .line 2430008
    :cond_0
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2430009
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    .line 2430010
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2430011
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    .line 2430012
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    .line 2430013
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->H()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2430014
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->H()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 2430015
    if-eqz v2, :cond_2

    .line 2430016
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    .line 2430017
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->h:Ljava/util/List;

    move-object v1, v0

    .line 2430018
    :cond_2
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->k()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2430019
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->k()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2430020
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->k()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2430021
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    .line 2430022
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->i:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2430023
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->J()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2430024
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->J()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 2430025
    if-eqz v2, :cond_4

    .line 2430026
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    .line 2430027
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->j:Ljava/util/List;

    move-object v1, v0

    .line 2430028
    :cond_4
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->l()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2430029
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->l()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    .line 2430030
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->l()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 2430031
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    .line 2430032
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->k:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    .line 2430033
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2430034
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    .line 2430035
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_6
    move-object p0, v1

    .line 2430036
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430001
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->me_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2429998
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2429999
    const/4 v0, 0x0

    const v1, 0x68ed5f3

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->e:I

    .line 2430000
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2429995
    new-instance v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    invoke-direct {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;-><init>()V

    .line 2429996
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2429997
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2429994
    const v0, -0x641b0d98

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2429993
    const v0, -0x14a5a4ff

    return v0
.end method

.method public final me_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2429991
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->f:Ljava/lang/String;

    .line 2429992
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->f:Ljava/lang/String;

    return-object v0
.end method
