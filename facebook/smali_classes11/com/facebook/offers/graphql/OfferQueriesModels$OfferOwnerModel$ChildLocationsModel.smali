.class public final Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5f9fdc33
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2429326
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2429327
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2429331
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2429332
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2429328
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2429329
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2429330
    return-void
.end method

.method public static a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;
    .locals 10
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2429296
    if-nez p0, :cond_0

    .line 2429297
    const/4 p0, 0x0

    .line 2429298
    :goto_0
    return-object p0

    .line 2429299
    :cond_0
    instance-of v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    if-eqz v0, :cond_1

    .line 2429300
    check-cast p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    goto :goto_0

    .line 2429301
    :cond_1
    new-instance v2, LX/H7F;

    invoke-direct {v2}, LX/H7F;-><init>()V

    .line 2429302
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2429303
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2429304
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel$EdgesModel;

    invoke-static {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel$EdgesModel;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel$EdgesModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel$EdgesModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2429305
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2429306
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/H7F;->a:LX/0Px;

    .line 2429307
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2429308
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 2429309
    iget-object v5, v2, LX/H7F;->a:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 2429310
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 2429311
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 2429312
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 2429313
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 2429314
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 2429315
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2429316
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2429317
    new-instance v5, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    invoke-direct {v5, v4}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;-><init>(LX/15i;)V

    .line 2429318
    move-object p0, v5

    .line 2429319
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2429320
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2429321
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2429322
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2429323
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2429324
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2429325
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEdges"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2429281
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;->e:Ljava/util/List;

    .line 2429282
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2429283
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2429284
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2429285
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2429286
    if-eqz v1, :cond_0

    .line 2429287
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    .line 2429288
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;->e:Ljava/util/List;

    .line 2429289
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2429290
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2429291
    new-instance v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    invoke-direct {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;-><init>()V

    .line 2429292
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2429293
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2429294
    const v0, -0x5add8547

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2429295
    const v0, 0x235757ef

    return v0
.end method
