.class public final Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/H70;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x54332bd3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J

.field private g:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Z

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Z

.field private o:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2428211
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2428205
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2428206
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2428207
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2428208
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2428209
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2428210
    return-void
.end method

.method public static a(LX/H70;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2428212
    if-nez p0, :cond_0

    .line 2428213
    const/4 p0, 0x0

    .line 2428214
    :goto_0
    return-object p0

    .line 2428215
    :cond_0
    instance-of v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    if-eqz v0, :cond_1

    .line 2428216
    check-cast p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    goto :goto_0

    .line 2428217
    :cond_1
    new-instance v0, LX/H78;

    invoke-direct {v0}, LX/H78;-><init>()V

    .line 2428218
    invoke-interface {p0}, LX/H6y;->t()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H78;->a:Ljava/lang/String;

    .line 2428219
    invoke-interface {p0}, LX/H6y;->u()J

    move-result-wide v2

    iput-wide v2, v0, LX/H78;->b:J

    .line 2428220
    invoke-interface {p0}, LX/H70;->C()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v1

    iput-object v1, v0, LX/H78;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    .line 2428221
    invoke-interface {p0}, LX/H6y;->v()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H78;->d:Ljava/lang/String;

    .line 2428222
    invoke-interface {p0}, LX/H6y;->w()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H78;->e:Ljava/lang/String;

    .line 2428223
    invoke-interface {p0}, LX/H6y;->me_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H78;->f:Ljava/lang/String;

    .line 2428224
    invoke-interface {p0}, LX/H6y;->x()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H78;->g:Ljava/lang/String;

    .line 2428225
    invoke-interface {p0}, LX/H6y;->y()Z

    move-result v1

    iput-boolean v1, v0, LX/H78;->h:Z

    .line 2428226
    invoke-interface {p0}, LX/H6y;->z()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H78;->i:Ljava/lang/String;

    .line 2428227
    invoke-interface {p0}, LX/H6y;->A()Z

    move-result v1

    iput-boolean v1, v0, LX/H78;->j:Z

    .line 2428228
    invoke-interface {p0}, LX/H70;->D()LX/H72;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->a(LX/H72;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    move-result-object v1

    iput-object v1, v0, LX/H78;->k:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    .line 2428229
    invoke-interface {p0}, LX/H6y;->B()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H78;->l:Ljava/lang/String;

    .line 2428230
    invoke-interface {p0}, LX/H70;->E()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/H78;->m:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2428231
    invoke-virtual {v0}, LX/H78;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final A()Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 2428198
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428199
    iget-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->n:Z

    return v0
.end method

.method public final B()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428232
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->p:Ljava/lang/String;

    .line 2428233
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic C()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDiscountBarcodeImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428234
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic D()LX/H72;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOfferView"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428235
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->k()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic E()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getShareStory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428236
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->l()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 17

    .prologue
    .line 2428237
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2428238
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->t()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2428239
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2428240
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->v()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 2428241
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->w()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 2428242
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->me_()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 2428243
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->x()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 2428244
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->z()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 2428245
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->k()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 2428246
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->B()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 2428247
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->l()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 2428248
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2428249
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 2428250
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->f:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2428251
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2428252
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2428253
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2428254
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2428255
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2428256
    const/4 v2, 0x7

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->l:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2428257
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2428258
    const/16 v2, 0x9

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->n:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2428259
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 2428260
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2428261
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2428262
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2428263
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2428264
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2428265
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2428266
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    .line 2428267
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2428268
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    .line 2428269
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    .line 2428270
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->k()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2428271
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->k()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    .line 2428272
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->k()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2428273
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    .line 2428274
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->o:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    .line 2428275
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->l()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2428276
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->l()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2428277
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->l()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2428278
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    .line 2428279
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->q:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2428280
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2428281
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428282
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->me_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2428200
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2428201
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->f:J

    .line 2428202
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->l:Z

    .line 2428203
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->n:Z

    .line 2428204
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2428195
    new-instance v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    invoke-direct {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;-><init>()V

    .line 2428196
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2428197
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2428194
    const v0, 0x5083f1a6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2428193
    const v0, 0x7ee69360

    return v0
.end method

.method public final j()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDiscountBarcodeImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428191
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    .line 2428192
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOfferView"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428189
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->o:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->o:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    .line 2428190
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->o:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getShareStory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428187
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->q:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->q:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2428188
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->q:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    return-object v0
.end method

.method public final me_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428185
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->j:Ljava/lang/String;

    .line 2428186
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428183
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->e:Ljava/lang/String;

    .line 2428184
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final u()J
    .locals 2

    .prologue
    .line 2428181
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428182
    iget-wide v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->f:J

    return-wide v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428179
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->h:Ljava/lang/String;

    .line 2428180
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final w()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428171
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->i:Ljava/lang/String;

    .line 2428172
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final x()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428177
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->k:Ljava/lang/String;

    .line 2428178
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final y()Z
    .locals 2

    .prologue
    .line 2428175
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2428176
    iget-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->l:Z

    return v0
.end method

.method public final z()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2428173
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->m:Ljava/lang/String;

    .line 2428174
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->m:Ljava/lang/String;

    return-object v0
.end method
