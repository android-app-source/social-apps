.class public final Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/H6w;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6cf2e5e4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J

.field private g:I

.field private h:Z

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2427935
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2427824
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2427825
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2427826
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2427827
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2427828
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2427829
    return-void
.end method

.method public static a(LX/H6w;)Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2427830
    if-nez p0, :cond_0

    .line 2427831
    const/4 p0, 0x0

    .line 2427832
    :goto_0
    return-object p0

    .line 2427833
    :cond_0
    instance-of v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    if-eqz v0, :cond_1

    .line 2427834
    check-cast p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    goto :goto_0

    .line 2427835
    :cond_1
    new-instance v0, LX/H76;

    invoke-direct {v0}, LX/H76;-><init>()V

    .line 2427836
    invoke-interface {p0}, LX/H6v;->b()Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    move-result-object v1

    iput-object v1, v0, LX/H76;->a:Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    .line 2427837
    invoke-interface {p0}, LX/H6v;->c()J

    move-result-wide v2

    iput-wide v2, v0, LX/H76;->b:J

    .line 2427838
    invoke-interface {p0}, LX/H6v;->d()I

    move-result v1

    iput v1, v0, LX/H76;->c:I

    .line 2427839
    invoke-interface {p0}, LX/H6v;->e()Z

    move-result v1

    iput-boolean v1, v0, LX/H76;->d:Z

    .line 2427840
    invoke-interface {p0}, LX/H6v;->me_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H76;->e:Ljava/lang/String;

    .line 2427841
    invoke-interface {p0}, LX/H6v;->mf_()Z

    move-result v1

    iput-boolean v1, v0, LX/H76;->f:Z

    .line 2427842
    invoke-interface {p0}, LX/H6v;->j()Z

    move-result v1

    iput-boolean v1, v0, LX/H76;->g:Z

    .line 2427843
    invoke-interface {p0}, LX/H6v;->k()Z

    move-result v1

    iput-boolean v1, v0, LX/H76;->h:Z

    .line 2427844
    invoke-interface {p0}, LX/H6w;->q()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, v0, LX/H76;->i:LX/15i;

    iput v1, v0, LX/H76;->j:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2427845
    invoke-interface {p0}, LX/H6v;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H76;->k:Ljava/lang/String;

    .line 2427846
    invoke-interface {p0}, LX/H6w;->r()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v1

    iput-object v1, v0, LX/H76;->l:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    .line 2427847
    invoke-interface {p0}, LX/H6w;->s()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iput-object v2, v0, LX/H76;->m:LX/15i;

    iput v1, v0, LX/H76;->n:I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2427848
    invoke-interface {p0}, LX/H6v;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H76;->o:Ljava/lang/String;

    .line 2427849
    invoke-interface {p0}, LX/H6v;->n()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H76;->p:Ljava/lang/String;

    .line 2427850
    invoke-interface {p0}, LX/H6v;->o()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H76;->q:Ljava/lang/String;

    .line 2427851
    invoke-interface {p0}, LX/H6v;->p()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H76;->r:Ljava/lang/String;

    .line 2427852
    invoke-virtual {v0}, LX/H76;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    move-result-object p0

    goto/16 :goto_0

    .line 2427853
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2427854
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 2427855
    iput-boolean p1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->h:Z

    .line 2427856
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2427857
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2427858
    if-eqz v0, :cond_0

    .line 2427859
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 2427860
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 17

    .prologue
    .line 2427861
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2427862
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->b()Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 2427863
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->me_()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2427864
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->q()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x57feaeeb

    invoke-static {v4, v3, v5}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2427865
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->l()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 2427866
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->t()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 2427867
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->s()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x420c3d46

    invoke-static {v4, v3, v5}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 2427868
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->m()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 2427869
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->n()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 2427870
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 2427871
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->p()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 2427872
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2427873
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 2427874
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->f:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2427875
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->g:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 2427876
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->h:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2427877
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2427878
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->j:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2427879
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->k:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2427880
    const/4 v2, 0x7

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->l:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2427881
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2427882
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2427883
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2427884
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2427885
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2427886
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 2427887
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2427888
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2427889
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2427890
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2427891
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2427892
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->q()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2427893
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->q()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x57feaeeb

    invoke-static {v2, v0, v3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2427894
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->q()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2427895
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    .line 2427896
    iput v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->m:I

    move-object v1, v0

    .line 2427897
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->t()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2427898
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->t()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    .line 2427899
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->t()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2427900
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    .line 2427901
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->o:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    .line 2427902
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->s()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2427903
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->s()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x420c3d46

    invoke-static {v2, v0, v3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2427904
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->s()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2427905
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    .line 2427906
    iput v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->p:I

    move-object v1, v0

    .line 2427907
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2427908
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 2427909
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2427910
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_3
    move-object p0, v1

    .line 2427911
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2427912
    new-instance v0, LX/H77;

    invoke-direct {v0, p1}, LX/H77;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2427913
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->me_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2427914
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2427915
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->f:J

    .line 2427916
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->g:I

    .line 2427917
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->h:Z

    .line 2427918
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->j:Z

    .line 2427919
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->k:Z

    .line 2427920
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->l:Z

    .line 2427921
    const/16 v0, 0x8

    const v1, 0x57feaeeb

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->m:I

    .line 2427922
    const/16 v0, 0xb

    const v1, 0x420c3d46

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->p:I

    .line 2427923
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2427924
    const-string v0, "has_viewer_claimed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2427925
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->e()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2427926
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2427927
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    .line 2427928
    :goto_0
    return-void

    .line 2427929
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2427930
    const-string v0, "has_viewer_claimed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2427931
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->a(Z)V

    .line 2427932
    :cond_0
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2427933
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->e:Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->e:Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    .line 2427934
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->e:Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2427789
    new-instance v0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    invoke-direct {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;-><init>()V

    .line 2427790
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2427791
    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 2427820
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2427821
    iget-wide v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->f:J

    return-wide v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 2427822
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2427823
    iget v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->g:I

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2427788
    const v0, 0x3d443bfc

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 2427792
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2427793
    iget-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->h:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2427794
    const v0, 0x78a7c446

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 2427795
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2427796
    iget-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->k:Z

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 2427797
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2427798
    iget-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->l:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2427799
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->n:Ljava/lang/String;

    .line 2427800
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2427801
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->q:Ljava/lang/String;

    .line 2427802
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final me_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2427803
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->i:Ljava/lang/String;

    .line 2427804
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final mf_()Z
    .locals 2

    .prologue
    .line 2427805
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2427806
    iget-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->j:Z

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2427807
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->r:Ljava/lang/String;

    .line 2427808
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2427809
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->s:Ljava/lang/String;

    .line 2427810
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2427811
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->t:Ljava/lang/String;

    .line 2427812
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final q()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMessage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2427813
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2427814
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->m:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic r()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOwningPage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2427815
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->t()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    return-object v0
.end method

.method public final s()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2427816
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2427817
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->p:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final t()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOwningPage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2427818
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->o:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->o:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    .line 2427819
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->o:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    return-object v0
.end method
