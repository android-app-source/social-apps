.class public final Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x18d5b22b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:I

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I

.field private i:I

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:I

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Z

.field private n:I

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2430738
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2430737
    const-class v0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2430735
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2430736
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2430732
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2430733
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2430734
    return-void
.end method

.method public static a(Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2430709
    if-nez p0, :cond_0

    .line 2430710
    const/4 p0, 0x0

    .line 2430711
    :goto_0
    return-object p0

    .line 2430712
    :cond_0
    instance-of v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;

    if-eqz v0, :cond_1

    .line 2430713
    check-cast p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;

    goto :goto_0

    .line 2430714
    :cond_1
    new-instance v0, LX/H7S;

    invoke-direct {v0}, LX/H7S;-><init>()V

    .line 2430715
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->b()I

    move-result v1

    iput v1, v0, LX/H7S;->a:I

    .line 2430716
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->c()I

    move-result v1

    iput v1, v0, LX/H7S;->b:I

    .line 2430717
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H7S;->c:Ljava/lang/String;

    .line 2430718
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->e()I

    move-result v1

    iput v1, v0, LX/H7S;->d:I

    .line 2430719
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->mk_()I

    move-result v1

    iput v1, v0, LX/H7S;->e:I

    .line 2430720
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->ml_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H7S;->f:Ljava/lang/String;

    .line 2430721
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->j()I

    move-result v1

    iput v1, v0, LX/H7S;->g:I

    .line 2430722
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H7S;->h:Ljava/lang/String;

    .line 2430723
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->l()Z

    move-result v1

    iput-boolean v1, v0, LX/H7S;->i:Z

    .line 2430724
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->m()I

    move-result v1

    iput v1, v0, LX/H7S;->j:I

    .line 2430725
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->n()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H7S;->k:Ljava/lang/String;

    .line 2430726
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->o()Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;)Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;

    move-result-object v1

    iput-object v1, v0, LX/H7S;->l:Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;

    .line 2430727
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->p()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H7S;->m:Ljava/lang/String;

    .line 2430728
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->r()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, v0, LX/H7S;->n:LX/15i;

    iput v1, v0, LX/H7S;->o:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2430729
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->q()I

    move-result v1

    iput v1, v0, LX/H7S;->p:I

    .line 2430730
    invoke-virtual {v0}, LX/H7S;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;

    move-result-object p0

    goto :goto_0

    .line 2430731
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private s()Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430707
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->p:Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->p:Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;

    .line 2430708
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->p:Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 2430681
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2430682
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2430683
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->ml_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2430684
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2430685
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2430686
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->s()Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2430687
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2430688
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->r()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v8, 0x25ccafee

    invoke-static {v7, v6, v8}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2430689
    const/16 v7, 0xf

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2430690
    iget v7, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->e:I

    invoke-virtual {p1, v9, v7, v9}, LX/186;->a(III)V

    .line 2430691
    const/4 v7, 0x1

    iget v8, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->f:I

    invoke-virtual {p1, v7, v8, v9}, LX/186;->a(III)V

    .line 2430692
    const/4 v7, 0x2

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 2430693
    const/4 v0, 0x3

    iget v7, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->h:I

    invoke-virtual {p1, v0, v7, v9}, LX/186;->a(III)V

    .line 2430694
    const/4 v0, 0x4

    iget v7, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->i:I

    invoke-virtual {p1, v0, v7, v9}, LX/186;->a(III)V

    .line 2430695
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2430696
    const/4 v0, 0x6

    iget v1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->k:I

    invoke-virtual {p1, v0, v1, v9}, LX/186;->a(III)V

    .line 2430697
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2430698
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->m:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2430699
    const/16 v0, 0x9

    iget v1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->n:I

    invoke-virtual {p1, v0, v1, v9}, LX/186;->a(III)V

    .line 2430700
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2430701
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2430702
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2430703
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2430704
    const/16 v0, 0xe

    iget v1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->s:I

    invoke-virtual {p1, v0, v1, v9}, LX/186;->a(III)V

    .line 2430705
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2430706
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2430666
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2430667
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->s()Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2430668
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->s()Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;

    .line 2430669
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->s()Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2430670
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;

    .line 2430671
    iput-object v0, v1, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->p:Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;

    .line 2430672
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->r()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2430673
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->r()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x25ccafee

    invoke-static {v2, v0, v3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2430674
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->r()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2430675
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;

    .line 2430676
    iput v3, v0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->r:I

    move-object v1, v0

    .line 2430677
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2430678
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2430679
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 2430680
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430622
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2430655
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2430656
    invoke-virtual {p1, p2, v2, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->e:I

    .line 2430657
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->f:I

    .line 2430658
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->h:I

    .line 2430659
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->i:I

    .line 2430660
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->k:I

    .line 2430661
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->m:Z

    .line 2430662
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->n:I

    .line 2430663
    const/16 v0, 0xd

    const v1, 0x25ccafee

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->r:I

    .line 2430664
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->s:I

    .line 2430665
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2430653
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2430654
    iget v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->e:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2430650
    new-instance v0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;

    invoke-direct {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;-><init>()V

    .line 2430651
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2430652
    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 2430648
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2430649
    iget v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->f:I

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430646
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->g:Ljava/lang/String;

    .line 2430647
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2430645
    const v0, 0x235749ad

    return v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 2430620
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2430621
    iget v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->h:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2430623
    const v0, 0x4ed245b

    return v0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 2430624
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2430625
    iget v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->k:I

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430626
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->l:Ljava/lang/String;

    .line 2430627
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 2430628
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2430629
    iget-boolean v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->m:Z

    return v0
.end method

.method public final m()I
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 2430630
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2430631
    iget v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->n:I

    return v0
.end method

.method public final mk_()I
    .locals 2

    .prologue
    .line 2430632
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2430633
    iget v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->i:I

    return v0
.end method

.method public final ml_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430634
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->j:Ljava/lang/String;

    .line 2430635
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430636
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->o:Ljava/lang/String;

    .line 2430637
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic o()Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430638
    invoke-direct {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->s()Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;

    move-result-object v0

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430639
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->q:Ljava/lang/String;

    .line 2430640
    iget-object v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final q()I
    .locals 2

    .prologue
    .line 2430641
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2430642
    iget v0, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->s:I

    return v0
.end method

.method public final r()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getVideoThumbnails"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2430643
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2430644
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->r:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
