.class public final Lcom/facebook/profile/discovery/protocol/CurationTagsMutationModels$CurationTagsMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/profile/discovery/protocol/CurationTagsMutationModels$CurationTagsMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2242388
    const-class v0, Lcom/facebook/profile/discovery/protocol/CurationTagsMutationModels$CurationTagsMutationModel;

    new-instance v1, Lcom/facebook/profile/discovery/protocol/CurationTagsMutationModels$CurationTagsMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/profile/discovery/protocol/CurationTagsMutationModels$CurationTagsMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2242389
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2242390
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/profile/discovery/protocol/CurationTagsMutationModels$CurationTagsMutationModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2242391
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2242392
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2242393
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2242394
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2242395
    if-eqz p0, :cond_0

    .line 2242396
    const-string p2, "client_mutation_id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2242397
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2242398
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2242399
    if-eqz p0, :cond_1

    .line 2242400
    const-string p2, "client_subscription_id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2242401
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2242402
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2242403
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2242404
    check-cast p1, Lcom/facebook/profile/discovery/protocol/CurationTagsMutationModels$CurationTagsMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/profile/discovery/protocol/CurationTagsMutationModels$CurationTagsMutationModel$Serializer;->a(Lcom/facebook/profile/discovery/protocol/CurationTagsMutationModels$CurationTagsMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
