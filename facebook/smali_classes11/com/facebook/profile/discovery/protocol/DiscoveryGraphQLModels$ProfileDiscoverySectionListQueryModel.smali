.class public final Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x619c41b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel$DiscoverySectionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2246908
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2246884
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2246885
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2246886
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2246887
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2246888
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel$DiscoverySectionsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2246889
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2246890
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2246891
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2246892
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2246893
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2246894
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel$DiscoverySectionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2246895
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel$DiscoverySectionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel$DiscoverySectionsModel;

    .line 2246896
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel$DiscoverySectionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2246897
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel;

    .line 2246898
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel$DiscoverySectionsModel;

    .line 2246899
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2246900
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel$DiscoverySectionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2246901
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel$DiscoverySectionsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel$DiscoverySectionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel$DiscoverySectionsModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel$DiscoverySectionsModel;

    .line 2246902
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel$DiscoverySectionsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2246903
    new-instance v0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel;

    invoke-direct {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionListQueryModel;-><init>()V

    .line 2246904
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2246905
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2246906
    const v0, -0x538d9898

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2246907
    const v0, -0x6747e1ce

    return v0
.end method
