.class public final Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2244780
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2244783
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2244781
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2244782
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2244772
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2244773
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2244774
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2244767
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2244768
    iget v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2244775
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2244776
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2244777
    iget v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 2244778
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2244779
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2244769
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2244770
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2244771
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2244764
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2244765
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;->e:I

    .line 2244766
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2244761
    new-instance v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;

    invoke-direct {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;-><init>()V

    .line 2244762
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2244763
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2244760
    const v0, -0x64ca5df9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2244759
    const v0, 0x34f0adde

    return v0
.end method
