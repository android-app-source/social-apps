.class public final Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2243396
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel;

    new-instance v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2243397
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2243399
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2243400
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2243401
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2243402
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2243403
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2243404
    if-eqz v2, :cond_0

    .line 2243405
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2243406
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2243407
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2243408
    if-eqz v2, :cond_1

    .line 2243409
    const-string p0, "selected_and_suggested_tags"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2243410
    invoke-static {v1, v2, p1, p2}, LX/FTc;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2243411
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2243412
    if-eqz v2, :cond_2

    .line 2243413
    const-string p0, "selected_tags"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2243414
    invoke-static {v1, v2, p1}, LX/FTa;->a(LX/15i;ILX/0nX;)V

    .line 2243415
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2243416
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2243398
    check-cast p1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel$Serializer;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
