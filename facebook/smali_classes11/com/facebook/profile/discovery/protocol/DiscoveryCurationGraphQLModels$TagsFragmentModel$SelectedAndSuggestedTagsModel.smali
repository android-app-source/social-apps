.class public final Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x578e7029
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2244970
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2244934
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2244968
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2244969
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2244965
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2244966
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2244967
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2244957
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2244958
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2244959
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2244960
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2244961
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2244962
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2244963
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2244964
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2244955
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->e:Ljava/util/List;

    .line 2244956
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2244942
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2244943
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2244944
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2244945
    if-eqz v1, :cond_2

    .line 2244946
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    .line 2244947
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 2244948
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2244949
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    .line 2244950
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2244951
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    .line 2244952
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    .line 2244953
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2244954
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2244939
    new-instance v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    invoke-direct {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;-><init>()V

    .line 2244940
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2244941
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2244938
    const v0, -0x65e1c5d8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2244937
    const v0, 0x34f0adde

    return v0
.end method

.method public final j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2244935
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    .line 2244936
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    return-object v0
.end method
