.class public final Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketCoreFieldsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2246359
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketCoreFieldsModel;

    new-instance v1, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketCoreFieldsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketCoreFieldsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2246360
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2246361
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2246362
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2246363
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2246364
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_a

    .line 2246365
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2246366
    :goto_0
    move v1, v2

    .line 2246367
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2246368
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2246369
    new-instance v1, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketCoreFieldsModel;

    invoke-direct {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketCoreFieldsModel;-><init>()V

    .line 2246370
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2246371
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2246372
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2246373
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2246374
    :cond_0
    return-object v1

    .line 2246375
    :cond_1
    const-string p0, "unread_count"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 2246376
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v5, v1

    move v1, v3

    .line 2246377
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_8

    .line 2246378
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2246379
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2246380
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v10, :cond_2

    .line 2246381
    const-string p0, "bucket_id"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2246382
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 2246383
    :cond_3
    const-string p0, "fbglyph_image"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 2246384
    invoke-static {p1, v0}, LX/FTq;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2246385
    :cond_4
    const-string p0, "subtitle"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2246386
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 2246387
    :cond_5
    const-string p0, "title"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2246388
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2246389
    :cond_6
    const-string p0, "uri"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 2246390
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2246391
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2246392
    :cond_8
    const/4 v10, 0x6

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 2246393
    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2246394
    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 2246395
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 2246396
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 2246397
    if-eqz v1, :cond_9

    .line 2246398
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v5, v2}, LX/186;->a(III)V

    .line 2246399
    :cond_9
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 2246400
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    goto/16 :goto_1
.end method
