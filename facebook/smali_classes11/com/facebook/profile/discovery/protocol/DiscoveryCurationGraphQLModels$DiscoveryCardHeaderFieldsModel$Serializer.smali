.class public final Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2243900
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel;

    new-instance v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2243901
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2243845
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2243846
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2243847
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2243848
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2243849
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2243850
    if-eqz v2, :cond_0

    .line 2243851
    const-string p0, "alternate_name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2243852
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2243853
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2243854
    if-eqz v2, :cond_1

    .line 2243855
    const-string p0, "cover_photo"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2243856
    invoke-static {v1, v2, p1, p2}, LX/FTN;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2243857
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2243858
    if-eqz v2, :cond_2

    .line 2243859
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2243860
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2243861
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2243862
    if-eqz v2, :cond_3

    .line 2243863
    const-string p0, "is_verified"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2243864
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2243865
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2243866
    if-eqz v2, :cond_4

    .line 2243867
    const-string p0, "is_work_user"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2243868
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2243869
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2243870
    if-eqz v2, :cond_5

    .line 2243871
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2243872
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2243873
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2243874
    if-eqz v2, :cond_6

    .line 2243875
    const-string p0, "preliminaryProfilePicture"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2243876
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2243877
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2243878
    if-eqz v2, :cond_7

    .line 2243879
    const-string p0, "profile_photo"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2243880
    invoke-static {v1, v2, p1, p2}, LX/FTS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2243881
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2243882
    if-eqz v2, :cond_8

    .line 2243883
    const-string p0, "profile_picture"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2243884
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2243885
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2243886
    if-eqz v2, :cond_9

    .line 2243887
    const-string p0, "profile_picture_is_silhouette"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2243888
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2243889
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2243890
    if-eqz v2, :cond_a

    .line 2243891
    const-string p0, "profile_video"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2243892
    invoke-static {v1, v2, p1, p2}, LX/FTP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2243893
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2243894
    if-eqz v2, :cond_b

    .line 2243895
    const-string p0, "structured_name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2243896
    invoke-static {v1, v2, p1, p2}, LX/3EF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2243897
    :cond_b
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2243898
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2243899
    check-cast p1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$Serializer;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
