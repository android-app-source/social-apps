.class public final Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/FSi;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x50012a26
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2242482
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2242483
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2242484
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2242485
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2242475
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2242476
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2242477
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 4

    .prologue
    .line 2242496
    iput-object p1, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2242497
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2242498
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2242499
    if-eqz v0, :cond_0

    .line 2242500
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSavedState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2242501
    :cond_0
    return-void

    .line 2242502
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2242486
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;->e:Ljava/lang/String;

    .line 2242487
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2242488
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2242489
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2242490
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;->p()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2242491
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2242492
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2242493
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2242494
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2242495
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2242478
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2242479
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2242480
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2242481
    new-instance v0, LX/FSl;

    invoke-direct {v0, p1}, LX/FSl;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2242470
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2242458
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2242459
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;->p()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2242460
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2242461
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 2242462
    :goto_0
    return-void

    .line 2242463
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2242464
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2242465
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-direct {p0, p2}, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;->a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 2242466
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2242467
    new-instance v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;

    invoke-direct {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;-><init>()V

    .line 2242468
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2242469
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2242471
    const v0, 0x77299666

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2242472
    const v0, 0x285feb

    return v0
.end method

.method public final p()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2242473
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2242474
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method
