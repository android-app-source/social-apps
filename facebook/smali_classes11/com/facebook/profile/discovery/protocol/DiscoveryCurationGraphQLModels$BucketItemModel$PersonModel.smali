.class public final Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/Eon;
.implements LX/Eoo;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/FSi;
.implements LX/FSx;
.implements LX/FSy;
.implements LX/FSz;
.implements LX/5wL;
.implements LX/5wM;
.implements LX/5wN;
.implements LX/5wO;
.implements LX/5wP;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x16162486
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$Serializer;
.end annotation


# instance fields
.field private A:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Z

.field private z:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2243277
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2243104
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2243105
    const/16 v0, 0x1c

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2243106
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2243100
    const/16 v0, 0x1c

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2243101
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2243102
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 2243107
    iput-object p1, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->n:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2243108
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2243109
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2243110
    if-eqz v0, :cond_0

    .line 2243111
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x9

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2243112
    :cond_0
    return-void

    .line 2243113
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 4

    .prologue
    .line 2243114
    iput-object p1, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->F:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2243115
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2243116
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2243117
    if-eqz v0, :cond_0

    .line 2243118
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x1b

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSavedState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2243119
    :cond_0
    return-void

    .line 2243120
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V
    .locals 4

    .prologue
    .line 2243121
    iput-object p1, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->A:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2243122
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2243123
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2243124
    if-eqz v0, :cond_0

    .line 2243125
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x16

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2243126
    :cond_0
    return-void

    .line 2243127
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 4

    .prologue
    .line 2243128
    iput-object p1, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->C:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2243129
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2243130
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2243131
    if-eqz v0, :cond_0

    .line 2243132
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x18

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2243133
    :cond_0
    return-void

    .line 2243134
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 2243135
    iput-boolean p1, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->h:Z

    .line 2243136
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2243137
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2243138
    if-eqz v0, :cond_0

    .line 2243139
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 2243140
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 2243141
    iput-boolean p1, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->i:Z

    .line 2243142
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2243143
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2243144
    if-eqz v0, :cond_0

    .line 2243145
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 2243146
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 2243147
    iput-boolean p1, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->j:Z

    .line 2243148
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2243149
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2243150
    if-eqz v0, :cond_0

    .line 2243151
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 2243152
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()Z
    .locals 2

    .prologue
    .line 2243153
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2243154
    iget-boolean v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->r:Z

    return v0
.end method

.method public final synthetic B()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243155
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->K()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic C()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243156
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->N()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final D()Z
    .locals 2

    .prologue
    .line 2243157
    const/4 v0, 0x2

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2243158
    iget-boolean v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->y:Z

    return v0
.end method

.method public final synthetic E()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243159
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->O()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;

    move-result-object v0

    return-object v0
.end method

.method public final F()Z
    .locals 2

    .prologue
    .line 2243160
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2243161
    iget-boolean v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->j:Z

    return v0
.end method

.method public final G()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243278
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;

    .line 2243279
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;

    return-object v0
.end method

.method public final H()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243280
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->m:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->m:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    .line 2243281
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->m:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    return-object v0
.end method

.method public final I()Z
    .locals 2

    .prologue
    .line 2243282
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2243283
    iget-boolean v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->p:Z

    return v0
.end method

.method public final J()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243286
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->t:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->t:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 2243287
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->t:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    return-object v0
.end method

.method public final K()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243275
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->u:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->u:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2243276
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->u:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final L()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243284
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->v:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->v:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    .line 2243285
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->v:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    return-object v0
.end method

.method public final M()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243273
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->w:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->w:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel;

    .line 2243274
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->w:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel;

    return-object v0
.end method

.method public final N()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243271
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->x:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfilePictureModel;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->x:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfilePictureModel;

    .line 2243272
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->x:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfilePictureModel;

    return-object v0
.end method

.method public final O()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243269
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->z:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->z:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;

    .line 2243270
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->z:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;

    return-object v0
.end method

.method public final P()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243267
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->B:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->B:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    .line 2243268
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->B:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    return-object v0
.end method

.method public final Q()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243265
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->D:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->D:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;

    .line 2243266
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->D:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 21

    .prologue
    .line 2243215
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2243216
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->x()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2243217
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->G()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2243218
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->H()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2243219
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 2243220
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->c()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2243221
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->ce_()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2243222
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->J()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2243223
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->K()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2243224
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->L()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 2243225
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->M()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 2243226
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->N()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfilePictureModel;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 2243227
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->O()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 2243228
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->j()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v14

    .line 2243229
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->P()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 2243230
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v16

    .line 2243231
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->Q()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 2243232
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->b()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 2243233
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->p()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    .line 2243234
    const/16 v20, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2243235
    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2243236
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->f:Z

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2243237
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->g:Z

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2243238
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->h:Z

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2243239
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->i:Z

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2243240
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->j:Z

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2243241
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->k:Z

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2243242
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2243243
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2243244
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2243245
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 2243246
    const/16 v2, 0xb

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->p:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2243247
    const/16 v2, 0xc

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->q:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2243248
    const/16 v2, 0xd

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->r:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2243249
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 2243250
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2243251
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2243252
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2243253
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2243254
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2243255
    const/16 v2, 0x14

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->y:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2243256
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2243257
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 2243258
    const/16 v2, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2243259
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2243260
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2243261
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2243262
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2243263
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2243264
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2243162
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2243163
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->G()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2243164
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->G()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;

    .line 2243165
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->G()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2243166
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    .line 2243167
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;

    .line 2243168
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->H()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2243169
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->H()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    .line 2243170
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->H()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2243171
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    .line 2243172
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->m:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    .line 2243173
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->J()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2243174
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->J()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 2243175
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->J()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2243176
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    .line 2243177
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->t:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 2243178
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->K()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2243179
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->K()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2243180
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->K()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2243181
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    .line 2243182
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->u:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2243183
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->L()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2243184
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->L()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    .line 2243185
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->L()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 2243186
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    .line 2243187
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->v:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    .line 2243188
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->M()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2243189
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->M()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel;

    .line 2243190
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->M()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 2243191
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    .line 2243192
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->w:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel;

    .line 2243193
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->N()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2243194
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->N()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfilePictureModel;

    .line 2243195
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->N()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 2243196
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    .line 2243197
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->x:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfilePictureModel;

    .line 2243198
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->O()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 2243199
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->O()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;

    .line 2243200
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->O()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 2243201
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    .line 2243202
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->z:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;

    .line 2243203
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->P()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 2243204
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->P()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    .line 2243205
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->P()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 2243206
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    .line 2243207
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->B:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    .line 2243208
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->Q()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 2243209
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->Q()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;

    .line 2243210
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->Q()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 2243211
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    .line 2243212
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->D:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;

    .line 2243213
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2243214
    if-nez v1, :cond_a

    :goto_0
    return-object p0

    :cond_a
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2243103
    new-instance v0, LX/FT4;

    invoke-direct {v0, p1}, LX/FT4;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243002
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2243065
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2243066
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->f:Z

    .line 2243067
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->g:Z

    .line 2243068
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->h:Z

    .line 2243069
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->i:Z

    .line 2243070
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->j:Z

    .line 2243071
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->k:Z

    .line 2243072
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->p:Z

    .line 2243073
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->q:Z

    .line 2243074
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->r:Z

    .line 2243075
    const/16 v0, 0x14

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->y:Z

    .line 2243076
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2243035
    const-string v0, "can_viewer_message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2243036
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2243037
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2243038
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    .line 2243039
    :goto_0
    return-void

    .line 2243040
    :cond_0
    const-string v0, "can_viewer_poke"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2243041
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->e()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2243042
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2243043
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2243044
    :cond_1
    const-string v0, "can_viewer_post"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2243045
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2243046
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2243047
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2243048
    :cond_2
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2243049
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2243050
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2243051
    const/16 v0, 0x9

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2243052
    :cond_3
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2243053
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->j()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2243054
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2243055
    const/16 v0, 0x16

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2243056
    :cond_4
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2243057
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2243058
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2243059
    const/16 v0, 0x18

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 2243060
    :cond_5
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2243061
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->p()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2243062
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2243063
    const/16 v0, 0x1b

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 2243064
    :cond_6
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2243020
    const-string v0, "can_viewer_message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2243021
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->a(Z)V

    .line 2243022
    :cond_0
    :goto_0
    return-void

    .line 2243023
    :cond_1
    const-string v0, "can_viewer_poke"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2243024
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->b(Z)V

    goto :goto_0

    .line 2243025
    :cond_2
    const-string v0, "can_viewer_post"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2243026
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->c(Z)V

    goto :goto_0

    .line 2243027
    :cond_3
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2243028
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    goto :goto_0

    .line 2243029
    :cond_4
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2243030
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    goto :goto_0

    .line 2243031
    :cond_5
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2243032
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    goto :goto_0

    .line 2243033
    :cond_6
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2243034
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-direct {p0, p2}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2243017
    new-instance v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    invoke-direct {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;-><init>()V

    .line 2243018
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2243019
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243015
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->E:Ljava/lang/String;

    const/16 v1, 0x1a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->E:Ljava/lang/String;

    .line 2243016
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->E:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243013
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->o:Ljava/lang/String;

    .line 2243014
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243011
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->n:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->n:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2243012
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->n:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final ce_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243009
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->s:Ljava/lang/String;

    .line 2243010
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 2243007
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2243008
    iget-boolean v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->h:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2243006
    const v0, 0x307a34de

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 2243004
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2243005
    iget-boolean v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->i:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2243003
    const v0, 0x285feb

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243000
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->A:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->A:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2243001
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->A:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243077
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->C:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->C:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2243078
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->C:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 2243079
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2243080
    iget-boolean v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->f:Z

    return v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 2243081
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2243082
    iget-boolean v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->g:Z

    return v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 2243083
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2243084
    iget-boolean v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->k:Z

    return v0
.end method

.method public final synthetic o()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243085
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->H()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    move-result-object v0

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243086
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->F:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->F:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2243087
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->F:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method public final synthetic q()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243088
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->L()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic r()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243089
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->Q()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic s()LX/2rX;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243090
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->J()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic t()LX/FSv;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243091
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->G()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/Eoq;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243092
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->N()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic v()LX/1k2;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243093
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->P()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic w()LX/FSt;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243094
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->M()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final x()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243095
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->e:Ljava/lang/String;

    .line 2243096
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic y()LX/Eos;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2243097
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->G()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;

    move-result-object v0

    return-object v0
.end method

.method public final z()Z
    .locals 2

    .prologue
    .line 2243098
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2243099
    iget-boolean v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->q:Z

    return v0
.end method
