.class public final Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x291f116c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2244797
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2244821
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2244819
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2244820
    return-void
.end method

.method private a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2244817
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;

    .line 2244818
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2244811
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2244812
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2244813
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2244814
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2244815
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2244816
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2244803
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2244804
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2244805
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;

    .line 2244806
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2244807
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel;

    .line 2244808
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;

    .line 2244809
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2244810
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2244800
    new-instance v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel;

    invoke-direct {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel;-><init>()V

    .line 2244801
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2244802
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2244799
    const v0, -0x546e01a8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2244798
    const v0, -0x61613e4e

    return v0
.end method
