.class public final Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6552e663
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2246791
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2246790
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2246788
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2246789
    return-void
.end method

.method private j()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2246786
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;

    .line 2246787
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2246784
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->f:Ljava/lang/String;

    .line 2246785
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2246774
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2246775
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2246776
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2246777
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 2246778
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2246779
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2246780
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2246781
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2246782
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2246783
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2246792
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2246793
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2246794
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;

    .line 2246795
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2246796
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;

    .line 2246797
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;

    .line 2246798
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2246799
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2246773
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2246765
    new-instance v0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;

    invoke-direct {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;-><init>()V

    .line 2246766
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2246767
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2246772
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2246770
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->g:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->g:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    .line 2246771
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->g:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2246769
    const v0, 0x1a586a59

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2246768
    const v0, -0x42ae0142

    return v0
.end method
