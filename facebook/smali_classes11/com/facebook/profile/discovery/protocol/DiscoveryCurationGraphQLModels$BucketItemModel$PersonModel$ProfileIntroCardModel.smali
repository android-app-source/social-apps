.class public final Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/FSw;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x459ae53e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel$ContextItemsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2242942
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2242941
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2242939
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2242940
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2242936
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2242937
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2242938
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2242924
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2242925
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2242926
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel$ContextItemsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2242927
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->l()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2242928
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2242929
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2242930
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2242931
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2242932
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2242933
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2242934
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2242935
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2242901
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2242902
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2242903
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2242904
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2242905
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    .line 2242906
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2242907
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel$ContextItemsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2242908
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel$ContextItemsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel$ContextItemsModel;

    .line 2242909
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel$ContextItemsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2242910
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    .line 2242911
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->f:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel$ContextItemsModel;

    .line 2242912
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->l()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2242913
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->l()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    .line 2242914
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->l()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2242915
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    .line 2242916
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->g:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    .line 2242917
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2242918
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2242919
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2242920
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    .line 2242921
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2242922
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2242923
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2242900
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2242897
    new-instance v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    invoke-direct {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;-><init>()V

    .line 2242898
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2242899
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel$ContextItemsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2242896
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel$ContextItemsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2242885
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->l()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2242895
    const v0, -0x4edc10af

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2242894
    const v0, 0x7d57e813

    return v0
.end method

.method public final j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2242892
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2242893
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel$ContextItemsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2242890
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->f:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel$ContextItemsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel$ContextItemsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel$ContextItemsModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->f:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel$ContextItemsModel;

    .line 2242891
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->f:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel$ContextItemsModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2242888
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->g:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->g:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    .line 2242889
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->g:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2242886
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2242887
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method
