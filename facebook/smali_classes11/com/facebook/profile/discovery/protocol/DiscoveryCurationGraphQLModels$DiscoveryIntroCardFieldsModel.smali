.class public final Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;
.implements LX/FSy;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3a31d30e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2244596
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2244595
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2244593
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2244594
    return-void
.end method

.method private a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2244591
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel;

    .line 2244592
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2244585
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2244586
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2244587
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2244588
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2244589
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2244590
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2244577
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2244578
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2244579
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel;

    .line 2244580
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2244581
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel;

    .line 2244582
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel;

    .line 2244583
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2244584
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2244576
    new-instance v0, LX/FT8;

    invoke-direct {v0, p1}, LX/FT8;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2244574
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2244575
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2244568
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2244571
    new-instance v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel;

    invoke-direct {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel;-><init>()V

    .line 2244572
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2244573
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2244570
    const v0, 0x2c7610be

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2244569
    const v0, 0x285feb

    return v0
.end method
