.class public final Lcom/facebook/profile/discovery/protocol/DiscoveryMutationsModels$ProfileDiscoverySaveMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x37aa3a13
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryMutationsModels$ProfileDiscoverySaveMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryMutationsModels$ProfileDiscoverySaveMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2247395
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryMutationsModels$ProfileDiscoverySaveMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2247396
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryMutationsModels$ProfileDiscoverySaveMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2247397
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2247398
    return-void
.end method

.method private a()Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2247399
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryMutationsModels$ProfileDiscoverySaveMutationModel;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryMutationsModels$ProfileDiscoverySaveMutationModel;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;

    .line 2247400
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryMutationsModels$ProfileDiscoverySaveMutationModel;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2247401
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2247402
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryMutationsModels$ProfileDiscoverySaveMutationModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2247403
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2247404
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2247405
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2247406
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2247407
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2247408
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryMutationsModels$ProfileDiscoverySaveMutationModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2247409
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryMutationsModels$ProfileDiscoverySaveMutationModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;

    .line 2247410
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryMutationsModels$ProfileDiscoverySaveMutationModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2247411
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryMutationsModels$ProfileDiscoverySaveMutationModel;

    .line 2247412
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryMutationsModels$ProfileDiscoverySaveMutationModel;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;

    .line 2247413
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2247414
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2247415
    new-instance v0, Lcom/facebook/profile/discovery/protocol/DiscoveryMutationsModels$ProfileDiscoverySaveMutationModel;

    invoke-direct {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryMutationsModels$ProfileDiscoverySaveMutationModel;-><init>()V

    .line 2247416
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2247417
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2247418
    const v0, 0x6bc221f0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2247419
    const v0, -0x4c87f59f

    return v0
.end method
