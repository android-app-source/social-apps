.class public final Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x653b97a1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemPhotoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2247613
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2247578
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2247611
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2247612
    return-void
.end method

.method private j()Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2247609
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;->e:Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;->e:Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    .line 2247610
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;->e:Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    return-object v0
.end method

.method private k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemPhotoFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2247607
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;->f:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemPhotoFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemPhotoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemPhotoFieldsModel;

    iput-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;->f:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemPhotoFieldsModel;

    .line 2247608
    iget-object v0, p0, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;->f:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemPhotoFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2247599
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2247600
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;->j()Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2247601
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemPhotoFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2247602
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2247603
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2247604
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2247605
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2247606
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2247586
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2247587
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;->j()Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2247588
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;->j()Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    .line 2247589
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;->j()Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2247590
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;

    .line 2247591
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;->e:Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    .line 2247592
    :cond_0
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemPhotoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2247593
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemPhotoFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemPhotoFieldsModel;

    .line 2247594
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemPhotoFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2247595
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;

    .line 2247596
    iput-object v0, v1, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;->f:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemPhotoFieldsModel;

    .line 2247597
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2247598
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()LX/5vn;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2247585
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;->j()Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/G1S;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2247584
    invoke-direct {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemPhotoFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2247581
    new-instance v0, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;

    invoke-direct {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;-><init>()V

    .line 2247582
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2247583
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2247580
    const v0, -0x1fe821d2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2247579
    const v0, 0x4b04278a    # 8660874.0f

    return v0
.end method
