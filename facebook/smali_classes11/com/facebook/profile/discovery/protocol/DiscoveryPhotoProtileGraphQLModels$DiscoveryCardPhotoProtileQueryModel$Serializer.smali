.class public final Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryCardPhotoProtileQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryCardPhotoProtileQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2247522
    const-class v0, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryCardPhotoProtileQueryModel;

    new-instance v1, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryCardPhotoProtileQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryCardPhotoProtileQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2247523
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2247502
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryCardPhotoProtileQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2247503
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2247504
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2247505
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2247506
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2247507
    if-eqz v2, :cond_2

    .line 2247508
    const-string p0, "profile_tile_sections"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2247509
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2247510
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 2247511
    if-eqz p0, :cond_1

    .line 2247512
    const-string v0, "nodes"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2247513
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2247514
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, p0}, LX/15i;->c(I)I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2247515
    invoke-virtual {v1, p0, v0}, LX/15i;->q(II)I

    move-result v2

    invoke-static {v1, v2, p1, p2}, LX/FUE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2247516
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2247517
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2247518
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2247519
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2247520
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2247521
    check-cast p1, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryCardPhotoProtileQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryCardPhotoProtileQueryModel$Serializer;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryCardPhotoProtileQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
