.class public final Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/GNP;


# direct methods
.method public constructor <init>(LX/GNP;)V
    .locals 0

    .prologue
    .line 2345866
    iput-object p1, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 2345867
    iget-object v1, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v0, v0, LX/GNP;->d:Landroid/view/ViewGroup;

    const v2, 0x7f0d3189

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2345868
    iput-object v0, v1, LX/GNP;->i:Landroid/widget/TextView;

    .line 2345869
    iget-object v1, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v0, v0, LX/GNP;->d:Landroid/view/ViewGroup;

    const v2, 0x7f0d3188

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2345870
    iput-object v0, v1, LX/GNP;->j:Landroid/widget/TextView;

    .line 2345871
    iget-object v1, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v0, v0, LX/GNP;->d:Landroid/view/ViewGroup;

    const v2, 0x7f0d3185

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/walkthrough/HoleView;

    .line 2345872
    iput-object v0, v1, LX/GNP;->h:Lcom/facebook/adinterfaces/walkthrough/HoleView;

    .line 2345873
    iget-object v1, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v0, v0, LX/GNP;->d:Landroid/view/ViewGroup;

    const v2, 0x7f0d3186

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2345874
    iput-object v0, v1, LX/GNP;->n:Landroid/widget/TextView;

    .line 2345875
    iget-object v1, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v0, v0, LX/GNP;->d:Landroid/view/ViewGroup;

    const v2, 0x7f0d3187

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2345876
    iput-object v0, v1, LX/GNP;->a:Landroid/widget/ImageView;

    .line 2345877
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v1, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v1, v1, LX/GNP;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    .line 2345878
    iput v1, v0, LX/GNP;->b:I

    .line 2345879
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v1, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v1, v1, LX/GNP;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    .line 2345880
    iput v1, v0, LX/GNP;->c:I

    .line 2345881
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v0, v0, LX/GNP;->i:Landroid/widget/TextView;

    new-instance v1, LX/GNM;

    invoke-direct {v1, p0}, LX/GNM;-><init>(Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2345882
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v0, v0, LX/GNP;->j:Landroid/widget/TextView;

    new-instance v1, LX/GNN;

    invoke-direct {v1, p0}, LX/GNN;-><init>(Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2345883
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 2345884
    iput-object v1, v0, LX/GNP;->f:Landroid/graphics/Rect;

    .line 2345885
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 2345886
    iput-object v1, v0, LX/GNP;->g:Landroid/graphics/Rect;

    .line 2345887
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v0, v0, LX/GNP;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v1, v1, LX/GNP;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 2345888
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v0, v0, LX/GNP;->j:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v1, v1, LX/GNP;->g:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 2345889
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v1, p0, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v1, v1, LX/GNP;->i:Landroid/widget/TextView;

    invoke-static {v0, v1}, LX/GNP;->a$redex0(LX/GNP;Landroid/view/View;)V

    .line 2345890
    return-void
.end method
