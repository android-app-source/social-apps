.class public Lcom/facebook/adinterfaces/walkthrough/HoleView;
.super Landroid/view/View;
.source ""


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:Landroid/graphics/Paint;

.field private c:Landroid/graphics/Canvas;

.field private d:Landroid/graphics/Bitmap;

.field private e:Landroid/graphics/Rect;

.field private f:Landroid/graphics/Rect;

.field private g:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/1FZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2346040
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2346041
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2346044
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2346045
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2346042
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2346043
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 2346027
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_0

    .line 2346028
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/adinterfaces/walkthrough/HoleView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 2346029
    :cond_0
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->a:Landroid/graphics/Paint;

    .line 2346030
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/walkthrough/HoleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2346031
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->a:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 2346032
    const-class v0, Lcom/facebook/adinterfaces/walkthrough/HoleView;

    invoke-static {v0, p0}, Lcom/facebook/adinterfaces/walkthrough/HoleView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2346033
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->h:LX/1FZ;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/1FZ;->a(II)LX/1FJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->g:LX/1FJ;

    .line 2346034
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->g:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->d:Landroid/graphics/Bitmap;

    .line 2346035
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->d:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->c:Landroid/graphics/Canvas;

    .line 2346036
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->b:Landroid/graphics/Paint;

    .line 2346037
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->b:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/walkthrough/HoleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a036e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2346038
    new-instance v0, LX/GNS;

    invoke-direct {v0, p0}, LX/GNS;-><init>(Lcom/facebook/adinterfaces/walkthrough/HoleView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/walkthrough/HoleView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2346039
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/adinterfaces/walkthrough/HoleView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;

    invoke-static {v0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v0

    check-cast v0, LX/1FZ;

    iput-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->h:LX/1FZ;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2346012
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->g:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 2346013
    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 2346014
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->e:Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->f:Landroid/graphics/Rect;

    .line 2346015
    iput-object p1, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->e:Landroid/graphics/Rect;

    .line 2346016
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/walkthrough/HoleView;->invalidate()V

    .line 2346017
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2346018
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->e:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 2346019
    :goto_0
    return-void

    .line 2346020
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->a:Landroid/graphics/Paint;

    if-nez v0, :cond_1

    .line 2346021
    invoke-direct {p0, p1}, Lcom/facebook/adinterfaces/walkthrough/HoleView;->a(Landroid/graphics/Canvas;)V

    .line 2346022
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->c:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->c:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    int-to-float v3, v2

    iget-object v2, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->c:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    int-to-float v4, v2

    iget-object v5, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->b:Landroid/graphics/Paint;

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2346023
    :cond_1
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->f:Landroid/graphics/Rect;

    if-eqz v0, :cond_2

    .line 2346024
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->c:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->f:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2346025
    :cond_2
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->c:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->e:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2346026
    iget-object v0, p0, Lcom/facebook/adinterfaces/walkthrough/HoleView;->d:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method
