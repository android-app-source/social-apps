.class public Lcom/facebook/adinterfaces/AdInterfacesTermsOfServiceActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2329034
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2329035
    const v0, 0x7f0d0492

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/AdInterfacesTermsOfServiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2329036
    if-nez v0, :cond_0

    .line 2329037
    :goto_0
    return-void

    .line 2329038
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/AdInterfacesTermsOfServiceActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080bbb

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/facebook/adinterfaces/AdInterfacesTermsOfServiceActivity;)V
    .locals 0

    .prologue
    .line 2329039
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2329040
    const v0, 0x7f0d0491

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2329041
    if-nez v0, :cond_0

    .line 2329042
    :goto_0
    return-void

    .line 2329043
    :cond_0
    const v1, 0x7f080bba

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 2329044
    new-instance v1, LX/GCe;

    invoke-direct {v1, p0}, LX/GCe;-><init>(Lcom/facebook/adinterfaces/AdInterfacesTermsOfServiceActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2329045
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2329046
    const v0, 0x7f030090

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/AdInterfacesTermsOfServiceActivity;->setContentView(I)V

    .line 2329047
    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesTermsOfServiceActivity;->b()V

    .line 2329048
    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesTermsOfServiceActivity;->a()V

    .line 2329049
    return-void
.end method
