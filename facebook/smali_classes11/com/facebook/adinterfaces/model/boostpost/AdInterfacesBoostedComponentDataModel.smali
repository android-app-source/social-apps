.class public Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;
.super Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

.field public c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

.field public d:Lcom/facebook/adinterfaces/model/EventSpecModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;

.field public h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

.field private i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;

.field public l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLCallToActionType;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:[Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2334235
    new-instance v0, LX/GGM;

    invoke-direct {v0}, LX/GGM;-><init>()V

    sput-object v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/GGN;)V
    .locals 1

    .prologue
    .line 2334224
    invoke-direct {p0, p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;-><init>(LX/GGI;)V

    .line 2334225
    iget-object v0, p1, LX/GGN;->a:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2334226
    iget-object v0, p1, LX/GGN;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 2334227
    iget-object v0, p1, LX/GGN;->c:Lcom/facebook/adinterfaces/model/EventSpecModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    .line 2334228
    iget-object v0, p1, LX/GGN;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->e:Ljava/lang/String;

    .line 2334229
    iget-object v0, p1, LX/GGN;->e:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->f:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2334230
    iget-object v0, p1, LX/GGN;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->g:Ljava/lang/String;

    .line 2334231
    iget-object v0, p1, LX/GGN;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2334232
    iget-object v0, p1, LX/GGN;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    .line 2334233
    iget-object v0, p1, LX/GGN;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->j:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    .line 2334234
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 2334201
    invoke-direct {p0, p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;-><init>(Landroid/os/Parcel;)V

    .line 2334202
    const-class v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2334203
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 2334204
    const-class v0, LX/4DA;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/EventSpecModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    .line 2334205
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->e:Ljava/lang/String;

    .line 2334206
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 2334207
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->f:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2334208
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->g:Ljava/lang/String;

    .line 2334209
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2334210
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    .line 2334211
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->j:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    .line 2334212
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->k:Ljava/lang/String;

    .line 2334213
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2334214
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 2334215
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2334216
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2334217
    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2334218
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->l:LX/0Px;

    .line 2334219
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->m:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2334220
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->n:Ljava/lang/String;

    .line 2334221
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->o:Ljava/lang/String;

    .line 2334222
    return-void

    .line 2334223
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final A()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2334200
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->j:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    return-object v0
.end method

.method public final C()Z
    .locals 1

    .prologue
    .line 2334195
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2334196
    if-eqz v0, :cond_0

    .line 2334197
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2334198
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->w()Z

    move-result v0

    .line 2334199
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final I()Lcom/facebook/graphql/model/FeedUnit;
    .locals 1

    .prologue
    .line 2334186
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2334187
    if-eqz v0, :cond_0

    .line 2334188
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2334189
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->t()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2334190
    :goto_0
    if-eqz v0, :cond_1

    .line 2334191
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2334192
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->t()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    :goto_1
    return-object v0

    .line 2334193
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2334194
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public Q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2334185
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public R()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2334169
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v1

    sget-object v2, LX/8wL;->BOOST_POST:LX/8wL;

    if-eq v1, v2, :cond_1

    .line 2334170
    :cond_0
    :goto_0
    return v0

    .line 2334171
    :cond_1
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v1, v1

    .line 2334172
    if-eqz v1, :cond_2

    .line 2334173
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v1, v1

    .line 2334174
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->B()Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2334175
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v1, v1

    .line 2334176
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->B()Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->SINGLE_SHARE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    if-eq v1, v2, :cond_0

    .line 2334177
    :cond_2
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v1, v1

    .line 2334178
    if-eqz v1, :cond_0

    .line 2334179
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v1, v1

    .line 2334180
    iget-boolean v2, v1, Lcom/facebook/adinterfaces/model/CreativeAdModel;->p:Z

    move v1, v2

    .line 2334181
    if-nez v1, :cond_0

    .line 2334182
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v1, v1

    .line 2334183
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-object v1, v2

    .line 2334184
    invoke-static {v1}, LX/GCi;->isCTAValidForURL(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public S()I
    .locals 1

    .prologue
    .line 2334168
    const v0, 0x7f080b98

    return v0
.end method

.method public T()I
    .locals 1

    .prologue
    .line 2334167
    const v0, 0x7f080b99

    return v0
.end method

.method public final aa()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2334236
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2334237
    if-eqz v0, :cond_0

    .line 2334238
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2334239
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->s()LX/0Px;

    move-result-object v0

    .line 2334240
    :goto_0
    return-object v0

    .line 2334241
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2334242
    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 2334117
    const/4 v0, 0x0

    return v0
.end method

.method public h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2334120
    iput-object p1, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->k:Ljava/lang/String;

    .line 2334121
    return-void
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2334122
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$AdAccountModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2334123
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$AdAccountModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$AdAccountModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->a:Ljava/lang/String;

    .line 2334124
    :cond_1
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final p()LX/175;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2334118
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2334119
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->C()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final s()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2334125
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2334126
    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel;

    move-result-object v0

    :goto_1
    return-object v0

    .line 2334127
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2334128
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final t()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2334129
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2334130
    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;

    move-result-object v0

    :goto_1
    return-object v0

    .line 2334131
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2334132
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 2334133
    const/4 v0, 0x1

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 2334134
    invoke-super {p0, p1, p2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2334135
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2334136
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2334137
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2334138
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334139
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->f:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->f:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->name()Ljava/lang/String;

    move-result-object v0

    .line 2334140
    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334141
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334142
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2334143
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2334144
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->j:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2334145
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334146
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2334147
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->l:LX/0Px;

    if-eqz v0, :cond_0

    .line 2334148
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->l:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, p2, :cond_0

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->l:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2334149
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2334150
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2334151
    :cond_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2334152
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->m:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2334153
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334154
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334155
    return-void

    .line 2334156
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x()LX/4Cf;
    .locals 1

    .prologue
    .line 2334157
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v0, v0

    .line 2334158
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2334159
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v0, v0

    .line 2334160
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/CreativeAdModel;->n()LX/4Cf;

    move-result-object v0

    goto :goto_0
.end method

.method public final y()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;
    .locals 1

    .prologue
    .line 2334161
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2334162
    if-eqz v0, :cond_0

    .line 2334163
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2334164
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    .line 2334165
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final z()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2334166
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    return-object v0
.end method
