.class public interface abstract Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# virtual methods
.method public abstract A()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract B()Lcom/facebook/graphql/enums/GraphQLCallToActionType;
.end method

.method public abstract C()Z
.end method

.method public abstract D()Landroid/content/Intent;
.end method

.method public abstract E()Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;
.end method

.method public abstract F()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;
.end method

.method public abstract a()LX/GGB;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract a(I)V
.end method

.method public abstract a(LX/0Px;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Landroid/content/Intent;)V
.end method

.method public abstract a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V
.end method

.method public abstract a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;)V
.end method

.method public abstract a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract b()LX/8wL;
.end method

.method public abstract b(I)V
.end method

.method public abstract b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V
.end method

.method public abstract b(Ljava/lang/String;)V
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public abstract c(I)Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract c(Ljava/lang/String;)V
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public abstract d(I)V
.end method

.method public abstract e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;
.end method

.method public abstract f()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;
.end method

.method public abstract g()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;
.end method

.method public abstract h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;
.end method

.method public abstract i()I
.end method

.method public abstract j()I
.end method

.method public abstract k()I
.end method

.method public abstract l()Ljava/lang/String;
.end method

.method public abstract m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;
.end method

.method public abstract n()Ljava/lang/String;
.end method

.method public abstract o()Ljava/lang/String;
.end method

.method public abstract p()LX/175;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract q()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract r()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel$LifetimeOverallStatsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract s()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract t()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract u()Z
.end method

.method public abstract v()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;",
            ">;"
        }
    .end annotation
.end method

.method public abstract w()I
.end method

.method public abstract x()LX/4Cf;
.end method

.method public abstract y()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;
.end method

.method public abstract z()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
