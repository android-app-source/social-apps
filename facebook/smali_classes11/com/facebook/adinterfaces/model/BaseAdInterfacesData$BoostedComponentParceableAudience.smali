.class public final Lcom/facebook/adinterfaces/model/BaseAdInterfacesData$BoostedComponentParceableAudience;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adinterfaces/model/BaseAdInterfacesData$BoostedComponentParceableAudience;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2333738
    new-instance v0, LX/GGH;

    invoke-direct {v0}, LX/GGH;-><init>()V

    sput-object v0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData$BoostedComponentParceableAudience;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2333732
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2333733
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData$BoostedComponentParceableAudience;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    .line 2333734
    return-void
.end method

.method public constructor <init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;)V
    .locals 0

    .prologue
    .line 2333739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2333740
    iput-object p1, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData$BoostedComponentParceableAudience;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    .line 2333741
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2333737
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2333735
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData$BoostedComponentParceableAudience;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2333736
    return-void
.end method
