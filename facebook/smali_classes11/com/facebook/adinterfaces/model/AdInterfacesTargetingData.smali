.class public Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public d:I

.field public e:I

.field public f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

.field public g:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

.field public h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GGF;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/GGG;

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2333725
    new-instance v0, LX/GGC;

    invoke-direct {v0}, LX/GGC;-><init>()V

    sput-object v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 2333726
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->AGE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->GENDERS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->INTERESTS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->LOCATIONS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->a:LX/0Px;

    .line 2333727
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->AGE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->GENDERS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->INTERESTS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->LOCATIONS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->b:LX/0Px;

    .line 2333728
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->AGE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->GENDERS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->INTERESTS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->LOCATIONS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->c:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/GGE;)V
    .locals 2

    .prologue
    .line 2333708
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2333709
    sget-object v0, LX/GGF;->HOME:LX/GGF;

    sget-object v1, LX/GGF;->RECENT:LX/GGF;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->n:LX/0Px;

    .line 2333710
    sget-object v0, LX/GGG;->REGION:LX/GGG;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    .line 2333711
    iget-object v0, p1, LX/GGE;->a:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->g:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    .line 2333712
    iget v0, p1, LX/GGE;->b:I

    iput v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->d:I

    .line 2333713
    iget v0, p1, LX/GGE;->c:I

    iput v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->e:I

    .line 2333714
    iget-object v0, p1, LX/GGE;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    .line 2333715
    iget-object v0, p1, LX/GGE;->e:LX/0Px;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    .line 2333716
    iget-object v0, p1, LX/GGE;->f:LX/0Px;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    .line 2333717
    iget-object v0, p1, LX/GGE;->g:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2333718
    iget-object v0, p1, LX/GGE;->h:LX/0Px;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->n:LX/0Px;

    .line 2333719
    iget-object v0, p1, LX/GGE;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    .line 2333720
    iget-object v0, p1, LX/GGE;->j:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2333721
    iget-object v0, p1, LX/GGE;->k:LX/GGG;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    .line 2333722
    iget-object v0, p1, LX/GGE;->l:LX/0Px;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->l:LX/0Px;

    .line 2333723
    iget-object v0, p1, LX/GGE;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->q:Ljava/lang/String;

    .line 2333724
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2333676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2333677
    sget-object v0, LX/GGF;->HOME:LX/GGF;

    sget-object v2, LX/GGF;->RECENT:LX/GGF;

    invoke-static {v0, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->n:LX/0Px;

    .line 2333678
    sget-object v0, LX/GGG;->REGION:LX/GGG;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    .line 2333679
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->g:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    .line 2333680
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->d:I

    .line 2333681
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->e:I

    .line 2333682
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    .line 2333683
    if-nez v0, :cond_1

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->j:LX/0Px;

    .line 2333684
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    .line 2333685
    if-nez v0, :cond_2

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    .line 2333686
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    .line 2333687
    if-nez v0, :cond_3

    move-object v0, v1

    :goto_2
    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    .line 2333688
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    .line 2333689
    if-nez v0, :cond_4

    :goto_3
    iput-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    .line 2333690
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2333691
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    .line 2333692
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2333693
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 2333694
    invoke-static {}, LX/GGF;->values()[LX/GGF;

    move-result-object v1

    .line 2333695
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2333696
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2333697
    aget-object v0, v1, v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 2333698
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->n:LX/0Px;

    .line 2333699
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2333700
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/GGG;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    .line 2333701
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->l:LX/0Px;

    .line 2333702
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->q:Ljava/lang/String;

    .line 2333703
    return-void

    .line 2333704
    :cond_1
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 2333705
    :cond_2
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_1

    .line 2333706
    :cond_3
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_2

    .line 2333707
    :cond_4
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    goto :goto_3
.end method

.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;IILX/0Px;LX/0Px;LX/0Px;LX/0Px;Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;Ljava/lang/String;LX/GGG;LX/0Px;)V
    .locals 2
    .param p10    # LX/GGG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;",
            "II",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;",
            "Ljava/lang/String;",
            "LX/GGG;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2333659
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2333660
    sget-object v0, LX/GGF;->HOME:LX/GGF;

    sget-object v1, LX/GGF;->RECENT:LX/GGF;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->n:LX/0Px;

    .line 2333661
    sget-object v0, LX/GGG;->REGION:LX/GGG;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    .line 2333662
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2333663
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2333664
    iput-object p1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->g:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    .line 2333665
    iput p2, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->d:I

    .line 2333666
    iput p3, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->e:I

    .line 2333667
    iput-object p4, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->j:LX/0Px;

    .line 2333668
    iput-object p5, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    .line 2333669
    iput-object p6, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    .line 2333670
    iput-object p7, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    .line 2333671
    iput-object p8, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2333672
    iput-object p9, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    .line 2333673
    if-nez p10, :cond_0

    sget-object p10, LX/GGG;->REGION:LX/GGG;

    :cond_0
    iput-object p10, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    .line 2333674
    iput-object p11, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->l:LX/0Px;

    .line 2333675
    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2333654
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    .line 2333655
    :cond_0
    :goto_0
    return v0

    .line 2333656
    :cond_1
    if-eqz p0, :cond_2

    if-nez p1, :cond_3

    :cond_2
    move v0, v1

    .line 2333657
    goto :goto_0

    .line 2333658
    :cond_3
    new-instance v2, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->ir_()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->j()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    new-instance v3, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->ir_()D

    move-result-wide v4

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->j()D

    move-result-wide v6

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v2, v3}, Lcom/facebook/android/maps/model/LatLng;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->l()D

    move-result-wide v2

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->l()D

    move-result-wide v4

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v4, 0x3f847ae147ae147bL    # 0.01

    cmpg-double v2, v2, v4

    if-gez v2, :cond_4

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private b(Lorg/json/JSONObject;Z)V
    .locals 5

    .prologue
    .line 2333639
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 2333640
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->g:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    if-nez v1, :cond_0

    .line 2333641
    :goto_0
    return-void

    .line 2333642
    :cond_0
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->g:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    const/4 p0, 0x2

    const/4 v4, 0x1

    .line 2333643
    sget-object v2, LX/GGD;->a:[I

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2333644
    :goto_1
    const-string v1, "genders"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 2333645
    :pswitch_0
    if-eqz p2, :cond_1

    .line 2333646
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->MALE:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->FEMALE:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_1

    .line 2333647
    :cond_1
    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->put(I)Lorg/json/JSONArray;

    move-result-object v2

    invoke-virtual {v2, p0}, Lorg/json/JSONArray;->put(I)Lorg/json/JSONArray;

    goto :goto_1

    .line 2333648
    :pswitch_1
    if-eqz p2, :cond_2

    .line 2333649
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->MALE:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_1

    .line 2333650
    :cond_2
    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->put(I)Lorg/json/JSONArray;

    goto :goto_1

    .line 2333651
    :pswitch_2
    if-eqz p2, :cond_3

    .line 2333652
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->FEMALE:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_1

    .line 2333653
    :cond_3
    invoke-virtual {v0, p0}, Lorg/json/JSONArray;->put(I)Lorg/json/JSONArray;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private c(Lorg/json/JSONObject;)V
    .locals 12

    .prologue
    .line 2333437
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    sget-object v1, LX/GGG;->REGION:LX/GGG;

    if-ne v0, v1, :cond_0

    .line 2333438
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 2333439
    const/4 v0, 0x0

    .line 2333440
    :goto_0
    move-object v0, v0

    .line 2333441
    :goto_1
    if-nez v0, :cond_1

    .line 2333442
    :goto_2
    return-void

    .line 2333443
    :cond_0
    iget-object v2, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    if-nez v2, :cond_7

    .line 2333444
    const/4 v2, 0x0

    .line 2333445
    :goto_3
    move-object v0, v2

    .line 2333446
    goto :goto_1

    .line 2333447
    :cond_1
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->n:LX/0Px;

    if-nez v1, :cond_a

    .line 2333448
    :goto_4
    const-string v1, "geo_locations"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2

    .line 2333449
    :cond_2
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 2333450
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 2333451
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 2333452
    iget-object v3, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    const/4 v3, 0x0

    move v6, v3

    :goto_5
    if-ge v6, v7, :cond_4

    iget-object v3, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    invoke-virtual {v3, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2333453
    invoke-virtual {v5}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->is_()Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->CUSTOM_LOCATION:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    if-eq v3, v4, :cond_3

    .line 2333454
    invoke-virtual {v5}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->is_()Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    move-result-object v4

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 2333455
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 2333456
    const-string v9, "key"

    invoke-virtual {v8, v9, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2333457
    sget-object v9, LX/GGD;->b:[I

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 2333458
    :cond_3
    :goto_6
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_5

    .line 2333459
    :cond_4
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 2333460
    const-string v4, "countries"

    invoke-virtual {v3, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2333461
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_5

    .line 2333462
    const-string v0, "regions"

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2333463
    :cond_5
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_6

    .line 2333464
    const-string v0, "cities"

    invoke-virtual {v3, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_6
    move-object v0, v3

    .line 2333465
    goto/16 :goto_0

    .line 2333466
    :pswitch_0
    invoke-virtual {v0, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_6

    .line 2333467
    :pswitch_1
    invoke-virtual {v1, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_6

    .line 2333468
    :pswitch_2
    invoke-virtual {v2, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_6

    .line 2333469
    :cond_7
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2333470
    iget-object v2, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2333471
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 2333472
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2333473
    :goto_7
    goto/16 :goto_3

    .line 2333474
    :cond_8
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    .line 2333475
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_8
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2333476
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 2333477
    const-string v8, "radius"

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->l()D

    move-result-wide v10

    invoke-virtual {v7, v8, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 2333478
    const-string v8, "latitude"

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->ir_()D

    move-result-wide v10

    invoke-virtual {v7, v8, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 2333479
    const-string v8, "longitude"

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->j()D

    move-result-wide v10

    invoke-virtual {v7, v8, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 2333480
    const-string v8, "distance_unit"

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v8, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2333481
    invoke-virtual {v5, v7}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_8

    .line 2333482
    :cond_9
    const-string v4, "custom_locations"

    invoke-virtual {v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_7

    .line 2333483
    :cond_a
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 2333484
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->n:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_9
    if-ge v2, v4, :cond_b

    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->n:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/GGF;

    .line 2333485
    iget-object v1, v1, LX/GGF;->key:Ljava/lang/String;

    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 2333486
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_9

    .line 2333487
    :cond_b
    const-string v1, "location_types"

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(ZZ)Ljava/lang/String;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2333578
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 2333579
    :try_start_0
    invoke-direct {p0, v0}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->c(Lorg/json/JSONObject;)V

    .line 2333580
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->GROUPER:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-ne v1, v2, :cond_0

    .line 2333581
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2333582
    :goto_0
    return-object v0

    .line 2333583
    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->b(Lorg/json/JSONObject;Z)V

    .line 2333584
    const-string v1, "age_min"

    iget v2, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->d:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 2333585
    iget v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->e:I

    const/16 v2, 0x41

    if-ge v1, v2, :cond_1

    .line 2333586
    const-string v1, "age_max"

    iget v2, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->e:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 2333587
    :cond_1
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    if-nez v1, :cond_3

    .line 2333588
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    if-nez v1, :cond_5
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2333589
    :cond_2
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2333590
    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    if-nez v1, :cond_8
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2333591
    :cond_4
    :goto_2
    goto :goto_1

    .line 2333592
    :catch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2333593
    :cond_5
    :try_start_2
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    if-nez v1, :cond_6

    .line 2333594
    const/4 v1, 0x0

    .line 2333595
    :goto_3
    move-object v1, v1

    .line 2333596
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 2333597
    const-string v2, "interests"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 2333598
    :cond_6
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 2333599
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v3, v1

    :goto_4
    if-ge v3, v4, :cond_7

    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;

    .line 2333600
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 2333601
    const-string p1, "id"

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;->j()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v5, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2333602
    const-string p1, "name"

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2333603
    invoke-virtual {v2, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 2333604
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_4

    :cond_7
    move-object v1, v2

    .line 2333605
    goto :goto_3
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 2333606
    :cond_8
    if-eqz p2, :cond_9

    .line 2333607
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    if-nez v1, :cond_b

    .line 2333608
    const/4 v1, 0x0

    .line 2333609
    :goto_5
    move-object v1, v1

    .line 2333610
    :goto_6
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lez v2, :cond_4

    .line 2333611
    if-eqz p2, :cond_a

    .line 2333612
    const-string v2, "flexible_spec"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2

    .line 2333613
    :cond_9
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    if-nez v1, :cond_10

    .line 2333614
    const/4 v1, 0x0

    .line 2333615
    :goto_7
    move-object v1, v1

    .line 2333616
    goto :goto_6

    .line 2333617
    :cond_a
    const-string v2, "detailed_targetings"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2

    .line 2333618
    :cond_b
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 2333619
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v3, v1

    :goto_8
    if-ge v3, v5, :cond_f

    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;

    .line 2333620
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 2333621
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_d

    .line 2333622
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2333623
    :goto_9
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;->o()Ljava/lang/String;

    move-result-object v6

    const-string p1, "interested_in"

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_c

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;->o()Ljava/lang/String;

    move-result-object v6

    const-string p1, "relationship_statuses"

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 2333624
    :cond_c
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 2333625
    :goto_a
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_8

    .line 2333626
    :cond_d
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/json/JSONArray;

    goto :goto_9

    .line 2333627
    :cond_e
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    const-string p1, "id"

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_a

    .line 2333628
    :cond_f
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 2333629
    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto/16 :goto_5

    .line 2333630
    :cond_10
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 2333631
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v3, v1

    :goto_b
    if-ge v3, v4, :cond_11

    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;

    .line 2333632
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 2333633
    const-string v6, "id"

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;->l()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v5, v6, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2333634
    const-string v6, "name"

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;->m()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v5, v6, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2333635
    const-string v6, "target_type"

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v6, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2333636
    invoke-virtual {v2, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 2333637
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_b

    :cond_11
    move-object v1, v2

    .line 2333638
    goto/16 :goto_7
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;)V
    .locals 1

    .prologue
    .line 2333575
    iput-object p1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2333576
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    .line 2333577
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;)V
    .locals 0

    .prologue
    .line 2333572
    iput-object p2, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2333573
    iput-object p1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    .line 2333574
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2333571
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2333524
    if-ne p0, p1, :cond_1

    move v2, v3

    .line 2333525
    :cond_0
    :goto_0
    return v2

    .line 2333526
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 2333527
    check-cast p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    .line 2333528
    iget v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->d:I

    iget v1, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->d:I

    if-ne v0, v1, :cond_0

    .line 2333529
    iget v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->e:I

    iget v1, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->e:I

    if-ne v0, v1, :cond_0

    .line 2333530
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->g:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    iget-object v1, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->g:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    if-ne v0, v1, :cond_0

    .line 2333531
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    iget-object v1, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    if-ne v0, v1, :cond_0

    .line 2333532
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    sget-object v1, LX/GGG;->REGION:LX/GGG;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    iget-object v1, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2333533
    if-nez v0, :cond_11

    if-nez v1, :cond_11

    move v6, v7

    .line 2333534
    :cond_2
    :goto_1
    move v0, v6

    .line 2333535
    if-eqz v0, :cond_0

    .line 2333536
    :cond_3
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    sget-object v1, LX/GGG;->ADDRESS:LX/GGG;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    iget-object v1, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    invoke-static {v0, v1}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2333537
    :cond_4
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    if-nez v0, :cond_5

    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    if-eqz v0, :cond_a

    .line 2333538
    :cond_5
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    if-nez v0, :cond_6

    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    if-nez v0, :cond_0

    :cond_6
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    if-nez v0, :cond_0

    .line 2333539
    :cond_7
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iget-object v1, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2333540
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    move v1, v2

    .line 2333541
    :goto_2
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 2333542
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2333543
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_8
    move v1, v2

    .line 2333544
    :goto_3
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 2333545
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 2333546
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2333547
    :cond_9
    invoke-virtual {v4}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2333548
    :cond_a
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    if-nez v0, :cond_b

    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    if-eqz v0, :cond_10

    .line 2333549
    :cond_b
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    if-nez v0, :cond_c

    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    if-nez v0, :cond_0

    :cond_c
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    if-nez v0, :cond_0

    .line 2333550
    :cond_d
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iget-object v1, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2333551
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 2333552
    iget-object v5, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v1, v2

    :goto_4
    if-ge v1, v6, :cond_e

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;

    .line 2333553
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2333554
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 2333555
    :cond_e
    iget-object v5, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v1, v2

    :goto_5
    if-ge v1, v6, :cond_f

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;

    .line 2333556
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 2333557
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 2333558
    :cond_f
    invoke-virtual {v4}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_10
    move v2, v3

    .line 2333559
    goto/16 :goto_0

    .line 2333560
    :cond_11
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 2333561
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    if-ne v4, v5, :cond_2

    .line 2333562
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    move v5, v6

    .line 2333563
    :goto_6
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_12

    .line 2333564
    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2333565
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_6

    :cond_12
    move v5, v6

    .line 2333566
    :goto_7
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_13

    .line 2333567
    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 2333568
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_7

    .line 2333569
    :cond_13
    invoke-virtual {v8}, Ljava/util/HashSet;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    move v6, v7

    .line 2333570
    goto/16 :goto_1
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2333510
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->g:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->g:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->hashCode()I

    move-result v0

    .line 2333511
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->d:I

    add-int/2addr v0, v2

    .line 2333512
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->e:I

    add-int/2addr v0, v2

    .line 2333513
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 2333514
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 2333515
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    .line 2333516
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    .line 2333517
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    invoke-virtual {v1}, LX/GGG;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 2333518
    return v0

    :cond_1
    move v0, v1

    .line 2333519
    goto :goto_0

    :cond_2
    move v0, v1

    .line 2333520
    goto :goto_1

    :cond_3
    move v0, v1

    .line 2333521
    goto :goto_2

    :cond_4
    move v0, v1

    .line 2333522
    goto :goto_3

    :cond_5
    move v0, v1

    .line 2333523
    goto :goto_4
.end method

.method public final n()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2333509
    invoke-virtual {p0, v0, v0}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->a(ZZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 2333488
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->g:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2333489
    iget v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2333490
    iget v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2333491
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->j:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 2333492
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 2333493
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 2333494
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 2333495
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2333496
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2333497
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->n:LX/0Px;

    if-nez v0, :cond_0

    .line 2333498
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 2333499
    :goto_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2333500
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2333501
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->l:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2333502
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2333503
    return-void

    .line 2333504
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2333505
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->n:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, p2, :cond_1

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->n:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GGF;

    .line 2333506
    invoke-virtual {v0}, LX/GGF;->ordinal()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2333507
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2333508
    :cond_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    goto :goto_0
.end method
