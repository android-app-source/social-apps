.class public Lcom/facebook/adinterfaces/model/CreativeAdModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adinterfaces/model/CreativeAdModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field public p:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2333948
    new-instance v0, LX/GGJ;

    invoke-direct {v0}, LX/GGJ;-><init>()V

    sput-object v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2333949
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2333950
    return-void
.end method

.method public constructor <init>(LX/GGK;)V
    .locals 1

    .prologue
    .line 2333951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2333952
    iget-object v0, p1, LX/GGK;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->a:Ljava/lang/String;

    .line 2333953
    iget-object v0, p1, LX/GGK;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->b:Ljava/lang/String;

    .line 2333954
    iget-object v0, p1, LX/GGK;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->c:Ljava/lang/String;

    .line 2333955
    iget-object v0, p1, LX/GGK;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->d:Ljava/lang/String;

    .line 2333956
    iget-object v0, p1, LX/GGK;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->e:Ljava/lang/String;

    .line 2333957
    iget-object v0, p1, LX/GGK;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->f:Ljava/lang/String;

    .line 2333958
    iget-object v0, p1, LX/GGK;->i:Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->g:Ljava/util/List;

    .line 2333959
    iget-object v0, p1, LX/GGK;->j:Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->h:Ljava/util/List;

    .line 2333960
    iget-object v0, p1, LX/GGK;->a:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2333961
    iget-object v0, p1, LX/GGK;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->l:Ljava/lang/String;

    .line 2333962
    iget-object v0, p1, LX/GGK;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->n:Ljava/lang/String;

    .line 2333963
    iget-object v0, p1, LX/GGK;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->o:Ljava/lang/String;

    .line 2333964
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2333965
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2333966
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->a:Ljava/lang/String;

    .line 2333967
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->b:Ljava/lang/String;

    .line 2333968
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->c:Ljava/lang/String;

    .line 2333969
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->d:Ljava/lang/String;

    .line 2333970
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->e:Ljava/lang/String;

    .line 2333971
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->f:Ljava/lang/String;

    .line 2333972
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->g:Ljava/util/List;

    .line 2333973
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->h:Ljava/util/List;

    .line 2333974
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2333975
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->l:Ljava/lang/String;

    .line 2333976
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->m:Ljava/lang/String;

    .line 2333977
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->n:Ljava/lang/String;

    .line 2333978
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->o:Ljava/lang/String;

    .line 2333979
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->p:Z

    .line 2333980
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->i:Ljava/lang/String;

    .line 2333981
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->j:Ljava/lang/String;

    .line 2333982
    return-void

    .line 2333983
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2333984
    const/4 v0, 0x0

    return v0
.end method

.method public final n()LX/4Cf;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 2333985
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->o:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2333986
    new-instance v0, LX/4Cf;

    invoke-direct {v0}, LX/4Cf;-><init>()V

    iget-object v1, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->o:Ljava/lang/String;

    .line 2333987
    const-string v2, "story_graphql_token"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2333988
    move-object v0, v0

    .line 2333989
    new-instance v1, LX/4Cc;

    invoke-direct {v1}, LX/4Cc;-><init>()V

    .line 2333990
    iget-object v2, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eqz v2, :cond_0

    .line 2333991
    iget-object v2, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4Cc;->a(Ljava/lang/String;)LX/4Cc;

    move-result-object v2

    .line 2333992
    const-string v3, "call_to_action"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2333993
    iget-object v2, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->l:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 2333994
    new-instance v2, LX/4Cd;

    invoke-direct {v2}, LX/4Cd;-><init>()V

    .line 2333995
    iget-object v3, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->l:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/4Cd;->a(Ljava/lang/String;)LX/4Cd;

    .line 2333996
    invoke-virtual {v1, v2}, LX/4Cc;->a(LX/4Cd;)LX/4Cc;

    .line 2333997
    :cond_0
    :goto_0
    return-object v0

    .line 2333998
    :cond_1
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2333999
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->a:Ljava/lang/String;

    if-eqz v0, :cond_10

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2334000
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->c:Ljava/lang/String;

    if-eqz v0, :cond_11

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2334001
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->LIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eq v0, v4, :cond_4

    .line 2334002
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->d:Ljava/lang/String;

    if-eqz v0, :cond_12

    move v0, v1

    :goto_3
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2334003
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    :cond_2
    move v2, v1

    :cond_3
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 2334004
    :cond_4
    const/4 v0, 0x0

    .line 2334005
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->LIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v1, v2, :cond_6

    .line 2334006
    new-instance v0, LX/4Cf;

    invoke-direct {v0}, LX/4Cf;-><init>()V

    iget-object v1, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4Cf;->a(Ljava/lang/String;)LX/4Cf;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4Cf;->c(Ljava/lang/String;)LX/4Cf;

    move-result-object v0

    .line 2334007
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->e:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 2334008
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4Cf;->d(Ljava/lang/String;)LX/4Cf;

    goto :goto_0

    .line 2334009
    :cond_5
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->e:Ljava/lang/String;

    .line 2334010
    const-string v2, "image_hash"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2334011
    goto :goto_0

    .line 2334012
    :cond_6
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eq v1, v2, :cond_f

    iget-object v1, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eqz v1, :cond_f

    .line 2334013
    new-instance v0, LX/4Cc;

    invoke-direct {v0}, LX/4Cc;-><init>()V

    iget-object v1, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4Cc;->a(Ljava/lang/String;)LX/4Cc;

    move-result-object v0

    new-instance v1, LX/4Cd;

    invoke-direct {v1}, LX/4Cd;-><init>()V

    iget-object v2, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/4Cd;->a(Ljava/lang/String;)LX/4Cd;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4Cc;->a(LX/4Cd;)LX/4Cc;

    move-result-object v0

    move-object v1, v0

    .line 2334014
    :goto_4
    new-instance v4, LX/4Cl;

    invoke-direct {v4}, LX/4Cl;-><init>()V

    .line 2334015
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->g:Ljava/util/List;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v9, :cond_a

    .line 2334016
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->b:Ljava/lang/String;

    invoke-virtual {v4, v0}, LX/4Cl;->e(Ljava/lang/String;)LX/4Cl;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/4Cl;->a(Ljava/lang/String;)LX/4Cl;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, LX/4Cl;->g(Ljava/lang/String;)LX/4Cl;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/4Cl;->d(Ljava/lang/String;)LX/4Cl;

    .line 2334017
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v2, v3

    .line 2334018
    :goto_5
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_8

    .line 2334019
    new-instance v6, LX/4Ce;

    invoke-direct {v6}, LX/4Ce;-><init>()V

    .line 2334020
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->d:Ljava/lang/String;

    .line 2334021
    const-string v7, "link"

    invoke-virtual {v6, v7, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2334022
    move-object v7, v6

    .line 2334023
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->h:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2334024
    const-string v8, "name"

    invoke-virtual {v7, v8, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2334025
    move-object v7, v7

    .line 2334026
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->g:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2334027
    const-string v8, "picture"

    invoke-virtual {v7, v8, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2334028
    if-eqz v1, :cond_7

    .line 2334029
    new-instance v0, LX/4Cc;

    invoke-direct {v0}, LX/4Cc;-><init>()V

    iget-object v7, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/4Cc;->a(Ljava/lang/String;)LX/4Cc;

    move-result-object v0

    new-instance v7, LX/4Cd;

    invoke-direct {v7}, LX/4Cd;-><init>()V

    iget-object v8, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->l:Ljava/lang/String;

    invoke-virtual {v7, v8}, LX/4Cd;->a(Ljava/lang/String;)LX/4Cd;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/4Cc;->a(LX/4Cd;)LX/4Cc;

    move-result-object v0

    .line 2334030
    const-string v7, "call_to_action"

    invoke-virtual {v6, v7, v0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2334031
    :cond_7
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2334032
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 2334033
    :cond_8
    const-string v0, "child_attachments"

    invoke-virtual {v4, v0, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2334034
    :goto_6
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->e:Ljava/lang/String;

    if-nez v0, :cond_d

    .line 2334035
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->g:Ljava/util/List;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v9, :cond_c

    .line 2334036
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->g:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, LX/4Cl;->c(Ljava/lang/String;)LX/4Cl;

    .line 2334037
    :goto_7
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->j:Ljava/lang/String;

    move-object v0, v0

    .line 2334038
    if-eqz v0, :cond_e

    .line 2334039
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->j:Ljava/lang/String;

    move-object v0, v0

    .line 2334040
    const-string v1, "offer_id"

    invoke-virtual {v4, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2334041
    :cond_9
    :goto_8
    new-instance v0, LX/4Cm;

    invoke-direct {v0}, LX/4Cm;-><init>()V

    iget-object v1, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->a:Ljava/lang/String;

    .line 2334042
    const-string v2, "page_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2334043
    move-object v0, v0

    .line 2334044
    const-string v1, "link_data"

    invoke-virtual {v0, v1, v4}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2334045
    move-object v0, v0

    .line 2334046
    new-instance v1, LX/4Cf;

    invoke-direct {v1}, LX/4Cf;-><init>()V

    .line 2334047
    const-string v2, "object_story_spec"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2334048
    move-object v0, v1

    .line 2334049
    goto/16 :goto_0

    .line 2334050
    :cond_a
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->g:Ljava/util/List;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v9, :cond_b

    .line 2334051
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->h:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, LX/4Cl;->e(Ljava/lang/String;)LX/4Cl;

    .line 2334052
    :goto_9
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->c:Ljava/lang/String;

    invoke-virtual {v4, v0}, LX/4Cl;->a(Ljava/lang/String;)LX/4Cl;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, LX/4Cl;->g(Ljava/lang/String;)LX/4Cl;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/4Cl;->d(Ljava/lang/String;)LX/4Cl;

    goto :goto_6

    .line 2334053
    :cond_b
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->b:Ljava/lang/String;

    invoke-virtual {v4, v0}, LX/4Cl;->e(Ljava/lang/String;)LX/4Cl;

    goto :goto_9

    .line 2334054
    :cond_c
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->f:Ljava/lang/String;

    invoke-virtual {v4, v0}, LX/4Cl;->c(Ljava/lang/String;)LX/4Cl;

    goto :goto_7

    .line 2334055
    :cond_d
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->e:Ljava/lang/String;

    .line 2334056
    const-string v2, "image_hash"

    invoke-virtual {v4, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2334057
    goto :goto_7

    .line 2334058
    :cond_e
    if-eqz v1, :cond_9

    .line 2334059
    const-string v0, "call_to_action"

    invoke-virtual {v4, v0, v1}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2334060
    goto :goto_8

    :cond_f
    move-object v1, v0

    goto/16 :goto_4

    :cond_10
    move v0, v2

    .line 2334061
    goto/16 :goto_1

    :cond_11
    move v0, v2

    .line 2334062
    goto/16 :goto_2

    :cond_12
    move v0, v2

    .line 2334063
    goto/16 :goto_3
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2334064
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334065
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334066
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334067
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334068
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334069
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334070
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->g:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2334071
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->h:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2334072
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2334073
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334074
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334075
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334076
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334077
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->p:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2334078
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334079
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334080
    return-void

    .line 2334081
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
