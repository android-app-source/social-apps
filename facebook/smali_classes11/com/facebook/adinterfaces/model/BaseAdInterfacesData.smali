.class public abstract Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;


# instance fields
.field public a:Ljava/lang/String;

.field private b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

.field public c:LX/8wL;

.field public d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field public f:LX/GGB;

.field private g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

.field private h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

.field private i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

.field private j:I

.field public k:I

.field private l:I

.field private m:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

.field private n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field private p:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

.field public q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;",
            ">;"
        }
    .end annotation
.end field

.field private r:I

.field public s:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field private t:Landroid/content/Intent;

.field public u:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

.field public v:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;

.field public w:I

.field public x:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 2333833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2333834
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d:Ljava/lang/String;

    .line 2333835
    iput-object v3, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->e:Ljava/lang/String;

    .line 2333836
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a:Ljava/lang/String;

    .line 2333837
    iput v1, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j:I

    .line 2333838
    iput v2, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->k:I

    .line 2333839
    const/16 v0, 0x16c

    iput v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l:I

    .line 2333840
    iput v1, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->r:I

    .line 2333841
    iput v2, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->w:I

    .line 2333842
    iput-object v3, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->x:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    .line 2333843
    return-void
.end method

.method public constructor <init>(LX/GGI;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 2333844
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2333845
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d:Ljava/lang/String;

    .line 2333846
    iput-object v3, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->e:Ljava/lang/String;

    .line 2333847
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a:Ljava/lang/String;

    .line 2333848
    iput v1, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j:I

    .line 2333849
    iput v2, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->k:I

    .line 2333850
    const/16 v0, 0x16c

    iput v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l:I

    .line 2333851
    iput v1, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->r:I

    .line 2333852
    iput v2, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->w:I

    .line 2333853
    iput-object v3, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->x:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    .line 2333854
    iget-object v0, p1, LX/GGI;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    .line 2333855
    iget-object v0, p1, LX/GGI;->b:LX/8wL;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c:LX/8wL;

    .line 2333856
    iget-object v0, p1, LX/GGI;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d:Ljava/lang/String;

    .line 2333857
    iget-object v0, p1, LX/GGI;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->e:Ljava/lang/String;

    .line 2333858
    iget-object v0, p1, LX/GGI;->e:LX/GGB;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->f:LX/GGB;

    .line 2333859
    iget-object v0, p1, LX/GGI;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a:Ljava/lang/String;

    .line 2333860
    iget-object v0, p1, LX/GGI;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    .line 2333861
    iget-object v0, p1, LX/GGI;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    .line 2333862
    iget-object v0, p1, LX/GGI;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    .line 2333863
    iget v0, p1, LX/GGI;->j:I

    iput v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j:I

    .line 2333864
    iget-object v0, p1, LX/GGI;->k:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    .line 2333865
    iget-object v0, p1, LX/GGI;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->n:Ljava/lang/String;

    .line 2333866
    iget-object v0, p1, LX/GGI;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->o:Ljava/lang/String;

    .line 2333867
    iget v0, p1, LX/GGI;->o:I

    iput v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->r:I

    .line 2333868
    iget-object v0, p1, LX/GGI;->n:LX/0Px;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->q:LX/0Px;

    .line 2333869
    iget-object v0, p1, LX/GGI;->p:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->s:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2333870
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 2333871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2333872
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d:Ljava/lang/String;

    .line 2333873
    iput-object v1, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->e:Ljava/lang/String;

    .line 2333874
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a:Ljava/lang/String;

    .line 2333875
    iput v2, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j:I

    .line 2333876
    iput v3, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->k:I

    .line 2333877
    const/16 v0, 0x16c

    iput v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l:I

    .line 2333878
    iput v2, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->r:I

    .line 2333879
    iput v3, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->w:I

    .line 2333880
    iput-object v1, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->x:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    .line 2333881
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    .line 2333882
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/8wL;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c:LX/8wL;

    .line 2333883
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d:Ljava/lang/String;

    .line 2333884
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->e:Ljava/lang/String;

    .line 2333885
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/GGB;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->f:LX/GGB;

    .line 2333886
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a:Ljava/lang/String;

    .line 2333887
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    .line 2333888
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    .line 2333889
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    .line 2333890
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j:I

    .line 2333891
    const-class v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    .line 2333892
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->n:Ljava/lang/String;

    .line 2333893
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->o:Ljava/lang/String;

    .line 2333894
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->r:I

    .line 2333895
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2333896
    sget-object v2, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData$BoostedComponentParceableAudience;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 2333897
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2333898
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData$BoostedComponentParceableAudience;

    .line 2333899
    iget-object v0, v2, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData$BoostedComponentParceableAudience;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-object v2, v0

    .line 2333900
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2333901
    :cond_0
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    move-object v0, v2

    .line 2333902
    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->q:LX/0Px;

    .line 2333903
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->s:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2333904
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->k:I

    .line 2333905
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l:I

    .line 2333906
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 2333907
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->u:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    .line 2333908
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->v:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;

    .line 2333909
    return-void

    :cond_1
    move-object v0, v1

    .line 2333910
    goto :goto_1
.end method


# virtual methods
.method public final B()Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 1

    .prologue
    .line 2333911
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->s:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    return-object v0
.end method

.method public final D()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 2333912
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->t:Landroid/content/Intent;

    return-object v0
.end method

.method public final E()Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;
    .locals 1

    .prologue
    .line 2333913
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->u:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    return-object v0
.end method

.method public final F()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;
    .locals 1

    .prologue
    .line 2333914
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->v:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;

    return-object v0
.end method

.method public I()Lcom/facebook/graphql/model/FeedUnit;
    .locals 1

    .prologue
    .line 2333915
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()LX/GGB;
    .locals 1

    .prologue
    .line 2333916
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->f:LX/GGB;

    return-object v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 2333917
    iput p1, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j:I

    .line 2333918
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2333830
    iput-object p1, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->q:LX/0Px;

    .line 2333831
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 2333919
    iput-object p1, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->t:Landroid/content/Intent;

    .line 2333920
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V
    .locals 0

    .prologue
    .line 2333921
    iput-object p1, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    .line 2333922
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;)V
    .locals 0

    .prologue
    .line 2333923
    iput-object p1, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    .line 2333924
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V
    .locals 0

    .prologue
    .line 2333925
    iput-object p1, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    .line 2333926
    iput-object p2, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    .line 2333927
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2333928
    iput-object p1, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->e:Ljava/lang/String;

    .line 2333929
    return-void
.end method

.method public final b()LX/8wL;
    .locals 1

    .prologue
    .line 2333930
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c:LX/8wL;

    return-object v0
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 2333931
    iput p1, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l:I

    .line 2333932
    return-void
.end method

.method public final b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V
    .locals 0

    .prologue
    .line 2333933
    iput-object p1, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    .line 2333934
    iput-object p2, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    .line 2333935
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2333936
    iput-object p1, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a:Ljava/lang/String;

    .line 2333937
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2333832
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final c(I)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2333938
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->s()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel;

    move-result-object v0

    .line 2333939
    if-nez v0, :cond_0

    .line 2333940
    const/4 v0, 0x0

    .line 2333941
    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel;->m()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2333773
    iput-object p1, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->n:Ljava/lang/String;

    .line 2333774
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2333775
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d(I)V
    .locals 0

    .prologue
    .line 2333776
    iput p1, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->r:I

    .line 2333777
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 2333778
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;
    .locals 1

    .prologue
    .line 2333779
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    return-object v0
.end method

.method public final f()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;
    .locals 1

    .prologue
    .line 2333780
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    return-object v0
.end method

.method public final g()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;
    .locals 1

    .prologue
    .line 2333781
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    return-object v0
.end method

.method public final h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;
    .locals 1

    .prologue
    .line 2333782
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    return-object v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 2333783
    iget v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j:I

    return v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 2333784
    iget v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->k:I

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2333785
    iget v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l:I

    return v0
.end method

.method public l()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2333786
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2333787
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a:Ljava/lang/String;

    .line 2333788
    :cond_1
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;
    .locals 1

    .prologue
    .line 2333772
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2333789
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2333790
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;
    .locals 1

    .prologue
    .line 2333791
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->p:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    return-object v0
.end method

.method public final r()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel$LifetimeOverallStatsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2333792
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->s()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel;

    move-result-object v1

    .line 2333793
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel;->k()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2333794
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel;->k()LX/0Px;

    move-result-object v0

    :goto_1
    return-object v0

    .line 2333795
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2333796
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2333797
    goto :goto_1
.end method

.method public final v()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2333798
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->q:LX/0Px;

    return-object v0
.end method

.method public final w()I
    .locals 1

    .prologue
    .line 2333799
    iget v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->r:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    .line 2333800
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2333801
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c:LX/8wL;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2333802
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2333803
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2333804
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->f:LX/GGB;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2333805
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2333806
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2333807
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2333808
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2333809
    iget v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2333810
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2333811
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2333812
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2333813
    iget v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->r:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2333814
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2333815
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->q:LX/0Px;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 2333816
    :goto_0
    move-object v0, v0

    .line 2333817
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2333818
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->s:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2333819
    iget v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->k:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2333820
    iget v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2333821
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->u:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->u:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->name()Ljava/lang/String;

    move-result-object v0

    .line 2333822
    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2333823
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->v:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2333824
    return-void

    .line 2333825
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 2333826
    :cond_1
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->q:LX/0Px;

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    .line 2333827
    new-instance p2, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData$BoostedComponentParceableAudience;

    invoke-direct {p2, v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData$BoostedComponentParceableAudience;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;)V

    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2333828
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    move-object v0, v1

    .line 2333829
    goto :goto_0
.end method
