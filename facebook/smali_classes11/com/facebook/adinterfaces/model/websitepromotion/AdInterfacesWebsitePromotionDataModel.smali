.class public Lcom/facebook/adinterfaces/model/websitepromotion/AdInterfacesWebsitePromotionDataModel;
.super Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adinterfaces/model/websitepromotion/AdInterfacesWebsitePromotionDataModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2334305
    new-instance v0, LX/GGR;

    invoke-direct {v0}, LX/GGR;-><init>()V

    sput-object v0, Lcom/facebook/adinterfaces/model/websitepromotion/AdInterfacesWebsitePromotionDataModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/GGS;)V
    .locals 0

    .prologue
    .line 2334303
    invoke-direct {p0, p1}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;-><init>(LX/GGN;)V

    .line 2334304
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2334300
    invoke-direct {p0, p1}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;-><init>(Landroid/os/Parcel;)V

    .line 2334301
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/websitepromotion/AdInterfacesWebsitePromotionDataModel;->b:Ljava/lang/String;

    .line 2334302
    return-void
.end method


# virtual methods
.method public final Q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2334299
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/websitepromotion/AdInterfacesWebsitePromotionDataModel;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final R()Z
    .locals 1

    .prologue
    .line 2334306
    const/4 v0, 0x1

    return v0
.end method

.method public final S()I
    .locals 1

    .prologue
    .line 2334298
    const v0, 0x7f080b0d

    return v0
.end method

.method public final T()I
    .locals 1

    .prologue
    .line 2334297
    const v0, 0x7f080b0e

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2334296
    const/4 v0, 0x0

    return v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2334294
    iput-object p1, p0, Lcom/facebook/adinterfaces/model/websitepromotion/AdInterfacesWebsitePromotionDataModel;->b:Ljava/lang/String;

    .line 2334295
    return-void
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2334291
    invoke-super {p0, p1, p2}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2334292
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/websitepromotion/AdInterfacesWebsitePromotionDataModel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334293
    return-void
.end method
