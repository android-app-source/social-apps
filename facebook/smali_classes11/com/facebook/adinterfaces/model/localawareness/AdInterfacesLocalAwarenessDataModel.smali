.class public Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;
.super Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Z

.field private c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2334281
    new-instance v0, LX/GGP;

    invoke-direct {v0}, LX/GGP;-><init>()V

    sput-object v0, Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/GGQ;)V
    .locals 0

    .prologue
    .line 2334279
    invoke-direct {p0, p1}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;-><init>(LX/GGN;)V

    .line 2334280
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2334274
    invoke-direct {p0, p1}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;-><init>(Landroid/os/Parcel;)V

    .line 2334275
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;->b:Z

    .line 2334276
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;->c:Ljava/lang/String;

    .line 2334277
    return-void

    .line 2334278
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final Q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2334273
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final R()Z
    .locals 2

    .prologue
    .line 2334268
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v0, v0

    .line 2334269
    if-eqz v0, :cond_0

    .line 2334270
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v0, v0

    .line 2334271
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-object v0, v1

    .line 2334272
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->LEARN_MORE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final S()I
    .locals 1

    .prologue
    .line 2334282
    const v0, 0x7f080a82

    return v0
.end method

.method public final T()I
    .locals 1

    .prologue
    .line 2334267
    const v0, 0x7f080b0e

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2334266
    const/4 v0, 0x0

    return v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2334264
    iput-object p1, p0, Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;->c:Ljava/lang/String;

    .line 2334265
    return-void
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2334259
    invoke-super {p0, p1, p2}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2334260
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2334261
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334262
    return-void

    .line 2334263
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
