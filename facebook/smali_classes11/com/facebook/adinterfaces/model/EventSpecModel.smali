.class public Lcom/facebook/adinterfaces/model/EventSpecModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adinterfaces/model/EventSpecModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/EventBoostType;
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:J

.field public f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2334085
    new-instance v0, LX/GGL;

    invoke-direct {v0}, LX/GGL;-><init>()V

    sput-object v0, Lcom/facebook/adinterfaces/model/EventSpecModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2334092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2334093
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/EventSpecModel;->a:Ljava/lang/String;

    .line 2334094
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/EventSpecModel;->b:Ljava/lang/String;

    .line 2334095
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/adinterfaces/model/EventSpecModel;->e:J

    .line 2334096
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/model/EventSpecModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2334097
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/EventBoostType;
        .end annotation
    .end param

    .prologue
    .line 2334098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2334099
    iput-object p1, p0, Lcom/facebook/adinterfaces/model/EventSpecModel;->a:Ljava/lang/String;

    .line 2334100
    iput-object p2, p0, Lcom/facebook/adinterfaces/model/EventSpecModel;->b:Ljava/lang/String;

    .line 2334101
    iput-object p3, p0, Lcom/facebook/adinterfaces/model/EventSpecModel;->c:Ljava/lang/String;

    .line 2334102
    iput-object p4, p0, Lcom/facebook/adinterfaces/model/EventSpecModel;->d:Ljava/lang/String;

    .line 2334103
    iput-wide p5, p0, Lcom/facebook/adinterfaces/model/EventSpecModel;->e:J

    .line 2334104
    iput-object p7, p0, Lcom/facebook/adinterfaces/model/EventSpecModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2334105
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2334091
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2334086
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/EventSpecModel;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334087
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/EventSpecModel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2334088
    iget-wide v0, p0, Lcom/facebook/adinterfaces/model/EventSpecModel;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2334089
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/EventSpecModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2334090
    return-void
.end method
