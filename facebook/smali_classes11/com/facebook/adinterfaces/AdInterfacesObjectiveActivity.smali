.class public Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/13o;


# static fields
.field private static final I:Ljava/lang/String;


# instance fields
.field public A:LX/GCE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/BXo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/8wR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/GHc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/GDb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/16I;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private J:LX/GGc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/GGc",
            "<*>;"
        }
    .end annotation
.end field

.field private K:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/GEW;",
            ">;"
        }
    .end annotation
.end field

.field public L:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/8wK;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private M:I

.field public N:LX/8wK;

.field private O:Landroid/os/Bundle;

.field private P:Z

.field private Q:I

.field private R:I

.field private S:Landroid/content/Intent;

.field public T:Z

.field private U:Z

.field public V:Ljava/lang/String;

.field private W:Landroid/view/View;

.field private X:Landroid/content/Intent;

.field private Y:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/8wK;",
            ">;"
        }
    .end annotation
.end field

.field public Z:LX/4At;

.field public aa:Lcom/facebook/widget/error/GenericErrorView;

.field private ab:LX/67X;

.field private ac:LX/0kb;

.field private ad:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

.field private ae:LX/1CX;

.field public af:LX/0h5;

.field private ag:LX/0Yb;

.field private ah:LX/0Yb;

.field public p:LX/GGd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/GF4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/1B1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/01T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/67X;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/2U3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1CX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2328675
    const-class v0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->I:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2328676
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2328677
    iput-boolean v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->P:Z

    .line 2328678
    iput-boolean v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->T:Z

    .line 2328679
    iput-boolean v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->U:Z

    .line 2328680
    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->V:Ljava/lang/String;

    .line 2328681
    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->W:Landroid/view/View;

    .line 2328682
    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->X:Landroid/content/Intent;

    .line 2328683
    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->Y:Ljava/util/Map;

    return-void
.end method

.method private A()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 2328636
    new-instance v0, LX/GCd;

    invoke-direct {v0, p0}, LX/GCd;-><init>(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)V

    .line 2328637
    new-instance v1, LX/GCL;

    invoke-direct {v1, p0}, LX/GCL;-><init>(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)V

    .line 2328638
    new-instance v2, LX/GCN;

    invoke-direct {v2, p0}, LX/GCN;-><init>(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)V

    .line 2328639
    new-instance v3, LX/GCP;

    invoke-direct {v3, p0}, LX/GCP;-><init>(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)V

    .line 2328640
    new-instance v4, LX/GCR;

    invoke-direct {v4, p0}, LX/GCR;-><init>(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)V

    .line 2328641
    iget-object v5, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    .line 2328642
    iget-object v6, v5, LX/GCE;->e:LX/0ad;

    move-object v5, v6

    .line 2328643
    sget-object v6, LX/0c0;->Live:LX/0c0;

    sget-object v7, LX/0c1;->Off:LX/0c1;

    sget-short v8, LX/GDK;->c:S

    invoke-interface {v5, v6, v7, v8, v9}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2328644
    new-instance v5, LX/GCT;

    invoke-direct {v5, p0}, LX/GCT;-><init>(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)V

    .line 2328645
    iget-object v6, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->s:LX/1B1;

    invoke-virtual {v6, v5}, LX/1B1;->a(LX/0b2;)Z

    .line 2328646
    :cond_0
    iget-object v5, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->s:LX/1B1;

    const/4 v6, 0x5

    new-array v6, v6, [LX/0b2;

    aput-object v1, v6, v9

    const/4 v1, 0x1

    aput-object v2, v6, v1

    const/4 v1, 0x2

    aput-object v3, v6, v1

    const/4 v1, 0x3

    aput-object v4, v6, v1

    const/4 v1, 0x4

    aput-object v0, v6, v1

    invoke-virtual {v5, v6}, LX/1B1;->a([LX/0b2;)V

    .line 2328647
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->s:LX/1B1;

    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->q:LX/GF4;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 2328648
    return-void
.end method

.method private B()V
    .locals 2

    .prologue
    .line 2328684
    iget v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->M:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 2328685
    :goto_0
    return-void

    .line 2328686
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->t:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_2

    .line 2328687
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ab:LX/67X;

    invoke-virtual {v0}, LX/67X;->g()LX/3u1;

    move-result-object v0

    .line 2328688
    new-instance v1, LX/63L;

    invoke-direct {v1, p0, v0}, LX/63L;-><init>(LX/0ew;LX/3u1;)V

    iput-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->af:LX/0h5;

    .line 2328689
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->af:LX/0h5;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 2328690
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->af:LX/0h5;

    new-instance v1, LX/GCU;

    invoke-direct {v1, p0}, LX/GCU;-><init>(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2328691
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->af:LX/0h5;

    iget v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->M:I

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    goto :goto_0

    .line 2328692
    :cond_2
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->t:LX/01T;

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_1

    .line 2328693
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2328694
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->af:LX/0h5;

    goto :goto_1
.end method

.method public static C(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)V
    .locals 3

    .prologue
    .line 2328695
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 2328696
    if-eqz v0, :cond_0

    .line 2328697
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->u:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2328698
    :cond_0
    return-void
.end method

.method private a(LX/GEW;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Landroid/view/LayoutInflater;Landroid/widget/LinearLayout;)Landroid/view/View;
    .locals 5

    .prologue
    .line 2328699
    :try_start_0
    invoke-interface {p1}, LX/GEW;->b()LX/GHg;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2328700
    invoke-interface {p1}, LX/GEW;->b()LX/GHg;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    invoke-virtual {v0, v1}, LX/GHg;->a(LX/GCE;)V

    .line 2328701
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->b(LX/GEW;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Landroid/view/LayoutInflater;Landroid/widget/LinearLayout;)Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2328702
    :goto_0
    return-object v0

    .line 2328703
    :catch_0
    move-exception v0

    .line 2328704
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->w:LX/2U3;

    const-class v2, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "bindComponent for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " failed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2328705
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2328706
    invoke-virtual {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2328707
    :try_start_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->E:LX/0Or;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->E:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2328708
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->H:LX/03V;

    sget-object v1, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->I:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Empty couponClaimMethod in LWI, couponPromotionGroupID: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", adAccountID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", placement: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2328709
    invoke-static {p0}, LX/GDb;->a(Landroid/content/Context;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->a$redex0(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;Landroid/os/Bundle;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;)V

    .line 2328710
    :goto_0
    return-void

    .line 2328711
    :cond_1
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->E:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GDb;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    new-instance v5, LX/GCa;

    invoke-direct {v5, p0, p1}, LX/GCa;-><init>(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;Landroid/os/Bundle;)V

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-virtual/range {v0 .. v5}, LX/GDb;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;LX/GCa;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2328712
    :catch_0
    move-exception v0

    .line 2328713
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->H:LX/03V;

    sget-object v2, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->I:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception claiming coupon in LWI, couponPromotionGroupID: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", adAccountID: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", placement: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2328714
    invoke-static {p0}, LX/GDb;->a(Landroid/content/Context;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->a$redex0(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;Landroid/os/Bundle;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;LX/GGd;LX/GF4;Lcom/facebook/content/SecureContextHelper;LX/1B1;LX/01T;Landroid/view/inputmethod/InputMethodManager;LX/0Or;LX/2U3;LX/0Or;LX/0Or;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/GCE;LX/BXo;LX/8wR;LX/GHc;LX/0Or;LX/16I;LX/0Uh;LX/03V;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;",
            "LX/GGd;",
            "LX/GF4;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/1B1;",
            "LX/01T;",
            "Landroid/view/inputmethod/InputMethodManager;",
            "LX/0Or",
            "<",
            "LX/67X;",
            ">;",
            "LX/2U3;",
            "LX/0Or",
            "<",
            "LX/0kb;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1CX;",
            ">;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/GCE;",
            "LX/BXo;",
            "LX/8wR;",
            "LX/GHc;",
            "LX/0Or",
            "<",
            "LX/GDb;",
            ">;",
            "LX/16I;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2328715
    iput-object p1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->p:LX/GGd;

    iput-object p2, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->q:LX/GF4;

    iput-object p3, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->r:Lcom/facebook/content/SecureContextHelper;

    iput-object p4, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->s:LX/1B1;

    iput-object p5, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->t:LX/01T;

    iput-object p6, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->u:Landroid/view/inputmethod/InputMethodManager;

    iput-object p7, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->v:LX/0Or;

    iput-object p8, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->w:LX/2U3;

    iput-object p9, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->x:LX/0Or;

    iput-object p10, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->y:LX/0Or;

    iput-object p11, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object p12, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    iput-object p13, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->B:LX/BXo;

    iput-object p14, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->C:LX/8wR;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->D:LX/GHc;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->E:LX/0Or;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->F:LX/16I;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->G:LX/0Uh;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->H:LX/03V;

    return-void
.end method

.method private a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 2

    .prologue
    .line 2328716
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2328717
    :cond_0
    :goto_0
    return-void

    .line 2328718
    :cond_1
    sget-object v0, LX/GCV;->a:[I

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    invoke-virtual {v1}, LX/GGB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2328719
    :pswitch_0
    sget-object v0, LX/8wK;->TARGETING:LX/8wK;

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->N:LX/8wK;

    .line 2328720
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ad:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->LOCAL_AWARENESS:LX/8wL;

    if-ne v0, v1, :cond_2

    .line 2328721
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->N:LX/8wK;

    .line 2328722
    :cond_2
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ad:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-ne v0, v1, :cond_0

    .line 2328723
    sget-object v0, LX/8wK;->BOOST_TYPE:LX/8wK;

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->N:LX/8wK;

    goto :goto_0

    .line 2328724
    :pswitch_1
    sget-object v0, LX/8wK;->INFO_CARD:LX/8wK;

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->N:LX/8wK;

    goto :goto_0

    .line 2328725
    :pswitch_2
    sget-object v0, LX/8wK;->INSIGHTS_SUMMARY:LX/8wK;

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->N:LX/8wK;

    .line 2328726
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ad:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    instance-of v0, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v0, :cond_0

    .line 2328727
    sget-object v0, LX/8wK;->PROMOTION_DETAILS:LX/8wK;

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->N:LX/8wK;

    goto :goto_0

    .line 2328728
    :pswitch_3
    sget-object v0, LX/8wK;->ERROR_CARD:LX/8wK;

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->N:LX/8wK;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const v10, 0x5a0001

    const/4 v2, 0x0

    .line 2328729
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->U:Z

    if-eqz v0, :cond_1

    .line 2328730
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x4

    invoke-interface {v0, v10, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2328731
    :cond_0
    :goto_0
    return-void

    .line 2328732
    :cond_1
    if-nez p1, :cond_2

    .line 2328733
    invoke-static {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->v(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)V

    .line 2328734
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x3

    invoke-interface {v0, v10, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_0

    .line 2328735
    :cond_2
    invoke-direct {p0, p1}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2328736
    const v0, 0x7f0d002f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2328737
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    .line 2328738
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 2328739
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2328740
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->W:Landroid/view/View;

    if-eqz v1, :cond_3

    .line 2328741
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->W:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->W:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2328742
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->W:Landroid/view/View;

    .line 2328743
    :cond_3
    const v1, 0x7f0d1e11

    invoke-virtual {p0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 2328744
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2328745
    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->u()V

    .line 2328746
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2328747
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 2328748
    invoke-static {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->y(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)LX/GGc;

    move-result-object v1

    invoke-interface {v1}, LX/GGc;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    :goto_1
    if-ge v2, v7, :cond_6

    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/GEW;

    .line 2328749
    invoke-direct {p0, v1, p1}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->a(LX/GEW;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2328750
    invoke-direct {p0, v1, p1, v3, v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->a(LX/GEW;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Landroid/view/LayoutInflater;Landroid/widget/LinearLayout;)Landroid/view/View;

    move-result-object v8

    .line 2328751
    if-eqz v8, :cond_4

    .line 2328752
    invoke-direct {p0, v1, p2}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->a(LX/GEW;Landroid/os/Bundle;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 2328753
    invoke-direct {p0, v1}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->a(LX/GEW;)Z

    .line 2328754
    :cond_4
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2328755
    :cond_5
    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2328756
    invoke-interface {v1}, LX/GEW;->c()LX/8wK;

    move-result-object v9

    if-eqz v9, :cond_4

    .line 2328757
    invoke-interface {v1}, LX/GEW;->c()LX/8wK;

    move-result-object v1

    invoke-virtual {v5, v1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 2328758
    :cond_6
    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->w()Z

    .line 2328759
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->K:LX/0Px;

    .line 2328760
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    invoke-virtual {v1, v5}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    move-result-object v1

    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->L:LX/0P1;

    .line 2328761
    new-instance v1, Landroid/animation/LayoutTransition;

    invoke-direct {v1}, Landroid/animation/LayoutTransition;-><init>()V

    .line 2328762
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 2328763
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x2

    invoke-interface {v0, v10, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2328764
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    const/4 v1, 0x0

    .line 2328765
    iget-object v2, v0, LX/GCE;->l:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2328766
    :goto_3
    move v0, v1

    .line 2328767
    if-nez v0, :cond_0

    .line 2328768
    invoke-direct {p0, p1}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    goto/16 :goto_0

    .line 2328769
    :cond_7
    iget-object v2, v0, LX/GCE;->l:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/GLe;

    .line 2328770
    if-eqz v1, :cond_8

    .line 2328771
    iget-object v2, v1, LX/GLe;->a:LX/GLj;

    iget-object v2, v2, LX/GLj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-static {v2}, LX/GMo;->a(Landroid/view/View;)V

    .line 2328772
    :cond_8
    const/4 v1, 0x1

    goto :goto_3
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 22

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v21

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    invoke-static/range {v21 .. v21}, LX/GGd;->a(LX/0QB;)LX/GGd;

    move-result-object v3

    check-cast v3, LX/GGd;

    invoke-static/range {v21 .. v21}, LX/GF4;->a(LX/0QB;)LX/GF4;

    move-result-object v4

    check-cast v4, LX/GF4;

    invoke-static/range {v21 .. v21}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v21 .. v21}, LX/1B1;->a(LX/0QB;)LX/1B1;

    move-result-object v6

    check-cast v6, LX/1B1;

    invoke-static/range {v21 .. v21}, LX/15N;->a(LX/0QB;)LX/01T;

    move-result-object v7

    check-cast v7, LX/01T;

    invoke-static/range {v21 .. v21}, LX/10d;->a(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v8

    check-cast v8, Landroid/view/inputmethod/InputMethodManager;

    const/16 v9, 0x1677

    move-object/from16 v0, v21

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static/range {v21 .. v21}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v10

    check-cast v10, LX/2U3;

    const/16 v11, 0x2ca

    move-object/from16 v0, v21

    invoke-static {v0, v11}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v12, 0x12af

    move-object/from16 v0, v21

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static/range {v21 .. v21}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v13

    check-cast v13, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static/range {v21 .. v21}, LX/GCE;->a(LX/0QB;)LX/GCE;

    move-result-object v14

    check-cast v14, LX/GCE;

    invoke-static/range {v21 .. v21}, LX/BXo;->a(LX/0QB;)LX/BXo;

    move-result-object v15

    check-cast v15, LX/BXo;

    invoke-static/range {v21 .. v21}, LX/8wR;->a(LX/0QB;)LX/8wR;

    move-result-object v16

    check-cast v16, LX/8wR;

    invoke-static/range {v21 .. v21}, LX/GHc;->a(LX/0QB;)LX/GHc;

    move-result-object v17

    check-cast v17, LX/GHc;

    const/16 v18, 0x1684

    move-object/from16 v0, v21

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v18

    invoke-static/range {v21 .. v21}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v19

    check-cast v19, LX/16I;

    invoke-static/range {v21 .. v21}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v20

    check-cast v20, LX/0Uh;

    invoke-static/range {v21 .. v21}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v21

    check-cast v21, LX/03V;

    invoke-static/range {v2 .. v21}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->a(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;LX/GGd;LX/GF4;Lcom/facebook/content/SecureContextHelper;LX/1B1;LX/01T;Landroid/view/inputmethod/InputMethodManager;LX/0Or;LX/2U3;LX/0Or;LX/0Or;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/GCE;LX/BXo;LX/8wR;LX/GHc;LX/0Or;LX/16I;LX/0Uh;LX/03V;)V

    return-void
.end method

.method private a(LX/GEW;)Z
    .locals 5

    .prologue
    .line 2328773
    :try_start_0
    invoke-interface {p1}, LX/GEW;->b()LX/GHg;

    move-result-object v0

    .line 2328774
    if-eqz v0, :cond_0

    .line 2328775
    invoke-virtual {v0}, LX/GHg;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2328776
    :cond_0
    const/4 v0, 0x1

    .line 2328777
    :goto_0
    return v0

    .line 2328778
    :catch_0
    move-exception v0

    .line 2328779
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->w:LX/2U3;

    const-class v2, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception Caught in unbind of component "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2328780
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/GEW;Landroid/os/Bundle;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2328781
    invoke-interface {p1}, LX/GEW;->b()LX/GHg;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2328782
    :goto_0
    return v0

    .line 2328783
    :cond_0
    :try_start_0
    invoke-interface {p1}, LX/GEW;->b()LX/GHg;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/GHg;->b(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2328784
    :catch_0
    move-exception v0

    .line 2328785
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->w:LX/2U3;

    const-class v2, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "restore for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " failed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2328786
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/GEW;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 5

    .prologue
    .line 2328787
    :try_start_0
    invoke-interface {p1, p2}, LX/GEW;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2328788
    :goto_0
    return v0

    .line 2328789
    :catch_0
    move-exception v0

    .line 2328790
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->w:LX/2U3;

    const-class v2, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "isNeeded for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " failed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2328791
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;Landroid/os/Bundle;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2328670
    invoke-static {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->y(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)LX/GGc;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    new-instance v2, LX/GCZ;

    invoke-direct {v2, p0, p1, p2}, LX/GCZ;-><init>(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;Landroid/os/Bundle;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;)V

    invoke-interface {v0, v1, v2}, LX/GGc;->a(Landroid/content/Intent;LX/GCY;)V

    .line 2328671
    return-void
.end method

.method private b(Ljava/lang/String;)LX/8wK;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2328792
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->Y:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 2328793
    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->q()V

    .line 2328794
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->Y:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2328795
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->Y:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8wK;

    .line 2328796
    :goto_0
    return-object v0

    .line 2328797
    :cond_1
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->H:LX/03V;

    sget-object v1, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->I:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot find client equivalent section of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2328798
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LX/GEW;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Landroid/view/LayoutInflater;Landroid/widget/LinearLayout;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2328799
    invoke-interface {p1}, LX/GEW;->a()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p3, v0, p4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2328800
    invoke-interface {p1}, LX/GEW;->c()LX/8wK;

    move-result-object v0

    sget-object v3, LX/8wK;->FOOTER:LX/8wK;

    if-ne v0, v3, :cond_2

    invoke-static {p2}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->c(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2328801
    const v0, 0x7f0d1e11

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2328802
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2328803
    :cond_0
    :goto_0
    instance-of v0, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    if-eqz v0, :cond_8

    move-object v0, v1

    .line 2328804
    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2328805
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    move-object v3, v1

    move-object v1, v0

    .line 2328806
    :goto_1
    invoke-interface {p1}, LX/GEW;->b()LX/GHg;

    move-result-object v0

    .line 2328807
    if-eqz v0, :cond_1

    .line 2328808
    :try_start_0
    invoke-virtual {v0, p2}, LX/GHg;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2328809
    invoke-virtual {v0, v3, v1}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2328810
    invoke-static {p1, p2}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->b(LX/GEW;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2328811
    new-instance v0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity$4;

    invoke-direct {v0, p0, v3}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity$4;-><init>(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;Landroid/view/View;)V

    .line 2328812
    invoke-virtual {v3, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2328813
    :cond_1
    if-eqz v1, :cond_7

    move-object v2, v1

    :goto_2
    return-object v2

    .line 2328814
    :cond_2
    instance-of v0, p1, LX/GF0;

    if-eqz v0, :cond_3

    .line 2328815
    const v0, 0x7f0d1e0f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v3

    .line 2328816
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2328817
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v3

    .line 2328818
    if-ltz v3, :cond_0

    .line 2328819
    iput-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->W:Landroid/view/View;

    .line 2328820
    invoke-virtual {v0, v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_0

    .line 2328821
    :cond_3
    invoke-virtual {p4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 2328822
    :catch_0
    move-exception v0

    move-object v4, v0

    .line 2328823
    invoke-interface {p1}, LX/GEW;->c()LX/8wK;

    move-result-object v0

    sget-object v5, LX/8wK;->FOOTER:LX/8wK;

    if-ne v0, v5, :cond_5

    invoke-static {p2}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->c(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2328824
    const v0, 0x7f0d1e11

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2328825
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2328826
    if-eqz v1, :cond_4

    :goto_3
    invoke-virtual {p4, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 2328827
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->w:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "bindComponent for "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " for component type "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1}, LX/GEW;->c()LX/8wK;

    move-result-object v5

    invoke-virtual {v5}, LX/8wK;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " failed"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3, v4}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_4
    move-object v1, v3

    .line 2328828
    goto :goto_3

    .line 2328829
    :cond_5
    if-eqz v1, :cond_6

    :goto_4
    invoke-virtual {p4, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 2328830
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->w:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "bindComponent for "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " for component type "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1}, LX/GEW;->c()LX/8wK;

    move-result-object v5

    invoke-virtual {v5}, LX/8wK;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " failed"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3, v4}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    :cond_6
    move-object v1, v3

    .line 2328831
    goto :goto_4

    :cond_7
    move-object v2, v3

    .line 2328832
    goto/16 :goto_2

    :cond_8
    move-object v3, v1

    move-object v1, v2

    goto/16 :goto_1
.end method

.method public static b(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Landroid/os/Bundle;)V
    .locals 13
    .param p0    # Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2328833
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ad:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2328834
    if-ne v0, p1, :cond_7

    .line 2328835
    :cond_0
    :goto_0
    move v0, v1

    .line 2328836
    if-eqz v0, :cond_2

    .line 2328837
    :cond_1
    :goto_1
    return-void

    .line 2328838
    :cond_2
    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->x()V

    .line 2328839
    if-eqz p1, :cond_5

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->T:Z

    .line 2328840
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    if-eqz p1, :cond_6

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    .line 2328841
    :goto_3
    iget-object v2, v1, LX/GCE;->h:LX/GGB;

    iput-object v2, v1, LX/GCE;->g:LX/GGB;

    .line 2328842
    iput-object v0, v1, LX/GCE;->h:LX/GGB;

    .line 2328843
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    invoke-virtual {v0}, LX/GCE;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2328844
    const-string v0, "confirmation_dialogue"

    invoke-interface {p1, v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->c(Ljava/lang/String;)V

    .line 2328845
    :cond_3
    iput-object p1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ad:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2328846
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    invoke-virtual {v0}, LX/GCE;->f()V

    .line 2328847
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    .line 2328848
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2328849
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ad:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2328850
    iput-object v1, v0, LX/GG3;->h:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2328851
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Landroid/os/Bundle;)V

    .line 2328852
    invoke-direct {p0, p1}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->d(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2328853
    invoke-direct {p0, p2}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->e(Landroid/os/Bundle;)V

    .line 2328854
    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->z()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_POST:LX/8wL;

    if-eq v0, v1, :cond_4

    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->z()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-ne v0, v1, :cond_1

    :cond_4
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->T:Z

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2328855
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 2328856
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->C:LX/8wR;

    new-instance v2, LX/8wS;

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v3

    invoke-static {v3}, LX/GG6;->a(LX/GGB;)Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v3

    invoke-direct {v2, v0, v3}, LX/8wS;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    goto :goto_1

    .line 2328857
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 2328858
    :cond_6
    const/4 v0, 0x0

    goto :goto_3

    .line 2328859
    :cond_7
    if-eqz v0, :cond_8

    if-nez p1, :cond_9

    :cond_8
    move v1, v2

    .line 2328860
    goto :goto_0

    .line 2328861
    :cond_9
    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v4

    if-ne v3, v4, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->t()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->t()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;

    move-result-object v4

    invoke-static {v3, v4}, LX/GG7;->a(Lcom/facebook/flatbuffers/Flattenable;Lcom/facebook/flatbuffers/Flattenable;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v4

    invoke-static {v3, v4}, LX/GG7;->a(Lcom/facebook/flatbuffers/Flattenable;Lcom/facebook/flatbuffers/Flattenable;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->u()Z

    move-result v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->u()Z

    move-result v4

    if-ne v3, v4, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->C()Z

    move-result v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->C()Z

    move-result v4

    if-ne v3, v4, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->w()I

    move-result v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->w()I

    move-result v4

    if-ne v3, v4, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->v()LX/0Px;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->v()LX/0Px;

    move-result-object v4

    invoke-static {v3, v4}, LX/GG7;->a(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->B()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->B()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v4

    if-ne v3, v4, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->s()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->s()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel;

    move-result-object v4

    invoke-static {v3, v4}, LX/GG7;->a(Lcom/facebook/flatbuffers/Flattenable;Lcom/facebook/flatbuffers/Flattenable;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->x()LX/4Cf;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->x()LX/4Cf;

    move-result-object v4

    .line 2328862
    invoke-static {v3, v4}, LX/GG7;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/03R;

    move-result-object v5

    .line 2328863
    invoke-virtual {v5}, LX/03R;->isSet()Z

    move-result v6

    if-eqz v6, :cond_d

    .line 2328864
    invoke-virtual {v5}, LX/03R;->asBoolean()Z

    move-result v5

    .line 2328865
    :goto_4
    move v3, v5

    .line 2328866
    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->y()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->y()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v4

    invoke-static {v3, v4}, LX/GG7;->a(Lcom/facebook/flatbuffers/Flattenable;Lcom/facebook/flatbuffers/Flattenable;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->f()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->f()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v4

    invoke-static {v3, v4}, LX/GG7;->a(Lcom/facebook/flatbuffers/Flattenable;Lcom/facebook/flatbuffers/Flattenable;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->i()I

    move-result v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->i()I

    move-result v4

    if-ne v3, v4, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->j()I

    move-result v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->j()I

    move-result v4

    if-ne v3, v4, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->D()Landroid/content/Intent;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->D()Landroid/content/Intent;

    move-result-object v4

    .line 2328867
    if-eq v3, v4, :cond_a

    if-eqz v3, :cond_e

    invoke-virtual {v3, v4}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    move-result v5

    if-eqz v5, :cond_e

    :cond_a
    const/4 v5, 0x1

    :goto_5
    move v3, v5

    .line 2328868
    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->F()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->F()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;

    move-result-object v4

    invoke-static {v3, v4}, LX/GG7;->a(Lcom/facebook/flatbuffers/Flattenable;Lcom/facebook/flatbuffers/Flattenable;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->E()Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->E()Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    move-result-object v4

    if-ne v3, v4, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->k()I

    move-result v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->k()I

    move-result v4

    if-ne v3, v4, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v4

    if-ne v3, v4, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->r()LX/0Px;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->r()LX/0Px;

    move-result-object v4

    invoke-static {v3, v4}, LX/GG7;->a(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->n()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->p()LX/175;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->p()LX/175;

    move-result-object v4

    const/4 v8, 0x0

    .line 2328869
    invoke-static {v3, v4}, LX/GG7;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/03R;

    move-result-object v5

    .line 2328870
    invoke-virtual {v5}, LX/03R;->isSet()Z

    move-result v6

    if-eqz v6, :cond_f

    .line 2328871
    invoke-virtual {v5}, LX/03R;->asBoolean()Z

    move-result v8

    .line 2328872
    :cond_b
    :goto_6
    move v3, v8

    .line 2328873
    if-eqz v3, :cond_c

    invoke-static {v0}, LX/GG7;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, LX/GG7;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v4

    invoke-static {v3, v4}, LX/GG7;->a(Lcom/facebook/flatbuffers/Flattenable;Lcom/facebook/flatbuffers/Flattenable;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->g()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->g()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v4

    invoke-static {v3, v4}, LX/GG7;->a(Lcom/facebook/flatbuffers/Flattenable;Lcom/facebook/flatbuffers/Flattenable;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->z()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->z()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    move-result-object v4

    invoke-static {v3, v4}, LX/GG7;->a(Lcom/facebook/flatbuffers/Flattenable;Lcom/facebook/flatbuffers/Flattenable;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->A()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->A()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    move-result-object v4

    invoke-static {v3, v4}, LX/GG7;->a(Lcom/facebook/flatbuffers/Flattenable;Lcom/facebook/flatbuffers/Flattenable;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v4

    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->q()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->q()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    move-result-object v4

    invoke-static {v3, v4}, LX/GG7;->a(Lcom/facebook/flatbuffers/Flattenable;Lcom/facebook/flatbuffers/Flattenable;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_c
    move v1, v2

    goto/16 :goto_0

    :cond_d
    invoke-virtual {v3}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v4}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v6

    invoke-static {v5, v6}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    goto/16 :goto_4

    :cond_e
    const/4 v5, 0x0

    goto/16 :goto_5

    .line 2328874
    :cond_f
    invoke-interface {v3}, LX/175;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4}, LX/175;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 2328875
    invoke-interface {v3}, LX/175;->b()LX/0Px;

    move-result-object v9

    .line 2328876
    invoke-interface {v4}, LX/175;->b()LX/0Px;

    move-result-object v10

    .line 2328877
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v5

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v6

    if-ne v5, v6, :cond_b

    move v7, v8

    .line 2328878
    :goto_7
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v5

    if-ge v7, v5, :cond_10

    .line 2328879
    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1W5;

    .line 2328880
    invoke-virtual {v10, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1W5;

    .line 2328881
    invoke-interface {v5}, LX/1W5;->b()I

    move-result v11

    invoke-interface {v6}, LX/1W5;->b()I

    move-result v12

    if-ne v11, v12, :cond_b

    invoke-interface {v5}, LX/1W5;->c()I

    move-result v11

    invoke-interface {v6}, LX/1W5;->c()I

    move-result v12

    if-ne v11, v12, :cond_b

    invoke-interface {v5}, LX/1W5;->a()LX/171;

    move-result-object v5

    invoke-interface {v6}, LX/1W5;->a()LX/171;

    move-result-object v6

    .line 2328882
    invoke-static {v5, v6}, LX/GG7;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/03R;

    move-result-object v11

    .line 2328883
    invoke-virtual {v11}, LX/03R;->isSet()Z

    move-result v12

    if-eqz v12, :cond_11

    .line 2328884
    invoke-virtual {v11}, LX/03R;->asBoolean()Z

    move-result v11

    .line 2328885
    :goto_8
    move v5, v11

    .line 2328886
    if-eqz v5, :cond_b

    .line 2328887
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto :goto_7

    .line 2328888
    :cond_10
    const/4 v8, 0x1

    goto/16 :goto_6

    :cond_11
    invoke-interface {v5}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    invoke-interface {v6}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v12

    if-ne v11, v12, :cond_12

    invoke-interface {v5}, LX/171;->c()LX/0Px;

    move-result-object v11

    invoke-interface {v6}, LX/171;->c()LX/0Px;

    move-result-object v12

    invoke-static {v11, v12}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_12

    invoke-interface {v5}, LX/171;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v11

    invoke-interface {v6}, LX/171;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v12

    if-ne v11, v12, :cond_12

    invoke-interface {v5}, LX/171;->e()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v6}, LX/171;->e()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_12

    invoke-interface {v5}, LX/171;->v_()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v6}, LX/171;->v_()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_12

    invoke-interface {v5}, LX/171;->w_()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v6}, LX/171;->w_()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_12

    invoke-interface {v5}, LX/171;->j()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v6}, LX/171;->j()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_12

    const/4 v11, 0x1

    goto :goto_8

    :cond_12
    const/4 v11, 0x0

    goto :goto_8
.end method

.method public static b(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;Z)V
    .locals 4

    .prologue
    .line 2328889
    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->n()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2328890
    :cond_0
    return-void

    .line 2328891
    :cond_1
    invoke-static {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->y(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)LX/GGc;

    move-result-object v0

    invoke-interface {v0}, LX/GGc;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GEW;

    .line 2328892
    invoke-interface {v0}, LX/GEW;->b()LX/GHg;

    move-result-object v0

    .line 2328893
    if-eqz v0, :cond_2

    .line 2328894
    invoke-virtual {v0, p1}, LX/GHg;->h_(Z)V

    .line 2328895
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2328896
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->c(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2328897
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "scroll_to_section"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2328898
    invoke-direct {p0, v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->b(Ljava/lang/String;)LX/8wK;

    move-result-object v0

    .line 2328899
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->L:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2328900
    if-eqz v0, :cond_0

    .line 2328901
    invoke-static {v0}, LX/GMo;->b(Landroid/view/View;)V

    .line 2328902
    iput-object v6, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->N:LX/8wK;

    .line 2328903
    :goto_0
    return-void

    .line 2328904
    :cond_0
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_POST:LX/8wL;

    if-ne v0, v1, :cond_2

    invoke-static {p1}, LX/GG6;->k(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2328905
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->L:LX/0P1;

    sget-object v1, LX/8wK;->BOOST_TYPE:LX/8wK;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2328906
    if-nez v0, :cond_1

    .line 2328907
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->L:LX/0P1;

    sget-object v1, LX/8wK;->CALL_TO_ACTION:LX/8wK;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2328908
    :cond_1
    if-eqz v0, :cond_2

    .line 2328909
    invoke-static {v0}, LX/GMo;->b(Landroid/view/View;)V

    .line 2328910
    iput-object v6, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->N:LX/8wK;

    goto :goto_0

    .line 2328911
    :cond_2
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->L:LX/0P1;

    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->N:LX/8wK;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2328912
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    .line 2328913
    iget-object v2, v1, LX/GCE;->e:LX/0ad;

    move-object v1, v2

    .line 2328914
    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget-short v4, LX/GDK;->c:S

    const/4 v5, 0x0

    invoke-interface {v1, v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    invoke-static {v1}, LX/GEw;->a(LX/GGB;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2328915
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->L:LX/0P1;

    sget-object v1, LX/8wK;->INFO_CARD:LX/8wK;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2328916
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 2328917
    :cond_3
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->L:LX/0P1;

    sget-object v1, LX/8wK;->OVERVIEW:LX/8wK;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2328918
    :cond_4
    if-eqz v0, :cond_5

    .line 2328919
    invoke-static {v0}, LX/GMo;->b(Landroid/view/View;)V

    .line 2328920
    :cond_5
    iput-object v6, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->N:LX/8wK;

    goto :goto_0
.end method

.method private b(LX/GEW;Landroid/os/Bundle;)Z
    .locals 5

    .prologue
    .line 2328921
    :try_start_0
    invoke-interface {p1}, LX/GEW;->b()LX/GHg;

    move-result-object v0

    .line 2328922
    if-eqz v0, :cond_0

    .line 2328923
    invoke-virtual {v0, p2}, LX/GHg;->a(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2328924
    :cond_0
    const/4 v0, 0x1

    .line 2328925
    :goto_0
    return v0

    .line 2328926
    :catch_0
    move-exception v0

    .line 2328927
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->w:LX/2U3;

    const-class v2, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception Caught in save of component "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2328928
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/GEW;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 2

    .prologue
    .line 2328929
    instance-of v0, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/GEW;->c()LX/8wK;

    move-result-object v0

    sget-object v1, LX/8wK;->FOOTER:LX/8wK;

    if-ne v0, v1, :cond_0

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOSTED_COMPONENT_EDIT_DURATION_BUDGET:LX/8wL;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 2328930
    const-string v0, "scroll_to_section"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2328931
    invoke-static {v0}, LX/8wJ;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static c(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 3
    .param p0    # Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2328932
    if-nez p0, :cond_1

    .line 2328933
    :cond_0
    :goto_0
    return v0

    .line 2328934
    :cond_1
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    sget-object v2, LX/8wL;->BOOST_POST:LX/8wL;

    if-eq v1, v2, :cond_0

    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    sget-object v2, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-eq v1, v2, :cond_0

    .line 2328935
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static d(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2328936
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->T:Z

    if-eqz v0, :cond_0

    .line 2328937
    invoke-direct {p0, p1}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->e(Landroid/os/Bundle;)V

    .line 2328938
    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->w()Z

    .line 2328939
    :goto_0
    return-void

    .line 2328940
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->Z:LX/4At;

    if-nez v0, :cond_1

    .line 2328941
    new-instance v0, LX/4At;

    const v1, 0x7f080b2e

    invoke-virtual {p0, v1}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->Z:LX/4At;

    .line 2328942
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->Z:LX/4At;

    invoke-virtual {v0}, LX/4At;->beginShowingProgress()V

    .line 2328943
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2328944
    invoke-static {v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->d(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2328945
    const-string v1, "coupon_promotion_group_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2328946
    const-string v2, "ad_account_id"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2328947
    invoke-static {v0}, LX/8wJ;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 2328948
    invoke-direct {p0, p1, v1, v2, v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2328949
    :cond_2
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->a$redex0(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;Landroid/os/Bundle;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;)V

    goto :goto_0
.end method

.method private d(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 8

    .prologue
    .line 2328950
    if-nez p1, :cond_0

    .line 2328951
    :goto_0
    return-void

    .line 2328952
    :cond_0
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_POST_INSIGHTS:LX/8wL;

    if-ne v0, v1, :cond_1

    .line 2328953
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    .line 2328954
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2328955
    const/4 v6, 0x0

    .line 2328956
    const-string v4, "enter_flow"

    const-string v5, "post_insights"

    move-object v2, v0

    move-object v3, p1

    move-object v7, v6

    invoke-static/range {v2 .. v7}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2328957
    goto :goto_0

    .line 2328958
    :cond_1
    invoke-static {p1}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2328959
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    .line 2328960
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2328961
    const/4 v6, 0x0

    .line 2328962
    const-string v4, "enter_flow"

    const-string v5, "edit"

    move-object v2, v0

    move-object v3, p1

    move-object v7, v6

    invoke-static/range {v2 .. v7}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2328963
    goto :goto_0

    .line 2328964
    :cond_2
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    .line 2328965
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2328966
    const/4 v6, 0x0

    .line 2328967
    const-string v4, "enter_flow"

    const-string v5, "create"

    move-object v2, v0

    move-object v3, p1

    move-object v7, v6

    invoke-static/range {v2 .. v7}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2328968
    goto :goto_0
.end method

.method private static d(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 2328969
    const-string v0, "coupon_promotion_group_id"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2328970
    const-string v1, "ad_account_id"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2328971
    invoke-static {v0}, LX/8wJ;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v1}, LX/8wJ;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2328972
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->B:LX/BXo;

    const-string v1, "adinterfaces_objective"

    const v2, 0x7f0d1e10

    invoke-virtual {p0, v2}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v2

    .line 2328973
    if-nez p1, :cond_1

    .line 2328974
    :cond_0
    return-void

    .line 2328975
    :cond_1
    invoke-virtual {v0}, LX/BXo;->a()V

    .line 2328976
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".scrollViewId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 2328977
    if-eqz v3, :cond_2

    .line 2328978
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 2328979
    if-eqz v3, :cond_2

    instance-of v4, v3, Lcom/facebook/widget/scrollview/LockableScrollView;

    if-eqz v4, :cond_2

    .line 2328980
    check-cast v3, Lcom/facebook/widget/scrollview/LockableScrollView;

    iput-object v3, v0, LX/BXo;->b:Lcom/facebook/widget/scrollview/LockableScrollView;

    .line 2328981
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".lockingViewIds"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 2328982
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_0

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 2328983
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 2328984
    if-eqz v3, :cond_3

    .line 2328985
    iget-object p0, v0, LX/BXo;->a:Ljava/util/List;

    invoke-interface {p0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2328986
    :cond_3
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0
.end method

.method private e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 13

    .prologue
    .line 2328987
    if-nez p1, :cond_1

    .line 2328988
    :cond_0
    :goto_0
    return-void

    .line 2328989
    :cond_1
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_POST_INSIGHTS:LX/8wL;

    if-ne v0, v1, :cond_2

    .line 2328990
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    .line 2328991
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2328992
    const/4 v8, 0x0

    .line 2328993
    const-string v6, "exit_flow"

    const-string v7, "post_insights"

    move-object v4, v0

    move-object v5, p1

    move-object v9, v8

    invoke-static/range {v4 .. v9}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2328994
    goto :goto_0

    .line 2328995
    :cond_2
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    const/4 v1, 0x5

    new-array v1, v1, [LX/8wL;

    const/4 v2, 0x0

    sget-object v3, LX/8wL;->PAGE_LIKE_EDIT_CREATIVE:LX/8wL;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, LX/8wL;->PROMOTE_WEBSITE_EDIT_CREATIVE:LX/8wL;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, LX/8wL;->BOOST_EVENT_EDIT_CREATIVE:LX/8wL;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, LX/8wL;->PROMOTE_CTA_EDIT_CREATIVE:LX/8wL;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, LX/8wL;->LOCAL_AWARENESS_EDIT_CREATIVE:LX/8wL;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2328996
    array-length v5, v1

    move v4, v3

    :goto_1
    if-ge v4, v5, :cond_8

    aget-object v6, v1, v4

    .line 2328997
    if-nez v0, :cond_6

    .line 2328998
    if-nez v6, :cond_7

    .line 2328999
    :cond_3
    :goto_2
    move v0, v2

    .line 2329000
    if-eqz v0, :cond_4

    .line 2329001
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    .line 2329002
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2329003
    const/4 v8, 0x0

    .line 2329004
    const-string v6, "cancel_flow"

    const-string v7, "creative_edit"

    const-string v10, "promote_dialog"

    const/4 v12, 0x1

    move-object v4, v0

    move-object v5, p1

    move-object v9, v8

    move-object v11, v8

    invoke-static/range {v4 .. v12}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2329005
    goto :goto_0

    .line 2329006
    :cond_4
    invoke-static {p1}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2329007
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    .line 2329008
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2329009
    const/4 v8, 0x0

    .line 2329010
    const-string v6, "cancel_flow"

    const-string v7, "edit"

    move-object v4, v0

    move-object v5, p1

    move-object v9, v8

    invoke-static/range {v4 .. v9}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2329011
    invoke-virtual {v0, p1}, LX/GG3;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2329012
    goto :goto_0

    .line 2329013
    :cond_5
    invoke-static {p1}, LX/GG6;->k(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2329014
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    .line 2329015
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2329016
    const/4 v8, 0x0

    .line 2329017
    const-string v6, "cancel_flow"

    const-string v7, "create"

    move-object v4, v0

    move-object v5, p1

    move-object v9, v8

    invoke-static/range {v4 .. v9}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2329018
    invoke-virtual {v0, p1}, LX/GG3;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2329019
    goto/16 :goto_0

    .line 2329020
    :cond_6
    invoke-virtual {v0, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 2329021
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_8
    move v2, v3

    .line 2329022
    goto :goto_2
.end method

.method private l()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2329023
    const v0, 0x7f0d1e10

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/scrollview/LockableScrollView;

    .line 2329024
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Lcom/facebook/widget/scrollview/LockableScrollView;->setDescendantFocusability(I)V

    .line 2329025
    invoke-virtual {v0, v2}, Lcom/facebook/widget/scrollview/LockableScrollView;->setFocusable(Z)V

    .line 2329026
    invoke-virtual {v0, v2}, Lcom/facebook/widget/scrollview/LockableScrollView;->setFocusableInTouchMode(Z)V

    .line 2329027
    new-instance v1, LX/GCW;

    invoke-direct {v1, p0, v0}, LX/GCW;-><init>(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;Lcom/facebook/widget/scrollview/LockableScrollView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/scrollview/LockableScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2329028
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->B:LX/BXo;

    .line 2329029
    iput-object v0, v1, LX/BXo;->b:Lcom/facebook/widget/scrollview/LockableScrollView;

    .line 2329030
    return-void
.end method

.method public static m(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2328672
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->F:LX/16I;

    invoke-virtual {v1}, LX/16I;->a()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    .line 2328673
    iget-object v2, v1, LX/GCE;->e:LX/0ad;

    move-object v1, v2

    .line 2328674
    sget-short v2, LX/GDK;->u:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private n()Z
    .locals 3

    .prologue
    .line 2328443
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->G:LX/0Uh;

    const/16 v1, 0x367

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method private o()V
    .locals 3

    .prologue
    .line 2328520
    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->n()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2328521
    :goto_0
    return-void

    .line 2328522
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->F:LX/16I;

    sget-object v1, LX/1Ed;->CONNECTED:LX/1Ed;

    new-instance v2, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity$2;

    invoke-direct {v2, p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity$2;-><init>(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)V

    invoke-virtual {v0, v1, v2}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ag:LX/0Yb;

    .line 2328523
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->F:LX/16I;

    sget-object v1, LX/1Ed;->NO_INTERNET:LX/1Ed;

    new-instance v2, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity$3;

    invoke-direct {v2, p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity$3;-><init>(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)V

    invoke-virtual {v0, v1, v2}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ah:LX/0Yb;

    goto :goto_0
.end method

.method private p()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2328512
    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->n()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2328513
    :goto_0
    return-void

    .line 2328514
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ag:LX/0Yb;

    if-eqz v0, :cond_1

    .line 2328515
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ag:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 2328516
    :cond_1
    iput-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ag:LX/0Yb;

    .line 2328517
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ah:LX/0Yb;

    if-eqz v0, :cond_2

    .line 2328518
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ah:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 2328519
    :cond_2
    iput-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ah:LX/0Yb;

    goto :goto_0
.end method

.method private q()V
    .locals 6

    .prologue
    .line 2328505
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 2328506
    invoke-static {}, LX/8wK;->values()[LX/8wK;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 2328507
    iget-object v5, v4, LX/8wK;->mServerType:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 2328508
    iget-object v5, v4, LX/8wK;->mServerType:Ljava/lang/String;

    invoke-virtual {v1, v5, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2328509
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2328510
    :cond_1
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->Y:Ljava/util/Map;

    .line 2328511
    return-void
.end method

.method private r()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2328496
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->V:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2328497
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "flow_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->V:Ljava/lang/String;

    .line 2328498
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->V:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2328499
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->V:Ljava/lang/String;

    .line 2328500
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    .line 2328501
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2328502
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->V:Ljava/lang/String;

    .line 2328503
    iput-object v1, v0, LX/GG3;->g:Ljava/lang/String;

    .line 2328504
    :cond_1
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->V:Ljava/lang/String;

    return-object v0
.end method

.method public static s(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)LX/0kb;
    .locals 1

    .prologue
    .line 2328493
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ac:LX/0kb;

    if-nez v0, :cond_0

    .line 2328494
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->x:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kb;

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ac:LX/0kb;

    .line 2328495
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ac:LX/0kb;

    return-object v0
.end method

.method public static t(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)LX/1CX;
    .locals 1

    .prologue
    .line 2328490
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ae:LX/1CX;

    if-nez v0, :cond_0

    .line 2328491
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->y:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1CX;

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ae:LX/1CX;

    .line 2328492
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ae:LX/1CX;

    return-object v0
.end method

.method private u()V
    .locals 1

    .prologue
    .line 2328486
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->Z:LX/4At;

    if-eqz v0, :cond_0

    .line 2328487
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->Z:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2328488
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->Z:LX/4At;

    .line 2328489
    :cond_0
    return-void
.end method

.method public static v(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)V
    .locals 2

    .prologue
    .line 2328476
    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->u()V

    .line 2328477
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->aa:Lcom/facebook/widget/error/GenericErrorView;

    if-nez v0, :cond_0

    .line 2328478
    const v0, 0x7f0d1e0e

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/error/GenericErrorViewStub;

    .line 2328479
    invoke-virtual {v0}, LX/4nv;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/error/GenericErrorView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->aa:Lcom/facebook/widget/error/GenericErrorView;

    .line 2328480
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->aa:Lcom/facebook/widget/error/GenericErrorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/error/GenericErrorView;->setVisibility(I)V

    .line 2328481
    invoke-static {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->s(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)LX/0kb;

    move-result-object v0

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2328482
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->aa:Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v0}, Lcom/facebook/widget/error/GenericErrorView;->b()V

    .line 2328483
    :goto_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->aa:Lcom/facebook/widget/error/GenericErrorView;

    new-instance v1, LX/GCX;

    invoke-direct {v1, p0}, LX/GCX;-><init>(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/error/GenericErrorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2328484
    return-void

    .line 2328485
    :cond_1
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->aa:Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v0}, Lcom/facebook/widget/error/GenericErrorView;->a()V

    goto :goto_0
.end method

.method private w()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2328467
    iget-boolean v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->P:Z

    if-eqz v1, :cond_1

    .line 2328468
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->q:LX/GF4;

    iget v2, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->Q:I

    iget v3, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->R:I

    iget-object v4, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->S:Landroid/content/Intent;

    .line 2328469
    iget-object v5, v1, LX/GF4;->a:Landroid/util/SparseArray;

    invoke-virtual {v5, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/GFR;

    .line 2328470
    if-eqz v5, :cond_0

    .line 2328471
    invoke-interface {v5, v2, v4}, LX/GFR;->a(ILandroid/content/Intent;)V

    .line 2328472
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->S:Landroid/content/Intent;

    .line 2328473
    iput-boolean v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->P:Z

    .line 2328474
    const/4 v0, 0x1

    .line 2328475
    :cond_1
    return v0
.end method

.method private x()V
    .locals 3

    .prologue
    .line 2328460
    invoke-static {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->y(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)LX/GGc;

    move-result-object v0

    .line 2328461
    instance-of v1, v0, LX/GGk;

    if-nez v1, :cond_0

    .line 2328462
    :goto_0
    return-void

    .line 2328463
    :cond_0
    check-cast v0, LX/GGk;

    .line 2328464
    invoke-interface {v0}, LX/GGk;->b()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2328465
    iget-object v2, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->af:LX/0h5;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v2, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2328466
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->af:LX/0h5;

    new-instance v2, LX/GCb;

    invoke-direct {v2, p0, v0}, LX/GCb;-><init>(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;LX/GGk;)V

    invoke-interface {v1, v2}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    goto :goto_0
.end method

.method public static y(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)LX/GGc;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/GGc",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2328454
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->J:LX/GGc;

    if-nez v0, :cond_0

    .line 2328455
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->p:LX/GGd;

    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->z()LX/8wL;

    move-result-object v1

    .line 2328456
    iget-object v2, v0, LX/GGd;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Or;

    .line 2328457
    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/GGc;

    move-object v0, v2

    .line 2328458
    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->J:LX/GGc;

    .line 2328459
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->J:LX/GGc;

    return-object v0
.end method

.method private z()LX/8wL;
    .locals 2

    .prologue
    .line 2328452
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "objective"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/8wL;

    .line 2328453
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2328451
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "uri_extra"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2328444
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/content/Intent;)V

    .line 2328445
    const-string v0, "storyId"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2328446
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "storyId"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2328447
    iput-object p1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->X:Landroid/content/Intent;

    .line 2328448
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->q:LX/GF4;

    new-instance v1, LX/GFV;

    invoke-direct {v1}, LX/GFV;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2328449
    :goto_0
    return-void

    .line 2328450
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->X:Landroid/content/Intent;

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const v5, 0x5a0001

    const/4 v4, -0x1

    .line 2328424
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 2328425
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2328426
    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->z()LX/8wL;

    move-result-object v2

    .line 2328427
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2328428
    iget-object v3, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, LX/8wL;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-interface {v3, v5, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 2328429
    const-string v0, "title"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->M:I

    .line 2328430
    iget v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->M:I

    if-eq v0, v4, :cond_1

    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->t:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_1

    .line 2328431
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ab:LX/67X;

    if-nez v0, :cond_0

    .line 2328432
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->v:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67X;

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ab:LX/67X;

    .line 2328433
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ab:LX/67X;

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    .line 2328434
    :cond_1
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->D:LX/GHc;

    .line 2328435
    invoke-static {v2}, LX/GHc;->b(LX/8wL;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2328436
    const-string v1, "186440775060762"

    .line 2328437
    iget-object v2, v0, LX/GHc;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0gt;

    .line 2328438
    iput-object v1, v2, LX/0gt;->a:Ljava/lang/String;

    .line 2328439
    move-object v2, v2

    .line 2328440
    invoke-virtual {v2}, LX/0gt;->b()V

    .line 2328441
    :cond_2
    return-void

    .line 2328442
    :cond_3
    const-string v0, "unknown"

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2328524
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->B:LX/BXo;

    if-eqz v0, :cond_0

    .line 2328525
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->B:LX/BXo;

    invoke-virtual {v0}, LX/BXo;->a()V

    .line 2328526
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ad:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2328527
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->K:LX/0Px;

    if-eqz v0, :cond_1

    .line 2328528
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->K:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->K:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GEW;

    .line 2328529
    invoke-direct {p0, v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->a(LX/GEW;)Z

    .line 2328530
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2328531
    :cond_1
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2328532
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ad:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    if-nez v0, :cond_1

    .line 2328533
    :cond_0
    :goto_0
    return-void

    .line 2328534
    :cond_1
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ad:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_POST:LX/8wL;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ad:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-ne v0, v1, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ad:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->ACTIVE:LX/GGB;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ad:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->PENDING:LX/GGB;

    if-eq v0, v1, :cond_0

    .line 2328535
    :cond_3
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ad:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->c(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2328536
    const v0, 0x7f0d002f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2328537
    invoke-virtual {v0, v2, v2, v2, p1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2328538
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2328539
    const v0, 0x7f030c4e

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->setContentView(I)V

    .line 2328540
    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->l()V

    .line 2328541
    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->B()V

    .line 2328542
    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A()V

    .line 2328543
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2328544
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->c(Landroid/os/Bundle;)V

    .line 2328545
    invoke-static {p0, p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2328546
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2328547
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->B:LX/BXo;

    invoke-virtual {v0, p1}, LX/BXo;->a(Landroid/view/MotionEvent;)V

    .line 2328548
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 2328549
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->X:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 2328550
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->X:Landroid/content/Intent;

    .line 2328551
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2328552
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2328553
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->P:Z

    .line 2328554
    iput p2, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->Q:I

    .line 2328555
    iput-object p3, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->S:Landroid/content/Intent;

    .line 2328556
    iput p1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->R:I

    .line 2328557
    return-void
.end method

.method public final onBackPressed()V
    .locals 9

    .prologue
    .line 2328558
    const v0, 0x7f0d3184

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->c(I)LX/0am;

    move-result-object v1

    .line 2328559
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2328560
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 2328561
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2328562
    :goto_0
    iget-object v2, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    .line 2328563
    iget-object v3, v2, LX/GCE;->f:LX/GG3;

    move-object v2, v3

    .line 2328564
    invoke-virtual {v2, v0}, LX/GG3;->b(Ljava/lang/String;)V

    .line 2328565
    iget-object v2, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    .line 2328566
    iget-object v3, v2, LX/GCE;->f:LX/GG3;

    move-object v2, v3

    .line 2328567
    iget-object v4, v2, LX/GG3;->h:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    const-string v5, "cancel_flow"

    const-string v6, "walkthrough"

    const/4 v8, 0x0

    move-object v3, v2

    move-object v7, v0

    invoke-static/range {v3 .. v8}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2328568
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2328569
    const v0, 0x7f0d0418

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->c(I)LX/0am;

    move-result-object v0

    .line 2328570
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2328571
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2328572
    :cond_0
    :goto_1
    return-void

    .line 2328573
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2328574
    :cond_2
    invoke-static {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->y(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)LX/GGc;

    move-result-object v0

    instance-of v0, v0, LX/GGl;

    if-eqz v0, :cond_3

    .line 2328575
    invoke-static {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->y(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)LX/GGc;

    move-result-object v0

    check-cast v0, LX/GGl;

    .line 2328576
    iget-boolean v1, v0, LX/GGl;->q:Z

    move v1, v1

    .line 2328577
    if-eqz v1, :cond_3

    .line 2328578
    invoke-virtual {v0}, LX/GGl;->d()V

    goto :goto_1

    .line 2328579
    :cond_3
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ad:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-direct {p0, v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2328580
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->D:LX/GHc;

    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->ad:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->z()LX/8wL;

    move-result-object v2

    .line 2328581
    iget-boolean v3, v0, LX/GHc;->c:Z

    if-nez v3, :cond_6

    .line 2328582
    invoke-static {v2}, LX/GHc;->b(LX/8wL;)Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v3, 0x0

    .line 2328583
    if-nez v1, :cond_8

    .line 2328584
    :cond_4
    :goto_2
    move v3, v3

    .line 2328585
    if-eqz v3, :cond_7

    const/4 v3, 0x1

    :goto_3
    move v3, v3

    .line 2328586
    if-eqz v3, :cond_6

    const-string v3, "186440775060762"

    invoke-static {v3}, LX/7Fv;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x1

    :goto_4
    move v0, v3

    .line 2328587
    if-eqz v0, :cond_5

    .line 2328588
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->D:LX/GHc;

    .line 2328589
    iget-object v1, v0, LX/GHc;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gt;

    const-string v2, "186440775060762"

    .line 2328590
    iput-object v2, v1, LX/0gt;->a:Ljava/lang/String;

    .line 2328591
    move-object v1, v1

    .line 2328592
    iget-object v2, v0, LX/GHc;->a:Landroid/content/Context;

    invoke-virtual {v1, v2}, LX/0gt;->b(Landroid/content/Context;)V

    .line 2328593
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/GHc;->c:Z

    .line 2328594
    goto :goto_1

    .line 2328595
    :cond_5
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->D:LX/GHc;

    .line 2328596
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/GHc;->c:Z

    .line 2328597
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_1

    :cond_6
    const/4 v3, 0x0

    goto :goto_4

    :cond_7
    const/4 v3, 0x0

    goto :goto_3

    .line 2328598
    :cond_8
    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    .line 2328599
    sget-object v2, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-eq v0, v2, :cond_9

    sget-object v2, LX/GGB;->INACTIVE:LX/GGB;

    if-ne v0, v2, :cond_4

    :cond_9
    const/4 v3, 0x1

    goto :goto_2
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 2328600
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    .line 2328601
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->af:LX/0h5;

    instance-of v0, v0, LX/63L;

    if-eqz v0, :cond_0

    .line 2328602
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->af:LX/0h5;

    check-cast v0, LX/63L;

    invoke-virtual {v0, p1}, LX/63L;->a(Landroid/view/Menu;)V

    .line 2328603
    :cond_0
    return v1
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x22

    const v1, -0x2118b1ed

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2328604
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2328605
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->U:Z

    .line 2328606
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->b()V

    .line 2328607
    iput-object v3, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->J:LX/GGc;

    .line 2328608
    iput-object v3, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->K:LX/0Px;

    .line 2328609
    iput-object v3, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->X:Landroid/content/Intent;

    .line 2328610
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->s:LX/1B1;

    if-eqz v1, :cond_0

    .line 2328611
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->s:LX/1B1;

    iget-object v2, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->q:LX/GF4;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2328612
    iput-object v3, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->s:LX/1B1;

    .line 2328613
    :cond_0
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    invoke-virtual {v1}, LX/GCE;->f()V

    .line 2328614
    const/16 v1, 0x23

    const v2, -0x38c7baed

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 2328615
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 2328616
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->onBackPressed()V

    .line 2328617
    const/4 v0, 0x1

    .line 2328618
    :goto_0
    return v0

    .line 2328619
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->af:LX/0h5;

    instance-of v0, v0, LX/63L;

    if-eqz v0, :cond_1

    .line 2328620
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->af:LX/0h5;

    check-cast v0, LX/63L;

    invoke-virtual {v0, p1}, LX/63L;->a(Landroid/view/MenuItem;)Z

    .line 2328621
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x19d598d0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2328622
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2328623
    invoke-static {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->C(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)V

    .line 2328624
    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->p()V

    .line 2328625
    const/16 v1, 0x23

    const v2, 0x385d2b48

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2328626
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 2328627
    const-string v0, "ad_interfaces_flow_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->V:Ljava/lang/String;

    .line 2328628
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->V:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2328629
    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->r()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->V:Ljava/lang/String;

    .line 2328630
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    .line 2328631
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2328632
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->V:Ljava/lang/String;

    .line 2328633
    iput-object v1, v0, LX/GG3;->g:Ljava/lang/String;

    .line 2328634
    iput-object p1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->O:Landroid/os/Bundle;

    .line 2328635
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x7e672b78

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2328649
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2328650
    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->r()Ljava/lang/String;

    .line 2328651
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->O:Landroid/os/Bundle;

    invoke-static {p0, v1}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->d(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;Landroid/os/Bundle;)V

    .line 2328652
    invoke-direct {p0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->o()V

    .line 2328653
    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->F:LX/16I;

    invoke-virtual {v1}, LX/16I;->a()Z

    move-result v1

    invoke-static {p0, v1}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->b(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;Z)V

    .line 2328654
    const/16 v1, 0x23

    const v2, 0x194e400d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2328655
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2328656
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->K:LX/0Px;

    if-eqz v0, :cond_0

    .line 2328657
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->K:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->K:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GEW;

    .line 2328658
    invoke-direct {p0, v0, p1}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->b(LX/GEW;Landroid/os/Bundle;)Z

    .line 2328659
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2328660
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->B:LX/BXo;

    const-string v1, "adinterfaces_objective"

    .line 2328661
    iget-object v2, v0, LX/BXo;->b:Lcom/facebook/widget/scrollview/LockableScrollView;

    if-nez v2, :cond_1

    const/4 v2, 0x0

    .line 2328662
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".scrollViewId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2328663
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2328664
    iget-object v2, v0, LX/BXo;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 2328665
    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2328666
    :cond_1
    iget-object v2, v0, LX/BXo;->b:Lcom/facebook/widget/scrollview/LockableScrollView;

    invoke-virtual {v2}, Lcom/facebook/widget/scrollview/LockableScrollView;->getId()I

    move-result v2

    goto :goto_1

    .line 2328667
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ".lockingViewIds"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2328668
    const-string v0, "ad_interfaces_flow_id"

    iget-object v1, p0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->V:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2328669
    return-void
.end method
