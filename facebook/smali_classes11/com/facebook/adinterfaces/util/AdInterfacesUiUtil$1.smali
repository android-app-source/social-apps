.class public final Lcom/facebook/adinterfaces/util/AdInterfacesUiUtil$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/widget/ScrollView;

.field public final synthetic b:I

.field public final synthetic c:Landroid/view/View;

.field public final synthetic d:I

.field public final synthetic e:LX/GJR;


# direct methods
.method public constructor <init>(Landroid/widget/ScrollView;ILandroid/view/View;ILX/GJR;)V
    .locals 0

    .prologue
    .line 2345298
    iput-object p1, p0, Lcom/facebook/adinterfaces/util/AdInterfacesUiUtil$1;->a:Landroid/widget/ScrollView;

    iput p2, p0, Lcom/facebook/adinterfaces/util/AdInterfacesUiUtil$1;->b:I

    iput-object p3, p0, Lcom/facebook/adinterfaces/util/AdInterfacesUiUtil$1;->c:Landroid/view/View;

    iput p4, p0, Lcom/facebook/adinterfaces/util/AdInterfacesUiUtil$1;->d:I

    iput-object p5, p0, Lcom/facebook/adinterfaces/util/AdInterfacesUiUtil$1;->e:LX/GJR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2345299
    iget-object v0, p0, Lcom/facebook/adinterfaces/util/AdInterfacesUiUtil$1;->a:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    .line 2345300
    iget v1, p0, Lcom/facebook/adinterfaces/util/AdInterfacesUiUtil$1;->b:I

    iget-object v2, p0, Lcom/facebook/adinterfaces/util/AdInterfacesUiUtil$1;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, p0, Lcom/facebook/adinterfaces/util/AdInterfacesUiUtil$1;->d:I

    sub-int/2addr v1, v2

    .line 2345301
    if-ne v0, v1, :cond_1

    .line 2345302
    iget-object v0, p0, Lcom/facebook/adinterfaces/util/AdInterfacesUiUtil$1;->e:LX/GJR;

    if-eqz v0, :cond_0

    .line 2345303
    iget-object v0, p0, Lcom/facebook/adinterfaces/util/AdInterfacesUiUtil$1;->e:LX/GJR;

    invoke-interface {v0}, LX/GJR;->a()V

    .line 2345304
    :cond_0
    :goto_0
    return-void

    .line 2345305
    :cond_1
    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput v0, v2, v3

    const/4 v0, 0x1

    aput v1, v2, v0

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 2345306
    new-instance v2, LX/GMn;

    invoke-direct {v2, p0, v1}, LX/GMn;-><init>(Lcom/facebook/adinterfaces/util/AdInterfacesUiUtil$1;I)V

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2345307
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method
