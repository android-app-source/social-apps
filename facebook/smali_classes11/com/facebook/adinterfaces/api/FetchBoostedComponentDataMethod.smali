.class public abstract Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D:",
        "Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# instance fields
.field private final a:LX/0tX;

.field private final b:LX/0rq;

.field private final c:LX/0sa;

.field public final d:LX/GF4;

.field private final e:LX/1Ck;

.field public final f:LX/GG6;

.field private final g:LX/0ad;


# direct methods
.method public constructor <init>(LX/0tX;LX/0rq;LX/0sa;LX/GF4;LX/1Ck;LX/GG6;LX/0ad;)V
    .locals 0

    .prologue
    .line 2330767
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2330768
    iput-object p1, p0, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->a:LX/0tX;

    .line 2330769
    iput-object p2, p0, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->b:LX/0rq;

    .line 2330770
    iput-object p3, p0, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->c:LX/0sa;

    .line 2330771
    iput-object p4, p0, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->d:LX/GF4;

    .line 2330772
    iput-object p5, p0, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->e:LX/1Ck;

    .line 2330773
    iput-object p6, p0, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->f:LX/GG6;

    .line 2330774
    iput-object p7, p0, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->g:LX/0ad;

    .line 2330775
    return-void
.end method

.method public static a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;)LX/0Px;
    .locals 1
    .param p0    # Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLCallToActionType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2330764
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2330765
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2330766
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->j()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;DLX/0W9;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2330744
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v1

    .line 2330745
    if-nez v1, :cond_0

    .line 2330746
    const/4 v0, 0x0

    .line 2330747
    :goto_0
    return-object v0

    .line 2330748
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, ""

    .line 2330749
    :goto_1
    new-instance v2, LX/A9E;

    invoke-direct {v2}, LX/A9E;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;->a()D

    move-result-wide v4

    .line 2330750
    iput-wide v4, v2, LX/A9E;->f:D

    .line 2330751
    move-object v2, v2

    .line 2330752
    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;->b()D

    move-result-wide v4

    .line 2330753
    iput-wide v4, v2, LX/A9E;->h:D

    .line 2330754
    move-object v1, v2

    .line 2330755
    iput-object v0, v1, LX/A9E;->a:Ljava/lang/String;

    .line 2330756
    move-object v0, v1

    .line 2330757
    iput-wide p1, v0, LX/A9E;->j:D

    .line 2330758
    move-object v0, v0

    .line 2330759
    invoke-static {p3}, LX/GNL;->a(LX/0W9;)Ljava/lang/String;

    move-result-object v1

    .line 2330760
    iput-object v1, v0, LX/A9E;->d:Ljava/lang/String;

    .line 2330761
    move-object v0, v0

    .line 2330762
    invoke-virtual {v0}, LX/A9E;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v0

    goto :goto_0

    .line 2330763
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public a(LX/8wL;Ljava/lang/String;Ljava/lang/String;Z)LX/AAj;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2330742
    new-instance v0, LX/AAj;

    invoke-direct {v0}, LX/AAj;-><init>()V

    move-object v0, v0

    .line 2330743
    const-string v3, "page_id"

    invoke-virtual {v0, v3, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "is_promote_website"

    sget-object v0, LX/8wL;->PROMOTE_WEBSITE:LX/8wL;

    if-ne p1, v0, :cond_4

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v3

    const-string v4, "is_page_like"

    sget-object v0, LX/8wL;->PAGE_LIKE:LX/8wL;

    if-ne p1, v0, :cond_5

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v3

    const-string v4, "is_local_awareness"

    sget-object v0, LX/8wL;->LOCAL_AWARENESS:LX/8wL;

    if-ne p1, v0, :cond_6

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v3

    const-string v4, "is_promote_product"

    sget-object v0, LX/8wL;->PROMOTE_PRODUCT:LX/8wL;

    if-ne p1, v0, :cond_7

    move v0, v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v3

    const-string v4, "is_boosted_post"

    sget-object v0, LX/8wL;->BOOST_POST:LX/8wL;

    if-ne p1, v0, :cond_8

    move v0, v1

    :goto_4
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v3

    const-string v4, "is_local_targeting"

    sget-object v0, LX/8wL;->LOCAL_AWARENESS:LX/8wL;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/8wL;->PAGE_LIKE:LX/8wL;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/8wL;->PROMOTE_WEBSITE:LX/8wL;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/8wL;->PROMOTE_CTA:LX/8wL;

    if-ne p1, v0, :cond_9

    :cond_0
    move v0, v1

    :goto_5
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v3, "product_id"

    invoke-virtual {v0, v3, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "fetch_saved_audiences"

    sget-object v0, LX/8wL;->BOOST_POST:LX/8wL;

    if-ne p1, v0, :cond_a

    move v0, v1

    :goto_6
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v3

    const-string v4, "is_cta"

    sget-object v0, LX/8wL;->PROMOTE_CTA:LX/8wL;

    if-ne p1, v0, :cond_b

    move v0, v1

    :goto_7
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v3, "component_app"

    invoke-virtual {p1}, LX/8wL;->getComponentAppEnum()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v3, "res_size"

    iget-object v4, p0, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->c:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->s()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v3, "cover_photo_size"

    iget-object v4, p0, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->c:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->s()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v3, "force_saved_settings"

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v3, "image_large_aspect_height"

    iget-object v4, p0, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->c:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->A()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v3, "image_large_aspect_width"

    iget-object v4, p0, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->c:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->z()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "fetch_websites"

    sget-object v0, LX/8wL;->LOCAL_AWARENESS:LX/8wL;

    if-eq p1, v0, :cond_1

    sget-object v0, LX/8wL;->PROMOTE_WEBSITE:LX/8wL;

    if-ne p1, v0, :cond_c

    :cond_1
    move v0, v1

    :goto_8
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v3, "max_budgets_count"

    sget-object v4, LX/GE2;->b:Ljava/lang/Integer;

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v3, "media_type"

    iget-object v4, p0, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->b:LX/0rq;

    invoke-virtual {v4}, LX/0rq;->b()LX/0wF;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    const-string v3, "audience_count"

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v3, "fetch_flexible_spec"

    iget-object v4, p0, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->g:LX/0ad;

    sget-short v5, LX/GDK;->d:S

    invoke-interface {v4, v5, v2}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v3, "include_call_to_action_types"

    sget-object v4, LX/8wL;->BOOST_POST:LX/8wL;

    if-eq p1, v4, :cond_2

    sget-object v4, LX/8wL;->LOCAL_AWARENESS:LX/8wL;

    if-ne p1, v4, :cond_3

    :cond_2
    move v2, v1

    :cond_3
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/AAj;

    return-object v0

    :cond_4
    move v0, v2

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto/16 :goto_1

    :cond_6
    move v0, v2

    goto/16 :goto_2

    :cond_7
    move v0, v2

    goto/16 :goto_3

    :cond_8
    move v0, v2

    goto/16 :goto_4

    :cond_9
    move v0, v2

    goto/16 :goto_5

    :cond_a
    move v0, v2

    goto/16 :goto_6

    :cond_b
    move v0, v2

    goto/16 :goto_7

    :cond_c
    move v0, v2

    goto :goto_8
.end method

.method public abstract a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;Ljava/lang/String;)Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;",
            "Ljava/lang/String;",
            ")TD;"
        }
    .end annotation
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public final a(Ljava/lang/String;LX/8wL;LX/GCY;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2330776
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2330777
    invoke-virtual {p0, p2, p1, p5, p6}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->a(LX/8wL;Ljava/lang/String;Ljava/lang/String;Z)LX/AAj;

    move-result-object v0

    .line 2330778
    iget-object v1, p0, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->e:LX/1Ck;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->a:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    .line 2330779
    iput-object v4, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2330780
    move-object v0, v0

    .line 2330781
    invoke-virtual {v3, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v3, LX/GDz;

    invoke-direct {v3, p0, p3, p1, p4}, LX/GDz;-><init>(Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;LX/GCY;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2330782
    return-void
.end method

.method public final a(Ljava/lang/String;LX/8wL;Ljava/lang/String;LX/GCY;Z)V
    .locals 7
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2330740
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p3

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->a(Ljava/lang/String;LX/8wL;LX/GCY;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2330741
    return-void
.end method

.method public abstract b(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public final c(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)LX/GGB;
    .locals 1

    .prologue
    .line 2330738
    invoke-virtual {p0, p1}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->b(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v0

    .line 2330739
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->m()Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    move-result-object v0

    invoke-static {v0}, LX/GG6;->a(Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;)LX/GGB;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    goto :goto_0
.end method

.method public final d(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2330724
    invoke-virtual {p0, p1}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->b(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v1

    .line 2330725
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2330726
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->k()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2330727
    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-eqz v0, :cond_1

    .line 2330728
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2330729
    invoke-static {v1, v0}, LX/GG6;->a(LX/15i;I)LX/0Px;

    move-result-object v0

    .line 2330730
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2330731
    invoke-virtual {p0, p1}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->b(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v2

    .line 2330732
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2330733
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2330734
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 2330735
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v1}, LX/15i;->j(II)I

    move-result v1

    .line 2330736
    :cond_0
    return v1

    :cond_1
    move v0, v1

    .line 2330737
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
