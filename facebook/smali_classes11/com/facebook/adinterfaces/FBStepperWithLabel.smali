.class public Lcom/facebook/adinterfaces/FBStepperWithLabel;
.super Landroid/support/v7/widget/LinearLayoutCompat;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lcom/facebook/resources/ui/FbTextView;

.field private b:Lcom/facebook/fbui/glyph/GlyphView;

.field private c:Lcom/facebook/fbui/glyph/GlyphView;

.field private d:LX/GCu;

.field private e:I

.field public f:I

.field public g:I

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2329271
    invoke-direct {p0, p1, v1}, Landroid/support/v7/widget/LinearLayoutCompat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2329272
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    .line 2329273
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->f:I

    .line 2329274
    const v0, 0x7fffffff

    iput v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->g:I

    .line 2329275
    invoke-direct {p0, v1, v1}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2329276
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2329277
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/LinearLayoutCompat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2329278
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    .line 2329279
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->f:I

    .line 2329280
    const v0, 0x7fffffff

    iput v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->g:I

    .line 2329281
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2329282
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2329283
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/LinearLayoutCompat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2329284
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    .line 2329285
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->f:I

    .line 2329286
    const v0, 0x7fffffff

    iput v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->g:I

    .line 2329287
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2329288
    return-void
.end method

.method private a(III)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2329289
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->b(III)V

    .line 2329290
    iput p1, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    .line 2329291
    iput p2, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->f:I

    .line 2329292
    iput p3, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->g:I

    .line 2329293
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->getStepperCallback()LX/GCu;

    move-result-object v3

    iget v4, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    invoke-interface {v3, v4}, LX/GCu;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2329294
    iget-object v3, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->b:Lcom/facebook/fbui/glyph/GlyphView;

    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    iget v4, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->g:I

    if-ge v0, v4, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setEnabled(Z)V

    .line 2329295
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->c:Lcom/facebook/fbui/glyph/GlyphView;

    iget v3, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    iget v4, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->f:I

    if-le v3, v4, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setEnabled(Z)V

    .line 2329296
    return-void

    :cond_0
    move v0, v2

    .line 2329297
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2329298
    goto :goto_1
.end method

.method private a(IILandroid/content/res/ColorStateList;)V
    .locals 1

    .prologue
    .line 2329299
    if-eqz p1, :cond_0

    .line 2329300
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2329301
    :cond_0
    if-eqz p2, :cond_1

    .line 2329302
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2329303
    :cond_1
    if-eqz p3, :cond_2

    .line 2329304
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p3}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 2329305
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p3}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 2329306
    :cond_2
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 2329350
    const v0, 0x7f0313c0

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->setContentView(I)V

    .line 2329351
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/LinearLayoutCompat;->setOrientation(I)V

    .line 2329352
    const v0, 0x7f0d2d95

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 2329353
    const v0, 0x7f0d2d97

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2329354
    const v0, 0x7f0d2d96

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->c:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2329355
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->b(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2329356
    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    iget v1, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->f:I

    iget v2, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->g:I

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->b(III)V

    .line 2329357
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p0}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2329358
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p0}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2329359
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->getStepperCallback()LX/GCu;

    move-result-object v1

    iget v2, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    invoke-interface {v1, v2}, LX/GCu;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2329360
    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->h:I

    if-eqz v0, :cond_0

    .line 2329361
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->h:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2329362
    :cond_0
    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->i:I

    if-eqz v0, :cond_1

    .line 2329363
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->i:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2329364
    :cond_1
    return-void
.end method

.method private b()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2329307
    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    iget v2, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->g:I

    if-ne v0, v2, :cond_0

    .line 2329308
    invoke-direct {p0}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->getStepperCallback()LX/GCu;

    .line 2329309
    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    .line 2329310
    :goto_0
    return v0

    .line 2329311
    :cond_0
    iget-object v2, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->c:Lcom/facebook/fbui/glyph/GlyphView;

    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    iget v3, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->f:I

    if-lt v0, v3, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setEnabled(Z)V

    .line 2329312
    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    iget v2, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->g:I

    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_1

    .line 2329313
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setEnabled(Z)V

    .line 2329314
    :cond_1
    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    .line 2329315
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->getStepperCallback()LX/GCu;

    move-result-object v1

    iget v2, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    invoke-interface {v1, v2}, LX/GCu;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2329316
    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2329317
    goto :goto_1
.end method

.method private b(III)V
    .locals 5

    .prologue
    .line 2329318
    if-lt p1, p2, :cond_0

    if-le p1, p3, :cond_1

    .line 2329319
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "step %d is our of bounds [%d,%d]"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 2329320
    iget v3, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->f:I

    move v3, v3

    .line 2329321
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 2329322
    iget v4, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->g:I

    move v4, v4

    .line 2329323
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2329324
    :cond_1
    return-void
.end method

.method private b(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2329325
    if-nez p2, :cond_0

    .line 2329326
    :goto_0
    return-void

    .line 2329327
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->StepperWithLabel:[I

    invoke-virtual {v0, p2, v1, v4, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2329328
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    .line 2329329
    const/16 v1, 0x0

    const/high16 v2, -0x80000000

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->f:I

    .line 2329330
    const/16 v1, 0x1

    const v2, 0x7fffffff

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->g:I

    .line 2329331
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    const/16 v2, 0x4

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    const/16 v3, 0x5

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->a(IILandroid/content/res/ColorStateList;)V

    .line 2329332
    const/16 v1, 0x6

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 2329333
    if-eqz v1, :cond_1

    .line 2329334
    iget-object v2, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2329335
    :cond_1
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->h:I

    .line 2329336
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->i:I

    .line 2329337
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method

.method private c()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2329338
    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    iget v2, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->f:I

    if-ne v0, v2, :cond_0

    .line 2329339
    invoke-direct {p0}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->getStepperCallback()LX/GCu;

    .line 2329340
    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    .line 2329341
    :goto_0
    return v0

    .line 2329342
    :cond_0
    iget-object v2, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->b:Lcom/facebook/fbui/glyph/GlyphView;

    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    iget v3, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->g:I

    if-gt v0, v3, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setEnabled(Z)V

    .line 2329343
    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    iget v2, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->f:I

    add-int/lit8 v2, v2, 0x1

    if-ne v0, v2, :cond_1

    .line 2329344
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setEnabled(Z)V

    .line 2329345
    :cond_1
    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    .line 2329346
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->getStepperCallback()LX/GCu;

    move-result-object v1

    iget v2, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    invoke-interface {v1, v2}, LX/GCu;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2329347
    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2329348
    goto :goto_1
.end method

.method private getDefaultStepperCallback()LX/GCu;
    .locals 1

    .prologue
    .line 2329349
    new-instance v0, LX/GCv;

    invoke-direct {v0, p0}, LX/GCv;-><init>(Lcom/facebook/adinterfaces/FBStepperWithLabel;)V

    return-object v0
.end method

.method private getStepperCallback()LX/GCu;
    .locals 1

    .prologue
    .line 2329267
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->d:LX/GCu;

    if-nez v0, :cond_0

    .line 2329268
    invoke-direct {p0}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->getDefaultStepperCallback()LX/GCu;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->d:LX/GCu;

    .line 2329269
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->d:LX/GCu;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 2329270
    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    iget v1, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->g:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStep()I
    .locals 1

    .prologue
    .line 2329231
    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    return v0
.end method

.method public getStepMax()I
    .locals 1

    .prologue
    .line 2329232
    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->g:I

    return v0
.end method

.method public getStepMin()I
    .locals 1

    .prologue
    .line 2329233
    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->f:I

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x3d01d227

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2329234
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0d2d97

    if-ne v1, v2, :cond_0

    .line 2329235
    invoke-direct {p0}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->b()I

    .line 2329236
    const v1, 0x3a9d5d97

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2329237
    :goto_0
    return-void

    .line 2329238
    :cond_0
    invoke-direct {p0}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->c()I

    .line 2329239
    const v1, 0x56d79d16

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setContentView(I)V
    .locals 1

    .prologue
    .line 2329240
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2329241
    invoke-virtual {v0, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_1

    .line 2329242
    :goto_0
    return-void

    .line 2329243
    :catch_0
    move-exception v0

    .line 2329244
    invoke-static {p0, p1, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V

    goto :goto_0

    .line 2329245
    :catch_1
    move-exception v0

    .line 2329246
    invoke-static {p0, p1, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V

    goto :goto_0
.end method

.method public setDecrementImageResource(I)V
    .locals 1

    .prologue
    .line 2329247
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2329248
    return-void
.end method

.method public setIncrementImageResource(I)V
    .locals 1

    .prologue
    .line 2329249
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2329250
    return-void
.end method

.method public setLabelTextColor(I)V
    .locals 1

    .prologue
    .line 2329251
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2329252
    return-void
.end method

.method public setStep(I)V
    .locals 2

    .prologue
    .line 2329253
    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->f:I

    move v0, v0

    .line 2329254
    iget v1, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->g:I

    move v1, v1

    .line 2329255
    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->a(III)V

    .line 2329256
    return-void
.end method

.method public setStepMax(I)V
    .locals 3

    .prologue
    .line 2329257
    iput p1, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->g:I

    .line 2329258
    iget v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    iget v1, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->g:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2329259
    iget v1, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->f:I

    iget v2, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->g:I

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->a(III)V

    .line 2329260
    return-void
.end method

.method public setStepperCallback(LX/GCu;)V
    .locals 3

    .prologue
    .line 2329261
    iput-object p1, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->d:LX/GCu;

    .line 2329262
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->getStepperCallback()LX/GCu;

    move-result-object v1

    iget v2, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->e:I

    invoke-interface {v1, v2}, LX/GCu;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2329263
    return-void
.end method

.method public setStepperControlColorStateList(Landroid/content/res/ColorStateList;)V
    .locals 1

    .prologue
    .line 2329264
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 2329265
    iget-object v0, p0, Lcom/facebook/adinterfaces/FBStepperWithLabel;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 2329266
    return-void
.end method
