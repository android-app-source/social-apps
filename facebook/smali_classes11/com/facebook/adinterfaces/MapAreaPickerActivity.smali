.class public Lcom/facebook/adinterfaces/MapAreaPickerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/67w;
.implements LX/68J;


# instance fields
.field public A:Lcom/facebook/android/maps/SupportMapFragment;

.field public B:Landroid/widget/TextView;

.field private C:Landroid/widget/TextView;

.field private D:Landroid/view/View;

.field public E:Ljava/lang/String;

.field public F:F

.field public G:D

.field public H:D

.field public I:Lcom/facebook/android/maps/model/LatLng;

.field public J:Z

.field public K:Landroid/widget/ProgressBar;

.field private L:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

.field private M:Ljava/lang/String;

.field public N:Landroid/text/SpannableString;

.field public O:LX/63L;

.field private P:Z

.field public Q:D

.field public R:I

.field private S:LX/5OM;

.field private T:Landroid/view/View;

.field public U:Ljava/lang/String;

.field public p:LX/GG6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/GNE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/GEO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/67X;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/01T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/63V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/GCB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:Lcom/facebook/adinterfaces/ui/MapSpinnerView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2329617
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/adinterfaces/MapAreaPickerActivity;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2329597
    invoke-virtual {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;
    .locals 3

    .prologue
    .line 2329598
    new-instance v0, LX/GGE;

    invoke-direct {v0}, LX/GGE;-><init>()V

    const/16 v1, 0xd

    .line 2329599
    iput v1, v0, LX/GGE;->b:I

    .line 2329600
    move-object v0, v0

    .line 2329601
    const/16 v1, 0x41

    .line 2329602
    iput v1, v0, LX/GGE;->c:I

    .line 2329603
    move-object v0, v0

    .line 2329604
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->ALL:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    .line 2329605
    iput-object v1, v0, LX/GGE;->a:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    .line 2329606
    move-object v0, v0

    .line 2329607
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v1, v1

    .line 2329608
    iput-object v1, v0, LX/GGE;->j:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2329609
    move-object v0, v0

    .line 2329610
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    move-object v1, v1

    .line 2329611
    iput-object v1, v0, LX/GGE;->k:LX/GGG;

    .line 2329612
    move-object v0, v0

    .line 2329613
    sget-object v1, LX/GGF;->HOME:LX/GGF;

    sget-object v2, LX/GGF;->RECENT:LX/GGF;

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2329614
    iput-object v1, v0, LX/GGE;->h:LX/0Px;

    .line 2329615
    move-object v0, v0

    .line 2329616
    invoke-virtual {v0}, LX/GGE;->a()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/5OG;)V
    .locals 2

    .prologue
    .line 2329573
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0a1c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/5OG;->a(Landroid/content/res/ColorStateList;)V

    .line 2329574
    const v0, 0x7f080ae3

    invoke-virtual {p1, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    .line 2329575
    const v1, 0x7f020964

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2329576
    new-instance v1, LX/GCz;

    invoke-direct {v1, p0}, LX/GCz;-><init>(Lcom/facebook/adinterfaces/MapAreaPickerActivity;)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2329577
    const v0, 0x7f080b18

    invoke-virtual {p1, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    .line 2329578
    const v1, 0x7f020952

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2329579
    new-instance v1, LX/GD0;

    invoke-direct {v1, p0}, LX/GD0;-><init>(Lcom/facebook/adinterfaces/MapAreaPickerActivity;)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2329580
    const v0, 0x7f080b19

    invoke-virtual {p1, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    .line 2329581
    const v1, 0x7f020995

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2329582
    new-instance v1, LX/GD1;

    invoke-direct {v1, p0}, LX/GD1;-><init>(Lcom/facebook/adinterfaces/MapAreaPickerActivity;)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2329583
    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/MapAreaPickerActivity;LX/GG6;LX/GNE;LX/1Ck;LX/GEO;LX/0W9;LX/67X;LX/01T;LX/63V;Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;LX/GCB;)V
    .locals 0

    .prologue
    .line 2329618
    iput-object p1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->p:LX/GG6;

    iput-object p2, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->q:LX/GNE;

    iput-object p3, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->r:LX/1Ck;

    iput-object p4, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->s:LX/GEO;

    iput-object p5, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->t:LX/0W9;

    iput-object p6, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->u:LX/67X;

    iput-object p7, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->v:LX/01T;

    iput-object p8, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->w:LX/63V;

    iput-object p9, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->x:Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

    iput-object p10, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->y:LX/GCB;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 11

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v10

    move-object v0, p0

    check-cast v0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    invoke-static {v10}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v1

    check-cast v1, LX/GG6;

    invoke-static {v10}, LX/GNE;->a(LX/0QB;)LX/GNE;

    move-result-object v2

    check-cast v2, LX/GNE;

    invoke-static {v10}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v10}, LX/GEO;->a(LX/0QB;)LX/GEO;

    move-result-object v4

    check-cast v4, LX/GEO;

    invoke-static {v10}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v5

    check-cast v5, LX/0W9;

    invoke-static {v10}, LX/67X;->b(LX/0QB;)LX/67X;

    move-result-object v6

    check-cast v6, LX/67X;

    invoke-static {v10}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v7

    check-cast v7, LX/01T;

    invoke-static {v10}, LX/63V;->a(LX/0QB;)LX/63V;

    move-result-object v8

    check-cast v8, LX/63V;

    invoke-static {v10}, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->b(LX/0QB;)Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

    invoke-static {v10}, LX/GCB;->b(LX/0QB;)LX/GCB;

    move-result-object v10

    check-cast v10, LX/GCB;

    invoke-static/range {v0 .. v10}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->a(Lcom/facebook/adinterfaces/MapAreaPickerActivity;LX/GG6;LX/GNE;LX/1Ck;LX/GEO;LX/0W9;LX/67X;LX/01T;LX/63V;Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;LX/GCB;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/adinterfaces/MapAreaPickerActivity;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;I)V
    .locals 2
    .param p0    # Lcom/facebook/adinterfaces/MapAreaPickerActivity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2329619
    if-nez p1, :cond_0

    .line 2329620
    invoke-direct {p0, p2}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->b(I)V

    .line 2329621
    :goto_0
    return-void

    .line 2329622
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->A:Lcom/facebook/android/maps/SupportMapFragment;

    new-instance v1, LX/GDD;

    invoke-direct {v1, p0, p1}, LX/GDD;-><init>(Lcom/facebook/adinterfaces/MapAreaPickerActivity;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;)V

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/SupportMapFragment;->a(LX/68J;)V

    goto :goto_0
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 2329691
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->z:Lcom/facebook/adinterfaces/ui/MapSpinnerView;

    sget-object v1, LX/GMY;->IDLE:LX/GMY;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->setState(LX/GMY;)V

    .line 2329692
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2329693
    return-void
.end method

.method public static b(Lcom/facebook/adinterfaces/MapAreaPickerActivity;LX/680;)V
    .locals 8

    .prologue
    .line 2329623
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->B:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2329624
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->K:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2329625
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->q:LX/GNE;

    .line 2329626
    iget-object v1, p1, LX/680;->k:LX/31h;

    move-object v1, v1

    .line 2329627
    iget-object v2, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->z:Lcom/facebook/adinterfaces/ui/MapSpinnerView;

    invoke-virtual {v0, v1, v2}, LX/GNE;->a(LX/31h;Landroid/view/View;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->Q:D

    .line 2329628
    new-instance v0, LX/A9E;

    invoke-direct {v0}, LX/A9E;-><init>()V

    iget-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->t:LX/0W9;

    invoke-static {v1}, LX/GNL;->a(LX/0W9;)Ljava/lang/String;

    move-result-object v1

    .line 2329629
    iput-object v1, v0, LX/A9E;->d:Ljava/lang/String;

    .line 2329630
    move-object v0, v0

    .line 2329631
    iget-wide v2, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->G:D

    .line 2329632
    iput-wide v2, v0, LX/A9E;->f:D

    .line 2329633
    move-object v0, v0

    .line 2329634
    iget-wide v2, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->H:D

    .line 2329635
    iput-wide v2, v0, LX/A9E;->h:D

    .line 2329636
    move-object v0, v0

    .line 2329637
    iget-wide v2, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->Q:D

    .line 2329638
    iput-wide v2, v0, LX/A9E;->j:D

    .line 2329639
    move-object v0, v0

    .line 2329640
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->CUSTOM_LOCATION:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    .line 2329641
    iput-object v1, v0, LX/A9E;->g:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    .line 2329642
    move-object v0, v0

    .line 2329643
    invoke-virtual {v0}, LX/A9E;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v0

    .line 2329644
    iget-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->L:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    .line 2329645
    iput-object v0, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2329646
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->L:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    invoke-static {v0}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2329647
    iget-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->s:LX/GEO;

    iget-object v2, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->M:Ljava/lang/String;

    new-instance v3, LX/GD8;

    invoke-direct {v3, p0}, LX/GD8;-><init>(Lcom/facebook/adinterfaces/MapAreaPickerActivity;)V

    .line 2329648
    new-instance v4, LX/ACF;

    invoke-direct {v4}, LX/ACF;-><init>()V

    move-object v4, v4

    .line 2329649
    const-string v5, "account_id"

    invoke-virtual {v4, v5, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "bid_for"

    const-string v6, "IMPRESSION"

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "targeting_spec"

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->a(ZZ)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    check-cast v4, LX/ACF;

    .line 2329650
    iget-object v5, v1, LX/GEO;->b:LX/1Ck;

    sget-object v6, LX/GEN;->REACH_TASK:LX/GEN;

    iget-object v7, v1, LX/GEO;->a:LX/0tX;

    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    invoke-virtual {v7, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    new-instance v7, LX/GEM;

    invoke-direct {v7, v1, v3}, LX/GEM;-><init>(LX/GEO;LX/GD8;)V

    invoke-virtual {v5, v6, v4, v7}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2329651
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->C:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->q:LX/GNE;

    .line 2329652
    iget-object v2, p1, LX/680;->k:LX/31h;

    move-object v2, v2

    .line 2329653
    iget-object v3, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->z:Lcom/facebook/adinterfaces/ui/MapSpinnerView;

    .line 2329654
    iget-object v4, v1, LX/GNE;->e:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 2329655
    iget-boolean v4, v1, LX/GNE;->d:Z

    if-eqz v4, :cond_1

    const v4, 0x7f080a94

    .line 2329656
    :goto_0
    iget-object v5, v1, LX/GNE;->c:Landroid/content/res/Resources;

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, LX/GNE;->e:Ljava/lang/String;

    .line 2329657
    :cond_0
    invoke-virtual {v1, v2, v3}, LX/GNE;->a(LX/31h;Landroid/view/View;)D

    move-result-wide v4

    .line 2329658
    iget-object v6, v1, LX/GNE;->e:Ljava/lang/String;

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-static {v6, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    move-object v1, v4

    .line 2329659
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2329660
    return-void

    .line 2329661
    :cond_1
    const v4, 0x7f080a95

    goto :goto_0
.end method

.method private d(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2329662
    const-string v0, "radius_extra"

    iget-wide v2, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->Q:D

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 2329663
    const-string v0, "location_extra"

    iget-wide v2, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->G:D

    iget-wide v4, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->H:D

    invoke-static {v2, v3, v4, v5}, LX/6aA;->a(DD)Landroid/location/Location;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2329664
    const-string v0, "target_spec_extra"

    iget-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->L:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2329665
    const-string v0, "ad_account_id_extra"

    iget-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->M:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2329666
    return-void
.end method

.method private e(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2329667
    const-string v0, "radius_extra"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->Q:D

    .line 2329668
    const-string v0, "location_extra"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 2329669
    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->G:D

    .line 2329670
    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->H:D

    .line 2329671
    const-string v0, "target_spec_extra"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    iput-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->L:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    .line 2329672
    const-string v0, "ad_account_id_extra"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->M:Ljava/lang/String;

    .line 2329673
    const-string v0, "page_location_extra"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/model/LatLng;

    iput-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->I:Lcom/facebook/android/maps/model/LatLng;

    .line 2329674
    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 2329675
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->z:Lcom/facebook/adinterfaces/ui/MapSpinnerView;

    sget-object v1, LX/GMY;->LOADING:LX/GMY;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->setState(LX/GMY;)V

    .line 2329676
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->x:Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

    new-instance v1, LX/GDC;

    invoke-direct {v1, p0}, LX/GDC;-><init>(Lcom/facebook/adinterfaces/MapAreaPickerActivity;)V

    invoke-virtual {v0, v1, p0}, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->a(LX/GDA;Landroid/app/Activity;)V

    .line 2329677
    return-void
.end method

.method private o()V
    .locals 2

    .prologue
    .line 2329678
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->v:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_1

    .line 2329679
    invoke-direct {p0}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->p()V

    .line 2329680
    :cond_0
    :goto_0
    return-void

    .line 2329681
    :cond_1
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->v:LX/01T;

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_0

    .line 2329682
    invoke-direct {p0}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->q()V

    goto :goto_0
.end method

.method private p()V
    .locals 2

    .prologue
    .line 2329683
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->u:LX/67X;

    invoke-virtual {v0}, LX/67X;->g()LX/3u1;

    move-result-object v0

    .line 2329684
    new-instance v1, LX/63L;

    invoke-direct {v1, p0, v0}, LX/63L;-><init>(LX/0ew;LX/3u1;)V

    iput-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->O:LX/63L;

    .line 2329685
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->O:LX/63L;

    const v1, 0x7f080af7

    invoke-virtual {v0, v1}, LX/63L;->setTitle(I)V

    .line 2329686
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->O:LX/63L;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/63L;->setHasBackButton(Z)V

    .line 2329687
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->O:LX/63L;

    iget-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->y:LX/GCB;

    invoke-virtual {v1}, LX/GCB;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/63L;->setButtonSpecs(Ljava/util/List;)V

    .line 2329688
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->O:LX/63L;

    new-instance v1, LX/GDE;

    invoke-direct {v1, p0}, LX/GDE;-><init>(Lcom/facebook/adinterfaces/MapAreaPickerActivity;)V

    invoke-virtual {v0, v1}, LX/63L;->setOnToolbarButtonListener(LX/63W;)V

    .line 2329689
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->O:LX/63L;

    new-instance v1, LX/GCw;

    invoke-direct {v1, p0}, LX/GCw;-><init>(Lcom/facebook/adinterfaces/MapAreaPickerActivity;)V

    invoke-virtual {v0, v1}, LX/63L;->setOnBackPressedListener(LX/63J;)V

    .line 2329690
    return-void
.end method

.method private q()V
    .locals 2

    .prologue
    .line 2329584
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2329585
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2329586
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 2329587
    const v1, 0x7f080af7

    invoke-virtual {p0, v1}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2329588
    iget-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->y:LX/GCB;

    invoke-virtual {v1}, LX/GCB;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2329589
    new-instance v1, LX/GCx;

    invoke-direct {v1, p0}, LX/GCx;-><init>(Lcom/facebook/adinterfaces/MapAreaPickerActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2329590
    new-instance v1, LX/GCy;

    invoke-direct {v1, p0}, LX/GCy;-><init>(Lcom/facebook/adinterfaces/MapAreaPickerActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2329591
    return-void
.end method

.method public static r(Lcom/facebook/adinterfaces/MapAreaPickerActivity;)V
    .locals 3

    .prologue
    .line 2329592
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2329593
    invoke-direct {p0, v0}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->d(Landroid/os/Bundle;)V

    .line 2329594
    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 2329595
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->finish()V

    .line 2329596
    return-void
.end method

.method public static s(Lcom/facebook/adinterfaces/MapAreaPickerActivity;)V
    .locals 4

    .prologue
    .line 2329477
    new-instance v0, Landroid/widget/EditText;

    invoke-direct {v0, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 2329478
    const v1, 0x7f080ae2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 2329479
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 2329480
    iget-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->U:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2329481
    new-instance v1, LX/0ju;

    invoke-direct {v1, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v2, 0x7f080b18

    invoke-virtual {v1, v2}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080b1a

    new-instance v3, LX/GD3;

    invoke-direct {v3, p0, v0}, LX/GD3;-><init>(Lcom/facebook/adinterfaces/MapAreaPickerActivity;Landroid/widget/EditText;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080147

    new-instance v3, LX/GD2;

    invoke-direct {v3, p0, v0}, LX/GD2;-><init>(Lcom/facebook/adinterfaces/MapAreaPickerActivity;Landroid/widget/EditText;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    .line 2329482
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2329483
    invoke-virtual {v0}, LX/2EJ;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 2329484
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2329485
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2329486
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->A:Lcom/facebook/android/maps/SupportMapFragment;

    new-instance v1, LX/GD7;

    invoke-direct {v1, p0}, LX/GD7;-><init>(Lcom/facebook/adinterfaces/MapAreaPickerActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/SupportMapFragment;->a(LX/68J;)V

    .line 2329487
    return-void
.end method

.method public final a(LX/680;)V
    .locals 7

    .prologue
    .line 2329488
    new-instance v3, Lcom/facebook/android/maps/model/LatLng;

    iget-wide v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->G:D

    iget-wide v4, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->H:D

    invoke-direct {v3, v0, v1, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    .line 2329489
    iget-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->q:LX/GNE;

    iget-wide v4, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->Q:D

    iget v6, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->R:I

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, LX/GNE;->a(LX/680;Lcom/facebook/android/maps/model/LatLng;DI)F

    move-result v0

    .line 2329490
    invoke-static {v3, v0}, LX/67e;->a(Lcom/facebook/android/maps/model/LatLng;F)LX/67d;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/680;->b(LX/67d;)V

    .line 2329491
    invoke-virtual {p1}, LX/680;->b()V

    .line 2329492
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->J:Z

    .line 2329493
    invoke-virtual {p1, p0}, LX/680;->a(LX/67w;)V

    .line 2329494
    new-instance v0, LX/GD6;

    invoke-direct {v0, p0}, LX/GD6;-><init>(Lcom/facebook/adinterfaces/MapAreaPickerActivity;)V

    .line 2329495
    iput-object v0, p1, LX/680;->g:LX/67n;

    .line 2329496
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2329497
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 2329498
    invoke-static {p0, p0}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2329499
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->w:LX/63V;

    invoke-virtual {v0}, LX/63V;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->P:Z

    .line 2329500
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->P:Z

    if-eqz v0, :cond_0

    .line 2329501
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->u:LX/67X;

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    .line 2329502
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2329503
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->z:Lcom/facebook/adinterfaces/ui/MapSpinnerView;

    sget-object v1, LX/GMY;->LOADING:LX/GMY;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->setState(LX/GMY;)V

    .line 2329504
    invoke-direct {p0}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->n()V

    .line 2329505
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2329506
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2329507
    const v0, 0x7f03006c

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->setContentView(I)V

    .line 2329508
    const v0, 0x7f080a97

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->E:Ljava/lang/String;

    .line 2329509
    new-instance v0, Landroid/text/SpannableString;

    const v1, 0x7f080b0f

    invoke-virtual {p0, v1}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->N:Landroid/text/SpannableString;

    .line 2329510
    const v0, 0x7f0d0440

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->z:Lcom/facebook/adinterfaces/ui/MapSpinnerView;

    .line 2329511
    const v0, 0x7f0d0442

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->B:Landroid/widget/TextView;

    .line 2329512
    const v0, 0x7f0d0441

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->C:Landroid/widget/TextView;

    .line 2329513
    const v0, 0x7f0d03cf

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->D:Landroid/view/View;

    .line 2329514
    const v0, 0x7f0d0443

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->K:Landroid/widget/ProgressBar;

    .line 2329515
    const v0, 0x7f0d043f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->T:Landroid/view/View;

    .line 2329516
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0442

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->R:I

    .line 2329517
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->e(Landroid/os/Bundle;)V

    .line 2329518
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->D:Landroid/view/View;

    new-instance v1, LX/GD4;

    invoke-direct {v1, p0}, LX/GD4;-><init>(Lcom/facebook/adinterfaces/MapAreaPickerActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2329519
    new-instance v0, LX/681;

    invoke-direct {v0}, LX/681;-><init>()V

    const/4 v1, 0x0

    .line 2329520
    iput-boolean v1, v0, LX/681;->d:Z

    .line 2329521
    move-object v0, v0

    .line 2329522
    const-string v1, "ad_area_picker"

    .line 2329523
    iput-object v1, v0, LX/681;->m:Ljava/lang/String;

    .line 2329524
    move-object v0, v0

    .line 2329525
    invoke-static {v0}, Lcom/facebook/android/maps/SupportMapFragment;->a(LX/681;)Lcom/facebook/android/maps/SupportMapFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->A:Lcom/facebook/android/maps/SupportMapFragment;

    .line 2329526
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d043e

    iget-object v2, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->A:Lcom/facebook/android/maps/SupportMapFragment;

    const-string v3, "map_fragment"

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2329527
    invoke-direct {p0}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->o()V

    .line 2329528
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2329529
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->z:Lcom/facebook/adinterfaces/ui/MapSpinnerView;

    sget-object v1, LX/GMY;->LOADING:LX/GMY;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->setState(LX/GMY;)V

    .line 2329530
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->x:Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

    new-instance v1, LX/GDB;

    invoke-direct {v1, p0}, LX/GDB;-><init>(Lcom/facebook/adinterfaces/MapAreaPickerActivity;)V

    .line 2329531
    iget-object v2, v0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->i:LX/1Ck;

    sget-object p0, LX/Gdh;->FETCH_RADIUS:LX/Gdh;

    invoke-virtual {v2, p0}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2329532
    iget-object v2, v0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->j:LX/GDS;

    new-instance p0, LX/Gdd;

    invoke-direct {p0, v0, v1}, LX/Gdd;-><init>(Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;LX/GDA;)V

    invoke-virtual {v2, p1, p0}, LX/GDS;->a(Ljava/lang/String;LX/GDQ;)V

    .line 2329533
    return-void
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 2329534
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->I:Lcom/facebook/android/maps/model/LatLng;

    if-nez v0, :cond_0

    .line 2329535
    :goto_0
    return-void

    .line 2329536
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->A:Lcom/facebook/android/maps/SupportMapFragment;

    new-instance v1, LX/GD9;

    invoke-direct {v1, p0}, LX/GD9;-><init>(Lcom/facebook/adinterfaces/MapAreaPickerActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/SupportMapFragment;->a(LX/68J;)V

    goto :goto_0
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 2329537
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->S:LX/5OM;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->S:LX/5OM;

    .line 2329538
    iget-boolean v1, v0, LX/0ht;->r:Z

    move v0, v1

    .line 2329539
    if-eqz v0, :cond_0

    .line 2329540
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->S:LX/5OM;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 2329541
    :cond_0
    new-instance v0, LX/5OM;

    invoke-direct {v0, p0}, LX/5OM;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->S:LX/5OM;

    .line 2329542
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->S:LX/5OM;

    invoke-virtual {v0}, LX/5OM;->c()LX/5OG;

    move-result-object v0

    .line 2329543
    invoke-direct {p0, v0}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->a(LX/5OG;)V

    .line 2329544
    iget-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->S:LX/5OM;

    invoke-virtual {v1, v0}, LX/5OM;->a(LX/5OG;)V

    .line 2329545
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->S:LX/5OM;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ht;->c(Z)V

    .line 2329546
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->S:LX/5OM;

    iget-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->T:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->c(Landroid/view/View;)V

    .line 2329547
    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->S:LX/5OM;

    invoke-virtual {v0}, LX/0ht;->d()V

    .line 2329548
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 2329549
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 2329550
    iget-boolean v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->P:Z

    if-eqz v1, :cond_0

    .line 2329551
    iget-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->O:LX/63L;

    invoke-virtual {v1, p1}, LX/63L;->a(Landroid/view/Menu;)V

    .line 2329552
    :cond_0
    return v0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 2329553
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->P:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->O:LX/63L;

    invoke-virtual {v0, p1}, LX/63L;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2329554
    const/4 v0, 0x0

    .line 2329555
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0xd4ac530

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2329556
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2329557
    iget-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->A:Lcom/facebook/android/maps/SupportMapFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 2329558
    iget-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->x:Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

    invoke-virtual {v1}, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->a()V

    .line 2329559
    iget-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->S:LX/5OM;

    if-eqz v1, :cond_0

    .line 2329560
    iget-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->S:LX/5OM;

    invoke-virtual {v1}, LX/0ht;->l()V

    .line 2329561
    :cond_0
    iget-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->r:LX/1Ck;

    const-string v2, "get_location_task_key"

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2329562
    iget-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->s:LX/GEO;

    invoke-virtual {v1}, LX/GEO;->a()V

    .line 2329563
    const/16 v1, 0x23

    const v2, -0x12a98e1b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2329564
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 2329565
    invoke-direct {p0, p1}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->e(Landroid/os/Bundle;)V

    .line 2329566
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x5af9c4c0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2329567
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2329568
    iget-object v1, p0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->A:Lcom/facebook/android/maps/SupportMapFragment;

    invoke-virtual {v1, p0}, Lcom/facebook/android/maps/SupportMapFragment;->a(LX/68J;)V

    .line 2329569
    const/16 v1, 0x23

    const v2, -0x1a2ca023

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2329570
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2329571
    invoke-direct {p0, p1}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->d(Landroid/os/Bundle;)V

    .line 2329572
    return-void
.end method
