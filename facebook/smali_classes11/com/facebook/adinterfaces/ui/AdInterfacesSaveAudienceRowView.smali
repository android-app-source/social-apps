.class public Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterEditTextView;

.field private b:Lcom/facebook/resources/ui/FbButton;

.field private c:Lcom/facebook/widget/CustomLinearLayout;

.field private d:Lcom/facebook/resources/ui/FbCheckBox;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2342062
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2342063
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->a()V

    .line 2342064
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2342059
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2342060
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->a()V

    .line 2342061
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2342038
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2342039
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->a()V

    .line 2342040
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2342053
    const v0, 0x7f030080

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2342054
    const v0, 0x7f0d0471

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbCheckBox;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->d:Lcom/facebook/resources/ui/FbCheckBox;

    .line 2342055
    const v0, 0x7f0d0473

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2342056
    const v0, 0x7f0d0472

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->c:Lcom/facebook/widget/CustomLinearLayout;

    .line 2342057
    const v0, 0x7f0d0474

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->b:Lcom/facebook/resources/ui/FbButton;

    .line 2342058
    return-void
.end method


# virtual methods
.method public setInputRowVisibility(Z)V
    .locals 2

    .prologue
    .line 2342050
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->c:Lcom/facebook/widget/CustomLinearLayout;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 2342051
    return-void

    .line 2342052
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setSaveAudienceCheckBoxListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 2342048
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->d:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2342049
    return-void
.end method

.method public setSaveAudienceEditTextViewListener(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 2342046
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2342047
    return-void
.end method

.method public setSaveButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2342044
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->b:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2342045
    return-void
.end method

.method public setSaveButtonVisibility(Z)V
    .locals 2

    .prologue
    .line 2342041
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->b:Lcom/facebook/resources/ui/FbButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2342042
    return-void

    .line 2342043
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
