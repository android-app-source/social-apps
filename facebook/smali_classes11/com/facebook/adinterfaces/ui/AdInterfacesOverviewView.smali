.class public Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;

.field public b:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;

.field private c:Lcom/facebook/widget/text/BetterTextView;

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:Lcom/facebook/widget/text/BetterTextView;

.field private f:Lcom/facebook/widget/text/BetterTextView;

.field private g:Lcom/facebook/widget/text/BetterTextView;

.field private h:Lcom/facebook/resources/ui/FbSwitch;

.field private i:Lcom/facebook/widget/FbImageView;

.field private j:Lcom/facebook/widget/CustomLinearLayout;

.field private k:Lcom/facebook/widget/CustomLinearLayout;

.field private l:Lcom/facebook/widget/CustomLinearLayout;

.field private m:Lcom/facebook/widget/CustomLinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2341006
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2341007
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->a()V

    .line 2341008
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2341061
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2341062
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->a()V

    .line 2341063
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2341058
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2341059
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->a()V

    .line 2341060
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2341043
    const v0, 0x7f030073

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2341044
    const v0, 0x7f0d03fd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;

    .line 2341045
    const v0, 0x7f0d045d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;

    .line 2341046
    const v0, 0x7f0d0454

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2341047
    const v0, 0x7f0d0456

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2341048
    const v0, 0x7f0d0458

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2341049
    const v0, 0x7f0d045a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2341050
    const v0, 0x7f0d045c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 2341051
    const v0, 0x7f0d0452

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbSwitch;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->h:Lcom/facebook/resources/ui/FbSwitch;

    .line 2341052
    const v0, 0x7f0d0453

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->i:Lcom/facebook/widget/FbImageView;

    .line 2341053
    const v0, 0x7f0d0455

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->j:Lcom/facebook/widget/CustomLinearLayout;

    .line 2341054
    const v0, 0x7f0d0457

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->k:Lcom/facebook/widget/CustomLinearLayout;

    .line 2341055
    const v0, 0x7f0d0459

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->l:Lcom/facebook/widget/CustomLinearLayout;

    .line 2341056
    const v0, 0x7f0d045b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->m:Lcom/facebook/widget/CustomLinearLayout;

    .line 2341057
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;IIZ)V
    .locals 4

    .prologue
    .line 2341029
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341030
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2341031
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->i:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v0, p3}, Lcom/facebook/widget/FbImageView;->setBackgroundResource(I)V

    .line 2341032
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-le v0, v1, :cond_0

    .line 2341033
    if-eqz p4, :cond_1

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0357

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 2341034
    :goto_0
    if-eqz p4, :cond_2

    move v0, v1

    .line 2341035
    :goto_1
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->h:Lcom/facebook/resources/ui/FbSwitch;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbSwitch;->getThumbDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v1, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2341036
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->h:Lcom/facebook/resources/ui/FbSwitch;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbSwitch;->getTrackDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v0, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2341037
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->h:Lcom/facebook/resources/ui/FbSwitch;

    invoke-virtual {v0, p4}, Lcom/facebook/resources/ui/FbSwitch;->setChecked(Z)V

    .line 2341038
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->h:Lcom/facebook/resources/ui/FbSwitch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbSwitch;->setVisibility(I)V

    .line 2341039
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->i:Lcom/facebook/widget/FbImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 2341040
    return-void

    .line 2341041
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0353

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_0

    .line 2341042
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a036e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_1
.end method

.method public getAYMTView()Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;
    .locals 1

    .prologue
    .line 2341028
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;

    return-object v0
.end method

.method public getOverviewFooterView()Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;
    .locals 1

    .prologue
    .line 2341027
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;

    return-object v0
.end method

.method public setAdStatusToggleListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 2341025
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->h:Lcom/facebook/resources/ui/FbSwitch;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbSwitch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2341026
    return-void
.end method

.method public setAmountSpentRowListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2341023
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->l:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2341024
    return-void
.end method

.method public setAmountSpentValue(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2341021
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341022
    return-void
.end method

.method public setAudienceRowListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2341019
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->j:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2341020
    return-void
.end method

.method public setAudienceValue(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2341017
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341018
    return-void
.end method

.method public setEndDateRowListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2341015
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->k:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2341016
    return-void
.end method

.method public setEndDateValue(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2341013
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341014
    return-void
.end method

.method public setPaymentMethodRowListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2341011
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->m:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2341012
    return-void
.end method

.method public setPaymentMethodValue(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2341009
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341010
    return-void
.end method
