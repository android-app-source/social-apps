.class public Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;

.field public b:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2340595
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2340596
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;->a()V

    .line 2340597
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2340598
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2340599
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;->a()V

    .line 2340600
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2340601
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2340602
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;->a()V

    .line 2340603
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2340604
    const v0, 0x7f03006f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2340605
    const v0, 0x7f0d0446

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;->a:Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;

    .line 2340606
    const v0, 0x7f0d0447

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;->b:Landroid/view/ViewGroup;

    .line 2340607
    return-void
.end method


# virtual methods
.method public getContainerView()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 2340608
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;->b:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public setIsLoading(Z)V
    .locals 2

    .prologue
    .line 2340609
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;->a:Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->setVisibility(I)V

    .line 2340610
    if-eqz p1, :cond_1

    .line 2340611
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;->a:Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->a()V

    .line 2340612
    :goto_1
    return-void

    .line 2340613
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 2340614
    :cond_1
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;->a:Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->b()V

    goto :goto_1
.end method
