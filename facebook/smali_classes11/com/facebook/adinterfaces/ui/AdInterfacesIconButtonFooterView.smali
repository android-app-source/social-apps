.class public Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/resources/ui/FbButton;

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2339608
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2339609
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;->a()V

    .line 2339610
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2339611
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2339612
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;->a()V

    .line 2339613
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2339614
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2339615
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;->a()V

    .line 2339616
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2339617
    const v0, 0x7f030060

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2339618
    const v0, 0x7f0d041f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;->a:Lcom/facebook/resources/ui/FbButton;

    .line 2339619
    const v0, 0x7f0d041e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2339620
    new-instance v0, Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView$1;

    invoke-direct {v0, p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView$1;-><init>(Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;)V

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;->c:Ljava/lang/Runnable;

    .line 2339621
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 4

    .prologue
    .line 2339622
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 2339623
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;->c:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2339624
    return-void
.end method

.method public setBoostText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2339625
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2339626
    return-void
.end method

.method public setCreadAdIconButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2339627
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;->a:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2339628
    return-void
.end method

.method public setCreateButtonText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2339629
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;->a:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2339630
    return-void
.end method
