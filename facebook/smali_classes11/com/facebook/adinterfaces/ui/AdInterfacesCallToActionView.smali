.class public Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field private a:I

.field private b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

.field private c:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

.field private d:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

.field private e:Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;

.field private f:Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2338713
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2338714
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->a:I

    .line 2338715
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->c()V

    .line 2338716
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2338717
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2338718
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->a:I

    .line 2338719
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->c()V

    .line 2338720
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2338650
    const v0, 0x7f030052

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2338651
    const v0, 0x7f0d03fc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 2338652
    const v0, 0x7f0d03f8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->c:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 2338653
    const v0, 0x7f0d03f9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->d:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    .line 2338654
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->d:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    const-string v1, "no_button_tag"

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTag(Ljava/lang/Object;)V

    .line 2338655
    const v0, 0x7f0d03fa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;

    .line 2338656
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;

    const-string v1, "instant_workflow_tag"

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;->setTag(Ljava/lang/Object;)V

    .line 2338657
    const v0, 0x7f0d03fb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;

    .line 2338658
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;

    const-string v1, "other_button_tag"

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;->setTag(Ljava/lang/Object;)V

    .line 2338659
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2338708
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v2}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2338709
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v2, v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2338710
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x42000000    # 32.0f

    invoke-static {v3, v4}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v3

    invoke-virtual {v2, v3, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 2338711
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2338712
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)V
    .locals 2

    .prologue
    .line 2338706
    invoke-static {p1}, LX/GCi;->fromGraphQLTypeCallToAction(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)LX/GCi;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GCi;->getText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->a(Lcom/facebook/graphql/enums/GraphQLCallToActionType;Ljava/lang/String;)V

    .line 2338707
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLCallToActionType;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2338697
    iget v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->a:I

    const/16 v2, 0x8

    if-ge v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2338698
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    iget v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->a:I

    invoke-virtual {v0, v2}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    .line 2338699
    if-nez v0, :cond_1

    .line 2338700
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 2338701
    goto :goto_0

    .line 2338702
    :cond_1
    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setVisibility(I)V

    .line 2338703
    invoke-virtual {v0, p2}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTextTextViewStart(Ljava/lang/CharSequence;)V

    .line 2338704
    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTag(Ljava/lang/Object;)V

    .line 2338705
    iget v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->a:I

    goto :goto_1
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2338694
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTextTextViewStart(Ljava/lang/CharSequence;)V

    .line 2338695
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;

    invoke-virtual {v0, p2}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTextTextViewEnd(Ljava/lang/CharSequence;)V

    .line 2338696
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2338688
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a()V

    move v0, v1

    .line 2338689
    :goto_0
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v2}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2338690
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v2, v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2338691
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2338692
    :cond_0
    iput v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->a:I

    .line 2338693
    return-void
.end method

.method public getCheckedCallToActionType()Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 5

    .prologue
    .line 2338721
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 2338722
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v1, v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2338723
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 2338724
    iget v4, v3, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a:I

    move v3, v4

    .line 2338725
    if-ne v2, v3, :cond_0

    .line 2338726
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2338727
    :goto_1
    return-object v0

    .line 2338728
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2338729
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto :goto_1
.end method

.method public setCallToActionType(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)V
    .locals 3

    .prologue
    .line 2338682
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2338683
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v1, v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2338684
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_1

    .line 2338685
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a(I)V

    .line 2338686
    :cond_0
    return-void

    .line 2338687
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setCallToActionTypeIndex(I)V
    .locals 2

    .prologue
    .line 2338679
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2338680
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v1, p1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a(I)V

    .line 2338681
    :cond_0
    return-void
.end method

.method public setInstantWorkflowOption(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2338672
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->c:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2338673
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->c:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v0, v1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2338674
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2338675
    invoke-virtual {v0, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2338676
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->c:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a(I)V

    .line 2338677
    :cond_0
    return-void

    .line 2338678
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public setInstantWorkflowRadioVisibility(I)V
    .locals 1

    .prologue
    .line 2338670
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;->setVisibility(I)V

    .line 2338671
    return-void
.end method

.method public setInstantWorkflowVisibility(I)V
    .locals 1

    .prologue
    .line 2338668
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->c:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v0, p1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->setVisibility(I)V

    .line 2338669
    return-void
.end method

.method public setOnCheckedChangeListener(LX/Bc9;)V
    .locals 1

    .prologue
    .line 2338665
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 2338666
    iput-object p1, v0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->d:LX/Bc9;

    .line 2338667
    return-void
.end method

.method public setOnCheckedChangeListenerForIW(LX/Bc9;)V
    .locals 1

    .prologue
    .line 2338662
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->c:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 2338663
    iput-object p1, v0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->d:LX/Bc9;

    .line 2338664
    return-void
.end method

.method public setRadioGroupVisibility(I)V
    .locals 1

    .prologue
    .line 2338660
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v0, p1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->setVisibility(I)V

    .line 2338661
    return-void
.end method
