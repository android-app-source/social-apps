.class public Lcom/facebook/adinterfaces/ui/EditableRadioButton;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Landroid/widget/Checkable;
.implements LX/BcB;


# instance fields
.field private a:Landroid/widget/ImageButton;

.field private b:Lcom/facebook/widget/text/BetterEditTextView;

.field private c:Lcom/facebook/widget/text/BetterTextView;

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field public e:Z

.field private f:Z

.field private g:LX/Bc8;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2344833
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2344834
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2344835
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2344836
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2344837
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2344838
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2344839
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2344840
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2344841
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    .line 2344842
    const v0, 0x7f030465

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2344843
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setClickable(Z)V

    .line 2344844
    const v0, 0x7f0d032f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->a:Landroid/widget/ImageButton;

    .line 2344845
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->a:Landroid/widget/ImageButton;

    const/4 v1, 0x2

    invoke-static {v0, v1}, LX/0vv;->d(Landroid/view/View;I)V

    .line 2344846
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->a:Landroid/widget/ImageButton;

    new-instance v1, LX/GMV;

    invoke-direct {v1, p0}, LX/GMV;-><init>(Lcom/facebook/adinterfaces/ui/EditableRadioButton;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 2344847
    const v0, 0x7f0d0d37

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->b:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2344848
    const v0, 0x7f0d0d36

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2344849
    const v0, 0x7f0d0d38

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2344850
    if-eqz p2, :cond_0

    .line 2344851
    sget-object v0, LX/03r;->BetterButton:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2344852
    const/16 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 2344853
    const/16 v2, 0x1

    sget-object v3, LX/0xr;->BUILTIN:LX/0xr;

    invoke-virtual {v3}, LX/0xr;->ordinal()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 2344854
    invoke-static {v1}, LX/0xq;->fromIndex(I)LX/0xq;

    move-result-object v1

    .line 2344855
    invoke-static {v2}, LX/0xr;->fromIndex(I)LX/0xr;

    move-result-object v2

    .line 2344856
    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->b:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v4, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v4}, Lcom/facebook/widget/text/BetterEditTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v4

    invoke-static {v3, v1, v2, v4}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 2344857
    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v4, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v4}, Lcom/facebook/widget/text/BetterTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v4

    invoke-static {v3, v1, v2, v4}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 2344858
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2344859
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 2344860
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2344861
    return-void
.end method

.method public final b(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 2344862
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2344863
    return-void
.end method

.method public getCursorPositionOnEditText()I
    .locals 1

    .prologue
    .line 2344864
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getSelectionEnd()I

    move-result v0

    return v0
.end method

.method public getEditTextHint()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2344865
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTextEditText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2344866
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final isChecked()Z
    .locals 1

    .prologue
    .line 2344867
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->e:Z

    return v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2344868
    const/4 v0, 0x1

    return v0
.end method

.method public final performClick()Z
    .locals 1

    .prologue
    .line 2344869
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->toggle()V

    .line 2344870
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->performClick()Z

    move-result v0

    return v0
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 2344794
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->e:Z

    if-eq v0, p1, :cond_0

    .line 2344795
    iput-boolean p1, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->e:Z

    .line 2344796
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2344797
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->refreshDrawableState()V

    .line 2344798
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->f:Z

    if-eqz v0, :cond_1

    .line 2344799
    :goto_0
    return-void

    .line 2344800
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->f:Z

    .line 2344801
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->g:LX/Bc8;

    if-eqz v0, :cond_2

    .line 2344802
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->g:LX/Bc8;

    invoke-virtual {v0, p0}, LX/Bc8;->a(Landroid/view/View;)V

    .line 2344803
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->f:Z

    goto :goto_0
.end method

.method public setContentDescriptionPrefixTextView(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2344831
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2344832
    return-void
.end method

.method public setContentDescriptionSuffixTextView(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2344871
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2344872
    return-void
.end method

.method public setCursorVisible(Z)V
    .locals 1

    .prologue
    .line 2344792
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setCursorVisible(Z)V

    .line 2344793
    return-void
.end method

.method public setHint(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2344804
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 2344805
    return-void
.end method

.method public setLeftAlignedSuffixTextView(Z)V
    .locals 2

    .prologue
    .line 2344806
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->d:Lcom/facebook/widget/text/BetterTextView;

    if-eqz p1, :cond_0

    const v0, 0x800003

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setGravity(I)V

    .line 2344807
    return-void

    .line 2344808
    :cond_0
    const v0, 0x800005

    goto :goto_0
.end method

.method public setOnCheckedChangeWidgetListener(LX/Bc8;)V
    .locals 0

    .prologue
    .line 2344809
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->g:LX/Bc8;

    .line 2344810
    return-void
.end method

.method public setOnEditorActionListenerEditText(Landroid/widget/TextView$OnEditorActionListener;)V
    .locals 1

    .prologue
    .line 2344811
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2344812
    return-void
.end method

.method public setOnFocusChangeListenerEditText(Landroid/view/View$OnFocusChangeListener;)V
    .locals 1

    .prologue
    .line 2344813
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2344814
    return-void
.end method

.method public setSelectionOnEditText(I)V
    .locals 1

    .prologue
    .line 2344815
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setSelection(I)V

    .line 2344816
    return-void
.end method

.method public setTextEditText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2344817
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2344818
    return-void
.end method

.method public setTextPrefixTextView(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2344819
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2344820
    return-void
.end method

.method public setTextSuffixTextView(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2344821
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2344822
    return-void
.end method

.method public setVisibilityPrefixTextView(I)V
    .locals 1

    .prologue
    .line 2344823
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2344824
    return-void
.end method

.method public setVisibilitySuffixTextView(I)V
    .locals 1

    .prologue
    .line 2344825
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2344826
    return-void
.end method

.method public final toggle()V
    .locals 1

    .prologue
    .line 2344827
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->e:Z

    if-nez v0, :cond_0

    .line 2344828
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setChecked(Z)V

    .line 2344829
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->requestFocus()Z

    .line 2344830
    :cond_0
    return-void
.end method
