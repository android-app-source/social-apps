.class public Lcom/facebook/adinterfaces/ui/BudgetOptionsView;
.super LX/GJD;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public d:Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;

.field public e:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2337979
    invoke-direct {p0, p1}, LX/GJD;-><init>(Landroid/content/Context;)V

    .line 2337980
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2337977
    invoke-direct {p0, p1, p2}, LX/GJD;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2337978
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2337975
    invoke-direct {p0, p1, p2, p3}, LX/GJD;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2337976
    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 2337964
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->b()V

    .line 2337965
    const v0, 0x7f0d03da

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 2337966
    const v0, 0x7f0d0429

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->b:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    .line 2337967
    const v0, 0x7f0d03ed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    const v1, 0x7f0d03ee

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    const v2, 0x7f0d03ef

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    const v3, 0x7f0d03f0

    invoke-virtual {p0, v3}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    const v4, 0x7f0d03f1

    invoke-virtual {p0, v4}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-static {v0, v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->c:LX/0Px;

    .line 2337968
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2337969
    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTag(Ljava/lang/Object;)V

    .line 2337970
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2337971
    :cond_0
    iget-object v0, p0, LX/GJD;->b:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    iget-object v1, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setTag(Ljava/lang/Object;)V

    .line 2337972
    const v0, 0x7f0d03eb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;

    .line 2337973
    const v0, 0x7f0d03ec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->e:Landroid/view/View;

    .line 2337974
    return-void
.end method

.method public final a(LX/GLM;Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .locals 1

    .prologue
    .line 2337961
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->setSpinnerVisibility(Z)V

    .line 2337962
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->a(Landroid/widget/SpinnerAdapter;Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 2337963
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 2337958
    if-ltz p2, :cond_0

    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt p2, v0, :cond_1

    .line 2337959
    :cond_0
    :goto_0
    return-void

    .line 2337960
    :cond_1
    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTextTextViewEnd(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 2337946
    const v0, 0x7f0301ff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2337947
    return-void
.end method

.method public final g(I)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2337956
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    move v0, v1

    .line 2337957
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public getRadioButtonsSize()I
    .locals 1

    .prologue
    .line 2337955
    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public setSpinnerSelected(I)V
    .locals 1

    .prologue
    .line 2337953
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->setSelected(I)V

    .line 2337954
    return-void
.end method

.method public setSpinnerVisibility(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2337948
    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->setVisibility(I)V

    .line 2337949
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->e:Landroid/view/View;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2337950
    return-void

    :cond_0
    move v0, v2

    .line 2337951
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2337952
    goto :goto_1
.end method
