.class public Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2336712
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2336713
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;->a()V

    .line 2336714
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2336715
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2336716
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;->a()V

    .line 2336717
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2336718
    const v0, 0x7f03004a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2336719
    const v0, 0x7f0d03df

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2336720
    const v0, 0x7f0d03e0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2336721
    return-void
.end method


# virtual methods
.method public setCreateNewButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2336722
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2336723
    return-void
.end method

.method public setCreateNewButtonText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2336724
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2336725
    return-void
.end method

.method public setSeeAllButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2336726
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2336727
    return-void
.end method

.method public setSeeAllButtonText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2336728
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2336729
    return-void
.end method

.method public setSeeAllButtonVisibility(Z)V
    .locals 2

    .prologue
    .line 2336730
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;->a:Lcom/facebook/widget/text/BetterTextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2336731
    return-void

    .line 2336732
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
