.class public Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/68J;


# instance fields
.field public a:LX/GNE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/android/maps/MapView;

.field private c:I

.field private d:D

.field private e:Lcom/facebook/android/maps/model/LatLng;

.field private f:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2340479
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2340480
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2340481
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2340482
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2340483
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2340484
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2340451
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2340452
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2340453
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2340465
    const-class v0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    invoke-static {v0, p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2340466
    const v0, 0x7f03006d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2340467
    const v0, 0x7f0d0444

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/MapView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->b:Lcom/facebook/android/maps/MapView;

    .line 2340468
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->b:Lcom/facebook/android/maps/MapView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/MapView;->a(Landroid/os/Bundle;)V

    .line 2340469
    const v0, 0x7f0d0445

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->f:Landroid/widget/ImageView;

    .line 2340470
    if-nez p2, :cond_0

    .line 2340471
    :goto_0
    return-void

    .line 2340472
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->AdInterfacesMapPreviewView:[I

    invoke-virtual {v0, p2, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2340473
    const/16 v1, 0x0

    const v2, 0x7f0b0440

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 2340474
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->f:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 2340475
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2340476
    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->f:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2340477
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->c:I

    .line 2340478
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    invoke-static {v0}, LX/GNE;->a(LX/0QB;)LX/GNE;

    move-result-object v0

    check-cast v0, LX/GNE;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->a:LX/GNE;

    return-void
.end method


# virtual methods
.method public final a(LX/680;)V
    .locals 7

    .prologue
    .line 2340462
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->a:LX/GNE;

    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->e:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v4, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->d:D

    iget v6, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->c:I

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, LX/GNE;->a(LX/680;Lcom/facebook/android/maps/model/LatLng;DI)F

    move-result v0

    .line 2340463
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->e:Lcom/facebook/android/maps/model/LatLng;

    invoke-static {v1, v0}, LX/67e;->a(Lcom/facebook/android/maps/model/LatLng;F)LX/67d;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/680;->b(LX/67d;)V

    .line 2340464
    return-void
.end method

.method public final a(Lcom/facebook/android/maps/model/LatLng;D)V
    .locals 2

    .prologue
    .line 2340458
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->e:Lcom/facebook/android/maps/model/LatLng;

    .line 2340459
    iput-wide p2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->d:D

    .line 2340460
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->b:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0, p0}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    .line 2340461
    return-void
.end method

.method public final a(Lcom/facebook/android/maps/model/LatLng;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 2340455
    invoke-static {p2}, LX/690;->a(Landroid/graphics/Bitmap;)LX/68w;

    move-result-object v0

    .line 2340456
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->b:Lcom/facebook/android/maps/MapView;

    new-instance v2, LX/GK9;

    invoke-direct {v2, p0, v0, p1}, LX/GK9;-><init>(Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;LX/68w;Lcom/facebook/android/maps/model/LatLng;)V

    invoke-virtual {v1, v2}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    .line 2340457
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2340454
    const/4 v0, 0x1

    return v0
.end method
