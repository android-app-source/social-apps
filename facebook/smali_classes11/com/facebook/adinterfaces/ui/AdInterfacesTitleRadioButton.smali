.class public Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;
.super Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;
.source ""


# instance fields
.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2342906
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2342907
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2342904
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2342905
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2342885
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2342886
    return-void
.end method

.method private b(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2342887
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->AdInterfacesTitleRadioButton:[I

    invoke-virtual {v0, p2, v1, v3, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2342888
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 2342889
    if-eqz v1, :cond_0

    .line 2342890
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTextTextViewStart(Ljava/lang/CharSequence;)V

    .line 2342891
    :cond_0
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 2342892
    if-eqz v1, :cond_1

    .line 2342893
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTextTextViewEnd(Ljava/lang/CharSequence;)V

    .line 2342894
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2342895
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 2342896
    const v0, 0x7f030092

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2342897
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;->setClickable(Z)V

    .line 2342898
    const v0, 0x7f0d032f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;->c:Landroid/widget/ImageButton;

    .line 2342899
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->c:Landroid/widget/ImageButton;

    const/4 v1, 0x2

    invoke-static {v0, v1}, LX/0vv;->d(Landroid/view/View;I)V

    .line 2342900
    const v0, 0x7f0d0493

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2342901
    const v0, 0x7f0d0494

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2342902
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;->b(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2342903
    return-void
.end method

.method public setContentDescriptionTextViewEnd(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2342879
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2342880
    return-void
.end method

.method public setTextTextViewEnd(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2342881
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2342882
    return-void
.end method

.method public setTextTextViewStart(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2342883
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesTitleRadioButton;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2342884
    return-void
.end method
