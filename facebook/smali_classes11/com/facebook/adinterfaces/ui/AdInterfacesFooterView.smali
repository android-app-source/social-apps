.class public Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/resources/ui/FbButton;

.field private b:Lcom/facebook/resources/ui/FbButton;

.field private c:Lcom/facebook/resources/ui/FbButton;

.field private d:Lcom/facebook/resources/ui/FbButton;

.field private e:Lcom/facebook/resources/ui/FbButton;

.field private f:Lcom/facebook/resources/ui/FbButton;

.field private g:Lcom/facebook/widget/text/BetterTextView;

.field private h:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2339552
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2339553
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->a()V

    .line 2339554
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2339555
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2339556
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->a()V

    .line 2339557
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2339558
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2339559
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->a()V

    .line 2339560
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2339561
    const v0, 0x7f03005b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2339562
    const v0, 0x7f0d0411

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->a:Lcom/facebook/resources/ui/FbButton;

    .line 2339563
    const v0, 0x7f0d0412

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->b:Lcom/facebook/resources/ui/FbButton;

    .line 2339564
    const v0, 0x7f0d0413

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->c:Lcom/facebook/resources/ui/FbButton;

    .line 2339565
    const v0, 0x7f0d0414

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->d:Lcom/facebook/resources/ui/FbButton;

    .line 2339566
    const v0, 0x7f0d0415

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->e:Lcom/facebook/resources/ui/FbButton;

    .line 2339567
    const v0, 0x7f0d0416

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->f:Lcom/facebook/resources/ui/FbButton;

    .line 2339568
    const v0, 0x7f0d0417

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 2339569
    new-instance v0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView$1;

    invoke-direct {v0, p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView$1;-><init>(Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;)V

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->h:Ljava/lang/Runnable;

    .line 2339570
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 4

    .prologue
    .line 2339571
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 2339572
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->h:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2339573
    return-void
.end method

.method public setAddBudgetButtonEnabled(Z)V
    .locals 1

    .prologue
    .line 2339574
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->b:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 2339575
    return-void
.end method

.method public setAddBudgetButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2339576
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->b:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2339577
    return-void
.end method

.method public setAddBudgetButtonText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2339578
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->b:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2339579
    return-void
.end method

.method public setAddBudgetButtonVisibility(I)V
    .locals 1

    .prologue
    .line 2339580
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->b:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2339581
    return-void
.end method

.method public setConfirmAdButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2339582
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2339583
    return-void
.end method

.method public setConfirmAdButtonVisibility(I)V
    .locals 1

    .prologue
    .line 2339584
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2339585
    return-void
.end method

.method public setCreateAdButtonEnabled(Z)V
    .locals 1

    .prologue
    .line 2339524
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->a:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 2339525
    return-void
.end method

.method public setCreateAdButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2339550
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->a:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2339551
    return-void
.end method

.method public setCreateAdButtonText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2339586
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->a:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2339587
    return-void
.end method

.method public setCreateAdButtonVisibility(I)V
    .locals 1

    .prologue
    .line 2339522
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->a:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2339523
    return-void
.end method

.method public setDeleteAdButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2339526
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2339527
    return-void
.end method

.method public setDeleteAdButtonText(I)V
    .locals 1

    .prologue
    .line 2339528
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2339529
    return-void
.end method

.method public setDeleteAdButtonVisibility(I)V
    .locals 1

    .prologue
    .line 2339530
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2339531
    return-void
.end method

.method public setLegalDisclaimerContent(Landroid/text/Spanned;)V
    .locals 1

    .prologue
    .line 2339532
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2339533
    return-void
.end method

.method public setLegalDisclaimerMovementMethod(Landroid/text/method/MovementMethod;)V
    .locals 1

    .prologue
    .line 2339534
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2339535
    return-void
.end method

.method public setLegalDisclaimerVisibility(I)V
    .locals 1

    .prologue
    .line 2339536
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2339537
    return-void
.end method

.method public setPauseAdButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2339538
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->d:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2339539
    return-void
.end method

.method public setPauseAdButtonText(I)V
    .locals 1

    .prologue
    .line 2339540
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->d:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2339541
    return-void
.end method

.method public setPauseAdButtonVisibility(I)V
    .locals 1

    .prologue
    .line 2339542
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->d:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2339543
    return-void
.end method

.method public setResumeAdButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2339544
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2339545
    return-void
.end method

.method public setResumeAdButtonText(I)V
    .locals 1

    .prologue
    .line 2339546
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2339547
    return-void
.end method

.method public setResumeAdButtonVisibility(I)V
    .locals 1

    .prologue
    .line 2339548
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2339549
    return-void
.end method
