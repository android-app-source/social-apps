.class public final Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1;)V
    .locals 0

    .prologue
    .line 2338870
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1$1;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 2338871
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1$1;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1;->a:LX/GJV;

    iget-object v0, v0, LX/GJV;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a004f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2338872
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1$1;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1;

    iget-object v1, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1;->a:LX/GJV;

    iget-object v1, v1, LX/GJV;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a036a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 2338873
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1$1;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1;

    iget-object v2, v2, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1;->a:LX/GJV;

    iget-object v2, v2, LX/GJV;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    new-instance v3, Landroid/animation/ArgbEvaluator;

    invoke-direct {v3}, Landroid/animation/ArgbEvaluator;-><init>()V

    new-array v4, v6, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Landroid/animation/ValueAnimator;->ofObject(Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 2338874
    iput-object v0, v2, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->q:Landroid/animation/ValueAnimator;

    .line 2338875
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1$1;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1;->a:LX/GJV;

    iget-object v0, v0, LX/GJV;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->q:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2338876
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1$1;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1;->a:LX/GJV;

    iget-object v0, v0, LX/GJV;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->q:Landroid/animation/ValueAnimator;

    new-instance v1, LX/GJU;

    invoke-direct {v1, p0}, LX/GJU;-><init>(Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1$1;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2338877
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1$1;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1;->a:LX/GJV;

    iget-object v0, v0, LX/GJV;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->q:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v6}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 2338878
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1$1;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1;->a:LX/GJV;

    iget-object v0, v0, LX/GJV;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->q:Landroid/animation/ValueAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 2338879
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1$1;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout$4$1;->a:LX/GJV;

    iget-object v0, v0, LX/GJV;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->q:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2338880
    return-void
.end method
