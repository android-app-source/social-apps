.class public Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Lcom/facebook/widget/text/BetterTextView;

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:Lcom/facebook/resources/ui/FbButton;

.field private f:Lcom/facebook/resources/ui/FbButton;

.field private g:Lcom/facebook/resources/ui/FbButton;

.field private h:Landroid/widget/LinearLayout;

.field private i:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2341715
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2341716
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->a()V

    .line 2341717
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2341769
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2341770
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->a()V

    .line 2341771
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2341766
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2341767
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->a()V

    .line 2341768
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2341755
    const v0, 0x7f030079

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2341756
    const v0, 0x7f0d0467

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2341757
    const v0, 0x7f0d0468

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2341758
    const v0, 0x7f0d0469

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2341759
    const v0, 0x7f0d0466

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->i:Landroid/view/View;

    .line 2341760
    const v0, 0x7f0d046a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2341761
    const v0, 0x7f0d0393

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->e:Lcom/facebook/resources/ui/FbButton;

    .line 2341762
    const v0, 0x7f0d046d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->f:Lcom/facebook/resources/ui/FbButton;

    .line 2341763
    const v0, 0x7f0d046c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->g:Lcom/facebook/resources/ui/FbButton;

    .line 2341764
    const v0, 0x7f0d046b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->h:Landroid/widget/LinearLayout;

    .line 2341765
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2341752
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341753
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341754
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2341749
    invoke-virtual {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->c(Ljava/lang/String;Ljava/lang/String;)LX/GKw;

    move-result-object v0

    .line 2341750
    invoke-virtual {v0, p3}, LX/GKw;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2341751
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2341746
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341747
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341748
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)LX/GKw;
    .locals 2

    .prologue
    .line 2341743
    new-instance v0, LX/GKw;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2}, LX/GKw;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2341744
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2341745
    return-object v0
.end method

.method public setActionButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2341741
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2341742
    return-void
.end method

.method public setActionButtonText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2341739
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2341740
    return-void
.end method

.method public setActionButtonVisibility(I)V
    .locals 1

    .prologue
    .line 2341737
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2341738
    return-void
.end method

.method public setColumnsActive(Z)V
    .locals 2

    .prologue
    .line 2341734
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->i:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2341735
    return-void

    .line 2341736
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setCreateNewButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2341732
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->g:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2341733
    return-void
.end method

.method public setCreateNewButtonText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2341730
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->g:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2341731
    return-void
.end method

.method public setCreateNewButtonVisibility(I)V
    .locals 1

    .prologue
    .line 2341728
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->g:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2341729
    return-void
.end method

.method public setPaidReach(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2341726
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341727
    return-void
.end method

.method public setSecondActionButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2341724
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2341725
    return-void
.end method

.method public setSecondActionButtonText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2341722
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2341723
    return-void
.end method

.method public setSecondActionButtonVisibility(I)V
    .locals 1

    .prologue
    .line 2341720
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2341721
    return-void
.end method

.method public setSpentText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2341718
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341719
    return-void
.end method
