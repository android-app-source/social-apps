.class public Lcom/facebook/adinterfaces/ui/preview/AdInterfacesBoostedComponentPreviewFetcher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GKO;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GKO",
        "<",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/GG6;

.field private final b:LX/GDt;


# direct methods
.method public constructor <init>(LX/GG6;LX/GDt;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2345177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2345178
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/preview/AdInterfacesBoostedComponentPreviewFetcher;->a:LX/GG6;

    .line 2345179
    iput-object p2, p0, Lcom/facebook/adinterfaces/ui/preview/AdInterfacesBoostedComponentPreviewFetcher;->b:LX/GDt;

    .line 2345180
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/adinterfaces/ui/preview/AdInterfacesBoostedComponentPreviewFetcher;
    .locals 7

    .prologue
    .line 2345197
    new-instance v2, Lcom/facebook/adinterfaces/ui/preview/AdInterfacesBoostedComponentPreviewFetcher;

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v0

    check-cast v0, LX/GG6;

    .line 2345198
    new-instance v6, LX/GDt;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v1

    check-cast v1, LX/2U3;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lC;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {p0}, LX/2U4;->a(LX/0QB;)LX/2U4;

    move-result-object v5

    check-cast v5, LX/2U4;

    invoke-direct {v6, v1, v3, v4, v5}, LX/GDt;-><init>(LX/2U3;LX/0lC;LX/0tX;LX/2U4;)V

    .line 2345199
    move-object v1, v6

    .line 2345200
    check-cast v1, LX/GDt;

    invoke-direct {v2, v0, v1}, Lcom/facebook/adinterfaces/ui/preview/AdInterfacesBoostedComponentPreviewFetcher;-><init>(LX/GG6;LX/GDt;)V

    .line 2345201
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6

    .prologue
    .line 2345181
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2345182
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->r()Ljava/lang/String;

    move-result-object v0

    .line 2345183
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/preview/AdInterfacesBoostedComponentPreviewFetcher;->b:LX/GDt;

    .line 2345184
    iget-object v2, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v2, v2

    .line 2345185
    :try_start_0
    iget-object v3, v1, LX/GDt;->b:LX/0lC;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/CreativeAdModel;->n()LX/4Cf;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 2345186
    iget-object v4, v1, LX/GDt;->d:LX/2U4;

    .line 2345187
    new-instance v5, LX/A8h;

    invoke-direct {v5}, LX/A8h;-><init>()V

    move-object v5, v5

    .line 2345188
    const-string p1, "creative_spec"

    invoke-virtual {v5, p1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string p1, "account_id"

    invoke-virtual {v5, p1, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string p1, "image_large_aspect_height"

    iget-object v2, v4, LX/2U4;->a:LX/0sa;

    invoke-virtual {v2}, LX/0sa;->A()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, p1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v5

    const-string p1, "image_large_aspect_width"

    iget-object v2, v4, LX/2U4;->a:LX/0sa;

    invoke-virtual {v2}, LX/0sa;->z()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, p1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v5

    .line 2345189
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    move-object v3, v5

    .line 2345190
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    .line 2345191
    iput-object v4, v3, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2345192
    iget-object v4, v1, LX/GDt;->c:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    :goto_0
    move-object v0, v3

    .line 2345193
    new-instance v1, LX/GMi;

    invoke-direct {v1, p0}, LX/GMi;-><init>(Lcom/facebook/adinterfaces/ui/preview/AdInterfacesBoostedComponentPreviewFetcher;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 2345194
    :catch_0
    move-exception v3

    .line 2345195
    iget-object v4, v1, LX/GDt;->a:LX/2U3;

    const-class v5, LX/GDt;

    const-string p1, "Error converting ad creative"

    invoke-virtual {v4, v5, p1, v3}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2345196
    const/4 v3, 0x0

    goto :goto_0
.end method
