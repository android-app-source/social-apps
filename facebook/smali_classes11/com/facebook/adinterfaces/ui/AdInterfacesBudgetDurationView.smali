.class public Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;
.super Lcom/facebook/adinterfaces/ui/BudgetOptionsView;
.source ""


# instance fields
.field private f:Lcom/facebook/widget/text/BetterTextView;

.field public g:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

.field private h:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2337981
    invoke-direct {p0, p1}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;-><init>(Landroid/content/Context;)V

    .line 2337982
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2338013
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2338014
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2338011
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2338012
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2337997
    const v0, 0x7f030050

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2337998
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->setOrientation(I)V

    .line 2337999
    const v0, 0x7f0d03da

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 2338000
    const v0, 0x7f0d03f3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->b:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    .line 2338001
    const v0, 0x7f0d03eb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;

    .line 2338002
    const v0, 0x7f0d03ec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->e:Landroid/view/View;

    .line 2338003
    const v0, 0x7f0d03f6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2338004
    const v0, 0x7f0d03f7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    .line 2338005
    const v0, 0x7f0d03f4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->h:Landroid/widget/LinearLayout;

    .line 2338006
    const v0, 0x7f0d03ed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    const v1, 0x7f0d03ee

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    const v2, 0x7f0d03ef

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    const v3, 0x7f0d03f0

    invoke-virtual {p0, v3}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    const v4, 0x7f0d03f1

    invoke-virtual {p0, v4}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    const v5, 0x7f0d03f2

    invoke-virtual {p0, v5}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-static/range {v0 .. v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->c:LX/0Px;

    .line 2338007
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2338008
    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTag(Ljava/lang/Object;)V

    .line 2338009
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2338010
    :cond_0
    return-void
.end method

.method public final a(Landroid/text/Spanned;)V
    .locals 2

    .prologue
    .line 2337992
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setConfirmAdButtonVisibility(I)V

    .line 2337993
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setCreateAdButtonVisibility(I)V

    .line 2337994
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setLegalDisclaimerContent(Landroid/text/Spanned;)V

    .line 2337995
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setLegalDisclaimerMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2337996
    return-void
.end method

.method public getFooterView()Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;
    .locals 1

    .prologue
    .line 2337991
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    return-object v0
.end method

.method public setCustomDurationBudgetOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2337989
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2337990
    return-void
.end method

.method public setFooterViewVisibility(I)V
    .locals 1

    .prologue
    .line 2337987
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setVisibility(I)V

    .line 2337988
    return-void
.end method

.method public setSummaryText(Landroid/text/Spanned;)V
    .locals 1

    .prologue
    .line 2337985
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2337986
    return-void
.end method

.method public setSummaryVisibility(I)V
    .locals 1

    .prologue
    .line 2337983
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2337984
    return-void
.end method
