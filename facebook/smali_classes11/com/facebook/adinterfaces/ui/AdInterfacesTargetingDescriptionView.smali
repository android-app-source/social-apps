.class public Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/widget/text/BetterTextView;

.field public b:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

.field public c:Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2342747
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2342748
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;->a()V

    .line 2342749
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2342750
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2342751
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;->a()V

    .line 2342752
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2342753
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2342754
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;->a()V

    .line 2342755
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2342756
    const v0, 0x7f03008e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2342757
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;->setOrientation(I)V

    .line 2342758
    const v0, 0x7f0d047f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2342759
    const v0, 0x7f0d03d4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    .line 2342760
    const v0, 0x7f0d0480

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

    .line 2342761
    return-void
.end method


# virtual methods
.method public getMapPreviewView()Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;
    .locals 1

    .prologue
    .line 2342762
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    return-object v0
.end method

.method public getSaveAudienceRowView()Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;
    .locals 1

    .prologue
    .line 2342763
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

    return-object v0
.end method

.method public getTargetingDescriptionTextView()Lcom/facebook/widget/text/BetterTextView;
    .locals 1

    .prologue
    .line 2342764
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;->a:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method
