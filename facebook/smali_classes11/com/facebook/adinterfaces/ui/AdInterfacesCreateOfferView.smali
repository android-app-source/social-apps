.class public Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/SwitchCompat;

.field private b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2339150
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2339151
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->c()V

    .line 2339152
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2339106
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2339107
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->c()V

    .line 2339108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2339147
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2339148
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->c()V

    .line 2339149
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 2339142
    const v0, 0x7f030056

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2339143
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->setOrientation(I)V

    .line 2339144
    const v0, 0x7f0d03fe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/SwitchCompat;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->a:Lcom/facebook/widget/SwitchCompat;

    .line 2339145
    const v0, 0x7f0d03ff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 2339146
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2339135
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 2339136
    iget v1, v0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a:I

    move v0, v1

    .line 2339137
    if-lez v0, :cond_0

    .line 2339138
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a()V

    .line 2339139
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v1, v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a(I)V

    .line 2339140
    :goto_0
    return-void

    .line 2339141
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->setRadioChecked(I)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2339130
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v0, v2}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->setVisibility(I)V

    .line 2339131
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2339132
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v1, v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2339133
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2339134
    :cond_0
    return-void
.end method

.method public setDiscountTypes(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2339120
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v0, v3}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->setVisibility(I)V

    .line 2339121
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v3

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;

    .line 2339122
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildCount()I

    move-result v1

    if-ge v2, v1, :cond_0

    .line 2339123
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v1, v2}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    .line 2339124
    invoke-virtual {v1, v3}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setVisibility(I)V

    .line 2339125
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTextTextViewStart(Ljava/lang/CharSequence;)V

    .line 2339126
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTag(Ljava/lang/Object;)V

    .line 2339127
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 2339128
    goto :goto_0

    .line 2339129
    :cond_0
    return-void
.end method

.method public setOfferEnabled(Z)V
    .locals 1

    .prologue
    .line 2339118
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->a:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 2339119
    return-void
.end method

.method public setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 2339116
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->a:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2339117
    return-void
.end method

.method public setOnRadioButtonCheckedListener(LX/Bc9;)V
    .locals 1

    .prologue
    .line 2339113
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 2339114
    iput-object p1, v0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->d:LX/Bc9;

    .line 2339115
    return-void
.end method

.method public setRadioChecked(I)V
    .locals 2

    .prologue
    .line 2339109
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2339110
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a()V

    .line 2339111
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->b:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v1, p1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a(I)V

    .line 2339112
    :cond_0
    return-void
.end method
