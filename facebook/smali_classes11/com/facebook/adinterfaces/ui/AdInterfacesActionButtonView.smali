.class public Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2336036
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2336037
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;->a()V

    .line 2336038
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2336039
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2336040
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;->a()V

    .line 2336041
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2336042
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2336043
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;->a()V

    .line 2336044
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2336045
    const v0, 0x7f030041

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2336046
    invoke-virtual {p0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;->setOrientation(I)V

    .line 2336047
    const v0, 0x7f0d03cc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2336048
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;->a:Lcom/facebook/widget/text/BetterTextView;

    const-string v1, "No button"

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2336049
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2336050
    return-void
.end method


# virtual methods
.method public setSelectedValues(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2336051
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2336052
    return-void
.end method
