.class public Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Landroid/animation/LayoutTransition$TransitionListener;


# instance fields
.field public a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Lcom/facebook/widget/text/BetterTextView;

.field private d:Landroid/view/View;

.field private e:Lcom/facebook/widget/text/BetterTextView;

.field public f:Lcom/facebook/widget/text/BetterTextView;

.field public g:Lcom/facebook/widget/text/BetterTextView;

.field private h:Landroid/view/View;

.field public i:Landroid/view/View;

.field private j:Lcom/facebook/fbui/glyph/GlyphButton;

.field public k:Lcom/facebook/fbui/glyph/GlyphButton;

.field private l:Landroid/view/View;

.field private m:Z

.field private n:Z

.field private o:Landroid/view/View;

.field private p:Z

.field public q:Landroid/animation/ValueAnimator;

.field public r:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/GNP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2338948
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2338949
    iput-boolean v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->m:Z

    .line 2338950
    iput-boolean v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->n:Z

    .line 2338951
    iput-boolean v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->p:Z

    .line 2338952
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2338953
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2338958
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2338959
    iput-boolean v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->m:Z

    .line 2338960
    iput-boolean v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->n:Z

    .line 2338961
    iput-boolean v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->p:Z

    .line 2338962
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2338963
    return-void
.end method

.method private a(Landroid/view/View$OnClickListener;I)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 2338947
    new-instance v0, LX/GJW;

    invoke-direct {v0, p0, p1, p2}, LX/GJW;-><init>(Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;Landroid/view/View$OnClickListener;I)V

    return-object v0
.end method

.method private a(II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2338964
    if-nez p2, :cond_0

    .line 2338965
    invoke-virtual {p0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setHeaderIconResource(I)V

    .line 2338966
    :goto_0
    return-void

    .line 2338967
    :cond_0
    new-instance v0, LX/0wM;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    .line 2338968
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_1

    .line 2338969
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0, v3, v3, v3}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2338970
    :cond_1
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0, v3, v3, v3}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/content/res/TypedArray;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2338971
    const/16 v0, 0x5

    invoke-virtual {p2, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 2338972
    const/16 v1, 0x6

    invoke-virtual {p2, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 2338973
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 2338974
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Ljava/lang/String;I)V

    .line 2338975
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 2338976
    const v0, 0x7f03023e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2338977
    const-class v0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-static {v0, p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2338978
    invoke-virtual {p0, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setOrientation(I)V

    .line 2338979
    const v0, 0x7f0d08a6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2338980
    const v0, 0x7f0d08a5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->o:Landroid/view/View;

    .line 2338981
    const v0, 0x7f0d08aa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2338982
    const v0, 0x7f0d08b3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2338983
    const v0, 0x7f0d08b2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->d:Landroid/view/View;

    .line 2338984
    const v0, 0x7f0d08b1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2338985
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 2338986
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getCompoundDrawablesRelative()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2338987
    aget-object v1, v0, v4

    if-eqz v1, :cond_0

    .line 2338988
    aget-object v1, v0, v4

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    .line 2338989
    :cond_0
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->e:Lcom/facebook/widget/text/BetterTextView;

    const/4 v2, 0x0

    aget-object v2, v0, v2

    aget-object v3, v0, v3

    aget-object v4, v0, v4

    const/4 v5, 0x3

    aget-object v0, v0, v5

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2338990
    :cond_1
    const v0, 0x7f0d08b0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->h:Landroid/view/View;

    .line 2338991
    const v0, 0x7f0d08a7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->j:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2338992
    const v0, 0x7f0d08a8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->k:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2338993
    const v0, 0x7f0d08ac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2338994
    const v0, 0x7f0d08ad

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->i:Landroid/view/View;

    .line 2338995
    const v0, 0x7f0d08ae

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 2338996
    const v0, 0x7f0d08af

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->l:Landroid/view/View;

    .line 2338997
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->b(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2338998
    return-void
.end method

.method private a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;Landroid/view/View$OnClickListener;)V
    .locals 9
    .param p2    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 2338999
    if-nez p1, :cond_0

    .line 2339000
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2339001
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2339002
    :goto_0
    return-void

    .line 2339003
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;->l()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2339004
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 2339005
    new-instance v0, LX/0wM;

    invoke-direct {v0, v6}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    .line 2339006
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2339007
    sget-object v4, LX/GJX;->a:[I

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    move-object v0, v1

    move v2, v3

    move v4, v3

    move v5, v3

    .line 2339008
    :goto_1
    iget-object v7, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->l:Landroid/view/View;

    new-instance v8, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v8, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v7, v8}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2339009
    iget-object v5, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->g:Lcom/facebook/widget/text/BetterTextView;

    new-instance v7, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v7, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v5, v7}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2339010
    iget-object v4, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v4, v2}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2339011
    iget-object v4, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v4, v0, v1, v1, v1}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2339012
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->p:Z

    .line 2339013
    invoke-static {p0, p0}, LX/GMo;->a(Landroid/view/ViewGroup;Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 2339014
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->l:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2339015
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2339016
    if-eqz p2, :cond_1

    .line 2339017
    const v0, 0x7f080b37

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2339018
    new-instance v1, LX/47x;

    invoke-direct {v1, v6}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2339019
    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3}, Lcom/facebook/widget/text/BetterTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2339020
    invoke-static {v2}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-static {v2, v3}, LX/3qk;->b(II)I

    move-result v2

    invoke-direct {p0, p2, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Landroid/view/View$OnClickListener;I)Landroid/text/style/ClickableSpan;

    move-result-object v2

    const/16 v3, 0x21

    invoke-virtual {v1, v0, v0, v2, v3}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2339021
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2339022
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2339023
    :cond_1
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 2339024
    :pswitch_0
    const v4, 0x7f0a0362

    invoke-static {v2, v4}, LX/GMo;->a(Landroid/content/Context;I)I

    move-result v5

    .line 2339025
    const v4, 0x7f0a0364

    invoke-static {v2, v4}, LX/GMo;->a(Landroid/content/Context;I)I

    move-result v4

    .line 2339026
    const v7, 0x7f0a0363

    invoke-static {v2, v7}, LX/GMo;->a(Landroid/content/Context;I)I

    move-result v2

    .line 2339027
    const v7, 0x7f021a0b

    invoke-virtual {v0, v7, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_1

    .line 2339028
    :pswitch_1
    const v4, 0x7f0a0365

    invoke-static {v2, v4}, LX/GMo;->a(Landroid/content/Context;I)I

    move-result v5

    .line 2339029
    const v4, 0x7f0a0367

    invoke-static {v2, v4}, LX/GMo;->a(Landroid/content/Context;I)I

    move-result v4

    .line 2339030
    const v7, 0x7f0a0366

    invoke-static {v2, v7}, LX/GMo;->a(Landroid/content/Context;I)I

    move-result v2

    .line 2339031
    const v7, 0x7f021a0b

    invoke-virtual {v0, v7, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_1

    .line 2339032
    :pswitch_2
    const v4, 0x7f0a0368

    invoke-static {v2, v4}, LX/GMo;->a(Landroid/content/Context;I)I

    move-result v5

    .line 2339033
    const v4, 0x7f0a036a

    invoke-static {v2, v4}, LX/GMo;->a(Landroid/content/Context;I)I

    move-result v4

    .line 2339034
    const v7, 0x7f0a0369

    invoke-static {v2, v7}, LX/GMo;->a(Landroid/content/Context;I)I

    move-result v2

    .line 2339035
    const v7, 0x7f021a0c

    invoke-virtual {v0, v7, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;LX/GNP;)V
    .locals 0

    .prologue
    .line 2339036
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->r:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->s:LX/0ad;

    iput-object p3, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->t:LX/GNP;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-static {v2}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v2}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    new-instance p1, LX/GNP;

    invoke-static {v2}, LX/GG3;->a(LX/0QB;)LX/GG3;

    move-result-object v3

    check-cast v3, LX/GG3;

    invoke-direct {p1, v3}, LX/GNP;-><init>(LX/GG3;)V

    invoke-static {v2}, LX/GG3;->a(LX/0QB;)LX/GG3;

    move-result-object v3

    check-cast v3, LX/GG3;

    iput-object v3, p1, LX/GNP;->q:LX/GG3;

    move-object v2, p1

    check-cast v2, LX/GNP;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;LX/GNP;)V

    return-void
.end method

.method private b(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2339067
    if-nez p2, :cond_0

    .line 2339068
    :goto_0
    return-void

    .line 2339069
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->CardLayout:[I

    invoke-virtual {v0, p2, v1, v4, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 2339070
    const/16 v0, 0x0

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 2339071
    const/4 v0, 0x0

    .line 2339072
    if-eqz v2, :cond_1

    .line 2339073
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2339074
    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setHeaderTitle(Ljava/lang/String;)V

    .line 2339075
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2339076
    :cond_1
    const/16 v2, 0x1

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 2339077
    const/16 v3, 0x2

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    .line 2339078
    if-eqz v2, :cond_2

    .line 2339079
    invoke-direct {p0, v2, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(II)V

    .line 2339080
    :cond_2
    if-nez v0, :cond_3

    if-nez v2, :cond_3

    .line 2339081
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a:Lcom/facebook/widget/text/BetterTextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2339082
    :cond_3
    const/16 v0, 0x4

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 2339083
    if-eqz v0, :cond_4

    .line 2339084
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setCallToActionText(Ljava/lang/String;)V

    .line 2339085
    :cond_4
    const/16 v0, 0x3

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 2339086
    if-eqz v0, :cond_5

    .line 2339087
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterText(Ljava/lang/String;)V

    .line 2339088
    :cond_5
    invoke-direct {p0, p1, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Landroid/content/Context;Landroid/content/res/TypedArray;)V

    .line 2339089
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/List;Ljava/lang/String;)LX/8wQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;>;",
            "Ljava/lang/String;",
            ")",
            "LX/8wQ;"
        }
    .end annotation

    .prologue
    .line 2339037
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->b()LX/8wQ;

    move-result-object v0

    .line 2339038
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->k:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v2, LX/GJT;

    invoke-direct {v2, p0, p1, p2}, LX/GJT;-><init>(Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2339039
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->k:Lcom/facebook/fbui/glyph/GlyphButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2339040
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->n:Z

    .line 2339041
    return-object v0
.end method

.method public final a(ILandroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 2339090
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->b:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2339091
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2339092
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2339093
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;)V
    .locals 1

    .prologue
    .line 2339094
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;Landroid/view/View$OnClickListener;)V

    .line 2339095
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2339057
    if-eqz p2, :cond_0

    .line 2339058
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2339059
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0, v2, v2, v2}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2339060
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2339061
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->f:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/GJP;

    invoke-direct {v1, p0}, LX/GJP;-><init>(Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2339062
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->k:Lcom/facebook/fbui/glyph/GlyphButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2339063
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->k:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/GJQ;

    invoke-direct {v1, p0}, LX/GJQ;-><init>(Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2339064
    invoke-static {p0, p0}, LX/GMo;->a(Landroid/view/ViewGroup;Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 2339065
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->n:Z

    .line 2339066
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2339096
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->m:Z

    if-eqz v0, :cond_0

    .line 2339097
    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->b(Z)V

    .line 2339098
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->n:Z

    if-eqz v0, :cond_1

    .line 2339099
    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->k:Lcom/facebook/fbui/glyph/GlyphButton;

    if-eqz p1, :cond_3

    move v0, v2

    :goto_1
    invoke-virtual {v3, v0}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2339100
    :cond_1
    const v0, 0x7f0d08a9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2339101
    if-eqz p1, :cond_4

    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2339102
    return-void

    :cond_2
    move v0, v1

    .line 2339103
    goto :goto_0

    :cond_3
    move v0, v1

    .line 2339104
    goto :goto_1

    :cond_4
    move v1, v2

    .line 2339105
    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2339056
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 2339052
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getChildCount()I

    move-result v0

    const/16 v1, 0x9

    if-le v0, v1, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2339053
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getChildCount()I

    move-result v0

    add-int/lit8 p2, v0, -0x4

    .line 2339054
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2339055
    return-void
.end method

.method public final b()LX/8wQ;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2339047
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->r:Lcom/facebook/prefs/shared/FbSharedPreferences;

    new-instance v2, LX/0Tn;

    sget-object v3, LX/0Tm;->g:LX/0Tn;

    const-string v4, "adinterfaces_walkthrough_nux"

    invoke-direct {v2, v3, v4}, LX/0Tn;-><init>(LX/0To;Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2339048
    :goto_0
    return-object v0

    .line 2339049
    :cond_0
    new-instance v1, LX/GJV;

    invoke-direct {v1, p0}, LX/GJV;-><init>(Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2339050
    invoke-virtual {v1, v0}, LX/0b2;->b(LX/0b7;)V

    move-object v0, v1

    .line 2339051
    goto :goto_0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 2339042
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->m:Z

    if-nez v0, :cond_0

    .line 2339043
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->m:Z

    .line 2339044
    :cond_0
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->j:Lcom/facebook/fbui/glyph/GlyphButton;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2339045
    return-void

    .line 2339046
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final endTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 2338954
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->p:Z

    if-eqz v0, :cond_0

    .line 2338955
    invoke-static {p0}, LX/GMo;->a(Landroid/view/View;)V

    .line 2338956
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->p:Z

    .line 2338957
    return-void
.end method

.method public getHeader()Landroid/view/View;
    .locals 1

    .prologue
    .line 2338899
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public getHeaderTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2338900
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x4136c7e8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2338901
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onAttachedToWindow()V

    .line 2338902
    const/16 v1, 0x2d

    const v2, 0x1f5d73cc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x78285ad7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2338903
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 2338904
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->q:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->q:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2338905
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->q:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->end()V

    .line 2338906
    :cond_0
    const/16 v1, 0x2d

    const v2, -0xbe1983a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setCallToActionClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2338907
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2338908
    return-void
.end method

.method public setCallToActionText(I)V
    .locals 1

    .prologue
    .line 2338909
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setCallToActionText(Ljava/lang/String;)V

    .line 2338910
    return-void
.end method

.method public setCallToActionText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2338911
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2338912
    if-nez p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setCallToActionVisibility(I)V

    .line 2338913
    return-void

    .line 2338914
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCallToActionVisibility(I)V
    .locals 1

    .prologue
    .line 2338915
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2338916
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->h:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2338917
    return-void
.end method

.method public setFooterMovementMethod(Landroid/text/method/MovementMethod;)V
    .locals 1

    .prologue
    .line 2338918
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2338919
    return-void
.end method

.method public setFooterSpannableText(Landroid/text/Spanned;)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2338920
    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->c:Lcom/facebook/widget/text/BetterTextView;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2338921
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->d:Landroid/view/View;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2338922
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->c:Lcom/facebook/widget/text/BetterTextView;

    sget-object v1, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 2338923
    return-void

    :cond_0
    move v0, v2

    .line 2338924
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2338925
    goto :goto_1
.end method

.method public setFooterText(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2338926
    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->c:Lcom/facebook/widget/text/BetterTextView;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2338927
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->d:Landroid/view/View;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2338928
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2338929
    return-void

    :cond_0
    move v0, v2

    .line 2338930
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2338931
    goto :goto_1
.end method

.method public setFooterTextResource(I)V
    .locals 1

    .prologue
    .line 2338932
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2338933
    return-void
.end method

.method public setHeaderIconResource(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2338934
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2338935
    return-void
.end method

.method public setHeaderOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
    .locals 1

    .prologue
    .line 2338936
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2338937
    return-void
.end method

.method public setHeaderTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2338938
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2338939
    return-void
.end method

.method public setHeaderTitleResource(I)V
    .locals 1

    .prologue
    .line 2338940
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2338941
    return-void
.end method

.method public setHeaderVisibility(I)V
    .locals 1

    .prologue
    .line 2338942
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->o:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2338943
    return-void
.end method

.method public setPencilOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2338944
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->j:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2338945
    return-void
.end method

.method public final startTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 2338946
    return-void
.end method
