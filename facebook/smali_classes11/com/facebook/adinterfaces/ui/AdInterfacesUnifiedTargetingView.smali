.class public Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;
.super LX/GIa;
.source ""


# instance fields
.field public c:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

.field public d:Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

.field public e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

.field public f:Lcom/facebook/widget/text/BetterTextView;

.field public g:Lcom/facebook/widget/text/BetterEditTextView;

.field public h:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2343354
    invoke-direct {p0, p1}, LX/GIa;-><init>(Landroid/content/Context;)V

    .line 2343355
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2343352
    invoke-direct {p0, p1, p2}, LX/GIa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2343353
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2343350
    invoke-direct {p0, p1, p2, p3}, LX/GIa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2343351
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2343342
    invoke-super {p0}, LX/GIa;->a()V

    .line 2343343
    const v0, 0x7f0d0481

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    .line 2343344
    const v0, 0x7f0d0484

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    .line 2343345
    const v0, 0x7f0d0496

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->g:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2343346
    const v0, 0x7f0d0495

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2343347
    const v0, 0x7f0d0498

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

    .line 2343348
    const v0, 0x7f0d0497

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->h:Landroid/view/View;

    .line 2343349
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2343340
    const v0, 0x7f030095

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2343341
    return-void
.end method

.method public getAudienceOptionsView()Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;
    .locals 1

    .prologue
    .line 2343326
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    return-object v0
.end method

.method public bridge synthetic getAudienceOptionsView()Lcom/facebook/widget/CustomLinearLayout;
    .locals 1

    .prologue
    .line 2343338
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    move-object v0, v0

    .line 2343339
    return-object v0
.end method

.method public getSaveAudienceRowView()Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;
    .locals 1

    .prologue
    .line 2343337
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

    return-object v0
.end method

.method public setAudienceEditTextViewListener(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 2343335
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->g:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2343336
    return-void
.end method

.method public setAudienceNameEditVisibility(I)V
    .locals 1

    .prologue
    .line 2343332
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->g:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setVisibility(I)V

    .line 2343333
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2343334
    return-void
.end method

.method public setAudienceNameText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2343330
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->g:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2343331
    return-void
.end method

.method public setSaveAudienceRowVisibility(I)V
    .locals 1

    .prologue
    .line 2343327
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->h:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2343328
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->setVisibility(I)V

    .line 2343329
    return-void
.end method
