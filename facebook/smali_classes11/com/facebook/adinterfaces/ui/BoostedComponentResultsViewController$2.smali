.class public final Lcom/facebook/adinterfaces/ui/BoostedComponentResultsViewController$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/2tl;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:LX/0iA;

.field public final synthetic d:LX/GMM;


# direct methods
.method public constructor <init>(LX/GMM;LX/2tl;Landroid/view/View;LX/0iA;)V
    .locals 0

    .prologue
    .line 2344397
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/BoostedComponentResultsViewController$2;->d:LX/GMM;

    iput-object p2, p0, Lcom/facebook/adinterfaces/ui/BoostedComponentResultsViewController$2;->a:LX/2tl;

    iput-object p3, p0, Lcom/facebook/adinterfaces/ui/BoostedComponentResultsViewController$2;->b:Landroid/view/View;

    iput-object p4, p0, Lcom/facebook/adinterfaces/ui/BoostedComponentResultsViewController$2;->c:LX/0iA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2344386
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/BoostedComponentResultsViewController$2;->b:Landroid/view/View;

    .line 2344387
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2344388
    new-instance v2, LX/0hs;

    const/4 v3, 0x2

    invoke-direct {v2, v1, v3}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2344389
    const v3, 0x7f083685

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2344390
    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v2, v1}, LX/0ht;->a(LX/3AV;)V

    .line 2344391
    const/4 v1, -0x1

    .line 2344392
    iput v1, v2, LX/0hs;->t:I

    .line 2344393
    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {v2, v1}, LX/0ht;->b(F)V

    .line 2344394
    invoke-virtual {v2, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 2344395
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/BoostedComponentResultsViewController$2;->c:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/BoostedComponentResultsViewController$2;->a:LX/2tl;

    invoke-virtual {v1}, LX/2tl;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2344396
    return-void
.end method
