.class public Lcom/facebook/adinterfaces/ui/MapSpinnerView;
.super Landroid/view/View;
.source ""


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:Landroid/graphics/RectF;

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Landroid/graphics/LinearGradient;

.field private h:Landroid/view/animation/Animation;

.field private i:LX/GMY;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2344940
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2344941
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2344942
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2344937
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2344938
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2344939
    return-void
.end method

.method private a()V
    .locals 8

    .prologue
    const/high16 v3, 0x40400000    # 3.0f

    const v5, 0x3fb33333    # 1.4f

    .line 2344935
    new-instance v0, Landroid/graphics/LinearGradient;

    iget v1, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->d:I

    int-to-float v1, v1

    div-float/2addr v1, v3

    iget v2, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->d:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    iget v3, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->d:I

    int-to-float v3, v3

    div-float/2addr v3, v5

    iget v4, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->d:I

    int-to-float v4, v4

    div-float/2addr v4, v5

    iget v5, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->f:I

    const/4 v6, 0x0

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->g:Landroid/graphics/LinearGradient;

    .line 2344936
    return-void
.end method

.method private final a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 2344892
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 2344893
    const v0, 0x7f020063

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->setBackgroundResource(I)V

    .line 2344894
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->f:I

    .line 2344895
    const/high16 v0, 0x41f00000    # 30.0f

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->d:I

    .line 2344896
    const/high16 v0, 0x40800000    # 4.0f

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->e:I

    .line 2344897
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->b(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2344898
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04006c

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->h:Landroid/view/animation/Animation;

    .line 2344899
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->a:Landroid/graphics/Paint;

    .line 2344900
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2344901
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2344902
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->a:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->e:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2344903
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->a:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->f:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2344904
    iget v0, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->e:I

    int-to-double v0, v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->c:I

    .line 2344905
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->c:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->c:I

    int-to-float v2, v2

    iget v3, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->d:I

    int-to-float v3, v3

    iget v4, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->d:I

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->b:Landroid/graphics/RectF;

    .line 2344906
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->a()V

    .line 2344907
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2344931
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->a:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->g:Landroid/graphics/LinearGradient;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 2344932
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->b:Landroid/graphics/RectF;

    const/high16 v2, 0x42b40000    # 90.0f

    const/high16 v3, 0x43340000    # 180.0f

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 2344933
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->a:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 2344934
    return-void
.end method

.method private b(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 2344924
    if-nez p2, :cond_0

    .line 2344925
    :goto_0
    return-void

    .line 2344926
    :cond_0
    sget-object v0, LX/03r;->MapSpinnerView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2344927
    const/16 v1, 0x0

    iget v2, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->f:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->f:I

    .line 2344928
    const/16 v1, 0x1

    iget v2, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->e:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->e:I

    .line 2344929
    const/16 v1, 0x2

    iget v2, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->d:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->d:I

    .line 2344930
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/adinterfaces/ui/MapSpinnerView;)V
    .locals 1

    .prologue
    .line 2344922
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->h:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2344923
    return-void
.end method

.method private static getSweepAngle()F
    .locals 1

    .prologue
    .line 2344921
    const v0, 0x42fc0002    # 126.000015f

    return v0
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2344914
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2344915
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->i:LX/GMY;

    sget-object v1, LX/GMY;->IDLE:LX/GMY;

    if-ne v0, v1, :cond_0

    .line 2344916
    :goto_0
    return-void

    .line 2344917
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->a(Landroid/graphics/Canvas;)V

    .line 2344918
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->b:Landroid/graphics/RectF;

    const/high16 v2, 0x43340000    # 180.0f

    .line 2344919
    const v0, 0x42fc0002    # 126.000015f

    move v3, v0

    .line 2344920
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 2344912
    iget v0, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->d:I

    iget v1, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->e:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->d:I

    iget v2, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->e:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->setMeasuredDimension(II)V

    .line 2344913
    return-void
.end method

.method public setState(LX/GMY;)V
    .locals 1

    .prologue
    .line 2344908
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->i:LX/GMY;

    if-ne p1, v0, :cond_0

    .line 2344909
    :goto_0
    return-void

    .line 2344910
    :cond_0
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->i:LX/GMY;

    .line 2344911
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->i:LX/GMY;

    invoke-virtual {v0, p0}, LX/GMY;->process(Lcom/facebook/adinterfaces/ui/MapSpinnerView;)V

    goto :goto_0
.end method
