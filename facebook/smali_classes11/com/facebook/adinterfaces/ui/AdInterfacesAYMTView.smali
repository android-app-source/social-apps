.class public Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/resources/ui/FbButton;

.field private b:Lcom/facebook/fbui/widget/contentview/ContentView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2335588
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2335589
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;->a()V

    .line 2335590
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2335585
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2335586
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;->a()V

    .line 2335587
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2335582
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2335583
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;->a()V

    .line 2335584
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2335578
    const v0, 0x7f03004b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2335579
    const v0, 0x7f0d03e2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;->a:Lcom/facebook/resources/ui/FbButton;

    .line 2335580
    const v0, 0x7f0d03e1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2335581
    return-void
.end method


# virtual methods
.method public setAYMTButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2335576
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;->a:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2335577
    return-void
.end method

.method public setAYMTButtonText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2335565
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;->a:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2335566
    return-void
.end method

.method public setAYMTButtonVisibility(Z)V
    .locals 2

    .prologue
    .line 2335573
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;->a:Lcom/facebook/resources/ui/FbButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2335574
    return-void

    .line 2335575
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setContentText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2335571
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2335572
    return-void
.end method

.method public setFbDraweeImageURI(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 2335569
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2335570
    return-void
.end method

.method public setTitleText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2335567
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2335568
    return-void
.end method
