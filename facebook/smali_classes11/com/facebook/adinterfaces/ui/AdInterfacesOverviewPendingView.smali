.class public Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2340983
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2340984
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;->a()V

    .line 2340985
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2340980
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2340981
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;->a()V

    .line 2340982
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2340977
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2340978
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;->a()V

    .line 2340979
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2340973
    const v0, 0x7f030072

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2340974
    const v0, 0x7f0d044f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2340975
    const v0, 0x7f0d044e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2340976
    return-void
.end method


# virtual methods
.method public setCancelOrderContent(Landroid/text/Spanned;)V
    .locals 1

    .prologue
    .line 2340971
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2340972
    return-void
.end method

.method public setLearnMoreContent(Landroid/text/Spanned;)V
    .locals 1

    .prologue
    .line 2340969
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2340970
    return-void
.end method

.method public setMovementMethod(Landroid/text/method/MovementMethod;)V
    .locals 1

    .prologue
    .line 2340966
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2340967
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2340968
    return-void
.end method
