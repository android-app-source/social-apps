.class public Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

.field private b:[Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

.field public c:Ljava/lang/Long;

.field private d:Ljava/lang/String;

.field private e:Ljava/text/DateFormat;

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2342403
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2342404
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->d()V

    .line 2342405
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2342400
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2342401
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->d()V

    .line 2342402
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2342397
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2342398
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->d()V

    .line 2342399
    return-void
.end method

.method private a(IILjava/lang/CharSequence;)Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;
    .locals 4

    .prologue
    .line 2342391
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030083

    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    .line 2342392
    invoke-virtual {v0, p3}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTextTextViewStart(Ljava/lang/CharSequence;)V

    .line 2342393
    const v1, 0x7f0d0041

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTag(ILjava/lang/Object;)V

    .line 2342394
    const v1, 0x7f0d0040

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTag(ILjava/lang/Object;)V

    .line 2342395
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v1, v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->addView(Landroid/view/View;)V

    .line 2342396
    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 2342321
    const v0, 0x7f030084

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2342322
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->setOrientation(I)V

    .line 2342323
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->e:Ljava/text/DateFormat;

    .line 2342324
    const v0, 0x7f0d03da

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 2342325
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080a67

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->d:Ljava/lang/String;

    .line 2342326
    iput v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->f:I

    .line 2342327
    iput v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->g:I

    .line 2342328
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->h:I

    .line 2342329
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2342385
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->e:Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->c:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2342386
    new-instance v1, LX/47x;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2342387
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2342388
    const-string v2, "date"

    new-instance v3, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0e033a

    invoke-direct {v3, v4, v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const/16 v4, 0x21

    invoke-virtual {v1, v2, v0, v3, v4}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2342389
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->b:[Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    iget v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->f:I

    aget-object v0, v0, v2

    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTextTextViewStart(Ljava/lang/CharSequence;)V

    .line 2342390
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2342383
    iget v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->f:I

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->c(I)V

    .line 2342384
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2342379
    iget v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2342380
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->b:[Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    iget v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->g:I

    aget-object v0, v0, v1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setVisibility(I)V

    .line 2342381
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->b:[Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    iget v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->g:I

    aget-object v0, v0, v1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setChecked(Z)V

    .line 2342382
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 2342375
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->b:[Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2342376
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v1, p1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a(I)V

    .line 2342377
    return-void

    .line 2342378
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(I)V
    .locals 3

    .prologue
    .line 2342364
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->b:[Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 2342365
    iget v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->f:I

    if-eq v1, v0, :cond_0

    .line 2342366
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->b:[Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    aget-object v0, v0, v1

    .line 2342367
    const v2, 0x7f0d0040

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 2342368
    invoke-virtual {p0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->c(I)V

    .line 2342369
    :goto_1
    return-void

    .line 2342370
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2342371
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2342372
    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->add(II)V

    .line 2342373
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->setDate(Ljava/lang/Long;)V

    .line 2342374
    iget v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->f:I

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->c(I)V

    goto :goto_1
.end method

.method public getDate()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 2342363
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->c:Ljava/lang/Long;

    return-object v0
.end method

.method public getSelectedIndex()I
    .locals 2

    .prologue
    .line 2342357
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 2342358
    iget v1, v0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a:I

    move v0, v1

    .line 2342359
    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2342360
    if-eqz v0, :cond_0

    const v1, 0x7f0d0041

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2342361
    const v1, 0x7f0d0041

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2342362
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public setDate(Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 2342354
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->c:Ljava/lang/Long;

    .line 2342355
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->a()V

    .line 2342356
    return-void
.end method

.method public setDateOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 2342352
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->b:[Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    iget v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->f:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2342353
    return-void
.end method

.method public setOnCheckChangedListener(LX/Bc9;)V
    .locals 1

    .prologue
    .line 2342349
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 2342350
    iput-object p1, v0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->d:LX/Bc9;

    .line 2342351
    return-void
.end method

.method public setScheduleOptions(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;)V
    .locals 6

    .prologue
    .line 2342330
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;->j()LX/0Px;

    move-result-object v1

    .line 2342331
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->h:I

    .line 2342332
    iget v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->h:I

    new-array v0, v0, [Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->b:[Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    .line 2342333
    const/4 v0, 0x0

    .line 2342334
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel$EdgesModel;

    .line 2342335
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel$EdgesModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 2342336
    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->b:[Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel$EdgesModel$NodeModel;->a()I

    move-result v4

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v1, v4, v5}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->a(IILjava/lang/CharSequence;)Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    move-result-object v4

    aput-object v4, v3, v1

    .line 2342337
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel$EdgesModel$NodeModel;->a()I

    move-result v0

    if-nez v0, :cond_0

    .line 2342338
    iput v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->g:I

    .line 2342339
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 2342340
    goto :goto_0

    .line 2342341
    :cond_1
    iput v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->f:I

    .line 2342342
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->b:[Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    iget v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->f:I

    iget v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->f:I

    sget-object v3, LX/GG5;->SPECIFIC_DATE:LX/GG5;

    invoke-virtual {v3}, LX/GG5;->getDuration()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080a67

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->a(IILjava/lang/CharSequence;)Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    move-result-object v2

    aput-object v2, v0, v1

    .line 2342343
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;->a()I

    move-result v0

    .line 2342344
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 2342345
    const/4 v2, 0x5

    invoke-virtual {v1, v2, v0}, Ljava/util/Calendar;->add(II)V

    .line 2342346
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->c:Ljava/lang/Long;

    .line 2342347
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->a()V

    .line 2342348
    return-void
.end method
