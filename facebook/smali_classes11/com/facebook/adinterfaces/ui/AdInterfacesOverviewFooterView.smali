.class public Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/resources/ui/FbButton;

.field private b:Lcom/facebook/resources/ui/FbButton;

.field private c:Lcom/facebook/resources/ui/FbButton;

.field private d:Lcom/facebook/resources/ui/FbButton;

.field private e:Lcom/facebook/resources/ui/FbButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2340918
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2340919
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->a()V

    .line 2340920
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2340883
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2340884
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->a()V

    .line 2340885
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2340915
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2340916
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->a()V

    .line 2340917
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2340908
    const v0, 0x7f030071

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2340909
    const v0, 0x7f0d044a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->a:Lcom/facebook/resources/ui/FbButton;

    .line 2340910
    const v0, 0x7f0d044b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->b:Lcom/facebook/resources/ui/FbButton;

    .line 2340911
    const v0, 0x7f0d044c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->c:Lcom/facebook/resources/ui/FbButton;

    .line 2340912
    const v0, 0x7f0d0449

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->d:Lcom/facebook/resources/ui/FbButton;

    .line 2340913
    const v0, 0x7f0d044d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->e:Lcom/facebook/resources/ui/FbButton;

    .line 2340914
    return-void
.end method


# virtual methods
.method public setBoostAgainButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2340906
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->b:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2340907
    return-void
.end method

.method public setBoostAgainButtonVisibility(I)V
    .locals 1

    .prologue
    .line 2340904
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->b:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2340905
    return-void
.end method

.method public setDeleteBoostButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2340902
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2340903
    return-void
.end method

.method public setDeleteBoostButtonVisibility(I)V
    .locals 1

    .prologue
    .line 2340900
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2340901
    return-void
.end method

.method public setIncreaseBudgetButtonEnabled(Z)V
    .locals 1

    .prologue
    .line 2340898
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 2340899
    return-void
.end method

.method public setIncreaseBudgetButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2340896
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2340897
    return-void
.end method

.method public setIncreaseBudgetButtonVisibility(I)V
    .locals 1

    .prologue
    .line 2340894
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2340895
    return-void
.end method

.method public setPauseBoostButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2340892
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->a:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2340893
    return-void
.end method

.method public setPauseBoostButtonVisibility(I)V
    .locals 1

    .prologue
    .line 2340890
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->a:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2340891
    return-void
.end method

.method public setResumeBoostButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2340888
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->d:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2340889
    return-void
.end method

.method public setResumeBoostButtonVisibility(I)V
    .locals 1

    .prologue
    .line 2340886
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->d:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2340887
    return-void
.end method
