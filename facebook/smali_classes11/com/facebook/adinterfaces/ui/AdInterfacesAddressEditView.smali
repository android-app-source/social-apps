.class public Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterEditTextView;

.field private b:Lcom/facebook/maps/FbStaticMapView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2336269
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2336270
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->a()V

    .line 2336271
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2336291
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2336292
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->a()V

    .line 2336293
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2336288
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2336289
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->a()V

    .line 2336290
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2336282
    invoke-virtual {p0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->setOrientation(I)V

    .line 2336283
    const v0, 0x7f030043

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2336284
    const v0, 0x7f0d03d0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2336285
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setSingleLine(Z)V

    .line 2336286
    const v0, 0x7f0d03d1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/FbStaticMapView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->b:Lcom/facebook/maps/FbStaticMapView;

    .line 2336287
    return-void
.end method


# virtual methods
.method public getEditTextToken()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 2336281
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public setAddressString(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2336279
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2336280
    return-void
.end method

.method public setMapEnabled(Z)V
    .locals 2

    .prologue
    .line 2336276
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->b:Lcom/facebook/maps/FbStaticMapView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/maps/FbStaticMapView;->setVisibility(I)V

    .line 2336277
    return-void

    .line 2336278
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V
    .locals 1

    .prologue
    .line 2336274
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->b:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {v0, p1}, LX/3BP;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    .line 2336275
    return-void
.end method

.method public setOnAddressChangeListener(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 2336272
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2336273
    return-void
.end method
