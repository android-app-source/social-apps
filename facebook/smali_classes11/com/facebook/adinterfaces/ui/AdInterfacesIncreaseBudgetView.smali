.class public Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;
.super Lcom/facebook/adinterfaces/ui/BudgetOptionsView;
.source ""


# instance fields
.field public f:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2339650
    invoke-direct {p0, p1}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;-><init>(Landroid/content/Context;)V

    .line 2339651
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2339652
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2339653
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2339654
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2339655
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2339656
    invoke-super {p0}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->a()V

    .line 2339657
    const v0, 0x7f0d0428

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;->f:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    .line 2339658
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;->f:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    iget-object v0, p0, LX/GJD;->b:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTag(Ljava/lang/Object;)V

    .line 2339659
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2339660
    const v0, 0x7f030064

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2339661
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2339662
    iget-object v0, p0, LX/GJD;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;->f:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a(I)V

    .line 2339663
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 2339664
    iget-object v1, p0, LX/GJD;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->getId()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a(I)V

    .line 2339665
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2339666
    iget-object v0, p0, LX/GJD;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    iget-object v1, p0, LX/GJD;->b:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a(I)V

    .line 2339667
    return-void
.end method

.method public getLeaveUnchangedButton()Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;
    .locals 1

    .prologue
    .line 2339668
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;->f:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    return-object v0
.end method

.method public setLeaveUnchangedButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2339669
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;->f:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTextTextViewStart(Ljava/lang/CharSequence;)V

    .line 2339670
    return-void
.end method
