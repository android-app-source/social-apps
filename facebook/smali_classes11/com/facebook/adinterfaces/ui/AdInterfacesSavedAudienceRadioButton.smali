.class public Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;
.super Lcom/facebook/resources/ui/FbRadioButton;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field private b:Landroid/widget/TableLayout;

.field private c:I

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2342318
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbRadioButton;-><init>(Landroid/content/Context;)V

    .line 2342319
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->getCompoundPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->getCompoundDrawablePadding()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->c:I

    .line 2342320
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 2342315
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbRadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2342316
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->getCompoundPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->getCompoundDrawablePadding()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->c:I

    .line 2342317
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2342312
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbRadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2342313
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->getCompoundPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->getCompoundDrawablePadding()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->c:I

    .line 2342314
    return-void
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 2342290
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->b:Landroid/widget/TableLayout;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2342291
    :cond_0
    return-void

    .line 2342292
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 2342293
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v2, v4, :cond_7

    .line 2342294
    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getLayoutDirection()I

    move-result v2

    if-ne v2, v1, :cond_3

    :goto_0
    move v4, v1

    .line 2342295
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030046

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TableLayout;

    iput-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->b:Landroid/widget/TableLayout;

    .line 2342296
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->b:Landroid/widget/TableLayout;

    invoke-static {}, LX/473;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TableLayout;->setId(I)V

    move v1, v3

    .line 2342297
    :goto_2
    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 2342298
    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-ne v2, p0, :cond_4

    .line 2342299
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->b:Landroid/widget/TableLayout;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;I)V

    :cond_2
    move v5, v3

    .line 2342300
    :goto_3
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v5, v0, :cond_0

    .line 2342301
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030047

    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->b:Landroid/widget/TableLayout;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    .line 2342302
    const v1, 0x7f0d03d8

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    .line 2342303
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->d:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2342304
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->b:Landroid/widget/TableLayout;

    invoke-virtual {v2, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 2342305
    if-eqz v4, :cond_5

    move v6, v3

    :goto_4
    if-eqz v4, :cond_6

    iget v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->c:I

    :goto_5
    invoke-virtual {v1, v6, v3, v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    .line 2342306
    const v1, 0x7f0d03d9

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2342307
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->d:Ljava/util/List;

    add-int/lit8 v2, v5, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2342308
    add-int/lit8 v0, v5, 0x2

    move v5, v0

    goto :goto_3

    :cond_3
    move v1, v3

    .line 2342309
    goto/16 :goto_0

    .line 2342310
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2342311
    :cond_5
    iget v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->c:I

    move v6, v2

    goto :goto_4

    :cond_6
    move v2, v3

    goto :goto_5

    :cond_7
    move v4, v3

    goto/16 :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2342286
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 2342287
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->b:Landroid/widget/TableLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->removeView(Landroid/view/View;)V

    .line 2342288
    invoke-virtual {v0, p0}, Landroid/widget/RadioGroup;->removeView(Landroid/view/View;)V

    .line 2342289
    return-void
.end method

.method public getAudienceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2342275
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->a:Ljava/lang/String;

    return-object v0
.end method

.method public setAudienceId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2342284
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->a:Ljava/lang/String;

    .line 2342285
    return-void
.end method

.method public setChecked(Z)V
    .locals 2

    .prologue
    .line 2342278
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbRadioButton;->setChecked(Z)V

    .line 2342279
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->b()V

    .line 2342280
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->b:Landroid/widget/TableLayout;

    if-eqz v0, :cond_0

    .line 2342281
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->b:Landroid/widget/TableLayout;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TableLayout;->setVisibility(I)V

    .line 2342282
    :cond_0
    return-void

    .line 2342283
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setDescription(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2342276
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->d:Ljava/util/List;

    .line 2342277
    return-void
.end method
