.class public Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/Spinner;

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2342744
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2342745
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->b()V

    .line 2342746
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2342741
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2342742
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->b()V

    .line 2342743
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2342714
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2342715
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->b()V

    .line 2342716
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2342729
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->setOrientation(I)V

    .line 2342730
    const v0, 0x7f030089

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2342731
    const v0, 0x7f0d047c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->a:Landroid/widget/Spinner;

    .line 2342732
    const v0, 0x7f0d047a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2342733
    const v0, 0x7f0d047b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2342734
    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/SpinnerAdapter;Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2342735
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 2342736
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, p2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 2342737
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2342738
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2342739
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->a:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 2342740
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/16 v0, 0x8

    const/4 v1, 0x0

    .line 2342721
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->a:Landroid/widget/Spinner;

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 2342722
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2342723
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2342724
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->c:Lcom/facebook/widget/text/BetterTextView;

    if-nez p2, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2342725
    if-nez p2, :cond_1

    .line 2342726
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 2342727
    goto :goto_0

    .line 2342728
    :cond_1
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2342720
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->a:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public getSelectedItem()Ljava/lang/Object;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2342719
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->a:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public setSelected(I)V
    .locals 1

    .prologue
    .line 2342717
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 2342718
    return-void
.end method
