.class public Lcom/facebook/adinterfaces/ui/PacingOptionsView;
.super Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;
.source ""


# instance fields
.field public b:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public c:Lcom/facebook/fbui/widget/contentview/CheckedContentView;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public d:Lcom/facebook/fbui/widget/contentview/CheckedContentView;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public e:Lcom/facebook/adinterfaces/ui/AdInterfacesPacingInfoView;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2344954
    invoke-direct {p0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;-><init>(Landroid/content/Context;)V

    .line 2344955
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2344952
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2344953
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2344956
    const v0, 0x7f030e00

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2344957
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 2344958
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02007f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2344959
    const v0, 0x7f0d2234

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    .line 2344960
    const v0, 0x7f0d2232

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->c:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 2344961
    const v0, 0x7f0d2233

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->d:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 2344962
    const v0, 0x7f0d2235

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesPacingInfoView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesPacingInfoView;

    .line 2344963
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->c:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->d:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->a:LX/0Px;

    .line 2344964
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->b()V

    .line 2344965
    const/4 v0, 0x0

    .line 2344966
    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->setSelected(I)V

    .line 2344967
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    .line 2344944
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->c:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setVisibility(I)V

    .line 2344945
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->d:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setVisibility(I)V

    .line 2344946
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->setVisibility(I)V

    .line 2344947
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesPacingInfoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesPacingInfoView;->setVisibility(I)V

    .line 2344948
    if-eqz p1, :cond_0

    .line 2344949
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesPacingInfoView;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080b42

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080b43

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesPacingInfoView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2344950
    :goto_0
    return-void

    .line 2344951
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesPacingInfoView;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080b44

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080b45

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesPacingInfoView;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getBidAmountEditView()Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;
    .locals 1

    .prologue
    .line 2344943
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    return-object v0
.end method
