.class public Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;
.super Lcom/facebook/resources/ui/FbRadioButton;
.source ""


# instance fields
.field private a:Landroid/widget/TableLayout;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:I

.field private e:Lcom/facebook/widget/text/BetterTextView;

.field public f:Ljava/lang/String;

.field public g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2341776
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbRadioButton;-><init>(Landroid/content/Context;)V

    .line 2341777
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2341849
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbRadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2341850
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2341847
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbRadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2341848
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2341839
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 2341840
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v3, v4, :cond_3

    .line 2341841
    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getLayoutDirection()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    .line 2341842
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->getCompoundPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->getCompoundDrawablePadding()I

    move-result v3

    add-int/2addr v1, v3

    .line 2341843
    if-eqz v0, :cond_1

    move v3, v2

    :goto_1
    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v3, v2, v0, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 2341844
    return-void

    :cond_0
    move v0, v2

    .line 2341845
    goto :goto_0

    :cond_1
    move v3, v1

    .line 2341846
    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2341834
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aget-object v2, v2, v4

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 2341835
    :cond_1
    :goto_0
    return v0

    .line 2341836
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->getRight()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int/2addr v2, v3

    .line 2341837
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->getRight()I

    move-result v3

    .line 2341838
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    int-to-float v2, v2

    cmpl-float v2, v4, v2

    if-ltz v2, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-lez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static synthetic a(Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2341833
    invoke-direct {p0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 2341807
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->a:Landroid/widget/TableLayout;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2341808
    :cond_0
    return-void

    .line 2341809
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 2341810
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030046

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TableLayout;

    iput-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->a:Landroid/widget/TableLayout;

    .line 2341811
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->a:Landroid/widget/TableLayout;

    invoke-static {}, LX/473;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TableLayout;->setId(I)V

    move v1, v3

    .line 2341812
    :goto_0
    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 2341813
    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-ne v2, p0, :cond_5

    .line 2341814
    iget-boolean v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->g:Z

    if-eqz v2, :cond_4

    .line 2341815
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->a:Landroid/widget/TableLayout;

    add-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v2, v1}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;I)V

    .line 2341816
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2341817
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03007d

    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->a:Landroid/widget/TableLayout;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2341818
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->e:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341819
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->a:Landroid/widget/TableLayout;

    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 2341820
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-direct {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->a(Landroid/view/View;)V

    .line 2341821
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->e:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    :cond_3
    move v4, v3

    .line 2341822
    :goto_2
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_0

    .line 2341823
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030047

    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->a:Landroid/widget/TableLayout;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    .line 2341824
    const v1, 0x7f0d03d8

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    .line 2341825
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->b:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341826
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->a:Landroid/widget/TableLayout;

    invoke-virtual {v2, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 2341827
    invoke-direct {p0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->a(Landroid/view/View;)V

    .line 2341828
    const v1, 0x7f0d03d9

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2341829
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->b:Ljava/util/List;

    add-int/lit8 v2, v4, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341830
    add-int/lit8 v0, v4, 0x2

    move v4, v0

    goto :goto_2

    .line 2341831
    :cond_4
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->a:Landroid/widget/TableLayout;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;I)V

    goto/16 :goto_1

    .line 2341832
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2341803
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 2341804
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->a:Landroid/widget/TableLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->removeView(Landroid/view/View;)V

    .line 2341805
    invoke-virtual {v0, p0}, Landroid/widget/RadioGroup;->removeView(Landroid/view/View;)V

    .line 2341806
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2341851
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 2341852
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03008b

    invoke-virtual {v1, v2, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 2341853
    invoke-direct {p0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->a(Landroid/view/View;)V

    .line 2341854
    invoke-virtual {v0, p0}, Landroid/widget/RadioGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    .line 2341855
    if-ltz v2, :cond_0

    .line 2341856
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;I)V

    .line 2341857
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2341858
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v4, 0x7f03008a

    invoke-virtual {v2, v4, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/text/BetterTextView;

    .line 2341859
    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341860
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 2341861
    :cond_1
    return-void
.end method

.method public setButtonOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 2341800
    new-instance v0, LX/GKx;

    invoke-direct {v0, p0, p1}, LX/GKx;-><init>(Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;Landroid/view/View$OnTouchListener;)V

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2341801
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->c:Z

    .line 2341802
    return-void
.end method

.method public setChecked(Z)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x2

    const/4 v1, 0x0

    .line 2341788
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbRadioButton;->setChecked(Z)V

    .line 2341789
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->b()V

    .line 2341790
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->a:Landroid/widget/TableLayout;

    if-eqz v0, :cond_0

    .line 2341791
    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->a:Landroid/widget/TableLayout;

    if-eqz p1, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TableLayout;->setVisibility(I)V

    .line 2341792
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->e:Lcom/facebook/widget/text/BetterTextView;

    if-eqz v0, :cond_1

    .line 2341793
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2341794
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->c:Z

    if-eqz v0, :cond_4

    if-eqz p1, :cond_4

    const v0, 0x7f020952

    :goto_1
    invoke-virtual {p0, v1, v1, v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 2341795
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v4

    if-eqz v0, :cond_2

    .line 2341796
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v4

    iget v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->d:I

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2341797
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 2341798
    goto :goto_0

    :cond_4
    move v0, v1

    .line 2341799
    goto :goto_1
.end method

.method public setDescription(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2341786
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->b:Ljava/util/List;

    .line 2341787
    return-void
.end method

.method public setHasDisplayTags(Z)V
    .locals 0

    .prologue
    .line 2341785
    iput-boolean p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->g:Z

    return-void
.end method

.method public setIconColor(I)V
    .locals 1

    .prologue
    .line 2341782
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->c:Z

    .line 2341783
    iput p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->d:I

    .line 2341784
    return-void
.end method

.method public setMessageRowText(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2341780
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->f:Ljava/lang/String;

    .line 2341781
    return-void
.end method

.method public setMessageRowVisibility(I)V
    .locals 1

    .prologue
    .line 2341778
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2341779
    return-void
.end method
