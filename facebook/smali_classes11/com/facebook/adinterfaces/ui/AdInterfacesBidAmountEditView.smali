.class public Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/widget/text/BetterEditTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2337308
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2337309
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->b()V

    .line 2337310
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2337305
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2337306
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->b()V

    .line 2337307
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2337302
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2337303
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->b()V

    .line 2337304
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2337281
    const v0, 0x7f03004c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2337282
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->setOrientation(I)V

    .line 2337283
    const v0, 0x7f0d03e6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->b:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2337284
    const v0, 0x7f0d03e5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2337285
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 2337300
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2337301
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2337299
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->requestFocus()Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 2337297
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2337298
    return-void
.end method

.method public getBidAmountText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2337296
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setBidAmount(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2337294
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2337295
    return-void
.end method

.method public setCurrencySymbol(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2337292
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2337293
    return-void
.end method

.method public setCursorVisible(Z)V
    .locals 1

    .prologue
    .line 2337290
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setCursorVisible(Z)V

    .line 2337291
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 2337288
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setEnabled(Z)V

    .line 2337289
    return-void
.end method

.method public setOnFocusChangeListenerEditText(Landroid/view/View$OnFocusChangeListener;)V
    .locals 1

    .prologue
    .line 2337286
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2337287
    return-void
.end method
