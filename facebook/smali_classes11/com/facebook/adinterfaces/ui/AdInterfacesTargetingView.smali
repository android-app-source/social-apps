.class public Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;
.super LX/GIa;
.source ""


# instance fields
.field public c:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2342775
    invoke-direct {p0, p1}, LX/GIa;-><init>(Landroid/content/Context;)V

    .line 2342776
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2342765
    invoke-direct {p0, p1, p2}, LX/GIa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2342766
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2342773
    invoke-direct {p0, p1, p2, p3}, LX/GIa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2342774
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2342770
    invoke-super {p0}, LX/GIa;->a()V

    .line 2342771
    const v0, 0x7f0d0481

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    .line 2342772
    return-void
.end method

.method public getAudienceOptionsView()Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;
    .locals 1

    .prologue
    .line 2342769
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    return-object v0
.end method

.method public bridge synthetic getAudienceOptionsView()Lcom/facebook/widget/CustomLinearLayout;
    .locals 1

    .prologue
    .line 2342767
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    move-object v0, v0

    .line 2342768
    return-object v0
.end method
