.class public Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field public static final a:I

.field public static final b:I


# instance fields
.field private c:Landroid/widget/RadioGroup;

.field private d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/resources/ui/FbRadioButton;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2336482
    const v0, 0x7f0d03dd

    sput v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->a:I

    .line 2336483
    const v0, 0x7f0d03db

    sput v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2336479
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2336480
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->a()V

    .line 2336481
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2336476
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2336477
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->a()V

    .line 2336478
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2336473
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2336474
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->a()V

    .line 2336475
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2336465
    const v0, 0x7f030048

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2336466
    const v0, 0x7f0d03da

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->c:Landroid/widget/RadioGroup;

    .line 2336467
    const v0, 0x7f0d03db

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbRadioButton;

    const v1, 0x7f0d03dd

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbRadioButton;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->d:LX/0Px;

    .line 2336468
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2336469
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbRadioButton;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbRadioButton;->setTag(Ljava/lang/Object;)V

    .line 2336470
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2336471
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->c:Landroid/widget/RadioGroup;

    invoke-static {v0}, LX/GMo;->a(Landroid/view/ViewGroup;)V

    .line 2336472
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;"
        }
    .end annotation

    .prologue
    .line 2336454
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030081

    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->c:Landroid/widget/RadioGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;

    .line 2336455
    invoke-static {}, LX/473;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->setId(I)V

    .line 2336456
    iput-object p1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->a:Ljava/lang/String;

    .line 2336457
    invoke-virtual {v0, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 2336458
    iput-object p3, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->d:Ljava/util/List;

    .line 2336459
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->c:Landroid/widget/RadioGroup;

    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->c:Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v0, v2}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;I)V

    .line 2336460
    return-object v0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 2336463
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->c:Landroid/widget/RadioGroup;

    invoke-virtual {v0, p1}, Landroid/widget/RadioGroup;->check(I)V

    .line 2336464
    return-void
.end method

.method public setOnCheckChangedListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 2336461
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->c:Landroid/widget/RadioGroup;

    invoke-virtual {v0, p1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 2336462
    return-void
.end method
