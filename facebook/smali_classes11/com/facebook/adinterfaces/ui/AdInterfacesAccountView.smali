.class public Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2335660
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2335661
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2335662
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2335663
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2335664
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2335665
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;->a()V

    .line 2335666
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2335667
    const v0, 0x7f03003e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2335668
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;->setOrientation(I)V

    .line 2335669
    const v0, 0x7f0d03c9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;

    .line 2335670
    return-void
.end method


# virtual methods
.method public getSpinnerView()Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;
    .locals 1

    .prologue
    .line 2335671
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;

    return-object v0
.end method
