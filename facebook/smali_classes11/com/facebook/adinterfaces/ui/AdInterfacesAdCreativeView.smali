.class public Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:I

.field private c:Lcom/facebook/fbui/widget/text/BadgeTextView;

.field private d:Landroid/view/View;

.field private e:Lcom/facebook/widget/text/BetterEditTextView;

.field private f:Lcom/facebook/widget/text/BetterEditTextView;

.field private g:Landroid/view/View;

.field private h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private i:Landroid/widget/TextView;

.field private j:Lcom/facebook/fbui/widget/text/BadgeTextView;

.field private k:Lcom/facebook/widget/text/BetterEditTextView;

.field private l:Lcom/facebook/widget/CustomLinearLayout;

.field private m:Lcom/facebook/widget/CustomLinearLayout;

.field private n:Landroid/view/View;

.field private o:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2336189
    const-class v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2336190
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2336191
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->n()V

    .line 2336192
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2336193
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2336194
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->n()V

    .line 2336195
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2336175
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2336176
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->n()V

    .line 2336177
    return-void
.end method

.method private n()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2336196
    const v0, 0x7f030057

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2336197
    invoke-virtual {p0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->setOrientation(I)V

    .line 2336198
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->b:I

    .line 2336199
    const v0, 0x7f0d0400

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/BadgeTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->c:Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2336200
    const v0, 0x7f0d0401

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->f:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2336201
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->f:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setSingleLine(Z)V

    .line 2336202
    const v0, 0x7f0d0402

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->d:Landroid/view/View;

    .line 2336203
    const v0, 0x7f0d0404

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2336204
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setSingleLine(Z)V

    .line 2336205
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setHorizontallyScrolling(Z)V

    .line 2336206
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0019

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setMaxLines(I)V

    .line 2336207
    const v0, 0x7f0d0406

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->g:Landroid/view/View;

    .line 2336208
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->c:Lcom/facebook/fbui/widget/text/BadgeTextView;

    iget v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->b:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 2336209
    const v0, 0x7f0d0407

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2336210
    const v0, 0x7f0d0408

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->i:Landroid/widget/TextView;

    .line 2336211
    const v0, 0x7f0d040d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/BadgeTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->j:Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2336212
    const v0, 0x7f0d040e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->k:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2336213
    const v0, 0x7f0d040c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->o:Landroid/view/View;

    .line 2336214
    const v0, 0x7f0d0409

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->l:Lcom/facebook/widget/CustomLinearLayout;

    .line 2336215
    const v0, 0x7f0d040b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->n:Landroid/view/View;

    .line 2336216
    const v0, 0x7f0d040a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->m:Lcom/facebook/widget/CustomLinearLayout;

    .line 2336217
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;
    .locals 4

    .prologue
    .line 2336218
    new-instance v0, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;-><init>(Landroid/content/Context;)V

    .line 2336219
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->a()V

    .line 2336220
    iget-object v1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2336221
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->l:Lcom/facebook/widget/CustomLinearLayout;

    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->l:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v2}, Lcom/facebook/widget/CustomLinearLayout;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v0, v2}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;I)V

    .line 2336222
    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2336223
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->c:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setVisibility(I)V

    .line 2336224
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->f:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setVisibility(I)V

    .line 2336225
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2336226
    return-void
.end method

.method public final a(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 2336227
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->k:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2336228
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;)V
    .locals 1

    .prologue
    .line 2336229
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->l:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/CustomLinearLayout;->removeView(Landroid/view/View;)V

    .line 2336230
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2336251
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->c:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setVisibility(I)V

    .line 2336252
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->f:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setVisibility(I)V

    .line 2336253
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2336254
    return-void
.end method

.method public final b(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 2336231
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->f:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2336232
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2336233
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->g:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2336234
    return-void
.end method

.method public final c(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 2336235
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2336236
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2336237
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->g:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2336238
    return-void
.end method

.method public final d(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 2336239
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->f:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2336240
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2336241
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2336242
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->l:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 2336243
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2336244
    return-void
.end method

.method public final e(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 2336245
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2336246
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2336247
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2336248
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->l:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 2336249
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2336250
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2336184
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->j:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setVisibility(I)V

    .line 2336185
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->k:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setVisibility(I)V

    .line 2336186
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2336187
    return-void
.end method

.method public getMaxHeadlineLength()I
    .locals 1

    .prologue
    .line 2336188
    iget v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->b:I

    return v0
.end method

.method public final h()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2336141
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->j:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setVisibility(I)V

    .line 2336142
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->k:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setVisibility(I)V

    .line 2336143
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2336144
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 2336145
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->k:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->requestFocus()Z

    .line 2336146
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->k:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->k:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setSelection(I)V

    .line 2336147
    return-void
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 2336148
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->f:Lcom/facebook/widget/text/BetterEditTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2336149
    return-void
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 2336150
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2336151
    return-void
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 2336152
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->m:Lcom/facebook/widget/CustomLinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 2336153
    return-void
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 2336154
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->m:Lcom/facebook/widget/CustomLinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 2336155
    return-void
.end method

.method public setAdImageThumbnail(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2336156
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2336157
    return-void
.end method

.method public setAddImageListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2336158
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->m:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2336159
    return-void
.end method

.method public setDescriptionText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2336160
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2336161
    return-void
.end method

.method public setHeadlineLabelText(Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 2336162
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->c:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setText(I)V

    .line 2336163
    return-void
.end method

.method public setHeadlineOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 1

    .prologue
    .line 2336164
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->f:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2336165
    return-void
.end method

.method public setHeadlineRemainingCharacters(I)V
    .locals 2

    .prologue
    .line 2336166
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->c:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 2336167
    return-void
.end method

.method public setHeadlineText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2336168
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->f:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2336169
    return-void
.end method

.method public setMaxHeadlineLength(I)V
    .locals 4

    .prologue
    .line 2336170
    iput p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->b:I

    .line 2336171
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->f:Lcom/facebook/widget/text/BetterEditTextView;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v3, p1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setFilters([Landroid/text/InputFilter;)V

    .line 2336172
    return-void
.end method

.method public setMultiHeadlineRemainingCharacters(I)V
    .locals 2

    .prologue
    .line 2336173
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->j:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 2336174
    return-void
.end method

.method public setMultiHeadlineText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2336178
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->k:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2336179
    return-void
.end method

.method public setOnImagePickerButtonClick(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2336180
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->g:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2336181
    return-void
.end method

.method public setTextOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 1

    .prologue
    .line 2336182
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2336183
    return-void
.end method
