.class public Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Lcom/facebook/widget/text/BetterTextView;

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:Lcom/facebook/widget/text/BetterTextView;

.field private f:Lcom/facebook/widget/text/BetterTextView;

.field private g:Lcom/facebook/widget/text/BetterTextView;

.field private h:Lcom/facebook/widget/text/BetterTextView;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2345038
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2345039
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->a()V

    .line 2345040
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2345035
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2345036
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->a()V

    .line 2345037
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2345032
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2345033
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->a()V

    .line 2345034
    return-void
.end method

.method private static a(F)Landroid/widget/LinearLayout$LayoutParams;
    .locals 3

    .prologue
    .line 2345031
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2, p0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2345018
    const v0, 0x7f0312e8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2345019
    const v0, 0x7f0d2c06

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2345020
    const v0, 0x7f0d0432

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2345021
    const v0, 0x7f0d0434

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2345022
    const v0, 0x7f0d0437

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2345023
    const v0, 0x7f0d2c05

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2345024
    const v0, 0x7f0d0433

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2345025
    const v0, 0x7f0d0435

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 2345026
    const v0, 0x7f0d0438

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->h:Lcom/facebook/widget/text/BetterTextView;

    .line 2345027
    const v0, 0x7f0d2c07

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->i:Landroid/view/View;

    .line 2345028
    const v0, 0x7f0d2c08

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->j:Landroid/view/View;

    .line 2345029
    const v0, 0x7f0d2c09

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->k:Landroid/view/View;

    .line 2345030
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2345015
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2345016
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2345017
    return-void
.end method

.method private setBarWeights(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 2345009
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->i:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v0}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->a(F)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2345010
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-lt v0, v2, :cond_0

    .line 2345011
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->j:Landroid/view/View;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v0}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->a(F)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2345012
    :cond_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_1

    .line 2345013
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->k:Landroid/view/View;

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v0}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->a(F)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2345014
    :cond_1
    return-void
.end method

.method private setColors(LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 2345000
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->i:Landroid/view/View;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2345001
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2345002
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-lt v0, v3, :cond_0

    .line 2345003
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->j:Landroid/view/View;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2345004
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2345005
    :cond_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 2345006
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->k:Landroid/view/View;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2345007
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2345008
    :cond_1
    return-void
.end method

.method private setDataText(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 2344994
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->c:Lcom/facebook/widget/text/BetterTextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2344995
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-lt v0, v2, :cond_0

    .line 2344996
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->d:Lcom/facebook/widget/text/BetterTextView;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2344997
    :cond_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 2344998
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2344999
    :cond_1
    return-void
.end method

.method private setLabels(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 2344968
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->f:Lcom/facebook/widget/text/BetterTextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2344969
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-lt v0, v2, :cond_0

    .line 2344970
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->g:Lcom/facebook/widget/text/BetterTextView;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2344971
    :cond_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 2344972
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->h:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2344973
    :cond_1
    return-void
.end method


# virtual methods
.method public setViewModel(LX/GGA;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2344974
    iget v0, p1, LX/GGA;->a:I

    move v0, v0

    .line 2344975
    const/4 v1, 0x2

    if-gt v0, v1, :cond_0

    .line 2344976
    const v0, 0x7f0d0436

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2344977
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2344978
    :cond_0
    iget v0, p1, LX/GGA;->a:I

    move v0, v0

    .line 2344979
    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    .line 2344980
    const v0, 0x7f0d2c0a

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2344981
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2344982
    :cond_1
    iget-object v0, p1, LX/GGA;->g:LX/0Px;

    move-object v0, v0

    .line 2344983
    invoke-direct {p0, v0}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->setColors(LX/0Px;)V

    .line 2344984
    iget-object v0, p1, LX/GGA;->e:LX/0Px;

    move-object v0, v0

    .line 2344985
    invoke-direct {p0, v0}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->setLabels(LX/0Px;)V

    .line 2344986
    iget-object v0, p1, LX/GGA;->f:LX/0Px;

    move-object v0, v0

    .line 2344987
    invoke-direct {p0, v0}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->setBarWeights(LX/0Px;)V

    .line 2344988
    iget-object v0, p1, LX/GGA;->d:LX/0Px;

    move-object v0, v0

    .line 2344989
    invoke-direct {p0, v0}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->setDataText(LX/0Px;)V

    .line 2344990
    iget-object v0, p1, LX/GGA;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2344991
    iget-object v1, p1, LX/GGA;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2344992
    invoke-direct {p0, v0, v1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2344993
    return-void
.end method
