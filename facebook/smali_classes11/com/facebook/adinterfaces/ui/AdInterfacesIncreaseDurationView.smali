.class public Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

.field public d:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2339803
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2339804
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->b()V

    .line 2339805
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2339800
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2339801
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->b()V

    .line 2339802
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2339771
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2339772
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->b()V

    .line 2339773
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2339810
    const v0, 0x7f030066

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2339811
    const v0, 0x7f0d03da

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 2339812
    const v0, 0x7f0d042b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->c:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    .line 2339813
    const v0, 0x7f0d042f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->d:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    .line 2339814
    const v0, 0x7f0d042c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    const v1, 0x7f0d042d

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    const v2, 0x7f0d042e

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->b:LX/0Px;

    .line 2339815
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2339816
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTag(Ljava/lang/Object;)V

    .line 2339817
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2339818
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->c:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080b91

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTextTextViewStart(Ljava/lang/CharSequence;)V

    .line 2339819
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2339806
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2339807
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setVisibility(I)V

    .line 2339808
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2339809
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 2339820
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setVisibility(I)V

    .line 2339821
    return-void
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 2339797
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v0, p1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a(I)V

    .line 2339798
    return-void
.end method

.method public getCustomDurationButton()Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;
    .locals 1

    .prologue
    .line 2339799
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->d:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    return-object v0
.end method

.method public getLeaveUnchangedButton()Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;
    .locals 1

    .prologue
    .line 2339796
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->c:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    return-object v0
.end method

.method public getRadioButtonList()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2339795
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->b:LX/0Px;

    return-object v0
.end method

.method public getSelectedIndex()I
    .locals 2

    .prologue
    .line 2339789
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 2339790
    iget v1, v0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a:I

    move v0, v1

    .line 2339791
    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2339792
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2339793
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2339794
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public setCustomDurationButtonVisibility(I)V
    .locals 1

    .prologue
    .line 2339787
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->d:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setVisibility(I)V

    .line 2339788
    return-void
.end method

.method public setCustomDurationDate(Ljava/lang/Long;)V
    .locals 6

    .prologue
    .line 2339779
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    .line 2339780
    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2339781
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080a67

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2339782
    new-instance v2, LX/47x;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2339783
    invoke-virtual {v2, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2339784
    const-string v1, "date"

    new-instance v3, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0e033a

    invoke-direct {v3, v4, v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const/16 v4, 0x21

    invoke-virtual {v2, v1, v0, v3, v4}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2339785
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->d:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-virtual {v2}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTextTextViewStart(Ljava/lang/CharSequence;)V

    .line 2339786
    return-void
.end method

.method public setDateOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2339777
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->d:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2339778
    return-void
.end method

.method public setOnCheckChangedListener(LX/Bc9;)V
    .locals 1

    .prologue
    .line 2339774
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 2339775
    iput-object p1, v0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->d:LX/Bc9;

    .line 2339776
    return-void
.end method
