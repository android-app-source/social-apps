.class public Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/widget/text/BetterEditTextView;

.field private b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2341463
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2341464
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;->a()V

    .line 2341465
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2341466
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2341467
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;->a()V

    .line 2341468
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2341469
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2341470
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;->a()V

    .line 2341471
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2341472
    const v0, 0x7f030076

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2341473
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;->setOrientation(I)V

    .line 2341474
    const v0, 0x7f0d0463

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2341475
    const v0, 0x7f0d0462

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2341476
    return-void
.end method


# virtual methods
.method public getPhoneNumberEditView()Lcom/facebook/widget/text/BetterEditTextView;
    .locals 1

    .prologue
    .line 2341477
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    return-object v0
.end method

.method public setCountryCode(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 2341478
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;->b:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341479
    return-void
.end method
