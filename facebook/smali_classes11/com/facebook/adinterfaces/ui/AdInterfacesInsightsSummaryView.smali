.class public Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Lcom/facebook/widget/text/BetterTextView;

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:Lcom/facebook/widget/text/BetterTextView;

.field private f:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2340218
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2340219
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->b()V

    .line 2340220
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2340215
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2340216
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->b()V

    .line 2340217
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2340212
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2340213
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->b()V

    .line 2340214
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2340204
    const v0, 0x7f03006a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2340205
    const v0, 0x7f0d0432

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2340206
    const v0, 0x7f0d0434

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2340207
    const v0, 0x7f0d0437

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2340208
    const v0, 0x7f0d0433

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2340209
    const v0, 0x7f0d0435

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2340210
    const v0, 0x7f0d0438

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2340211
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2340199
    const v0, 0x7f0d0431

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    .line 2340200
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 2340201
    const v0, 0x7f0d0430

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2340202
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2340203
    return-void
.end method

.method public setFirstDataLabel(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2340221
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2340222
    return-void
.end method

.method public setFirstDataValue(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2340197
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2340198
    return-void
.end method

.method public setSecondDataLabel(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2340189
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2340190
    return-void
.end method

.method public setSecondDataValue(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2340195
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2340196
    return-void
.end method

.method public setThirdDataLabel(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2340193
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2340194
    return-void
.end method

.method public setThirdDataValue(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2340191
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2340192
    return-void
.end method
