.class public Lcom/facebook/adinterfaces/ui/UploadImageHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/4At;

.field public final c:LX/0aG;

.field public final d:LX/1Ck;

.field public e:LX/2U3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2345140
    const-class v0, LX/GHx;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/adinterfaces/ui/UploadImageHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/1Ck;LX/2U3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2345132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2345133
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/UploadImageHelper;->c:LX/0aG;

    .line 2345134
    iput-object p2, p0, Lcom/facebook/adinterfaces/ui/UploadImageHelper;->d:LX/1Ck;

    .line 2345135
    iput-object p3, p0, Lcom/facebook/adinterfaces/ui/UploadImageHelper;->e:LX/2U3;

    .line 2345136
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2345137
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/UploadImageHelper;->b:LX/4At;

    if-eqz v0, :cond_0

    .line 2345138
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/UploadImageHelper;->b:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2345139
    :cond_0
    return-void
.end method
