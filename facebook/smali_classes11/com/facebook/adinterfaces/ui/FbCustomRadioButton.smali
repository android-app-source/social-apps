.class public Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Landroid/widget/Checkable;
.implements LX/BcB;


# instance fields
.field public a:Z

.field public b:Z

.field public c:Landroid/widget/ImageButton;

.field private d:LX/Bc8;

.field private e:Lcom/facebook/widget/text/BetterTextView;

.field private f:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2342877
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2342878
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2342874
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2342875
    invoke-virtual {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2342876
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2342871
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2342872
    invoke-virtual {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2342873
    return-void
.end method

.method private b(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2342865
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->FbCustomRadioButton:[I

    invoke-virtual {v0, p2, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2342866
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 2342867
    if-eqz v1, :cond_0

    .line 2342868
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTextTextViewStart(Ljava/lang/CharSequence;)V

    .line 2342869
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2342870
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2342847
    const v0, 0x7f03060b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2342848
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setClickable(Z)V

    .line 2342849
    const v0, 0x7f0d032f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->c:Landroid/widget/ImageButton;

    .line 2342850
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->c:Landroid/widget/ImageButton;

    const/4 v1, 0x2

    invoke-static {v0, v1}, LX/0vv;->d(Landroid/view/View;I)V

    .line 2342851
    const v0, 0x7f0d10c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2342852
    const v0, 0x7f0d10c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2342853
    sget-object v0, LX/03r;->BetterButton:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2342854
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 2342855
    const/16 v2, 0x1

    sget-object v3, LX/0xr;->BUILTIN:LX/0xr;

    invoke-virtual {v3}, LX/0xr;->ordinal()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 2342856
    invoke-static {v1}, LX/0xq;->fromIndex(I)LX/0xq;

    move-result-object v1

    .line 2342857
    invoke-static {v2}, LX/0xr;->fromIndex(I)LX/0xr;

    move-result-object v2

    .line 2342858
    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->e:Lcom/facebook/widget/text/BetterTextView;

    iget-object v4, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v4}, Lcom/facebook/widget/text/BetterTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v4

    invoke-static {v3, v1, v2, v4}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 2342859
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 2342860
    if-eqz v1, :cond_0

    .line 2342861
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTextTextViewStart(Ljava/lang/CharSequence;)V

    .line 2342862
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->b(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2342863
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2342864
    return-void
.end method

.method public final isChecked()Z
    .locals 1

    .prologue
    .line 2342846
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->a:Z

    return v0
.end method

.method public final onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 2342842
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 2342843
    const-class v0, Landroid/widget/RadioButton;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 2342844
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->a:Z

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setChecked(Z)V

    .line 2342845
    return-void
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 2342812
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 2342813
    const-class v0, Landroid/widget/RadioButton;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 2342814
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCheckable(Z)V

    .line 2342815
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->a:Z

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setChecked(Z)V

    .line 2342816
    return-void
.end method

.method public final performClick()Z
    .locals 1

    .prologue
    .line 2342840
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->toggle()V

    .line 2342841
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->performClick()Z

    move-result v0

    return v0
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 2342830
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->a:Z

    if-eq v0, p1, :cond_0

    .line 2342831
    iput-boolean p1, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->a:Z

    .line 2342832
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2342833
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->refreshDrawableState()V

    .line 2342834
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->b:Z

    if-eqz v0, :cond_1

    .line 2342835
    :goto_0
    return-void

    .line 2342836
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->b:Z

    .line 2342837
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->d:LX/Bc8;

    if-eqz v0, :cond_2

    .line 2342838
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->d:LX/Bc8;

    invoke-virtual {v0, p0}, LX/Bc8;->a(Landroid/view/View;)V

    .line 2342839
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->b:Z

    goto :goto_0
.end method

.method public setContentDescriptionTextViewEnd(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2342828
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2342829
    return-void
.end method

.method public setContentDescriptionTextViewStart(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2342826
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2342827
    return-void
.end method

.method public setOnCheckedChangeWidgetListener(LX/Bc8;)V
    .locals 0

    .prologue
    .line 2342824
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->d:LX/Bc8;

    .line 2342825
    return-void
.end method

.method public setTextTextViewEnd(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2342822
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2342823
    return-void
.end method

.method public setTextTextViewStart(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2342820
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2342821
    return-void
.end method

.method public final toggle()V
    .locals 1

    .prologue
    .line 2342817
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->a:Z

    if-nez v0, :cond_0

    .line 2342818
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setChecked(Z)V

    .line 2342819
    :cond_0
    return-void
.end method
