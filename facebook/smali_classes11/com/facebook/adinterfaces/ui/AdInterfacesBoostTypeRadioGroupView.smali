.class public Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/fbui/widget/contentview/CheckedContentView;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/GJ4;

.field public c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2337833
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2337834
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->c:I

    .line 2337835
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->a()V

    .line 2337836
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2337829
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2337830
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->c:I

    .line 2337831
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->a()V

    .line 2337832
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 2337822
    const v0, 0x7f0301c9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2337823
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->setOrientation(I)V

    .line 2337824
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 2337825
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02007f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2337826
    const v0, 0x7f0d074c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const v1, 0x7f0d074d

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->a:LX/0Px;

    .line 2337827
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->b()V

    .line 2337828
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2337818
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2337819
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    new-instance v2, LX/GJ8;

    invoke-direct {v2, p0, v1}, LX/GJ8;-><init>(Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;I)V

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2337820
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2337821
    :cond_0
    return-void
.end method

.method public final d(I)Lcom/facebook/fbui/widget/contentview/CheckedContentView;
    .locals 1

    .prologue
    .line 2337837
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    return-object v0
.end method

.method public getCheckedIndex()I
    .locals 1

    .prologue
    .line 2337817
    iget v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->c:I

    return v0
.end method

.method public setCheckedIndex(I)V
    .locals 0

    .prologue
    .line 2337815
    invoke-virtual {p0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->setSelected(I)V

    .line 2337816
    return-void
.end method

.method public setOnCheckChangeListener(LX/GJ4;)V
    .locals 0

    .prologue
    .line 2337813
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->b:LX/GJ4;

    .line 2337814
    return-void
.end method

.method public setSelected(I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2337807
    iput p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->c:I

    move v1, v2

    .line 2337808
    :goto_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2337809
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    if-ne p1, v1, :cond_0

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 2337810
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v3, v2

    .line 2337811
    goto :goto_1

    .line 2337812
    :cond_1
    return-void
.end method
