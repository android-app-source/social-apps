.class public Lcom/facebook/adinterfaces/ui/AdInterfacesNavBarButtonView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/63a;


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2340869
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2340870
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesNavBarButtonView;->a()V

    .line 2340871
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2340880
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2340881
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesNavBarButtonView;->a()V

    .line 2340882
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2340877
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2340878
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesNavBarButtonView;->a()V

    .line 2340879
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2340874
    const v0, 0x7f0300b0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2340875
    const v0, 0x7f0d04cc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesNavBarButtonView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2340876
    return-void
.end method


# virtual methods
.method public setButtonTintColor(I)V
    .locals 1

    .prologue
    .line 2340872
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesNavBarButtonView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2340873
    return-void
.end method
