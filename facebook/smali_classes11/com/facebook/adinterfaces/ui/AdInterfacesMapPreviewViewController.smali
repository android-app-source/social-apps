.class public Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;
.super LX/GHg;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:Ljava/util/concurrent/Executor;

.field private final c:LX/1HI;

.field public final d:LX/GF4;

.field private final e:LX/3hh;

.field public final f:LX/2U3;

.field public g:Landroid/view/View$OnClickListener;

.field private h:Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

.field private i:LX/GFR;

.field public j:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

.field public k:Landroid/location/Location;

.field public l:D

.field public m:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field private n:LX/1ca;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2340594
    const-class v0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;LX/1HI;LX/GF4;Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;LX/3hh;LX/2U3;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2340549
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2340550
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iput-wide v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->l:D

    .line 2340551
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->b:Ljava/util/concurrent/Executor;

    .line 2340552
    iput-object p2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->c:LX/1HI;

    .line 2340553
    iput-object p3, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->d:LX/GF4;

    .line 2340554
    iput-object p4, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->h:Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

    .line 2340555
    iput-object p5, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->e:LX/3hh;

    .line 2340556
    iput-object p6, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->f:LX/2U3;

    .line 2340557
    return-void
.end method

.method private a(Lcom/facebook/android/maps/model/LatLng;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2340558
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->m:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2340559
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->g:Ljava/lang/String;

    move-object v0, v1

    .line 2340560
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->e:LX/3hh;

    .line 2340561
    iput-object v1, v0, LX/1bX;->j:LX/33B;

    .line 2340562
    move-object v0, v0

    .line 2340563
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 2340564
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->c:LX/1HI;

    sget-object v2, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->n:LX/1ca;

    .line 2340565
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->n:LX/1ca;

    new-instance v1, LX/GKD;

    invoke-direct {v1, p0, p1}, LX/GKD;-><init>(Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;Lcom/facebook/android/maps/model/LatLng;)V

    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->b:Ljava/util/concurrent/Executor;

    invoke-interface {v0, v1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 2340566
    return-void
.end method

.method public static a$redex0(Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 2340567
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2340568
    const-string v1, "location_extra"

    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->k:Landroid/location/Location;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2340569
    const-string v1, "radius_extra"

    iget-wide v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->l:D

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 2340570
    const-string v1, "target_spec_extra"

    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->m:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2340571
    const-string v1, "ad_account_id_extra"

    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2340572
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->m:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2340573
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v1, v2

    .line 2340574
    if-eqz v1, :cond_0

    .line 2340575
    const-string v2, "page_location_extra"

    new-instance v3, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->ir_()D

    move-result-wide v4

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->j()D

    move-result-wide v6

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2340576
    :cond_0
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->h:Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

    invoke-virtual {v1}, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->a()V

    .line 2340577
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->d:LX/GF4;

    new-instance v2, LX/GFS;

    const/16 v3, 0x4e25

    invoke-direct {v2, v0, v3}, LX/GFS;-><init>(Landroid/content/Intent;I)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2340578
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;
    .locals 7

    .prologue
    .line 2340579
    new-instance v0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v2

    check-cast v2, LX/1HI;

    invoke-static {p0}, LX/GF4;->a(LX/0QB;)LX/GF4;

    move-result-object v3

    check-cast v3, LX/GF4;

    invoke-static {p0}, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->b(LX/0QB;)Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

    invoke-static {p0}, LX/3hh;->b(LX/0QB;)LX/3hh;

    move-result-object v5

    check-cast v5, LX/3hh;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v6

    check-cast v6, LX/2U3;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;-><init>(Ljava/util/concurrent/Executor;LX/1HI;LX/GF4;Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;LX/3hh;LX/2U3;)V

    .line 2340580
    return-object v0
.end method

.method private b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 2340581
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2340582
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v0, v1

    .line 2340583
    if-nez v0, :cond_0

    .line 2340584
    invoke-static {v2, v3, v2, v3}, LX/6aA;->a(DD)Landroid/location/Location;

    move-result-object v0

    iget-wide v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->l:D

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->a(Landroid/location/Location;D)V

    .line 2340585
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->d()V

    .line 2340586
    :goto_0
    return-void

    .line 2340587
    :cond_0
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->m:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2340588
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v1, v2

    .line 2340589
    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->m:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2340590
    iget-object v3, v2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->g:Ljava/lang/String;

    move-object v2, v3

    .line 2340591
    if-eqz v2, :cond_1

    .line 2340592
    new-instance v2, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->ir_()D

    move-result-wide v4

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->j()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-direct {p0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->a(Lcom/facebook/android/maps/model/LatLng;)V

    .line 2340593
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->ir_()D

    move-result-wide v2

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->j()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, LX/6aA;->a(DD)Landroid/location/Location;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->l()D

    move-result-wide v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->a(Landroid/location/Location;D)V

    goto :goto_0
.end method

.method private c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2340546
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->m:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->r()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 2340547
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->h:Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

    new-instance v2, LX/GKC;

    invoke-direct {v2, p0}, LX/GKC;-><init>(Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;)V

    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Landroid/app/Activity;

    invoke-static {v0, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->a(LX/GDA;Landroid/app/Activity;)V

    .line 2340548
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 2340533
    invoke-super {p0}, LX/GHg;->a()V

    .line 2340534
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->d:LX/GF4;

    const/16 v1, 0x4e25

    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->i:LX/GFR;

    .line 2340535
    iget-object v4, v0, LX/GF4;->a:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/GFR;

    .line 2340536
    if-eqz v4, :cond_0

    .line 2340537
    if-ne v4, v2, :cond_2

    .line 2340538
    iget-object v4, v0, LX/GF4;->a:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 2340539
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->n:LX/1ca;

    if-eqz v0, :cond_1

    .line 2340540
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->n:LX/1ca;

    invoke-interface {v0}, LX/1ca;->g()Z

    .line 2340541
    :cond_1
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2340542
    iput-object v3, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    .line 2340543
    iput-object v3, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->i:LX/GFR;

    .line 2340544
    return-void

    .line 2340545
    :cond_2
    iget-object v4, v0, LX/GF4;->b:LX/2U3;

    const-class v5, LX/GF4;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unregister handler mismatch for request code "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/location/Location;D)V
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2340529
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->k:Landroid/location/Location;

    .line 2340530
    iput-wide p2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->l:D

    .line 2340531
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    new-instance v1, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v0, v1, p2, p3}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->a(Lcom/facebook/android/maps/model/LatLng;D)V

    .line 2340532
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2340526
    const-string v0, "location_extra"

    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->k:Landroid/location/Location;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2340527
    const-string v0, "radius_extra"

    iget-wide v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->l:D

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 2340528
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2340509
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    invoke-virtual {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2340523
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2340524
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->m:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2340525
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 3

    .prologue
    .line 2340515
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2340516
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    .line 2340517
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->m:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-direct {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2340518
    new-instance v0, LX/GKA;

    invoke-direct {v0, p0}, LX/GKA;-><init>(Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;)V

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->g:Landroid/view/View$OnClickListener;

    .line 2340519
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2340520
    new-instance v0, LX/GKB;

    invoke-direct {v0, p0}, LX/GKB;-><init>(Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;)V

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->i:LX/GFR;

    .line 2340521
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->d:LX/GF4;

    const/16 v1, 0x4e25

    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->i:LX/GFR;

    invoke-virtual {v0, v1, v2}, LX/GF4;->a(ILX/GFR;)V

    .line 2340522
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2340510
    if-nez p1, :cond_0

    .line 2340511
    :goto_0
    return-void

    .line 2340512
    :cond_0
    const-string v0, "location_extra"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 2340513
    const-string v1, "radius_extra"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 2340514
    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->a(Landroid/location/Location;D)V

    goto :goto_0
.end method
