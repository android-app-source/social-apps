.class public Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field public static final a:LX/Hed;

.field public static final b:LX/Hed;


# instance fields
.field private c:Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;

.field private d:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;

.field public e:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressTabView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2341882
    sget-object v0, LX/Hed;->LEFT:LX/Hed;

    sput-object v0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->a:LX/Hed;

    .line 2341883
    sget-object v0, LX/Hed;->RIGHT:LX/Hed;

    sput-object v0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->b:LX/Hed;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2341907
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2341908
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->a()V

    .line 2341909
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2341904
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2341905
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->a()V

    .line 2341906
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2341901
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2341902
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->a()V

    .line 2341903
    return-void
.end method

.method private a()V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 2341896
    const v0, 0x7f03006b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2341897
    const v0, 0x7f0d043a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->c:Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;

    .line 2341898
    const v0, 0x7f0d043c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;

    .line 2341899
    const v0, 0x7f0d043d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressTabView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressTabView;

    .line 2341900
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Iterable;LX/1jt;)V
    .locals 1

    .prologue
    .line 2341910
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;->a(Ljava/lang/Iterable;LX/1jt;)V

    .line 2341911
    return-void
.end method

.method public getAddressTab()Lcom/facebook/adinterfaces/ui/AdInterfacesAddressTabView;
    .locals 1

    .prologue
    .line 2341895
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressTabView;

    return-object v0
.end method

.method public setRegionSelectorOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2341893
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2341894
    return-void
.end method

.method public setSegmentedTabBarOnClickListener(LX/GKz;)V
    .locals 1

    .prologue
    .line 2341890
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->c:Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;

    .line 2341891
    iput-object p1, v0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->e:LX/GKz;

    .line 2341892
    return-void
.end method

.method public setVisibleTab(LX/Hed;)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2341884
    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;

    sget-object v0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->a:LX/Hed;

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;->setVisibility(I)V

    .line 2341885
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressTabView;

    sget-object v3, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->a:LX/Hed;

    if-ne p1, v3, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressTabView;->setVisibility(I)V

    .line 2341886
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->c:Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;

    invoke-virtual {v0, p1}, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->setSelectedTab(LX/Hed;)V

    .line 2341887
    return-void

    :cond_0
    move v0, v2

    .line 2341888
    goto :goto_0

    :cond_1
    move v2, v1

    .line 2341889
    goto :goto_1
.end method
