.class public Lcom/facebook/adinterfaces/ui/AdInterfacesPacingInfoView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2341214
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2341215
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPacingInfoView;->a()V

    .line 2341216
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2341211
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2341212
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPacingInfoView;->a()V

    .line 2341213
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2341208
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2341209
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPacingInfoView;->a()V

    .line 2341210
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2341200
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPacingInfoView;->setOrientation(I)V

    .line 2341201
    const v0, 0x7f030075

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2341202
    const v0, 0x7f0d0460

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPacingInfoView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2341203
    const v0, 0x7f0d0461

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPacingInfoView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2341204
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2341205
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPacingInfoView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341206
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesPacingInfoView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341207
    return-void
.end method
