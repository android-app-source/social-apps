.class public Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Ljava/util/Locale;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Lcom/facebook/widget/text/BetterTextView;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2340310
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2340311
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;->a()V

    .line 2340312
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2340313
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2340314
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;->a()V

    .line 2340315
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2340298
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2340299
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;->a()V

    .line 2340300
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2340301
    const v0, 0x7f030a40

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2340302
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;->setOrientation(I)V

    .line 2340303
    const-class v0, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;

    invoke-static {v0, p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2340304
    const v0, 0x7f0d19d9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2340305
    const v0, 0x7f0d19d8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2340306
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;->a:Ljava/util/Locale;

    invoke-static {v0}, LX/482;->from(Ljava/util/Locale;)LX/482;

    move-result-object v0

    sget-object v1, LX/482;->IMPERIAL:LX/482;

    if-ne v0, v1, :cond_0

    const v0, 0x7f080af8

    .line 2340307
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;->d:Ljava/lang/String;

    .line 2340308
    return-void

    .line 2340309
    :cond_0
    const v0, 0x7f080af9

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;

    invoke-static {v0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;->a:Ljava/util/Locale;

    return-void
.end method


# virtual methods
.method public setAddress(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2340296
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2340297
    return-void
.end method

.method public setRadius(D)V
    .locals 7

    .prologue
    .line 2340294
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;->b:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;->d:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2340295
    return-void
.end method
