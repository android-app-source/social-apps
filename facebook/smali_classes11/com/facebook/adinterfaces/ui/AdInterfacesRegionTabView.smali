.class public Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/fbui/glyph/GlyphView;

.field private b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2342006
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2342007
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2342008
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2342009
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2342010
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2342011
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2342012
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2342013
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2342014
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 2342015
    const v0, 0x7f03007f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2342016
    const v0, 0x7f0d0470

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2342017
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f080a6f

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2342018
    const v0, 0x7f0d046f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2342019
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;->b(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2342020
    return-void
.end method

.method private b(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2342021
    if-nez p2, :cond_0

    .line 2342022
    :goto_0
    return-void

    .line 2342023
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->AdInterfacesSelector:[I

    invoke-virtual {v0, p2, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2342024
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 2342025
    if-eqz v1, :cond_1

    .line 2342026
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3}, Lcom/facebook/widget/text/BetterTextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    .line 2342027
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 2342028
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Iterable;LX/1jt;)V
    .locals 4

    .prologue
    .line 2342029
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2342030
    const-string v1, ", "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, p2, v2}, LX/0YN;->a(Ljava/lang/StringBuilder;Ljava/lang/String;LX/1jt;[Ljava/lang/Object;)V

    .line 2342031
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;->setSelectedValues(Ljava/lang/CharSequence;)V

    .line 2342032
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2342033
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2342034
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2342035
    return-void
.end method

.method public setSelectedValues(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2342036
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionTabView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2342037
    return-void
.end method
