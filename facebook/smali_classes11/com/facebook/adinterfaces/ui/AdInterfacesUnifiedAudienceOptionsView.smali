.class public Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/RadioGroup;

.field private b:Landroid/view/View;

.field public c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2342908
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2342909
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->a()V

    .line 2342910
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2342966
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2342967
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->a()V

    .line 2342968
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2342963
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2342964
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->a()V

    .line 2342965
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2342957
    const v0, 0x7f030093

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2342958
    const v0, 0x7f0d03da

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->a:Landroid/widget/RadioGroup;

    .line 2342959
    const v0, 0x7f0d03dc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->b:Landroid/view/View;

    .line 2342960
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->c:Z

    .line 2342961
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->a:Landroid/widget/RadioGroup;

    invoke-static {v0}, LX/GMo;->a(Landroid/view/ViewGroup;)V

    .line 2342962
    return-void
.end method

.method private c(Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;Ljava/lang/String;)Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2342947
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 2342948
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2342949
    instance-of v0, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;

    if-eqz v0, :cond_0

    .line 2342950
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    .line 2342951
    if-eqz v0, :cond_0

    .line 2342952
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, v1

    .line 2342953
    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;

    .line 2342954
    :goto_1
    return-object v0

    .line 2342955
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2342956
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/util/List;LX/3Sb;I)Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;
    .locals 6
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "addRadioButtonWithDetailsOption"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/3Sb;",
            "I)",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2342933
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03007e

    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;

    .line 2342934
    invoke-static {}, LX/473;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->setId(I)V

    .line 2342935
    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->setText(Ljava/lang/CharSequence;)V

    .line 2342936
    iput-object p2, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->b:Ljava/util/List;

    .line 2342937
    if-eqz p3, :cond_0

    invoke-interface {p3}, LX/3Sb;->a()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    .line 2342938
    :goto_0
    iput-boolean v1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->g:Z

    .line 2342939
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->a:Landroid/widget/RadioGroup;

    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v3}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v3

    sub-int/2addr v3, p4

    invoke-virtual {v1, v0, v3}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;I)V

    .line 2342940
    if-eqz p3, :cond_2

    invoke-interface {p3}, LX/3Sb;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2342941
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p3}, LX/3Sb;->c()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 2342942
    invoke-interface {p3}, LX/3Sb;->b()LX/2sN;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, LX/2sN;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, LX/2sN;->b()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2342943
    invoke-virtual {v5, v4, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    move v1, v2

    .line 2342944
    goto :goto_0

    .line 2342945
    :cond_1
    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->a(Ljava/util/List;)V

    .line 2342946
    :cond_2
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2342929
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->c(Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;Ljava/lang/String;)Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;

    move-result-object v0

    .line 2342930
    if-eqz v0, :cond_0

    .line 2342931
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->setChecked(Z)V

    .line 2342932
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2342925
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->c(Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;Ljava/lang/String;)Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;

    move-result-object v0

    .line 2342926
    if-eqz v0, :cond_0

    .line 2342927
    invoke-static {v0}, LX/GMo;->b(Landroid/view/View;)V

    .line 2342928
    :cond_0
    return-void
.end method

.method public getMoreOptionsOffset()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2342920
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2342921
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->a:Landroid/widget/RadioGroup;

    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v3}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v3

    sub-int/2addr v3, v0

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->b:Landroid/view/View;

    if-ne v2, v3, :cond_0

    .line 2342922
    :goto_1
    return v0

    .line 2342923
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2342924
    goto :goto_1
.end method

.method public setMoreOptionsView(Z)V
    .locals 0

    .prologue
    .line 2342918
    iput-boolean p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->c:Z

    .line 2342919
    return-void
.end method

.method public setMoreOptionsViewOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2342916
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2342917
    return-void
.end method

.method public setMoreOptionsViewVisibility(I)V
    .locals 2

    .prologue
    .line 2342913
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->b:Landroid/view/View;

    iget-boolean v1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->c:Z

    if-eqz v1, :cond_0

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2342914
    return-void

    .line 2342915
    :cond_0
    const/16 p1, 0x8

    goto :goto_0
.end method

.method public setOnCheckChangedListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 2342911
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0, p1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 2342912
    return-void
.end method
