.class public Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/fbui/glyph/GlyphButton;

.field public c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public d:Landroid/view/View;

.field public e:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2339649
    const-class v0, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2339641
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2339642
    const p1, 0x7f030062

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2339643
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->setOrientation(I)V

    .line 2339644
    const p1, 0x7f0d0425

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2339645
    const p1, 0x7f0d0423

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2339646
    const p1, 0x7f0d0424

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->d:Landroid/view/View;

    .line 2339647
    const p1, 0x7f0d0426

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->e:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2339648
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2339637
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2339638
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2339639
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2339640
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2339633
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2339634
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2339635
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2339636
    return-void
.end method

.method public setDeleteImageOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2339631
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2339632
    return-void
.end method
