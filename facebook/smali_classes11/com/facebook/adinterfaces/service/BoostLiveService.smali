.class public Lcom/facebook/adinterfaces/service/BoostLiveService;
.super LX/0te;
.source ""


# instance fields
.field public a:LX/8wR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/GDg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:LX/GHb;

.field public e:LX/GCE;

.field public f:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2335082
    invoke-direct {p0}, LX/0te;-><init>()V

    .line 2335083
    new-instance v0, LX/GHb;

    invoke-direct {v0, p0}, LX/GHb;-><init>(Lcom/facebook/adinterfaces/service/BoostLiveService;)V

    iput-object v0, p0, Lcom/facebook/adinterfaces/service/BoostLiveService;->d:LX/GHb;

    .line 2335084
    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/service/BoostLiveService;LX/8wR;LX/GDg;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2335085
    iput-object p1, p0, Lcom/facebook/adinterfaces/service/BoostLiveService;->a:LX/8wR;

    iput-object p2, p0, Lcom/facebook/adinterfaces/service/BoostLiveService;->b:LX/GDg;

    iput-object p3, p0, Lcom/facebook/adinterfaces/service/BoostLiveService;->c:Landroid/content/Context;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/adinterfaces/service/BoostLiveService;

    invoke-static {v2}, LX/8wR;->a(LX/0QB;)LX/8wR;

    move-result-object v0

    check-cast v0, LX/8wR;

    invoke-static {v2}, LX/GDg;->a(LX/0QB;)LX/GDg;

    move-result-object v1

    check-cast v1, LX/GDg;

    const-class v3, Landroid/content/Context;

    invoke-interface {v2, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/adinterfaces/service/BoostLiveService;->a(Lcom/facebook/adinterfaces/service/BoostLiveService;LX/8wR;LX/GDg;Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 2335086
    new-instance v0, LX/GHa;

    invoke-direct {v0, p0}, LX/GHa;-><init>(Lcom/facebook/adinterfaces/service/BoostLiveService;)V

    return-object v0
.end method

.method public final onFbCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x2a9f215c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2335087
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 2335088
    invoke-static {p0, p0}, Lcom/facebook/adinterfaces/service/BoostLiveService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2335089
    iget-object v1, p0, Lcom/facebook/adinterfaces/service/BoostLiveService;->a:LX/8wR;

    iget-object v2, p0, Lcom/facebook/adinterfaces/service/BoostLiveService;->d:LX/GHb;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2335090
    const/16 v1, 0x25

    const v2, -0x5725fd3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x5bca4fec

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2335091
    iget-object v1, p0, Lcom/facebook/adinterfaces/service/BoostLiveService;->a:LX/8wR;

    iget-object v2, p0, Lcom/facebook/adinterfaces/service/BoostLiveService;->d:LX/GHb;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2335092
    invoke-super {p0}, LX/0te;->onFbDestroy()V

    .line 2335093
    const/16 v1, 0x25

    const v2, -0x47208bca

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
