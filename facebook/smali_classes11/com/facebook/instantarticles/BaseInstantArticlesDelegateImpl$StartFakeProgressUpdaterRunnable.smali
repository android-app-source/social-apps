.class public final Lcom/facebook/instantarticles/BaseInstantArticlesDelegateImpl$StartFakeProgressUpdaterRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/GmZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/GmZ;)V
    .locals 1

    .prologue
    .line 2392099
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2392100
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/instantarticles/BaseInstantArticlesDelegateImpl$StartFakeProgressUpdaterRunnable;->a:Ljava/lang/ref/WeakReference;

    .line 2392101
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 1

    .prologue
    .line 2392102
    iget-object v0, p0, Lcom/facebook/instantarticles/BaseInstantArticlesDelegateImpl$StartFakeProgressUpdaterRunnable;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GmZ;

    .line 2392103
    if-eqz v0, :cond_0

    .line 2392104
    iget-object p0, v0, LX/GmZ;->ad:LX/Cry;

    .line 2392105
    iget-object v0, p0, LX/Cry;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2392106
    iget-object v0, p0, LX/Cry;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2392107
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Cry;->f:Z

    .line 2392108
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Cry;->g:Z

    .line 2392109
    :cond_0
    return-void
.end method
