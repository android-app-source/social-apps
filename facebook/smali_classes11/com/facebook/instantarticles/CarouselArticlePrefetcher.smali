.class public Lcom/facebook/instantarticles/CarouselArticlePrefetcher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hc;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0Ya;
.implements LX/CqC;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/CH7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field public final d:LX/CqD;

.field public e:I

.field private f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2392536
    const-class v0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/CqD;)V
    .locals 2

    .prologue
    .line 2392530
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2392531
    const/4 v0, 0x2

    iput v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->e:I

    .line 2392532
    iput-object p1, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->c:Landroid/content/Context;

    .line 2392533
    iput-object p2, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->d:LX/CqD;

    .line 2392534
    iget-object v1, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->c:Landroid/content/Context;

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;

    invoke-static {v0}, LX/CH7;->a(LX/0QB;)LX/CH7;

    move-result-object v0

    check-cast v0, LX/CH7;

    iput-object v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->a:LX/CH7;

    .line 2392535
    return-void
.end method

.method public static c(Lcom/facebook/instantarticles/CarouselArticlePrefetcher;I)V
    .locals 5

    .prologue
    .line 2392502
    iget v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->h:I

    if-eq p1, v0, :cond_0

    if-ltz p1, :cond_0

    iget v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->f:I

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_1

    .line 2392503
    :cond_0
    :goto_0
    return-void

    .line 2392504
    :cond_1
    iget-object v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->d:LX/CqD;

    invoke-interface {v0, p1}, LX/CqD;->c(I)Lcom/facebook/richdocument/view/carousel/PageableFragment;

    move-result-object v1

    .line 2392505
    instance-of v0, v1, Lcom/facebook/instantarticles/InstantArticlesFragment;

    if-eqz v0, :cond_2

    move-object v0, v1

    check-cast v0, Lcom/facebook/instantarticles/InstantArticlesFragment;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/InstantArticlesFragment;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2392506
    :cond_2
    if-eqz v1, :cond_0

    .line 2392507
    iget-object v0, v1, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2392508
    if-eqz v0, :cond_0

    .line 2392509
    const-string v1, "extra_instant_articles_id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2392510
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2392511
    iget-object v1, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->a:LX/CH7;

    iget-object v2, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->c:Landroid/content/Context;

    sget-object v3, LX/CH2;->CAROUSEL:LX/CH2;

    sget-object v4, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v0, v3, v4}, LX/CH7;->a(Landroid/content/Context;Ljava/lang/String;LX/CH2;Lcom/facebook/common/callercontext/CallerContext;)LX/CGw;

    goto :goto_0
.end method


# virtual methods
.method public final B_(I)V
    .locals 0

    .prologue
    .line 2392537
    return-void
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2392528
    iget-object v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->d:LX/CqD;

    invoke-interface {v0}, LX/CqD;->getFragmentCount()I

    move-result v0

    iput v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->f:I

    .line 2392529
    return-void
.end method

.method public final a(IFI)V
    .locals 0

    .prologue
    .line 2392527
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 2392512
    packed-switch p1, :pswitch_data_0

    .line 2392513
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 2392514
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->d:LX/CqD;

    invoke-interface {v0}, LX/CqD;->getActiveFragmentIndex()I

    move-result v0

    iput v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->g:I

    goto :goto_0

    .line 2392515
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->d:LX/CqD;

    invoke-interface {v0}, LX/CqD;->getActiveFragmentIndex()I

    move-result v0

    iput v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->h:I

    .line 2392516
    iget v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->h:I

    iget v1, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->g:I

    if-eq v0, v1, :cond_0

    .line 2392517
    iget v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->g:I

    iget v1, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->h:I

    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    .line 2392518
    :goto_1
    if-eqz v0, :cond_2

    .line 2392519
    iget v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->j:I

    .line 2392520
    iget v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->i:I

    .line 2392521
    iget v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->j:I

    invoke-static {p0, v0}, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->c(Lcom/facebook/instantarticles/CarouselArticlePrefetcher;I)V

    .line 2392522
    :goto_2
    goto :goto_0

    .line 2392523
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2392524
    :cond_2
    iget v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->j:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->j:I

    .line 2392525
    iget v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->i:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->i:I

    .line 2392526
    iget v0, p0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->i:I

    invoke-static {p0, v0}, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->c(Lcom/facebook/instantarticles/CarouselArticlePrefetcher;I)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
