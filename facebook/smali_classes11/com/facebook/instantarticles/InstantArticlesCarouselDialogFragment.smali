.class public Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# static fields
.field public static final m:Ljava/lang/String;


# instance fields
.field private final A:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/Gmp;",
            ">;"
        }
    .end annotation
.end field

.field private final B:LX/ChT;

.field public final C:LX/ChV;

.field public final D:LX/CqC;

.field private E:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

.field public F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

.field public G:LX/Gmd;

.field public H:Lcom/facebook/instantarticles/CarouselArticlePrefetcher;

.field public I:LX/B5Z;

.field public J:LX/IXr;

.field public K:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

.field public L:Landroid/widget/TextView;

.field public M:Z

.field public N:Z

.field private O:Z

.field public P:LX/0hc;

.field public Q:I

.field public n:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/Chr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/8bL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/ClD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/IXE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/CqH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/8bM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final z:Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment$ShowNuxRunnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2392803
    const-class v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2392795
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2392796
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->A:Ljava/util/ArrayList;

    .line 2392797
    new-instance v0, LX/Gmf;

    invoke-direct {v0, p0}, LX/Gmf;-><init>(Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;)V

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->B:LX/ChT;

    .line 2392798
    new-instance v0, LX/Gmg;

    invoke-direct {v0, p0}, LX/Gmg;-><init>(Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;)V

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->C:LX/ChV;

    .line 2392799
    new-instance v0, LX/Gmh;

    invoke-direct {v0, p0}, LX/Gmh;-><init>(Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;)V

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->D:LX/CqC;

    .line 2392800
    new-instance v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment$ShowNuxRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment$ShowNuxRunnable;-><init>(Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;)V

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->z:Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment$ShowNuxRunnable;

    .line 2392801
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->O:Z

    .line 2392802
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;
    .locals 2

    .prologue
    .line 2392791
    const-class v0, LX/0ew;

    invoke-static {p0, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ew;

    .line 2392792
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2392793
    :cond_0
    const/4 v0, 0x0

    .line 2392794
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->b(LX/0gc;)Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;

    move-result-object v0

    goto :goto_0
.end method

.method private a(LX/Gmp;)V
    .locals 3

    .prologue
    .line 2392719
    if-nez p1, :cond_1

    .line 2392720
    :cond_0
    :goto_0
    return-void

    .line 2392721
    :cond_1
    iget v0, p1, LX/Gmp;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 2392722
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    iget-object v1, p1, LX/Gmp;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    invoke-virtual {v0, v1}, LX/CsS;->b(Lcom/facebook/richdocument/view/carousel/PageableFragment;)V

    goto :goto_0

    .line 2392723
    :cond_2
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    iget-object v1, p1, LX/Gmp;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    iget v2, p1, LX/Gmp;->b:I

    invoke-virtual {v0, v1, v2}, LX/CsS;->a(Lcom/facebook/richdocument/view/carousel/PageableFragment;I)V

    .line 2392724
    iget-boolean v0, p1, LX/Gmp;->c:Z

    if-eqz v0, :cond_0

    .line 2392725
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v0}, LX/CsS;->getViewPager()Landroid/support/v4/view/ViewPager;

    move-result-object v0

    iget v1, p1, LX/Gmp;->b:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;

    invoke-static {p0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v2

    check-cast v2, LX/Chv;

    invoke-static {p0}, LX/Chr;->a(LX/0QB;)LX/Chr;

    move-result-object v3

    check-cast v3, LX/Chr;

    invoke-static {p0}, LX/8bL;->b(LX/0QB;)LX/8bL;

    move-result-object v4

    check-cast v4, LX/8bL;

    invoke-static {p0}, LX/ClD;->a(LX/0QB;)LX/ClD;

    move-result-object v5

    check-cast v5, LX/ClD;

    invoke-static {p0}, LX/IXE;->a(LX/0QB;)LX/IXE;

    move-result-object v6

    check-cast v6, LX/IXE;

    invoke-static {p0}, LX/CqH;->a(LX/0QB;)LX/CqH;

    move-result-object v7

    check-cast v7, LX/CqH;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v8

    check-cast v8, LX/0Zb;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v9

    check-cast v9, LX/0W3;

    invoke-static {p0}, LX/8bM;->a(LX/0QB;)LX/8bM;

    move-result-object v10

    check-cast v10, LX/8bM;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v12

    check-cast v12, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object p0

    check-cast p0, LX/0So;

    iput-object v2, v1, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->n:LX/Chv;

    iput-object v3, v1, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->o:LX/Chr;

    iput-object v4, v1, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->p:LX/8bL;

    iput-object v5, v1, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->q:LX/ClD;

    iput-object v6, v1, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->r:LX/IXE;

    iput-object v7, v1, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->s:LX/CqH;

    iput-object v8, v1, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->t:LX/0Zb;

    iput-object v9, v1, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->u:LX/0W3;

    iput-object v10, v1, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->v:LX/8bM;

    iput-object v11, v1, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->w:LX/0Uh;

    iput-object v12, v1, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->x:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p0, v1, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->y:LX/0So;

    return-void
.end method

.method public static b(LX/0gc;)Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;
    .locals 1

    .prologue
    .line 2392790
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->m:Ljava/lang/String;

    invoke-virtual {p0, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;

    goto :goto_0
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 2392779
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->J:LX/IXr;

    const v1, 0x7f0d167d

    invoke-virtual {v0, v1}, LX/IXr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2392780
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->J:LX/IXr;

    const v2, 0x7f0d1801

    invoke-virtual {v1, v2}, LX/IXr;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2392781
    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 2392782
    invoke-virtual {v1, v3}, Landroid/view/View;->setAlpha(F)V

    .line 2392783
    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 2392784
    sget v3, LX/CoL;->M:I

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2392785
    new-instance v3, LX/Gmj;

    invoke-direct {v3, p0, v0, v1}, LX/Gmj;-><init>(Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2392786
    new-instance v1, LX/Gmk;

    invoke-direct {v1, p0, v2}, LX/Gmk;-><init>(Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;Landroid/animation/ValueAnimator;)V

    invoke-virtual {v2, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2392787
    new-instance v1, LX/Gml;

    invoke-direct {v1, p0, v0, v2}, LX/Gml;-><init>(Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;Landroid/view/View;Landroid/animation/ValueAnimator;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 2392788
    return-void

    .line 2392789
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static k(Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;)V
    .locals 1

    .prologue
    .line 2392777
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->E:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->e()V

    .line 2392778
    return-void
.end method

.method public static m(Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 2392804
    iget-boolean v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->N:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->I:LX/B5Z;

    invoke-static {v1}, LX/IXi;->a(LX/B5Z;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2392805
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2392806
    if-eqz v1, :cond_5

    .line 2392807
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2392808
    const-string v3, "extra_instant_articles_featured_articles_launch_ok"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2392809
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->I:LX/B5Z;

    invoke-interface {v1}, LX/B5Z;->b()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel$FeaturedArticlesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel$FeaturedArticlesModel;->a()LX/0Px;

    move-result-object v5

    .line 2392810
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v0

    move v3, v0

    :goto_0
    if-ge v4, v6, :cond_2

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel$FeaturedArticlesModel$EdgesModel;

    .line 2392811
    invoke-virtual {v0}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel$FeaturedArticlesModel$EdgesModel;->a()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleExternalUrlModel;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel$FeaturedArticlesModel$EdgesModel;->a()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleExternalUrlModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleExternalUrlModel;->b()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleExternalUrlModel$InstantArticleModel;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel$FeaturedArticlesModel$EdgesModel;->a()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleExternalUrlModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleExternalUrlModel;->b()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleExternalUrlModel$InstantArticleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleExternalUrlModel$InstantArticleModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 2392812
    invoke-virtual {v0}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel$FeaturedArticlesModel$EdgesModel;->a()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleExternalUrlModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleExternalUrlModel;->b()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleExternalUrlModel$InstantArticleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleExternalUrlModel$InstantArticleModel;->c()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleExternalUrlModel$InstantArticleModel$LatestVersionModel;

    move-result-object v1

    .line 2392813
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleExternalUrlModel$InstantArticleModel$LatestVersionModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2392814
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel$FeaturedArticlesModel$EdgesModel;->a()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleExternalUrlModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleExternalUrlModel;->b()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleExternalUrlModel$InstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleExternalUrlModel$InstantArticleModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2392815
    new-instance v7, Lcom/facebook/instantarticles/InstantArticlesFragment;

    invoke-direct {v7}, Lcom/facebook/instantarticles/InstantArticlesFragment;-><init>()V

    .line 2392816
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 2392817
    const-string v9, "extra_instant_articles_id"

    invoke-virtual {v8, v9, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2392818
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 2392819
    const-string v9, "extra_instant_articles_canonical_url"

    invoke-virtual {v8, v9, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2392820
    const-string v9, "extra_instant_articles_click_url"

    invoke-virtual {v8, v9, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2392821
    :cond_0
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2392822
    const-string v10, "richdocument_fragment_tag"

    invoke-virtual {v8, v10, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2392823
    const-string v9, "extra_instant_articles_referrer"

    const-string v10, "ia_auto_related_article_launch"

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2392824
    invoke-virtual {v7, v8}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2392825
    invoke-virtual {p0, v7}, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->a(Lcom/facebook/richdocument/view/carousel/PageableFragment;)V

    .line 2392826
    add-int/lit8 v0, v3, 0x1

    .line 2392827
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v3, v0

    goto/16 :goto_0

    :cond_1
    move-object v1, v2

    .line 2392828
    goto :goto_1

    .line 2392829
    :cond_2
    iput-object v2, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->I:LX/B5Z;

    .line 2392830
    if-lez v3, :cond_5

    .line 2392831
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->H:Lcom/facebook/instantarticles/CarouselArticlePrefetcher;

    .line 2392832
    iget-object v1, v0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->d:LX/CqD;

    invoke-interface {v1}, LX/CqD;->getActiveFragmentIndex()I

    move-result v2

    .line 2392833
    iget v1, v0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->e:I

    sub-int v1, v2, v1

    iput v1, v0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->i:I

    .line 2392834
    iget v1, v0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->e:I

    add-int/2addr v1, v2

    iput v1, v0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->j:I

    .line 2392835
    iget v1, v0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->i:I

    :goto_3
    iget v3, v0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->j:I

    if-gt v1, v3, :cond_4

    .line 2392836
    if-eq v1, v2, :cond_3

    .line 2392837
    invoke-static {v0, v1}, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;->c(Lcom/facebook/instantarticles/CarouselArticlePrefetcher;I)V

    .line 2392838
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2392839
    :cond_4
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->p:LX/8bL;

    invoke-virtual {v0}, LX/8bL;->d()Ljava/lang/String;

    move-result-object v0

    .line 2392840
    const-string v1, "top"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2392841
    iget-object v7, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v7, v7

    .line 2392842
    if-eqz v7, :cond_5

    .line 2392843
    iget-object v7, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v7, v7

    .line 2392844
    iget-object v8, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->z:Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment$ShowNuxRunnable;

    const-wide/16 v9, 0x7d0

    invoke-virtual {v7, v8, v9, v10}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2392845
    :cond_5
    return-void

    :cond_6
    move v0, v3

    goto :goto_2
.end method


# virtual methods
.method public final S_()Z
    .locals 2

    .prologue
    .line 2392769
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    if-nez v0, :cond_0

    .line 2392770
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->S_()Z

    move-result v0

    .line 2392771
    :goto_0
    return v0

    .line 2392772
    :cond_0
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v0}, LX/CsS;->getActiveFragmentIndex()I

    move-result v0

    .line 2392773
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v1, v0}, LX/CsS;->c(I)Lcom/facebook/richdocument/view/carousel/PageableFragment;

    move-result-object v0

    .line 2392774
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->S_()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2392775
    :cond_1
    invoke-static {p0}, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->k(Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;)V

    .line 2392776
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 2392768
    new-instance v0, LX/Gmq;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x1030011

    invoke-direct {v0, p0, v1, v2}, LX/Gmq;-><init>(Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;Landroid/content/Context;I)V

    return-object v0
.end method

.method public final a()V
    .locals 12

    .prologue
    const-wide/16 v6, 0x0

    const/4 v9, 0x1

    const/4 v5, 0x0

    const/4 v8, 0x2

    .line 2392738
    iget-boolean v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->O:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->K:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v0}, LX/CsS;->getFragmentCount()I

    move-result v0

    if-ge v0, v8, :cond_1

    .line 2392739
    :cond_0
    :goto_0
    return-void

    .line 2392740
    :cond_1
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->x:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2fv;->e:LX/0Tn;

    invoke-interface {v0, v1, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    .line 2392741
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->x:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2fv;->g:LX/0Tn;

    invoke-interface {v0, v2, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 2392742
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->x:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/2fv;->f:LX/0Tn;

    invoke-interface {v0, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v4

    .line 2392743
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->p:LX/8bL;

    .line 2392744
    iget-object v5, v0, LX/8bL;->b:LX/0ad;

    sget v10, LX/2yD;->s:I

    const/4 v11, 0x5

    invoke-interface {v5, v10, v11}, LX/0ad;->a(II)I

    move-result v5

    move v0, v5

    .line 2392745
    if-ge v1, v0, :cond_0

    if-ge v4, v8, :cond_0

    cmp-long v0, v2, v6

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->y:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v6

    sub-long v2, v6, v2

    const-wide/32 v6, 0x5265c00

    cmp-long v0, v2, v6

    if-ltz v0, :cond_0

    .line 2392746
    :cond_2
    iput-boolean v9, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->O:Z

    .line 2392747
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->y:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 2392748
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2392749
    new-instance v5, LX/Gmo;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0308b1

    invoke-direct {v5, v6, v8, v7}, LX/Gmo;-><init>(Landroid/content/Context;II)V

    .line 2392750
    iget-object v6, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->p:LX/8bL;

    .line 2392751
    iget-object v7, v6, LX/8bL;->b:LX/0ad;

    sget v8, LX/2yD;->u:I

    const/4 v10, 0x1

    invoke-interface {v7, v8, v10}, LX/0ad;->a(II)I

    move-result v7

    move v6, v7

    .line 2392752
    if-ne v6, v9, :cond_3

    const v6, 0x7f0837e5

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2392753
    :goto_1
    invoke-virtual {v5, v0}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2392754
    sget-object v0, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v5, v0}, LX/0ht;->a(LX/3AV;)V

    .line 2392755
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v6, 0x7f0b23c2

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2392756
    iput v0, v5, LX/0hs;->r:I

    .line 2392757
    const/4 v0, -0x1

    .line 2392758
    iput v0, v5, LX/0hs;->t:I

    .line 2392759
    new-instance v0, LX/Gmm;

    invoke-direct {v0, p0, v2, v3, v1}, LX/Gmm;-><init>(Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;JI)V

    .line 2392760
    iput-object v0, v5, LX/0ht;->H:LX/2dD;

    .line 2392761
    new-instance v0, LX/Gmn;

    invoke-direct {v0, p0, v4}, LX/Gmn;-><init>(Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;I)V

    invoke-virtual {v5, v0}, LX/0hs;->a(LX/5Od;)V

    .line 2392762
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->K:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    invoke-virtual {v5, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 2392763
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->K:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    new-instance v1, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment$10;

    invoke-direct {v1, p0}, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment$10;-><init>(Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2392764
    new-instance v0, LX/Gme;

    invoke-direct {v0, p0, v4}, LX/Gme;-><init>(Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;I)V

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->P:LX/0hc;

    .line 2392765
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    if-eqz v0, :cond_0

    .line 2392766
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->P:LX/0hc;

    invoke-virtual {v0, v1}, LX/CsS;->a(LX/0hc;)V

    goto/16 :goto_0

    .line 2392767
    :cond_3
    const v6, 0x7f0837e6

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/richdocument/view/carousel/PageableFragment;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2392726
    if-nez p1, :cond_0

    .line 2392727
    :goto_0
    return-void

    .line 2392728
    :cond_0
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2392729
    const-string v2, "open_action"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2392730
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "clicked"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2392731
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    if-nez v1, :cond_1

    .line 2392732
    :goto_1
    new-instance v1, LX/Gmp;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v0, v2}, LX/Gmp;-><init>(Lcom/facebook/richdocument/view/carousel/PageableFragment;IZ)V

    move-object v0, v1

    .line 2392733
    :goto_2
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    if-nez v1, :cond_3

    .line 2392734
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->A:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2392735
    :cond_1
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v0}, LX/CsS;->getActiveFragmentIndex()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2392736
    :cond_2
    new-instance v1, LX/Gmp;

    const/4 v2, -0x1

    invoke-direct {v1, p1, v2, v0}, LX/Gmp;-><init>(Lcom/facebook/richdocument/view/carousel/PageableFragment;IZ)V

    move-object v0, v1

    goto :goto_2

    .line 2392737
    :cond_3
    invoke-direct {p0, v0}, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->a(LX/Gmp;)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7437c213

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2392716
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2392717
    const-class v1, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;

    invoke-static {v1, p0}, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2392718
    const/16 v1, 0x2b

    const v2, -0x25ae0f1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v3, -0x17671dbc

    invoke-static {v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2392666
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v0

    .line 2392667
    if-eqz v0, :cond_0

    .line 2392668
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->v:LX/8bM;

    invoke-virtual {v1, v0}, LX/8bM;->a(Landroid/app/Activity;)V

    .line 2392669
    invoke-virtual {v0}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v1

    iput v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->Q:I

    .line 2392670
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 2392671
    :cond_0
    const v0, 0x7f030956

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 2392672
    const v0, 0x7f0d1800

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/IXr;

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->J:LX/IXr;

    .line 2392673
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->o:LX/Chr;

    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->J:LX/IXr;

    check-cast v0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    .line 2392674
    if-eqz v0, :cond_1

    .line 2392675
    iget-object v5, v1, LX/Chr;->a:Ljava/util/Set;

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2392676
    :cond_1
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->J:LX/IXr;

    const v1, 0x7f0d1802

    invoke-virtual {v0, v1}, LX/IXr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->L:Landroid/widget/TextView;

    .line 2392677
    const v0, 0x7f0d1803

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->K:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    .line 2392678
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->K:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v5, 0x40200000    # 2.5f

    invoke-static {v1, v5}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->setDotRadius(F)V

    .line 2392679
    invoke-direct {p0}, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->b()V

    .line 2392680
    const v0, 0x7f0d17fe

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->E:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    .line 2392681
    const v0, 0x7f0d167d

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantarticles/view/ShareBar;

    .line 2392682
    new-instance v1, LX/Gmi;

    invoke-direct {v1, p0}, LX/Gmi;-><init>(Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;)V

    .line 2392683
    iput-object v1, v0, Lcom/facebook/instantarticles/view/ShareBar;->A:LX/IYL;

    .line 2392684
    const v0, 0x7f0d17ff

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    .line 2392685
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    new-instance v1, LX/IXo;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v5

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v1, v5, v6}, LX/IXo;-><init>(LX/0gc;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/CsS;->setPagerAdapter(LX/CsQ;)V

    .line 2392686
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->E:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    .line 2392687
    iput-object v1, v0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->i:Landroid/view/View;

    .line 2392688
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->D:LX/CqC;

    invoke-virtual {v0, v1}, LX/CsS;->a(LX/CqC;)V

    .line 2392689
    new-instance v0, LX/Gmd;

    invoke-direct {v0}, LX/Gmd;-><init>()V

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->G:LX/Gmd;

    .line 2392690
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->G:LX/Gmd;

    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v0, v1}, LX/Gmd;->setFragmentPager(LX/CqD;)V

    .line 2392691
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->G:LX/Gmd;

    invoke-virtual {v0, v1}, LX/CsS;->a(LX/0hc;)V

    .line 2392692
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v0}, LX/CsS;->getViewPager()Landroid/support/v4/view/ViewPager;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;

    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->G:LX/Gmd;

    invoke-virtual {v0, v1}, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->a(LX/IXp;)V

    .line 2392693
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->G:LX/Gmd;

    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->n:LX/Chv;

    invoke-virtual {v0, v1}, LX/Gmd;->a(LX/0b4;)V

    .line 2392694
    new-instance v0, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v5, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    invoke-direct {v0, v1, v5}, Lcom/facebook/instantarticles/CarouselArticlePrefetcher;-><init>(Landroid/content/Context;LX/CqD;)V

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->H:Lcom/facebook/instantarticles/CarouselArticlePrefetcher;

    .line 2392695
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->H:Lcom/facebook/instantarticles/CarouselArticlePrefetcher;

    invoke-virtual {v0, v1}, LX/CsS;->a(LX/0hc;)V

    .line 2392696
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->H:Lcom/facebook/instantarticles/CarouselArticlePrefetcher;

    invoke-virtual {v0, v1}, LX/CsS;->a(LX/CqC;)V

    .line 2392697
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->A:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2392698
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->A:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_2

    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->A:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gmp;

    .line 2392699
    invoke-direct {p0, v0}, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->a(LX/Gmp;)V

    .line 2392700
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2392701
    :cond_2
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->A:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2392702
    :cond_3
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->n:LX/Chv;

    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->B:LX/ChT;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2392703
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->n:LX/Chv;

    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->C:LX/ChV;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2392704
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->r:LX/IXE;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2392705
    const/16 v5, 0xf

    .line 2392706
    invoke-virtual {v0}, LX/IXE;->c()V

    .line 2392707
    iput-object v1, v0, LX/IXE;->b:Landroid/content/Context;

    .line 2392708
    iput v5, v0, LX/IXE;->c:I

    .line 2392709
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->w:LX/0Uh;

    const/16 v1, 0xad

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2392710
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->s:LX/CqH;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2392711
    const/16 v2, 0xf

    .line 2392712
    invoke-virtual {v0}, LX/CqH;->c()V

    .line 2392713
    iput-object v1, v0, LX/CqH;->b:Landroid/content/Context;

    .line 2392714
    iput v2, v0, LX/CqH;->c:I

    .line 2392715
    :cond_4
    const v0, 0x106bd0f2

    invoke-static {v0, v3}, LX/02F;->f(II)V

    return-object v4
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x638ca5ec    # 5.189E21f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2392662
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2392663
    iget-object v2, p0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->z:Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment$ShowNuxRunnable;

    invoke-virtual {v1, v2}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2392664
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroyView()V

    .line 2392665
    const/16 v1, 0x2b

    const v2, -0x68a8a724

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
