.class public Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;
.super LX/GmZ;
.source ""

# interfaces
.implements LX/ChL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GmZ",
        "<",
        "LX/0zO",
        "<",
        "LX/B5Z;",
        ">;",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "LX/B5Z;",
        ">;>;",
        "Lcom/facebook/instantarticles/InstantArticlesDelegate;"
    }
.end annotation


# static fields
.field private static final am:Ljava/lang/String;


# instance fields
.field public X:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalUFI;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CH7;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Z:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CjG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public aa:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Crz;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ab:LX/CKh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ac:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ckw;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ad:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Chv;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ae:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cju;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public af:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Chi;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ag:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8bL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ah:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CsO;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ai:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Crk;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public aj:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8bZ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ak:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Sc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public al:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public an:LX/CKg;

.field public ao:LX/B5Z;

.field public ap:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

.field public aq:LX/CnR;

.field public ar:LX/IXt;

.field private as:Z

.field public final at:LX/Ci8;

.field private final au:LX/Ci5;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2392898
    const-class v0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->am:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2393082
    invoke-direct {p0}, LX/GmZ;-><init>()V

    .line 2393083
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->as:Z

    .line 2393084
    new-instance v0, LX/Gmr;

    invoke-direct {v0, p0}, LX/Gmr;-><init>(Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;)V

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->at:LX/Ci8;

    .line 2393085
    new-instance v0, LX/Gms;

    invoke-direct {v0, p0}, LX/Gms;-><init>(Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;)V

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->au:LX/Ci5;

    .line 2393086
    return-void
.end method

.method private V()V
    .locals 9

    .prologue
    .line 2393087
    iget-boolean v0, p0, LX/Chc;->A:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->as:Z

    if-nez v0, :cond_0

    .line 2393088
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2393089
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v1

    .line 2393090
    if-nez v0, :cond_1

    .line 2393091
    :cond_0
    :goto_0
    return-void

    .line 2393092
    :cond_1
    iget-object v0, p0, LX/GmZ;->O:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x3ef

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2393093
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->as:Z

    .line 2393094
    const v0, 0x7f0311fc

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const v1, 0x7f0d017b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    .line 2393095
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2393096
    iget-object v2, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v2

    .line 2393097
    check-cast v0, LX/CoJ;

    .line 2393098
    new-instance v2, LX/Crr;

    invoke-direct {v2, v0}, LX/Crr;-><init>(LX/CoJ;)V

    .line 2393099
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 2393100
    iget-object v0, v2, LX/Crr;->a:LX/CoJ;

    if-eqz v0, :cond_7

    .line 2393101
    const/4 v0, 0x0

    move v4, v0

    :goto_1
    iget-object v0, v2, LX/Crr;->a:LX/CoJ;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    if-ge v4, v0, :cond_7

    .line 2393102
    const/4 v8, -0x1

    .line 2393103
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2393104
    iget-object v3, v2, LX/Crr;->c:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v3, v7, v8}, Landroid/util/SparseIntArray;->get(II)I

    move-result v3

    if-eq v3, v8, :cond_3

    invoke-interface {v5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v7, v2, LX/Crr;->c:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v7, v0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 2393105
    :cond_3
    const/4 v0, 0x1

    .line 2393106
    :goto_2
    move v0, v0

    .line 2393107
    if-eqz v0, :cond_7

    .line 2393108
    iget-object v0, v2, LX/Crr;->a:LX/CoJ;

    invoke-virtual {v0, v4}, LX/CoJ;->e(I)LX/Clr;

    move-result-object v6

    .line 2393109
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_4
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2393110
    iget-object v3, v2, LX/Crr;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v3, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 2393111
    iget-object v3, v2, LX/Crr;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v3, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Crn;

    invoke-interface {v3, v6}, LX/Crn;->a(LX/Clr;)I

    move-result v8

    .line 2393112
    invoke-interface {v5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2393113
    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v3, v8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v5, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 2393114
    :cond_5
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v5, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 2393115
    :cond_6
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_1

    .line 2393116
    :cond_7
    move-object v1, v5

    .line 2393117
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Crk;

    const/4 v5, 0x0

    .line 2393118
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v5

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 2393119
    iget-object v3, v0, LX/Crk;->d:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Crh;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2393120
    iget-object v7, v3, LX/Crh;->a:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    add-int/2addr v7, v2

    iget v8, v3, LX/Crh;->c:I

    if-gt v7, v8, :cond_8

    .line 2393121
    :goto_5
    const/4 v7, 0x0

    :goto_6
    if-ge v7, v2, :cond_9

    .line 2393122
    iget-object v8, v3, LX/Crh;->a:Ljava/util/List;

    invoke-virtual {v3}, LX/Crh;->b()LX/CnP;

    move-result-object p0

    invoke-interface {v8, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2393123
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 2393124
    :cond_8
    iget v7, v3, LX/Crh;->c:I

    iget-object v8, v3, LX/Crh;->a:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    sub-int v2, v7, v8

    goto :goto_5

    .line 2393125
    :cond_9
    move v2, v2

    .line 2393126
    if-nez v4, :cond_a

    if-lez v2, :cond_b

    :cond_a
    const/4 v2, 0x1

    :goto_7
    move v4, v2

    .line 2393127
    goto :goto_4

    :cond_b
    move v2, v5

    .line 2393128
    goto :goto_7

    .line 2393129
    :cond_c
    goto/16 :goto_0

    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_2
.end method

.method private a(LX/B5Z;)V
    .locals 13

    .prologue
    const/4 v3, 0x0

    .line 2393130
    const/4 v1, 0x0

    .line 2393131
    iget-object v0, p0, LX/Chc;->D:Landroid/view/View;

    move-object v0, v0

    .line 2393132
    const v2, 0x7f0d167f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2393133
    if-eqz v0, :cond_1

    .line 2393134
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;

    .line 2393135
    :goto_0
    if-nez v0, :cond_0

    .line 2393136
    :goto_1
    return-void

    .line 2393137
    :cond_0
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ab:LX/CKh;

    .line 2393138
    new-instance v4, LX/CKg;

    invoke-static {v1}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v5

    check-cast v5, LX/0wW;

    invoke-static {v1}, LX/3Rb;->a(LX/0QB;)LX/3Rb;

    move-result-object v6

    check-cast v6, LX/3Rb;

    invoke-static {v1}, LX/3MV;->a(LX/0QB;)LX/3MV;

    move-result-object v7

    check-cast v7, LX/3MV;

    invoke-static {v1}, LX/CK7;->b(LX/0QB;)LX/CK7;

    move-result-object v8

    check-cast v8, LX/CK7;

    .line 2393139
    new-instance v12, LX/CKk;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v10

    check-cast v10, LX/0tX;

    invoke-static {v1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v11

    check-cast v11, LX/1Ck;

    invoke-direct {v12, v9, v10, v11}, LX/CKk;-><init>(LX/03V;LX/0tX;LX/1Ck;)V

    .line 2393140
    move-object v9, v12

    .line 2393141
    check-cast v9, LX/CKk;

    invoke-static {v1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    move-object v11, v0

    invoke-direct/range {v4 .. v11}, LX/CKg;-><init>(LX/0wW;LX/3Rb;LX/3MV;LX/CK7;LX/CKk;LX/0ad;Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;)V

    .line 2393142
    move-object v1, v4

    .line 2393143
    iput-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->an:LX/CKg;

    .line 2393144
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->an:LX/CKg;

    new-instance v2, LX/Gmv;

    invoke-direct {v2, p0}, LX/Gmv;-><init>(Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;)V

    .line 2393145
    iput-object v2, v1, LX/0Dp;->e:LX/0Cl;

    .line 2393146
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->an:LX/CKg;

    .line 2393147
    if-eqz p1, :cond_3

    invoke-interface {p1}, LX/B5Z;->e()LX/B5Y;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, LX/B5Z;->e()LX/B5Y;

    move-result-object v2

    invoke-interface {v2}, LX/B5Y;->n()LX/8Yp;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, LX/B5Z;->iT_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBMessengerSubscriptionInfoModel$MessengerContentSubscriptionOptionModel;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, LX/B5Z;->iT_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBMessengerSubscriptionInfoModel$MessengerContentSubscriptionOptionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBMessengerSubscriptionInfoModel$MessengerContentSubscriptionOptionModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-interface {p1}, LX/B5Z;->iT_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBMessengerSubscriptionInfoModel$MessengerContentSubscriptionOptionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBMessengerSubscriptionInfoModel$MessengerContentSubscriptionOptionModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_2
    move v2, v2

    .line 2393148
    if-nez v2, :cond_2

    .line 2393149
    :goto_3
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto/16 :goto_1

    :cond_1
    move-object v0, v1

    goto/16 :goto_0

    .line 2393150
    :cond_2
    iget-object v2, v1, LX/CKg;->l:LX/CKk;

    invoke-interface {p1}, LX/B5Z;->e()LX/B5Y;

    move-result-object v4

    invoke-interface {v4}, LX/B5Y;->n()LX/8Yp;

    move-result-object v4

    invoke-interface {v4}, LX/8Yp;->d()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/CKd;

    invoke-direct {v5, v1, p1}, LX/CKd;-><init>(LX/CKg;LX/B5Z;)V

    .line 2393151
    new-instance v6, LX/CKY;

    invoke-direct {v6}, LX/CKY;-><init>()V

    move-object v6, v6

    .line 2393152
    const-string v7, "pageId"

    invoke-virtual {v6, v7, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2393153
    iget-object v7, v2, LX/CKk;->c:LX/1Ck;

    sget-object v8, LX/CKj;->IS_CONTENT_SUBSCRIBED:LX/CKj;

    iget-object v1, v2, LX/CKk;->b:LX/0tX;

    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    sget-object p1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v6, p1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-static {p1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p1

    .line 2393154
    iput-object p1, v6, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2393155
    move-object v6, v6

    .line 2393156
    invoke-virtual {v1, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    .line 2393157
    new-instance v1, LX/CKi;

    invoke-direct {v1, v2, v5}, LX/CKi;-><init>(LX/CKk;LX/CKd;)V

    move-object v1, v1

    .line 2393158
    invoke-virtual {v7, v8, v6, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2393159
    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    goto :goto_2
.end method

.method private static a(Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/CKh;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0tX;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalUFI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CH7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CjG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Crz;",
            ">;",
            "LX/CKh;",
            "LX/0Ot",
            "<",
            "LX/Ckw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Chv;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Cju;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Chi;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8bL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CsO;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Crk;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8bZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Sc;",
            ">;",
            "LX/0tX;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2393079
    iput-object p1, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->X:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->Y:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->Z:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->aa:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ab:LX/CKh;

    iput-object p6, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ac:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ad:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ae:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->af:LX/0Ot;

    iput-object p10, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ag:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ah:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ai:LX/0Ot;

    iput-object p13, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->aj:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ak:LX/0Ot;

    iput-object p15, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->al:LX/0tX;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 16

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v15

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    const/16 v1, 0x3227

    invoke-static {v15, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x2524

    invoke-static {v15, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x31e7

    invoke-static {v15, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x3244

    invoke-static {v15, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const-class v5, LX/CKh;

    invoke-interface {v15, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/CKh;

    const/16 v6, 0x3216

    invoke-static {v15, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x31e0

    invoke-static {v15, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x320e

    invoke-static {v15, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x31dc

    invoke-static {v15, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x3231

    invoke-static {v15, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x3246

    invoke-static {v15, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x3243

    invoke-static {v15, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x3236

    invoke-static {v15, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x1132

    invoke-static {v15, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static {v15}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v15

    check-cast v15, LX/0tX;

    invoke-static/range {v0 .. v15}, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->a(Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/CKh;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0tX;)V

    return-void
.end method

.method public static d(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2393160
    const-string v0, "ia_gql_query_result"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, LX/B5Z;

    .line 2393161
    if-eqz v0, :cond_0

    .line 2393162
    invoke-interface {v0}, LX/B5Z;->d()Ljava/lang/String;

    move-result-object v0

    .line 2393163
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "extra_instant_articles_id"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final D()LX/CH4;
    .locals 1

    .prologue
    .line 2393164
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->Y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CH4;

    return-object v0
.end method

.method public final E()LX/CGs;
    .locals 5

    .prologue
    .line 2393165
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2393166
    invoke-static {v0}, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 2393167
    new-instance v1, LX/CGu;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/CGu;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 2393168
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2393169
    const-string v2, "fetch_from_server"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2393170
    const-wide/16 v2, 0x1

    .line 2393171
    iput-wide v2, v1, LX/CGt;->j:J

    .line 2393172
    :goto_0
    return-object v1

    .line 2393173
    :cond_0
    const-wide/32 v2, 0x15180

    .line 2393174
    iput-wide v2, v1, LX/CGt;->j:J

    .line 2393175
    goto :goto_0
.end method

.method public final H()LX/0P1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2393176
    iget-object v0, p0, LX/GmZ;->R:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXX;

    .line 2393177
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2393178
    const-string p0, "data_freshness"

    iget-object v1, v0, LX/IXX;->s:LX/0ta;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/IXX;->s:LX/0ta;

    invoke-virtual {v1}, LX/0ta;->name()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-interface {v2, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2393179
    const-string p0, "connection_quality_at_start"

    iget-object v1, v0, LX/IXX;->w:LX/0p3;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/IXX;->w:LX/0p3;

    invoke-virtual {v1}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-interface {v2, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2393180
    const-string v1, "fetched_from_memory_cache"

    iget-boolean p0, v0, LX/IXX;->x:Z

    invoke-static {p0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v2, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2393181
    const-string v1, "first_open_since_start"

    iget-boolean p0, v0, LX/IXX;->y:Z

    invoke-static {p0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v2, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2393182
    invoke-static {v2}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v1

    .line 2393183
    move-object v0, v1

    .line 2393184
    return-object v0

    .line 2393185
    :cond_0
    sget-object v1, LX/0ta;->NO_DATA:LX/0ta;

    invoke-virtual {v1}, LX/0ta;->name()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 2393186
    :cond_1
    sget-object v1, LX/0p3;->UNKNOWN:LX/0p3;

    invoke-virtual {v1}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public final O()LX/0Pq;
    .locals 1

    .prologue
    .line 2393187
    sget-object v0, LX/IXZ;->a:LX/IXY;

    return-object v0
.end method

.method public final Q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2393188
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2393189
    invoke-static {v0}, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final R()V
    .locals 8

    .prologue
    .line 2393190
    invoke-super {p0}, LX/GmZ;->R()V

    .line 2393191
    iget-object v0, p0, LX/GmZ;->Q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckw;

    const/4 v1, 0x0

    .line 2393192
    iget-object v2, v0, LX/Ckw;->a:LX/0Zb;

    const-string v3, "native_article_open"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2393193
    iget-object v2, v0, LX/Ckw;->e:LX/ClO;

    .line 2393194
    iget-boolean v4, v2, LX/ClO;->b:Z

    move v2, v4

    .line 2393195
    if-eqz v2, :cond_1

    .line 2393196
    new-instance v4, LX/ClL;

    const-string v2, "native_article_open"

    invoke-direct {v4, v2}, LX/ClL;-><init>(Ljava/lang/String;)V

    .line 2393197
    const-string v2, "open_action"

    iget-object v5, v0, LX/Ckw;->j:Ljava/lang/String;

    invoke-virtual {v4, v2, v5}, LX/ClL;->a(Ljava/lang/String;Ljava/lang/Object;)LX/ClL;

    .line 2393198
    const-string v2, "click_source"

    iget-object v5, v0, LX/Ckw;->i:Ljava/lang/String;

    invoke-virtual {v4, v2, v5}, LX/ClL;->a(Ljava/lang/String;Ljava/lang/Object;)LX/ClL;

    .line 2393199
    iget-object v2, v0, LX/Ckw;->c:Ljava/lang/ref/WeakReference;

    if-eqz v2, :cond_0

    .line 2393200
    iget-object v2, v0, LX/Ckw;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    .line 2393201
    const-string v5, "session_id"

    iget-object v6, v0, LX/Ckw;->b:LX/ClD;

    .line 2393202
    iget-object v7, v6, LX/ClD;->h:Ljava/lang/String;

    move-object v6, v7

    .line 2393203
    invoke-virtual {v4, v5, v6}, LX/ClL;->a(Ljava/lang/String;Ljava/lang/Object;)LX/ClL;

    .line 2393204
    const-string v5, "chain_id"

    iget-object v6, v0, LX/Ckw;->b:LX/ClD;

    invoke-virtual {v6, v2}, LX/ClD;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/ClL;->a(Ljava/lang/String;Ljava/lang/Object;)LX/ClL;

    .line 2393205
    const-string v5, "depth"

    iget-object v6, v0, LX/Ckw;->b:LX/ClD;

    invoke-virtual {v6, v2}, LX/ClD;->a(Landroid/content/Context;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, LX/ClL;->a(Ljava/lang/String;Ljava/lang/Object;)LX/ClL;

    .line 2393206
    :cond_0
    iget-object v2, v0, LX/Ckw;->e:LX/ClO;

    invoke-virtual {v4}, LX/ClL;->a()LX/ClM;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/ClO;->a(LX/ClM;)V

    .line 2393207
    :cond_1
    invoke-static {v0, v3, v1}, LX/Ckw;->a(LX/Ckw;LX/0oG;Ljava/util/Map;)V

    .line 2393208
    iget-object v0, p0, LX/GmZ;->U:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckt;

    iget-object v1, p0, LX/GmZ;->M:LX/Chi;

    .line 2393209
    iget-object v2, v1, LX/Chi;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2393210
    iget-object v2, p0, LX/GmZ;->M:LX/Chi;

    .line 2393211
    iget-object v3, v2, LX/Chi;->f:Ljava/lang/String;

    move-object v2, v3

    .line 2393212
    iget-object v3, p0, LX/GmZ;->M:LX/Chi;

    .line 2393213
    iget-object v4, v3, LX/Chi;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2393214
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2393215
    :cond_2
    :goto_0
    return-void

    .line 2393216
    :cond_3
    iget-object v4, v0, LX/Ckt;->d:Ljava/util/Set;

    invoke-interface {v4, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2393217
    iget-object v4, v0, LX/Ckt;->d:Ljava/util/Set;

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2393218
    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2393219
    iget-object v4, v0, LX/Ckt;->g:Ljava/lang/String;

    if-nez v4, :cond_4

    .line 2393220
    iput-object v2, v0, LX/Ckt;->g:Ljava/lang/String;

    .line 2393221
    iput-object v1, v0, LX/Ckt;->h:Ljava/lang/String;

    .line 2393222
    iput-object v3, v0, LX/Ckt;->i:Ljava/lang/String;

    .line 2393223
    const/4 v4, 0x1

    iput v4, v0, LX/Ckt;->j:I

    .line 2393224
    iget-object v4, v0, LX/Ckt;->c:LX/Cig;

    iget-object v5, v0, LX/Ckt;->e:LX/Cii;

    invoke-virtual {v4, v5}, LX/0b4;->a(LX/0b2;)Z

    .line 2393225
    iget-object v4, v0, LX/Ckt;->c:LX/Cig;

    iget-object v5, v0, LX/Ckt;->f:LX/Cij;

    invoke-virtual {v4, v5}, LX/0b4;->a(LX/0b2;)Z

    goto :goto_0

    .line 2393226
    :cond_4
    iget-object v4, v0, LX/Ckt;->g:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2393227
    iget v4, v0, LX/Ckt;->j:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v0, LX/Ckt;->j:I

    goto :goto_0
.end method

.method public final S()Landroid/support/v7/widget/RecyclerView;
    .locals 1

    .prologue
    .line 2393080
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2393081
    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2393078
    const-string v0, "native_article_story"

    return-object v0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2393075
    invoke-super {p0, p1}, LX/GmZ;->a(Landroid/content/Context;)V

    .line 2393076
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2393077
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2393065
    invoke-super {p0, p1, p2}, LX/GmZ;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2393066
    iget-object v0, p0, LX/GmZ;->R:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXX;

    .line 2393067
    iget-object v1, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v1, v1

    .line 2393068
    const-string v2, "extra_instant_articles_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/IXX;->r:Ljava/lang/String;

    .line 2393069
    const-string v2, "extra_instant_articles_referrer"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/IXX;->A:Ljava/lang/String;

    .line 2393070
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2393071
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GmZ;->O:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x3d3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2393072
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2393073
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setDescendantFocusability(I)V

    .line 2393074
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;)LX/Clo;
    .locals 7

    .prologue
    .line 2392994
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2392995
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2392996
    check-cast v0, LX/B5Z;

    .line 2392997
    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ao:LX/B5Z;

    .line 2392998
    iget-object v1, p0, LX/GmZ;->E:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11i;

    sget-object v2, LX/IXZ;->a:LX/IXY;

    invoke-interface {v1, v2}, LX/11i;->e(LX/0Pq;)LX/11o;

    .line 2392999
    if-eqz v0, :cond_7

    invoke-interface {v0}, LX/B5Z;->e()LX/B5Y;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 2393000
    iget-object v1, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v1, v1

    .line 2393001
    const-string v2, "extra_instant_articles_click_url"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2393002
    invoke-interface {v0}, LX/B5Z;->e()LX/B5Y;

    move-result-object v1

    invoke-interface {v1}, LX/B5Y;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-result-object v3

    .line 2393003
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->af:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Chi;

    .line 2393004
    iput-object v3, v1, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    .line 2393005
    new-instance v1, LX/I4D;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3, v2}, LX/I4D;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, LX/I4D;->a(LX/B5Z;)LX/Clo;

    move-result-object v1

    check-cast v1, LX/Clo;

    .line 2393006
    iget-boolean v2, p0, LX/Chc;->A:Z

    if-eqz v2, :cond_0

    .line 2393007
    iget-object v2, p0, LX/Chc;->B:Lcom/facebook/richdocument/RichDocumentFragment;

    move-object v2, v2

    .line 2393008
    instance-of v2, v2, Lcom/facebook/richdocument/view/carousel/PageableFragment;

    if-eqz v2, :cond_0

    invoke-virtual {v1}, LX/Clo;->d()I

    move-result v2

    if-lez v2, :cond_0

    invoke-static {v0}, LX/IXi;->a(LX/B5Z;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2393009
    iget-object v2, p0, LX/Chc;->B:Lcom/facebook/richdocument/RichDocumentFragment;

    move-object v2, v2

    .line 2393010
    check-cast v2, Lcom/facebook/richdocument/view/carousel/PageableFragment;

    .line 2393011
    invoke-virtual {v2}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->m()LX/CqD;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->m()LX/CqD;

    move-result-object v3

    invoke-interface {v3, v2}, LX/CqD;->a(Lcom/facebook/richdocument/view/carousel/PageableFragment;)I

    move-result v2

    if-nez v2, :cond_0

    .line 2393012
    invoke-virtual {v1}, LX/Clo;->d()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    new-instance v3, LX/IXd;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/IXd;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2, v3}, LX/Clo;->b(ILX/Clr;)V

    .line 2393013
    :cond_0
    invoke-interface {v0}, LX/B5Z;->e()LX/B5Y;

    move-result-object v2

    invoke-interface {v2}, LX/B5Y;->t()Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

    move-result-object v3

    .line 2393014
    iget-object v2, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->aa:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Crz;

    .line 2393015
    iput-object v3, v2, LX/Crz;->a:Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

    .line 2393016
    iget-object v2, p0, LX/GmZ;->O:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Uh;

    const/16 v3, 0x168

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2393017
    invoke-direct {p0, v0}, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->a(LX/B5Z;)V

    .line 2393018
    :cond_1
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ap:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ap:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    .line 2393019
    iget-object v2, v0, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->a:LX/CnR;

    move-object v0, v2

    .line 2393020
    if-nez v0, :cond_8

    .line 2393021
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ad:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    new-instance v2, LX/CiP;

    iget-object v3, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    invoke-virtual {p1}, Lcom/facebook/graphql/executor/GraphQLResult;->e()Ljava/util/Set;

    move-result-object v4

    .line 2393022
    if-nez v4, :cond_d

    .line 2393023
    const/4 v5, 0x0

    .line 2393024
    :goto_1
    move v4, v5

    .line 2393025
    invoke-direct {v2, v1, v3, v4}, LX/CiP;-><init>(LX/Clo;LX/0ta;Z)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2393026
    iget-object v0, v1, LX/Clo;->b:Landroid/os/Bundle;

    move-object v0, v0

    .line 2393027
    if-eqz v0, :cond_3

    .line 2393028
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    .line 2393029
    iget-object v2, v1, LX/Clo;->b:Landroid/os/Bundle;

    move-object v2, v2

    .line 2393030
    const-string v3, "publisher_name"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2393031
    iput-object v2, v0, LX/Chi;->f:Ljava/lang/String;

    .line 2393032
    :cond_3
    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->a(Landroid/content/Context;)Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;

    move-result-object v0

    .line 2393033
    iget-boolean v2, p0, LX/Chc;->A:Z

    if-eqz v2, :cond_6

    if-eqz v0, :cond_6

    iget-object v2, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ao:LX/B5Z;

    if-eqz v2, :cond_6

    .line 2393034
    iget-object v2, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ao:LX/B5Z;

    .line 2393035
    iget-boolean v3, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->M:Z

    if-nez v3, :cond_6

    if-eqz v2, :cond_6

    .line 2393036
    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->M:Z

    .line 2393037
    iput-object v2, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->I:LX/B5Z;

    .line 2393038
    invoke-interface {v2}, LX/B5Z;->e()LX/B5Y;

    move-result-object v3

    .line 2393039
    if-eqz v3, :cond_5

    invoke-interface {v3}, LX/B5Y;->n()LX/8Yp;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-interface {v3}, LX/B5Y;->n()LX/8Yp;

    move-result-object v4

    invoke-interface {v4}, LX/8Yp;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->J:LX/IXr;

    if-eqz v4, :cond_5

    .line 2393040
    iget-object v4, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->L:Landroid/widget/TextView;

    if-eqz v4, :cond_5

    .line 2393041
    invoke-interface {v3}, LX/B5Y;->n()LX/8Yp;

    move-result-object v3

    invoke-interface {v3}, LX/8Yp;->e()Ljava/lang/String;

    move-result-object v3

    .line 2393042
    iget-object v4, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->L:Landroid/widget/TextView;

    const/16 v5, 0x1e

    .line 2393043
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-gt v6, v5, :cond_e

    .line 2393044
    :cond_4
    :goto_2
    move-object v3, v3

    .line 2393045
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2393046
    :cond_5
    invoke-static {v0}, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->m(Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;)V

    .line 2393047
    :cond_6
    :goto_3
    return-object v1

    :cond_7
    const/4 v1, 0x0

    goto :goto_3

    .line 2393048
    :cond_8
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ao:LX/B5Z;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ao:LX/B5Z;

    invoke-interface {v0}, LX/B5Z;->c()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-nez v0, :cond_a

    .line 2393049
    :cond_9
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->aq:LX/CnR;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, LX/CnR;->setVisibility(I)V

    goto/16 :goto_0

    .line 2393050
    :cond_a
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ao:LX/B5Z;

    invoke-interface {v0}, LX/B5Z;->c()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 2393051
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ar:LX/IXt;

    sget-object v3, LX/IXt;->CONDENSED:LX/IXt;

    if-eq v0, v3, :cond_b

    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ar:LX/IXt;

    sget-object v3, LX/IXt;->FEED:LX/IXt;

    if-ne v0, v3, :cond_c

    .line 2393052
    :cond_b
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ao:LX/B5Z;

    invoke-interface {v0}, LX/B5Z;->e()LX/B5Y;

    move-result-object v3

    .line 2393053
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->aq:LX/CnR;

    check-cast v0, LX/CnL;

    .line 2393054
    invoke-interface {v3}, LX/B5Y;->q()LX/8Z4;

    move-result-object v4

    invoke-static {v4}, LX/IXa;->a(LX/8Z4;)LX/Cly;

    move-result-object v4

    invoke-interface {v0, v4}, LX/CnL;->setFeedbackHeaderTitle(LX/Cly;)V

    .line 2393055
    invoke-static {v3}, LX/IXa;->a(LX/B5Y;)LX/ClZ;

    move-result-object v4

    invoke-static {v4}, LX/IXa;->a(LX/ClZ;)LX/CmQ;

    move-result-object v4

    invoke-interface {v0, v4}, LX/CnL;->setLogoInformation(LX/CmQ;)V

    .line 2393056
    new-instance v4, LX/CmC;

    invoke-interface {v3}, LX/B5Y;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel;

    move-result-object v5

    invoke-interface {v3}, LX/B5Y;->e()LX/0Px;

    move-result-object v6

    invoke-direct {v4, v5, v6}, LX/CmC;-><init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel;Ljava/util/List;)V

    invoke-virtual {v4}, LX/CmC;->c()LX/CmD;

    move-result-object v4

    move-object v3, v4

    .line 2393057
    invoke-interface {v0, v3}, LX/CnL;->setFeedbackHeaderAuthorByline(LX/CmD;)V

    .line 2393058
    iget-object v3, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v3, v3

    .line 2393059
    const-string v4, "extra_instant_articles_click_url"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, LX/CnL;->setShareUrl(Ljava/lang/String;)V

    .line 2393060
    :cond_c
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->aq:LX/CnR;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/CnR;->setVisibility(I)V

    .line 2393061
    iget-object v3, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->aq:LX/CnR;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES_AND_COMMENTS:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->aa:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Crz;

    invoke-static {v4, v2, v0}, LX/ClW;->a(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;LX/Crz;)LX/ClW;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/CnR;->setAnnotation(LX/ClW;)V

    goto/16 :goto_0

    :cond_d
    const-string v5, "FROM_MEMORY_CACHE"

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    goto/16 :goto_1

    .line 2393062
    :cond_e
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2393063
    const/4 p0, 0x0

    add-int/lit8 v2, v5, -0x3

    invoke-virtual {v3, p0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    const-string v2, "..."

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2393064
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2
.end method

.method public final b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, -0x1

    .line 2392968
    iget-object v0, p0, LX/GmZ;->R:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXX;

    .line 2392969
    iget-object v4, v0, LX/IXX;->b:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    iput-wide v4, v0, LX/IXX;->v:J

    .line 2392970
    invoke-super {p0, p1, p2, p3}, LX/GmZ;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 2392971
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ad:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    iget-object v3, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->au:LX/Ci5;

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 2392972
    const v0, 0x7f0d1683

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ap:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    .line 2392973
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ap:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/Chc;->A:Z

    if-nez v0, :cond_1

    .line 2392974
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ag:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8bL;

    const-string v3, "SHARE_BUTTON"

    invoke-virtual {v0, v3}, LX/8bL;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/IXt;->valueOf(Ljava/lang/String;)LX/IXt;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ar:LX/IXt;

    .line 2392975
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ar:LX/IXt;

    if-eqz v0, :cond_1

    .line 2392976
    sget-object v0, LX/Gmw;->a:[I

    iget-object v3, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ar:LX/IXt;

    invoke-virtual {v3}, LX/IXt;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 2392977
    :goto_0
    if-eq v0, v1, :cond_1

    .line 2392978
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->aq:LX/CnR;

    if-eqz v1, :cond_0

    .line 2392979
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ap:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    iget-object v3, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->aq:LX/CnR;

    invoke-virtual {v1, v3}, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->removeView(Landroid/view/View;)V

    .line 2392980
    :cond_0
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->X:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/K2B;

    .line 2392981
    const/4 v3, 0x1

    move v1, v3

    .line 2392982
    if-eqz v1, :cond_1

    .line 2392983
    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v3, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ap:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/CnR;

    iput-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->aq:LX/CnR;

    .line 2392984
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->aq:LX/CnR;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, LX/CnR;->setVisibility(I)V

    .line 2392985
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ap:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    iget-object v3, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->aq:LX/CnR;

    .line 2392986
    iput-object v3, v1, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->a:LX/CnR;

    .line 2392987
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->aq:LX/CnR;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, LX/CnR;->setShowTopDivider(Z)V

    .line 2392988
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v1, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2392989
    const/16 v3, 0x50

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 2392990
    iget-object v3, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ap:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    iget-object v4, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->aq:LX/CnR;

    invoke-virtual {v3, v4, v1}, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2392991
    :cond_1
    return-object v2

    .line 2392992
    :pswitch_0
    const v0, 0x7f030122

    goto :goto_0

    .line 2392993
    :pswitch_1
    const v0, 0x7f03011d

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2392962
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2392963
    invoke-static {v0}, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 2392964
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2392965
    const-string p0, "instant_article_id"

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2392966
    move-object v0, v1

    .line 2392967
    return-object v0
.end method

.method public final b(LX/Clo;)V
    .locals 5

    .prologue
    .line 2392942
    invoke-super {p0, p1}, LX/GmZ;->b(LX/Clo;)V

    .line 2392943
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2392944
    const-string v1, "extra_instant_articles_featured_element_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2392945
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2392946
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->V()V

    .line 2392947
    return-void

    .line 2392948
    :cond_1
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2392949
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v1

    .line 2392950
    check-cast v0, LX/CoJ;

    .line 2392951
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 2392952
    invoke-virtual {v0, v1}, LX/CoJ;->e(I)LX/Clr;

    move-result-object v3

    .line 2392953
    invoke-interface {v3}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v3

    .line 2392954
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2392955
    const/4 v0, 0x1

    .line 2392956
    iget-object v2, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v2, v2

    .line 2392957
    check-cast v2, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;

    .line 2392958
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v3

    check-cast v3, LX/1OS;

    .line 2392959
    new-instance v4, LX/Ct6;

    new-instance p1, LX/Gmu;

    invoke-direct {p1, p0, v0, v3}, LX/Gmu;-><init>(Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;ZLX/1OS;)V

    invoke-direct {v4, v1, p1}, LX/Ct6;-><init>(ILX/Ct5;)V

    invoke-virtual {v2, v4}, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->a(LX/Ct6;)V

    .line 2392960
    goto :goto_0

    .line 2392961
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public final b(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2392935
    iget-object v0, p0, LX/GmZ;->F:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->am:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".onFetchError"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error attempting to fetch blocks. article id("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2392936
    iget-object v3, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v3, v3

    .line 2392937
    invoke-static {v3}, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2392938
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2392939
    move-object v1, v1

    .line 2392940
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2392941
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 2392932
    iget-boolean v0, p0, LX/Chc;->A:Z

    if-eqz v0, :cond_0

    .line 2392933
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ah:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CsO;

    sget-object v1, LX/Crd;->BACK:LX/Crd;

    invoke-virtual {v0, v1}, LX/CsO;->a(LX/Crd;)Z

    move-result v0

    .line 2392934
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, LX/GmZ;->c()Z

    move-result v0

    goto :goto_0
.end method

.method public final m()V
    .locals 0

    .prologue
    .line 2392929
    invoke-super {p0}, LX/GmZ;->m()V

    .line 2392930
    invoke-direct {p0}, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->V()V

    .line 2392931
    return-void
.end method

.method public final v()V
    .locals 5

    .prologue
    .line 2392902
    invoke-super {p0}, LX/GmZ;->v()V

    .line 2392903
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->Z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CjG;

    .line 2392904
    iget-object v1, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v1, v1

    .line 2392905
    invoke-static {v1}, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/CjG;->a(Ljava/lang/String;)V

    .line 2392906
    iget-object v0, p0, LX/GmZ;->L:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x441

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2392907
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2392908
    check-cast v0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;

    .line 2392909
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v1, v1

    .line 2392910
    check-cast v1, LX/CoJ;

    .line 2392911
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1OS;

    .line 2392912
    invoke-interface {v0}, LX/1OS;->l()I

    move-result v3

    .line 2392913
    const/4 v2, 0x0

    .line 2392914
    invoke-interface {v0}, LX/1OS;->n()I

    move-result v0

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_1

    .line 2392915
    invoke-virtual {v1, v3}, LX/CoJ;->e(I)LX/Clr;

    move-result-object v0

    invoke-interface {v0}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2392916
    :goto_0
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2392917
    invoke-static {v0}, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    .line 2392918
    new-instance v0, LX/4GP;

    invoke-direct {v0}, LX/4GP;-><init>()V

    .line 2392919
    const-string v3, "article_id"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2392920
    move-object v0, v0

    .line 2392921
    const-string v3, "block_id"

    invoke-virtual {v0, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2392922
    move-object v0, v0

    .line 2392923
    new-instance v3, LX/B5S;

    invoke-direct {v3}, LX/B5S;-><init>()V

    move-object v3, v3

    .line 2392924
    const-string v4, "input"

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2392925
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2392926
    iget-object v3, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->al:LX/0tX;

    invoke-virtual {v3, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2392927
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ak:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Sc;

    invoke-virtual {v0, v2, v1}, LX/1Sc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2392928
    :cond_0
    return-void

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public y()I
    .locals 1

    .prologue
    .line 2392899
    iget-boolean v0, p0, LX/Chc;->A:Z

    if-eqz v0, :cond_0

    .line 2392900
    const v0, 0x7f0308b5

    .line 2392901
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, LX/GmZ;->y()I

    move-result v0

    goto :goto_0
.end method
