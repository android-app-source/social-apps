.class public Lcom/facebook/internal/FacebookDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source ""


# instance fields
.field public j:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2399135
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/internal/FacebookDialogFragment;Landroid/os/Bundle;LX/GAA;)V
    .locals 3

    .prologue
    .line 2399083
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2399084
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0, p1, p2}, LX/GsS;->a(Landroid/content/Intent;Landroid/os/Bundle;LX/GAA;)Landroid/content/Intent;

    move-result-object v2

    .line 2399085
    if-nez p2, :cond_0

    const/4 v0, -0x1

    .line 2399086
    :goto_0
    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2399087
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2399088
    return-void

    .line 2399089
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2399130
    iget-object v0, p0, Lcom/facebook/internal/FacebookDialogFragment;->j:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 2399131
    invoke-static {p0, v1, v1}, Lcom/facebook/internal/FacebookDialogFragment;->a$redex0(Lcom/facebook/internal/FacebookDialogFragment;Landroid/os/Bundle;LX/GAA;)V

    .line 2399132
    const/4 v0, 0x0

    .line 2399133
    iput-boolean v0, p0, Landroid/support/v4/app/DialogFragment;->d:Z

    .line 2399134
    :cond_0
    iget-object v0, p0, Lcom/facebook/internal/FacebookDialogFragment;->j:Landroid/app/Dialog;

    return-object v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2399126
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2399127
    iget-object v0, p0, Lcom/facebook/internal/FacebookDialogFragment;->j:Landroid/app/Dialog;

    instance-of v0, v0, LX/GsI;

    if-eqz v0, :cond_0

    .line 2399128
    iget-object v0, p0, Lcom/facebook/internal/FacebookDialogFragment;->j:Landroid/app/Dialog;

    check-cast v0, LX/GsI;

    invoke-virtual {v0}, LX/GsI;->d()V

    .line 2399129
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v6, 0x0

    const/16 v0, 0x2a

    const v1, 0x3361e335

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2399098
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2399099
    iget-object v0, p0, Lcom/facebook/internal/FacebookDialogFragment;->j:Landroid/app/Dialog;

    if-nez v0, :cond_1

    .line 2399100
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 2399101
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2399102
    invoke-static {v0}, LX/GsS;->a(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    .line 2399103
    const-string v3, "is_fallback"

    invoke-virtual {v0, v3, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 2399104
    if-nez v3, :cond_2

    .line 2399105
    const-string v3, "action"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2399106
    const-string v4, "params"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2399107
    invoke-static {v3}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2399108
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2399109
    const/16 v0, 0x2b

    const v2, 0x628ae09e

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2399110
    :goto_0
    return-void

    .line 2399111
    :cond_0
    new-instance v4, LX/Gsi;

    invoke-direct {v4, v2, v3, v0}, LX/Gsi;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V

    new-instance v0, LX/GsC;

    invoke-direct {v0, p0}, LX/GsC;-><init>(Lcom/facebook/internal/FacebookDialogFragment;)V

    .line 2399112
    iput-object v0, v4, LX/Gsi;->e:LX/GsB;

    .line 2399113
    move-object v0, v4

    .line 2399114
    invoke-virtual {v0}, LX/Gsi;->a()LX/GsI;

    move-result-object v0

    .line 2399115
    :goto_1
    iput-object v0, p0, Lcom/facebook/internal/FacebookDialogFragment;->j:Landroid/app/Dialog;

    .line 2399116
    :cond_1
    const v0, -0x16783671

    invoke-static {v0, v1}, LX/02F;->f(II)V

    goto :goto_0

    .line 2399117
    :cond_2
    const-string v3, "url"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2399118
    invoke-static {v3}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2399119
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2399120
    const v0, -0xc3b2cd2

    invoke-static {v0, v1}, LX/02F;->f(II)V

    goto :goto_0

    .line 2399121
    :cond_3
    const-string v0, "fb%s://bridge/"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {}, LX/GAK;->i()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2399122
    new-instance v0, LX/GsJ;

    invoke-direct {v0, v2, v3, v4}, LX/GsJ;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2399123
    new-instance v2, LX/GsD;

    invoke-direct {v2, p0}, LX/GsD;-><init>(Lcom/facebook/internal/FacebookDialogFragment;)V

    .line 2399124
    iput-object v2, v0, LX/GsI;->c:LX/GsB;

    .line 2399125
    goto :goto_1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7c2e09ec

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2399090
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 2399091
    if-eqz v1, :cond_0

    .line 2399092
    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->mRetainInstance:Z

    move v1, v1

    .line 2399093
    if-eqz v1, :cond_0

    .line 2399094
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 2399095
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 2399096
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDestroyView()V

    .line 2399097
    const/16 v1, 0x2b

    const v2, -0x25d8947c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
