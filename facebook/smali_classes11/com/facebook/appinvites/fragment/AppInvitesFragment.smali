.class public Lcom/facebook/appinvites/fragment/AppInvitesFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final h:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final i:Ljava/lang/String;


# instance fields
.field public a:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/GRN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/GRy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/2FX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/GRM;

.field private k:LX/1B1;

.field public l:LX/GRX;

.field public m:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

.field public n:Landroid/widget/ListView;

.field public o:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private p:Landroid/view/View;

.field public q:Landroid/view/ViewStub;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2351735
    const-class v0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    sput-object v0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->h:Ljava/lang/Class;

    .line 2351736
    sget-object v0, LX/0ax;->ej:Ljava/lang/String;

    sput-object v0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2351798
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2351799
    return-void
.end method

.method public static a$redex0(Lcom/facebook/appinvites/fragment/AppInvitesFragment;Z)V
    .locals 5

    .prologue
    .line 2351791
    iget-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->b:LX/1Ck;

    sget-object v1, LX/GRo;->FETCH_INVITES:LX/GRo;

    .line 2351792
    new-instance v2, LX/GSX;

    invoke-direct {v2}, LX/GSX;-><init>()V

    move-object v2, v2

    .line 2351793
    const-string v3, "invite_image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v2

    const-string v3, "app_profile_image_size"

    const v4, 0x7f0b238b

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "sender_profile_image_size"

    const v4, 0x7f0b238a

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "request_type"

    const-string v4, "INVITE"

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "device"

    const-string v4, "ANDROID"

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/GSX;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    .line 2351794
    iget-object v3, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    move-object v2, v2

    .line 2351795
    new-instance v3, LX/GRn;

    invoke-direct {v3, p0, p1}, LX/GRn;-><init>(Lcom/facebook/appinvites/fragment/AppInvitesFragment;Z)V

    move-object v3, v3

    .line 2351796
    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2351797
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2351761
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2351762
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    const-class v5, LX/GRN;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/GRN;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v6

    check-cast v6, LX/17W;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v7

    check-cast v7, LX/0iA;

    invoke-static {v0}, LX/GRy;->a(LX/0QB;)LX/GRy;

    move-result-object p1

    check-cast p1, LX/GRy;

    invoke-static {v0}, LX/2FX;->a(LX/0QB;)LX/2FX;

    move-result-object v0

    check-cast v0, LX/2FX;

    iput-object v3, v2, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->a:LX/0tX;

    iput-object v4, v2, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->b:LX/1Ck;

    iput-object v5, v2, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->c:LX/GRN;

    iput-object v6, v2, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->d:LX/17W;

    iput-object v7, v2, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->e:LX/0iA;

    iput-object p1, v2, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->f:LX/GRy;

    iput-object v0, v2, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->g:LX/2FX;

    .line 2351763
    new-instance v0, LX/GRX;

    invoke-direct {v0}, LX/GRX;-><init>()V

    iput-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->l:LX/GRX;

    .line 2351764
    iget-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->c:LX/GRN;

    iget-object v1, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->l:LX/GRX;

    .line 2351765
    new-instance v3, LX/GRM;

    invoke-static {v0}, LX/GTB;->b(LX/0QB;)LX/GTB;

    move-result-object v2

    check-cast v2, LX/GTB;

    invoke-direct {v3, v1, v2}, LX/GRM;-><init>(LX/GRX;LX/GTB;)V

    .line 2351766
    move-object v0, v3

    .line 2351767
    iput-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->j:LX/GRM;

    .line 2351768
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->k:LX/1B1;

    .line 2351769
    iget-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->k:LX/1B1;

    iget-object v1, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->j:LX/GRM;

    .line 2351770
    new-instance v2, LX/GRG;

    invoke-direct {v2, v1}, LX/GRG;-><init>(LX/GRM;)V

    move-object v1, v2

    .line 2351771
    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2351772
    iget-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->k:LX/1B1;

    iget-object v1, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->j:LX/GRM;

    .line 2351773
    new-instance v2, LX/GRH;

    invoke-direct {v2, v1}, LX/GRH;-><init>(LX/GRM;)V

    move-object v1, v2

    .line 2351774
    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2351775
    iget-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->k:LX/1B1;

    iget-object v1, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->j:LX/GRM;

    .line 2351776
    new-instance v2, LX/GRI;

    invoke-direct {v2, v1}, LX/GRI;-><init>(LX/GRM;)V

    move-object v1, v2

    .line 2351777
    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2351778
    iget-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->k:LX/1B1;

    iget-object v1, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->j:LX/GRM;

    .line 2351779
    new-instance v2, LX/GRJ;

    invoke-direct {v2, v1}, LX/GRJ;-><init>(LX/GRM;)V

    move-object v1, v2

    .line 2351780
    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2351781
    iget-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->k:LX/1B1;

    iget-object v1, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->j:LX/GRM;

    .line 2351782
    new-instance v2, LX/GRK;

    invoke-direct {v2, v1}, LX/GRK;-><init>(LX/GRM;)V

    move-object v1, v2

    .line 2351783
    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2351784
    iget-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->k:LX/1B1;

    iget-object v1, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->j:LX/GRM;

    .line 2351785
    new-instance v2, LX/GRL;

    invoke-direct {v2, v1}, LX/GRL;-><init>(LX/GRM;)V

    move-object v1, v2

    .line 2351786
    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2351787
    iget-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->k:LX/1B1;

    iget-object v1, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->g:LX/2FX;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 2351788
    iget-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->f:LX/GRy;

    .line 2351789
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/GRy;->a:Z

    .line 2351790
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x39f09fe9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2351743
    const v0, 0x7f0300f1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2351744
    const v0, 0x7f0d0566

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->n:Landroid/widget/ListView;

    .line 2351745
    iget-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->n:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->j:LX/GRM;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2351746
    iget-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->n:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setOverScrollMode(I)V

    .line 2351747
    iget-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->n:Landroid/widget/ListView;

    new-instance v3, LX/GRj;

    invoke-direct {v3, p0}, LX/GRj;-><init>(Lcom/facebook/appinvites/fragment/AppInvitesFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2351748
    const v0, 0x7f0d0565

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    iput-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->m:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    .line 2351749
    const v0, 0x7f0d0564

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->o:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2351750
    iget-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->m:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v0, v5}, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->setVisibility(I)V

    .line 2351751
    iget-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->m:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    new-instance v3, LX/GRk;

    invoke-direct {v3, p0}, LX/GRk;-><init>(Lcom/facebook/appinvites/fragment/AppInvitesFragment;)V

    invoke-virtual {v0, v3}, LX/62l;->setOnRefreshListener(LX/62n;)V

    .line 2351752
    const v0, 0x7f0d0567

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->p:Landroid/view/View;

    .line 2351753
    iget-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->n:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->p:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 2351754
    iget-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->p:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2351755
    const v0, 0x7f0d056a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 2351756
    new-instance v3, LX/GRl;

    invoke-direct {v3, p0}, LX/GRl;-><init>(Lcom/facebook/appinvites/fragment/AppInvitesFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2351757
    const v0, 0x7f0d0563

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->q:Landroid/view/ViewStub;

    .line 2351758
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->a$redex0(Lcom/facebook/appinvites/fragment/AppInvitesFragment;Z)V

    .line 2351759
    iget-object v0, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->o:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2351760
    const/16 v0, 0x2b

    const v3, -0x7b4e8fc0

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4fe9c6ae

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2351740
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2351741
    iget-object v1, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->k:LX/1B1;

    iget-object v2, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->g:LX/2FX;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2351742
    const/16 v1, 0x2b

    const v2, -0x3c929984

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x64be852d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2351737
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2351738
    iget-object v1, p0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->b:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2351739
    const/16 v1, 0x2b

    const v2, -0x10b28b0f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
