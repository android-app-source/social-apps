.class public Lcom/facebook/appinvites/fragment/AppInvitesSettingsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/2FX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/GRa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/GSx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2351820
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2351821
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2351822
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/appinvites/fragment/AppInvitesSettingsFragment;

    invoke-static {p1}, LX/2FX;->a(LX/0QB;)LX/2FX;

    move-result-object v2

    check-cast v2, LX/2FX;

    invoke-static {p1}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v3

    check-cast v3, LX/0kL;

    invoke-static {p1}, LX/GRa;->a(LX/0QB;)LX/GRa;

    move-result-object v4

    check-cast v4, LX/GRa;

    const-class v0, LX/GSx;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/GSx;

    iput-object v2, p0, Lcom/facebook/appinvites/fragment/AppInvitesSettingsFragment;->a:LX/2FX;

    iput-object v3, p0, Lcom/facebook/appinvites/fragment/AppInvitesSettingsFragment;->b:LX/0kL;

    iput-object v4, p0, Lcom/facebook/appinvites/fragment/AppInvitesSettingsFragment;->c:LX/GRa;

    iput-object p1, p0, Lcom/facebook/appinvites/fragment/AppInvitesSettingsFragment;->d:LX/GSx;

    .line 2351823
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x67e74f16

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2351824
    const v1, 0x7f0300f8

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/appinvites/fragment/AppInvitesSettingsFragment;->e:Landroid/view/View;

    .line 2351825
    iget-object v1, p0, Lcom/facebook/appinvites/fragment/AppInvitesSettingsFragment;->e:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, -0xdd20498

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2351826
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2351827
    iget-object p1, p0, Lcom/facebook/appinvites/fragment/AppInvitesSettingsFragment;->e:Landroid/view/View;

    const p2, 0x7f0d057f

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 2351828
    new-instance p2, LX/GRs;

    invoke-direct {p2, p0}, LX/GRs;-><init>(Lcom/facebook/appinvites/fragment/AppInvitesSettingsFragment;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2351829
    iget-object p1, p0, Lcom/facebook/appinvites/fragment/AppInvitesSettingsFragment;->e:Landroid/view/View;

    const p2, 0x7f0d0580

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 2351830
    new-instance p2, LX/GRr;

    invoke-direct {p2, p0}, LX/GRr;-><init>(Lcom/facebook/appinvites/fragment/AppInvitesSettingsFragment;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2351831
    iget-object p1, p0, Lcom/facebook/appinvites/fragment/AppInvitesSettingsFragment;->e:Landroid/view/View;

    const p2, 0x7f0d0581

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 2351832
    new-instance p2, LX/GRq;

    invoke-direct {p2, p0}, LX/GRq;-><init>(Lcom/facebook/appinvites/fragment/AppInvitesSettingsFragment;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2351833
    return-void
.end method
