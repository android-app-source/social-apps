.class public Lcom/facebook/appinvites/sdk/AppInviteDialogActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field public p:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/4hk;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:LX/4hk;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2354784
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/appinvites/sdk/AppInviteDialogActivity;LX/0Or;LX/0So;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/appinvites/sdk/AppInviteDialogActivity;",
            "LX/0Or",
            "<",
            "LX/4hk;",
            ">;",
            "LX/0So;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2354785
    iput-object p1, p0, Lcom/facebook/appinvites/sdk/AppInviteDialogActivity;->p:LX/0Or;

    iput-object p2, p0, Lcom/facebook/appinvites/sdk/AppInviteDialogActivity;->q:LX/0So;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/appinvites/sdk/AppInviteDialogActivity;

    const/16 v1, 0x2f8d

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-static {p0, v1, v0}, Lcom/facebook/appinvites/sdk/AppInviteDialogActivity;->a(Lcom/facebook/appinvites/sdk/AppInviteDialogActivity;LX/0Or;LX/0So;)V

    return-void
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2354786
    const-string v0, "\\+"

    const-string v1, "%20"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 2354787
    invoke-static {p0, p0}, Lcom/facebook/appinvites/sdk/AppInviteDialogActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2354788
    invoke-virtual {p0}, Lcom/facebook/appinvites/sdk/AppInviteDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2354789
    if-eqz v1, :cond_0

    .line 2354790
    const-string v0, "application_id"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2354791
    const-string v2, "application_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2354792
    const-string v3, "applink_url"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2354793
    const-string v4, "preview_image_url"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2354794
    const-string v5, "promo_code"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2354795
    const-string v6, "promo_text"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2354796
    const-string v6, "destination"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2354797
    const-string v7, "source"

    invoke-virtual {v1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2354798
    const-string v9, "sponsored_context"

    invoke-virtual {v1, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 2354799
    if-eqz v2, :cond_1

    invoke-static {v2}, Lcom/facebook/appinvites/sdk/AppInviteDialogActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    if-eqz v3, :cond_2

    invoke-static {v3}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    if-eqz v4, :cond_3

    invoke-static {v4}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_2
    if-eqz v5, :cond_4

    invoke-static {v5}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_3
    if-eqz v8, :cond_5

    invoke-static {v8}, Lcom/facebook/appinvites/sdk/AppInviteDialogActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :goto_4
    if-eqz v9, :cond_6

    invoke-static {v9}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    :goto_5
    invoke-virtual {p0}, Lcom/facebook/appinvites/sdk/AppInviteDialogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-static/range {v0 .. v9}, LX/GSr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v4

    .line 2354800
    iget-object v0, p0, Lcom/facebook/appinvites/sdk/AppInviteDialogActivity;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4hk;

    iput-object v0, p0, Lcom/facebook/appinvites/sdk/AppInviteDialogActivity;->r:LX/4hk;

    .line 2354801
    iget-object v1, p0, Lcom/facebook/appinvites/sdk/AppInviteDialogActivity;->r:LX/4hk;

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/facebook/appinvites/sdk/AppInviteDialogActivity;->q:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v6

    move-object v2, v10

    move-object v3, p0

    invoke-virtual/range {v1 .. v7}, LX/4hk;->a(Landroid/os/Bundle;Landroid/app/Activity;Landroid/content/Intent;ZJ)V

    .line 2354802
    invoke-virtual {p0}, Lcom/facebook/appinvites/sdk/AppInviteDialogActivity;->finish()V

    .line 2354803
    :cond_0
    return-void

    :cond_1
    move-object v1, v10

    .line 2354804
    goto :goto_0

    :cond_2
    move-object v2, v10

    goto :goto_1

    :cond_3
    move-object v3, v10

    goto :goto_2

    :cond_4
    move-object v4, v10

    goto :goto_3

    :cond_5
    move-object v5, v10

    goto :goto_4

    :cond_6
    move-object v8, v10

    goto :goto_5
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x7c084d49

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2354805
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2354806
    const/16 v1, 0x23

    const v2, 0x2dffcd44

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
