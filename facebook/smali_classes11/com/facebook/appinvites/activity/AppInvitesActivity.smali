.class public Lcom/facebook/appinvites/activity/AppInvitesActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/67U;


# instance fields
.field private p:Z

.field private q:LX/67X;

.field private r:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/67X;",
            ">;"
        }
    .end annotation
.end field

.field private s:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public t:LX/GRv;

.field private u:LX/0h5;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2351335
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a(LX/0Or;LX/63V;LX/0Or;LX/GRv;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/appinvites/annotations/ArePeerPressureAppInvitesEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/63V;",
            "LX/0Or",
            "<",
            "LX/67X;",
            ">;",
            "LX/GRv;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2351389
    iput-object p1, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->s:LX/0Or;

    .line 2351390
    invoke-virtual {p2}, LX/63V;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->p:Z

    .line 2351391
    iput-object p3, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->r:LX/0Or;

    .line 2351392
    iput-object p4, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->t:LX/GRv;

    .line 2351393
    return-void
.end method

.method public static a(Lcom/facebook/appinvites/activity/AppInvitesActivity;)V
    .locals 5

    .prologue
    .line 2351394
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/appinvites/activity/AppInvitesActivity;->b(Z)V

    .line 2351395
    invoke-interface {p0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    .line 2351396
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f040056

    const v2, 0x7f040092

    const v3, 0x7f040055

    const v4, 0x7f040093

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0hH;->a(IIII)LX/0hH;

    move-result-object v0

    const v1, 0x7f0d0582

    new-instance v2, Lcom/facebook/appinvites/fragment/AppInvitesSettingsFragment;

    invoke-direct {v2}, Lcom/facebook/appinvites/fragment/AppInvitesSettingsFragment;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2351397
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;

    const/16 v0, 0x144b

    invoke-static {v1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {v1}, LX/63V;->a(LX/0QB;)LX/63V;

    move-result-object v0

    check-cast v0, LX/63V;

    const/16 v3, 0x1677

    invoke-static {v1, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {v1}, LX/GRv;->b(LX/0QB;)LX/GRv;

    move-result-object v1

    check-cast v1, LX/GRv;

    invoke-direct {p0, v2, v0, v3, v1}, Lcom/facebook/appinvites/activity/AppInvitesActivity;->a(LX/0Or;LX/63V;LX/0Or;LX/GRv;)V

    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 2351376
    iget-object v0, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->u:LX/0h5;

    if-nez v0, :cond_0

    .line 2351377
    :goto_0
    return-void

    .line 2351378
    :cond_0
    if-eqz p1, :cond_1

    .line 2351379
    iget-object v0, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->u:LX/0h5;

    const v1, 0x7f083782

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 2351380
    iget-object v0, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->u:LX/0h5;

    .line 2351381
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2351382
    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    goto :goto_0

    .line 2351383
    :cond_1
    iget-object v0, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->u:LX/0h5;

    const v1, 0x7f08376a

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 2351384
    iget-object v0, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->u:LX/0h5;

    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const v2, 0x7f020bf7

    .line 2351385
    iput v2, v1, LX/108;->i:I

    .line 2351386
    move-object v1, v1

    .line 2351387
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2351388
    iget-object v0, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->u:LX/0h5;

    new-instance v1, LX/GR9;

    invoke-direct {v1, p0}, LX/GR9;-><init>(Lcom/facebook/appinvites/activity/AppInvitesActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2351370
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 2351371
    invoke-static {p0, p0}, Lcom/facebook/appinvites/activity/AppInvitesActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2351372
    iget-boolean v0, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->p:Z

    if-eqz v0, :cond_0

    .line 2351373
    iget-object v0, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->r:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67X;

    iput-object v0, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->q:LX/67X;

    .line 2351374
    iget-object v0, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->q:LX/67X;

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    .line 2351375
    :cond_0
    return-void
.end method

.method public final b()LX/3u1;
    .locals 1

    .prologue
    .line 2351367
    iget-boolean v0, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->p:Z

    if-eqz v0, :cond_0

    .line 2351368
    iget-object v0, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->q:LX/67X;

    invoke-virtual {v0}, LX/67X;->g()LX/3u1;

    move-result-object v0

    .line 2351369
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2351339
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2351340
    iget-object v0, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2351341
    invoke-virtual {p0}, Lcom/facebook/appinvites/activity/AppInvitesActivity;->finish()V

    .line 2351342
    :goto_0
    return-void

    .line 2351343
    :cond_0
    const v0, 0x7f0300f9

    invoke-virtual {p0, v0}, Lcom/facebook/appinvites/activity/AppInvitesActivity;->setContentView(I)V

    .line 2351344
    iget-boolean v0, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->p:Z

    if-eqz v0, :cond_2

    .line 2351345
    iget-object v0, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->q:LX/67X;

    invoke-virtual {v0}, LX/67X;->g()LX/3u1;

    move-result-object v0

    .line 2351346
    new-instance v1, LX/63L;

    invoke-direct {v1, p0, v0}, LX/63L;-><init>(LX/0ew;LX/3u1;)V

    .line 2351347
    iput-object v1, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->u:LX/0h5;

    .line 2351348
    :goto_1
    iget-object v0, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->u:LX/0h5;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0h5;->setHasFbLogo(Z)V

    .line 2351349
    iget-object v0, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->u:LX/0h5;

    new-instance v1, LX/GR8;

    invoke-direct {v1, p0}, LX/GR8;-><init>(Lcom/facebook/appinvites/activity/AppInvitesActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2351350
    invoke-interface {p0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    .line 2351351
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d0582

    new-instance v2, Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    invoke-direct {v2}, Lcom/facebook/appinvites/fragment/AppInvitesFragment;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2351352
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/appinvites/activity/AppInvitesActivity;->b(Z)V

    .line 2351353
    const-string v0, "unknown"

    .line 2351354
    invoke-virtual {p0}, Lcom/facebook/appinvites/activity/AppInvitesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2351355
    const-string v2, "extra_launch_uri"

    .line 2351356
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2351357
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2351358
    const-string v2, "source"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2351359
    const-string v0, "source"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2351360
    :cond_1
    iget-object v1, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->t:LX/GRv;

    const-string v2, "app_invite_view_did_show"

    .line 2351361
    invoke-static {v2}, LX/GRv;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2351362
    const-string p0, "openingSource"

    invoke-virtual {v3, p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2351363
    iget-object p0, v1, LX/GRv;->a:LX/0Zb;

    invoke-interface {p0, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2351364
    goto/16 :goto_0

    .line 2351365
    :cond_2
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2351366
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/appinvites/activity/AppInvitesActivity;->u:LX/0h5;

    goto :goto_1
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 2351336
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/appinvites/activity/AppInvitesActivity;->b(Z)V

    .line 2351337
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2351338
    return-void
.end method
