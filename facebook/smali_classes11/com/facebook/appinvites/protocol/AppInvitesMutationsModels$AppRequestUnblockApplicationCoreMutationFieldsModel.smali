.class public final Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4db6b065
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel$ApplicationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2352554
    const-class v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2352553
    const-class v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2352551
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2352552
    return-void
.end method

.method private a()Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel$ApplicationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2352549
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel;->e:Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel$ApplicationModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel$ApplicationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel$ApplicationModel;

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel;->e:Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel$ApplicationModel;

    .line 2352550
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel;->e:Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel$ApplicationModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2352543
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2352544
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel;->a()Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel$ApplicationModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2352545
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2352546
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2352547
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2352548
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2352535
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2352536
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel;->a()Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel$ApplicationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2352537
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel;->a()Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel$ApplicationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel$ApplicationModel;

    .line 2352538
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel;->a()Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel$ApplicationModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2352539
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel;

    .line 2352540
    iput-object v0, v1, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel;->e:Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel$ApplicationModel;

    .line 2352541
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2352542
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2352530
    new-instance v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestUnblockApplicationCoreMutationFieldsModel;-><init>()V

    .line 2352531
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2352532
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2352534
    const v0, -0x49b00a13

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2352533
    const v0, 0x2544a7d2

    return v0
.end method
