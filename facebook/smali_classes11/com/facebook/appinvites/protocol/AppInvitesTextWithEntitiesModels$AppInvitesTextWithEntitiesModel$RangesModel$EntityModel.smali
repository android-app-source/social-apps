.class public final Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4ad7ddcf
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2352852
    const-class v0, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2352853
    const-class v0, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2352850
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2352851
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2352840
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2352841
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2352842
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2352843
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2352844
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2352845
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2352846
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2352847
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2352848
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2352849
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2352837
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2352838
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2352839
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2352854
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2352834
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2352835
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2352836
    :cond_0
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2352831
    new-instance v0, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel;

    invoke-direct {v0}, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel;-><init>()V

    .line 2352832
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2352833
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2352829
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel;->f:Ljava/lang/String;

    .line 2352830
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2352827
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel;->g:Ljava/lang/String;

    .line 2352828
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2352825
    const v0, -0x19a9e45d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2352826
    const v0, 0x7c02d003

    return v0
.end method
