.class public final Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x46c7cfb9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel$UserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2352331
    const-class v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2352330
    const-class v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2352328
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2352329
    return-void
.end method

.method private a()Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel$UserModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2352307
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel;->e:Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel$UserModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel$UserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel$UserModel;

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel;->e:Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel$UserModel;

    .line 2352308
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel;->e:Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel$UserModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2352322
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2352323
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel;->a()Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel$UserModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2352324
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2352325
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2352326
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2352327
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2352314
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2352315
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel;->a()Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel$UserModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2352316
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel;->a()Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel$UserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel$UserModel;

    .line 2352317
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel;->a()Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel$UserModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2352318
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel;

    .line 2352319
    iput-object v0, v1, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel;->e:Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel$UserModel;

    .line 2352320
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2352321
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2352311
    new-instance v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockUserCoreMutationFieldsModel;-><init>()V

    .line 2352312
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2352313
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2352310
    const v0, -0x246d2234

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2352309
    const v0, 0x1bb0fb54

    return v0
.end method
