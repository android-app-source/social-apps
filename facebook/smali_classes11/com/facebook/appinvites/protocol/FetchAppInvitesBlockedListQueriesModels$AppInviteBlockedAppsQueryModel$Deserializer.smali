.class public final Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2353084
    const-class v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel;

    new-instance v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2353085
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2353086
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2353087
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2353088
    const/4 v2, 0x0

    .line 2353089
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2353090
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2353091
    :goto_0
    move v1, v2

    .line 2353092
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2353093
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2353094
    new-instance v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel;

    invoke-direct {v1}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel;-><init>()V

    .line 2353095
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2353096
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2353097
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2353098
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2353099
    :cond_0
    return-object v1

    .line 2353100
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2353101
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2353102
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2353103
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2353104
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2353105
    const-string v4, "application_request_blocked_applications"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2353106
    const/4 v3, 0x0

    .line 2353107
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 2353108
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2353109
    :goto_2
    move v1, v3

    .line 2353110
    goto :goto_1

    .line 2353111
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2353112
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2353113
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 2353114
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2353115
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 2353116
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2353117
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2353118
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 2353119
    const-string v5, "edges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2353120
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2353121
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_7

    .line 2353122
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_7

    .line 2353123
    const/4 v5, 0x0

    .line 2353124
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_d

    .line 2353125
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2353126
    :goto_5
    move v4, v5

    .line 2353127
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2353128
    :cond_7
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 2353129
    goto :goto_3

    .line 2353130
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 2353131
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2353132
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    goto :goto_3

    .line 2353133
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2353134
    :cond_b
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_c

    .line 2353135
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2353136
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2353137
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_b

    if-eqz v6, :cond_b

    .line 2353138
    const-string p0, "node"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2353139
    invoke-static {p1, v0}, LX/GSS;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_6

    .line 2353140
    :cond_c
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 2353141
    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 2353142
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_5

    :cond_d
    move v4, v5

    goto :goto_6
.end method
