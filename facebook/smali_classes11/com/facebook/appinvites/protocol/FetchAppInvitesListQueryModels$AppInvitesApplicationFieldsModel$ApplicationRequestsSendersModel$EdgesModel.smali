.class public final Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x63d53682
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2354122
    const-class v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2354123
    const-class v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2354130
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2354131
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2354124
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2354125
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel;->a()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2354126
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2354127
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2354128
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2354129
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2354114
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2354115
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel;->a()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2354116
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel;->a()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel;

    .line 2354117
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel;->a()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2354118
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel;

    .line 2354119
    iput-object v0, v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel;->e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel;

    .line 2354120
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2354121
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2354107
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel;->e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel;

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel;->e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel;

    .line 2354108
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel;->e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2354111
    new-instance v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel;-><init>()V

    .line 2354112
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2354113
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2354110
    const v0, -0x47ae6721

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2354109
    const v0, 0x2d63c2fc

    return v0
.end method
