.class public final Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x223736b0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2354184
    const-class v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2354185
    const-class v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2354186
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2354187
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2354188
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel;->f:Ljava/lang/String;

    .line 2354189
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2354190
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2354191
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel;->j()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2354192
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2354193
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2354194
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2354195
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2354196
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2354197
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2354198
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2354199
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel;->j()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2354200
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel;->j()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel;

    .line 2354201
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel;->j()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2354202
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel;

    .line 2354203
    iput-object v0, v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel;->e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel;

    .line 2354204
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2354205
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2354206
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2354207
    new-instance v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel;-><init>()V

    .line 2354208
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2354209
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2354210
    const v0, 0xb31fc43

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2354211
    const v0, -0x3ff252d0

    return v0
.end method

.method public final j()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2354212
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel;->e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel;

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel;->e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel;

    .line 2354213
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel;->e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel;

    return-object v0
.end method
