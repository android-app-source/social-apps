.class public final Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2353468
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2353469
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2353500
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2353501
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2353476
    if-nez p1, :cond_0

    move v0, v1

    .line 2353477
    :goto_0
    return v0

    .line 2353478
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2353479
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2353480
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2353481
    const v2, -0x3171f5c1

    const/4 v5, 0x0

    .line 2353482
    if-nez v0, :cond_1

    move v4, v5

    .line 2353483
    :goto_1
    move v0, v4

    .line 2353484
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2353485
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2353486
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2353487
    :sswitch_1
    const-class v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel$ApplicationRequestBlockedApplicationsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel$ApplicationRequestBlockedApplicationsModel$EdgesModel$NodeModel;

    .line 2353488
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2353489
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2353490
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2353491
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2353492
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    .line 2353493
    if-nez p1, :cond_2

    const/4 v4, 0x0

    .line 2353494
    :goto_2
    if-ge v5, p1, :cond_3

    .line 2353495
    invoke-virtual {p0, v0, v5}, LX/15i;->q(II)I

    move-result p2

    .line 2353496
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v4, v5

    .line 2353497
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 2353498
    :cond_2
    new-array v4, p1, [I

    goto :goto_2

    .line 2353499
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v4, v5}, LX/186;->a([IZ)I

    move-result v4

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x3171f5c1 -> :sswitch_1
        0x4fbe268a -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2353475
    new-instance v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2353470
    if-eqz p0, :cond_0

    .line 2353471
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2353472
    if-eq v0, p0, :cond_0

    .line 2353473
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2353474
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2353455
    sparse-switch p2, :sswitch_data_0

    .line 2353456
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2353457
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2353458
    const v1, -0x3171f5c1

    .line 2353459
    if-eqz v0, :cond_0

    .line 2353460
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2353461
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 2353462
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2353463
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2353464
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2353465
    :cond_0
    :goto_1
    return-void

    .line 2353466
    :sswitch_1
    const-class v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel$ApplicationRequestBlockedApplicationsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel$ApplicationRequestBlockedApplicationsModel$EdgesModel$NodeModel;

    .line 2353467
    invoke-static {v0, p3}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3171f5c1 -> :sswitch_1
        0x4fbe268a -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2353449
    if-eqz p1, :cond_0

    .line 2353450
    invoke-static {p0, p1, p2}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$DraculaImplementation;

    move-result-object v1

    .line 2353451
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$DraculaImplementation;

    .line 2353452
    if-eq v0, v1, :cond_0

    .line 2353453
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2353454
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2353448
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2353502
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2353503
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2353443
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2353444
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2353445
    :cond_0
    iput-object p1, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$DraculaImplementation;->a:LX/15i;

    .line 2353446
    iput p2, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$DraculaImplementation;->b:I

    .line 2353447
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2353442
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2353441
    new-instance v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2353438
    iget v0, p0, LX/1vt;->c:I

    .line 2353439
    move v0, v0

    .line 2353440
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2353417
    iget v0, p0, LX/1vt;->c:I

    .line 2353418
    move v0, v0

    .line 2353419
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2353435
    iget v0, p0, LX/1vt;->b:I

    .line 2353436
    move v0, v0

    .line 2353437
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353432
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2353433
    move-object v0, v0

    .line 2353434
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2353423
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2353424
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2353425
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2353426
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2353427
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2353428
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2353429
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2353430
    invoke-static {v3, v9, v2}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2353431
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2353420
    iget v0, p0, LX/1vt;->c:I

    .line 2353421
    move v0, v0

    .line 2353422
    return v0
.end method
