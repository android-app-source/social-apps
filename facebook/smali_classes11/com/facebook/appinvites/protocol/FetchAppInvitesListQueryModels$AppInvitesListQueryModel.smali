.class public final Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x21b4211d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$ApplicationsWithApplicationRequestsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2354343
    const-class v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2354344
    const-class v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2354345
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2354346
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2354347
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2354348
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel;->a()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$ApplicationsWithApplicationRequestsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2354349
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2354350
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2354351
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2354352
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2354353
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2354354
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel;->a()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$ApplicationsWithApplicationRequestsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2354355
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel;->a()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$ApplicationsWithApplicationRequestsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$ApplicationsWithApplicationRequestsModel;

    .line 2354356
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel;->a()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$ApplicationsWithApplicationRequestsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2354357
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel;

    .line 2354358
    iput-object v0, v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel;->e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$ApplicationsWithApplicationRequestsModel;

    .line 2354359
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2354360
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$ApplicationsWithApplicationRequestsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2354361
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel;->e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$ApplicationsWithApplicationRequestsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$ApplicationsWithApplicationRequestsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$ApplicationsWithApplicationRequestsModel;

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel;->e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$ApplicationsWithApplicationRequestsModel;

    .line 2354362
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel;->e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$ApplicationsWithApplicationRequestsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2354363
    new-instance v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel;

    invoke-direct {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel;-><init>()V

    .line 2354364
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2354365
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2354366
    const v0, 0x2abae023

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2354367
    const v0, -0x6747e1ce

    return v0
.end method
