.class public final Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x53012c93
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLAppInviteSettingsForSenderNotification;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2352031
    const-class v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2352030
    const-class v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2352028
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2352029
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2352026
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;->e:Ljava/lang/String;

    .line 2352027
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLAppInviteSettingsForSenderNotification;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2352024
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;->f:Lcom/facebook/graphql/enums/GraphQLAppInviteSettingsForSenderNotification;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAppInviteSettingsForSenderNotification;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAppInviteSettingsForSenderNotification;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAppInviteSettingsForSenderNotification;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAppInviteSettingsForSenderNotification;

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;->f:Lcom/facebook/graphql/enums/GraphQLAppInviteSettingsForSenderNotification;

    .line 2352025
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;->f:Lcom/facebook/graphql/enums/GraphQLAppInviteSettingsForSenderNotification;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2352007
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2352008
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2352009
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;->k()Lcom/facebook/graphql/enums/GraphQLAppInviteSettingsForSenderNotification;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2352010
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2352011
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2352012
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2352013
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2352014
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2352021
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2352022
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2352023
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2352020
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2352017
    new-instance v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;

    invoke-direct {v0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;-><init>()V

    .line 2352018
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2352019
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2352016
    const v0, 0x12963802

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2352015
    const v0, -0x59b255ef

    return v0
.end method
