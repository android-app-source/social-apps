.class public final Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestDeleteCoreMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5cdb52b2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestDeleteCoreMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestDeleteCoreMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2352444
    const-class v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestDeleteCoreMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2352445
    const-class v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestDeleteCoreMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2352448
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2352449
    return-void
.end method

.method private a()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2352446
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestDeleteCoreMutationFieldsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestDeleteCoreMutationFieldsModel;->e:Ljava/util/List;

    .line 2352447
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestDeleteCoreMutationFieldsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2352438
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2352439
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestDeleteCoreMutationFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 2352440
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2352441
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2352442
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2352443
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2352430
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2352431
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2352432
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2352435
    new-instance v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestDeleteCoreMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestDeleteCoreMutationFieldsModel;-><init>()V

    .line 2352436
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2352437
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2352434
    const v0, 0x2f37be88

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2352433
    const v0, 0x18cf06e3

    return v0
.end method
