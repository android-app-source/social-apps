.class public final Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockApplicationCoreMutationFieldsModel$ApplicationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockApplicationCoreMutationFieldsModel$ApplicationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockApplicationCoreMutationFieldsModel$ApplicationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2352156
    const-class v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockApplicationCoreMutationFieldsModel$ApplicationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2352155
    const-class v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockApplicationCoreMutationFieldsModel$ApplicationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2352153
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2352154
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2352151
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockApplicationCoreMutationFieldsModel$ApplicationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockApplicationCoreMutationFieldsModel$ApplicationModel;->e:Ljava/lang/String;

    .line 2352152
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockApplicationCoreMutationFieldsModel$ApplicationModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2352145
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2352146
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockApplicationCoreMutationFieldsModel$ApplicationModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2352147
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2352148
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2352149
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2352150
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2352142
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2352143
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2352144
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2352141
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockApplicationCoreMutationFieldsModel$ApplicationModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2352138
    new-instance v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockApplicationCoreMutationFieldsModel$ApplicationModel;

    invoke-direct {v0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestBlockApplicationCoreMutationFieldsModel$ApplicationModel;-><init>()V

    .line 2352139
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2352140
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2352136
    const v0, 0x2081bef0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2352137
    const v0, -0x3ff252d0

    return v0
.end method
