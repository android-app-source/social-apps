.class public final Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2353143
    const-class v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel;

    new-instance v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2353144
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2353170
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2353146
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2353147
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2353148
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2353149
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2353150
    if-eqz v2, :cond_3

    .line 2353151
    const-string v3, "application_request_blocked_applications"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2353152
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2353153
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2353154
    if-eqz v3, :cond_2

    .line 2353155
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2353156
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2353157
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result p0

    if-ge v4, p0, :cond_1

    .line 2353158
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result p0

    .line 2353159
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2353160
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2353161
    if-eqz v0, :cond_0

    .line 2353162
    const-string v2, "node"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2353163
    invoke-static {v1, v0, p1, p2}, LX/GSS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2353164
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2353165
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2353166
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2353167
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2353168
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2353169
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2353145
    check-cast p1, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel$Serializer;->a(Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
