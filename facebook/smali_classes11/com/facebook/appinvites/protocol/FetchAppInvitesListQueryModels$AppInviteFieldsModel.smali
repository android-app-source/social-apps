.class public final Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x105968f4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:J

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2353919
    const-class v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2353920
    const-class v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2353921
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2353922
    return-void
.end method

.method private l()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353913
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->f:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->f:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    .line 2353914
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->f:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353923
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->i:Ljava/lang/String;

    .line 2353924
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353925
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2353926
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private o()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353927
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->l:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->l:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;

    .line 2353928
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->l:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353929
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->m:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->m:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2353930
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->m:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353931
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2353932
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 13

    .prologue
    .line 2353933
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2353934
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 2353935
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->l()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2353936
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2353937
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2353938
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2353939
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->mv_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 2353940
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->o()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 2353941
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->p()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 2353942
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->q()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 2353943
    const/16 v2, 0xa

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2353944
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2353945
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2353946
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->g:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2353947
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2353948
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2353949
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2353950
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2353951
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 2353952
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 2353953
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 2353954
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2353955
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2353956
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2353957
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->l()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2353958
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->l()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    .line 2353959
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->l()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2353960
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    .line 2353961
    iput-object v0, v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->f:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    .line 2353962
    :cond_0
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2353963
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2353964
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2353965
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    .line 2353966
    iput-object v0, v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2353967
    :cond_1
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->o()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2353968
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->o()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;

    .line 2353969
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->o()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2353970
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    .line 2353971
    iput-object v0, v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->l:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;

    .line 2353972
    :cond_2
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->p()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2353973
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->p()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2353974
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->p()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2353975
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    .line 2353976
    iput-object v0, v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->m:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2353977
    :cond_3
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->q()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2353978
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->q()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2353979
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->q()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 2353980
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    .line 2353981
    iput-object v0, v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2353982
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2353983
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353915
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2353916
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2353917
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->g:J

    .line 2353918
    return-void
.end method

.method public final b()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2353896
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->e:Ljava/util/List;

    .line 2353897
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2353898
    new-instance v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    invoke-direct {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;-><init>()V

    .line 2353899
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2353900
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353901
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->l()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 2353902
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2353903
    iget-wide v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->g:J

    return-wide v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2353904
    const v0, -0x10af87d6

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353905
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->h:Ljava/lang/String;

    .line 2353906
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2353907
    const v0, -0x2e9a58c1

    return v0
.end method

.method public final synthetic j()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353908
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->o()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353909
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->p()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final mv_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353910
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->k:Ljava/lang/String;

    .line 2353911
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic mw_()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353912
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method
