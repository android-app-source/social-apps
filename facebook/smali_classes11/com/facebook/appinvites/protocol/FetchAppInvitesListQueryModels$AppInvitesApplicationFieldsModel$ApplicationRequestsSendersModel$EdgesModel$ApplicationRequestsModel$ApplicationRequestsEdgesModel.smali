.class public final Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel$ApplicationRequestsEdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x43f7b3f7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel$ApplicationRequestsEdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel$ApplicationRequestsEdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2354017
    const-class v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel$ApplicationRequestsEdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2354018
    const-class v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel$ApplicationRequestsEdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2354019
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2354020
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2354021
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2354022
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel$ApplicationRequestsEdgesModel;->a()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2354023
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2354024
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2354025
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2354026
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2354027
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2354028
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel$ApplicationRequestsEdgesModel;->a()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2354029
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel$ApplicationRequestsEdgesModel;->a()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    .line 2354030
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel$ApplicationRequestsEdgesModel;->a()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2354031
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel$ApplicationRequestsEdgesModel;

    .line 2354032
    iput-object v0, v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel$ApplicationRequestsEdgesModel;->e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    .line 2354033
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2354034
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2354035
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel$ApplicationRequestsEdgesModel;->e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel$ApplicationRequestsEdgesModel;->e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    .line 2354036
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel$ApplicationRequestsEdgesModel;->e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2354037
    new-instance v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel$ApplicationRequestsEdgesModel;

    invoke-direct {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel$ApplicationRequestsEdgesModel;-><init>()V

    .line 2354038
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2354039
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2354040
    const v0, -0x6eb96b53

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2354041
    const v0, 0x6d35b10

    return v0
.end method
