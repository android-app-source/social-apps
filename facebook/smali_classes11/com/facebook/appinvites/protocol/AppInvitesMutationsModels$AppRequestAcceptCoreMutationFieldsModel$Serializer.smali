.class public final Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestAcceptCoreMutationFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestAcceptCoreMutationFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2352083
    const-class v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestAcceptCoreMutationFieldsModel;

    new-instance v1, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestAcceptCoreMutationFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestAcceptCoreMutationFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2352084
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2352085
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestAcceptCoreMutationFieldsModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2352086
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2352087
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 2352088
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2352089
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result p0

    .line 2352090
    if-eqz p0, :cond_0

    .line 2352091
    const-string p0, "accepted_request_ids"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2352092
    invoke-virtual {v1, v0, p2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2352093
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2352094
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2352095
    check-cast p1, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestAcceptCoreMutationFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestAcceptCoreMutationFieldsModel$Serializer;->a(Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppRequestAcceptCoreMutationFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
