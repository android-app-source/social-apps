.class public final Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3cf4f491
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$AndroidAppConfigModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$OverallStarRatingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2353783
    const-class v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2353811
    const-class v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2353809
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2353810
    return-void
.end method

.method private k()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$AndroidAppConfigModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353807
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$AndroidAppConfigModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$AndroidAppConfigModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$AndroidAppConfigModel;

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$AndroidAppConfigModel;

    .line 2353808
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$AndroidAppConfigModel;

    return-object v0
.end method

.method private l()Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353805
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->f:Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->f:Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;

    .line 2353806
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->f:Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;

    return-object v0
.end method

.method private m()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$OverallStarRatingModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353803
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->j:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$OverallStarRatingModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$OverallStarRatingModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$OverallStarRatingModel;

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->j:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$OverallStarRatingModel;

    .line 2353804
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->j:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$OverallStarRatingModel;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353801
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2353802
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 2353784
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2353785
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->k()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$AndroidAppConfigModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2353786
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->l()Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2353787
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2353788
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->mx_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2353789
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->m()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$OverallStarRatingModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2353790
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2353791
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2353792
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 2353793
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2353794
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2353795
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2353796
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2353797
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2353798
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2353799
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2353800
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2353759
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2353760
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->k()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$AndroidAppConfigModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2353761
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->k()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$AndroidAppConfigModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$AndroidAppConfigModel;

    .line 2353762
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->k()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$AndroidAppConfigModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2353763
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    .line 2353764
    iput-object v0, v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->e:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$AndroidAppConfigModel;

    .line 2353765
    :cond_0
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->l()Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2353766
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->l()Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;

    .line 2353767
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->l()Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2353768
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    .line 2353769
    iput-object v0, v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->f:Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;

    .line 2353770
    :cond_1
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->m()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$OverallStarRatingModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2353771
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->m()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$OverallStarRatingModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$OverallStarRatingModel;

    .line 2353772
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->m()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$OverallStarRatingModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2353773
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    .line 2353774
    iput-object v0, v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->j:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$OverallStarRatingModel;

    .line 2353775
    :cond_2
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2353776
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2353777
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2353778
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    .line 2353779
    iput-object v0, v1, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2353780
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2353781
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353782
    invoke-virtual {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2353812
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2353813
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->h:Z

    .line 2353814
    return-void
.end method

.method public final synthetic b()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$AndroidAppConfigModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353751
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->k()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$AndroidAppConfigModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2353748
    new-instance v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    invoke-direct {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;-><init>()V

    .line 2353749
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2353750
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353747
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->l()Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353745
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->g:Ljava/lang/String;

    .line 2353746
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2353744
    const v0, -0x450843d

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 2353752
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2353753
    iget-boolean v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->h:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2353754
    const v0, -0x3ff252d0

    return v0
.end method

.method public final synthetic j()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353755
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final mx_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353756
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->i:Ljava/lang/String;

    .line 2353757
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic my_()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$OverallStarRatingModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2353758
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->m()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$OverallStarRatingModel;

    move-result-object v0

    return-object v0
.end method
