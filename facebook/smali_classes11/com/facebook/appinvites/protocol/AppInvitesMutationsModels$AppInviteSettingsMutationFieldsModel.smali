.class public final Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x67050840
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2352056
    const-class v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2352055
    const-class v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2352053
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2352054
    return-void
.end method

.method private a()Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2352051
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel;->e:Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;

    iput-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel;->e:Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;

    .line 2352052
    iget-object v0, p0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel;->e:Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2352032
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2352033
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel;->a()Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2352034
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2352035
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2352036
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2352037
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2352043
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2352044
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel;->a()Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2352045
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel;->a()Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;

    .line 2352046
    invoke-direct {p0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel;->a()Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2352047
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel;

    .line 2352048
    iput-object v0, v1, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel;->e:Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel$UserApplicationInviteSettingsModel;

    .line 2352049
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2352050
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2352040
    new-instance v0, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/appinvites/protocol/AppInvitesMutationsModels$AppInviteSettingsMutationFieldsModel;-><init>()V

    .line 2352041
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2352042
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2352039
    const v0, -0x2a790db8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2352038
    const v0, 0x6f160bc8

    return v0
.end method
