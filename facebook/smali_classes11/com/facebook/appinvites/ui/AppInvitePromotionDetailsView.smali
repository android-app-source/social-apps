.class public Lcom/facebook/appinvites/ui/AppInvitePromotionDetailsView;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2355035
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 2355036
    const-string v0, "This will be applied automatically."

    iput-object v0, p0, Lcom/facebook/appinvites/ui/AppInvitePromotionDetailsView;->c:Ljava/lang/String;

    .line 2355037
    invoke-direct {p0}, Lcom/facebook/appinvites/ui/AppInvitePromotionDetailsView;->a()V

    .line 2355038
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2355043
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2355044
    const-string v0, "This will be applied automatically."

    iput-object v0, p0, Lcom/facebook/appinvites/ui/AppInvitePromotionDetailsView;->c:Ljava/lang/String;

    .line 2355045
    invoke-direct {p0}, Lcom/facebook/appinvites/ui/AppInvitePromotionDetailsView;->a()V

    .line 2355046
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2355047
    const v0, 0x7f0300f5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2355048
    const v0, 0x7f0d057b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/appinvites/ui/AppInvitePromotionDetailsView;->a:Landroid/widget/TextView;

    .line 2355049
    const v0, 0x7f0d057c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/appinvites/ui/AppInvitePromotionDetailsView;->b:Landroid/widget/TextView;

    .line 2355050
    return-void
.end method


# virtual methods
.method public setPromotionCodeLine(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2355041
    iget-object v0, p0, Lcom/facebook/appinvites/ui/AppInvitePromotionDetailsView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2355042
    return-void
.end method

.method public setPromotionText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2355039
    iget-object v0, p0, Lcom/facebook/appinvites/ui/AppInvitePromotionDetailsView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2355040
    return-void
.end method
