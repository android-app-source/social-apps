.class public Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field private a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2354876
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 2354877
    invoke-direct {p0}, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->a()V

    .line 2354878
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2354873
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2354874
    invoke-direct {p0}, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->a()V

    .line 2354875
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2354866
    const v0, 0x7f0300f3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2354867
    const v0, 0x7f0d0570

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->c:Landroid/widget/TextView;

    .line 2354868
    const v0, 0x7f0d056f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2354869
    const v0, 0x7f0d0571

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->b:Landroid/view/View;

    .line 2354870
    const v0, 0x7f0d0572

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->d:Landroid/widget/TextView;

    .line 2354871
    const v0, 0x7f0d0573

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->e:Landroid/widget/Button;

    .line 2354872
    return-void
.end method


# virtual methods
.method public setAppName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2354864
    iget-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2354865
    return-void
.end method

.method public setAppPictureURI(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 2354853
    iget-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2354854
    return-void
.end method

.method public setAppRating(F)V
    .locals 3

    .prologue
    .line 2354859
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    .line 2354860
    iget-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2354861
    :goto_0
    iget-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->d:Landroid/widget/TextView;

    const-string v1, "%.1f"

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2354862
    return-void

    .line 2354863
    :cond_0
    iget-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setInstallLabel(I)V
    .locals 1

    .prologue
    .line 2354857
    iget-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->e:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 2354858
    return-void
.end method

.method public setInstallOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2354855
    iget-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->e:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2354856
    return-void
.end method
