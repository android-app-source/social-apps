.class public Lcom/facebook/appinvites/ui/AppInviteContentView;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/2eZ;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/11S;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field private f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private g:Landroid/widget/TextView;

.field public h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public i:Lcom/facebook/appinvites/ui/AppInvitePromotionDetailsView;

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2355021
    const-class v0, Lcom/facebook/appinvites/ui/AppInviteContentView;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/appinvites/ui/AppInviteContentView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2355015
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 2355016
    invoke-direct {p0}, Lcom/facebook/appinvites/ui/AppInviteContentView;->b()V

    .line 2355017
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2355018
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2355019
    invoke-direct {p0}, Lcom/facebook/appinvites/ui/AppInviteContentView;->b()V

    .line 2355020
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/appinvites/ui/AppInviteContentView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/appinvites/ui/AppInviteContentView;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v0

    check-cast v0, LX/11S;

    iput-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->a:LX/11S;

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2355022
    const-class v0, Lcom/facebook/appinvites/ui/AppInviteContentView;

    invoke-static {v0, p0}, Lcom/facebook/appinvites/ui/AppInviteContentView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2355023
    const v0, 0x7f0300f4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2355024
    const v0, 0x7f0d0576

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->c:Landroid/widget/TextView;

    .line 2355025
    const v0, 0x7f0d0577

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->d:Landroid/widget/TextView;

    .line 2355026
    const v0, 0x7f0d0574

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->e:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2355027
    const v0, 0x7f0d0575

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2355028
    const v0, 0x7f0d0578

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->g:Landroid/widget/TextView;

    .line 2355029
    const v0, 0x7f0d0579

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2355030
    iget-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/GSy;

    invoke-direct {v1, p0}, LX/GSy;-><init>(Lcom/facebook/appinvites/ui/AppInviteContentView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2355031
    const v0, 0x7f0d057a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/ui/AppInvitePromotionDetailsView;

    iput-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->i:Lcom/facebook/appinvites/ui/AppInvitePromotionDetailsView;

    .line 2355032
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2355034
    iget-boolean v0, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->j:Z

    return v0
.end method

.method public getPromotionDetailsView()Lcom/facebook/appinvites/ui/AppInvitePromotionDetailsView;
    .locals 1

    .prologue
    .line 2355033
    iget-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->i:Lcom/facebook/appinvites/ui/AppInvitePromotionDetailsView;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x39f2837b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2355007
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onAttachedToWindow()V

    .line 2355008
    const/4 v1, 0x1

    .line 2355009
    iput-boolean v1, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->j:Z

    .line 2355010
    const/16 v1, 0x2d

    const v2, -0xf927727

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x7c6ea45f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2355011
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onDetachedFromWindow()V

    .line 2355012
    const/4 v1, 0x0

    .line 2355013
    iput-boolean v1, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->j:Z

    .line 2355014
    const/16 v1, 0x2d

    const v2, -0x2661d6cd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setHasBeenAttached(Z)V
    .locals 0

    .prologue
    .line 2355005
    iput-boolean p1, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->j:Z

    .line 2355006
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2355003
    iget-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2355004
    return-void
.end method

.method public setMessageMaxLines(I)V
    .locals 1

    .prologue
    .line 2355001
    iget-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 2355002
    return-void
.end method

.method public setMessageOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2354999
    iget-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2355000
    return-void
.end method

.method public setPreviewImageOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2354986
    iget-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2354987
    return-void
.end method

.method public setPreviewImageUri(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 2354997
    iget-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/appinvites/ui/AppInviteContentView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2354998
    return-void
.end method

.method public setSenderImageOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2354995
    iget-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2354996
    return-void
.end method

.method public setSenderImageURI(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 2354993
    iget-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/appinvites/ui/AppInviteContentView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2354994
    return-void
.end method

.method public setSenderName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2354991
    iget-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2354992
    return-void
.end method

.method public setTimestamp(J)V
    .locals 5

    .prologue
    .line 2354988
    iget-object v0, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->a:LX/11S;

    sget-object v1, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    invoke-interface {v0, v1, v2, v3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    .line 2354989
    iget-object v1, p0, Lcom/facebook/appinvites/ui/AppInviteContentView;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2354990
    return-void
.end method
