.class public final Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x74b87432
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel$ActiveTeamWithBallModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:Z

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2381525
    const-class v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2381524
    const-class v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2381526
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2381527
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2381534
    iput-object p1, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->k:Ljava/lang/String;

    .line 2381535
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2381536
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2381537
    if-eqz v0, :cond_0

    .line 2381538
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 2381539
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel$ActiveTeamWithBallModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2381528
    iget-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->e:Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel$ActiveTeamWithBallModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel$ActiveTeamWithBallModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel$ActiveTeamWithBallModel;

    iput-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->e:Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel$ActiveTeamWithBallModel;

    .line 2381529
    iget-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->e:Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel$ActiveTeamWithBallModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2381530
    iget-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->h:Ljava/lang/String;

    .line 2381531
    iget-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2381532
    iget-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->j:Ljava/lang/String;

    .line 2381533
    iget-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2381506
    iget-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->k:Ljava/lang/String;

    .line 2381507
    iget-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->k:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2381508
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2381509
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->j()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel$ActiveTeamWithBallModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2381510
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2381511
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2381512
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2381513
    const/16 v4, 0x8

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2381514
    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2381515
    const/4 v0, 0x1

    iget v4, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->f:I

    invoke-virtual {p1, v0, v4, v5}, LX/186;->a(III)V

    .line 2381516
    const/4 v0, 0x2

    iget-boolean v4, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->g:Z

    invoke-virtual {p1, v0, v4}, LX/186;->a(IZ)V

    .line 2381517
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2381518
    const/4 v0, 0x4

    iget v1, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->i:I

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 2381519
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2381520
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2381521
    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->l:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2381522
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2381523
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2381498
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2381499
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->j()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel$ActiveTeamWithBallModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2381500
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->j()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel$ActiveTeamWithBallModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel$ActiveTeamWithBallModel;

    .line 2381501
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->j()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel$ActiveTeamWithBallModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2381502
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;

    .line 2381503
    iput-object v0, v1, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->e:Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel$ActiveTeamWithBallModel;

    .line 2381504
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2381505
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2381497
    new-instance v0, LX/Ggq;

    invoke-direct {v0, p1}, LX/Ggq;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2381496
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2381490
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2381491
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->f:I

    .line 2381492
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->g:Z

    .line 2381493
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->i:I

    .line 2381494
    const/4 v0, 0x7

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->l:J

    .line 2381495
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2381484
    const-string v0, "status_text"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2381485
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2381486
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2381487
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    .line 2381488
    :goto_0
    return-void

    .line 2381489
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2381481
    const-string v0, "status_text"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2381482
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;->a(Ljava/lang/String;)V

    .line 2381483
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2381478
    new-instance v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;

    invoke-direct {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderLiveUpdatingFragmentModel$SportsMatchDataModel;-><init>()V

    .line 2381479
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2381480
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2381476
    const v0, 0x204a144

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2381477
    const v0, 0x31509926

    return v0
.end method
