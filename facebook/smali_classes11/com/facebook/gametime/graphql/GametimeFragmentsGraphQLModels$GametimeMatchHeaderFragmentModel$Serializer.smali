.class public final Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2381012
    const-class v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel;

    new-instance v1, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2381013
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2381014
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2381015
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2381016
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2381017
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2381018
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2381019
    if-eqz v2, :cond_0

    .line 2381020
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2381021
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2381022
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2381023
    if-eqz v2, :cond_1

    .line 2381024
    const-string p0, "sports_match_data"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2381025
    invoke-static {v1, v2, p1, p2}, LX/Gh3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2381026
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2381027
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2381028
    check-cast p1, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel$Serializer;->a(Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
