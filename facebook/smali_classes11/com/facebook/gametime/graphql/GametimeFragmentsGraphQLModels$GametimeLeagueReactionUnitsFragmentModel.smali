.class public final Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9qT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7af4d8a8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2380980
    const-class v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2380979
    const-class v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2380977
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2380978
    return-void
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNextUpdateCondition"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2380975
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2380976
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2380973
    iget-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    iput-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2380974
    iget-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2380956
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2380957
    invoke-virtual {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->c()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2380958
    invoke-virtual {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 2380959
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0xbbbd4b6

    invoke-static {v3, v2, v4}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2380960
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2380961
    invoke-virtual {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2380962
    invoke-virtual {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->j()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    const v7, -0x6970f043

    invoke-static {v6, v5, v7}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2380963
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2380964
    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 2380965
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2380966
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2380967
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2380968
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2380969
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2380970
    const/4 v0, 0x6

    iget v1, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->k:I

    invoke-virtual {p1, v0, v1, v8}, LX/186;->a(III)V

    .line 2380971
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2380972
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2380954
    iget-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->f:Ljava/util/List;

    .line 2380955
    iget-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2380928
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2380929
    invoke-virtual {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2380930
    invoke-virtual {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2380931
    if-eqz v1, :cond_4

    .line 2380932
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;

    .line 2380933
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 2380934
    :goto_0
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2380935
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0xbbbd4b6

    invoke-static {v2, v0, v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2380936
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2380937
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;

    .line 2380938
    iput v3, v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->g:I

    move-object v1, v0

    .line 2380939
    :cond_0
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2380940
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2380941
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2380942
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;

    .line 2380943
    iput-object v0, v1, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2380944
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2380945
    invoke-virtual {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x6970f043

    invoke-static {v2, v0, v3}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2380946
    invoke-virtual {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2380947
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;

    .line 2380948
    iput v3, v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->j:I

    move-object v1, v0

    .line 2380949
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2380950
    if-nez v1, :cond_3

    :goto_1
    return-object p0

    .line 2380951
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2380952
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_3
    move-object p0, v1

    .line 2380953
    goto :goto_1

    :cond_4
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2380908
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2380909
    const/4 v0, 0x2

    const v1, -0xbbbd4b6

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->g:I

    .line 2380910
    const/4 v0, 0x5

    const v1, -0x6970f043

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->j:I

    .line 2380911
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->k:I

    .line 2380912
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2380925
    new-instance v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;

    invoke-direct {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;-><init>()V

    .line 2380926
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2380927
    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2380922
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2380923
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2380924
    :cond_0
    iget-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final synthetic d()LX/0us;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2380921
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2380920
    const v0, -0x15e25e05

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2380918
    iget-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->i:Ljava/lang/String;

    .line 2380919
    iget-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2380917
    const v0, 0x4d67f176    # 2.4321008E8f

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2380915
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2380916
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->j:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 2380913
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2380914
    iget v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->k:I

    return v0
.end method
