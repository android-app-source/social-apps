.class public final Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderUpdateSubscriptionModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderUpdateSubscriptionModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2381825
    const-class v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderUpdateSubscriptionModel;

    new-instance v1, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderUpdateSubscriptionModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderUpdateSubscriptionModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2381826
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2381824
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderUpdateSubscriptionModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2381815
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2381816
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2381817
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2381818
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2381819
    if-eqz v2, :cond_0

    .line 2381820
    const-string p0, "page"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2381821
    invoke-static {v1, v2, p1, p2}, LX/Gh6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2381822
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2381823
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2381814
    check-cast p1, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderUpdateSubscriptionModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderUpdateSubscriptionModel$Serializer;->a(Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderUpdateSubscriptionModel;LX/0nX;LX/0my;)V

    return-void
.end method
