.class public final Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4222f51e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2381938
    const-class v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2381956
    const-class v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2381954
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2381955
    return-void
.end method

.method private a()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2381952
    iget-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel;->e:Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel$PageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel$PageModel;

    iput-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel;->e:Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel$PageModel;

    .line 2381953
    iget-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel;->e:Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel$PageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2381957
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2381958
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel;->a()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel$PageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2381959
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2381960
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2381961
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2381962
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2381944
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2381945
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel;->a()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2381946
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel;->a()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel$PageModel;

    .line 2381947
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel;->a()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2381948
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel;

    .line 2381949
    iput-object v0, v1, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel;->e:Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel$PageModel;

    .line 2381950
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2381951
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2381941
    new-instance v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel;

    invoke-direct {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionSeenStateAddUnitMutationModel;-><init>()V

    .line 2381942
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2381943
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2381940
    const v0, 0x47b883e4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2381939
    const v0, 0x68c443f8

    return v0
.end method
