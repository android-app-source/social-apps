.class public final Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel$SportsMatchDataModel$SecondTeamObjectModel$OfficialPageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel$SportsMatchDataModel$SecondTeamObjectModel$OfficialPageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel$SportsMatchDataModel$SecondTeamObjectModel$OfficialPageModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2381203
    const-class v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel$SportsMatchDataModel$SecondTeamObjectModel$OfficialPageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2381202
    const-class v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel$SportsMatchDataModel$SecondTeamObjectModel$OfficialPageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2381200
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2381201
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2381198
    iget-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel$SportsMatchDataModel$SecondTeamObjectModel$OfficialPageModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel$SportsMatchDataModel$SecondTeamObjectModel$OfficialPageModel;->e:Ljava/lang/String;

    .line 2381199
    iget-object v0, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel$SportsMatchDataModel$SecondTeamObjectModel$OfficialPageModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2381192
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2381193
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel$SportsMatchDataModel$SecondTeamObjectModel$OfficialPageModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2381194
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2381195
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2381196
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2381197
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2381189
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2381190
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2381191
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2381204
    new-instance v0, LX/Ggn;

    invoke-direct {v0, p1}, LX/Ggn;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2381188
    invoke-direct {p0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel$SportsMatchDataModel$SecondTeamObjectModel$OfficialPageModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2381186
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2381187
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2381185
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2381182
    new-instance v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel$SportsMatchDataModel$SecondTeamObjectModel$OfficialPageModel;

    invoke-direct {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel$SportsMatchDataModel$SecondTeamObjectModel$OfficialPageModel;-><init>()V

    .line 2381183
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2381184
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2381181
    const v0, 0x1884f71b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2381180
    const v0, 0x25d6af

    return v0
.end method
