.class public final Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2380499
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2380500
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2380410
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2380411
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 8

    .prologue
    .line 2380412
    if-nez p1, :cond_0

    .line 2380413
    const/4 v0, 0x0

    .line 2380414
    :goto_0
    return v0

    .line 2380415
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2380416
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2380417
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2380418
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2380419
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v1

    .line 2380420
    const v2, 0x4d0e5174    # 1.49231424E8f

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2380421
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2380422
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2380423
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2380424
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2380425
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2380426
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2380427
    const/4 v5, 0x5

    invoke-virtual {p0, p1, v5}, LX/15i;->b(II)Z

    move-result v5

    .line 2380428
    const/4 v6, 0x6

    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 2380429
    invoke-virtual {p3, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2380430
    const/4 v7, 0x7

    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2380431
    const/4 v7, 0x0

    invoke-virtual {p3, v7, v0}, LX/186;->b(II)V

    .line 2380432
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2380433
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 2380434
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 2380435
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v4}, LX/186;->b(II)V

    .line 2380436
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v5}, LX/186;->a(IZ)V

    .line 2380437
    const/4 v0, 0x6

    invoke-virtual {p3, v0, v6}, LX/186;->b(II)V

    .line 2380438
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2380439
    :sswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2380440
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2380441
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2380442
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2380443
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2380444
    :sswitch_2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2380445
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2380446
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2380447
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2380448
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2380449
    :sswitch_3
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2380450
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2380451
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v1

    .line 2380452
    const v2, -0x5f89d579

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2380453
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2380454
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2380455
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2380456
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2380457
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2380458
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2380459
    const/4 v5, 0x5

    invoke-virtual {p0, p1, v5}, LX/15i;->b(II)Z

    move-result v5

    .line 2380460
    const/4 v6, 0x6

    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 2380461
    invoke-virtual {p3, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2380462
    const/4 v7, 0x7

    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2380463
    const/4 v7, 0x0

    invoke-virtual {p3, v7, v0}, LX/186;->b(II)V

    .line 2380464
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2380465
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 2380466
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 2380467
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v4}, LX/186;->b(II)V

    .line 2380468
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v5}, LX/186;->a(IZ)V

    .line 2380469
    const/4 v0, 0x6

    invoke-virtual {p3, v0, v6}, LX/186;->b(II)V

    .line 2380470
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2380471
    :sswitch_4
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2380472
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2380473
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2380474
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2380475
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6970f043 -> :sswitch_2
        -0x5f89d579 -> :sswitch_4
        -0x26f872c8 -> :sswitch_3
        0x2f87d596 -> :sswitch_0
        0x4d0e5174 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2380476
    if-nez p0, :cond_0

    move v0, v1

    .line 2380477
    :goto_0
    return v0

    .line 2380478
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 2380479
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2380480
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2380481
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 2380482
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2380483
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2380484
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 2380369
    const/4 v7, 0x0

    .line 2380370
    const/4 v1, 0x0

    .line 2380371
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 2380372
    invoke-static {v2, v3, v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2380373
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 2380374
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2380375
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2380485
    new-instance v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2380486
    sparse-switch p2, :sswitch_data_0

    .line 2380487
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2380488
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2380489
    const v1, 0x4d0e5174    # 1.49231424E8f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2380490
    :goto_0
    :sswitch_1
    return-void

    .line 2380491
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2380492
    const v1, -0x5f89d579

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6970f043 -> :sswitch_1
        -0x5f89d579 -> :sswitch_1
        -0x26f872c8 -> :sswitch_2
        0x2f87d596 -> :sswitch_0
        0x4d0e5174 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2380493
    if-eqz p1, :cond_0

    .line 2380494
    invoke-static {p0, p1, p2}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 2380495
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;

    .line 2380496
    if-eq v0, v1, :cond_0

    .line 2380497
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2380498
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2380407
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2380408
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2380409
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2380402
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2380403
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2380404
    :cond_0
    iput-object p1, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2380405
    iput p2, p0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;->b:I

    .line 2380406
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2380401
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2380400
    new-instance v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2380397
    iget v0, p0, LX/1vt;->c:I

    .line 2380398
    move v0, v0

    .line 2380399
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2380394
    iget v0, p0, LX/1vt;->c:I

    .line 2380395
    move v0, v0

    .line 2380396
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2380391
    iget v0, p0, LX/1vt;->b:I

    .line 2380392
    move v0, v0

    .line 2380393
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2380388
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2380389
    move-object v0, v0

    .line 2380390
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2380379
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2380380
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2380381
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2380382
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2380383
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2380384
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2380385
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2380386
    invoke-static {v3, v9, v2}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2380387
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2380376
    iget v0, p0, LX/1vt;->c:I

    .line 2380377
    move v0, v0

    .line 2380378
    return v0
.end method
