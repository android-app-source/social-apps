.class public final Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionSeenStateAddUnitMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionSeenStateAddUnitMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2380726
    const-class v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionSeenStateAddUnitMutationModel;

    new-instance v1, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionSeenStateAddUnitMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionSeenStateAddUnitMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2380727
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2380728
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionSeenStateAddUnitMutationModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2380717
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2380718
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2380719
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2380720
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2380721
    if-eqz p0, :cond_0

    .line 2380722
    const-string p2, "league"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2380723
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2380724
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2380725
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2380716
    check-cast p1, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionSeenStateAddUnitMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionSeenStateAddUnitMutationModel$Serializer;->a(Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionSeenStateAddUnitMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
