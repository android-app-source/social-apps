.class public Lcom/facebook/gametime/ui/GametimeHeaderView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:Lcom/facebook/widget/text/BetterTextView;

.field private f:Lcom/facebook/widget/text/BetterTextView;

.field private g:Lcom/facebook/widget/text/BetterTextView;

.field private h:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2384048
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2384049
    invoke-direct {p0}, Lcom/facebook/gametime/ui/GametimeHeaderView;->a()V

    .line 2384050
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2384045
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2384046
    invoke-direct {p0}, Lcom/facebook/gametime/ui/GametimeHeaderView;->a()V

    .line 2384047
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2384042
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2384043
    invoke-direct {p0}, Lcom/facebook/gametime/ui/GametimeHeaderView;->a()V

    .line 2384044
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2384033
    const-class v0, Lcom/facebook/gametime/ui/GametimeHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/gametime/ui/GametimeHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2384034
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/gametime/ui/GametimeHeaderView;->setWillNotDraw(Z)V

    .line 2384035
    const v0, 0x7f03078c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2384036
    const v0, 0x7f0d1428

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeHeaderView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2384037
    const v0, 0x7f0d1429

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeHeaderView;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2384038
    const v0, 0x7f0d142c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeHeaderView;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2384039
    const v0, 0x7f0d142b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeHeaderView;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 2384040
    const v0, 0x7f0d142a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeHeaderView;->h:Lcom/facebook/widget/text/BetterTextView;

    .line 2384041
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/gametime/ui/GametimeHeaderView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/gametime/ui/GametimeHeaderView;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeHeaderView;->a:LX/03V;

    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2384004
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2384005
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 2384006
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2384007
    const v1, 0x3e8930a3

    invoke-virtual {p0}, Lcom/facebook/gametime/ui/GametimeHeaderView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 2384008
    invoke-virtual {p0}, Lcom/facebook/gametime/ui/GametimeHeaderView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    .line 2384009
    :try_start_0
    iget-object v3, p0, Lcom/facebook/gametime/ui/GametimeHeaderView;->b:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 2384010
    iget-object v3, p0, Lcom/facebook/gametime/ui/GametimeHeaderView;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 2384011
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 2384012
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2384013
    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/facebook/gametime/ui/GametimeHeaderView;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2384014
    sub-int v4, v2, v1

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/facebook/gametime/ui/GametimeHeaderView;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2384015
    add-int v4, v2, v1

    int-to-float v4, v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2384016
    invoke-virtual {v3}, Landroid/graphics/Path;->close()V

    .line 2384017
    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2384018
    :cond_0
    iget-object v3, p0, Lcom/facebook/gametime/ui/GametimeHeaderView;->c:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 2384019
    iget-object v3, p0, Lcom/facebook/gametime/ui/GametimeHeaderView;->c:Ljava/lang/String;

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 2384020
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 2384021
    invoke-virtual {p0}, Lcom/facebook/gametime/ui/GametimeHeaderView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2384022
    invoke-virtual {p0}, Lcom/facebook/gametime/ui/GametimeHeaderView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/facebook/gametime/ui/GametimeHeaderView;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2384023
    sub-int v4, v2, v1

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/facebook/gametime/ui/GametimeHeaderView;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2384024
    add-int/2addr v1, v2

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v3, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2384025
    invoke-virtual {v3}, Landroid/graphics/Path;->close()V

    .line 2384026
    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2384027
    :cond_1
    :goto_0
    return-void

    .line 2384028
    :catch_0
    move-exception v0

    .line 2384029
    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeHeaderView;->a:LX/03V;

    const-string v2, "GametimeHeaderView"

    const-string v3, "Could not parse color for header"

    invoke-static {v2, v3}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    .line 2384030
    iput-object v0, v2, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2384031
    move-object v0, v2

    .line 2384032
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    goto :goto_0
.end method

.method public setMatchData(LX/Ggh;)V
    .locals 2

    .prologue
    .line 2383994
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeHeaderView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-interface {p1}, LX/Ggh;->b()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel$SportsMatchDataModel$FirstTeamObjectModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel$SportsMatchDataModel$FirstTeamObjectModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2383995
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeHeaderView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-interface {p1}, LX/Ggh;->lT_()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel$SportsMatchDataModel$SecondTeamObjectModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderFragmentModel$SportsMatchDataModel$SecondTeamObjectModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2383996
    invoke-interface {p1}, LX/Ggh;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2383997
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeHeaderView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-interface {p1}, LX/Ggh;->d()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2383998
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeHeaderView;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-interface {p1}, LX/Ggh;->j()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2383999
    :cond_0
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeHeaderView;->h:Lcom/facebook/widget/text/BetterTextView;

    invoke-interface {p1}, LX/Ggh;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2384000
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, LX/Ggh;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeHeaderView;->b:Ljava/lang/String;

    .line 2384001
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, LX/Ggh;->lS_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeHeaderView;->c:Ljava/lang/String;

    .line 2384002
    invoke-virtual {p0}, Lcom/facebook/gametime/ui/GametimeHeaderView;->invalidate()V

    .line 2384003
    return-void
.end method
