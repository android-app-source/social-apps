.class public Lcom/facebook/gametime/ui/GametimeLeagueActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/13v;
.implements LX/0f2;
.implements LX/0f1;
.implements LX/GhQ;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field public p:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/GiS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/GiT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:Ljava/lang/String;

.field public u:LX/GhU;

.field public v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/GhT;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field private x:Landroid/support/v4/view/ViewPager;

.field private y:Lcom/facebook/ui/titlebar/Fb4aTitleBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2384169
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/gametime/ui/GametimeLeagueActivity;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 2384158
    new-instance v0, LX/4FE;

    invoke-direct {v0}, LX/4FE;-><init>()V

    .line 2384159
    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->t:Ljava/lang/String;

    .line 2384160
    const-string v2, "league"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2384161
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2384162
    const-string v2, "latest_timestamp"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2384163
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 2384164
    const-string v2, "surface"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2384165
    new-instance v1, LX/2rQ;

    invoke-direct {v1}, LX/2rQ;-><init>()V

    .line 2384166
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2384167
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->r:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2384168
    return-void
.end method

.method private static a(Lcom/facebook/gametime/ui/GametimeLeagueActivity;LX/0Zb;LX/GiS;LX/0tX;LX/GiT;)V
    .locals 0

    .prologue
    .line 2384157
    iput-object p1, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->p:LX/0Zb;

    iput-object p2, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->q:LX/GiS;

    iput-object p3, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->r:LX/0tX;

    iput-object p4, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->s:LX/GiT;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;

    invoke-static {v3}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {v3}, LX/GiS;->b(LX/0QB;)LX/GiS;

    move-result-object v1

    check-cast v1, LX/GiS;

    invoke-static {v3}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {v3}, LX/GiT;->b(LX/0QB;)LX/GiT;

    move-result-object v3

    check-cast v3, LX/GiT;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->a(Lcom/facebook/gametime/ui/GametimeLeagueActivity;LX/0Zb;LX/GiS;LX/0tX;LX/GiT;)V

    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 2384153
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2384154
    const v0, 0x7f0d1421

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->y:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2384155
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->y:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/GhN;

    invoke-direct {v1, p0}, LX/GhN;-><init>(Lcom/facebook/gametime/ui/GametimeLeagueActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 2384156
    return-void
.end method

.method public static m(Lcom/facebook/gametime/ui/GametimeLeagueActivity;)V
    .locals 5

    .prologue
    .line 2384088
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->v:Ljava/util/List;

    .line 2384089
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->v:Ljava/util/List;

    new-instance v1, LX/GhT;

    sget-object v2, LX/GiR;->TAIL_LOAD:LX/GiR;

    const-string v3, "ANDROID_GAMETIME_LEAGUE_SCHEDULE"

    const-string v4, "Scores"

    invoke-direct {v1, v2, v3, v4}, LX/GhT;-><init>(LX/GiR;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2384090
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->v:Ljava/util/List;

    new-instance v1, LX/GhT;

    sget-object v2, LX/GiR;->TAIL_LOAD:LX/GiR;

    const-string v3, "ANDROID_GAMETIME_LEAGUE_FRIEND_STORIES"

    const v4, 0x7f0835cc

    invoke-virtual {p0, v4}, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/GhT;-><init>(LX/GiR;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2384091
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->v:Ljava/util/List;

    new-instance v1, LX/GhT;

    sget-object v2, LX/GiR;->TAIL_LOAD:LX/GiR;

    const-string v3, "ANDROID_GAMETIME_LEAGUE_EXPERT_STORIES"

    const v4, 0x7f0835cb

    invoke-virtual {p0, v4}, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/GhT;-><init>(LX/GiR;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2384092
    return-void
.end method

.method public static n(Lcom/facebook/gametime/ui/GametimeLeagueActivity;)V
    .locals 5

    .prologue
    .line 2384144
    new-instance v0, LX/GhU;

    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->t:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->v:Ljava/util/List;

    sget-object v3, LX/GhR;->LEAGUE:LX/GhR;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, LX/GhU;-><init>(Ljava/lang/String;Ljava/util/List;LX/GhR;LX/0gc;)V

    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->u:LX/GhU;

    .line 2384145
    const v0, 0x7f0d1423

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->x:Landroid/support/v4/view/ViewPager;

    .line 2384146
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->x:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->u:LX/GhU;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2384147
    const v0, 0x7f0d1422

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->w:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2384148
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->w:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->x:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2384149
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->w:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    new-instance v1, LX/GhO;

    invoke-direct {v1, p0, p0}, LX/GhO;-><init>(Lcom/facebook/gametime/ui/GametimeLeagueActivity;Lcom/facebook/gametime/ui/GametimeLeagueActivity;)V

    .line 2384150
    iput-object v1, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 2384151
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->s:LX/GiT;

    invoke-virtual {v0, p0}, LX/GiT;->a(Lcom/facebook/base/activity/FbFragmentActivity;)V

    .line 2384152
    return-void
.end method

.method private o()V
    .locals 8

    .prologue
    .line 2384120
    new-instance v0, LX/GhP;

    invoke-direct {v0, p0}, LX/GhP;-><init>(Lcom/facebook/gametime/ui/GametimeLeagueActivity;)V

    .line 2384121
    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->q:LX/GiS;

    iget-object v2, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->t:Ljava/lang/String;

    .line 2384122
    new-instance v3, LX/GgW;

    invoke-direct {v3}, LX/GgW;-><init>()V

    move-object v3, v3

    .line 2384123
    const-string v4, "trigger_data"

    .line 2384124
    const-string v5, "NON_QUERYABLE_REACTION_SURFACE"

    .line 2384125
    new-instance v6, LX/4J3;

    invoke-direct {v6}, LX/4J3;-><init>()V

    const-string v7, "normal"

    invoke-virtual {v6, v7}, LX/4J3;->b(Ljava/lang/String;)LX/4J3;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/4J3;->a(Ljava/lang/String;)LX/4J3;

    move-result-object v6

    iget-object v7, v1, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2384126
    iget-object p0, v7, Lcom/facebook/reaction/ReactionUtil;->j:LX/0Px;

    move-object v7, p0

    .line 2384127
    invoke-virtual {v6, v7}, LX/4J3;->a(Ljava/util/List;)LX/4J3;

    move-result-object v6

    iget-object v7, v1, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2384128
    iget-object p0, v7, Lcom/facebook/reaction/ReactionUtil;->t:LX/0Px;

    move-object v7, p0

    .line 2384129
    invoke-virtual {v6, v7}, LX/4J3;->e(Ljava/util/List;)LX/4J3;

    move-result-object v6

    iget-object v7, v1, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2384130
    iget-object p0, v7, Lcom/facebook/reaction/ReactionUtil;->m:LX/1vj;

    move-object v7, p0

    .line 2384131
    invoke-virtual {v7}, LX/1vj;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/4J3;->d(Ljava/util/List;)LX/4J3;

    move-result-object v6

    iget-object v7, v1, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2384132
    iget-object p0, v7, Lcom/facebook/reaction/ReactionUtil;->r:LX/1s6;

    move-object v7, p0

    .line 2384133
    invoke-virtual {v7, v5}, LX/1s6;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/4J3;->b(Ljava/util/List;)LX/4J3;

    move-result-object v5

    iget-object v6, v1, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2384134
    iget-object v7, v6, Lcom/facebook/reaction/ReactionUtil;->w:LX/1vo;

    move-object v6, v7

    .line 2384135
    invoke-virtual {v6}, LX/1vo;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/4J3;->c(Ljava/util/List;)LX/4J3;

    move-result-object v5

    iget-object v6, v1, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    invoke-virtual {v6}, Lcom/facebook/reaction/ReactionUtil;->f()LX/0Px;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/4J3;->f(Ljava/util/List;)LX/4J3;

    move-result-object v5

    .line 2384136
    new-instance v6, LX/4FD;

    invoke-direct {v6}, LX/4FD;-><init>()V

    invoke-virtual {v6, v2}, LX/4FD;->a(Ljava/lang/String;)LX/4FD;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/4FD;->a(LX/4J3;)LX/4FD;

    move-result-object v5

    move-object v5, v5

    .line 2384137
    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2384138
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    const/4 v4, 0x1

    .line 2384139
    iput-boolean v4, v3, LX/0zO;->p:Z

    .line 2384140
    move-object v3, v3

    .line 2384141
    sget-object v4, LX/0zS;->d:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    .line 2384142
    iget-object v4, v1, LX/GiS;->f:LX/1Ck;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "gametime_league_surfaces-"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v1, LX/GiS;->b:LX/0tX;

    invoke-virtual {v6, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    invoke-virtual {v4, v5, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2384143
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2384116
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->x:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->v:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->x:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 2384117
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->x:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 2384118
    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->v:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GhT;

    iget-object v0, v0, LX/GhT;->b:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 2384119
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;)V
    .locals 3

    .prologue
    .line 2384107
    invoke-virtual {p1}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->k()I

    move-result v0

    .line 2384108
    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->u:LX/GhU;

    invoke-virtual {v1, p2}, LX/GhU;->b(Ljava/lang/Object;)I

    move-result v1

    .line 2384109
    iget-object v2, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->u:LX/GhU;

    invoke-virtual {v2, v0, v1}, LX/GhU;->a(II)V

    .line 2384110
    iget-object v2, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->x:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    .line 2384111
    if-ne v2, v1, :cond_0

    if-lez v0, :cond_0

    .line 2384112
    invoke-virtual {p1}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/GiS;->a(LX/0Px;)I

    move-result v0

    .line 2384113
    invoke-virtual {p2}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->a(Lcom/facebook/gametime/ui/GametimeLeagueActivity;ILjava/lang/String;)V

    .line 2384114
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->u:LX/GhU;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, LX/GhU;->a(II)V

    .line 2384115
    :cond_0
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2384104
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2384105
    const-string v1, "gametime_sport_id"

    iget-object v2, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->t:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2384106
    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2384096
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2384097
    invoke-static {p0, p0}, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2384098
    const v0, 0x7f03078a

    invoke-virtual {p0, v0}, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->setContentView(I)V

    .line 2384099
    invoke-virtual {p0}, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "league_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "-1"

    :goto_0
    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->t:Ljava/lang/String;

    .line 2384100
    invoke-direct {p0}, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->o()V

    .line 2384101
    invoke-direct {p0}, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->l()V

    .line 2384102
    return-void

    .line 2384103
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "league_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2384093
    if-eqz p1, :cond_0

    .line 2384094
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeLeagueActivity;->y:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v0, p1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 2384095
    :cond_0
    return-void
.end method
