.class public Lcom/facebook/gametime/ui/components/view/TwoColorBarView;
.super Landroid/view/View;
.source ""


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private b:I

.field private c:I

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private h:F

.field private i:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2385294
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2385295
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v1, 0x7f0a00d5

    invoke-static {p1, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2385296
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->a:Landroid/graphics/Paint;

    .line 2385297
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 2385298
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2385299
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v1, 0x7f0a00d5

    invoke-static {p1, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2385300
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->a:Landroid/graphics/Paint;

    .line 2385301
    return-void
.end method


# virtual methods
.method public final a(FF)V
    .locals 3

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 2385288
    cmpl-float v1, p1, v2

    if-nez v1, :cond_0

    cmpl-float v1, p2, v2

    if-nez v1, :cond_0

    move p2, v0

    move p1, v0

    .line 2385289
    :cond_0
    add-float v0, p1, p2

    div-float v0, p1, v0

    iput v0, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->e:F

    .line 2385290
    return-void
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 2385291
    iput p1, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->b:I

    .line 2385292
    iput p2, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->c:I

    .line 2385293
    return-void
.end method

.method public final a(ILandroid/content/Context;)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 2385286
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->d:F

    .line 2385287
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2385261
    iget-object v0, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->a:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->b:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2385262
    iget v1, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->f:F

    iget v3, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->g:F

    iget v4, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->d:F

    iget-object v5, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2385263
    iget-object v0, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->a:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2385264
    iget v1, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->h:F

    iget v3, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->i:F

    iget v4, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->d:F

    iget-object v5, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2385265
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 2385273
    sub-int v1, p4, p2

    .line 2385274
    invoke-virtual {p0}, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b161d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 2385275
    int-to-float v0, v1

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, v2

    sub-float/2addr v0, v3

    .line 2385276
    iget v3, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->e:F

    mul-float/2addr v0, v3

    .line 2385277
    iput v2, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->f:F

    .line 2385278
    iget v3, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->f:F

    add-float/2addr v0, v3

    iput v0, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->g:F

    .line 2385279
    iget v0, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->e:F

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->e:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 2385280
    :goto_0
    iget v3, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->g:F

    iput v3, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->h:F

    .line 2385281
    if-eqz v0, :cond_0

    .line 2385282
    iget v0, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->h:F

    invoke-virtual {p0}, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b22de

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    add-float/2addr v0, v3

    iput v0, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->h:F

    .line 2385283
    :cond_0
    int-to-float v0, v1

    sub-float/2addr v0, v2

    iput v0, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->i:F

    .line 2385284
    return-void

    .line 2385285
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    .line 2385266
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 2385267
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 2385268
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 2385269
    const/high16 v3, 0x40000000    # 2.0f

    if-eq v2, v3, :cond_0

    .line 2385270
    iget v0, p0, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->d:F

    invoke-virtual {p0}, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0082

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v0, v2

    float-to-int v0, v0

    .line 2385271
    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->setMeasuredDimension(II)V

    .line 2385272
    return-void
.end method
