.class public Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/9o7;",
        "LX/GhX;",
        "LX/1Pn;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final c:LX/1Ad;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/0hy;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2384402
    const-class v0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1Ad;Lcom/facebook/content/SecureContextHelper;LX/0hy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2384396
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2384397
    iput-object p1, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2384398
    iput-object p2, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;->c:LX/1Ad;

    .line 2384399
    iput-object p3, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2384400
    iput-object p4, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;->e:LX/0hy;

    .line 2384401
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;
    .locals 7

    .prologue
    .line 2384368
    const-class v1, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;

    monitor-enter v1

    .line 2384369
    :try_start_0
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2384370
    sput-object v2, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2384371
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2384372
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2384373
    new-instance p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v6

    check-cast v6, LX/0hy;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1Ad;Lcom/facebook/content/SecureContextHelper;LX/0hy;)V

    .line 2384374
    move-object v0, p0

    .line 2384375
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2384376
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2384377
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2384378
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2384384
    check-cast p2, LX/9o7;

    check-cast p3, LX/1Pn;

    .line 2384385
    const v0, 0x7f0d142f

    iget-object v1, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/GhW;

    invoke-direct {v2, p0, p2, p3}, LX/GhW;-><init>(Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;LX/9o7;LX/1Pn;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2384386
    new-instance v0, LX/GhX;

    iget-object v1, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;->c:LX/1Ad;

    invoke-interface {p2}, LX/9o7;->b()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->d()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v1

    sget-object v2, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    invoke-interface {p2}, LX/9o7;->l()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2}, LX/9o7;->gG_()Ljava/lang/String;

    move-result-object v3

    .line 2384387
    new-instance p0, Landroid/text/style/StyleSpan;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 2384388
    new-instance p1, Landroid/text/SpannableStringBuilder;

    invoke-direct {p1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2384389
    if-eqz v2, :cond_0

    .line 2384390
    invoke-virtual {p1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2384391
    const-string p2, " "

    invoke-virtual {p1, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2384392
    :cond_0
    if-eqz v3, :cond_1

    .line 2384393
    invoke-static {p1, v3, p0}, LX/E6H;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Landroid/text/style/CharacterStyle;)V

    .line 2384394
    :cond_1
    move-object v2, p1

    .line 2384395
    invoke-direct {v0, v1, v2}, LX/GhX;-><init>(LX/1aZ;Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x1816fd6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2384379
    check-cast p1, LX/9o7;

    check-cast p2, LX/GhX;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2384380
    iget-object v1, p2, LX/GhX;->a:LX/1aZ;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailController(LX/1aZ;)V

    .line 2384381
    iget-object v1, p2, LX/GhX;->b:Ljava/lang/CharSequence;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2384382
    invoke-interface {p1}, LX/9o7;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2384383
    const/16 v1, 0x1f

    const v2, 0x2f73538c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
