.class public Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadUnitComponentPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadTextPartDefinition;

.field private final b:Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadTextPartDefinition;Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2384492
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2384493
    iput-object p1, p0, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadUnitComponentPartDefinition;->a:Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadTextPartDefinition;

    .line 2384494
    iput-object p2, p0, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadUnitComponentPartDefinition;->b:Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;

    .line 2384495
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadUnitComponentPartDefinition;
    .locals 5

    .prologue
    .line 2384468
    const-class v1, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadUnitComponentPartDefinition;

    monitor-enter v1

    .line 2384469
    :try_start_0
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadUnitComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2384470
    sput-object v2, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadUnitComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2384471
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2384472
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2384473
    new-instance p0, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadTextPartDefinition;->a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadTextPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;->a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadUnitComponentPartDefinition;-><init>(Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadTextPartDefinition;Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;)V

    .line 2384474
    move-object v0, p0

    .line 2384475
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2384476
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2384477
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2384478
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2384479
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2384480
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v7, v0

    .line 2384481
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v7}, LX/9uc;->bq()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    .line 2384482
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v7}, LX/9uc;->cA()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v5

    .line 2384483
    iget-object v6, p0, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadUnitComponentPartDefinition;->a:Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadTextPartDefinition;

    new-instance v0, LX/Ghc;

    invoke-interface {v7}, LX/9uc;->dc()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7}, LX/9uc;->br()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v7}, LX/9uc;->cB()Ljava/lang/String;

    move-result-object v3

    invoke-direct/range {v0 .. v5}, LX/Ghc;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {p1, v6, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2384484
    iget-object v0, p0, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadUnitComponentPartDefinition;->b:Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;

    new-instance v3, LX/Ghd;

    invoke-interface {v7}, LX/9uc;->bs()D

    move-result-wide v8

    double-to-float v6, v8

    invoke-interface {v7}, LX/9uc;->cC()D

    move-result-wide v8

    double-to-float v7, v8

    const v8, 0x7f0b22e1

    invoke-direct/range {v3 .. v8}, LX/Ghd;-><init>(IIFFI)V

    invoke-virtual {p1, v0, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2384485
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 2384486
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v0, 0x0

    const-wide/16 v4, 0x0

    .line 2384487
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2384488
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, LX/9uc;->bq()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 2384489
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, LX/9uc;->cA()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2384490
    invoke-interface {v1}, LX/9uc;->dc()LX/174;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->dc()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, LX/9uc;->br()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, LX/9uc;->cB()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, LX/9uc;->bs()D

    move-result-wide v2

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->cC()D

    move-result-wide v2

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    .line 2384491
    :catch_0
    goto :goto_0
.end method
