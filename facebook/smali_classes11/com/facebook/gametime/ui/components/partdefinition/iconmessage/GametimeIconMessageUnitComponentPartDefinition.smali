.class public Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageUnitComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/Ghi;

.field private final e:LX/E4b;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Ghi;LX/E4b;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2384707
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2384708
    iput-object p2, p0, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageUnitComponentPartDefinition;->d:LX/Ghi;

    .line 2384709
    iput-object p3, p0, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageUnitComponentPartDefinition;->e:LX/E4b;

    .line 2384710
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2384648
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v0

    .line 2384649
    iget-object v0, p0, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageUnitComponentPartDefinition;->d:LX/Ghi;

    const/4 v2, 0x0

    .line 2384650
    new-instance v3, LX/Ghh;

    invoke-direct {v3, v0}, LX/Ghh;-><init>(LX/Ghi;)V

    .line 2384651
    sget-object v4, LX/Ghi;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Ghg;

    .line 2384652
    if-nez v4, :cond_0

    .line 2384653
    new-instance v4, LX/Ghg;

    invoke-direct {v4}, LX/Ghg;-><init>()V

    .line 2384654
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/Ghg;->a$redex0(LX/Ghg;LX/1De;IILX/Ghh;)V

    .line 2384655
    move-object v3, v4

    .line 2384656
    move-object v2, v3

    .line 2384657
    move-object v0, v2

    .line 2384658
    invoke-interface {v1}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2384659
    invoke-interface {v1}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2384660
    iget-object v3, v0, LX/Ghg;->a:LX/Ghh;

    iput-object v2, v3, LX/Ghh;->b:Ljava/lang/String;

    .line 2384661
    :cond_1
    invoke-interface {v1}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2384662
    invoke-interface {v1}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2384663
    iget-object v3, v0, LX/Ghg;->a:LX/Ghh;

    iput-object v2, v3, LX/Ghh;->c:Ljava/lang/String;

    .line 2384664
    :cond_2
    invoke-interface {v1}, LX/9uc;->aS()LX/5sY;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2384665
    invoke-interface {v1}, LX/9uc;->aS()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    .line 2384666
    iget-object v3, v0, LX/Ghg;->a:LX/Ghh;

    iput-object v2, v3, LX/Ghh;->a:Ljava/lang/String;

    .line 2384667
    :cond_3
    invoke-interface {v1}, LX/9uc;->aj()LX/5sY;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2384668
    invoke-interface {v1}, LX/9uc;->aj()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    .line 2384669
    iget-object v3, v0, LX/Ghg;->a:LX/Ghh;

    iput-object v2, v3, LX/Ghh;->d:Ljava/lang/String;

    .line 2384670
    :cond_4
    invoke-interface {v1}, LX/9uc;->aG()Z

    move-result v2

    .line 2384671
    iget-object v3, v0, LX/Ghg;->a:LX/Ghh;

    iput-boolean v2, v3, LX/Ghh;->e:Z

    .line 2384672
    move-object v2, v0

    .line 2384673
    invoke-interface {v1}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    .line 2384674
    :goto_0
    iget-object v3, v2, LX/Ghg;->a:LX/Ghh;

    iput-boolean v0, v3, LX/Ghh;->f:Z

    .line 2384675
    move-object v0, v2

    .line 2384676
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2384677
    invoke-interface {v1}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 2384678
    iget-object v2, p0, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageUnitComponentPartDefinition;->e:LX/E4b;

    invoke-virtual {v2, p1}, LX/E4b;->c(LX/1De;)LX/E4Z;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/E4Z;->a(LX/1X1;)LX/E4Z;

    move-result-object v0

    invoke-interface {v1}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/E4Z;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)LX/E4Z;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/E4Z;->a(LX/2km;)LX/E4Z;

    move-result-object v0

    .line 2384679
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2384680
    invoke-virtual {v0, v1}, LX/E4Z;->c(Ljava/lang/String;)LX/E4Z;

    move-result-object v0

    .line 2384681
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2384682
    invoke-virtual {v0, v1}, LX/E4Z;->d(Ljava/lang/String;)LX/E4Z;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2384683
    :cond_5
    return-object v0

    .line 2384684
    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageUnitComponentPartDefinition;
    .locals 6

    .prologue
    .line 2384696
    const-class v1, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageUnitComponentPartDefinition;

    monitor-enter v1

    .line 2384697
    :try_start_0
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageUnitComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2384698
    sput-object v2, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageUnitComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2384699
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2384700
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2384701
    new-instance p0, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageUnitComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/Ghi;->a(LX/0QB;)LX/Ghi;

    move-result-object v4

    check-cast v4, LX/Ghi;

    invoke-static {v0}, LX/E4b;->a(LX/0QB;)LX/E4b;

    move-result-object v5

    check-cast v5, LX/E4b;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageUnitComponentPartDefinition;-><init>(Landroid/content/Context;LX/Ghi;LX/E4b;)V

    .line 2384702
    move-object v0, p0

    .line 2384703
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2384704
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2384705
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2384706
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2384695
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2384694
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2384686
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v0, 0x1

    .line 2384687
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2384688
    invoke-interface {v1}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2384689
    :cond_0
    :goto_0
    return v0

    .line 2384690
    :cond_1
    invoke-interface {v1}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2384691
    :cond_2
    invoke-interface {v1}, LX/9uc;->aS()LX/5sY;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, LX/9uc;->aS()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2384692
    :cond_3
    invoke-interface {v1}, LX/9uc;->aj()LX/5sY;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, LX/9uc;->aj()LX/5sY;

    move-result-object v1

    invoke-interface {v1}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2384693
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2384685
    const/4 v0, 0x0

    return-object v0
.end method
