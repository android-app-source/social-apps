.class public Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlaysRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/9o7;",
        "Ljava/lang/Void;",
        "LX/1Pn;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2384454
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2384455
    iput-object p1, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlaysRootPartDefinition;->a:LX/0Ot;

    .line 2384456
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlaysRootPartDefinition;
    .locals 4

    .prologue
    .line 2384443
    const-class v1, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlaysRootPartDefinition;

    monitor-enter v1

    .line 2384444
    :try_start_0
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlaysRootPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2384445
    sput-object v2, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlaysRootPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2384446
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2384447
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2384448
    new-instance v3, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlaysRootPartDefinition;

    const/16 p0, 0xaa9

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlaysRootPartDefinition;-><init>(LX/0Ot;)V

    .line 2384449
    move-object v0, v3

    .line 2384450
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2384451
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlaysRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2384452
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2384453
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2384458
    check-cast p2, LX/9o7;

    .line 2384459
    iget-object v0, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlaysRootPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    new-instance v1, LX/GhZ;

    sget-object v2, LX/Ghb;->SHOW_COMMENT:LX/Ghb;

    invoke-direct {v1, p2, v2}, LX/GhZ;-><init>(LX/9o7;LX/Ghb;)V

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2384460
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2384457
    const/4 v0, 0x1

    return v0
.end method
