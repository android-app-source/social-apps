.class public Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayFeedAttachmentPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2384403
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayFeedAttachmentPartDefinition;
    .locals 3

    .prologue
    .line 2384404
    const-class v1, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayFeedAttachmentPartDefinition;

    monitor-enter v1

    .line 2384405
    :try_start_0
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayFeedAttachmentPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2384406
    sput-object v2, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayFeedAttachmentPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2384407
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2384408
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2384409
    new-instance v0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayFeedAttachmentPartDefinition;

    invoke-direct {v0}, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayFeedAttachmentPartDefinition;-><init>()V

    .line 2384410
    move-object v0, v0

    .line 2384411
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2384412
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayFeedAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2384413
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2384414
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2384415
    const/4 v0, 0x1

    return v0
.end method
