.class public Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field private static final e:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile g:Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;


# instance fields
.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2384626
    const-class v0, Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2384627
    sput-object v1, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;->a:Ljava/lang/String;

    .line 2384628
    sput-object v1, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;->b:Ljava/lang/String;

    .line 2384629
    sput-object v1, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;->c:Ljava/lang/String;

    .line 2384630
    sput-object v1, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2384631
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2384632
    iput-object p1, p0, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;->f:LX/0Or;

    .line 2384633
    return-void
.end method

.method public static a(Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;LX/1De;Ljava/lang/String;II)LX/1Di;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 2384634
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p2}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    sget-object v2, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, p3}, LX/1Di;->i(I)LX/1Di;

    move-result-object v0

    const v1, 0x7f0b0062

    invoke-interface {v0, p4, v1}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;
    .locals 4

    .prologue
    .line 2384635
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;->g:Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;

    if-nez v0, :cond_1

    .line 2384636
    const-class v1, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;

    monitor-enter v1

    .line 2384637
    :try_start_0
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;->g:Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2384638
    if-eqz v2, :cond_0

    .line 2384639
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2384640
    new-instance v3, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;-><init>(LX/0Or;)V

    .line 2384641
    move-object v0, v3

    .line 2384642
    sput-object v0, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;->g:Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2384643
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2384644
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2384645
    :cond_1
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;->g:Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;

    return-object v0

    .line 2384646
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2384647
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
