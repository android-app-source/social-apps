.class public Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile c:Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2384974
    const-class v0, Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2384975
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2384976
    iput-object p1, p0, Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;->b:LX/0Or;

    .line 2384977
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;
    .locals 4

    .prologue
    .line 2384978
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;->c:Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;

    if-nez v0, :cond_1

    .line 2384979
    const-class v1, Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;

    monitor-enter v1

    .line 2384980
    :try_start_0
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;->c:Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2384981
    if-eqz v2, :cond_0

    .line 2384982
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2384983
    new-instance v3, Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;-><init>(LX/0Or;)V

    .line 2384984
    move-object v0, v3

    .line 2384985
    sput-object v0, Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;->c:Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2384986
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2384987
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2384988
    :cond_1
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;->c:Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;

    return-object v0

    .line 2384989
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2384990
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
