.class public Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "Lcom/facebook/gametime/ui/reaction/HasGametimePlays;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:LX/2dq;

.field private final c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2384315
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2384316
    iput-object p1, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2384317
    iput-object p2, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;->b:LX/2dq;

    .line 2384318
    iput-object p3, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;->c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2384319
    iput-object p4, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;->d:Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;

    .line 2384320
    return-void
.end method

.method private a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/E2b;LX/2km;)LX/2eJ;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "LX/E2b;",
            "TE;)",
            "LX/2eJ",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation

    .prologue
    .line 2384312
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2384313
    invoke-interface {v0}, LX/9uc;->cb()LX/0Px;

    move-result-object v2

    .line 2384314
    new-instance v0, LX/GhV;

    move-object v1, p0

    move-object v3, p3

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/GhV;-><init>(Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;LX/0Px;LX/2km;LX/E2b;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;
    .locals 7

    .prologue
    .line 2384321
    const-class v1, Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;

    monitor-enter v1

    .line 2384322
    :try_start_0
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2384323
    sput-object v2, Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2384324
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2384325
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2384326
    new-instance p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v4

    check-cast v4, LX/2dq;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;)V

    .line 2384327
    move-object v0, p0

    .line 2384328
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2384329
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2384330
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2384331
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2384311
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2384286
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 2384287
    move-object v0, p3

    check-cast v0, LX/GiH;

    .line 2384288
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2384289
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 2384290
    invoke-interface {v2}, LX/9uc;->R()Ljava/lang/String;

    move-result-object v2

    .line 2384291
    iget-object v3, v0, LX/GiH;->n:LX/GiO;

    .line 2384292
    iget-object v4, v3, LX/GiO;->d:Ljava/util/Set;

    new-instance v5, LX/GiN;

    invoke-direct {v5, v1, v2}, LX/GiN;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2384293
    iget-boolean v4, v3, LX/GiO;->g:Z

    if-nez v4, :cond_0

    .line 2384294
    const/4 v4, 0x1

    iput-boolean v4, v3, LX/GiO;->g:Z

    .line 2384295
    new-instance v4, LX/Ggb;

    invoke-direct {v4}, LX/Ggb;-><init>()V

    move-object v4, v4

    .line 2384296
    const-string v5, "input"

    new-instance v0, LX/4FG;

    invoke-direct {v0}, LX/4FG;-><init>()V

    iget-object v1, v3, LX/GiO;->b:Ljava/lang/String;

    .line 2384297
    const-string v2, "match_page_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2384298
    move-object v0, v0

    .line 2384299
    invoke-virtual {v4, v5, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2384300
    new-instance v5, LX/GiM;

    invoke-direct {v5, v3}, LX/GiM;-><init>(LX/GiO;)V

    .line 2384301
    :try_start_0
    iget-object v0, v3, LX/GiO;->e:LX/0gX;

    invoke-virtual {v0, v4, v5}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;

    move-result-object v4

    iput-object v4, v3, LX/GiO;->h:LX/0gM;
    :try_end_0
    .catch LX/31B; {:try_start_0 .. :try_end_0} :catch_0

    .line 2384302
    :cond_0
    :goto_0
    new-instance v3, LX/E2b;

    .line 2384303
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2384304
    invoke-direct {v3, v0}, LX/E2b;-><init>(Ljava/lang/String;)V

    move-object v0, p3

    .line 2384305
    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v0, p3

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b1625

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-static {v1, v0}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v1

    .line 2384306
    iget-object v0, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v4, LX/2eF;->a:LX/1Ua;

    invoke-direct {v2, v4}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2384307
    iget-object v6, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;->c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    iget-object v2, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;->b:LX/2dq;

    int-to-float v1, v1

    const/high16 v4, 0x41000000    # 8.0f

    add-float/2addr v1, v4

    sget-object v4, LX/2eF;->a:LX/1Ua;

    const/4 v5, 0x1

    invoke-virtual {v2, v1, v4, v5}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v1

    move-object v2, p3

    check-cast v2, LX/1Pr;

    invoke-interface {v2, v3, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/E2c;

    .line 2384308
    iget v4, v2, LX/E2c;->d:I

    move v2, v4

    .line 2384309
    invoke-direct {p0, p2, v3, p3}, Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/E2b;LX/2km;)LX/2eJ;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/graphql/model/BaseFeedUnit;->g()Ljava/lang/String;

    move-result-object v4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2384310
    const/4 v0, 0x0

    return-object v0

    :catch_0
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2384283
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2384284
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2384285
    invoke-interface {v0}, LX/9uc;->cb()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method
