.class public Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/Gi0;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static final a:I
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation
.end field

.field private static final b:I
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation
.end field

.field public static final c:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field

.field public static final d:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableRowComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2385175
    const v0, 0x7f0a010e

    sput v0, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;->a:I

    .line 2385176
    const v0, 0x7f0a00d6

    sput v0, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;->b:I

    .line 2385177
    const v0, 0x7f0b004e

    sput v0, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;->c:I

    .line 2385178
    const v0, 0x7f0b0050

    sput v0, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;->d:I

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableRowComponentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2385179
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2385180
    iput-object p1, p0, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;->e:LX/0Or;

    .line 2385181
    iput-object p2, p0, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;->f:LX/0Or;

    .line 2385182
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;
    .locals 5

    .prologue
    .line 2385183
    const-class v1, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;

    monitor-enter v1

    .line 2385184
    :try_start_0
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2385185
    sput-object v2, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2385186
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2385187
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2385188
    new-instance v3, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;

    const/16 v4, 0xaae

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 p0, 0x1070

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, v4, p0}, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;-><init>(LX/0Or;LX/0Or;)V

    .line 2385189
    move-object v0, v3

    .line 2385190
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2385191
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2385192
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2385193
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2385194
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    const/4 v10, 0x0

    const/4 v5, 0x0

    .line 2385195
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2385196
    invoke-interface {v0}, LX/9uc;->dg()LX/0Px;

    move-result-object v2

    .line 2385197
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2385198
    invoke-interface {v0}, LX/9uc;->df()LX/0Px;

    move-result-object v3

    .line 2385199
    new-instance v0, LX/Gi0;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object v1, p0

    invoke-direct/range {v0 .. v4}, LX/Gi0;-><init>(Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;LX/0Px;LX/0Px;Landroid/content/Context;)V

    .line 2385200
    iget-object v1, p0, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    new-instance v4, LX/Ghy;

    iget-object v6, v0, LX/Gi0;->b:[I

    sget v7, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;->a:I

    sget v8, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;->c:I

    invoke-direct {v4, v2, v6, v7, v8}, LX/Ghy;-><init>(LX/0Px;[III)V

    invoke-virtual {p1, v1, v4}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2385201
    iget-object v1, p0, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v1, v10}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2385202
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    :goto_0
    if-ge v5, v4, :cond_0

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeTableComponentFragmentModel$TypedDataModel;

    .line 2385203
    iget-object v2, p0, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    new-instance v6, LX/Ghy;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeTableComponentFragmentModel$TypedDataModel;->a()LX/0Px;

    move-result-object v1

    iget-object v7, v0, LX/Gi0;->b:[I

    sget v8, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;->b:I

    sget v9, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;->d:I

    invoke-direct {v6, v1, v7, v8, v9}, LX/Ghy;-><init>(LX/0Px;[III)V

    invoke-virtual {p1, v2, v6}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2385204
    iget-object v1, p0, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v1, v10}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2385205
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 2385206
    :cond_0
    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 2385207
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2385208
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2385209
    invoke-interface {v0}, LX/9uc;->dg()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->df()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2385210
    const/4 v0, 0x0

    .line 2385211
    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, LX/9uc;->dg()LX/0Px;

    move-result-object v1

    invoke-interface {v0}, LX/9uc;->df()LX/0Px;

    move-result-object v0

    const/4 v4, 0x0

    .line 2385212
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    move v3, v2

    .line 2385213
    :goto_1
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p1

    move p0, v4

    :goto_2
    if-ge p0, p1, :cond_3

    invoke-virtual {v0, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeTableComponentFragmentModel$TypedDataModel;

    .line 2385214
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeTableComponentFragmentModel$TypedDataModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-eq v2, v3, :cond_2

    move v2, v4

    .line 2385215
    :goto_3
    move v0, v2

    .line 2385216
    goto :goto_0

    .line 2385217
    :cond_1
    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeTableComponentFragmentModel$TypedDataModel;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeTableComponentFragmentModel$TypedDataModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    move v3, v2

    goto :goto_1

    .line 2385218
    :cond_2
    add-int/lit8 v2, p0, 0x1

    move p0, v2

    goto :goto_2

    .line 2385219
    :cond_3
    const/4 v2, 0x1

    goto :goto_3
.end method
