.class public Lcom/facebook/gametime/ui/components/partdefinition/GametimeSingleSportsPlayGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "LX/1Pn;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2384332
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2384333
    iput-object p1, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSingleSportsPlayGroupPartDefinition;->a:Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;

    .line 2384334
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/GametimeSingleSportsPlayGroupPartDefinition;
    .locals 4

    .prologue
    .line 2384335
    const-class v1, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSingleSportsPlayGroupPartDefinition;

    monitor-enter v1

    .line 2384336
    :try_start_0
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSingleSportsPlayGroupPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2384337
    sput-object v2, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSingleSportsPlayGroupPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2384338
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2384339
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2384340
    new-instance p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSingleSportsPlayGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSingleSportsPlayGroupPartDefinition;-><init>(Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;)V

    .line 2384341
    move-object v0, p0

    .line 2384342
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2384343
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSingleSportsPlayGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2384344
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2384345
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2384346
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2384347
    iget-object v0, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSingleSportsPlayGroupPartDefinition;->a:Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;

    new-instance v1, LX/GhZ;

    .line 2384348
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 2384349
    invoke-interface {v2}, LX/9uc;->cL()LX/9o7;

    move-result-object v2

    sget-object v3, LX/Ghb;->WITH_INSET_BORDER:LX/Ghb;

    invoke-direct {v1, v2, v3}, LX/GhZ;-><init>(LX/9o7;LX/Ghb;)V

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2384350
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2384351
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2384352
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2384353
    invoke-interface {v0}, LX/9uc;->cL()LX/9o7;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
