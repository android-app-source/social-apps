.class public Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoriteUnitComponentPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;

.field private final b:Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2384509
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2384510
    iput-object p1, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoriteUnitComponentPartDefinition;->a:Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;

    .line 2384511
    iput-object p2, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoriteUnitComponentPartDefinition;->b:Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;

    .line 2384512
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoriteUnitComponentPartDefinition;
    .locals 5

    .prologue
    .line 2384536
    const-class v1, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoriteUnitComponentPartDefinition;

    monitor-enter v1

    .line 2384537
    :try_start_0
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoriteUnitComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2384538
    sput-object v2, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoriteUnitComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2384539
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2384540
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2384541
    new-instance p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoriteUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;

    invoke-static {v0}, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;->a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoriteUnitComponentPartDefinition;-><init>(Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;)V

    .line 2384542
    move-object v0, p0

    .line 2384543
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2384544
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoriteUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2384545
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2384546
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2384526
    iget-object v1, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2384527
    invoke-interface {v1}, LX/9uc;->bz()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;

    move-result-object v2

    .line 2384528
    if-eqz v2, :cond_0

    invoke-interface {v2}, LX/9oF;->c()LX/9oA;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, LX/9oF;->c()LX/9oA;

    move-result-object v3

    invoke-interface {v3}, LX/9oA;->a()LX/9o9;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, LX/9oF;->c()LX/9oA;

    move-result-object v3

    invoke-interface {v3}, LX/9oA;->a()LX/9o9;

    move-result-object v3

    invoke-interface {v3}, LX/9o9;->b()LX/9o8;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, LX/9oF;->gJ_()LX/9oE;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, LX/9oF;->gJ_()LX/9oE;

    move-result-object v3

    invoke-interface {v3}, LX/9oE;->a()LX/9oD;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, LX/9oF;->gJ_()LX/9oE;

    move-result-object v3

    invoke-interface {v3}, LX/9oE;->a()LX/9oD;

    move-result-object v3

    invoke-interface {v3}, LX/9oD;->b()LX/9oC;

    move-result-object v3

    if-nez v3, :cond_1

    .line 2384529
    :cond_0
    :goto_0
    return v0

    .line 2384530
    :cond_1
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "#"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, LX/9oF;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 2384531
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "#"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, LX/9oF;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 2384532
    invoke-interface {v2}, LX/9oF;->c()LX/9oA;

    move-result-object v3

    invoke-interface {v3}, LX/9oA;->a()LX/9o9;

    move-result-object v3

    invoke-interface {v3}, LX/9o9;->b()LX/9o8;

    move-result-object v3

    invoke-interface {v3}, LX/9o8;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    .line 2384533
    invoke-interface {v2}, LX/9oF;->gJ_()LX/9oE;

    move-result-object v3

    invoke-interface {v3}, LX/9oE;->a()LX/9oD;

    move-result-object v3

    invoke-interface {v3}, LX/9oD;->b()LX/9oC;

    move-result-object v3

    invoke-interface {v3}, LX/9oC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2384534
    invoke-interface {v2}, LX/9oF;->c()LX/9oA;

    move-result-object v3

    invoke-interface {v3}, LX/9oA;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v2}, LX/9oF;->gJ_()LX/9oE;

    move-result-object v3

    invoke-interface {v3}, LX/9oE;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v1}, LX/9uc;->az()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$FirstVotingPageModel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, LX/9uc;->az()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$FirstVotingPageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$FirstVotingPageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v1}, LX/9uc;->cF()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$SecondVotingPageModel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, LX/9uc;->cF()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$SecondVotingPageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$SecondVotingPageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v1}, LX/9uc;->bz()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;->q()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$MatchPageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, LX/9oF;->e()LX/9oB;

    move-result-object v1

    invoke-interface {v1}, LX/9oB;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v2}, LX/9oF;->b()I

    move-result v1

    if-ltz v1, :cond_0

    invoke-interface {v2}, LX/9oF;->gI_()I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2384535
    :catch_0
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2384515
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pq;

    .line 2384516
    check-cast p3, LX/1Pr;

    new-instance v0, LX/Gi2;

    invoke-direct {v0, p2}, LX/Gi2;-><init>(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    invoke-interface {p3, v0, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gi1;

    .line 2384517
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2384518
    invoke-interface {v1}, LX/9uc;->bz()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;->u()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2384519
    iget-boolean v1, v0, LX/Gi1;->a:Z

    move v0, v1

    .line 2384520
    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2384521
    :goto_0
    if-eqz v0, :cond_1

    .line 2384522
    iget-object v0, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoriteUnitComponentPartDefinition;->a:Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2384523
    :goto_1
    const/4 v0, 0x0

    return-object v0

    .line 2384524
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2384525
    :cond_1
    iget-object v0, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoriteUnitComponentPartDefinition;->b:Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2384513
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2384514
    invoke-static {p1}, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoriteUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method
