.class public Lcom/facebook/gametime/ui/GametimeActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/13v;
.implements LX/0f2;
.implements LX/0f1;
.implements LX/GhL;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field public A:Ljava/lang/String;

.field public B:LX/GhU;

.field public C:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/GhT;",
            ">;"
        }
    .end annotation
.end field

.field private D:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field private E:Landroid/support/v4/view/ViewPager;

.field public p:LX/0gX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/1My;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/GiS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/GiT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private y:LX/0gM;

.field public z:Lcom/facebook/gametime/ui/GametimeHeaderView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2383968
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a(Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 2383926
    new-instance v0, LX/GhJ;

    invoke-direct {v0, p0}, LX/GhJ;-><init>(Lcom/facebook/gametime/ui/GametimeActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2383927
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->q:LX/0hL;

    const v1, 0x7f020756

    invoke-virtual {v0, v1}, LX/0hL;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2383928
    return-void
.end method

.method public static a(Lcom/facebook/gametime/ui/GametimeActivity;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 2383929
    new-instance v0, LX/4FI;

    invoke-direct {v0}, LX/4FI;-><init>()V

    .line 2383930
    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeActivity;->A:Ljava/lang/String;

    .line 2383931
    const-string v2, "page_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2383932
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2383933
    const-string v2, "latest_timestamp"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2383934
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 2383935
    const-string v2, "surface"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2383936
    new-instance v1, LX/Gga;

    invoke-direct {v1}, LX/Gga;-><init>()V

    .line 2383937
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2383938
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->w:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2383939
    return-void
.end method

.method private static a(Lcom/facebook/gametime/ui/GametimeActivity;LX/0gX;LX/0hL;LX/1My;LX/1Ck;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/0Zb;LX/GiS;LX/0tX;LX/GiT;)V
    .locals 0

    .prologue
    .line 2383940
    iput-object p1, p0, Lcom/facebook/gametime/ui/GametimeActivity;->p:LX/0gX;

    iput-object p2, p0, Lcom/facebook/gametime/ui/GametimeActivity;->q:LX/0hL;

    iput-object p3, p0, Lcom/facebook/gametime/ui/GametimeActivity;->r:LX/1My;

    iput-object p4, p0, Lcom/facebook/gametime/ui/GametimeActivity;->s:LX/1Ck;

    iput-object p5, p0, Lcom/facebook/gametime/ui/GametimeActivity;->t:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iput-object p6, p0, Lcom/facebook/gametime/ui/GametimeActivity;->u:LX/0Zb;

    iput-object p7, p0, Lcom/facebook/gametime/ui/GametimeActivity;->v:LX/GiS;

    iput-object p8, p0, Lcom/facebook/gametime/ui/GametimeActivity;->w:LX/0tX;

    iput-object p9, p0, Lcom/facebook/gametime/ui/GametimeActivity;->x:LX/GiT;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/gametime/ui/GametimeActivity;

    invoke-static {v9}, LX/0gX;->a(LX/0QB;)LX/0gX;

    move-result-object v1

    check-cast v1, LX/0gX;

    invoke-static {v9}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v2

    check-cast v2, LX/0hL;

    invoke-static {v9}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v3

    check-cast v3, LX/1My;

    invoke-static {v9}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {v9}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {v9}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v9}, LX/GiS;->b(LX/0QB;)LX/GiS;

    move-result-object v7

    check-cast v7, LX/GiS;

    invoke-static {v9}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    invoke-static {v9}, LX/GiT;->b(LX/0QB;)LX/GiT;

    move-result-object v9

    check-cast v9, LX/GiT;

    invoke-static/range {v0 .. v9}, Lcom/facebook/gametime/ui/GametimeActivity;->a(Lcom/facebook/gametime/ui/GametimeActivity;LX/0gX;LX/0hL;LX/1My;LX/1Ck;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/0Zb;LX/GiS;LX/0tX;LX/GiT;)V

    return-void
.end method

.method private m()V
    .locals 5

    .prologue
    .line 2383976
    new-instance v0, LX/GgY;

    invoke-direct {v0}, LX/GgY;-><init>()V

    move-object v0, v0

    .line 2383977
    const-string v1, "page_id"

    iget-object v2, p0, Lcom/facebook/gametime/ui/GametimeActivity;->A:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2383978
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 2383979
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 2383980
    move-object v0, v0

    .line 2383981
    sget-object v1, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2383982
    new-instance v1, LX/GhH;

    invoke-direct {v1, p0}, LX/GhH;-><init>(Lcom/facebook/gametime/ui/GametimeActivity;)V

    .line 2383983
    iget-object v2, p0, Lcom/facebook/gametime/ui/GametimeActivity;->r:LX/1My;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "gametime_header_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/facebook/gametime/ui/GametimeActivity;->A:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v1, v3}, LX/1My;->a(LX/0zO;LX/0TF;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2383984
    iget-object v2, p0, Lcom/facebook/gametime/ui/GametimeActivity;->s:LX/1Ck;

    const-string v3, "task_header_fetch"

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2383985
    return-void
.end method

.method private n()V
    .locals 8

    .prologue
    .line 2383941
    new-instance v0, LX/GhI;

    invoke-direct {v0, p0}, LX/GhI;-><init>(Lcom/facebook/gametime/ui/GametimeActivity;)V

    .line 2383942
    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeActivity;->v:LX/GiS;

    iget-object v2, p0, Lcom/facebook/gametime/ui/GametimeActivity;->A:Ljava/lang/String;

    .line 2383943
    new-instance v3, LX/Ggd;

    invoke-direct {v3}, LX/Ggd;-><init>()V

    move-object v3, v3

    .line 2383944
    const-string v4, "trigger_data"

    .line 2383945
    const-string v5, "NON_QUERYABLE_REACTION_SURFACE"

    .line 2383946
    new-instance v6, LX/4J3;

    invoke-direct {v6}, LX/4J3;-><init>()V

    iget-object v7, v1, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2383947
    iget-object p0, v7, Lcom/facebook/reaction/ReactionUtil;->j:LX/0Px;

    move-object v7, p0

    .line 2383948
    invoke-virtual {v6, v7}, LX/4J3;->a(Ljava/util/List;)LX/4J3;

    move-result-object v6

    iget-object v7, v1, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2383949
    iget-object p0, v7, Lcom/facebook/reaction/ReactionUtil;->t:LX/0Px;

    move-object v7, p0

    .line 2383950
    invoke-virtual {v6, v7}, LX/4J3;->e(Ljava/util/List;)LX/4J3;

    move-result-object v6

    iget-object v7, v1, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2383951
    iget-object p0, v7, Lcom/facebook/reaction/ReactionUtil;->m:LX/1vj;

    move-object v7, p0

    .line 2383952
    invoke-virtual {v7}, LX/1vj;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/4J3;->d(Ljava/util/List;)LX/4J3;

    move-result-object v6

    iget-object v7, v1, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2383953
    iget-object p0, v7, Lcom/facebook/reaction/ReactionUtil;->r:LX/1s6;

    move-object v7, p0

    .line 2383954
    invoke-virtual {v7, v5}, LX/1s6;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/4J3;->b(Ljava/util/List;)LX/4J3;

    move-result-object v6

    iget-object v7, v1, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2383955
    iget-object p0, v7, Lcom/facebook/reaction/ReactionUtil;->w:LX/1vo;

    move-object v7, p0

    .line 2383956
    invoke-virtual {v7}, LX/1vo;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/4J3;->c(Ljava/util/List;)LX/4J3;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/4J3;->a(Ljava/lang/String;)LX/4J3;

    move-result-object v5

    .line 2383957
    new-instance v6, LX/4FH;

    invoke-direct {v6}, LX/4FH;-><init>()V

    .line 2383958
    const-string v7, "page_id"

    invoke-virtual {v6, v7, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2383959
    const-string v7, "reaction_context"

    invoke-virtual {v6, v7, v5}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2383960
    move-object v5, v6

    .line 2383961
    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2383962
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    const/4 v4, 0x1

    .line 2383963
    iput-boolean v4, v3, LX/0zO;->p:Z

    .line 2383964
    move-object v3, v3

    .line 2383965
    sget-object v4, LX/0zS;->d:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    .line 2383966
    iget-object v4, v1, LX/GiS;->f:LX/1Ck;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "gametime_matchup_surfaces-"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v1, LX/GiS;->b:LX/0tX;

    invoke-virtual {v6, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    invoke-virtual {v4, v5, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2383967
    return-void
.end method

.method private o()V
    .locals 5

    .prologue
    .line 2383969
    new-instance v0, LX/GgZ;

    invoke-direct {v0}, LX/GgZ;-><init>()V

    move-object v0, v0

    .line 2383970
    const-string v1, "input"

    new-instance v2, LX/4FF;

    invoke-direct {v2}, LX/4FF;-><init>()V

    iget-object v3, p0, Lcom/facebook/gametime/ui/GametimeActivity;->A:Ljava/lang/String;

    .line 2383971
    const-string v4, "match_page_id"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2383972
    move-object v2, v2

    .line 2383973
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2383974
    :try_start_0
    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeActivity;->p:LX/0gX;

    invoke-virtual {v1, v0}, LX/0gX;->a(LX/0gV;)LX/0gM;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->y:LX/0gM;
    :try_end_0
    .catch LX/31B; {:try_start_0 .. :try_end_0} :catch_0

    .line 2383975
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public static p(Lcom/facebook/gametime/ui/GametimeActivity;)V
    .locals 5

    .prologue
    .line 2383911
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->C:Ljava/util/List;

    .line 2383912
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->C:Ljava/util/List;

    new-instance v1, LX/GhT;

    sget-object v2, LX/GiR;->TAIL_LOAD:LX/GiR;

    const-string v3, "ANDROID_GAMETIME_MATCHUP_TAB"

    const v4, 0x7f0835cd

    invoke-virtual {p0, v4}, Lcom/facebook/gametime/ui/GametimeActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/GhT;-><init>(LX/GiR;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2383913
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->C:Ljava/util/List;

    new-instance v1, LX/GhT;

    sget-object v2, LX/GiR;->TAIL_LOAD:LX/GiR;

    const-string v3, "ANDROID_GAMETIME_FRIEND_STORIES"

    const v4, 0x7f0835cc

    invoke-virtual {p0, v4}, Lcom/facebook/gametime/ui/GametimeActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/GhT;-><init>(LX/GiR;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2383914
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->C:Ljava/util/List;

    new-instance v1, LX/GhT;

    sget-object v2, LX/GiR;->TAIL_LOAD:LX/GiR;

    const-string v3, "ANDROID_GAMETIME_EXPERT_STORIES"

    const v4, 0x7f0835cb

    invoke-virtual {p0, v4}, Lcom/facebook/gametime/ui/GametimeActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/GhT;-><init>(LX/GiR;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2383915
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->C:Ljava/util/List;

    new-instance v1, LX/GhT;

    sget-object v2, LX/GiR;->TAIL_LOAD:LX/GiR;

    const-string v3, "ANDROID_GAMETIME_MATCH_STATS"

    const v4, 0x7f0835cf

    invoke-virtual {p0, v4}, Lcom/facebook/gametime/ui/GametimeActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/GhT;-><init>(LX/GiR;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2383916
    return-void
.end method

.method public static q(Lcom/facebook/gametime/ui/GametimeActivity;)V
    .locals 5

    .prologue
    .line 2383917
    new-instance v0, LX/GhU;

    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeActivity;->A:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/gametime/ui/GametimeActivity;->C:Ljava/util/List;

    sget-object v3, LX/GhR;->MATCH:LX/GhR;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, LX/GhU;-><init>(Ljava/lang/String;Ljava/util/List;LX/GhR;LX/0gc;)V

    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->B:LX/GhU;

    .line 2383918
    const v0, 0x7f0d1426

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->E:Landroid/support/v4/view/ViewPager;

    .line 2383919
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->E:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeActivity;->B:LX/GhU;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2383920
    const v0, 0x7f0d1425

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->D:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2383921
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->D:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeActivity;->E:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2383922
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->D:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    new-instance v1, LX/GhK;

    invoke-direct {v1, p0, p0}, LX/GhK;-><init>(Lcom/facebook/gametime/ui/GametimeActivity;Lcom/facebook/gametime/ui/GametimeActivity;)V

    .line 2383923
    iput-object v1, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 2383924
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->x:LX/GiT;

    invoke-virtual {v0, p0}, LX/GiT;->a(Lcom/facebook/base/activity/FbFragmentActivity;)V

    .line 2383925
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2383860
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->E:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->C:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeActivity;->E:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 2383861
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->E:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 2383862
    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeActivity;->C:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GhT;

    iget-object v0, v0, LX/GhT;->b:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 2383863
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionUnitsFragmentModel;Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;)V
    .locals 3

    .prologue
    .line 2383864
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->B:LX/GhU;

    if-nez v0, :cond_1

    .line 2383865
    :cond_0
    :goto_0
    return-void

    .line 2383866
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionUnitsFragmentModel;->j()I

    move-result v0

    .line 2383867
    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeActivity;->B:LX/GhU;

    invoke-virtual {v1, p2}, LX/GhU;->b(Ljava/lang/Object;)I

    move-result v1

    .line 2383868
    iget-object v2, p0, Lcom/facebook/gametime/ui/GametimeActivity;->B:LX/GhU;

    invoke-virtual {v2, v0, v1}, LX/GhU;->a(II)V

    .line 2383869
    iget-object v2, p0, Lcom/facebook/gametime/ui/GametimeActivity;->E:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    .line 2383870
    if-ne v2, v1, :cond_0

    if-lez v0, :cond_0

    .line 2383871
    invoke-virtual {p1}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionUnitsFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/GiS;->a(LX/0Px;)I

    move-result v0

    .line 2383872
    invoke-virtual {p2}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/gametime/ui/GametimeActivity;->a(Lcom/facebook/gametime/ui/GametimeActivity;ILjava/lang/String;)V

    .line 2383873
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->B:LX/GhU;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, LX/GhU;->a(II)V

    goto :goto_0
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2383874
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2383875
    const-string v1, "gametime_page_id"

    iget-object v2, p0, Lcom/facebook/gametime/ui/GametimeActivity;->A:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2383876
    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2383900
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2383901
    invoke-static {p0, p0}, Lcom/facebook/gametime/ui/GametimeActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2383902
    const v0, 0x7f03078b

    invoke-virtual {p0, v0}, Lcom/facebook/gametime/ui/GametimeActivity;->setContentView(I)V

    .line 2383903
    invoke-virtual {p0}, Lcom/facebook/gametime/ui/GametimeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "page_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "-1"

    :goto_0
    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->A:Ljava/lang/String;

    .line 2383904
    invoke-direct {p0}, Lcom/facebook/gametime/ui/GametimeActivity;->n()V

    .line 2383905
    const v0, 0x7f0d1424

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/gametime/ui/GametimeHeaderView;

    iput-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->z:Lcom/facebook/gametime/ui/GametimeHeaderView;

    .line 2383906
    const v0, 0x7f0d1427

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/facebook/gametime/ui/GametimeActivity;->a(Landroid/widget/ImageView;)V

    .line 2383907
    invoke-direct {p0}, Lcom/facebook/gametime/ui/GametimeActivity;->m()V

    .line 2383908
    invoke-direct {p0}, Lcom/facebook/gametime/ui/GametimeActivity;->o()V

    .line 2383909
    return-void

    .line 2383910
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/gametime/ui/GametimeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "page_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final l()V
    .locals 0

    .prologue
    .line 2383877
    invoke-direct {p0}, Lcom/facebook/gametime/ui/GametimeActivity;->m()V

    .line 2383878
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2383879
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 2383880
    :cond_0
    :goto_0
    return-void

    .line 2383881
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 2383882
    :pswitch_0
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 2383883
    :pswitch_1
    const-string v0, "extra_composer_has_published"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2383884
    iget-object v0, p0, Lcom/facebook/gametime/ui/GametimeActivity;->t:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x6dc
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x4eee8229

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2383885
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2383886
    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeActivity;->r:LX/1My;

    invoke-virtual {v1}, LX/1My;->a()V

    .line 2383887
    const/16 v1, 0x23

    const v2, -0x3429d27a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x681d8c22

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2383888
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2383889
    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeActivity;->r:LX/1My;

    invoke-virtual {v1}, LX/1My;->d()V

    .line 2383890
    const/16 v1, 0x23

    const v2, -0x5d92444e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x17abf978

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2383891
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2383892
    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeActivity;->r:LX/1My;

    invoke-virtual {v1}, LX/1My;->e()V

    .line 2383893
    const/16 v1, 0x23

    const v2, 0x7420060b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x4ab84cbe    # 6039135.0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2383894
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 2383895
    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeActivity;->s:LX/1Ck;

    const-string v2, "task_header_fetch"

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2383896
    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeActivity;->y:LX/0gM;

    if-eqz v1, :cond_0

    .line 2383897
    iget-object v1, p0, Lcom/facebook/gametime/ui/GametimeActivity;->p:LX/0gX;

    iget-object v2, p0, Lcom/facebook/gametime/ui/GametimeActivity;->y:LX/0gM;

    invoke-static {v2}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0gX;->a(Ljava/util/Set;)V

    .line 2383898
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/gametime/ui/GametimeActivity;->y:LX/0gM;

    .line 2383899
    :cond_0
    const/16 v1, 0x23

    const v2, -0x4e5649af

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
