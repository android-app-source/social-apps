.class public final Lcom/facebook/gametime/ui/reaction/GametimeReactionUtil$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/GiS;


# direct methods
.method public constructor <init>(LX/GiS;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2385741
    iput-object p1, p0, Lcom/facebook/gametime/ui/reaction/GametimeReactionUtil$2;->c:LX/GiS;

    iput-object p2, p0, Lcom/facebook/gametime/ui/reaction/GametimeReactionUtil$2;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/gametime/ui/reaction/GametimeReactionUtil$2;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 2385742
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeReactionUtil$2;->c:LX/GiS;

    iget-object v1, p0, Lcom/facebook/gametime/ui/reaction/GametimeReactionUtil$2;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/gametime/ui/reaction/GametimeReactionUtil$2;->b:Ljava/lang/String;

    const/4 v3, 0x0

    .line 2385743
    new-instance v4, LX/GgT;

    invoke-direct {v4}, LX/GgT;-><init>()V

    move-object v4, v4

    .line 2385744
    iget-object v5, v0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    invoke-virtual {v5, v4, v2}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0gW;Ljava/lang/String;)V

    .line 2385745
    const-string v5, "afterCursor"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "count"

    const/16 p0, 0xa

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v5, v6, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v5

    const-string v6, "trigger_data"

    invoke-static {v0, v2, v1}, LX/GiS;->a(LX/GiS;Ljava/lang/String;Ljava/lang/String;)LX/4FJ;

    move-result-object p0

    invoke-virtual {v5, v6, p0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2385746
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    sget-object v5, LX/0zS;->c:LX/0zS;

    invoke-virtual {v4, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    .line 2385747
    iget-object v5, v0, LX/GiS;->f:LX/1Ck;

    iget-object v6, v0, LX/GiS;->b:LX/0tX;

    invoke-virtual {v6, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    new-instance v6, LX/GiQ;

    invoke-direct {v6, v0, v1}, LX/GiQ;-><init>(LX/GiS;Ljava/lang/String;)V

    invoke-virtual {v5, v1, v4, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2385748
    iget-object v4, v0, LX/GiS;->c:LX/1vi;

    new-instance v5, LX/2jX;

    const/4 v6, 0x0

    invoke-direct {v5, v1, v6}, LX/2jX;-><init>(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)V

    invoke-virtual {v4, v5}, LX/0b4;->a(LX/0b7;)V

    .line 2385749
    return-void
.end method
