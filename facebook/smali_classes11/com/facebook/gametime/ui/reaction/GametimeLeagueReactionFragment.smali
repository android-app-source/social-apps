.class public Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;
.super Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final i:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public j:LX/GiS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:LX/GiR;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2385594
    const-class v0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->i:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2385592
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;-><init>()V

    .line 2385593
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;

    invoke-static {p0}, LX/GiS;->b(LX/0QB;)LX/GiS;

    move-result-object p0

    check-cast p0, LX/GiS;

    iput-object p0, p1, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->j:LX/GiS;

    return-void
.end method


# virtual methods
.method public final M()I
    .locals 1

    .prologue
    .line 2385591
    const v0, 0x7f030789

    return v0
.end method

.method public final N()V
    .locals 9

    .prologue
    .line 2385574
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v0, v0

    .line 2385575
    if-eqz v0, :cond_0

    .line 2385576
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v0, v0

    .line 2385577
    iget-boolean v1, v0, LX/2jY;->o:Z

    move v0, v1

    .line 2385578
    if-eqz v0, :cond_0

    .line 2385579
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v0, v0

    .line 2385580
    iget-boolean v1, v0, LX/2jY;->p:Z

    move v0, v1

    .line 2385581
    if-eqz v0, :cond_1

    .line 2385582
    :cond_0
    :goto_0
    return-void

    .line 2385583
    :cond_1
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v0, v0

    .line 2385584
    const/4 v1, 0x1

    .line 2385585
    iput-boolean v1, v0, LX/2jY;->p:Z

    .line 2385586
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->j:LX/GiS;

    iget-object v1, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->l:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->n()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    iget-object v4, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->m:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->k:LX/GiR;

    .line 2385587
    iget-object v6, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v6, v6

    .line 2385588
    invoke-virtual {v6}, LX/2jY;->g()Ljava/lang/String;

    move-result-object v6

    .line 2385589
    iget-object v7, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v7, v7

    .line 2385590
    invoke-virtual {v7}, LX/2jY;->g()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, LX/GiS;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;LX/GiR;Ljava/lang/String;Ljava/lang/String;LX/GiJ;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2385573
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->m:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2385564
    const-class v0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;

    invoke-static {v0, p0}, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2385565
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2385566
    const-string v1, "empty_state_image_uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->n:Ljava/lang/String;

    .line 2385567
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2385568
    const-string v1, "empty_state_text"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->o:Ljava/lang/String;

    .line 2385569
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2385570
    const-string v1, "empty_state_sub_text"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->p:Ljava/lang/String;

    .line 2385571
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->a(Landroid/os/Bundle;)V

    .line 2385572
    return-void
.end method

.method public final b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 4

    .prologue
    .line 2385552
    invoke-super {p0, p1, p2}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 2385553
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a:Landroid/view/View;

    move-object v1, v0

    .line 2385554
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2385555
    const v0, 0x7f0d141e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2385556
    iget-object v2, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->n:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->i:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2385557
    :cond_0
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->o:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2385558
    const v0, 0x7f0d141f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2385559
    iget-object v2, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->o:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2385560
    :cond_1
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->p:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2385561
    const v0, 0x7f0d1420

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2385562
    iget-object v1, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2385563
    :cond_2
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2385532
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->e()V

    .line 2385533
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, LX/GhQ;

    if-eqz v0, :cond_0

    .line 2385534
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    .line 2385535
    :cond_0
    return-void
.end method

.method public final v()LX/2jY;
    .locals 12

    .prologue
    .line 2385539
    new-instance v5, LX/GiK;

    invoke-direct {v5, p0, p0}, LX/GiK;-><init>(Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;)V

    .line 2385540
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2385541
    if-nez v0, :cond_0

    .line 2385542
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->j:LX/GiS;

    .line 2385543
    const-string v7, "ANDROID_GAMETIME_LEAGUE_SCHEDULE"

    const-string v8, "-1"

    sget-object v9, LX/GiR;->HEAD_LOAD:LX/GiR;

    new-instance v10, Ljava/util/Date;

    invoke-direct {v10}, Ljava/util/Date;-><init>()V

    move-object v6, v0

    move-object v11, v5

    invoke-virtual/range {v6 .. v11}, LX/GiS;->a(Ljava/lang/String;Ljava/lang/String;LX/GiR;Ljava/util/Date;LX/GiJ;)LX/2jY;

    move-result-object v6

    move-object v0, v6

    .line 2385544
    :goto_0
    return-object v0

    .line 2385545
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2385546
    const-string v1, "load_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/GiR;

    iput-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->k:LX/GiR;

    .line 2385547
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2385548
    const-string v1, "league_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->l:Ljava/lang/String;

    .line 2385549
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2385550
    const-string v1, "reaction_surface"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->m:Ljava/lang/String;

    .line 2385551
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->j:LX/GiS;

    iget-object v1, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->m:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->l:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->k:LX/GiR;

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual/range {v0 .. v5}, LX/GiS;->a(Ljava/lang/String;Ljava/lang/String;LX/GiR;Ljava/util/Date;LX/GiJ;)LX/2jY;

    move-result-object v0

    goto :goto_0
.end method

.method public final w()I
    .locals 2

    .prologue
    .line 2385536
    const-string v0, "ANDROID_GAMETIME_LEAGUE_FRIEND_STORIES"

    iget-object v1, p0, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2385537
    const/4 v0, 0x3

    .line 2385538
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
