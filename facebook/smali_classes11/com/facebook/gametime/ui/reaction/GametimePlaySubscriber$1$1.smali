.class public final Lcom/facebook/gametime/ui/reaction/GametimePlaySubscriber$1$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimePlaySubscriptionModel$PlayModel;

.field public final synthetic b:LX/GiM;


# direct methods
.method public constructor <init>(LX/GiM;Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimePlaySubscriptionModel$PlayModel;)V
    .locals 0

    .prologue
    .line 2385687
    iput-object p1, p0, Lcom/facebook/gametime/ui/reaction/GametimePlaySubscriber$1$1;->b:LX/GiM;

    iput-object p2, p0, Lcom/facebook/gametime/ui/reaction/GametimePlaySubscriber$1$1;->a:Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimePlaySubscriptionModel$PlayModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 2385688
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimePlaySubscriber$1$1;->b:LX/GiM;

    iget-object v0, v0, LX/GiM;->a:LX/GiO;

    iget-object v1, p0, Lcom/facebook/gametime/ui/reaction/GametimePlaySubscriber$1$1;->a:Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimePlaySubscriptionModel$PlayModel;

    .line 2385689
    iget-object v2, v0, LX/GiO;->d:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/GiN;

    .line 2385690
    iget-object v3, v0, LX/GiO;->c:LX/0o8;

    iget-object v5, v2, LX/GiN;->a:Ljava/lang/String;

    invoke-interface {v3, v5}, LX/0o8;->b(Ljava/lang/String;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v5

    .line 2385691
    if-eqz v5, :cond_0

    .line 2385692
    iget-object v3, v2, LX/GiN;->b:Ljava/lang/String;

    invoke-static {v5, v3}, LX/Cfu;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    move-result-object v6

    .line 2385693
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->cb()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2385694
    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->cb()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, -0x1

    .line 2385695
    :goto_1
    invoke-virtual {v1}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimePlaySubscriptionModel$PlayModel;->m()I

    move-result v7

    if-ge v3, v7, :cond_0

    .line 2385696
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 2385697
    invoke-static {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel;->a(LX/9o7;)Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel;

    move-result-object v3

    invoke-virtual {v8, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2385698
    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->cb()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result p0

    const/4 v3, 0x0

    move v7, v3

    :goto_2
    if-ge v7, p0, :cond_1

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/9o7;

    .line 2385699
    invoke-static {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel;->a(LX/9o7;)Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel;

    move-result-object v3

    invoke-virtual {v8, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2385700
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    goto :goto_2

    .line 2385701
    :cond_1
    invoke-static {v6}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    move-result-object v3

    invoke-static {v3}, LX/9vz;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)LX/9vz;

    move-result-object v3

    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    .line 2385702
    iput-object v7, v3, LX/9vz;->bV:LX/0Px;

    .line 2385703
    move-object v3, v3

    .line 2385704
    invoke-virtual {v3}, LX/9vz;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    move-result-object v3

    move-object v3, v3

    .line 2385705
    iget-object v6, v2, LX/GiN;->b:Ljava/lang/String;

    invoke-static {v5, v3, v6}, LX/Cfu;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;LX/9uc;Ljava/lang/String;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v3

    .line 2385706
    iget-object v5, v0, LX/GiO;->c:LX/0o8;

    iget-object v2, v2, LX/GiN;->a:Ljava/lang/String;

    invoke-interface {v5, v3, v2}, LX/0o8;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z

    goto :goto_0

    .line 2385707
    :cond_2
    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->cb()LX/0Px;

    move-result-object v3

    const/4 v7, 0x0

    invoke-virtual {v3, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/9o7;

    invoke-interface {v3}, LX/9o7;->m()I

    move-result v3

    goto :goto_1

    .line 2385708
    :cond_3
    return-void
.end method
