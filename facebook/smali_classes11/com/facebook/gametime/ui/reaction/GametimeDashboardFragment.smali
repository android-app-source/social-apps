.class public Lcom/facebook/gametime/ui/reaction/GametimeDashboardFragment;
.super Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->GAMETIME_DASHBOARD_FRAGMENT:LX/0cQ;
.end annotation


# instance fields
.field public i:LX/GiS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/GiT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2385516
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/gametime/ui/reaction/GametimeDashboardFragment;

    invoke-static {p0}, LX/GiS;->b(LX/0QB;)LX/GiS;

    move-result-object v1

    check-cast v1, LX/GiS;

    invoke-static {p0}, LX/GiT;->b(LX/0QB;)LX/GiT;

    move-result-object p0

    check-cast p0, LX/GiT;

    iput-object v1, p1, Lcom/facebook/gametime/ui/reaction/GametimeDashboardFragment;->i:LX/GiS;

    iput-object p0, p1, Lcom/facebook/gametime/ui/reaction/GametimeDashboardFragment;->j:LX/GiT;

    return-void
.end method


# virtual methods
.method public final N()V
    .locals 7

    .prologue
    .line 2385493
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v0, v0

    .line 2385494
    if-eqz v0, :cond_0

    .line 2385495
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v0, v0

    .line 2385496
    iget-boolean v1, v0, LX/2jY;->o:Z

    move v0, v1

    .line 2385497
    if-eqz v0, :cond_0

    .line 2385498
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v0, v0

    .line 2385499
    iget-boolean v1, v0, LX/2jY;->p:Z

    move v0, v1

    .line 2385500
    if-eqz v0, :cond_1

    .line 2385501
    :cond_0
    :goto_0
    return-void

    .line 2385502
    :cond_1
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v0, v0

    .line 2385503
    const/4 v1, 0x1

    .line 2385504
    iput-boolean v1, v0, LX/2jY;->p:Z

    .line 2385505
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeDashboardFragment;->i:LX/GiS;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->n()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ANDROID_GAMETIME_SCORES"

    .line 2385506
    iget-object v3, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v3, v3

    .line 2385507
    invoke-virtual {v3}, LX/2jY;->g()Ljava/lang/String;

    move-result-object v3

    .line 2385508
    new-instance v4, LX/GgT;

    invoke-direct {v4}, LX/GgT;-><init>()V

    move-object v4, v4

    .line 2385509
    iget-object v5, v0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    invoke-virtual {v5, v4, v2}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0gW;Ljava/lang/String;)V

    .line 2385510
    const-string v5, "afterCursor"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "count"

    const/16 p0, 0xa

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v5, v6, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v5

    const-string v6, "trigger_data"

    invoke-static {v0, v2, v1}, LX/GiS;->a(LX/GiS;Ljava/lang/String;Ljava/lang/String;)LX/4FJ;

    move-result-object p0

    invoke-virtual {v5, v6, p0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2385511
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    sget-object v5, LX/0zS;->c:LX/0zS;

    invoke-virtual {v4, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    .line 2385512
    iget-object v5, v0, LX/GiS;->f:LX/1Ck;

    iget-object v6, v0, LX/GiS;->b:LX/0tX;

    invoke-virtual {v6, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    new-instance v6, LX/GiQ;

    invoke-direct {v6, v0, v1}, LX/GiQ;-><init>(LX/GiS;Ljava/lang/String;)V

    invoke-virtual {v5, v1, v4, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2385513
    iget-object v4, v0, LX/GiS;->c:LX/1vi;

    new-instance v5, LX/2jX;

    const/4 v6, 0x0

    invoke-direct {v5, v1, v6}, LX/2jX;-><init>(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)V

    invoke-virtual {v4, v5}, LX/0b4;->a(LX/0b7;)V

    .line 2385514
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2385515
    const-string v0, "ANDROID_GAMETIME_SCORES"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2385466
    const-class v0, Lcom/facebook/gametime/ui/reaction/GametimeDashboardFragment;

    invoke-static {v0, p0}, Lcom/facebook/gametime/ui/reaction/GametimeDashboardFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2385467
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->a(Landroid/os/Bundle;)V

    .line 2385468
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2ff54a4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2385469
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->onStart()V

    .line 2385470
    const v1, 0x7f0835ce

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->c(Ljava/lang/String;)V

    .line 2385471
    const/16 v1, 0x2b

    const v2, 0x645c0a81

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final v()LX/2jY;
    .locals 8

    .prologue
    .line 2385472
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeDashboardFragment;->j:LX/GiT;

    .line 2385473
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2385474
    invoke-virtual {v0, v1}, LX/GiT;->a(Landroid/os/Bundle;)V

    .line 2385475
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeDashboardFragment;->i:LX/GiS;

    const-string v1, "ANDROID_GAMETIME_SCORES"

    .line 2385476
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2385477
    iget-object v3, v0, LX/GiS;->d:LX/2iz;

    invoke-virtual {v3, v2, v1}, LX/2iz;->b(Ljava/lang/String;Ljava/lang/String;)LX/2jY;

    move-result-object v3

    .line 2385478
    const/4 v4, 0x1

    .line 2385479
    iput-boolean v4, v3, LX/2jY;->w:Z

    .line 2385480
    new-instance v4, Lcom/facebook/gametime/ui/reaction/GametimeReactionUtil$2;

    invoke-direct {v4, v0, v2, v1}, Lcom/facebook/gametime/ui/reaction/GametimeReactionUtil$2;-><init>(LX/GiS;Ljava/lang/String;Ljava/lang/String;)V

    .line 2385481
    iput-object v4, v3, LX/2jY;->A:Ljava/lang/Runnable;

    .line 2385482
    iget-object v4, v0, LX/GiS;->d:LX/2iz;

    invoke-virtual {v4, v2}, LX/2iz;->c(Ljava/lang/String;)V

    .line 2385483
    iget-object v4, v0, LX/GiS;->d:LX/2iz;

    invoke-virtual {v4, v2}, LX/2iz;->d(Ljava/lang/String;)V

    .line 2385484
    const/4 v4, 0x0

    .line 2385485
    new-instance v5, LX/GgT;

    invoke-direct {v5}, LX/GgT;-><init>()V

    move-object v5, v5

    .line 2385486
    iget-object v6, v0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    invoke-virtual {v6, v5, v1}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0gW;Ljava/lang/String;)V

    .line 2385487
    const-string v6, "afterCursor"

    invoke-virtual {v5, v6, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string v7, "count"

    const/16 p0, 0xa

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v7, "trigger_data"

    invoke-static {v0, v1, v2}, LX/GiS;->a(LX/GiS;Ljava/lang/String;Ljava/lang/String;)LX/4FJ;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2385488
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    sget-object v6, LX/0zS;->c:LX/0zS;

    invoke-virtual {v5, v6}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v5

    .line 2385489
    iget-object v6, v0, LX/GiS;->f:LX/1Ck;

    iget-object v7, v0, LX/GiS;->b:LX/0tX;

    invoke-virtual {v7, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    new-instance v7, LX/GiQ;

    invoke-direct {v7, v0, v2}, LX/GiQ;-><init>(LX/GiS;Ljava/lang/String;)V

    invoke-virtual {v6, v2, v5, v7}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2385490
    iget-object v5, v0, LX/GiS;->c:LX/1vi;

    new-instance v6, LX/2jX;

    const/4 v7, 0x0

    invoke-direct {v6, v2, v7}, LX/2jX;-><init>(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)V

    invoke-virtual {v5, v6}, LX/0b4;->a(LX/0b7;)V

    .line 2385491
    move-object v0, v3

    .line 2385492
    return-object v0
.end method
