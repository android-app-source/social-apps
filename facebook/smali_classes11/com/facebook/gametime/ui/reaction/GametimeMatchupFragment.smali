.class public Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;
.super Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final i:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public j:LX/GiG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/GiS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/GiP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private m:LX/GiR;

.field private n:LX/GiO;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2385674
    const-class v0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->i:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2385672
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;-><init>()V

    .line 2385673
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;

    const-class v1, LX/GiG;

    invoke-interface {v3, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/GiG;

    invoke-static {v3}, LX/GiS;->b(LX/0QB;)LX/GiS;

    move-result-object v2

    check-cast v2, LX/GiS;

    const-class p0, LX/GiP;

    invoke-interface {v3, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/GiP;

    iput-object v1, p1, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->j:LX/GiG;

    iput-object v2, p1, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->k:LX/GiS;

    iput-object v3, p1, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->l:LX/GiP;

    return-void
.end method


# virtual methods
.method public final M()I
    .locals 1

    .prologue
    .line 2385601
    const v0, 0x7f030789

    return v0
.end method

.method public final N()V
    .locals 8

    .prologue
    .line 2385655
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v0, v0

    .line 2385656
    if-eqz v0, :cond_0

    .line 2385657
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v0, v0

    .line 2385658
    iget-boolean v1, v0, LX/2jY;->o:Z

    move v0, v1

    .line 2385659
    if-eqz v0, :cond_0

    .line 2385660
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v0, v0

    .line 2385661
    iget-boolean v1, v0, LX/2jY;->p:Z

    move v0, v1

    .line 2385662
    if-eqz v0, :cond_1

    .line 2385663
    :cond_0
    :goto_0
    return-void

    .line 2385664
    :cond_1
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v0, v0

    .line 2385665
    const/4 v1, 0x1

    .line 2385666
    iput-boolean v1, v0, LX/2jY;->p:Z

    .line 2385667
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->k:LX/GiS;

    iget-object v1, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->o:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->n()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->p:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->m:LX/GiR;

    .line 2385668
    iget-object v5, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v5, v5

    .line 2385669
    invoke-virtual {v5}, LX/2jY;->g()Ljava/lang/String;

    move-result-object v5

    .line 2385670
    iget-object v6, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v6, v6

    .line 2385671
    invoke-virtual {v6}, LX/2jY;->g()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, LX/GiS;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/GiR;Ljava/lang/String;Ljava/lang/String;LX/GiJ;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)LX/E8m;
    .locals 7

    .prologue
    .line 2385654
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->j:LX/GiG;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->H()LX/1PT;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->G()LX/1SX;

    move-result-object v3

    iget-object v5, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->o:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->n:LX/GiO;

    move-object v1, p1

    move-object v4, p0

    invoke-virtual/range {v0 .. v6}, LX/GiG;->a(Landroid/content/Context;LX/1PT;LX/1SX;LX/0o8;Ljava/lang/String;LX/GiO;)LX/GiF;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2385653
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->p:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2385640
    const-class v0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;

    invoke-static {v0, p0}, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2385641
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2385642
    const-string v1, "empty_state_image_uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->q:Ljava/lang/String;

    .line 2385643
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2385644
    const-string v1, "empty_state_text"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->r:Ljava/lang/String;

    .line 2385645
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2385646
    const-string v1, "empty_state_sub_text"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->s:Ljava/lang/String;

    .line 2385647
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->a(Landroid/os/Bundle;)V

    .line 2385648
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->l:LX/GiP;

    iget-object v1, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->o:Ljava/lang/String;

    .line 2385649
    new-instance v2, LX/GiO;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static {v0}, LX/0gX;->a(LX/0QB;)LX/0gX;

    move-result-object v6

    check-cast v6, LX/0gX;

    invoke-static {v0}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v7

    check-cast v7, LX/1My;

    move-object v3, v1

    move-object v4, p0

    invoke-direct/range {v2 .. v7}, LX/GiO;-><init>(Ljava/lang/String;LX/0o8;LX/0Sh;LX/0gX;LX/1My;)V

    .line 2385650
    move-object v0, v2

    .line 2385651
    iput-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->n:LX/GiO;

    .line 2385652
    return-void
.end method

.method public final b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 4

    .prologue
    .line 2385675
    invoke-super {p0, p1, p2}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 2385676
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a:Landroid/view/View;

    move-object v1, v0

    .line 2385677
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2385678
    const v0, 0x7f0d141e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2385679
    iget-object v2, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->q:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->i:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2385680
    :cond_0
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->r:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2385681
    const v0, 0x7f0d141f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2385682
    iget-object v2, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2385683
    :cond_1
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->s:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2385684
    const v0, 0x7f0d1420

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2385685
    iget-object v1, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2385686
    :cond_2
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2385636
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->e()V

    .line 2385637
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, LX/GhL;

    if-eqz v0, :cond_0

    .line 2385638
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, LX/GhL;

    invoke-interface {v0}, LX/GhL;->l()V

    .line 2385639
    :cond_0
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3709db4f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2385632
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->onDestroy()V

    .line 2385633
    iget-object v1, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->n:LX/GiO;

    .line 2385634
    iget-object v2, v1, LX/GiO;->f:LX/1My;

    invoke-virtual {v2}, LX/1My;->a()V

    .line 2385635
    const/16 v1, 0x2b

    const v2, 0x3ed96d05

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5c12507f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2385628
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->onPause()V

    .line 2385629
    iget-object v1, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->n:LX/GiO;

    .line 2385630
    iget-object v2, v1, LX/GiO;->f:LX/1My;

    invoke-virtual {v2}, LX/1My;->d()V

    .line 2385631
    const/16 v1, 0x2b

    const v2, -0x5a903fa5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x34a8edaf    # -1.4094929E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2385624
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->onResume()V

    .line 2385625
    iget-object v1, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->n:LX/GiO;

    .line 2385626
    iget-object v2, v1, LX/GiO;->f:LX/1My;

    invoke-virtual {v2}, LX/1My;->e()V

    .line 2385627
    const/16 v1, 0x2b

    const v2, -0x340fb142

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x776d33d7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2385618
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->onStop()V

    .line 2385619
    iget-object v1, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->n:LX/GiO;

    .line 2385620
    iget-object v2, v1, LX/GiO;->h:LX/0gM;

    if-eqz v2, :cond_0

    .line 2385621
    iget-object v2, v1, LX/GiO;->e:LX/0gX;

    iget-object p0, v1, LX/GiO;->h:LX/0gM;

    invoke-static {p0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/0gX;->a(Ljava/util/Set;)V

    .line 2385622
    const/4 v2, 0x0

    iput-object v2, v1, LX/GiO;->h:LX/0gM;

    .line 2385623
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x659d0ec

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final v()LX/2jY;
    .locals 5

    .prologue
    .line 2385605
    new-instance v1, LX/GiL;

    invoke-direct {v1, p0, p0}, LX/GiL;-><init>(Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;)V

    .line 2385606
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2385607
    if-nez v0, :cond_0

    .line 2385608
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->k:LX/GiS;

    .line 2385609
    const-string v2, "ANDROID_GAMETIME_MATCHUP_TAB"

    const-string v3, "-1"

    sget-object v4, LX/GiR;->TAIL_LOAD:LX/GiR;

    invoke-virtual {v0, v2, v3, v4, v1}, LX/GiS;->a(Ljava/lang/String;Ljava/lang/String;LX/GiR;LX/GiJ;)LX/2jY;

    move-result-object v2

    move-object v0, v2

    .line 2385610
    :goto_0
    return-object v0

    .line 2385611
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2385612
    const-string v2, "load_type"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/GiR;

    iput-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->m:LX/GiR;

    .line 2385613
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2385614
    const-string v2, "page_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->o:Ljava/lang/String;

    .line 2385615
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2385616
    const-string v2, "reaction_surface"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->p:Ljava/lang/String;

    .line 2385617
    iget-object v0, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->k:LX/GiS;

    iget-object v2, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->p:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->o:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->m:LX/GiR;

    invoke-virtual {v0, v2, v3, v4, v1}, LX/GiS;->a(Ljava/lang/String;Ljava/lang/String;LX/GiR;LX/GiJ;)LX/2jY;

    move-result-object v0

    goto :goto_0
.end method

.method public final w()I
    .locals 2

    .prologue
    .line 2385602
    const-string v0, "ANDROID_GAMETIME_FRIEND_STORIES"

    iget-object v1, p0, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2385603
    const/4 v0, 0x3

    .line 2385604
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
