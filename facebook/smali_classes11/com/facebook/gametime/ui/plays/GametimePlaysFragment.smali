.class public Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Gi7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlaysRootPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1DS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1Db;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:LX/1Qq;

.field private i:LX/GiD;

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z

.field private l:Z

.field private m:Ljava/lang/String;

.field public n:LX/Gi5;

.field public o:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field private p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public q:Z

.field private r:LX/0g7;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2385438
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2385439
    return-void
.end method

.method public static a$redex0(Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2385430
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->q:Z

    .line 2385431
    new-instance v0, LX/Ggf;

    invoke-direct {v0}, LX/Ggf;-><init>()V

    move-object v0, v0

    .line 2385432
    const-string v1, "count"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "scoring_plays"

    iget-boolean v2, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->l:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v1, "page_id"

    iget-object v2, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "reaction_profile_image_size_medium"

    iget-object v2, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b164a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/Ggf;

    .line 2385433
    const-string v1, "afterCursor"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2385434
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2385435
    iget-object v1, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2385436
    iget-object v1, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->g:LX/1Ck;

    const-string v2, "GAMETIME_PLAYS"

    iget-object v3, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->i:LX/GiD;

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2385437
    return-void
.end method

.method public static b$redex0(Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;)LX/1Qq;
    .locals 5

    .prologue
    .line 2385416
    iget-object v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->h:LX/1Qq;

    if-eqz v0, :cond_0

    .line 2385417
    iget-object v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->h:LX/1Qq;

    .line 2385418
    :goto_0
    return-object v0

    .line 2385419
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2385420
    sget-object v1, LX/Gi8;->a:LX/Gi8;

    move-object v1, v1

    .line 2385421
    new-instance v2, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment$3;

    invoke-direct {v2, p0}, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment$3;-><init>(Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;)V

    iget-object v3, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2385422
    new-instance v4, LX/GiC;

    invoke-direct {v4, p0, v3}, LX/GiC;-><init>(Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    move-object v3, v4

    .line 2385423
    new-instance v4, LX/Gi6;

    invoke-direct {v4, v0, v1, v2, v3}, LX/Gi6;-><init>(Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;)V

    .line 2385424
    move-object v0, v4

    .line 2385425
    iget-object v1, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->e:LX/1DS;

    iget-object v2, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->d:LX/0Ot;

    iget-object v3, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->n:LX/Gi5;

    invoke-virtual {v1, v2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    .line 2385426
    iput-object v0, v1, LX/1Ql;->f:LX/1PW;

    .line 2385427
    move-object v0, v1

    .line 2385428
    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->h:LX/1Qq;

    .line 2385429
    iget-object v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->h:LX/1Qq;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 2385404
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v3, p0

    check-cast v3, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const-class v5, LX/Gi7;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/Gi7;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    const/16 v7, 0x2302

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v8

    check-cast v8, LX/1DS;

    invoke-static {v0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v9

    check-cast v9, LX/1Db;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    iput-object v4, v3, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->a:Landroid/content/Context;

    iput-object v5, v3, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->b:LX/Gi7;

    iput-object v6, v3, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->c:LX/0tX;

    iput-object v7, v3, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->d:LX/0Ot;

    iput-object v8, v3, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->e:LX/1DS;

    iput-object v9, v3, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->f:LX/1Db;

    iput-object v0, v3, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->g:LX/1Ck;

    .line 2385405
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2385406
    new-instance v0, LX/GiD;

    invoke-direct {v0, p0}, LX/GiD;-><init>(Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;)V

    iput-object v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->i:LX/GiD;

    .line 2385407
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->j:Ljava/lang/String;

    .line 2385408
    iput-boolean v2, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->k:Z

    .line 2385409
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2385410
    const-string v1, "scoring_plays"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->l:Z

    .line 2385411
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2385412
    const-string v1, "page_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->m:Ljava/lang/String;

    .line 2385413
    new-instance v0, LX/Gi5;

    invoke-direct {v0}, LX/Gi5;-><init>()V

    iput-object v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->n:LX/Gi5;

    .line 2385414
    iput-boolean v2, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->q:Z

    .line 2385415
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x2a

    const v1, -0x418f543

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2385378
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f03078d

    invoke-virtual {v0, v2, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2385379
    const v0, 0x102000a

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2385380
    new-instance v0, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/1P0;-><init>(Landroid/content/Context;)V

    .line 2385381
    iget-object v3, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2385382
    const v0, 0x7f0d142d

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->o:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2385383
    iget-object v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->o:Lcom/facebook/widget/FbSwipeRefreshLayout;

    new-instance v3, LX/Gi9;

    invoke-direct {v3, p0}, LX/Gi9;-><init>(Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;)V

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2385384
    new-instance v0, LX/0g7;

    iget-object v3, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-direct {v0, v3}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->r:LX/0g7;

    .line 2385385
    iget-object v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->r:LX/0g7;

    invoke-virtual {v0, v4}, LX/0g7;->b(Z)V

    .line 2385386
    iget-object v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->r:LX/0g7;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/0g7;->d(Z)V

    .line 2385387
    iget-object v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->r:LX/0g7;

    const v3, 0x1020004

    invoke-static {v2, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0g7;->f(Landroid/view/View;)V

    .line 2385388
    iget-object v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->r:LX/0g7;

    new-instance v3, LX/GiA;

    invoke-direct {v3, p0}, LX/GiA;-><init>(Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;)V

    invoke-virtual {v0, v3}, LX/0g7;->a(LX/0fx;)V

    .line 2385389
    iget-object v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->r:LX/0g7;

    invoke-static {p0}, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->b$redex0(Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;)LX/1Qq;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0g7;->a(Landroid/widget/ListAdapter;)V

    .line 2385390
    iget-object v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->r:LX/0g7;

    iget-object v3, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->f:LX/1Db;

    invoke-virtual {v3}, LX/1Db;->a()LX/1St;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0g7;->a(LX/1St;)V

    .line 2385391
    iget-object v0, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->j:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->a$redex0(Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;Ljava/lang/String;)V

    .line 2385392
    const/16 v0, 0x2b

    const v3, 0x54246839

    invoke-static {v5, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x873369d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2385401
    iget-object v1, p0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->h:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2385402
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2385403
    const/16 v1, 0x2b

    const v2, -0x22d7e61e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x4a45a6ff

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2385393
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2385394
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2385395
    if-eqz v0, :cond_0

    .line 2385396
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2385397
    const-string v3, "show_more_title"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2385398
    if-eqz v2, :cond_0

    .line 2385399
    invoke-interface {v0, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2385400
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x54f9b907

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
