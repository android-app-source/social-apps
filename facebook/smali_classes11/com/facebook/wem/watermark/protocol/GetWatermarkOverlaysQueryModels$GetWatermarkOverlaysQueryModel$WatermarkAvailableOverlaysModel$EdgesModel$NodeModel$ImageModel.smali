.class public final Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel$NodeModel$ImageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x32384fc8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel$NodeModel$ImageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel$NodeModel$ImageModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2320312
    const-class v0, Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel$NodeModel$ImageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2320311
    const-class v0, Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel$NodeModel$ImageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2320309
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2320310
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2320303
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2320304
    invoke-virtual {p0}, Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel$NodeModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2320305
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2320306
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2320307
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2320308
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2320300
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2320301
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2320302
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2320298
    iget-object v0, p0, Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel$NodeModel$ImageModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel$NodeModel$ImageModel;->e:Ljava/lang/String;

    .line 2320299
    iget-object v0, p0, Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel$NodeModel$ImageModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2320295
    new-instance v0, Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel$NodeModel$ImageModel;

    invoke-direct {v0}, Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel$NodeModel$ImageModel;-><init>()V

    .line 2320296
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2320297
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2320294
    const v0, -0x5f680c5b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2320293
    const v0, 0x437b93b

    return v0
.end method
