.class public Lcom/facebook/wem/watermark/WatermarkActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# static fields
.field public static final v:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:LX/G6J;

.field public B:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public C:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public p:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/G6f;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public r:LX/G6G;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public s:LX/G6Q;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:LX/3Rb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public u:Lcom/facebook/photos/editgallery/utils/FetchImageUtils;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final w:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel;",
            ">;"
        }
    .end annotation
.end field

.field public x:I

.field public y:Landroid/net/Uri;

.field public z:LX/5QV;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2319843
    const-class v0, Lcom/facebook/wem/watermark/WatermarkActivity;

    const-string v1, "growth"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/wem/watermark/WatermarkActivity;->v:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2319935
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2319936
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->w:Ljava/util/List;

    return-void
.end method

.method private a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 2319931
    new-instance v0, LX/G69;

    invoke-direct {v0, p0}, LX/G69;-><init>(Lcom/facebook/wem/watermark/WatermarkActivity;)V

    .line 2319932
    new-instance v1, LX/G6A;

    invoke-direct {v1, p0, v0}, LX/G6A;-><init>(Lcom/facebook/wem/watermark/WatermarkActivity;LX/9fc;)V

    .line 2319933
    iget-object v0, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->u:Lcom/facebook/photos/editgallery/utils/FetchImageUtils;

    invoke-virtual {v0, p0, p1, v1}, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->a(Landroid/content/Context;Landroid/net/Uri;LX/0Vd;)V

    .line 2319934
    return-void
.end method

.method private static a(Lcom/facebook/wem/watermark/WatermarkActivity;LX/0Or;LX/G6f;LX/G6G;LX/G6Q;LX/3Rb;Lcom/facebook/photos/editgallery/utils/FetchImageUtils;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/wem/watermark/WatermarkActivity;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/G6f;",
            "LX/G6G;",
            "LX/G6Q;",
            "LX/3Rb;",
            "Lcom/facebook/photos/editgallery/utils/FetchImageUtils;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2319930
    iput-object p1, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->p:LX/0Or;

    iput-object p2, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->q:LX/G6f;

    iput-object p3, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->r:LX/G6G;

    iput-object p4, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->s:LX/G6Q;

    iput-object p5, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->t:LX/3Rb;

    iput-object p6, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->u:Lcom/facebook/photos/editgallery/utils/FetchImageUtils;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 14

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/wem/watermark/WatermarkActivity;

    const/16 v1, 0x15e7

    invoke-static {v6, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v6}, LX/G6f;->b(LX/0QB;)LX/G6f;

    move-result-object v2

    check-cast v2, LX/G6f;

    new-instance v7, LX/G6G;

    invoke-static {v6}, LX/8GV;->b(LX/0QB;)LX/8GV;

    move-result-object v8

    check-cast v8, LX/8GV;

    const-class v9, LX/9fo;

    invoke-interface {v6, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/9fo;

    invoke-static {v6}, LX/5vm;->b(LX/0QB;)LX/5vm;

    move-result-object v10

    check-cast v10, LX/5vm;

    invoke-static {v6}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v11

    check-cast v11, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v6}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v12

    check-cast v12, LX/17Y;

    invoke-static {v6}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v13

    check-cast v13, LX/1Ck;

    invoke-direct/range {v7 .. v13}, LX/G6G;-><init>(LX/8GV;LX/9fo;LX/5vm;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/1Ck;)V

    move-object v3, v7

    check-cast v3, LX/G6G;

    invoke-static {v6}, LX/G6Q;->b(LX/0QB;)LX/G6Q;

    move-result-object v4

    check-cast v4, LX/G6Q;

    invoke-static {v6}, LX/3Rb;->a(LX/0QB;)LX/3Rb;

    move-result-object v5

    check-cast v5, LX/3Rb;

    invoke-static {v6}, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->b(LX/0QB;)Lcom/facebook/photos/editgallery/utils/FetchImageUtils;

    move-result-object v6

    check-cast v6, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;

    invoke-static/range {v0 .. v6}, Lcom/facebook/wem/watermark/WatermarkActivity;->a(Lcom/facebook/wem/watermark/WatermarkActivity;LX/0Or;LX/G6f;LX/G6G;LX/G6Q;LX/3Rb;Lcom/facebook/photos/editgallery/utils/FetchImageUtils;)V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2319923
    invoke-direct {p0}, Lcom/facebook/wem/watermark/WatermarkActivity;->l()V

    .line 2319924
    const v0, 0x7f0d31a9

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->C:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2319925
    const v0, 0x7f0d31aa

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->B:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2319926
    invoke-direct {p0}, Lcom/facebook/wem/watermark/WatermarkActivity;->m()V

    .line 2319927
    invoke-direct {p0}, Lcom/facebook/wem/watermark/WatermarkActivity;->n()V

    .line 2319928
    invoke-direct {p0}, Lcom/facebook/wem/watermark/WatermarkActivity;->o()V

    .line 2319929
    return-void
.end method

.method public static b(Lcom/facebook/wem/watermark/WatermarkActivity;I)V
    .locals 6

    .prologue
    .line 2319916
    iget-object v1, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->q:LX/G6f;

    iget-object v0, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->w:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel;->a()Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    iget v2, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->x:I

    new-instance v3, LX/G6D;

    invoke-direct {v3, p0}, LX/G6D;-><init>(Lcom/facebook/wem/watermark/WatermarkActivity;)V

    invoke-static {v3}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object v3

    .line 2319917
    new-instance v4, LX/B4f;

    invoke-direct {v4}, LX/B4f;-><init>()V

    .line 2319918
    const-string v5, "image_overlay_id"

    invoke-virtual {v4, v5, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2319919
    const-string v5, "image_high_width"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2319920
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    sget-object v5, LX/0zS;->a:LX/0zS;

    invoke-virtual {v4, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    .line 2319921
    iget-object v5, v1, LX/G6f;->a:LX/1Ck;

    const-string p0, "fetch_overlay"

    iget-object p1, v1, LX/G6f;->c:LX/0tX;

    invoke-virtual {p1, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    invoke-virtual {v5, p0, v4, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2319922
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 2319911
    const v0, 0x7f0d31a7

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2319912
    if-eqz v0, :cond_0

    .line 2319913
    const v1, 0x7f081568

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 2319914
    new-instance v1, LX/G6B;

    invoke-direct {v1, p0}, LX/G6B;-><init>(Lcom/facebook/wem/watermark/WatermarkActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 2319915
    :cond_0
    return-void
.end method

.method private m()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2319902
    const v0, 0x7f0d31ab

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 2319903
    const/4 v1, 0x1

    .line 2319904
    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 2319905
    new-instance v1, LX/1P1;

    invoke-direct {v1, v2, v2}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2319906
    new-instance v2, LX/G6C;

    invoke-direct {v2, p0, v0}, LX/G6C;-><init>(Lcom/facebook/wem/watermark/WatermarkActivity;Landroid/support/v7/widget/RecyclerView;)V

    .line 2319907
    invoke-direct {p0}, Lcom/facebook/wem/watermark/WatermarkActivity;->q()I

    move-result v3

    .line 2319908
    new-instance v4, LX/G6J;

    iget-object v5, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->w:Ljava/util/List;

    sget-object v6, Lcom/facebook/wem/watermark/WatermarkActivity;->v:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v7, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->t:LX/3Rb;

    iget-object v1, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->p:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v7, v1, v3, v3}, LX/3Rb;->a(Ljava/lang/String;II)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v4, v5, v2, v6, v1}, LX/G6J;-><init>(Ljava/util/List;Landroid/view/View$OnClickListener;Lcom/facebook/common/callercontext/CallerContext;Landroid/net/Uri;)V

    iput-object v4, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->A:LX/G6J;

    .line 2319909
    iget-object v1, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->A:LX/G6J;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2319910
    return-void
.end method

.method private n()V
    .locals 6

    .prologue
    .line 2319896
    iget-object v0, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->q:LX/G6f;

    invoke-direct {p0}, Lcom/facebook/wem/watermark/WatermarkActivity;->q()I

    move-result v1

    new-instance v2, LX/G6E;

    invoke-direct {v2, p0}, LX/G6E;-><init>(Lcom/facebook/wem/watermark/WatermarkActivity;)V

    invoke-static {v2}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object v2

    .line 2319897
    new-instance v3, LX/G6W;

    invoke-direct {v3}, LX/G6W;-><init>()V

    .line 2319898
    const-string v4, "image_high_width"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2319899
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->a:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    .line 2319900
    iget-object v4, v0, LX/G6f;->a:LX/1Ck;

    const-string v5, "fetch_overlays"

    iget-object p0, v0, LX/G6f;->c:LX/0tX;

    invoke-virtual {p0, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    invoke-virtual {v4, v5, v3, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2319901
    return-void
.end method

.method private o()V
    .locals 3

    .prologue
    .line 2319893
    const v0, 0x7f0d31a8

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 2319894
    invoke-virtual {v0}, Landroid/widget/ScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, LX/G6F;

    invoke-direct {v2, p0, v0}, LX/G6F;-><init>(Lcom/facebook/wem/watermark/WatermarkActivity;Landroid/widget/ScrollView;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2319895
    return-void
.end method

.method private p()I
    .locals 3

    .prologue
    .line 2319889
    invoke-virtual {p0}, Lcom/facebook/wem/watermark/WatermarkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0d8e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    .line 2319890
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 2319891
    invoke-virtual {p0}, Lcom/facebook/wem/watermark/WatermarkActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 2319892
    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int v0, v1, v0

    return v0
.end method

.method private q()I
    .locals 2

    .prologue
    .line 2319888
    invoke-virtual {p0}, Lcom/facebook/wem/watermark/WatermarkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0d8d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2319872
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2319873
    const v0, 0x7f031615

    invoke-virtual {p0, v0}, Lcom/facebook/wem/watermark/WatermarkActivity;->setContentView(I)V

    .line 2319874
    invoke-static {p0, p0}, Lcom/facebook/wem/watermark/WatermarkActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2319875
    invoke-direct {p0}, Lcom/facebook/wem/watermark/WatermarkActivity;->b()V

    .line 2319876
    invoke-direct {p0}, Lcom/facebook/wem/watermark/WatermarkActivity;->p()I

    move-result v0

    iput v0, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->x:I

    .line 2319877
    iget-object v1, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->t:LX/3Rb;

    iget-object v0, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget v2, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->x:I

    iget v3, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->x:I

    invoke-virtual {v1, v0, v2, v3}, LX/3Rb;->a(Ljava/lang/String;II)Landroid/net/Uri;

    move-result-object v0

    .line 2319878
    invoke-direct {p0, v0}, Lcom/facebook/wem/watermark/WatermarkActivity;->a(Landroid/net/Uri;)V

    .line 2319879
    invoke-virtual {p0}, Lcom/facebook/wem/watermark/WatermarkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2319880
    iget-object v1, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->s:LX/G6Q;

    const-string v2, "photo_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2319881
    iget-object v3, v1, LX/G6Q;->b:Ljava/util/HashMap;

    const-string v4, "old_profile_picture"

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2319882
    iget-object v3, v1, LX/G6Q;->b:Ljava/util/HashMap;

    const-string v4, "photo_selector"

    const-string p1, "existing"

    invoke-virtual {v3, v4, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2319883
    iget-object v1, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->s:LX/G6Q;

    .line 2319884
    const-string v2, "fb4a_watermark_viewed_landing_page"

    iget-object v3, v1, LX/G6Q;->b:Ljava/util/HashMap;

    invoke-static {v1, v2, v3}, LX/G6Q;->a(LX/G6Q;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 2319885
    const-string v1, "show_picker"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2319886
    iget-object v0, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->r:LX/G6G;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, LX/G6G;->a(Landroid/app/Activity;I)V

    .line 2319887
    :cond_0
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 2319853
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2319854
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-nez p3, :cond_1

    .line 2319855
    :cond_0
    :goto_0
    return-void

    .line 2319856
    :cond_1
    if-ne p1, v2, :cond_0

    .line 2319857
    const-string v0, "extra_media_items"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2319858
    const-string v0, "extra_media_items"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2319859
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 2319860
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 2319861
    iget-object v1, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->s:LX/G6Q;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v2

    .line 2319862
    iget-object v3, v2, Lcom/facebook/ipc/media/data/MediaData;->mId:Ljava/lang/String;

    move-object v2, v3

    .line 2319863
    const-string v3, "camera_roll"

    invoke-virtual {v1, v2, v3}, LX/G6Q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2319864
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    .line 2319865
    :goto_1
    invoke-direct {p0, v0}, Lcom/facebook/wem/watermark/WatermarkActivity;->a(Landroid/net/Uri;)V

    goto :goto_0

    .line 2319866
    :cond_2
    const-string v0, "photo"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2319867
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "photo"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2319868
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2319869
    iget-object v1, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->s:LX/G6Q;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v2

    const-string v3, "existing"

    invoke-virtual {v1, v2, v3}, LX/G6Q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2319870
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    .line 2319871
    :cond_3
    iget-object v0, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->s:LX/G6Q;

    const-string v1, "Returned from changing photo without a valid one"

    invoke-virtual {v0, v1}, LX/G6Q;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onChangePhotoClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2319851
    iget-object v0, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->r:LX/G6G;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, LX/G6G;->a(Landroid/app/Activity;I)V

    .line 2319852
    return-void
.end method

.method public onSaveClick(Landroid/view/View;)V
    .locals 12

    .prologue
    .line 2319844
    new-instance v0, LX/G68;

    invoke-direct {v0, p0}, LX/G68;-><init>(Lcom/facebook/wem/watermark/WatermarkActivity;)V

    .line 2319845
    iget-object v1, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->r:LX/G6G;

    iget-object v2, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->y:Landroid/net/Uri;

    iget-object v3, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->z:LX/5QV;

    .line 2319846
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->newBuilder()Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v4

    invoke-static {v3}, LX/B5P;->a(LX/5QV;)Lcom/facebook/photos/creativeediting/model/StickerParams;

    move-result-object v5

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setFrameOverlayItems(LX/0Px;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v6

    .line 2319847
    iget-object v10, v1, LX/G6G;->g:LX/1Ck;

    const-string v11, "apply_overlay"

    iget-object v4, v1, LX/G6G;->b:LX/8GV;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object v8, v2

    invoke-virtual/range {v4 .. v9}, LX/8GV;->b(FLcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/0Px;Landroid/net/Uri;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    invoke-virtual {v10, v11, v4, v0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2319848
    iget-object v0, p0, Lcom/facebook/wem/watermark/WatermarkActivity;->s:LX/G6Q;

    .line 2319849
    const-string v1, "fb4a_watermark_set_new_photo"

    iget-object v2, v0, LX/G6Q;->b:Ljava/util/HashMap;

    invoke-static {v0, v1, v2}, LX/G6Q;->a(LX/G6Q;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 2319850
    return-void
.end method
