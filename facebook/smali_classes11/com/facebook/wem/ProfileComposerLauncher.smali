.class public Lcom/facebook/wem/ProfileComposerLauncher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Ljava/lang/Object;


# instance fields
.field private final b:LX/1HI;

.field public final c:LX/03V;

.field public final d:LX/0Uh;

.field private final e:LX/1Kf;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Er;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/74n;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0TD;

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G4a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2319466
    const-class v0, Lcom/facebook/wem/ProfileComposerLauncher;

    const-string v1, "composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/wem/ProfileComposerLauncher;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/1HI;LX/03V;LX/0Uh;LX/1Kf;LX/0Ot;LX/0Ot;LX/0TD;LX/0Or;)V
    .locals 0
    .param p7    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1HI;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/1Kf;",
            "LX/0Ot",
            "<",
            "LX/1Er;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/74n;",
            ">;",
            "LX/0TD;",
            "LX/0Or",
            "<",
            "LX/G4a;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2319467
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2319468
    iput-object p1, p0, Lcom/facebook/wem/ProfileComposerLauncher;->b:LX/1HI;

    .line 2319469
    iput-object p2, p0, Lcom/facebook/wem/ProfileComposerLauncher;->c:LX/03V;

    .line 2319470
    iput-object p5, p0, Lcom/facebook/wem/ProfileComposerLauncher;->f:LX/0Ot;

    .line 2319471
    iput-object p7, p0, Lcom/facebook/wem/ProfileComposerLauncher;->h:LX/0TD;

    .line 2319472
    iput-object p3, p0, Lcom/facebook/wem/ProfileComposerLauncher;->d:LX/0Uh;

    .line 2319473
    iput-object p6, p0, Lcom/facebook/wem/ProfileComposerLauncher;->g:LX/0Ot;

    .line 2319474
    iput-object p4, p0, Lcom/facebook/wem/ProfileComposerLauncher;->e:LX/1Kf;

    .line 2319475
    iput-object p8, p0, Lcom/facebook/wem/ProfileComposerLauncher;->i:LX/0Or;

    .line 2319476
    return-void
.end method

.method public static a(Lcom/facebook/wem/ProfileComposerLauncher;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;LX/5SB;LX/BQ1;)V
    .locals 8

    .prologue
    .line 2319477
    invoke-static {p1}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v0

    .line 2319478
    iget-object v1, p0, Lcom/facebook/wem/ProfileComposerLauncher;->b:LX/1HI;

    sget-object v2, Lcom/facebook/wem/ProfileComposerLauncher;->a:Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v7

    .line 2319479
    new-instance v0, LX/G5t;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, LX/G5t;-><init>(Lcom/facebook/wem/ProfileComposerLauncher;Landroid/os/Bundle;Landroid/app/Activity;LX/5SB;LX/BQ1;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/wem/ProfileComposerLauncher;->h:LX/0TD;

    invoke-interface {v7, v0, v1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 2319480
    return-void
.end method

.method public static a$redex0(Lcom/facebook/wem/ProfileComposerLauncher;Landroid/graphics/Bitmap;)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 2319481
    iget-object v0, p0, Lcom/facebook/wem/ProfileComposerLauncher;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Er;

    const-string v1, "profile_composer"

    const-string v2, ".png"

    sget-object v3, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, v1, v2, v3}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    .line 2319482
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 2319483
    :try_start_0
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {p1, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 2319484
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V

    .line 2319485
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2319486
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/wem/ProfileComposerLauncher;Landroid/os/Bundle;Landroid/app/Activity;LX/5SB;LX/BQ1;LX/0Px;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Landroid/app/Activity;",
            "LX/5SB;",
            "LX/BQ1;",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2319487
    iget-object v0, p0, Lcom/facebook/wem/ProfileComposerLauncher;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G4a;

    .line 2319488
    iget-wide v5, p3, LX/5SB;->b:J

    move-wide v2, v5

    .line 2319489
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4}, LX/BQ1;->N()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p4}, LX/BQ1;->Y()LX/2rX;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/G4a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2rX;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 2319490
    if-eqz p1, :cond_0

    const-string v1, "initial_text"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2319491
    const-string v1, "initial_text"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2319492
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2319493
    invoke-static {v1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2319494
    const-string v2, "+"

    const-string v3, " "

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 2319495
    invoke-static {v1}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialText(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2319496
    :cond_0
    if-eqz p5, :cond_1

    .line 2319497
    invoke-virtual {v0, p5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAttachments(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2319498
    :cond_1
    iget-object v1, p0, Lcom/facebook/wem/ProfileComposerLauncher;->e:LX/1Kf;

    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    const/16 v3, 0x6dc

    invoke-interface {v1, v2, v0, v3, p2}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2319499
    return-void
.end method
