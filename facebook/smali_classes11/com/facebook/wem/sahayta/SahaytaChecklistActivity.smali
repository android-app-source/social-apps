.class public Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field public p:LX/G62;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/G63;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:Landroid/view/View;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/CheckBox;

.field private v:Landroid/widget/CheckBox;

.field private w:Landroid/widget/CheckBox;

.field private x:Landroid/widget/CheckBox;

.field public y:Ljava/lang/String;

.field public z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2319580
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;LX/G62;LX/G63;LX/17W;)V
    .locals 0

    .prologue
    .line 2319579
    iput-object p1, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->p:LX/G62;

    iput-object p2, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->q:LX/G63;

    iput-object p3, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->r:LX/17W;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;

    invoke-static {v2}, LX/G62;->b(LX/0QB;)LX/G62;

    move-result-object v0

    check-cast v0, LX/G62;

    invoke-static {v2}, LX/G63;->b(LX/0QB;)LX/G63;

    move-result-object v1

    check-cast v1, LX/G63;

    invoke-static {v2}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v2

    check-cast v2, LX/17W;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->a(Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;LX/G62;LX/G63;LX/17W;)V

    return-void
.end method

.method private a(Ljava/util/Map;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2319572
    move v1, v0

    move v2, v0

    .line 2319573
    :goto_0
    const/4 v0, 0x3

    if-ge v1, v0, :cond_0

    .line 2319574
    invoke-direct {p0, v1}, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->b(I)Landroid/widget/CheckBox;

    move-result-object v3

    .line 2319575
    invoke-virtual {v3}, Landroid/widget/CheckBox;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {p1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2319576
    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    or-int/2addr v2, v0

    .line 2319577
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2319578
    :cond_0
    return v2
.end method

.method private b(I)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 2319571
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->u:Landroid/widget/CheckBox;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->v:Landroid/widget/CheckBox;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->w:Landroid/widget/CheckBox;

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2319567
    invoke-static {p1}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    .line 2319568
    const v0, 0x7f0d0936

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    .line 2319569
    invoke-static {v1}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2319570
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2319562
    const v0, 0x7f0d0933

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2319563
    if-eqz v0, :cond_0

    .line 2319564
    const v1, 0x7f08155b

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 2319565
    new-instance v1, LX/G5v;

    invoke-direct {v1, p0}, LX/G5v;-><init>(Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 2319566
    :cond_0
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2319522
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2319523
    const v0, 0x7f03028a

    invoke-virtual {p0, v0}, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->setContentView(I)V

    .line 2319524
    invoke-static {p0, p0}, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2319525
    invoke-virtual {p0}, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 2319526
    const-string v0, "short_name"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->y:Ljava/lang/String;

    .line 2319527
    const-string v0, "profile_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->z:Ljava/lang/String;

    .line 2319528
    iget-object v0, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->y:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->c(Ljava/lang/String;)V

    .line 2319529
    iget-object v0, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->z:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->b(Ljava/lang/String;)V

    .line 2319530
    const v0, 0x7f0d0937

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2319531
    const v1, 0x7f08155d

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->y:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2319532
    const v0, 0x7f0d0934

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->s:Landroid/view/View;

    .line 2319533
    const v0, 0x7f0d0935

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->t:Landroid/widget/TextView;

    .line 2319534
    const v0, 0x7f0d0938

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->u:Landroid/widget/CheckBox;

    .line 2319535
    const v0, 0x7f0d0939

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->v:Landroid/widget/CheckBox;

    .line 2319536
    const v0, 0x7f0d093a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->w:Landroid/widget/CheckBox;

    .line 2319537
    const v0, 0x7f0d093b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->x:Landroid/widget/CheckBox;

    .line 2319538
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x2

    const v1, 0x496a28af

    invoke-static {v0, v5, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2319539
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2319540
    invoke-direct {p0, v0}, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->a(Ljava/util/Map;)Z

    move-result v2

    .line 2319541
    iget-object v3, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->x:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    .line 2319542
    if-eqz v2, :cond_0

    if-nez v3, :cond_2

    .line 2319543
    :cond_0
    if-eqz v2, :cond_1

    .line 2319544
    const v0, 0x7f081564

    invoke-virtual {p0, v0}, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2319545
    iget-object v2, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->p:LX/G62;

    .line 2319546
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "checklist_disclaimer_error"

    invoke-direct {v3, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2319547
    invoke-static {v2, v3}, LX/G62;->a(LX/G62;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2319548
    :goto_0
    iget-object v2, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->t:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2319549
    iget-object v0, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->s:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2319550
    const v0, -0x3a852356

    invoke-static {v0, v1}, LX/02F;->a(II)V

    .line 2319551
    :goto_1
    return-void

    .line 2319552
    :cond_1
    const v0, 0x7f081563

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->y:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p0, v0, v2}, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2319553
    iget-object v2, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->p:LX/G62;

    .line 2319554
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "checklist_suggestions_error"

    invoke-direct {v3, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2319555
    invoke-static {v2, v3}, LX/G62;->a(LX/G62;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2319556
    goto :goto_0

    .line 2319557
    :cond_2
    invoke-virtual {p1, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 2319558
    iget-object v2, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->s:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2319559
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2319560
    iget-object v2, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->q:LX/G63;

    iget-object v3, p0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->z:Ljava/lang/String;

    new-instance v4, LX/G5w;

    invoke-direct {v4, p0, p1, v0}, LX/G5w;-><init>(Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;Landroid/view/View;Ljava/lang/String;)V

    invoke-static {v4}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object v4

    invoke-virtual {v2, v3, v0, v4}, LX/G63;->a(Ljava/lang/String;Ljava/lang/String;LX/0Ve;)V

    .line 2319561
    const v0, -0x3dd40659

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_1
.end method
