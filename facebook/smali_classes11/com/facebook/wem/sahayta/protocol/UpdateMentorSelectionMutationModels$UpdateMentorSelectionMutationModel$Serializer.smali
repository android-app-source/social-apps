.class public final Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2319698
    const-class v0, Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel;

    new-instance v1, Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2319699
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2319700
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2319701
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2319702
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2319703
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2319704
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2319705
    if-eqz p0, :cond_0

    .line 2319706
    const-string p2, "mentor_selection"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2319707
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2319708
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2319709
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2319710
    check-cast p1, Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel$Serializer;->a(Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
