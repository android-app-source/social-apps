.class public final Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x75f3809a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2319711
    const-class v0, Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2319724
    const-class v0, Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2319722
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2319723
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2319720
    iget-object v0, p0, Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel;->e:Ljava/lang/String;

    .line 2319721
    iget-object v0, p0, Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2319725
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2319726
    invoke-direct {p0}, Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2319727
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2319728
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2319729
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2319730
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2319717
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2319718
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2319719
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2319714
    new-instance v0, Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel;

    invoke-direct {v0}, Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel;-><init>()V

    .line 2319715
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2319716
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2319713
    const v0, 0x554f5095

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2319712
    const v0, 0xdce1216    # 1.2700084E-30f

    return v0
.end method
