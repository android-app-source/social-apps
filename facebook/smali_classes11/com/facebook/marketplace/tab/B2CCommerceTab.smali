.class public Lcom/facebook/marketplace/tab/B2CCommerceTab;
.super Lcom/facebook/marketplace/tab/MarketplaceTab;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/marketplace/tab/B2CCommerceTab;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:Lcom/facebook/marketplace/tab/B2CCommerceTab;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2210191
    new-instance v0, Lcom/facebook/marketplace/tab/B2CCommerceTab;

    invoke-direct {v0}, Lcom/facebook/marketplace/tab/B2CCommerceTab;-><init>()V

    sput-object v0, Lcom/facebook/marketplace/tab/B2CCommerceTab;->l:Lcom/facebook/marketplace/tab/B2CCommerceTab;

    .line 2210192
    new-instance v0, LX/FCK;

    invoke-direct {v0}, LX/FCK;-><init>()V

    sput-object v0, Lcom/facebook/marketplace/tab/B2CCommerceTab;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 12

    .prologue
    const/4 v8, 0x0

    const v6, 0x63000e

    .line 2210188
    sget-object v0, LX/0ax;->do:Ljava/lang/String;

    const-string v1, "/marketplace?ref=tab"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/0cQ;->FACEWEB_FRAGMENT:LX/0cQ;

    const v3, 0x7f021073

    const/4 v4, 0x0

    const-string v5, "b2c_commerce"

    const v10, 0x7f080a2b

    const v11, 0x7f0d0034

    move-object v0, p0

    move v7, v6

    move-object v9, v8

    invoke-direct/range {v0 .. v11}, Lcom/facebook/marketplace/tab/MarketplaceTab;-><init>(Ljava/lang/String;LX/0cQ;IZLjava/lang/String;IILjava/lang/String;Ljava/lang/String;II)V

    .line 2210189
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2210190
    const-string v0, "B2C_Commerce"

    return-object v0
.end method
