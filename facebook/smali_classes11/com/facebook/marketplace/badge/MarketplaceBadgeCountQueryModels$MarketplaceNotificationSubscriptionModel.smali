.class public final Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x36842413
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2412097
    const-class v0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2412096
    const-class v0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2412094
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2412095
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2412088
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2412089
    invoke-virtual {p0}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel;->a()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2412090
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2412091
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2412092
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2412093
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2412080
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2412081
    invoke-virtual {p0}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel;->a()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2412082
    invoke-virtual {p0}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel;->a()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;

    .line 2412083
    invoke-virtual {p0}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel;->a()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2412084
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel;

    .line 2412085
    iput-object v0, v1, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel;->e:Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;

    .line 2412086
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2412087
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2412078
    iget-object v0, p0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel;->e:Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;

    iput-object v0, p0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel;->e:Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;

    .line 2412079
    iget-object v0, p0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel;->e:Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2412075
    new-instance v0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel;

    invoke-direct {v0}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel;-><init>()V

    .line 2412076
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2412077
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2412074
    const v0, 0x7dfc6777

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2412073
    const v0, 0x71bb039

    return v0
.end method
