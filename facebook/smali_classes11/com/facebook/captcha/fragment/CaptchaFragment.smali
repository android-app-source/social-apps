.class public Lcom/facebook/captcha/fragment/CaptchaFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final f:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/8tu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/GVe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/2Bt;

.field public h:Lcom/facebook/ui/search/SearchEditText;

.field public i:Landroid/widget/Button;

.field public j:Landroid/view/View;

.field public k:Landroid/widget/TextView;

.field public l:Landroid/widget/TextView;

.field public m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/Integer;

.field private p:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2359672
    const-class v0, Lcom/facebook/captcha/fragment/CaptchaFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/captcha/fragment/CaptchaFragment;->f:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2359668
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2359669
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->n:Ljava/lang/String;

    .line 2359670
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->o:Ljava/lang/Integer;

    .line 2359671
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->p:Ljava/lang/Integer;

    return-void
.end method

.method public static k(Lcom/facebook/captcha/fragment/CaptchaFragment;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2359660
    const/4 v2, 0x0

    .line 2359661
    iget-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2359662
    iget-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2359663
    iget-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2359664
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2359665
    const-string v0, "requestCaptchaParamsKey"

    invoke-virtual {v2, v0, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2359666
    iget-object v6, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->e:LX/1Ck;

    iget-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->b:LX/0aG;

    const-string v1, "request_captcha"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v4, Lcom/facebook/captcha/fragment/CaptchaFragment;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x34d6a710    # -1.1098352E7f

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    new-instance v1, LX/GVZ;

    invoke-direct {v1, p0}, LX/GVZ;-><init>(Lcom/facebook/captcha/fragment/CaptchaFragment;)V

    invoke-virtual {v6, v7, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2359667
    return-void
.end method

.method public static n(Lcom/facebook/captcha/fragment/CaptchaFragment;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2359656
    iget-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2359657
    iget-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2359658
    iget-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2359659
    return-void
.end method

.method public static o(Lcom/facebook/captcha/fragment/CaptchaFragment;)V
    .locals 8

    .prologue
    .line 2359641
    iget-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2359642
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2359643
    :goto_0
    return-void

    .line 2359644
    :cond_0
    iget-object v1, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->n:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 2359645
    invoke-static {p0}, Lcom/facebook/captcha/fragment/CaptchaFragment;->k(Lcom/facebook/captcha/fragment/CaptchaFragment;)V

    goto :goto_0

    .line 2359646
    :cond_1
    iget-object v1, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->p:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->p:Ljava/lang/Integer;

    .line 2359647
    iget-object v1, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->c:LX/GVe;

    iget-object v2, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->p:Ljava/lang/Integer;

    .line 2359648
    iget-object v3, v1, LX/GVe;->a:LX/0Zb;

    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v5, LX/GVd;->SUBMIT_CAPTCHA_INPUT_CLICKED:LX/GVd;

    invoke-virtual {v5}, LX/GVd;->getEventName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "captcha"

    .line 2359649
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2359650
    move-object v4, v4

    .line 2359651
    const-string v5, "numSubmitClicks"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2359652
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2359653
    new-instance v1, Lcom/facebook/captcha/protocol/SolveCaptchaMethod$Params;

    iget-object v3, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->n:Ljava/lang/String;

    invoke-direct {v1, v3, v0}, Lcom/facebook/captcha/protocol/SolveCaptchaMethod$Params;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2359654
    const-string v0, "solveCaptchaParamsKey"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2359655
    iget-object v6, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->e:LX/1Ck;

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->b:LX/0aG;

    const-string v1, "solve_captcha"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v4, Lcom/facebook/captcha/fragment/CaptchaFragment;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x72c2dae0

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    new-instance v1, LX/GVa;

    invoke-direct {v1, p0}, LX/GVa;-><init>(Lcom/facebook/captcha/fragment/CaptchaFragment;)V

    invoke-virtual {v6, v7, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method

.method public static p(Lcom/facebook/captcha/fragment/CaptchaFragment;)V
    .locals 3

    .prologue
    .line 2359673
    iget-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->a()V

    .line 2359674
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f083556

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f083557

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080036

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/GVb;

    invoke-direct {v2, p0}, LX/GVb;-><init>(Lcom/facebook/captcha/fragment/CaptchaFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2359675
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2359638
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2359639
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/captcha/fragment/CaptchaFragment;

    invoke-static {v0}, LX/8tu;->a(LX/0QB;)LX/8tu;

    move-result-object v3

    check-cast v3, LX/8tu;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {v0}, LX/GVe;->a(LX/0QB;)LX/GVe;

    move-result-object v5

    check-cast v5, LX/GVe;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object p1

    check-cast p1, LX/0kL;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    iput-object v3, v2, Lcom/facebook/captcha/fragment/CaptchaFragment;->a:LX/8tu;

    iput-object v4, v2, Lcom/facebook/captcha/fragment/CaptchaFragment;->b:LX/0aG;

    iput-object v5, v2, Lcom/facebook/captcha/fragment/CaptchaFragment;->c:LX/GVe;

    iput-object p1, v2, Lcom/facebook/captcha/fragment/CaptchaFragment;->d:LX/0kL;

    iput-object v0, v2, Lcom/facebook/captcha/fragment/CaptchaFragment;->e:LX/1Ck;

    .line 2359640
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2359631
    invoke-static {p0}, Lcom/facebook/captcha/fragment/CaptchaFragment;->p(Lcom/facebook/captcha/fragment/CaptchaFragment;)V

    .line 2359632
    iget-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->c:LX/GVe;

    .line 2359633
    iget-object v1, v0, LX/GVe;->a:LX/0Zb;

    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p0, LX/GVd;->CAPTCHA_FETCH_REQUEST_FAILED:LX/GVd;

    invoke-virtual {p0}, LX/GVd;->getEventName()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v2, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "captcha"

    .line 2359634
    iput-object p0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2359635
    move-object v2, v2

    .line 2359636
    const/4 p0, 0x1

    invoke-interface {v1, v2, p0}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2359637
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/16 v2, 0x2a

    const v3, -0xdf8d01d

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2359627
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/app/Activity;)V

    .line 2359628
    :try_start_0
    move-object v0, p1

    check-cast v0, LX/2Bt;

    move-object v1, v0

    iput-object v1, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->g:LX/2Bt;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2359629
    const v1, 0x21909716

    invoke-static {v1, v2}, LX/02F;->f(II)V

    return-void

    .line 2359630
    :catch_0
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " must implement OnCaptchaSolvedListener"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    const v3, -0x6c67df10

    invoke-static {v3, v2}, LX/02F;->f(II)V

    throw v1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x5e96e400

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2359626
    const v1, 0x7f03023a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x614cde9

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x31a2b0eb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2359621
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2359622
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2359623
    if-nez v1, :cond_0

    .line 2359624
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x6fe9cb98

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2359625
    :cond_0
    const v2, 0x7f08354e

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2359598
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2359599
    const v0, 0x7f0d089a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2359600
    const v0, 0x7f0d089d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/search/SearchEditText;

    iput-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    .line 2359601
    const v0, 0x7f0d089e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->i:Landroid/widget/Button;

    .line 2359602
    const v0, 0x7f0d089f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->k:Landroid/widget/TextView;

    .line 2359603
    const v0, 0x7f0d089c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->l:Landroid/widget/TextView;

    .line 2359604
    const v0, 0x7f0d089b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->j:Landroid/view/View;

    .line 2359605
    new-instance v0, LX/47x;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2359606
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083553

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "{CAPTCHA_RETRY_TOKEN}"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2359607
    const-string v1, "{CAPTCHA_RETRY_TOKEN}"

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083554

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/GVV;

    invoke-direct {v3, p0}, LX/GVV;-><init>(Lcom/facebook/captcha/fragment/CaptchaFragment;)V

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2359608
    iget-object v1, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->k:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->a:LX/8tu;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2359609
    iget-object v1, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->k:Landroid/widget/TextView;

    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2359610
    iget-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    new-instance v1, LX/GVW;

    invoke-direct {v1, p0}, LX/GVW;-><init>(Lcom/facebook/captcha/fragment/CaptchaFragment;)V

    .line 2359611
    iput-object v1, v0, Lcom/facebook/ui/search/SearchEditText;->f:LX/7HK;

    .line 2359612
    iget-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    new-instance v1, LX/GVX;

    invoke-direct {v1, p0}, LX/GVX;-><init>(Lcom/facebook/captcha/fragment/CaptchaFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2359613
    iget-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->i:Landroid/widget/Button;

    new-instance v1, LX/GVY;

    invoke-direct {v1, p0}, LX/GVY;-><init>(Lcom/facebook/captcha/fragment/CaptchaFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2359614
    invoke-static {p0}, Lcom/facebook/captcha/fragment/CaptchaFragment;->k(Lcom/facebook/captcha/fragment/CaptchaFragment;)V

    .line 2359615
    iget-object v0, p0, Lcom/facebook/captcha/fragment/CaptchaFragment;->c:LX/GVe;

    .line 2359616
    iget-object v1, v0, LX/GVe;->a:LX/0Zb;

    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/GVd;->CAPTCHA_SHOWN:LX/GVd;

    invoke-virtual {v3}, LX/GVd;->getEventName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "captcha"

    .line 2359617
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2359618
    move-object v2, v2

    .line 2359619
    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2359620
    return-void
.end method
