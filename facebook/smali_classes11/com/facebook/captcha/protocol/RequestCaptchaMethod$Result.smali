.class public final Lcom/facebook/captcha/protocol/RequestCaptchaMethod$Result;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/captcha/protocol/RequestCaptchaMethod_ResultDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/captcha/protocol/RequestCaptchaMethod$Result;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2359725
    const-class v0, Lcom/facebook/captcha/protocol/RequestCaptchaMethod_ResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2359726
    new-instance v0, LX/GVf;

    invoke-direct {v0}, LX/GVf;-><init>()V

    sput-object v0, Lcom/facebook/captcha/protocol/RequestCaptchaMethod$Result;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2359727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2359728
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/captcha/protocol/RequestCaptchaMethod$Result;->a:Ljava/lang/String;

    .line 2359729
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/captcha/protocol/RequestCaptchaMethod$Result;->b:Ljava/lang/String;

    .line 2359730
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2359721
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2359722
    iput-object p1, p0, Lcom/facebook/captcha/protocol/RequestCaptchaMethod$Result;->a:Ljava/lang/String;

    .line 2359723
    iput-object p2, p0, Lcom/facebook/captcha/protocol/RequestCaptchaMethod$Result;->b:Ljava/lang/String;

    .line 2359724
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2359720
    iget-object v0, p0, Lcom/facebook/captcha/protocol/RequestCaptchaMethod$Result;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2359715
    iget-object v0, p0, Lcom/facebook/captcha/protocol/RequestCaptchaMethod$Result;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2359719
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2359716
    iget-object v0, p0, Lcom/facebook/captcha/protocol/RequestCaptchaMethod$Result;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2359717
    iget-object v0, p0, Lcom/facebook/captcha/protocol/RequestCaptchaMethod$Result;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2359718
    return-void
.end method
