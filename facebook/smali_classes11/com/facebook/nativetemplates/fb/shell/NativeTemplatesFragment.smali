.class public Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->NATIVE_TEMPLATES_FRAGMENT:LX/0cQ;
.end annotation


# instance fields
.field public a:LX/CNt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/CNc;

.field private f:LX/H1c;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2415652
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;

    const-class v1, LX/CNt;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/CNt;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object p0

    check-cast p0, LX/0zG;

    iput-object v1, p1, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->a:LX/CNt;

    iput-object v2, p1, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->b:LX/0tX;

    iput-object v3, p1, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->c:Ljava/util/concurrent/ExecutorService;

    iput-object p0, p1, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->d:LX/0zG;

    return-void
.end method

.method public static b(Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;)V
    .locals 3

    .prologue
    .line 2415653
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2415654
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->h:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2415655
    :cond_0
    :goto_0
    return-void

    .line 2415656
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    .line 2415657
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2415658
    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v0, v0, Landroid/graphics/drawable/ColorDrawable;

    if-eqz v0, :cond_2

    .line 2415659
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_2

    .line 2415660
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2415661
    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result v0

    .line 2415662
    if-eq v0, v1, :cond_0

    .line 2415663
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2415664
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2415665
    iget-object v0, p0, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2415666
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2415667
    const-class v0, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;

    invoke-static {v0, p0}, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2415668
    sget-object v0, LX/3j6;->a:LX/0P1;

    sget-object v1, LX/3jH;->a:LX/0P1;

    sget-object v2, LX/3jL;->a:LX/0P1;

    invoke-static {v0, v1, v2}, LX/3jU;->a(LX/0P1;LX/0P1;LX/0P1;)V

    .line 2415669
    iget-object v0, p0, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->a:LX/CNt;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/CNt;->a(Landroid/content/Context;)LX/CNq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->e:LX/CNc;

    .line 2415670
    new-instance v0, LX/H1c;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->e:LX/CNc;

    invoke-direct {v0, v1, v2}, LX/H1c;-><init>(Landroid/content/Context;LX/CNc;)V

    iput-object v0, p0, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->f:LX/H1c;

    .line 2415671
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2415672
    const-string v1, "id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->a(Ljava/lang/String;)V

    .line 2415673
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 2415674
    invoke-static {p1}, LX/H1h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2415675
    iget-object v1, p0, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->b:LX/0tX;

    iget-object v2, p0, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->c:Ljava/util/concurrent/ExecutorService;

    .line 2415676
    new-instance v3, LX/5f1;

    invoke-direct {v3}, LX/5f1;-><init>()V

    move-object v3, v3

    .line 2415677
    const-string v4, "params"

    .line 2415678
    new-instance v6, LX/0m9;

    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v5}, LX/0m9;-><init>(LX/0mC;)V

    .line 2415679
    const-string v5, "query"

    invoke-virtual {v6, v5, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2415680
    new-instance v7, LX/0m9;

    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-direct {v7, v5}, LX/0m9;-><init>(LX/0mC;)V

    .line 2415681
    const/high16 v5, 0x3f800000    # 1.0f

    .line 2415682
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2415683
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    .line 2415684
    :cond_0
    const-string p1, "pixel_ratio"

    invoke-virtual {v7, p1, v5}, LX/0m9;->a(Ljava/lang/String;F)LX/0m9;

    .line 2415685
    const-string v5, "styles_id"

    const-string p1, "701268200040015"

    invoke-virtual {v7, v5, p1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2415686
    const-string v5, "nt_context"

    invoke-virtual {v7}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v5, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2415687
    invoke-virtual {v6}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 2415688
    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2415689
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->c:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    .line 2415690
    invoke-virtual {v1, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2415691
    new-instance v4, LX/H1g;

    invoke-direct {v4, p0}, LX/H1g;-><init>(Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;)V

    invoke-static {v3, v4, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2415692
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/16 v0, 0x2a

    const v1, -0x2c6dd796

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2415693
    const v0, 0x7f030bae

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 2415694
    const v0, 0x7f0d0be2

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2415695
    const v1, 0x7f0d0be3

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/components/ComponentView;

    .line 2415696
    invoke-static {p0}, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->b(Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;)V

    .line 2415697
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2415698
    const-string v5, "id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2415699
    new-instance v5, LX/H1d;

    invoke-direct {v5, p0, v4, v0}, LX/H1d;-><init>(Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;Ljava/lang/String;Lcom/facebook/widget/FbSwipeRefreshLayout;)V

    invoke-virtual {v0, v5}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2415700
    new-instance v4, LX/H1e;

    invoke-direct {v4, p0, v0}, LX/H1e;-><init>(Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;Lcom/facebook/widget/FbSwipeRefreshLayout;)V

    .line 2415701
    move-object v0, v4

    .line 2415702
    new-instance v4, LX/1De;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, LX/1De;-><init>(Landroid/content/Context;)V

    .line 2415703
    invoke-static {v4}, LX/5K1;->c(LX/1De;)LX/5Jz;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->f:LX/H1c;

    invoke-virtual {v5, v6}, LX/5Jz;->a(LX/5Je;)LX/5Jz;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/5Jz;->a(LX/1Of;)LX/5Jz;

    move-result-object v5

    invoke-virtual {v5, v0}, LX/5Jz;->a(LX/1OX;)LX/5Jz;

    move-result-object v0

    invoke-static {v4, v0}, LX/1dV;->a(LX/1De;LX/1X5;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 2415704
    const/16 v0, 0x2b

    const v1, -0x4322abe4

    invoke-static {v7, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v3
.end method

.method public final onStart()V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/16 v0, 0x2a

    const v3, -0x29de5e8

    invoke-static {v4, v0, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2415705
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2415706
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2415707
    if-nez v0, :cond_0

    .line 2415708
    const/16 v0, 0x2b

    const v1, -0x402d6ca3

    invoke-static {v4, v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2415709
    :goto_0
    return-void

    .line 2415710
    :cond_0
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2415711
    const-string v5, "title"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/H1h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2415712
    iget-object v5, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v5

    .line 2415713
    const-string v6, "search"

    invoke-virtual {v5, v6, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2415714
    iget-object v5, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v5

    .line 2415715
    const-string v6, "search"

    invoke-virtual {v5, v6, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_1
    move v1, v2

    .line 2415716
    :cond_2
    if-eqz v1, :cond_3

    .line 2415717
    iget-object v1, p0, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->d:LX/0zG;

    invoke-interface {v1}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0h5;

    .line 2415718
    instance-of v5, v1, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    if-eqz v5, :cond_3

    .line 2415719
    invoke-interface {v0, v2}, LX/1ZF;->k_(Z)V

    .line 2415720
    check-cast v1, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    sget-object v2, LX/0zI;->DEFAULT:LX/0zI;

    invoke-virtual {v1, v2}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->a(LX/0zI;)V

    .line 2415721
    :cond_3
    invoke-interface {v0, v4}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2415722
    const v0, 0x25d643bd

    invoke-static {v0, v3}, LX/02F;->f(II)V

    goto :goto_0
.end method
