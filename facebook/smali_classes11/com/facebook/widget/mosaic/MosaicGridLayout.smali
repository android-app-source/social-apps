.class public Lcom/facebook/widget/mosaic/MosaicGridLayout;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field private a:I

.field private b:I

.field public c:Z

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x6

    .line 2304525
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 2304526
    iput v0, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->a:I

    .line 2304527
    iput v0, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->b:I

    .line 2304528
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->c:Z

    .line 2304529
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x6

    .line 2304520
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2304521
    iput v0, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->a:I

    .line 2304522
    iput v0, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->b:I

    .line 2304523
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->c:Z

    .line 2304524
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x6

    .line 2304515
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2304516
    iput v0, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->a:I

    .line 2304517
    iput v0, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->b:I

    .line 2304518
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->c:Z

    .line 2304519
    return-void
.end method

.method private static a()LX/G6g;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2304514
    new-instance v0, LX/G6g;

    invoke-direct {v0, v1, v1, v1, v1}, LX/G6g;-><init>(IIII)V

    return-object v0
.end method

.method private c()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2304506
    invoke-virtual {p0}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->getChildCount()I

    move-result v3

    move v2, v0

    move v1, v0

    .line 2304507
    :goto_0
    if-ge v2, v3, :cond_0

    .line 2304508
    invoke-virtual {p0, v2}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2304509
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_1

    .line 2304510
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/G6g;

    .line 2304511
    iget v4, v0, LX/G6g;->b:I

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    add-int/2addr v0, v4

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2304512
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 2304513
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(II)V
    .locals 0

    .prologue
    .line 2304503
    iput p2, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->a:I

    .line 2304504
    iput p1, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->b:I

    .line 2304505
    return-void
.end method

.method public final a(IIII)V
    .locals 0

    .prologue
    .line 2304498
    iput p1, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->d:I

    .line 2304499
    iput p3, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->e:I

    .line 2304500
    iput p2, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->f:I

    .line 2304501
    iput p4, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->g:I

    .line 2304502
    return-void
.end method

.method public final b(II)V
    .locals 1

    .prologue
    .line 2304491
    div-int/lit8 v0, p1, 0x2

    iput v0, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->j:I

    iput v0, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->k:I

    .line 2304492
    rem-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    .line 2304493
    iget v0, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->k:I

    .line 2304494
    :cond_0
    div-int/lit8 v0, p2, 0x2

    iput v0, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->h:I

    iput v0, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->i:I

    .line 2304495
    rem-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_1

    .line 2304496
    iget v0, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->i:I

    .line 2304497
    :cond_1
    return-void
.end method

.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 2304490
    instance-of v0, p1, LX/G6g;

    return v0
.end method

.method public final synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 2304454
    invoke-static {}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->a()LX/G6g;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 2304489
    invoke-static {}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->a()LX/G6g;

    move-result-object v0

    return-object v0
.end method

.method public final onLayout(ZIIII)V
    .locals 22

    .prologue
    .line 2304462
    sub-int v2, p4, p2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->d:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->e:I

    add-int/2addr v3, v4

    sub-int v8, v2, v3

    .line 2304463
    sub-int v2, p5, p3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->f:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->g:I

    add-int/2addr v3, v4

    sub-int v9, v2, v3

    .line 2304464
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->getChildCount()I

    move-result v10

    .line 2304465
    const/4 v2, 0x0

    move v7, v2

    :goto_0
    if-ge v7, v10, :cond_5

    .line 2304466
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    .line 2304467
    if-eqz v11, :cond_0

    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 2304468
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, LX/G6g;

    .line 2304469
    iget v3, v2, LX/G6g;->a:I

    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    add-int v12, v3, v4

    .line 2304470
    iget v3, v2, LX/G6g;->b:I

    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    add-int v13, v3, v4

    .line 2304471
    iget v3, v2, LX/G6g;->a:I

    if-nez v3, :cond_1

    const/4 v3, 0x0

    .line 2304472
    :goto_1
    iget v4, v2, LX/G6g;->b:I

    if-nez v4, :cond_2

    const/4 v4, 0x0

    .line 2304473
    :goto_2
    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->b:I

    if-ne v12, v5, :cond_3

    const/4 v5, 0x0

    .line 2304474
    :goto_3
    move-object/from16 v0, p0

    iget v6, v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->a:I

    if-ne v13, v6, :cond_4

    const/4 v6, 0x0

    .line 2304475
    :goto_4
    move-object/from16 v0, p0

    iget v14, v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->b:I

    div-int v14, v8, v14

    int-to-double v14, v14

    .line 2304476
    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->a:I

    move/from16 v16, v0

    div-int v16, v9, v16

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v16, v0

    .line 2304477
    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->d:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v18, v0

    iget v0, v2, LX/G6g;->a:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-double v0, v0

    move-wide/from16 v20, v0

    mul-double v20, v20, v14

    add-double v18, v18, v20

    int-to-double v0, v3

    move-wide/from16 v20, v0

    add-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v3, v0

    .line 2304478
    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->f:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v18, v0

    iget v2, v2, LX/G6g;->b:I

    int-to-double v0, v2

    move-wide/from16 v20, v0

    mul-double v20, v20, v16

    add-double v18, v18, v20

    int-to-double v0, v4

    move-wide/from16 v20, v0

    add-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v2, v0

    .line 2304479
    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->d:I

    int-to-double v0, v4

    move-wide/from16 v18, v0

    int-to-double v0, v12

    move-wide/from16 v20, v0

    mul-double v14, v14, v20

    add-double v14, v14, v18

    int-to-double v4, v5

    sub-double v4, v14, v4

    double-to-int v4, v4

    .line 2304480
    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->f:I

    int-to-double v14, v5

    int-to-double v12, v13

    mul-double v12, v12, v16

    add-double/2addr v12, v14

    int-to-double v14, v6

    sub-double/2addr v12, v14

    double-to-int v5, v12

    .line 2304481
    sub-int v6, v4, v3

    const/high16 v12, 0x40000000    # 2.0f

    invoke-static {v6, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    sub-int v12, v5, v2

    const/high16 v13, 0x40000000    # 2.0f

    invoke-static {v12, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    invoke-virtual {v11, v6, v12}, Landroid/view/View;->measure(II)V

    .line 2304482
    invoke-virtual {v11, v3, v2, v4, v5}, Landroid/view/View;->layout(IIII)V

    .line 2304483
    :cond_0
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto/16 :goto_0

    .line 2304484
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->j:I

    goto/16 :goto_1

    .line 2304485
    :cond_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->h:I

    goto/16 :goto_2

    .line 2304486
    :cond_3
    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->k:I

    goto/16 :goto_3

    .line 2304487
    :cond_4
    move-object/from16 v0, p0

    iget v6, v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->i:I

    goto/16 :goto_4

    .line 2304488
    :cond_5
    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 2304457
    invoke-virtual {p0}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {v0, p1}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->getDefaultSize(II)I

    move-result v0

    .line 2304458
    iget-boolean v1, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->c:Z

    if-eqz v1, :cond_0

    .line 2304459
    invoke-direct {p0}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->c()I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->a:I

    .line 2304460
    :cond_0
    iget v1, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->b:I

    div-int v1, v0, v1

    iget v2, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->a:I

    mul-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->setMeasuredDimension(II)V

    .line 2304461
    return-void
.end method

.method public setResizeToFit(Z)V
    .locals 0

    .prologue
    .line 2304455
    iput-boolean p1, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->c:Z

    .line 2304456
    return-void
.end method
