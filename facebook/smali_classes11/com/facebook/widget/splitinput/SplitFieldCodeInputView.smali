.class public Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field public a:Landroid/view/LayoutInflater;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/resources/ui/DigitEditText;",
            ">;"
        }
    .end annotation
.end field

.field public e:J

.field public f:I

.field public g:Landroid/widget/PopupWindow;

.field public h:I

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/G6q;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/EiK;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/EiM;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2320894
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 2320895
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->a(Landroid/util/AttributeSet;)V

    .line 2320896
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2320891
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2320892
    invoke-direct {p0, p2}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->a(Landroid/util/AttributeSet;)V

    .line 2320893
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2320888
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2320889
    invoke-direct {p0, p2}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->a(Landroid/util/AttributeSet;)V

    .line 2320890
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x6

    .line 2320878
    const-class v0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    invoke-static {v0, p0}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2320879
    const v0, 0x7f03139a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2320880
    if-nez p1, :cond_0

    .line 2320881
    iput v2, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->h:I

    .line 2320882
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->i:Ljava/util/List;

    .line 2320883
    iget v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->h:I

    invoke-virtual {p0, v0}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->setupView(I)V

    .line 2320884
    return-void

    .line 2320885
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->SplitFieldCodeInputAttributes:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2320886
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->h:I

    .line 2320887
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;Landroid/view/LayoutInflater;Landroid/view/inputmethod/InputMethodManager;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .prologue
    .line 2320877
    iput-object p1, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->a:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->b:Landroid/view/inputmethod/InputMethodManager;

    iput-object p3, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->c:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    invoke-static {v2}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    invoke-static {v2}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v2}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->a(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;Landroid/view/LayoutInflater;Landroid/view/inputmethod/InputMethodManager;Ljava/util/concurrent/ExecutorService;)V

    return-void
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2320861
    new-instance v0, LX/G6n;

    invoke-direct {v0, p0}, LX/G6n;-><init>(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;)V

    .line 2320862
    const/4 v1, 0x2

    new-array v5, v1, [Landroid/text/InputFilter;

    aput-object v0, v5, v3

    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v0, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v0, v5, v2

    move v2, v3

    .line 2320863
    :goto_0
    iget v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->h:I

    if-ge v2, v0, :cond_3

    .line 2320864
    iget-object v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/DigitEditText;

    .line 2320865
    invoke-virtual {v0, v5}, Lcom/facebook/resources/ui/DigitEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 2320866
    if-eqz v2, :cond_0

    .line 2320867
    new-instance v1, LX/G6p;

    invoke-direct {v1, p0}, LX/G6p;-><init>(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/DigitEditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2320868
    :cond_0
    iget v1, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->h:I

    add-int/lit8 v1, v1, -0x1

    if-eq v2, v1, :cond_1

    new-instance v4, LX/G6q;

    iget-object v1, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->d:Ljava/util/ArrayList;

    add-int/lit8 v6, v2, 0x1

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    invoke-direct {v4, p0, v1, v0}, LX/G6q;-><init>(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;Landroid/widget/EditText;Landroid/widget/EditText;)V

    .line 2320869
    :goto_1
    iget-object v1, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v2, v1, :cond_2

    .line 2320870
    iget-object v1, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->i:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2320871
    :goto_2
    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/DigitEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2320872
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2320873
    :cond_1
    new-instance v1, LX/G6q;

    const/4 v4, 0x0

    invoke-direct {v1, p0, v4, v0}, LX/G6q;-><init>(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;Landroid/widget/EditText;Landroid/widget/EditText;)V

    move-object v4, v1

    goto :goto_1

    .line 2320874
    :cond_2
    iget-object v1, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->i:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/DigitEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2320875
    iget-object v1, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->i:Ljava/util/List;

    invoke-interface {v1, v2, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 2320876
    :cond_3
    return-void
.end method

.method public static c(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, -0x2

    .line 2320849
    new-instance v0, Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x10102c8

    invoke-direct {v0, v1, v5, v2}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->g:Landroid/widget/PopupWindow;

    .line 2320850
    iget-object v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->g:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 2320851
    iget-object v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->g:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 2320852
    iget-object v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f03139b

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2320853
    const v1, 0x7f0d2d4e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2320854
    const v2, 0x7f0833d8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 2320855
    new-instance v2, LX/G6o;

    invoke-direct {v2, p0}, LX/G6o;-><init>(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2320856
    iget-object v1, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->g:Landroid/widget/PopupWindow;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2320857
    iget-object v1, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->g:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v4}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 2320858
    iget-object v1, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->g:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v4}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 2320859
    iget-object v1, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->g:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v0}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 2320860
    return-void
.end method

.method public static d(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2320897
    iget-object v1, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->k:LX/EiM;

    if-eqz v1, :cond_0

    .line 2320898
    iget-object v1, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->k:LX/EiM;

    invoke-interface {v1}, LX/EiM;->a()V

    .line 2320899
    :cond_0
    invoke-direct {p0}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->e()Ljava/lang/CharSequence;

    move-result-object v3

    .line 2320900
    if-eqz v3, :cond_2

    move v1, v0

    move v2, v0

    .line 2320901
    :goto_0
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->h:I

    if-ge v2, v0, :cond_2

    .line 2320902
    invoke-interface {v3, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    .line 2320903
    const/16 v0, 0x30

    if-lt v4, v0, :cond_1

    const/16 v0, 0x39

    if-gt v4, v0, :cond_1

    .line 2320904
    iget-object v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/DigitEditText;

    invoke-static {v4}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/DigitEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2320905
    add-int/lit8 v2, v2, 0x1

    .line 2320906
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2320907
    :cond_2
    return-void
.end method

.method public static synthetic e(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;)I
    .locals 2

    .prologue
    .line 2320848
    iget v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->f:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->f:I

    return v0
.end method

.method private e()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2320794
    invoke-virtual {p0}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 2320795
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v0

    .line 2320796
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 2320797
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ClipData$Item;->coerceToText(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2320798
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static getCodeText(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2320842
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2320843
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->h:I

    if-ge v1, v0, :cond_0

    .line 2320844
    iget-object v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/DigitEditText;

    .line 2320845
    invoke-virtual {v0}, Lcom/facebook/resources/ui/DigitEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 2320846
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2320847
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getMaxCodeLength()I
    .locals 1

    .prologue
    .line 2320841
    const/4 v0, 0x6

    return v0
.end method

.method public static synthetic j(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;)I
    .locals 2

    .prologue
    .line 2320840
    iget v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->f:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->f:I

    return v0
.end method

.method public static setCodeEnabled(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;Z)V
    .locals 2

    .prologue
    .line 2320836
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->h:I

    if-ge v1, v0, :cond_0

    .line 2320837
    iget-object v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/DigitEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/DigitEditText;->setEnabled(Z)V

    .line 2320838
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2320839
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 2320825
    move v1, v2

    :goto_0
    iget v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->h:I

    if-ge v1, v0, :cond_0

    .line 2320826
    iget-object v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 2320827
    const-string v3, ""

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2320828
    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 2320829
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2320830
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->d:Ljava/util/ArrayList;

    iget v1, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->f:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/DigitEditText;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/DigitEditText;->setFocusableInTouchMode(Z)V

    .line 2320831
    iget-object v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/DigitEditText;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/DigitEditText;->setFocusableInTouchMode(Z)V

    .line 2320832
    iput v2, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->f:I

    .line 2320833
    invoke-virtual {p0}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2320834
    iget-object v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/DigitEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/DigitEditText;->requestFocus()Z

    .line 2320835
    :cond_1
    return-void
.end method

.method public getNumberOfDigits()I
    .locals 1

    .prologue
    .line 2320824
    iget v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->h:I

    return v0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 2320817
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iget v1, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->h:I

    if-ne v0, v1, :cond_1

    move v1, v2

    .line 2320818
    :goto_0
    iget v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->h:I

    if-ge v1, v0, :cond_0

    .line 2320819
    iget-object v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/DigitEditText;

    invoke-virtual {p1, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/DigitEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2320820
    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 2320821
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2320822
    :cond_0
    iget v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->f:I

    .line 2320823
    :cond_1
    return-void
.end method

.method public setupView(I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x6

    const/4 v2, 0x0

    .line 2320799
    iput p1, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->h:I

    .line 2320800
    iget v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->h:I

    if-gt v0, v5, :cond_0

    iget v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->h:I

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Number of input digits must lie between 1 and %d"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0, v3, v4}, LX/0Tp;->b(ZLjava/lang/String;Ljava/lang/Object;)V

    .line 2320801
    new-array v3, v5, [Lcom/facebook/resources/ui/DigitEditText;

    const v0, 0x7f0d2d48

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/DigitEditText;

    aput-object v0, v3, v2

    const v0, 0x7f0d2d49

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/DigitEditText;

    aput-object v0, v3, v1

    const/4 v4, 0x2

    const v0, 0x7f0d2d4a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/DigitEditText;

    aput-object v0, v3, v4

    const/4 v4, 0x3

    const v0, 0x7f0d2d4b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/DigitEditText;

    aput-object v0, v3, v4

    const/4 v4, 0x4

    const v0, 0x7f0d2d4c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/DigitEditText;

    aput-object v0, v3, v4

    const/4 v4, 0x5

    const v0, 0x7f0d2d4d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/DigitEditText;

    aput-object v0, v3, v4

    invoke-static {v3}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->d:Ljava/util/ArrayList;

    .line 2320802
    iget-object v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/DigitEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/DigitEditText;->setFocusableInTouchMode(Z)V

    move v1, v2

    .line 2320803
    :goto_1
    if-ge v1, v5, :cond_2

    .line 2320804
    iget-object v0, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/DigitEditText;

    .line 2320805
    iget v3, p0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->h:I

    if-lt v1, v3, :cond_1

    .line 2320806
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/DigitEditText;->setVisibility(I)V

    .line 2320807
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_0
    move v0, v2

    .line 2320808
    goto :goto_0

    .line 2320809
    :cond_1
    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/DigitEditText;->setVisibility(I)V

    .line 2320810
    new-instance v3, LX/G6k;

    invoke-direct {v3, p0}, LX/G6k;-><init>(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;)V

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/DigitEditText;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2320811
    new-instance v3, LX/G6l;

    invoke-direct {v3, p0}, LX/G6l;-><init>(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;)V

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/DigitEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2320812
    new-instance v3, LX/G6m;

    invoke-direct {v3, p0}, LX/G6m;-><init>(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;)V

    .line 2320813
    iput-object v3, v0, Lcom/facebook/resources/ui/DigitEditText;->b:LX/4lG;

    .line 2320814
    goto :goto_2

    .line 2320815
    :cond_2
    invoke-direct {p0}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->b()V

    .line 2320816
    return-void
.end method
