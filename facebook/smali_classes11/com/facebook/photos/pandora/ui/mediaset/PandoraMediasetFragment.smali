.class public Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediasetFragment;
.super Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;
.source ""


# instance fields
.field public n:LX/DvR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:LX/DvQ;

.field private p:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2241319
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediasetFragment;

    const-class p0, LX/DvR;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/DvR;

    iput-object v1, p1, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediasetFragment;->n:LX/DvR;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2241320
    invoke-super {p0, p1}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->a(Landroid/os/Bundle;)V

    .line 2241321
    const-class v0, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediasetFragment;

    invoke-static {v0, p0}, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediasetFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2241322
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2241323
    const-string v1, "mediasetId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediasetFragment;->p:Ljava/lang/String;

    .line 2241324
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 3
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2241325
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2241326
    sget-object v1, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediaSetActivity;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2241327
    sget-object v1, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediaSetActivity;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2241328
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2241329
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2241330
    return-void
.end method

.method public final c()LX/Dcc;
    .locals 8

    .prologue
    .line 2241331
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediasetFragment;->o:LX/DvQ;

    if-nez v0, :cond_0

    .line 2241332
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediasetFragment;->n:LX/DvR;

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediasetFragment;->p:Ljava/lang/String;

    .line 2241333
    new-instance v2, LX/DvQ;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/DvI;->a(LX/0QB;)LX/DvI;

    move-result-object v5

    check-cast v5, LX/DvI;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v0}, LX/DvM;->a(LX/0QB;)LX/DvM;

    move-result-object v7

    check-cast v7, LX/DvM;

    move-object v3, v1

    invoke-direct/range {v2 .. v7}, LX/DvQ;-><init>(Ljava/lang/String;Ljava/util/concurrent/ExecutorService;LX/DvI;LX/0tX;LX/DvM;)V

    .line 2241334
    move-object v0, v2

    .line 2241335
    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediasetFragment;->o:LX/DvQ;

    .line 2241336
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediasetFragment;->o:LX/DvQ;

    return-object v0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5d705d8a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2241337
    invoke-super {p0}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->onDestroyView()V

    .line 2241338
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/DxJ;->a(LX/Dce;)V

    .line 2241339
    const/16 v1, 0x2b

    const v2, 0x2a5c4ae4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
