.class public Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediaSetActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field public static p:Ljava/lang/String;

.field public static q:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2241291
    const-string v0, "media_set_display_activity_selected_photo_fb_id_extra"

    sput-object v0, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediaSetActivity;->p:Ljava/lang/String;

    .line 2241292
    const-string v0, "media_set_display_activity_selected_photo_uri_extra"

    sput-object v0, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediaSetActivity;->q:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2241293
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2241294
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediaSetActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2241295
    const-string v1, "media_set_display_activity_profile_id_extra"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2241296
    const-string v1, "media_set_display_activity_mediaset_extra"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2241297
    const-string v1, "media_set_display_activity_titlebard_label"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2241298
    const-string v1, "media_set_display_activity_titlebard_label_resource_id_extra"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2241299
    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2241300
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2241301
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2241302
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediaSetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "media_set_display_activity_titlebard_label"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2241303
    new-instance v1, LX/FS9;

    invoke-direct {v1, p0}, LX/FS9;-><init>(Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediaSetActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2241304
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2241305
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2241306
    const v0, 0x7f030aa4

    invoke-virtual {p0, v0}, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediaSetActivity;->setContentView(I)V

    .line 2241307
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediaSetActivity;->a()V

    .line 2241308
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediaSetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2241309
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    const-string v3, "media_set_display_activity_profile_id_extra"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "media_set_display_activity_mediaset_extra"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "media_set_display_activity_titlebard_label_resource_id_extra"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    .line 2241310
    new-instance v5, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediasetFragment;

    invoke-direct {v5}, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediasetFragment;-><init>()V

    .line 2241311
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2241312
    const-string v7, "mediasetId"

    invoke-virtual {v6, v7, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2241313
    const-string v7, "callerContext"

    invoke-virtual {v6, v7, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2241314
    const-string v7, "pandora_instance_id"

    new-instance p0, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;

    const-string p1, "ms"

    invoke-direct {p0, v3, p1}, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2241315
    invoke-virtual {v5, v6}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2241316
    move-object v0, v5

    .line 2241317
    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2241318
    return-void
.end method
