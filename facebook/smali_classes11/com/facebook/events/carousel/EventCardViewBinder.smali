.class public Lcom/facebook/events/carousel/EventCardViewBinder;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "LX/Gco;",
            ">;"
        }
    .end annotation
.end field

.field public final B:LX/Gcs;

.field public final b:Landroid/content/Context;

.field public final c:Lcom/facebook/events/common/EventAnalyticsParams;

.field public final d:LX/1aZ;

.field public final e:Landroid/graphics/PointF;

.field public final f:Landroid/content/res/Resources;

.field public final g:LX/Bne;

.field public final h:LX/7vW;

.field public final i:LX/7vZ;

.field private final j:LX/6RU;

.field public final k:LX/Bl6;

.field public final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/DB4;

.field public n:Lcom/facebook/events/model/Event;

.field public o:Ljava/lang/CharSequence;

.field public p:Ljava/util/Date;

.field public q:Ljava/lang/CharSequence;

.field public r:Ljava/lang/CharSequence;

.field public s:Ljava/lang/CharSequence;

.field public t:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public u:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

.field public v:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public w:Lcom/facebook/events/widget/eventcard/EventsCardView;

.field public x:Lcom/facebook/events/common/ActionMechanism;

.field public y:LX/Gcz;

.field public z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2372680
    const-class v0, Lcom/facebook/events/carousel/EventCardViewBinder;

    const-string v1, "event_dashboard_all_upcoming"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/carousel/EventCardViewBinder;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/util/List;Landroid/content/Context;LX/38v;LX/7vW;LX/7vZ;LX/6RU;LX/Bl6;LX/0Or;LX/DB4;Landroid/content/res/Resources;)V
    .locals 2
    .param p1    # Lcom/facebook/events/model/Event;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/events/common/EventAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/model/Event;",
            "Lcom/facebook/events/graphql/EventsGraphQLInterfaces$EventCardFragment$CoverPhoto;",
            "Lcom/facebook/events/common/EventAnalyticsParams;",
            "Ljava/util/List",
            "<+",
            "LX/Gco;",
            ">;",
            "Landroid/content/Context;",
            "LX/38v;",
            "LX/7vW;",
            "LX/7vZ;",
            "LX/6RU;",
            "LX/Bl6;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/DB4;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2372681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2372682
    new-instance v0, LX/Gcs;

    invoke-direct {v0, p0}, LX/Gcs;-><init>(Lcom/facebook/events/carousel/EventCardViewBinder;)V

    iput-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->B:LX/Gcs;

    .line 2372683
    iput-object p13, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->f:Landroid/content/res/Resources;

    .line 2372684
    iget-object v0, p3, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2372685
    iget-object v1, v0, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v0, v1

    .line 2372686
    sget-object v1, Lcom/facebook/events/common/ActionSource;->DASHBOARD:Lcom/facebook/events/common/ActionSource;

    if-ne v0, v1, :cond_1

    .line 2372687
    sget-object v0, Lcom/facebook/events/common/ActionSource;->MOBILE_SUGGESTIONS_DASHBOARD:Lcom/facebook/events/common/ActionSource;

    .line 2372688
    :goto_0
    invoke-virtual {p3, v0}, Lcom/facebook/events/common/EventAnalyticsParams;->a(Lcom/facebook/events/common/ActionSource;)Lcom/facebook/events/common/EventAnalyticsParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2372689
    iput-object p4, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->A:Ljava/util/List;

    .line 2372690
    new-instance v0, LX/Gcr;

    invoke-direct {v0, p0}, LX/Gcr;-><init>(Lcom/facebook/events/carousel/EventCardViewBinder;)V

    invoke-virtual {p6, v0}, LX/38v;->a(LX/Bni;)LX/Bne;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->g:LX/Bne;

    .line 2372691
    iput-object p7, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->h:LX/7vW;

    .line 2372692
    iput-object p8, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->i:LX/7vZ;

    .line 2372693
    iput-object p9, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->j:LX/6RU;

    .line 2372694
    iput-object p10, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->k:LX/Bl6;

    .line 2372695
    iput-object p11, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->l:LX/0Or;

    .line 2372696
    iput-object p5, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->b:Landroid/content/Context;

    .line 2372697
    iput-object p12, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->m:LX/DB4;

    .line 2372698
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->j()LX/1Fb;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2372699
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v1, Lcom/facebook/events/carousel/EventCardViewBinder;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2372700
    :goto_1
    move-object v0, v0

    .line 2372701
    iput-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->d:LX/1aZ;

    .line 2372702
    invoke-static {p2}, Lcom/facebook/events/carousel/EventCardViewBinder;->b(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;)Landroid/graphics/PointF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->e:Landroid/graphics/PointF;

    .line 2372703
    invoke-virtual {p0, p1}, Lcom/facebook/events/carousel/EventCardViewBinder;->a(Lcom/facebook/events/model/Event;)V

    .line 2372704
    return-void

    .line 2372705
    :cond_1
    iget-object v0, p3, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2372706
    iget-object v1, v0, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v0, v1

    .line 2372707
    goto :goto_0

    .line 2372708
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->j()LX/1Fb;

    move-result-object p3

    .line 2372709
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->eT_()LX/1Fb;

    move-result-object v0

    .line 2372710
    if-eqz v0, :cond_3

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    .line 2372711
    :goto_2
    iget-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object p4, Lcom/facebook/events/carousel/EventCardViewBinder;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-interface {p3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    invoke-static {v1}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    goto :goto_1

    .line 2372712
    :cond_3
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_2
.end method

.method private static b(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;)Landroid/graphics/PointF;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2372713
    if-nez p0, :cond_1

    .line 2372714
    :cond_0
    :goto_0
    return-object v0

    .line 2372715
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->a()LX/1f8;

    move-result-object v1

    .line 2372716
    if-eqz v1, :cond_0

    new-instance v0, Landroid/graphics/PointF;

    invoke-interface {v1}, LX/1f8;->a()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-interface {v1}, LX/1f8;->b()D

    move-result-wide v4

    double-to-float v1, v4

    invoke-direct {v0, v2, v1}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_0
.end method

.method public static c(Lcom/facebook/events/carousel/EventCardViewBinder;)V
    .locals 5

    .prologue
    .line 2372717
    iget-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->w:Lcom/facebook/events/widget/eventcard/EventsCardView;

    if-eqz v0, :cond_0

    .line 2372718
    iget-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->w:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {v0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->getActionButton()Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->g:LX/Bne;

    iget-object v2, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    .line 2372719
    iget-object v3, v2, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v2, v3

    .line 2372720
    iget-object v3, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    invoke-virtual {v3}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    .line 2372721
    iget-object p0, v4, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v4, p0

    .line 2372722
    invoke-virtual {v1, v2, v3, v4}, LX/Bne;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->a(LX/BnW;)V

    .line 2372723
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2372724
    iget-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->k:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->B:LX/Gcs;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2372725
    iget-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->w:Lcom/facebook/events/widget/eventcard/EventsCardView;

    if-eqz v0, :cond_0

    .line 2372726
    iget-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->w:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {v0, v2}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setCoverPhotoController(LX/1aZ;)V

    .line 2372727
    iget-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->w:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {v0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->getActionButton()Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372728
    iget-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->w:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {v0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->getRemoveButton()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372729
    iput-object v2, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->w:Lcom/facebook/events/widget/eventcard/EventsCardView;

    .line 2372730
    :cond_0
    iput-object v2, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->y:LX/Gcz;

    .line 2372731
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;)V
    .locals 2

    .prologue
    .line 2372732
    iput-object p1, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    .line 2372733
    iget-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    .line 2372734
    iget-object v1, v0, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2372735
    iput-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->o:Ljava/lang/CharSequence;

    .line 2372736
    iget-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    invoke-virtual {v0}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->p:Ljava/util/Date;

    .line 2372737
    iget-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    .line 2372738
    iget-object v1, v0, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v0, v1

    .line 2372739
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2372740
    iget-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    .line 2372741
    iget-object v1, v0, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v0, v1

    .line 2372742
    :goto_0
    move-object v0, v0

    .line 2372743
    iput-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->q:Ljava/lang/CharSequence;

    .line 2372744
    iget-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    .line 2372745
    iget-object v1, v0, Lcom/facebook/events/model/Event;->U:Ljava/lang/String;

    move-object v0, v1

    .line 2372746
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2372747
    iget-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    .line 2372748
    iget-object v1, v0, Lcom/facebook/events/model/Event;->U:Ljava/lang/String;

    move-object v0, v1

    .line 2372749
    :goto_1
    move-object v0, v0

    .line 2372750
    iput-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->r:Ljava/lang/CharSequence;

    .line 2372751
    iget-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->m:LX/DB4;

    iget-object v1, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    invoke-virtual {v0, v1}, LX/DB4;->a(Lcom/facebook/events/model/Event;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->s:Ljava/lang/CharSequence;

    .line 2372752
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->t:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2372753
    iget-object v0, p1, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v0, v0

    .line 2372754
    iput-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->v:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 2372755
    iget-object v0, p1, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v0, v0

    .line 2372756
    iput-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->u:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 2372757
    return-void

    .line 2372758
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    .line 2372759
    iget-object v1, v0, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v0, v1

    .line 2372760
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2372761
    iget-object v0, p0, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    .line 2372762
    iget-object v1, v0, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v0, v1

    .line 2372763
    goto :goto_0

    .line 2372764
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
