.class public Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# static fields
.field public static final e:Ljava/lang/String;

.field public static final f:Ljava/lang/String;


# instance fields
.field public a:LX/9J2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/87v;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/GdL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/feed/fragment/NewsFeedFragment;

.field public h:Lcom/facebook/feed/fragment/NewsFeedFragment;

.field public i:Z

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2373812
    const-class v0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->e:Ljava/lang/String;

    .line 2373813
    const-class v0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2373809
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2373810
    iput-boolean v0, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->i:Z

    .line 2373811
    iput-boolean v0, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->j:Z

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;

    invoke-static {p0}, LX/9J2;->a(LX/0QB;)LX/9J2;

    move-result-object v1

    check-cast v1, LX/9J2;

    invoke-static {p0}, LX/87v;->b(LX/0QB;)LX/87v;

    move-result-object v2

    check-cast v2, LX/87v;

    invoke-static {p0}, LX/GdL;->a(LX/0QB;)LX/GdL;

    move-result-object v3

    check-cast v3, LX/GdL;

    invoke-static {p0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object p0

    check-cast p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iput-object v1, p1, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->a:LX/9J2;

    iput-object v2, p1, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->b:LX/87v;

    iput-object v3, p1, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->c:LX/GdL;

    iput-object p0, p1, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;Z)V
    .locals 4

    .prologue
    .line 2373799
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    sget-object v1, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2373800
    if-nez v0, :cond_0

    .line 2373801
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 2373802
    const v1, 0x7f0d148e

    invoke-static {p0, p1}, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->d(Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;Z)Lcom/facebook/base/fragment/FbFragment;

    move-result-object v2

    sget-object v3, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    .line 2373803
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2373804
    :goto_0
    return-void

    .line 2373805
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 2373806
    const v1, 0x7f0d148e

    invoke-static {p0, p1}, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->d(Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;Z)Lcom/facebook/base/fragment/FbFragment;

    move-result-object v2

    sget-object v3, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    .line 2373807
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2373808
    goto :goto_0
.end method

.method public static d(Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;Z)Lcom/facebook/base/fragment/FbFragment;
    .locals 1

    .prologue
    .line 2373783
    if-eqz p1, :cond_1

    .line 2373784
    iget-object v0, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->h:Lcom/facebook/feed/fragment/NewsFeedFragment;

    if-nez v0, :cond_0

    .line 2373785
    new-instance v0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;

    invoke-direct {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;-><init>()V

    sget-object p1, Lcom/facebook/api/feedtype/FeedType;->c:Lcom/facebook/api/feedtype/FeedType;

    .line 2373786
    iput-object p1, v0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 2373787
    move-object v0, v0

    .line 2373788
    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->d()Lcom/facebook/feed/fragment/NewsFeedFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->h:Lcom/facebook/feed/fragment/NewsFeedFragment;

    .line 2373789
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->h:Lcom/facebook/feed/fragment/NewsFeedFragment;

    move-object v0, v0

    .line 2373790
    :goto_0
    return-object v0

    .line 2373791
    :cond_1
    iget-object p1, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->a:LX/9J2;

    iget-boolean v0, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->i:Z

    if-eqz v0, :cond_2

    const-string v0, "no_content_placeholder"

    :goto_1
    invoke-virtual {p1, v0}, LX/9J2;->b(Ljava/lang/String;)V

    .line 2373792
    new-instance v0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;

    invoke-direct {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;-><init>()V

    sget-object p1, Lcom/facebook/api/feedtype/FeedType;->d:Lcom/facebook/api/feedtype/FeedType;

    .line 2373793
    iput-object p1, v0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 2373794
    move-object v0, v0

    .line 2373795
    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->d()Lcom/facebook/feed/fragment/NewsFeedFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->g:Lcom/facebook/feed/fragment/NewsFeedFragment;

    .line 2373796
    iget-object v0, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->g:Lcom/facebook/feed/fragment/NewsFeedFragment;

    move-object v0, v0

    .line 2373797
    goto :goto_0

    .line 2373798
    :cond_2
    const-string v0, "no_friends_placeholder"

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2373782
    const-string v0, "good_friends_feed"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2373778
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2373779
    const-class v0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;

    invoke-static {v0, p0}, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2373780
    iget-object v0, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->c:LX/GdL;

    sget-object v1, LX/GdL;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LX/98h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2373781
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v1, -0x1

    .line 2373763
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2373764
    const/16 v0, 0x6dc

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->h:Lcom/facebook/feed/fragment/NewsFeedFragment;

    if-eqz v0, :cond_1

    .line 2373765
    iget-object v0, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->h:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2373766
    :cond_0
    :goto_0
    return-void

    .line 2373767
    :cond_1
    const/16 v0, 0x65

    if-ne p1, v0, :cond_4

    .line 2373768
    if-ne p2, v1, :cond_3

    .line 2373769
    const-string v0, "has_good_friends"

    invoke-virtual {p3, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->i:Z

    .line 2373770
    iput-boolean v2, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->j:Z

    .line 2373771
    :cond_2
    :goto_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    sget-object v1, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2373772
    if-eqz v0, :cond_0

    .line 2373773
    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 2373774
    :cond_3
    sget-object v0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->e:Ljava/lang/String;

    const-string v1, "Can\'t handle activity result, result code=%d"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 2373775
    :cond_4
    const/16 v0, 0x66

    if-ne p1, v0, :cond_2

    if-ne p2, v1, :cond_2

    .line 2373776
    const-string v0, "extra_composer_has_published"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2373777
    iget-object v0, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0xdab71c5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2373760
    iget-object v1, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->b:LX/87v;

    .line 2373761
    iget-boolean v2, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->j:Z

    new-instance v3, LX/GdM;

    invoke-direct {v3, p0}, LX/GdM;-><init>(Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;)V

    invoke-virtual {v1, v2, v3}, LX/87v;->a(ZLX/87u;)V

    .line 2373762
    const v1, 0x7f0307bb

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x4cb1be3e

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5be712b6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2373743
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2373744
    iget-boolean v1, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->j:Z

    if-eqz v1, :cond_0

    .line 2373745
    iget-boolean v1, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->i:Z

    invoke-static {p0, v1}, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->a$redex0(Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;Z)V

    .line 2373746
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->j:Z

    .line 2373747
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x42f53f74

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 1

    .prologue
    .line 2373748
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 2373749
    iget-object v0, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->a:LX/9J2;

    if-eqz v0, :cond_0

    .line 2373750
    if-eqz p1, :cond_1

    .line 2373751
    iget-object v0, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->a:LX/9J2;

    .line 2373752
    sget-object p0, LX/9J1;->ENTRY:LX/9J1;

    invoke-virtual {v0, p0}, LX/9J2;->a(LX/9J1;)V

    .line 2373753
    iget-object p0, v0, LX/9J2;->b:LX/0if;

    sget-object p1, LX/0ig;->aw:LX/0ih;

    invoke-virtual {p0, p1}, LX/0if;->a(LX/0ih;)V

    .line 2373754
    :cond_0
    :goto_0
    return-void

    .line 2373755
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->a:LX/9J2;

    .line 2373756
    sget-object p0, LX/9J1;->EXIT:LX/9J1;

    invoke-virtual {v0, p0}, LX/9J2;->a(LX/9J1;)V

    .line 2373757
    iget-object p0, v0, LX/9J2;->b:LX/0if;

    sget-object p1, LX/0ig;->aw:LX/0ih;

    invoke-virtual {p0, p1}, LX/0if;->c(LX/0ih;)V

    .line 2373758
    const/4 p0, 0x0

    iput-object p0, v0, LX/9J2;->c:Ljava/lang/String;

    .line 2373759
    goto :goto_0
.end method
