.class public final Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Landroid/app/Activity;

.field public final synthetic c:LX/GdO;


# direct methods
.method public constructor <init>(LX/GdO;ILandroid/app/Activity;)V
    .locals 0

    .prologue
    .line 2373855
    iput-object p1, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$3;->c:LX/GdO;

    iput p2, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$3;->a:I

    iput-object p3, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$3;->b:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2373856
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$3;->c:LX/GdO;

    iget-object v0, v0, LX/GdO;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2373857
    const v1, 0x7f082ef7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$3;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2373858
    iget-object v1, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$3;->b:Landroid/app/Activity;

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2373859
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$3;->c:LX/GdO;

    iget-object v0, v0, LX/GdO;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    const/4 v1, 0x0

    .line 2373860
    iput-boolean v1, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->n:Z

    .line 2373861
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$3;->c:LX/GdO;

    iget-object v0, v0, LX/GdO;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->j:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2373862
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$3;->c:LX/GdO;

    iget-object v0, v0, LX/GdO;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->k:Landroid/widget/Button;

    const v1, 0x7f082ef2

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 2373863
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$3;->c:LX/GdO;

    iget-object v0, v0, LX/GdO;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    .line 2373864
    iput-boolean v2, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->o:Z

    .line 2373865
    return-void
.end method
