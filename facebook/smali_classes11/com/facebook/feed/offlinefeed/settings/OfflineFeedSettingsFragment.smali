.class public Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/DC7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17X;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0fU;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0fJ;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pG;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Ym;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Or;
    .annotation runtime Lcom/facebook/base/activity/FragmentBaseActivity;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public j:Landroid/widget/LinearLayout;

.field public k:Landroid/widget/Button;

.field public l:Lcom/facebook/resources/ui/FbTextView;

.field public m:Landroid/widget/ProgressBar;

.field public n:Z

.field public o:Z

.field public final p:LX/DC2;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2373916
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2373917
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2373918
    iput-object v0, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->b:LX/0Ot;

    .line 2373919
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2373920
    iput-object v0, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->c:LX/0Ot;

    .line 2373921
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2373922
    iput-object v0, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->d:LX/0Ot;

    .line 2373923
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2373924
    iput-object v0, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->e:LX/0Ot;

    .line 2373925
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2373926
    iput-object v0, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->f:LX/0Ot;

    .line 2373927
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2373928
    iput-object v0, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->g:LX/0Ot;

    .line 2373929
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2373930
    iput-object v0, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->h:LX/0Ot;

    .line 2373931
    iput-boolean v1, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->n:Z

    .line 2373932
    iput-boolean v1, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->o:Z

    .line 2373933
    new-instance v0, LX/GdO;

    invoke-direct {v0, p0}, LX/GdO;-><init>(Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;)V

    iput-object v0, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->p:LX/DC2;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2373913
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v11

    move-object v2, p0

    check-cast v2, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    invoke-static {v11}, LX/Err;->a(LX/0QB;)LX/Err;

    move-result-object v3

    check-cast v3, LX/DC7;

    const/16 v4, 0xc49

    invoke-static {v11, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x230

    invoke-static {v11, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xc16

    invoke-static {v11, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x455

    invoke-static {v11, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x5ec

    invoke-static {v11, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x60a

    invoke-static {v11, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xaa5

    invoke-static {v11, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v0, 0xb

    invoke-static {v11, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    iput-object v3, v2, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->a:LX/DC7;

    iput-object v4, v2, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->b:LX/0Ot;

    iput-object v5, v2, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->c:LX/0Ot;

    iput-object v6, v2, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->d:LX/0Ot;

    iput-object v7, v2, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->e:LX/0Ot;

    iput-object v8, v2, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->f:LX/0Ot;

    iput-object v9, v2, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->g:LX/0Ot;

    iput-object v10, v2, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->h:LX/0Ot;

    iput-object v11, v2, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->i:LX/0Or;

    .line 2373914
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2373915
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x1c47f1bd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2373898
    const v1, 0x7f030c61

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x53830a2b

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x76f5a72f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2373906
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0if;

    sget-object v2, LX/0ig;->bb:LX/0ih;

    invoke-virtual {v0, v2}, LX/0if;->a(LX/0ih;)V

    .line 2373907
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2373908
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2373909
    if-eqz v0, :cond_0

    .line 2373910
    const v2, 0x7f082ef3

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2373911
    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/1ZF;->k_(Z)V

    .line 2373912
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x702b1cb8

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2373899
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2373900
    const v0, 0x7f0d1e74

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->j:Landroid/widget/LinearLayout;

    .line 2373901
    const v0, 0x7f0d1e75

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 2373902
    const v0, 0x7f0d1e76

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->m:Landroid/widget/ProgressBar;

    .line 2373903
    const v0, 0x7f0d1e70

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->k:Landroid/widget/Button;

    .line 2373904
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->k:Landroid/widget/Button;

    new-instance v1, LX/GdN;

    invoke-direct {v1, p0}, LX/GdN;-><init>(Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2373905
    return-void
.end method
