.class public final Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xece797e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Z

.field private o:I

.field private p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Z

.field private r:Z

.field private s:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2374124
    const-class v0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2374146
    const-class v0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2374144
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2374145
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2374141
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2374142
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2374143
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getComposerActions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2374139
    iget-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->f:Ljava/util/List;

    .line 2374140
    iget-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2374137
    iget-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->g:Ljava/lang/String;

    .line 2374138
    iget-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2374135
    iget-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->i:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    iput-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->i:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    .line 2374136
    iget-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->i:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    return-object v0
.end method

.method private n()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2374133
    iget-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->j:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    iput-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->j:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    .line 2374134
    iget-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->j:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/enums/GraphQLFeedSectionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2374131
    iget-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->k:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    iput-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->k:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 2374132
    iget-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->k:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    return-object v0
.end method

.method private p()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2374129
    iget-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->l:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    iput-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->l:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    .line 2374130
    iget-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->l:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2374127
    iget-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->m:Ljava/lang/String;

    .line 2374128
    iget-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2374125
    iget-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->p:Ljava/lang/String;

    .line 2374126
    iget-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->p:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 2374056
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2374057
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2374058
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->k()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 2374059
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2374060
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->m()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2374061
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->n()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2374062
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->o()Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 2374063
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->p()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2374064
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->q()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2374065
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->r()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2374066
    const/16 v9, 0xf

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2374067
    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 2374068
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2374069
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2374070
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2374071
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2374072
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2374073
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2374074
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2374075
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2374076
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->n:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2374077
    const/16 v0, 0xa

    iget v1, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->o:I

    invoke-virtual {p1, v0, v1, v10}, LX/186;->a(III)V

    .line 2374078
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2374079
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->q:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2374080
    const/16 v0, 0xd

    iget-boolean v1, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->r:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2374081
    const/16 v0, 0xe

    iget-boolean v1, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->s:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2374082
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2374083
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2374084
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2374085
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2374086
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2374087
    if-eqz v1, :cond_4

    .line 2374088
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;

    .line 2374089
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 2374090
    :goto_0
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->m()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2374091
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->m()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    .line 2374092
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->m()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2374093
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;

    .line 2374094
    iput-object v0, v1, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->i:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    .line 2374095
    :cond_0
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->n()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2374096
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->n()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    .line 2374097
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->n()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2374098
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;

    .line 2374099
    iput-object v0, v1, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->j:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    .line 2374100
    :cond_1
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->p()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2374101
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->p()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    .line 2374102
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->p()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2374103
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;

    .line 2374104
    iput-object v0, v1, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->l:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    .line 2374105
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2374106
    if-nez v1, :cond_3

    :goto_1
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2374107
    new-instance v0, LX/GdX;

    invoke-direct {v0, p1}, LX/GdX;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2374108
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->q()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2374109
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2374110
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->h:Z

    .line 2374111
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->n:Z

    .line 2374112
    const/16 v0, 0xa

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->o:I

    .line 2374113
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->q:Z

    .line 2374114
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->r:Z

    .line 2374115
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;->s:Z

    .line 2374116
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2374117
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2374118
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2374119
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2374120
    new-instance v0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;

    invoke-direct {v0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;-><init>()V

    .line 2374121
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2374122
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2374055
    const v0, -0x191e84c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2374123
    const v0, 0x4d63c154    # 2.38818624E8f

    return v0
.end method
