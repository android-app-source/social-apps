.class public final Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4695f582
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2374188
    const-class v0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2374164
    const-class v0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2374165
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2374166
    return-void
.end method

.method private a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFeedSections"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2374167
    iget-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel$FeedSectionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel;->e:Ljava/util/List;

    .line 2374168
    iget-object v0, p0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2374169
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2374170
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2374171
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2374172
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2374173
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2374174
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2374175
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2374176
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2374177
    invoke-direct {p0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2374178
    if-eqz v1, :cond_0

    .line 2374179
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel;

    .line 2374180
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel;->e:Ljava/util/List;

    .line 2374181
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2374182
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2374183
    new-instance v0, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel;

    invoke-direct {v0}, Lcom/facebook/feed/switcher/protocol/FetchFeedSwitcherItemsQueryModels$FeedSwitcherItemsModel;-><init>()V

    .line 2374184
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2374185
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2374186
    const v0, 0x30c8bbe6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2374187
    const v0, -0x77a0e3ee

    return v0
.end method
