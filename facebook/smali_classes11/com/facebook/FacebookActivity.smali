.class public Lcom/facebook/FacebookActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source ""


# static fields
.field public static p:Ljava/lang/String;

.field private static q:Ljava/lang/String;


# instance fields
.field public r:Landroid/support/v4/app/Fragment;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2324782
    const-string v0, "PassThrough"

    sput-object v0, Lcom/facebook/FacebookActivity;->p:Ljava/lang/String;

    .line 2324783
    const-string v0, "SingleFragment"

    sput-object v0, Lcom/facebook/FacebookActivity;->q:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2324781
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2324784
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2324785
    iget-object v0, p0, Lcom/facebook/FacebookActivity;->r:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    .line 2324786
    iget-object v0, p0, Lcom/facebook/FacebookActivity;->r:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2324787
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v0, 0x22

    const v1, -0x5b00dc3c

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2324746
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 2324747
    const v0, 0x7f0302ba

    invoke-virtual {p0, v0}, Lcom/facebook/FacebookActivity;->setContentView(I)V

    .line 2324748
    invoke-virtual {p0}, Lcom/facebook/FacebookActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 2324749
    sget-object v0, Lcom/facebook/FacebookActivity;->p:Ljava/lang/String;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2324750
    invoke-virtual {p0}, Lcom/facebook/FacebookActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2324751
    invoke-static {v0}, LX/GsS;->a(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v2

    .line 2324752
    if-nez v2, :cond_3

    .line 2324753
    const/4 v3, 0x0

    .line 2324754
    :goto_0
    move-object v2, v3

    .line 2324755
    const/4 v3, 0x0

    invoke-static {v0, v3, v2}, LX/GsS;->a(Landroid/content/Intent;Landroid/os/Bundle;LX/GAA;)Landroid/content/Intent;

    move-result-object v0

    .line 2324756
    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/FacebookActivity;->setResult(ILandroid/content/Intent;)V

    .line 2324757
    invoke-virtual {p0}, Lcom/facebook/FacebookActivity;->finish()V

    .line 2324758
    const/16 v0, 0x23

    const v2, -0x12701408

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2324759
    :goto_1
    return-void

    .line 2324760
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v3

    .line 2324761
    sget-object v0, Lcom/facebook/FacebookActivity;->q:Ljava/lang/String;

    invoke-virtual {v3, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2324762
    if-nez v0, :cond_1

    .line 2324763
    const-string v0, "FacebookDialogFragment"

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2324764
    new-instance v0, Lcom/facebook/internal/FacebookDialogFragment;

    invoke-direct {v0}, Lcom/facebook/internal/FacebookDialogFragment;-><init>()V

    .line 2324765
    invoke-virtual {v0, v4}, Landroid/support/v4/app/Fragment;->setRetainInstance(Z)V

    .line 2324766
    sget-object v2, Lcom/facebook/FacebookActivity;->q:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2324767
    :cond_1
    :goto_2
    iput-object v0, p0, Lcom/facebook/FacebookActivity;->r:Landroid/support/v4/app/Fragment;

    .line 2324768
    const v0, 0x5ea0bcda

    invoke-static {v0, v1}, LX/02F;->c(II)V

    goto :goto_1

    .line 2324769
    :cond_2
    new-instance v0, Lcom/facebook/login/LoginFragment;

    invoke-direct {v0}, Lcom/facebook/login/LoginFragment;-><init>()V

    .line 2324770
    invoke-virtual {v0, v4}, Landroid/support/v4/app/Fragment;->setRetainInstance(Z)V

    .line 2324771
    invoke-virtual {v3}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    const v3, 0x7f0d09a5

    sget-object v4, Lcom/facebook/FacebookActivity;->q:Ljava/lang/String;

    invoke-virtual {v2, v3, v0, v4}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v2

    invoke-virtual {v2}, LX/0hH;->b()I

    goto :goto_2

    .line 2324772
    :cond_3
    const-string v3, "error_type"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2324773
    if-nez v3, :cond_4

    .line 2324774
    const-string v3, "com.facebook.platform.status.ERROR_TYPE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2324775
    :cond_4
    const-string v4, "error_description"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2324776
    if-nez v4, :cond_5

    .line 2324777
    const-string v4, "com.facebook.platform.status.ERROR_DESCRIPTION"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2324778
    :cond_5
    if-eqz v3, :cond_6

    const-string p1, "UserCanceled"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2324779
    new-instance v3, LX/GAC;

    invoke-direct {v3, v4}, LX/GAC;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2324780
    :cond_6
    new-instance v3, LX/GAA;

    invoke-direct {v3, v4}, LX/GAA;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
