.class public Lcom/facebook/auth/reauth/ReauthActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/GTG;


# instance fields
.field public p:LX/GTO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Lcom/facebook/auth/reauth/ReauthFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2355369
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/auth/reauth/ReauthActivity;

    invoke-static {v0}, LX/GTO;->a(LX/0QB;)LX/GTO;

    move-result-object v0

    check-cast v0, LX/GTO;

    iput-object v0, p0, Lcom/facebook/auth/reauth/ReauthActivity;->p:LX/GTO;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2355335
    iget-object v0, p0, Lcom/facebook/auth/reauth/ReauthActivity;->q:Lcom/facebook/auth/reauth/ReauthFragment;

    .line 2355336
    iget-object v1, v0, Lcom/facebook/auth/reauth/ReauthFragment;->b:Lcom/facebook/resources/ui/FbButton;

    const/16 p0, 0x8

    invoke-virtual {v1, p0}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2355337
    iget-object v1, v0, Lcom/facebook/auth/reauth/ReauthFragment;->c:Landroid/view/View;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    .line 2355338
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2355339
    iget-object v0, p0, Lcom/facebook/auth/reauth/ReauthActivity;->q:Lcom/facebook/auth/reauth/ReauthFragment;

    .line 2355340
    iget-object v1, v0, Lcom/facebook/auth/reauth/ReauthFragment;->a:Lcom/facebook/resources/ui/FbEditText;

    const-string p0, ""

    invoke-virtual {v1, p0}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2355341
    iget-object v1, v0, Lcom/facebook/auth/reauth/ReauthFragment;->b:Lcom/facebook/resources/ui/FbButton;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2355342
    iget-object v1, v0, Lcom/facebook/auth/reauth/ReauthFragment;->c:Landroid/view/View;

    const/16 p0, 0x8

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    .line 2355343
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2355344
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2355345
    invoke-static {p0, p0}, Lcom/facebook/auth/reauth/ReauthActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2355346
    const v0, 0x7f031192

    invoke-virtual {p0, v0}, Lcom/facebook/auth/reauth/ReauthActivity;->setContentView(I)V

    .line 2355347
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2355348
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/auth/reauth/ReauthActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2355349
    const v1, 0x7f080176

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 2355350
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 2355351
    new-instance v1, Lcom/facebook/auth/reauth/ReauthFragment;

    invoke-direct {v1}, Lcom/facebook/auth/reauth/ReauthFragment;-><init>()V

    iput-object v1, p0, Lcom/facebook/auth/reauth/ReauthActivity;->q:Lcom/facebook/auth/reauth/ReauthFragment;

    .line 2355352
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2355353
    const-string v2, "message"

    invoke-virtual {p0}, Lcom/facebook/auth/reauth/ReauthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "message"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2355354
    iget-object v2, p0, Lcom/facebook/auth/reauth/ReauthActivity;->q:Lcom/facebook/auth/reauth/ReauthFragment;

    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2355355
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d2945

    iget-object v2, p0, Lcom/facebook/auth/reauth/ReauthActivity;->q:Lcom/facebook/auth/reauth/ReauthFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2355356
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 2355357
    iget-object v0, p0, Lcom/facebook/auth/reauth/ReauthActivity;->p:LX/GTO;

    .line 2355358
    invoke-virtual {p0}, Lcom/facebook/auth/reauth/ReauthActivity;->a()V

    .line 2355359
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2355360
    const-string v1, "password"

    invoke-virtual {v3, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2355361
    iget-object v7, v0, LX/GTO;->b:LX/1Ck;

    const-string v8, "auth_reauth"

    iget-object v1, v0, LX/GTO;->a:LX/0aG;

    const-string v2, "auth_reauth"

    sget-object v4, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v5, LX/GTO;

    invoke-static {v5}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v5

    const v6, 0x7e33dc5d    # 5.9769E37f

    invoke-static/range {v1 .. v6}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v1

    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    .line 2355362
    new-instance v2, LX/GTN;

    invoke-direct {v2, v0, p0}, LX/GTN;-><init>(LX/GTO;Lcom/facebook/auth/reauth/ReauthActivity;)V

    move-object v2, v2

    .line 2355363
    invoke-virtual {v7, v8, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2355364
    return-void
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    .line 2355365
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2355366
    iget-object v0, p0, Lcom/facebook/auth/reauth/ReauthActivity;->p:LX/GTO;

    .line 2355367
    iget-object v1, v0, LX/GTO;->f:LX/0TF;

    new-instance v2, Ljava/util/concurrent/CancellationException;

    const-string p0, "Cancelled"

    invoke-direct {v2, p0}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 2355368
    return-void
.end method
