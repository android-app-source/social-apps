.class public Lcom/facebook/sideloading/SideloadingServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static k:LX/0Xm;


# instance fields
.field private final b:Landroid/content/res/Resources;

.field public final c:LX/EmQ;

.field public final d:LX/11H;

.field public final e:LX/2V3;

.field public final f:LX/0s1;

.field private final g:LX/EmO;

.field public final h:LX/Fk3;

.field private final i:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final j:LX/Fk5;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2278413
    const-class v0, Lcom/facebook/sideloading/SideloadingServiceHandler;

    sput-object v0, Lcom/facebook/sideloading/SideloadingServiceHandler;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/EmQ;LX/11H;LX/2V3;LX/0s1;LX/EmO;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/Fk3;LX/Fk5;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2278402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2278403
    iput-object p1, p0, Lcom/facebook/sideloading/SideloadingServiceHandler;->b:Landroid/content/res/Resources;

    .line 2278404
    iput-object p2, p0, Lcom/facebook/sideloading/SideloadingServiceHandler;->c:LX/EmQ;

    .line 2278405
    iput-object p3, p0, Lcom/facebook/sideloading/SideloadingServiceHandler;->d:LX/11H;

    .line 2278406
    iput-object p4, p0, Lcom/facebook/sideloading/SideloadingServiceHandler;->e:LX/2V3;

    .line 2278407
    iput-object p5, p0, Lcom/facebook/sideloading/SideloadingServiceHandler;->f:LX/0s1;

    .line 2278408
    iput-object p6, p0, Lcom/facebook/sideloading/SideloadingServiceHandler;->g:LX/EmO;

    .line 2278409
    iput-object p8, p0, Lcom/facebook/sideloading/SideloadingServiceHandler;->h:LX/Fk3;

    .line 2278410
    iput-object p7, p0, Lcom/facebook/sideloading/SideloadingServiceHandler;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2278411
    iput-object p9, p0, Lcom/facebook/sideloading/SideloadingServiceHandler;->j:LX/Fk5;

    .line 2278412
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/sideloading/SideloadingServiceHandler;
    .locals 13

    .prologue
    .line 2278414
    const-class v1, Lcom/facebook/sideloading/SideloadingServiceHandler;

    monitor-enter v1

    .line 2278415
    :try_start_0
    sget-object v0, Lcom/facebook/sideloading/SideloadingServiceHandler;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2278416
    sput-object v2, Lcom/facebook/sideloading/SideloadingServiceHandler;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2278417
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2278418
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2278419
    new-instance v3, Lcom/facebook/sideloading/SideloadingServiceHandler;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/EmQ;->a(LX/0QB;)LX/EmQ;

    move-result-object v5

    check-cast v5, LX/EmQ;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v6

    check-cast v6, LX/11H;

    invoke-static {v0}, LX/2V3;->b(LX/0QB;)LX/2V3;

    move-result-object v7

    check-cast v7, LX/2V3;

    invoke-static {v0}, LX/0s0;->a(LX/0QB;)LX/0s0;

    move-result-object v8

    check-cast v8, LX/0s1;

    invoke-static {v0}, LX/EmO;->a(LX/0QB;)LX/EmO;

    move-result-object v9

    check-cast v9, LX/EmO;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v10

    check-cast v10, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/Fk3;->b(LX/0QB;)LX/Fk3;

    move-result-object v11

    check-cast v11, LX/Fk3;

    invoke-static {v0}, LX/Fk5;->b(LX/0QB;)LX/Fk5;

    move-result-object v12

    check-cast v12, LX/Fk5;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/sideloading/SideloadingServiceHandler;-><init>(Landroid/content/res/Resources;LX/EmQ;LX/11H;LX/2V3;LX/0s1;LX/EmO;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/Fk3;LX/Fk5;)V

    .line 2278420
    move-object v0, v3

    .line 2278421
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2278422
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/sideloading/SideloadingServiceHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2278423
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2278424
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/sideloading/SideloadingServiceHandler;LX/Fk1;J)V
    .locals 6

    .prologue
    .line 2278396
    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 2278397
    const-string v0, "com.facebook.orca"

    .line 2278398
    iget-object v1, p0, Lcom/facebook/sideloading/SideloadingServiceHandler;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-static {v0}, LX/FkE;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v2

    invoke-interface {v1, v2, p2, p3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v1

    .line 2278399
    invoke-static {v0}, LX/FkE;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v2

    const-string v3, "download_url"

    invoke-virtual {v2, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v2

    check-cast v2, LX/0Tn;

    move-object v2, v2

    .line 2278400
    iget-object v3, p1, LX/Fk1;->c:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-static {v0}, LX/FkE;->f(Ljava/lang/String;)LX/0Tn;

    move-result-object v2

    iget-object v3, p1, LX/Fk1;->j:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-static {v0}, LX/FkE;->e(Ljava/lang/String;)LX/0Tn;

    move-result-object v2

    iget-wide v4, p1, LX/Fk1;->d:J

    invoke-interface {v1, v2, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v1

    invoke-static {v0}, LX/FkE;->g(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2278401
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/sideloading/SideloadingServiceHandler;Ljava/lang/String;Z)V
    .locals 12

    .prologue
    .line 2278350
    const/4 v1, 0x0

    .line 2278351
    :try_start_0
    new-instance v0, LX/Fk0;

    const-string v2, "ijxLJi1yGs1JpL-X1SExmchvork"

    const/4 v3, 0x0

    invoke-direct {v0, p1, v2, v3}, LX/Fk0;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2278352
    iget-object v2, p0, Lcom/facebook/sideloading/SideloadingServiceHandler;->j:LX/Fk5;

    const-string v3, "sideloading_send_request_to_oxygen"

    invoke-virtual {v2, v3}, LX/Fk5;->a(Ljava/lang/String;)V

    .line 2278353
    iget-object v2, p0, Lcom/facebook/sideloading/SideloadingServiceHandler;->d:LX/11H;

    iget-object v3, p0, Lcom/facebook/sideloading/SideloadingServiceHandler;->e:LX/2V3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    invoke-virtual {v2, v3, v0, v4}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fk1;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 2278354
    :goto_0
    move-object v0, v0

    .line 2278355
    iget-object v1, p0, Lcom/facebook/sideloading/SideloadingServiceHandler;->b:Landroid/content/res/Resources;

    const v2, 0x7f0832ca

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2278356
    if-eqz v0, :cond_0

    .line 2278357
    iget-object v1, v0, LX/Fk1;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2278358
    iget-object v1, v0, LX/Fk1;->b:LX/FQV;

    invoke-virtual {v1}, LX/FQV;->name()Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2278359
    iget-object v1, p0, Lcom/facebook/sideloading/SideloadingServiceHandler;->j:LX/Fk5;

    const-string v2, "sideloading_queue_download"

    invoke-virtual {v1, v2}, LX/Fk5;->a(Ljava/lang/String;)V

    .line 2278360
    iget-object v2, v0, LX/Fk1;->c:Ljava/lang/String;

    iget-object v1, v0, LX/Fk1;->b:LX/FQV;

    invoke-virtual {v1}, LX/FQV;->asInt()I

    move-result v3

    iget-wide v4, v0, LX/Fk1;->d:J

    move-object v1, p0

    move v7, p2

    .line 2278361
    invoke-static {}, LX/EmL;->a()LX/EmL;

    move-result-object v8

    sget-object v9, LX/EmM;->SIDELOADING:LX/EmM;

    .line 2278362
    iput-object v9, v8, LX/EmL;->a:LX/EmM;

    .line 2278363
    move-object v8, v8

    .line 2278364
    iput-object v2, v8, LX/EmL;->b:Ljava/lang/String;

    .line 2278365
    move-object v8, v8

    .line 2278366
    invoke-static {v3}, LX/EmO;->a(I)LX/EmK;

    move-result-object v9

    .line 2278367
    iput-object v9, v8, LX/EmL;->c:LX/EmK;

    .line 2278368
    move-object v8, v8

    .line 2278369
    iput-object v6, v8, LX/EmL;->d:Ljava/lang/String;

    .line 2278370
    move-object v8, v8

    .line 2278371
    iput-wide v4, v8, LX/EmL;->e:J

    .line 2278372
    move-object v8, v8

    .line 2278373
    iput-boolean v7, v8, LX/EmL;->f:Z

    .line 2278374
    move-object v8, v8

    .line 2278375
    iget-object v9, v1, Lcom/facebook/sideloading/SideloadingServiceHandler;->f:LX/0s1;

    invoke-interface {v9}, LX/0s1;->d()Ljava/lang/String;

    move-result-object v9

    .line 2278376
    iput-object v9, v8, LX/EmL;->h:Ljava/lang/String;

    .line 2278377
    move-object v8, v8

    .line 2278378
    invoke-virtual {v8}, LX/EmL;->b()LX/EmN;

    move-result-object v8

    .line 2278379
    iget-object v9, v1, Lcom/facebook/sideloading/SideloadingServiceHandler;->c:LX/EmQ;

    invoke-virtual {v9, v8}, LX/EmQ;->a(LX/EmN;)J

    move-result-wide v8

    move-wide v2, v8

    .line 2278380
    invoke-static {p0, v0, v2, v3}, Lcom/facebook/sideloading/SideloadingServiceHandler;->a(Lcom/facebook/sideloading/SideloadingServiceHandler;LX/Fk1;J)V

    .line 2278381
    :cond_0
    return-void

    .line 2278382
    :catch_0
    move-object v0, v1

    goto :goto_0

    .line 2278383
    :catch_1
    move-object v0, v1

    goto :goto_0

    .line 2278384
    :catch_2
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2278385
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2278386
    const-string v1, "sideload_app"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2278387
    iget-object v0, p0, Lcom/facebook/sideloading/SideloadingServiceHandler;->h:LX/Fk3;

    invoke-virtual {v0}, LX/Fk3;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2278388
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2278389
    const-string v1, "package_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2278390
    const-string v2, "force_download"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2278391
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {p0, v1, v0}, Lcom/facebook/sideloading/SideloadingServiceHandler;->a(Lcom/facebook/sideloading/SideloadingServiceHandler;Ljava/lang/String;Z)V

    .line 2278392
    :cond_0
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2278393
    move-object v0, v0

    .line 2278394
    return-object v0

    .line 2278395
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
