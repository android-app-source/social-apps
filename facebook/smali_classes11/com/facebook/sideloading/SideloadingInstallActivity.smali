.class public Lcom/facebook/sideloading/SideloadingInstallActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field private static final t:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public p:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0V8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/Fk5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2278145
    const-class v0, Lcom/facebook/sideloading/SideloadingInstallActivity;

    sput-object v0, Lcom/facebook/sideloading/SideloadingInstallActivity;->t:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2278144
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2278118
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2278119
    const-string v1, "application/vnd.android.package-archive"

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 2278120
    return-object v0
.end method

.method private a(Landroid/net/Uri;Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 2278139
    invoke-static {p1}, Lcom/facebook/sideloading/SideloadingInstallActivity;->a(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 2278140
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2278141
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2278142
    iget-object v1, p0, Lcom/facebook/sideloading/SideloadingInstallActivity;->p:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2278143
    return-void
.end method

.method private static a(Lcom/facebook/sideloading/SideloadingInstallActivity;Lcom/facebook/content/SecureContextHelper;LX/0V8;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/Fk5;)V
    .locals 0

    .prologue
    .line 2278138
    iput-object p1, p0, Lcom/facebook/sideloading/SideloadingInstallActivity;->p:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, p0, Lcom/facebook/sideloading/SideloadingInstallActivity;->q:LX/0V8;

    iput-object p3, p0, Lcom/facebook/sideloading/SideloadingInstallActivity;->r:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p4, p0, Lcom/facebook/sideloading/SideloadingInstallActivity;->s:LX/Fk5;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/sideloading/SideloadingInstallActivity;

    invoke-static {v3}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v3}, LX/0V7;->a(LX/0QB;)LX/0V8;

    move-result-object v1

    check-cast v1, LX/0V8;

    invoke-static {v3}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v3}, LX/Fk5;->b(LX/0QB;)LX/Fk5;

    move-result-object v3

    check-cast v3, LX/Fk5;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/sideloading/SideloadingInstallActivity;->a(Lcom/facebook/sideloading/SideloadingInstallActivity;Lcom/facebook/content/SecureContextHelper;LX/0V8;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/Fk5;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2278121
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2278122
    invoke-static {p0, p0}, Lcom/facebook/sideloading/SideloadingInstallActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2278123
    invoke-virtual {p0}, Lcom/facebook/sideloading/SideloadingInstallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2278124
    const-string v1, "local_uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2278125
    const-string v2, "package_name"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2278126
    const-string v3, "triggered_from"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2278127
    iget-object v0, p0, Lcom/facebook/sideloading/SideloadingInstallActivity;->r:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v2}, LX/FkE;->e(Ljava/lang/String;)LX/0Tn;

    move-result-object v2

    const-wide/32 v4, 0x1e00000

    invoke-interface {v0, v2, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 2278128
    iget-object v0, p0, Lcom/facebook/sideloading/SideloadingInstallActivity;->q:LX/0V8;

    sget-object v2, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v0, v2}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v6

    .line 2278129
    const-wide/16 v8, 0x2

    mul-long/2addr v4, v8

    cmp-long v0, v6, v4

    if-gez v0, :cond_0

    .line 2278130
    invoke-virtual {p0}, Lcom/facebook/sideloading/SideloadingInstallActivity;->finish()V

    .line 2278131
    :goto_0
    return-void

    .line 2278132
    :cond_0
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2278133
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2278134
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 2278135
    :cond_1
    invoke-direct {p0, v0, p0}, Lcom/facebook/sideloading/SideloadingInstallActivity;->a(Landroid/net/Uri;Landroid/app/Activity;)V

    .line 2278136
    iget-object v0, p0, Lcom/facebook/sideloading/SideloadingInstallActivity;->s:LX/Fk5;

    const-string v1, "sideloading_show_android_installer"

    const-string v2, "triggered_from"

    invoke-static {v2, v3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Fk5;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2278137
    invoke-virtual {p0}, Lcom/facebook/sideloading/SideloadingInstallActivity;->finish()V

    goto :goto_0
.end method
