.class public Lcom/facebook/adpreview/activity/AdPreviewActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/GNY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0hy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0pf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:Ljava/lang/String;

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2346120
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2346121
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/adpreview/activity/AdPreviewActivity;->z:Z

    return-void
.end method

.method private static a(Lcom/facebook/adpreview/activity/AdPreviewActivity;LX/0Zb;LX/03V;LX/GNY;LX/0tX;Lcom/facebook/content/SecureContextHelper;LX/1Ck;LX/17Y;LX/0hy;LX/0pf;)V
    .locals 0

    .prologue
    .line 2346119
    iput-object p1, p0, Lcom/facebook/adpreview/activity/AdPreviewActivity;->p:LX/0Zb;

    iput-object p2, p0, Lcom/facebook/adpreview/activity/AdPreviewActivity;->q:LX/03V;

    iput-object p3, p0, Lcom/facebook/adpreview/activity/AdPreviewActivity;->r:LX/GNY;

    iput-object p4, p0, Lcom/facebook/adpreview/activity/AdPreviewActivity;->s:LX/0tX;

    iput-object p5, p0, Lcom/facebook/adpreview/activity/AdPreviewActivity;->t:Lcom/facebook/content/SecureContextHelper;

    iput-object p6, p0, Lcom/facebook/adpreview/activity/AdPreviewActivity;->u:LX/1Ck;

    iput-object p7, p0, Lcom/facebook/adpreview/activity/AdPreviewActivity;->v:LX/17Y;

    iput-object p8, p0, Lcom/facebook/adpreview/activity/AdPreviewActivity;->w:LX/0hy;

    iput-object p9, p0, Lcom/facebook/adpreview/activity/AdPreviewActivity;->x:LX/0pf;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/adpreview/activity/AdPreviewActivity;

    invoke-static {v9}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {v9}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {v9}, LX/GNY;->c(LX/0QB;)LX/GNY;

    move-result-object v3

    check-cast v3, LX/GNY;

    invoke-static {v9}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v9}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v9}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {v9}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v7

    check-cast v7, LX/17Y;

    invoke-static {v9}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v8

    check-cast v8, LX/0hy;

    invoke-static {v9}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v9

    check-cast v9, LX/0pf;

    invoke-static/range {v0 .. v9}, Lcom/facebook/adpreview/activity/AdPreviewActivity;->a(Lcom/facebook/adpreview/activity/AdPreviewActivity;LX/0Zb;LX/03V;LX/GNY;LX/0tX;Lcom/facebook/content/SecureContextHelper;LX/1Ck;LX/17Y;LX/0hy;LX/0pf;)V

    return-void
.end method

.method private static a(ZLjava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2346109
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2346110
    :cond_0
    :goto_0
    return v0

    .line 2346111
    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2346112
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2346113
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 2346114
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2346115
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2346116
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-le v1, v2, :cond_2

    .line 2346117
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 2346118
    :cond_2
    if-eqz p0, :cond_3

    const-string v0, "ads/hype_ad?"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :cond_3
    const-string v0, "ads/mobile_preview?"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2346085
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2346086
    invoke-static {p0, p0}, Lcom/facebook/adpreview/activity/AdPreviewActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2346087
    invoke-virtual {p0}, Lcom/facebook/adpreview/activity/AdPreviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2346088
    if-nez v0, :cond_0

    .line 2346089
    invoke-virtual {p0}, Lcom/facebook/adpreview/activity/AdPreviewActivity;->finish()V

    .line 2346090
    :goto_0
    return-void

    .line 2346091
    :cond_0
    const-string v1, "extra_launch_uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2346092
    const-string v2, "is_hype_ad_unit"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 2346093
    invoke-static {v0, v1}, Lcom/facebook/adpreview/activity/AdPreviewActivity;->a(ZLjava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2346094
    invoke-virtual {p0}, Lcom/facebook/adpreview/activity/AdPreviewActivity;->finish()V

    goto :goto_0

    .line 2346095
    :cond_1
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2346096
    if-eqz v0, :cond_3

    .line 2346097
    const-string v0, "preview_id"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adpreview/activity/AdPreviewActivity;->y:Ljava/lang/String;

    .line 2346098
    const-string v0, "open_attachment"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/adpreview/activity/AdPreviewActivity;->z:Z

    .line 2346099
    :goto_2
    iget-object v0, p0, Lcom/facebook/adpreview/activity/AdPreviewActivity;->y:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 2346100
    invoke-virtual {p0}, Lcom/facebook/adpreview/activity/AdPreviewActivity;->finish()V

    goto :goto_0

    .line 2346101
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2346102
    :cond_3
    invoke-virtual {v1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adpreview/activity/AdPreviewActivity;->y:Ljava/lang/String;

    goto :goto_2

    .line 2346103
    :cond_4
    iget-object v0, p0, Lcom/facebook/adpreview/activity/AdPreviewActivity;->u:LX/1Ck;

    iget-object v1, p0, Lcom/facebook/adpreview/activity/AdPreviewActivity;->y:Ljava/lang/String;

    new-instance v2, LX/GNT;

    invoke-direct {v2, p0}, LX/GNT;-><init>(Lcom/facebook/adpreview/activity/AdPreviewActivity;)V

    new-instance v3, LX/GNU;

    invoke-direct {v3, p0}, LX/GNU;-><init>(Lcom/facebook/adpreview/activity/AdPreviewActivity;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x67279809

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2346106
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2346107
    iget-object v1, p0, Lcom/facebook/adpreview/activity/AdPreviewActivity;->u:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2346108
    const/16 v1, 0x23

    const v2, -0x140dc3c5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x165ac5c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2346104
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2346105
    const/16 v1, 0x23

    const v2, 0x6500de28

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
