.class public final Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x40f3a0b2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2350057
    const-class v0, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2350056
    const-class v0, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2350054
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2350055
    return-void
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCurrentRegionTosLinks"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2350052
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2350053
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2350040
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2350041
    invoke-virtual {p0}, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->a()Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2350042
    invoke-direct {p0}, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->l()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x2f307acc

    invoke-static {v2, v1, v3}, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2350043
    invoke-virtual {p0}, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 2350044
    invoke-virtual {p0}, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 2350045
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2350046
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2350047
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2350048
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2350049
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2350050
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2350051
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2350018
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2350019
    invoke-direct {p0}, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2350020
    invoke-direct {p0}, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x2f307acc

    invoke-static {v2, v0, v3}, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2350021
    invoke-direct {p0}, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2350022
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;

    .line 2350023
    iput v3, v0, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->f:I

    .line 2350024
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2350025
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 2350026
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 2350027
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2350058
    iget-object v0, p0, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    iput-object v0, p0, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    .line 2350059
    iget-object v0, p0, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2350037
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2350038
    const/4 v0, 0x1

    const v1, -0x2f307acc

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->f:I

    .line 2350039
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2350034
    new-instance v0, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;

    invoke-direct {v0}, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;-><init>()V

    .line 2350035
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2350036
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2350033
    const v0, 0x78814714

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2350032
    const v0, -0x6d34a14a

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2350030
    iget-object v0, p0, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->g:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    iput-object v0, p0, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->g:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    .line 2350031
    iget-object v0, p0, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->g:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2350028
    iget-object v0, p0, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->h:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    iput-object v0, p0, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->h:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    .line 2350029
    iget-object v0, p0, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->h:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    return-object v0
.end method
