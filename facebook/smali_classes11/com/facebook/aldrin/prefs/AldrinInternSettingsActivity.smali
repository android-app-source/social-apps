.class public Lcom/facebook/aldrin/prefs/AldrinInternSettingsActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""


# instance fields
.field public a:LX/GQW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2350075
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/aldrin/prefs/AldrinInternSettingsActivity;

    invoke-static {v0}, LX/GQW;->a(LX/0QB;)LX/GQW;

    move-result-object v0

    check-cast v0, LX/GQW;

    iput-object v0, p0, Lcom/facebook/aldrin/prefs/AldrinInternSettingsActivity;->a:LX/GQW;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2350064
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->b(Landroid/os/Bundle;)V

    .line 2350065
    invoke-static {p0, p0}, Lcom/facebook/aldrin/prefs/AldrinInternSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2350066
    const-string v0, "Aldrin Internal Settings"

    invoke-virtual {p0, v0}, Lcom/facebook/aldrin/prefs/AldrinInternSettingsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 2350067
    invoke-virtual {p0}, Lcom/facebook/aldrin/prefs/AldrinInternSettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 2350068
    invoke-virtual {p0, v0}, Lcom/facebook/aldrin/prefs/AldrinInternSettingsActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 2350069
    new-instance v1, LX/GQO;

    invoke-direct {v1, p0}, LX/GQO;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2350070
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2350071
    const-string v1, "Launch Transition Activity"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2350072
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/aldrin/transition/activity/AldrinTransitionActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 2350073
    iget-object v1, p0, Lcom/facebook/aldrin/prefs/AldrinInternSettingsActivity;->a:LX/GQW;

    invoke-virtual {v1}, LX/GQW;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 2350074
    return-void
.end method
