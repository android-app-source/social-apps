.class public Lcom/facebook/aldrin/transition/TosTransitionFragmentController;
.super Lcom/facebook/base/fragment/AbstractNavigableFragmentController;
.source ""


# instance fields
.field public a:LX/GQW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/GQh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2350221
    invoke-direct {p0}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/aldrin/transition/TosTransitionFragmentController;LX/GQW;LX/GQh;)V
    .locals 0

    .prologue
    .line 2350222
    iput-object p1, p0, Lcom/facebook/aldrin/transition/TosTransitionFragmentController;->a:LX/GQW;

    iput-object p2, p0, Lcom/facebook/aldrin/transition/TosTransitionFragmentController;->b:LX/GQh;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/aldrin/transition/TosTransitionFragmentController;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/aldrin/transition/TosTransitionFragmentController;

    invoke-static {v1}, LX/GQW;->a(LX/0QB;)LX/GQW;

    move-result-object v0

    check-cast v0, LX/GQW;

    invoke-static {v1}, LX/GQh;->a(LX/0QB;)LX/GQh;

    move-result-object v1

    check-cast v1, LX/GQh;

    invoke-static {p0, v0, v1}, Lcom/facebook/aldrin/transition/TosTransitionFragmentController;->a(Lcom/facebook/aldrin/transition/TosTransitionFragmentController;LX/GQW;LX/GQh;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/aldrin/transition/TosTransitionFragmentController;LX/GQZ;)V
    .locals 1

    .prologue
    .line 2350223
    invoke-virtual {p1}, LX/GQZ;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2350224
    if-eqz v0, :cond_0

    .line 2350225
    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->a(Landroid/content/Intent;)V

    .line 2350226
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2350227
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->a(Landroid/os/Bundle;)V

    .line 2350228
    const-class v0, Lcom/facebook/aldrin/transition/TosTransitionFragmentController;

    invoke-static {v0, p0}, Lcom/facebook/aldrin/transition/TosTransitionFragmentController;->a(Ljava/lang/Class;LX/02k;)V

    .line 2350229
    iget-object v0, p0, Lcom/facebook/aldrin/transition/TosTransitionFragmentController;->a:LX/GQW;

    invoke-virtual {v0}, LX/GQW;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2350230
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2350231
    :goto_0
    return-void

    .line 2350232
    :cond_0
    iget-object v0, p0, Lcom/facebook/aldrin/transition/TosTransitionFragmentController;->b:LX/GQh;

    new-instance v1, LX/GQY;

    invoke-direct {v1, p0}, LX/GQY;-><init>(Lcom/facebook/aldrin/transition/TosTransitionFragmentController;)V

    .line 2350233
    iget-object p1, v0, LX/GQh;->a:LX/HcO;

    .line 2350234
    iput-object v1, p1, LX/HcO;->e:LX/GQX;

    .line 2350235
    iget-object v0, p0, Lcom/facebook/aldrin/transition/TosTransitionFragmentController;->b:LX/GQh;

    .line 2350236
    iget-object v1, v0, LX/GQh;->a:LX/HcO;

    .line 2350237
    iget-object v0, v1, LX/HcO;->b:Ljava/lang/Object;

    move-object v1, v0

    .line 2350238
    check-cast v1, LX/GQZ;

    invoke-virtual {v1}, LX/GQZ;->getIntent()Landroid/content/Intent;

    move-result-object v1

    move-object v0, v1

    .line 2350239
    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method
