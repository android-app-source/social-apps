.class public final Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;)V
    .locals 0

    .prologue
    .line 2350343
    iput-object p1, p0, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment$2;->a:Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x4c1f74c7    # 4.1800476E7f

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2350344
    iget-object v1, p0, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment$2;->a:Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;

    .line 2350345
    iget-object v3, v1, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;->b:LX/2YW;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->ALDRIN:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    sget-object p0, LX/GQU;->DENIED:LX/GQU;

    iget-object p1, v1, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;->g:Lcom/facebook/aldrin/status/AldrinUserStatus;

    iget-object p1, p1, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosVersion:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, p0, p1}, LX/2YW;->a(Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;LX/GQU;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2350346
    iget-object v3, v1, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;->d:LX/GQh;

    sget-object v4, LX/GQf;->DENIED_ALDRIN_TOS:LX/GQf;

    invoke-virtual {v3, v4}, LX/GQh;->a(LX/GQf;)V

    .line 2350347
    const v1, 0x63550347

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
