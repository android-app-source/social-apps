.class public Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment;
.super Lcom/facebook/base/fragment/AbstractNavigableFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public b:LX/2YW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/GQh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2350381
    invoke-direct {p0}, Lcom/facebook/base/fragment/AbstractNavigableFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2350382
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->a(Landroid/os/Bundle;)V

    .line 2350383
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment;

    invoke-static {v0}, LX/2YW;->b(LX/0QB;)LX/2YW;

    move-result-object v2

    check-cast v2, LX/2YW;

    invoke-static {v0}, LX/GQh;->a(LX/0QB;)LX/GQh;

    move-result-object v3

    check-cast v3, LX/GQh;

    invoke-static {v0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v0

    check-cast v0, LX/0kL;

    iput-object v2, p0, Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment;->b:LX/2YW;

    iput-object v3, p0, Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment;->c:LX/GQh;

    iput-object p1, p0, Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment;->d:Ljava/util/concurrent/ExecutorService;

    iput-object v0, p0, Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment;->e:LX/0kL;

    .line 2350384
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x3881c6b2

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2350385
    const v1, 0x7f03068a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x1c8f2da7

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2350386
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2350387
    const v0, 0x7f0d11ed

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment;->f:Landroid/view/View;

    .line 2350388
    iget-object v0, p0, Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment;->f:Landroid/view/View;

    new-instance v1, Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment$1;

    invoke-direct {v1, p0}, Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment$1;-><init>(Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2350389
    return-void
.end method
