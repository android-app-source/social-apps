.class public Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;
.super Lcom/facebook/base/fragment/AbstractNavigableFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public b:LX/2YW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2Dg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/GQh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/aldrin/status/AldrinUserStatus;

.field public h:Landroid/view/View;

.field public i:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2350363
    invoke-direct {p0}, Lcom/facebook/base/fragment/AbstractNavigableFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2350364
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->a(Landroid/os/Bundle;)V

    .line 2350365
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;

    invoke-static {v0}, LX/2YW;->b(LX/0QB;)LX/2YW;

    move-result-object v3

    check-cast v3, LX/2YW;

    invoke-static {v0}, LX/2Dg;->a(LX/0QB;)LX/2Dg;

    move-result-object v4

    check-cast v4, LX/2Dg;

    invoke-static {v0}, LX/GQh;->a(LX/0QB;)LX/GQh;

    move-result-object v5

    check-cast v5, LX/GQh;

    invoke-static {v0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v0

    check-cast v0, LX/0kL;

    iput-object v3, v2, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;->b:LX/2YW;

    iput-object v4, v2, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;->c:LX/2Dg;

    iput-object v5, v2, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;->d:LX/GQh;

    iput-object p1, v2, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;->e:Ljava/util/concurrent/ExecutorService;

    iput-object v0, v2, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;->f:LX/0kL;

    .line 2350366
    iget-object v0, p0, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;->c:LX/2Dg;

    invoke-virtual {v0}, LX/2Dg;->d()Lcom/facebook/aldrin/status/AldrinUserStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;->g:Lcom/facebook/aldrin/status/AldrinUserStatus;

    .line 2350367
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x6e94645a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2350362
    const v1, 0x7f030688

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x19471502

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2350356
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2350357
    const v0, 0x7f0d11e8

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;->h:Landroid/view/View;

    .line 2350358
    const v0, 0x7f0d11e9

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;->i:Landroid/view/View;

    .line 2350359
    iget-object v0, p0, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;->h:Landroid/view/View;

    new-instance v1, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment$1;

    invoke-direct {v1, p0}, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment$1;-><init>(Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2350360
    iget-object v0, p0, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;->i:Landroid/view/View;

    new-instance v1, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment$2;

    invoke-direct {v1, p0}, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment$2;-><init>(Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2350361
    return-void
.end method
