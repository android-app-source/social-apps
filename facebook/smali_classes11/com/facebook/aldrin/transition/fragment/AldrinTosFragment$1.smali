.class public final Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;)V
    .locals 0

    .prologue
    .line 2350336
    iput-object p1, p0, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment$1;->a:Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x4ba7cb8b

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2350337
    iget-object v1, p0, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment$1;->a:Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;

    const/4 v4, 0x0

    .line 2350338
    iget-object v3, v1, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;->h:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 2350339
    iget-object v3, v1, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;->i:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 2350340
    iget-object v3, v1, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;->b:LX/2YW;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->ALDRIN:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    sget-object p0, LX/GQU;->AGREED:LX/GQU;

    iget-object p1, v1, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;->g:Lcom/facebook/aldrin/status/AldrinUserStatus;

    iget-object p1, p1, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosVersion:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, p0, p1}, LX/2YW;->a(Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;LX/GQU;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2350341
    new-instance v4, LX/GQi;

    invoke-direct {v4, v1}, LX/GQi;-><init>(Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;)V

    iget-object v5, v1, Lcom/facebook/aldrin/transition/fragment/AldrinTosFragment;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2350342
    const v1, -0x204a9a73

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
