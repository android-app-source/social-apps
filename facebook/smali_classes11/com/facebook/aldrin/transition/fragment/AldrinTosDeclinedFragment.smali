.class public Lcom/facebook/aldrin/transition/fragment/AldrinTosDeclinedFragment;
.super Lcom/facebook/base/fragment/AbstractNavigableFragment;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/GQh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/GvB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2350335
    invoke-direct {p0}, Lcom/facebook/base/fragment/AbstractNavigableFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2350332
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->a(Landroid/os/Bundle;)V

    .line 2350333
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/aldrin/transition/fragment/AldrinTosDeclinedFragment;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/GQh;->a(LX/0QB;)LX/GQh;

    move-result-object p1

    check-cast p1, LX/GQh;

    invoke-static {v0}, LX/GvB;->b(LX/0QB;)LX/GvB;

    move-result-object v0

    check-cast v0, LX/GvB;

    iput-object v2, p0, Lcom/facebook/aldrin/transition/fragment/AldrinTosDeclinedFragment;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object p1, p0, Lcom/facebook/aldrin/transition/fragment/AldrinTosDeclinedFragment;->c:LX/GQh;

    iput-object v0, p0, Lcom/facebook/aldrin/transition/fragment/AldrinTosDeclinedFragment;->d:LX/GvB;

    .line 2350334
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x48b9c13d

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2350320
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 2350321
    const v2, 0x7f0d11ec

    if-ne v1, v2, :cond_1

    .line 2350322
    iget-object v1, p0, Lcom/facebook/aldrin/transition/fragment/AldrinTosDeclinedFragment;->c:LX/GQh;

    sget-object v2, LX/GQf;->GO_BACK:LX/GQf;

    invoke-virtual {v1, v2}, LX/GQh;->a(LX/GQf;)V

    .line 2350323
    :cond_0
    :goto_0
    const v1, -0x578742f3

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2350324
    :cond_1
    const v2, 0x7f0d11eb

    if-ne v1, v2, :cond_2

    .line 2350325
    iget-object v1, p0, Lcom/facebook/aldrin/transition/fragment/AldrinTosDeclinedFragment;->d:LX/GvB;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/GvB;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 2350326
    :cond_2
    const v2, 0x7f0d11ea

    if-ne v1, v2, :cond_0

    .line 2350327
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2350328
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2350329
    const-string v3, "android.intent.category.HOME"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 2350330
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2350331
    iget-object v3, p0, Lcom/facebook/aldrin/transition/fragment/AldrinTosDeclinedFragment;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v2, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0xcc0d2a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2350319
    const v1, 0x7f030689

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x50e6b2e1

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2350314
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2350315
    const v0, 0x7f0d11ea

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2350316
    const v0, 0x7f0d11eb

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2350317
    const v0, 0x7f0d11ec

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2350318
    return-void
.end method
