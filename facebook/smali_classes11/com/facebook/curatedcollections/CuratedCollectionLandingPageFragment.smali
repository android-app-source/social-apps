.class public Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->CURATED_COLLECTION_LANDING_FRAGMENT:LX/0cQ;
.end annotation


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Db;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/1DS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/Gae;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/1Ck;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/3E1;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:Ljava/lang/String;

.field public j:LX/Gam;

.field public k:LX/1Qq;

.field public l:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public m:Landroid/widget/ProgressBar;

.field private n:LX/1P0;

.field private o:LX/0g7;

.field private p:LX/Gai;

.field public q:LX/162;

.field public r:Z

.field public s:Z

.field public t:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2368699
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2368700
    return-void
.end method

.method public static a$redex0(Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2368622
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->r:Z

    .line 2368623
    invoke-virtual {p0}, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->a()Ljava/lang/String;

    .line 2368624
    new-instance v0, LX/GQn;

    invoke-direct {v0}, LX/GQn;-><init>()V

    move-object v0, v0

    .line 2368625
    const-string v1, "collection_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "feed_story_render_location"

    const-string v2, "curated_collection_page"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "first_count"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/GQn;

    .line 2368626
    iget-object v1, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->t:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2368627
    const-string v1, "after_cursor"

    iget-object v2, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2368628
    :cond_0
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2368629
    iget-object v1, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->f:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2368630
    iget-object v1, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->e:LX/1Ck;

    sget-object v2, LX/Gaj;->FETCH_ALL:LX/Gaj;

    iget-object v3, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->p:LX/Gai;

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2368631
    return-void
.end method

.method public static c(Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;)LX/1Qq;
    .locals 7

    .prologue
    .line 2368688
    iget-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->k:LX/1Qq;

    if-eqz v0, :cond_0

    .line 2368689
    iget-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->k:LX/1Qq;

    .line 2368690
    :goto_0
    return-object v0

    .line 2368691
    :cond_0
    iget-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->d:LX/Gae;

    invoke-virtual {p0}, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2368692
    sget-object v3, LX/Gaf;->a:LX/Gaf;

    move-object v3, v3

    .line 2368693
    new-instance v4, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment$2;

    invoke-direct {v4, p0}, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment$2;-><init>(Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;)V

    iget-object v5, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->o:LX/0g7;

    invoke-static {v5}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v5

    new-instance v6, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment$3;

    invoke-direct {v6, p0}, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment$3;-><init>(Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;)V

    invoke-virtual/range {v0 .. v6}, LX/Gae;->a(Ljava/lang/String;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;Ljava/lang/Runnable;)LX/Gad;

    move-result-object v0

    .line 2368694
    iget-object v1, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->c:LX/1DS;

    iget-object v2, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->a:LX/0Ot;

    iget-object v3, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->j:LX/Gam;

    invoke-virtual {v1, v2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    .line 2368695
    iput-object v0, v1, LX/1Ql;->f:LX/1PW;

    .line 2368696
    move-object v0, v1

    .line 2368697
    new-instance v1, LX/Gah;

    invoke-direct {v1, p0}, LX/Gah;-><init>(Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;)V

    invoke-virtual {v0, v1}, LX/1Ql;->b(LX/99g;)LX/1Ql;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->k:LX/1Qq;

    .line 2368698
    iget-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->k:LX/1Qq;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2368687
    const-string v0, "curated_collection"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2368680
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2368681
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    const/16 v3, 0x6bd

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v4

    check-cast v4, LX/1Db;

    invoke-static {v0}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v5

    check-cast v5, LX/1DS;

    const-class v6, LX/Gae;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/Gae;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    const/16 p1, 0x122d

    invoke-static {v0, p1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    invoke-static {v0}, LX/3E1;->b(LX/0QB;)LX/3E1;

    move-result-object v0

    check-cast v0, LX/3E1;

    iput-object v3, v2, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->a:LX/0Ot;

    iput-object v4, v2, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->b:LX/1Db;

    iput-object v5, v2, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->c:LX/1DS;

    iput-object v6, v2, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->d:LX/Gae;

    iput-object v7, v2, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->e:LX/1Ck;

    iput-object v8, v2, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->f:LX/0tX;

    iput-object p1, v2, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->g:LX/0Or;

    iput-object v0, v2, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->h:LX/3E1;

    .line 2368682
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2368683
    const-string v1, "collection_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->i:Ljava/lang/String;

    .line 2368684
    new-instance v0, LX/Gam;

    invoke-direct {v0}, LX/Gam;-><init>()V

    iput-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->j:LX/Gam;

    .line 2368685
    new-instance v0, LX/Gai;

    invoke-direct {v0, p0}, LX/Gai;-><init>(Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;)V

    iput-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->p:LX/Gai;

    .line 2368686
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2368672
    iget-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->j:LX/Gam;

    .line 2368673
    iget-object v2, v0, LX/Gam;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2368674
    iput-boolean v1, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->s:Z

    .line 2368675
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->t:Ljava/lang/String;

    .line 2368676
    iput-boolean v1, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->r:Z

    .line 2368677
    iget-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->i:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->a$redex0(Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;Ljava/lang/String;)V

    .line 2368678
    invoke-virtual {p0}, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->a()Ljava/lang/String;

    .line 2368679
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v0, 0x2a

    const v1, -0x44219ea8

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2368657
    const v0, 0x7f0303bb

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2368658
    const v0, 0x7f0d04de

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->m:Landroid/widget/ProgressBar;

    .line 2368659
    iget-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->m:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 2368660
    const v0, 0x7f0d0bc6

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->l:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2368661
    iget-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->l:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2368662
    new-instance v0, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/1P0;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->n:LX/1P0;

    .line 2368663
    iget-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->l:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->n:LX/1P0;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2368664
    new-instance v0, LX/0g7;

    iget-object v3, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->l:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-direct {v0, v3}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->o:LX/0g7;

    .line 2368665
    iget-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->o:LX/0g7;

    invoke-virtual {v0, v4}, LX/0g7;->b(Z)V

    .line 2368666
    iget-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->o:LX/0g7;

    invoke-virtual {v0, v5}, LX/0g7;->d(Z)V

    .line 2368667
    iget-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->o:LX/0g7;

    new-instance v3, LX/Gag;

    invoke-direct {v3, p0}, LX/Gag;-><init>(Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;)V

    invoke-virtual {v0, v3}, LX/0g7;->a(LX/0fx;)V

    .line 2368668
    iget-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->o:LX/0g7;

    invoke-static {p0}, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->c(Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;)LX/1Qq;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0g7;->a(Landroid/widget/ListAdapter;)V

    .line 2368669
    iget-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->o:LX/0g7;

    iget-object v3, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->b:LX/1Db;

    invoke-virtual {v3}, LX/1Db;->a()LX/1St;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0g7;->a(LX/1St;)V

    .line 2368670
    invoke-virtual {p0}, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->b()V

    .line 2368671
    const/16 v0, 0x2b

    const v3, -0x26f909f1

    invoke-static {v6, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x411e0a5a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2368653
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2368654
    iget-object v1, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->k:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2368655
    iget-object v1, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->e:LX/1Ck;

    sget-object v2, LX/Gaj;->FETCH_ALL:LX/Gaj;

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2368656
    const/16 v1, 0x2b

    const v2, 0x333b94af

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x27e03343

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2368649
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2368650
    iget-object v1, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->q:LX/162;

    if-eqz v1, :cond_0

    .line 2368651
    iget-object v1, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->h:LX/3E1;

    iget-object v2, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->q:LX/162;

    invoke-virtual {p0}, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/3E1;->a(LX/162;Ljava/lang/String;)V

    .line 2368652
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x35f3a600

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3be65f5e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2368646
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2368647
    iget-object v1, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->h:LX/3E1;

    invoke-virtual {v1}, LX/3E1;->b()V

    .line 2368648
    const/16 v1, 0x2b

    const v2, 0x19444bd9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2d8c075a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2368638
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2368639
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2368640
    if-nez v0, :cond_0

    .line 2368641
    const/16 v0, 0x2b

    const v2, 0x69d1a28c

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2368642
    :goto_0
    return-void

    .line 2368643
    :cond_0
    const v2, 0x7f083452

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2368644
    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/1ZF;->k_(Z)V

    .line 2368645
    const v0, -0x37b72eb1

    invoke-static {v0, v1}, LX/02F;->f(II)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2368632
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2368633
    iget-object v0, p0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v1, "364898053863491"

    .line 2368634
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 2368635
    move-object v0, v0

    .line 2368636
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gt;->a(Landroid/content/Context;)V

    .line 2368637
    return-void
.end method
