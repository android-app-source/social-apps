.class public Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0sK;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Gd5;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2373504
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2373495
    invoke-virtual {p0}, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 2373496
    new-instance v1, Landroid/preference/PreferenceCategory;

    invoke-direct {v1, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2373497
    const-string v2, "React OTA Update Info"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2373498
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2373499
    invoke-direct {p0, v1}, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;->a(Landroid/preference/PreferenceCategory;)V

    .line 2373500
    invoke-direct {p0, v1}, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;->b(Landroid/preference/PreferenceCategory;)V

    .line 2373501
    invoke-direct {p0, v1}, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;->c(Landroid/preference/PreferenceCategory;)V

    .line 2373502
    invoke-virtual {p0, v0}, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 2373503
    return-void
.end method

.method private a(Landroid/preference/PreferenceCategory;)V
    .locals 3

    .prologue
    .line 2373486
    iget-object v0, p0, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0sK;

    invoke-virtual {v0}, LX/0sK;->c()I

    move-result v0

    .line 2373487
    if-nez v0, :cond_0

    .line 2373488
    iget-object v0, p0, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gd5;

    .line 2373489
    iget v1, v0, LX/Gd5;->c:I

    move v0, v1

    .line 2373490
    :cond_0
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2373491
    const-string v2, "React Bundle Version"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2373492
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2373493
    invoke-virtual {p1, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2373494
    return-void
.end method

.method private static a(Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;LX/0Or;LX/0Or;LX/0Or;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;",
            "LX/0Or",
            "<",
            "LX/0sK;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Gd5;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2373485
    iput-object p1, p0, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;->c:LX/0Or;

    iput-object p4, p0, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;->d:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;

    const/16 v1, 0x538

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x1c35

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x1c36

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0, v1, v2, v3, v0}, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;->a(Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;LX/0Or;LX/0Or;LX/0Or;Ljava/util/concurrent/ExecutorService;)V

    return-void
.end method

.method private b(Landroid/preference/PreferenceCategory;)V
    .locals 5

    .prologue
    .line 2373449
    iget-object v0, p0, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0sK;

    invoke-virtual {v0}, LX/0sK;->c()I

    move-result v1

    .line 2373450
    if-nez v1, :cond_0

    .line 2373451
    const-string v0, "No test Bundle is found"

    .line 2373452
    :goto_0
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2373453
    const-string v2, "React Bundle Content Preview"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2373454
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2373455
    invoke-virtual {p1, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2373456
    return-void

    .line 2373457
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gd5;

    invoke-virtual {v0, v1}, LX/Gd5;->c(I)Ljava/io/File;

    move-result-object v2

    .line 2373458
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2373459
    const/4 v0, 0x0

    .line 2373460
    :try_start_0
    new-instance v1, Ljava/io/InputStreamReader;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-string v2, "UTF-8"

    invoke-direct {v1, v3, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2373461
    const/16 v0, 0x2a

    :try_start_1
    new-array v2, v0, [C

    .line 2373462
    invoke-virtual {v1, v2}, Ljava/io/Reader;->read([C)I

    move-result v0

    if-gez v0, :cond_1

    .line 2373463
    const-string v0, "Fail to read test bundle content"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2373464
    :goto_1
    invoke-static {v1}, LX/1md;->a(Ljava/io/Reader;)V

    goto :goto_0

    .line 2373465
    :cond_1
    :try_start_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([C)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    .line 2373466
    :catch_0
    :goto_2
    const-string v0, "Fail to read test bundle content"

    .line 2373467
    invoke-static {v1}, LX/1md;->a(Ljava/io/Reader;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_3
    invoke-static {v1}, LX/1md;->a(Ljava/io/Reader;)V

    throw v0

    .line 2373468
    :cond_2
    const-string v0, "Test Bundle is invalid"

    goto :goto_0

    .line 2373469
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 2373470
    :catch_1
    move-object v1, v0

    goto :goto_2
.end method

.method public static b(Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;)V
    .locals 3

    .prologue
    .line 2373483
    iget-object v0, p0, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity$2;

    invoke-direct {v1, p0}, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity$2;-><init>(Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;)V

    const v2, -0x594b40c8

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2373484
    return-void
.end method

.method private c(Landroid/preference/PreferenceCategory;)V
    .locals 2

    .prologue
    .line 2373477
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2373478
    new-instance v1, LX/GdH;

    invoke-direct {v1, p0}, LX/GdH;-><init>(Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2373479
    const-string v1, "Force OTA Update (Test Only)"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2373480
    const-string v1, "Have the best Facebook experience by using OTA NOW, click here and go to Marketplace to activate it."

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2373481
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2373482
    return-void
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2373473
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 2373474
    invoke-static {p0, p0}, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2373475
    invoke-direct {p0}, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;->a()V

    .line 2373476
    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x46463984

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2373471
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onStop()V

    .line 2373472
    const/16 v1, 0x23

    const v2, 0x72dfb9a1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
