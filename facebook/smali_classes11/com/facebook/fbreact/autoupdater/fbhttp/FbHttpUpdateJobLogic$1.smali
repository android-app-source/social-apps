.class public final Lcom/facebook/fbreact/autoupdater/fbhttp/FbHttpUpdateJobLogic$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/45o;

.field public final synthetic b:LX/GdB;


# direct methods
.method public constructor <init>(LX/GdB;LX/45o;)V
    .locals 0

    .prologue
    .line 2373264
    iput-object p1, p0, Lcom/facebook/fbreact/autoupdater/fbhttp/FbHttpUpdateJobLogic$1;->b:LX/GdB;

    iput-object p2, p0, Lcom/facebook/fbreact/autoupdater/fbhttp/FbHttpUpdateJobLogic$1;->a:LX/45o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2373265
    iget-object v0, p0, Lcom/facebook/fbreact/autoupdater/fbhttp/FbHttpUpdateJobLogic$1;->b:LX/GdB;

    iget-object v1, p0, Lcom/facebook/fbreact/autoupdater/fbhttp/FbHttpUpdateJobLogic$1;->a:LX/45o;

    .line 2373266
    :try_start_0
    iget-object v2, v0, LX/GdB;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0WJ;

    invoke-virtual {v2}, LX/0WJ;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2373267
    iget-object v2, v0, LX/GdB;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;

    invoke-virtual {v2}, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->a()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2373268
    :cond_0
    :goto_0
    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/45o;->a(Z)V

    .line 2373269
    return-void

    .line 2373270
    :catch_0
    move-exception v3

    .line 2373271
    const-string v2, "AutoUpdater"

    const-string p0, "Exception running auto-update service"

    invoke-static {v2, p0, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2373272
    iget-object v2, v0, LX/GdB;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    const-string p0, "AutoUpdater"

    invoke-virtual {v2, p0, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
