.class public final Lcom/facebook/fbreact/autoupdater/fbhttp/FbHttpUpdateService;
.super LX/1ZN;
.source ""


# instance fields
.field public a:Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2373298
    const-class v0, Lcom/facebook/fbreact/autoupdater/fbhttp/FbHttpUpdateService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 2373299
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/fbreact/autoupdater/fbhttp/FbHttpUpdateService;

    invoke-static {v0}, LX/GdC;->b(LX/0QB;)Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;

    iput-object v0, p0, Lcom/facebook/fbreact/autoupdater/fbhttp/FbHttpUpdateService;->a:Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x47222c66

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2373300
    iget-object v1, p0, Lcom/facebook/fbreact/autoupdater/fbhttp/FbHttpUpdateService;->a:Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;

    invoke-virtual {v1}, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->a()V

    .line 2373301
    const/16 v1, 0x25

    const v2, 0x3ebd45e5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x219cf18

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2373302
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 2373303
    invoke-static {p0, p0}, Lcom/facebook/fbreact/autoupdater/fbhttp/FbHttpUpdateService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2373304
    const/16 v1, 0x25

    const v2, -0x13f5f7c5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
