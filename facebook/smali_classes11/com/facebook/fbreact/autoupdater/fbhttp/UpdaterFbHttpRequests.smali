.class public Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2373386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2373387
    iput-object p1, p0, Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;->a:LX/0Or;

    .line 2373388
    iput-object p2, p0, Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;->b:LX/0Or;

    .line 2373389
    iput-object p3, p0, Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;->c:LX/0Or;

    .line 2373390
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;
    .locals 4

    .prologue
    .line 2373391
    new-instance v0, Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;

    const/16 v1, 0xb83

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0xb45

    invoke-static {p0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x259

    invoke-static {p0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;-><init>(LX/0Or;LX/0Or;LX/0Or;)V

    .line 2373392
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/io/File;)V
    .locals 10

    .prologue
    .line 2373393
    :try_start_0
    new-instance v9, Ljava/net/URL;

    invoke-direct {v9, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 2373394
    new-instance v4, Ljava/net/URI;

    invoke-virtual {v9}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9}, Ljava/net/URL;->getAuthority()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9}, Ljava/net/URL;->getRef()Ljava/lang/String;

    move-result-object v9

    invoke-direct/range {v4 .. v9}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2373395
    new-instance v5, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v5, v4}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v4, v5

    .line 2373396
    :goto_0
    move-object v0, v4

    .line 2373397
    if-nez v0, :cond_0

    .line 2373398
    :goto_1
    return-void

    .line 2373399
    :cond_0
    new-instance v1, LX/GdE;

    invoke-direct {v1, p2}, LX/GdE;-><init>(Ljava/io/File;)V

    .line 2373400
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v2

    const-string v3, "downloadOtaUpdate"

    .line 2373401
    iput-object v3, v2, LX/15E;->c:Ljava/lang/String;

    .line 2373402
    move-object v2, v2

    .line 2373403
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    .line 2373404
    iput-object v3, v2, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 2373405
    move-object v2, v2

    .line 2373406
    iput-object v0, v2, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 2373407
    move-object v0, v2

    .line 2373408
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2373409
    iput-object v2, v0, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2373410
    move-object v0, v0

    .line 2373411
    iput-object v1, v0, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 2373412
    move-object v0, v0

    .line 2373413
    invoke-virtual {v0}, LX/15E;->a()LX/15D;

    move-result-object v1

    .line 2373414
    iget-object v0, p0, Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v0, v1}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;)Ljava/lang/Object;

    goto :goto_1

    .line 2373415
    :catch_0
    move-exception v4

    move-object v5, v4

    .line 2373416
    :goto_2
    iget-object v4, p0, Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;->c:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/03V;

    const-string v6, "AutoUpdater"

    invoke-virtual {v4, v6, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2373417
    const-string v4, "AutoUpdater"

    const-string v6, "Problem parsing URL"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6, v7}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2373418
    const/4 v4, 0x0

    goto :goto_0

    .line 2373419
    :catch_1
    move-exception v4

    move-object v5, v4

    goto :goto_2
.end method
