.class public Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[I


# instance fields
.field private b:Landroid/content/Context;

.field public c:LX/0sK;

.field public d:LX/Gd2;

.field public e:LX/Gd5;

.field public f:Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2373149
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x2
        0x4
        0xc
        0x18
        0x20
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;LX/0sK;LX/Gd2;LX/Gd5;Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;)V
    .locals 0

    .prologue
    .line 2373142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2373143
    iput-object p1, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->b:Landroid/content/Context;

    .line 2373144
    iput-object p2, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->c:LX/0sK;

    .line 2373145
    iput-object p3, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->d:LX/Gd2;

    .line 2373146
    iput-object p4, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->e:LX/Gd5;

    .line 2373147
    iput-object p5, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->f:Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;

    .line 2373148
    return-void
.end method

.method private static a(Ljava/io/File;Ljava/io/File;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2373006
    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    .line 2373007
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2373008
    new-instance v2, Ljava/util/zip/ZipInputStream;

    new-instance v1, Ljava/io/BufferedInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v1}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 2373009
    const/4 v1, 0x0

    .line 2373010
    :goto_0
    :try_start_0
    invoke-virtual {v2}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2373011
    invoke-virtual {v3}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v3

    .line 2373012
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2373013
    invoke-static {v4, v2}, LX/Gd5;->a(Ljava/io/File;Ljava/io/InputStream;)V

    .line 2373014
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 2373015
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2373016
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_1
    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v2}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    throw v0

    :cond_0
    invoke-virtual {v2}, Ljava/util/zip/ZipInputStream;->close()V

    .line 2373017
    return-object v0

    .line 2373018
    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_1
    invoke-virtual {v2}, Ljava/util/zip/ZipInputStream;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;I)Z
    .locals 10

    .prologue
    .line 2373130
    iget-object v0, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->c:LX/0sK;

    const/4 v1, 0x0

    .line 2373131
    invoke-static {v0}, LX/0sK;->i(LX/0sK;)LX/0WS;

    move-result-object v2

    const-string v3, "download_start_ota_version"

    invoke-virtual {v2, v3, v1}, LX/0WS;->a(Ljava/lang/String;I)I

    move-result v2

    .line 2373132
    if-ne p1, v2, :cond_0

    .line 2373133
    invoke-static {v0}, LX/0sK;->i(LX/0sK;)LX/0WS;

    move-result-object v2

    const-string v3, "download_start_attempt_count"

    invoke-virtual {v2, v3, v1}, LX/0WS;->a(Ljava/lang/String;I)I

    move-result v2

    .line 2373134
    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2373135
    :cond_0
    move v0, v1

    .line 2373136
    sget-object v1, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->a:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2373137
    sget-object v1, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->a:[I

    aget v0, v1, v0

    .line 2373138
    iget-object v1, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->c:LX/0sK;

    .line 2373139
    invoke-static {v1}, LX/0sK;->i(LX/0sK;)LX/0WS;

    move-result-object v6

    const-string v7, "download_start_time"

    const-wide/16 v8, 0x0

    invoke-virtual {v6, v7, v8, v9}, LX/0WS;->a(Ljava/lang/String;J)J

    move-result-wide v6

    move-wide v2, v6

    .line 2373140
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 2373141
    sub-long v2, v4, v2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    int-to-long v4, v0

    invoke-virtual {v1, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    .line 2373019
    const/4 v8, 0x0

    .line 2373020
    iget-object v5, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->c:LX/0sK;

    .line 2373021
    invoke-static {v5}, LX/0sK;->i(LX/0sK;)LX/0WS;

    move-result-object v6

    const-string v7, "download_in_progress_ota_version"

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v9}, LX/0WS;->a(Ljava/lang/String;I)I

    move-result v6

    move v7, v6

    .line 2373022
    iget-object v5, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->c:LX/0sK;

    invoke-virtual {v5}, LX/0sK;->e()V

    .line 2373023
    if-lez v7, :cond_0

    .line 2373024
    new-instance v5, LX/2fU;

    const-string v6, ""

    const/4 v10, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v11

    move-object v9, v8

    invoke-direct/range {v5 .. v11}, LX/2fU;-><init>(Ljava/lang/String;ILjava/util/Date;Ljava/lang/String;ILjava/util/Map;)V

    .line 2373025
    new-instance v6, Ljava/lang/Throwable;

    const-string v7, "Job killed while downloading the update"

    invoke-direct {v6, v7}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    .line 2373026
    iget-object v7, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->d:LX/Gd2;

    invoke-virtual {v7, v5, v6}, LX/Gd2;->a(LX/2fU;Ljava/lang/Throwable;)V

    .line 2373027
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->f:Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;

    iget-object v1, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->b:Landroid/content/Context;

    .line 2373028
    new-instance v2, LX/GVn;

    invoke-direct {v2, v1}, LX/GVn;-><init>(Landroid/content/Context;)V

    .line 2373029
    iget-object v3, v2, LX/GVn;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2373030
    iget v4, v2, LX/GVn;->d:I

    move v2, v4

    .line 2373031
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "update"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v5, 0x7b

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "download_uri,"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "version_code,"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "published_date,"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "file_size,"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ota_bundle_type,"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "resources_checksum}"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 2373032
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2373033
    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "fields"

    invoke-direct {v6, v7, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2373034
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "version_name"

    invoke-direct {v4, v6, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2373035
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "version_code"

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2373036
    :try_start_0
    new-instance v3, LX/GdF;

    invoke-direct {v3}, LX/GdF;-><init>()V

    .line 2373037
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    .line 2373038
    iget-object v2, v0, Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/11H;

    invoke-virtual {v2, v3, v5, v4}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2fU;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2373039
    :goto_0
    move-object v1, v2

    .line 2373040
    if-eqz v1, :cond_1

    .line 2373041
    iget-object v0, v1, LX/2fU;->c:LX/Gd7;

    move-object v0, v0

    .line 2373042
    sget-object v2, LX/Gd7;->NOOP:LX/Gd7;

    if-ne v0, v2, :cond_2

    .line 2373043
    :cond_1
    :goto_1
    return-void

    .line 2373044
    :cond_2
    iget-object v0, v1, LX/2fU;->c:LX/Gd7;

    move-object v0, v0

    .line 2373045
    sget-object v2, LX/Gd7;->REVERT:LX/Gd7;

    if-ne v0, v2, :cond_4

    .line 2373046
    iget-object v0, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->c:LX/0sK;

    invoke-virtual {v0}, LX/0sK;->c()I

    move-result v0

    if-nez v0, :cond_3

    .line 2373047
    iget-object v0, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->c:LX/0sK;

    invoke-virtual {v0}, LX/0sK;->b()I

    move-result v0

    if-eqz v0, :cond_1

    .line 2373048
    iget-object v0, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->c:LX/0sK;

    invoke-virtual {v0}, LX/0sK;->h()V

    goto :goto_1

    .line 2373049
    :cond_3
    iget-object v0, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->c:LX/0sK;

    .line 2373050
    invoke-static {v0}, LX/0sK;->i(LX/0sK;)LX/0WS;

    move-result-object v1

    invoke-virtual {v1}, LX/0WS;->b()LX/1gW;

    move-result-object v1

    const-string v2, "next"

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, LX/1gW;->a(Ljava/lang/String;I)LX/1gW;

    move-result-object v1

    invoke-interface {v1}, LX/1gW;->c()V

    .line 2373051
    goto :goto_1

    .line 2373052
    :cond_4
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 2373053
    invoke-virtual {v1}, LX/2fU;->c()I

    move-result v3

    .line 2373054
    if-gtz v3, :cond_a

    .line 2373055
    const-string v4, "AutoUpdater"

    const-string v5, "Invalid build number %d"

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v2

    invoke-static {v4, v5, v6}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2373056
    :cond_5
    :goto_2
    move v0, v0

    .line 2373057
    if-nez v0, :cond_1

    .line 2373058
    invoke-virtual {v1}, LX/2fU;->b()Ljava/lang/String;

    move-result-object v0

    .line 2373059
    iget-object v5, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->c:LX/0sK;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    .line 2373060
    invoke-static {v5}, LX/0sK;->i(LX/0sK;)LX/0WS;

    move-result-object v6

    invoke-virtual {v6}, LX/0WS;->b()LX/1gW;

    move-result-object v6

    const-string v9, "download_start_time"

    invoke-interface {v6, v9, v7, v8}, LX/1gW;->a(Ljava/lang/String;J)LX/1gW;

    move-result-object v6

    invoke-interface {v6}, LX/1gW;->c()V

    .line 2373061
    iget-object v5, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->c:LX/0sK;

    invoke-virtual {v1}, LX/2fU;->c()I

    move-result v6

    const/4 v7, 0x0

    .line 2373062
    invoke-static {v5}, LX/0sK;->i(LX/0sK;)LX/0WS;

    move-result-object v8

    const-string v9, "download_start_ota_version"

    invoke-virtual {v8, v9, v7}, LX/0WS;->a(Ljava/lang/String;I)I

    move-result v8

    .line 2373063
    if-ne v6, v8, :cond_6

    .line 2373064
    invoke-static {v5}, LX/0sK;->i(LX/0sK;)LX/0WS;

    move-result-object v8

    const-string v9, "download_start_attempt_count"

    invoke-virtual {v8, v9, v7}, LX/0WS;->a(Ljava/lang/String;I)I

    move-result v7

    .line 2373065
    :cond_6
    invoke-static {v5}, LX/0sK;->i(LX/0sK;)LX/0WS;

    move-result-object v8

    invoke-virtual {v8}, LX/0WS;->b()LX/1gW;

    move-result-object v8

    const-string v9, "download_start_ota_version"

    invoke-interface {v8, v9, v6}, LX/1gW;->a(Ljava/lang/String;I)LX/1gW;

    move-result-object v8

    const-string v9, "download_start_attempt_count"

    add-int/lit8 v7, v7, 0x1

    invoke-interface {v8, v9, v7}, LX/1gW;->a(Ljava/lang/String;I)LX/1gW;

    move-result-object v7

    invoke-interface {v7}, LX/1gW;->c()V

    .line 2373066
    iget-object v5, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->c:LX/0sK;

    invoke-virtual {v1}, LX/2fU;->c()I

    move-result v6

    .line 2373067
    invoke-static {v5}, LX/0sK;->i(LX/0sK;)LX/0WS;

    move-result-object v7

    invoke-virtual {v7}, LX/0WS;->b()LX/1gW;

    move-result-object v7

    const-string v8, "download_in_progress_ota_version"

    invoke-interface {v7, v8, v6}, LX/1gW;->a(Ljava/lang/String;I)LX/1gW;

    move-result-object v7

    invoke-interface {v7}, LX/1gW;->c()V

    .line 2373068
    iget-object v5, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->d:LX/Gd2;

    invoke-virtual {v5, v1}, LX/Gd2;->a(LX/2fU;)V

    .line 2373069
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    .line 2373070
    :try_start_1
    iget-object v5, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->e:LX/Gd5;

    invoke-virtual {v1}, LX/2fU;->c()I

    move-result v6

    invoke-virtual {v5, v6}, LX/Gd5;->a(I)Ljava/io/File;

    move-result-object v5

    .line 2373071
    iget-object v6, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->f:Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;

    invoke-virtual {v6, v0, v5}, Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;->a(Ljava/lang/String;Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    .line 2373072
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    .line 2373073
    sub-long v7, v9, v7

    .line 2373074
    iget-object v6, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->d:LX/Gd2;

    invoke-virtual {v6, v1, v7, v8}, LX/Gd2;->a(LX/2fU;J)V

    .line 2373075
    iget-object v6, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->c:LX/0sK;

    .line 2373076
    invoke-static {v6}, LX/0sK;->i(LX/0sK;)LX/0WS;

    move-result-object v7

    invoke-virtual {v7}, LX/0WS;->b()LX/1gW;

    move-result-object v7

    const-string v8, "download_end"

    invoke-interface {v7, v8, v9, v10}, LX/1gW;->a(Ljava/lang/String;J)LX/1gW;

    move-result-object v7

    invoke-interface {v7}, LX/1gW;->c()V

    .line 2373077
    iget-object v6, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->c:LX/0sK;

    invoke-virtual {v6}, LX/0sK;->e()V

    .line 2373078
    :goto_3
    move-object v0, v5

    .line 2373079
    if-eqz v0, :cond_1

    .line 2373080
    invoke-virtual {v1}, LX/2fU;->c()I

    move-result v2

    .line 2373081
    iget-object v3, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->e:LX/Gd5;

    invoke-virtual {v3, v2}, LX/Gd5;->b(I)Ljava/io/File;

    move-result-object v3

    .line 2373082
    :try_start_2
    invoke-static {v0, v3}, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->a(Ljava/io/File;Ljava/io/File;)Ljava/util/List;

    move-result-object v0

    .line 2373083
    const-string v4, ","

    invoke-static {v4, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 2373084
    const/4 v5, 0x1

    .line 2373085
    iget-object v0, v1, LX/2fU;->d:LX/Gd6;

    if-eqz v0, :cond_7

    iget-object v0, v1, LX/2fU;->d:LX/Gd6;

    iget-object v0, v0, LX/Gd6;->f:Ljava/util/Map;

    if-nez v0, :cond_12

    .line 2373086
    :cond_7
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    .line 2373087
    :goto_4
    move-object v0, v0

    .line 2373088
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_f

    move v0, v5

    .line 2373089
    :goto_5
    move v0, v0

    .line 2373090
    if-eqz v0, :cond_9

    .line 2373091
    iget-object v0, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->d:LX/Gd2;

    invoke-virtual {v0, v1}, LX/Gd2;->b(LX/2fU;)V

    .line 2373092
    iget-object v0, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->c:LX/0sK;

    .line 2373093
    invoke-static {v0}, LX/0sK;->i(LX/0sK;)LX/0WS;

    move-result-object v1

    invoke-virtual {v1}, LX/0WS;->b()LX/1gW;

    move-result-object v1

    const-string v3, "next"

    invoke-interface {v1, v3, v2}, LX/1gW;->a(Ljava/lang/String;I)LX/1gW;

    move-result-object v1

    invoke-interface {v1}, LX/1gW;->c()V

    .line 2373094
    :goto_6
    iget-object v0, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->e:LX/Gd5;

    iget-object v1, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->c:LX/0sK;

    const/4 p0, 0x1

    const/4 v5, 0x0

    .line 2373095
    iget-object v2, v0, LX/Gd5;->b:Ljava/io/File;

    invoke-static {v2}, LX/Gd5;->a(Ljava/io/File;)V

    .line 2373096
    new-instance v2, Ljava/io/File;

    iget-object v3, v0, LX/Gd5;->a:Ljava/io/File;

    const-string v4, "updates"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2373097
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_13

    .line 2373098
    :cond_8
    :goto_7
    goto/16 :goto_1

    .line 2373099
    :catch_0
    move-exception v0

    .line 2373100
    const-string v2, "AutoUpdater"

    const-string v3, "Could not unzip bundle files"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2373101
    iget-object v2, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->d:LX/Gd2;

    invoke-virtual {v2, v1, v0}, LX/Gd2;->a(LX/2fU;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 2373102
    :cond_9
    const-string v0, "AutoUpdater"

    const-string v2, "Verification failed"

    invoke-static {v0, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2373103
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Verification failed"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 2373104
    iget-object v2, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->d:LX/Gd2;

    invoke-virtual {v2, v1, v0}, LX/Gd2;->a(LX/2fU;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 2373105
    :catch_1
    sget-object v2, LX/2fU;->a:LX/2fU;

    move-object v2, v2

    .line 2373106
    goto/16 :goto_0

    .line 2373107
    :cond_a
    iget-object v4, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->c:LX/0sK;

    invoke-virtual {v4}, LX/0sK;->c()I

    move-result v4

    if-ne v4, v3, :cond_b

    iget-object v4, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->e:LX/Gd5;

    invoke-virtual {v4, v3}, LX/Gd5;->d(I)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 2373108
    iget-object v2, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->c:LX/0sK;

    invoke-virtual {v2}, LX/0sK;->h()V

    goto/16 :goto_2

    .line 2373109
    :cond_b
    iget-object v4, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->c:LX/0sK;

    invoke-virtual {v4}, LX/0sK;->b()I

    move-result v4

    if-ne v4, v3, :cond_c

    iget-object v4, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->e:LX/Gd5;

    invoke-virtual {v4, v3}, LX/Gd5;->d(I)Z

    move-result v4

    if-nez v4, :cond_5

    .line 2373110
    :cond_c
    invoke-virtual {v1}, LX/2fU;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 2373111
    const-string v4, "AutoUpdater"

    const-string v5, "No download url with update %d"

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v2

    invoke-static {v4, v5, v6}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 2373112
    :cond_d
    invoke-static {p0, v3}, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->a(Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;I)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 2373113
    const-string v4, "AutoUpdater"

    const-string v5, "Download for update %d skipped due to previous download failures"

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v2

    invoke-static {v4, v5, v6}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_e
    move v0, v2

    .line 2373114
    goto/16 :goto_2

    .line 2373115
    :catch_2
    move-exception v5

    .line 2373116
    const-string v6, "AutoUpdater"

    const-string v7, "Failed to download"

    invoke-static {v6, v7, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2373117
    iget-object v6, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->c:LX/0sK;

    invoke-virtual {v6}, LX/0sK;->e()V

    .line 2373118
    iget-object v6, p0, Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;->d:LX/Gd2;

    invoke-virtual {v6, v1, v5}, LX/Gd2;->a(LX/2fU;Ljava/lang/Throwable;)V

    .line 2373119
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 2373120
    :cond_f
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_10
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2373121
    new-instance v7, Ljava/io/File;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {v7, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2373122
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2373123
    invoke-static {v7, v0}, LX/Gd4;->a(Ljava/io/File;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 2373124
    const/4 v0, 0x0

    goto/16 :goto_5

    :cond_11
    move v0, v5

    .line 2373125
    goto/16 :goto_5

    :cond_12
    iget-object v0, v1, LX/2fU;->d:LX/Gd6;

    iget-object v0, v0, LX/Gd6;->f:Ljava/util/Map;

    goto/16 :goto_4

    .line 2373126
    :cond_13
    new-array v3, p0, [I

    iget v4, v0, LX/Gd5;->c:I

    aput v4, v3, v5

    invoke-static {v2, v3}, LX/Gd5;->a(Ljava/io/File;[I)V

    .line 2373127
    new-instance v3, Ljava/io/File;

    iget v4, v0, LX/Gd5;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2373128
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2373129
    const/4 v2, 0x2

    new-array v2, v2, [I

    invoke-virtual {v1}, LX/0sK;->c()I

    move-result v4

    aput v4, v2, v5

    invoke-virtual {v1}, LX/0sK;->b()I

    move-result v4

    aput v4, v2, p0

    invoke-static {v3, v2}, LX/Gd5;->a(Ljava/io/File;[I)V

    goto/16 :goto_7
.end method
