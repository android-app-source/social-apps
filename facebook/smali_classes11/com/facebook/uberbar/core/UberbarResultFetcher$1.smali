.class public final Lcom/facebook/uberbar/core/UberbarResultFetcher$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/G5W;


# direct methods
.method public constructor <init>(LX/G5W;)V
    .locals 0

    .prologue
    .line 2318827
    iput-object p1, p0, Lcom/facebook/uberbar/core/UberbarResultFetcher$1;->a:LX/G5W;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2318800
    iget-object v0, p0, Lcom/facebook/uberbar/core/UberbarResultFetcher$1;->a:LX/G5W;

    iget-boolean v0, v0, LX/G5W;->l:Z

    if-eqz v0, :cond_0

    .line 2318801
    :goto_0
    return-void

    .line 2318802
    :cond_0
    iget-object v0, p0, Lcom/facebook/uberbar/core/UberbarResultFetcher$1;->a:LX/G5W;

    .line 2318803
    iget-object v1, v0, LX/G5W;->t:LX/G5P;

    .line 2318804
    sget-object v2, LX/G5O;->a:LX/G5N;

    const-string v3, "request_time_to_request_begin_time"

    invoke-static {v1, v2, v3}, LX/G5P;->b(LX/G5P;LX/0Pq;Ljava/lang/String;)V

    .line 2318805
    iget-object v1, v0, LX/G5W;->t:LX/G5P;

    .line 2318806
    sget-object v2, LX/G5O;->a:LX/G5N;

    const-string v3, "request_begin_to_response_end"

    invoke-static {v1, v2, v3}, LX/G5P;->a(LX/G5P;LX/0Pq;Ljava/lang/String;)V

    .line 2318807
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2318808
    iget-boolean v1, v0, LX/G5W;->s:Z

    if-eqz v1, :cond_1

    sget-object v1, LX/G5W;->d:LX/0Px;

    .line 2318809
    :goto_1
    new-instance v3, LX/7BS;

    invoke-direct {v3}, LX/7BS;-><init>()V

    iget-object p0, v0, LX/G5W;->i:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2318810
    iput-object p0, v3, LX/7BS;->a:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2318811
    move-object v3, v3

    .line 2318812
    iget-object p0, v0, LX/G5W;->j:Ljava/lang/String;

    .line 2318813
    iput-object p0, v3, LX/7BS;->b:Ljava/lang/String;

    .line 2318814
    move-object v3, v3

    .line 2318815
    const/16 p0, 0x64

    .line 2318816
    iput p0, v3, LX/7BS;->c:I

    .line 2318817
    move-object v3, v3

    .line 2318818
    iput-object v1, v3, LX/7BS;->d:Ljava/util/List;

    .line 2318819
    move-object v1, v3

    .line 2318820
    invoke-virtual {v1}, LX/7BS;->f()Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;

    move-result-object v1

    .line 2318821
    const-string v3, "fetchQueryResultParams"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2318822
    iget-object v1, v0, LX/G5W;->h:LX/0aG;

    const-string v3, "fetch_uberbar_result"

    const p0, -0x22c34914

    invoke-static {v1, v3, v2, p0}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v1

    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    .line 2318823
    new-instance v2, LX/G5T;

    invoke-direct {v2, v0}, LX/G5T;-><init>(LX/G5W;)V

    move-object v2, v2

    .line 2318824
    invoke-static {v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2318825
    goto :goto_0

    .line 2318826
    :cond_1
    sget-object v1, LX/G5W;->c:LX/0Px;

    goto :goto_1
.end method
