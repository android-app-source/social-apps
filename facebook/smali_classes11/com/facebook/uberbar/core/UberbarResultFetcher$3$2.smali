.class public final Lcom/facebook/uberbar/core/UberbarResultFetcher$3$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/Throwable;

.field public final synthetic b:LX/G5U;


# direct methods
.method public constructor <init>(LX/G5U;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2318877
    iput-object p1, p0, Lcom/facebook/uberbar/core/UberbarResultFetcher$3$2;->b:LX/G5U;

    iput-object p2, p0, Lcom/facebook/uberbar/core/UberbarResultFetcher$3$2;->a:Ljava/lang/Throwable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 2318857
    iget-object v0, p0, Lcom/facebook/uberbar/core/UberbarResultFetcher$3$2;->b:LX/G5U;

    iget-object v0, v0, LX/G5U;->b:LX/G5W;

    iget-boolean v0, v0, LX/G5W;->l:Z

    if-eqz v0, :cond_0

    .line 2318858
    :goto_0
    return-void

    .line 2318859
    :cond_0
    sget-object v0, LX/G5V;->a:[I

    iget-object v1, p0, Lcom/facebook/uberbar/core/UberbarResultFetcher$3$2;->b:LX/G5U;

    iget-object v1, v1, LX/G5U;->a:LX/7BK;

    invoke-virtual {v1}, LX/7BK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2318860
    :goto_1
    iget-object v0, p0, Lcom/facebook/uberbar/core/UberbarResultFetcher$3$2;->b:LX/G5U;

    iget-object v0, v0, LX/G5U;->b:LX/G5W;

    invoke-static {v0}, LX/G5W;->g(LX/G5W;)V

    .line 2318861
    iget-object v0, p0, Lcom/facebook/uberbar/core/UberbarResultFetcher$3$2;->b:LX/G5U;

    invoke-static {v0}, LX/G5U;->a$redex0(LX/G5U;)V

    .line 2318862
    iget-object v0, p0, Lcom/facebook/uberbar/core/UberbarResultFetcher$3$2;->b:LX/G5U;

    iget-object v0, v0, LX/G5U;->b:LX/G5W;

    iget-object v0, v0, LX/G5W;->v:LX/03V;

    sget-object v1, LX/G5W;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Local "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/uberbar/core/UberbarResultFetcher$3$2;->b:LX/G5U;

    iget-object v3, v3, LX/G5U;->a:LX/7BK;

    invoke-virtual {v3}, LX/7BK;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " search failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/uberbar/core/UberbarResultFetcher$3$2;->a:Ljava/lang/Throwable;

    invoke-virtual {v0, v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2318863
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/uberbar/core/UberbarResultFetcher$3$2;->b:LX/G5U;

    iget-object v0, v0, LX/G5U;->b:LX/G5W;

    sget-object v1, LX/G5W;->b:Ljava/util/List;

    .line 2318864
    iput-object v1, v0, LX/G5W;->p:Ljava/util/List;

    .line 2318865
    iget-object v0, p0, Lcom/facebook/uberbar/core/UberbarResultFetcher$3$2;->b:LX/G5U;

    iget-object v0, v0, LX/G5U;->b:LX/G5W;

    .line 2318866
    iput-boolean v2, v0, LX/G5W;->m:Z

    .line 2318867
    iget-object v0, p0, Lcom/facebook/uberbar/core/UberbarResultFetcher$3$2;->b:LX/G5U;

    iget-object v0, v0, LX/G5U;->b:LX/G5W;

    iget-object v0, v0, LX/G5W;->t:LX/G5P;

    .line 2318868
    sget-object v1, LX/G5O;->b:LX/G5M;

    const-string v2, "fetch_users_time"

    invoke-static {v0, v1, v2}, LX/G5P;->c(LX/G5P;LX/0Pq;Ljava/lang/String;)V

    .line 2318869
    goto :goto_1

    .line 2318870
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/uberbar/core/UberbarResultFetcher$3$2;->b:LX/G5U;

    iget-object v0, v0, LX/G5U;->b:LX/G5W;

    sget-object v1, LX/G5W;->b:Ljava/util/List;

    .line 2318871
    iput-object v1, v0, LX/G5W;->q:Ljava/util/List;

    .line 2318872
    iget-object v0, p0, Lcom/facebook/uberbar/core/UberbarResultFetcher$3$2;->b:LX/G5U;

    iget-object v0, v0, LX/G5U;->b:LX/G5W;

    .line 2318873
    iput-boolean v2, v0, LX/G5W;->n:Z

    .line 2318874
    iget-object v0, p0, Lcom/facebook/uberbar/core/UberbarResultFetcher$3$2;->b:LX/G5U;

    iget-object v0, v0, LX/G5U;->b:LX/G5W;

    iget-object v0, v0, LX/G5W;->t:LX/G5P;

    .line 2318875
    sget-object v1, LX/G5O;->b:LX/G5M;

    const-string v2, "fetch_pages_time"

    invoke-static {v0, v1, v2}, LX/G5P;->c(LX/G5P;LX/0Pq;Ljava/lang/String;)V

    .line 2318876
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
