.class public Lcom/facebook/greetingcards/create/GreetingCardUploadHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/7mC;

.field private final c:LX/8OJ;

.field private final d:LX/73w;

.field private final e:LX/8Ne;


# direct methods
.method public constructor <init>(LX/0Or;LX/7mC;LX/8OJ;LX/73w;LX/8Ne;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/7mC;",
            "LX/8OJ;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            "LX/8Ne;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2387216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2387217
    iput-object p1, p0, Lcom/facebook/greetingcards/create/GreetingCardUploadHandler;->a:LX/0Or;

    .line 2387218
    iput-object p2, p0, Lcom/facebook/greetingcards/create/GreetingCardUploadHandler;->b:LX/7mC;

    .line 2387219
    iput-object p3, p0, Lcom/facebook/greetingcards/create/GreetingCardUploadHandler;->c:LX/8OJ;

    .line 2387220
    iput-object p4, p0, Lcom/facebook/greetingcards/create/GreetingCardUploadHandler;->d:LX/73w;

    .line 2387221
    iput-object p5, p0, Lcom/facebook/greetingcards/create/GreetingCardUploadHandler;->e:LX/8Ne;

    .line 2387222
    return-void
.end method

.method public static a(Lcom/facebook/greetingcards/create/GreetingCardUploadHandler;Ljava/util/List;Lcom/facebook/composer/publish/common/PublishPostParams;)Ljava/util/Map;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "Lcom/facebook/composer/publish/common/PublishPostParams;",
            ")",
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2387223
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 2387224
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 2387225
    new-instance v3, LX/8NJ;

    new-instance v4, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v5

    .line 2387226
    iget-wide v10, v0, Lcom/facebook/ipc/media/MediaItem;->e:J

    move-wide v6, v10

    .line 2387227
    invoke-direct {v4, v5, v6, v7}, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;-><init>(Ljava/lang/String;J)V

    invoke-direct {v3, v4}, LX/8NJ;-><init>(Lcom/facebook/photos/upload/protocol/UploadPhotoSource;)V

    new-instance v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    iget-object v4, p2, Lcom/facebook/composer/publish/common/PublishPostParams;->privacy:Ljava/lang/String;

    invoke-direct {v0, v4}, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;-><init>(Ljava/lang/String;)V

    .line 2387228
    iput-object v0, v3, LX/8NJ;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 2387229
    move-object v0, v3

    .line 2387230
    invoke-virtual {v0}, LX/8NJ;->a()Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    move-result-object v0

    .line 2387231
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2387232
    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardUploadHandler;->d:LX/73w;

    iget-object v2, p2, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/73w;->a(Ljava/lang/String;)V

    .line 2387233
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v9

    .line 2387234
    new-instance v2, LX/GjA;

    invoke-direct {v2, p0, v9}, LX/GjA;-><init>(Lcom/facebook/greetingcards/create/GreetingCardUploadHandler;Ljava/util/HashMap;)V

    .line 2387235
    new-instance v3, LX/8OL;

    invoke-direct {v3}, LX/8OL;-><init>()V

    .line 2387236
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardUploadHandler;->c:LX/8OJ;

    iget-object v4, p0, Lcom/facebook/greetingcards/create/GreetingCardUploadHandler;->d:LX/73w;

    iget-object v5, p0, Lcom/facebook/greetingcards/create/GreetingCardUploadHandler;->d:LX/73w;

    const-string v6, "2.0"

    invoke-virtual {v5, v6}, LX/73w;->j(Ljava/lang/String;)LX/74b;

    move-result-object v5

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/facebook/greetingcards/create/GreetingCardUploadHandler;->e:LX/8Ne;

    const-class v8, Lcom/facebook/greetingcards/create/GreetingCardUploadHandler;

    invoke-static {v8}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v8

    invoke-virtual/range {v0 .. v8}, LX/8OJ;->a(Ljava/util/Collection;LX/8O7;LX/8OL;LX/73w;LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ne;Lcom/facebook/common/callercontext/CallerContext;)Ljava/util/Map;

    .line 2387237
    return-object v9
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 9

    .prologue
    .line 2387238
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2387239
    const-string v1, "upload_card"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2387240
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 2387241
    const-string v0, "greeting_card"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/GreetingCard;

    .line 2387242
    const-string v2, "post_params"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 2387243
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2387244
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    iget-object v3, v0, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v3, v3, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/greetingcards/model/GreetingCard;->c:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v3, v3, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v5

    .line 2387245
    iget-object v2, v0, Lcom/facebook/greetingcards/model/GreetingCard;->b:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_0

    iget-object v2, v0, Lcom/facebook/greetingcards/model/GreetingCard;->b:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    .line 2387246
    iget-object v2, v2, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    invoke-virtual {v5, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2387247
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2387248
    :cond_0
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v5, v2

    .line 2387249
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_2

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/greetingcards/model/CardPhoto;

    .line 2387250
    iget-object v7, v2, Lcom/facebook/greetingcards/model/CardPhoto;->b:LX/5QD;

    sget-object v8, LX/5QD;->LOCAL:LX/5QD;

    if-ne v7, v8, :cond_1

    .line 2387251
    iget-object v2, v2, Lcom/facebook/greetingcards/model/CardPhoto;->d:Lcom/facebook/ipc/media/MediaItem;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2387252
    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 2387253
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2387254
    invoke-static {p0, v4, v1}, Lcom/facebook/greetingcards/create/GreetingCardUploadHandler;->a(Lcom/facebook/greetingcards/create/GreetingCardUploadHandler;Ljava/util/List;Lcom/facebook/composer/publish/common/PublishPostParams;)Ljava/util/Map;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/facebook/greetingcards/model/GreetingCard;->a(Lcom/facebook/greetingcards/model/GreetingCard;Ljava/util/Map;)Lcom/facebook/greetingcards/model/GreetingCard;

    move-result-object v0

    .line 2387255
    :cond_3
    new-instance v2, LX/5M9;

    invoke-direct {v2, v1}, LX/5M9;-><init>(Lcom/facebook/composer/publish/common/PublishPostParams;)V

    .line 2387256
    iput-object v0, v2, LX/5M9;->O:Lcom/facebook/greetingcards/model/GreetingCard;

    .line 2387257
    move-object v2, v2

    .line 2387258
    invoke-virtual {v2}, LX/5M9;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v3

    .line 2387259
    iget-object v2, p0, Lcom/facebook/greetingcards/create/GreetingCardUploadHandler;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/11H;

    iget-object v4, p0, Lcom/facebook/greetingcards/create/GreetingCardUploadHandler;->b:LX/7mC;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v5

    invoke-virtual {v2, v4, v3, v5}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 2387260
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2387261
    move-object v0, v0

    .line 2387262
    return-object v0

    .line 2387263
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
