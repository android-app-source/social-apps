.class public Lcom/facebook/greetingcards/create/PreviewCardFragment;
.super Lcom/facebook/greetingcards/render/RenderCardFragment;
.source ""


# static fields
.field public static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public d:Landroid/app/NotificationManager;

.field public e:LX/0tX;

.field public f:Lcom/facebook/content/SecureContextHelper;

.field public g:LX/0rq;

.field public h:LX/0Zb;

.field public i:LX/GjC;

.field public j:LX/1Kf;

.field public k:Lcom/facebook/fbservice/ops/BlueServiceFragment;

.field public l:Landroid/app/PendingIntent;

.field private m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2387781
    const-class v0, Lcom/facebook/greetingcards/create/PreviewCardFragment;

    sput-object v0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2387778
    invoke-direct {p0}, Lcom/facebook/greetingcards/render/RenderCardFragment;-><init>()V

    .line 2387779
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->m:Z

    .line 2387780
    return-void
.end method

.method public static o(Lcom/facebook/greetingcards/create/PreviewCardFragment;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2387697
    iget-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    invoke-static {v0}, Lcom/facebook/greetingcards/model/GreetingCard;->a(Lcom/facebook/greetingcards/model/GreetingCard;)Lcom/facebook/greetingcards/model/GreetingCard;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    .line 2387698
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "greeting_card_share"

    invoke-direct {v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "greeting_card_id"

    iget-object v3, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    iget-object v3, v3, Lcom/facebook/greetingcards/model/GreetingCard;->f:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v2, "edited"

    iget-boolean v3, p0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->m:Z

    invoke-virtual {v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2387699
    iget-object v2, p0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->h:LX/0Zb;

    invoke-interface {v2, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2387700
    iget-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    move-object v0, v0

    .line 2387701
    new-instance v2, LX/39x;

    invoke-direct {v2}, LX/39x;-><init>()V

    iget-object v3, v0, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v3, v3, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->a:Ljava/lang/String;

    .line 2387702
    iput-object v3, v2, LX/39x;->t:Ljava/lang/String;

    .line 2387703
    move-object v2, v2

    .line 2387704
    iget-object v3, v0, Lcom/facebook/greetingcards/model/GreetingCard;->c:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v3, v3, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->b:Ljava/lang/String;

    .line 2387705
    iput-object v3, v2, LX/39x;->r:Ljava/lang/String;

    .line 2387706
    move-object v2, v2

    .line 2387707
    iget-object v3, v0, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v3, v3, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v0, v1

    .line 2387708
    :goto_0
    iput-object v0, v2, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 2387709
    move-object v0, v2

    .line 2387710
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 2387711
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    sget-object v3, LX/2rt;->GREETING_CARD:LX/2rt;

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setComposerType(LX/2rt;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v3

    sget-object v4, LX/21D;->GREETING_CARD:LX/21D;

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setSourceSurface(LX/21D;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v3

    const-string v4, "previewCardFragment"

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setEntryPointName(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setLaunchLoggingParams(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0835e7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialText(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    const-string v3, "greeting_card_composer"

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-static {}, LX/89G;->a()LX/89G;

    move-result-object v3

    .line 2387712
    iput-object v0, v3, LX/89G;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2387713
    move-object v0, v3

    .line 2387714
    invoke-virtual {v0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v2, LX/89K;

    invoke-direct {v2}, LX/89K;-><init>()V

    sget-object v2, LX/Giu;->a:LX/89L;

    invoke-static {v2}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2387715
    iget-object v2, p0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->j:LX/1Kf;

    const/16 v3, 0x8

    invoke-interface {v2, v1, v0, v3, p0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    .line 2387716
    return-void

    .line 2387717
    :cond_0
    new-instance v3, LX/4XB;

    invoke-direct {v3}, LX/4XB;-><init>()V

    new-instance v4, LX/2dc;

    invoke-direct {v4}, LX/2dc;-><init>()V

    iget-object v0, v0, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v0, v0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/CardPhoto;

    iget-object v0, v0, Lcom/facebook/greetingcards/model/CardPhoto;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2387718
    iput-object v0, v4, LX/2dc;->h:Ljava/lang/String;

    .line 2387719
    move-object v0, v4

    .line 2387720
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 2387721
    iput-object v0, v3, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2387722
    move-object v0, v3

    .line 2387723
    invoke-virtual {v0}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2387777
    const-string v0, "tinsel_preview_card"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2387761
    invoke-super {p0, p1}, Lcom/facebook/greetingcards/render/RenderCardFragment;->a(Landroid/os/Bundle;)V

    .line 2387762
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/greetingcards/create/PreviewCardFragment;

    invoke-static {v0}, LX/1s4;->b(LX/0QB;)Landroid/app/NotificationManager;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v6

    check-cast v6, LX/0rq;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    const-class p1, LX/GjC;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/GjC;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v0

    check-cast v0, LX/1Kf;

    iput-object v3, v2, Lcom/facebook/greetingcards/create/PreviewCardFragment;->d:Landroid/app/NotificationManager;

    iput-object v4, v2, Lcom/facebook/greetingcards/create/PreviewCardFragment;->e:LX/0tX;

    iput-object v5, v2, Lcom/facebook/greetingcards/create/PreviewCardFragment;->f:Lcom/facebook/content/SecureContextHelper;

    iput-object v6, v2, Lcom/facebook/greetingcards/create/PreviewCardFragment;->g:LX/0rq;

    iput-object v7, v2, Lcom/facebook/greetingcards/create/PreviewCardFragment;->h:LX/0Zb;

    iput-object p1, v2, Lcom/facebook/greetingcards/create/PreviewCardFragment;->i:LX/GjC;

    iput-object v0, v2, Lcom/facebook/greetingcards/create/PreviewCardFragment;->j:LX/1Kf;

    .line 2387763
    new-instance v0, LX/GjQ;

    invoke-direct {v0, p0}, LX/GjQ;-><init>(Lcom/facebook/greetingcards/create/PreviewCardFragment;)V

    .line 2387764
    iput-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->n:LX/CEO;

    .line 2387765
    new-instance v0, LX/GjR;

    invoke-direct {v0, p0}, LX/GjR;-><init>(Lcom/facebook/greetingcards/create/PreviewCardFragment;)V

    .line 2387766
    iput-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->m:LX/GjR;

    .line 2387767
    iget-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    if-nez v0, :cond_0

    .line 2387768
    new-instance v4, LX/5QG;

    invoke-direct {v4}, LX/5QG;-><init>()V

    move-object v4, v4

    .line 2387769
    const-string v5, "size_large"

    iget-object v6, p0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->g:LX/0rq;

    invoke-virtual {v6}, LX/0rq;->f()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v5

    const-string v6, "size_medium"

    iget-object v7, p0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->g:LX/0rq;

    invoke-virtual {v7}, LX/0rq;->g()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2387770
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    const-wide/16 v6, 0xe10

    invoke-virtual {v4, v6, v7}, LX/0zO;->a(J)LX/0zO;

    move-result-object v4

    .line 2387771
    iget-object v5, p0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->e:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    .line 2387772
    new-instance v5, LX/GjS;

    invoke-direct {v5, p0}, LX/GjS;-><init>(Lcom/facebook/greetingcards/create/PreviewCardFragment;)V

    iget-object v6, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->b:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2387773
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    move-object v0, v0

    .line 2387774
    invoke-virtual {v0}, LX/0k3;->p()LX/0gc;

    move-result-object v0

    const-string v1, "upload_card"

    invoke-static {v0, v1}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->create(LX/0gc;Ljava/lang/String;)Lcom/facebook/fbservice/ops/BlueServiceFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->k:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    .line 2387775
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->l:Landroid/app/PendingIntent;

    .line 2387776
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 2387757
    iget-boolean v0, p0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->m:Z

    if-eqz v0, :cond_0

    .line 2387758
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080017

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f0835ea

    new-instance v2, LX/GjT;

    invoke-direct {v2, p0}, LX/GjT;-><init>(Lcom/facebook/greetingcards/create/PreviewCardFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0835e9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2387759
    const/4 v0, 0x1

    .line 2387760
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 2387724
    const/16 v0, 0x8

    if-ne p1, v0, :cond_2

    .line 2387725
    if-ne p2, v1, :cond_1

    .line 2387726
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "greeting_share_completed"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "greeting_card_id"

    iget-object v2, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    iget-object v2, v2, Lcom/facebook/greetingcards/model/GreetingCard;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2387727
    iget-object v1, p0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->h:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2387728
    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 2387729
    const/4 p3, 0x1

    const/4 p2, 0x0

    .line 2387730
    iget-object v1, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    move-object v1, v1

    .line 2387731
    new-instance v2, LX/2HB;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, p2, p2, p3}, LX/2HB;->a(IIZ)LX/2HB;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0835eb

    new-array v5, p3, [Ljava/lang/Object;

    iget-object p1, v1, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object p1, p1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->a:Ljava/lang/String;

    aput-object p1, v5, p2

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0835ec

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->l:Landroid/app/PendingIntent;

    .line 2387732
    iput-object v3, v2, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2387733
    move-object v2, v2

    .line 2387734
    const v3, 0x7f0218e8

    invoke-virtual {v2, v3}, LX/2HB;->a(I)LX/2HB;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v2

    .line 2387735
    iget-object v3, p0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->d:Landroid/app/NotificationManager;

    const/16 v4, 0x9

    invoke-virtual {v2}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 2387736
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2387737
    const-string v4, "greeting_card"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2387738
    const-string v1, "post_params"

    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2387739
    iget-object v1, p0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->k:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    const-string v4, "upload_card"

    invoke-virtual {v1, v4, v3}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->start(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2387740
    iget-object v1, p0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->k:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    iget-object v3, p0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->i:LX/GjC;

    .line 2387741
    new-instance p2, LX/GjB;

    const-class v4, Landroid/content/Context;

    invoke-interface {v3, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v3}, LX/1s4;->b(LX/0QB;)Landroid/app/NotificationManager;

    move-result-object v5

    check-cast v5, Landroid/app/NotificationManager;

    invoke-static {v3}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p1

    check-cast p1, LX/03V;

    invoke-direct {p2, v4, v5, p1, v2}, LX/GjB;-><init>(Landroid/content/Context;Landroid/app/NotificationManager;LX/03V;LX/2HB;)V

    .line 2387742
    move-object v2, p2

    .line 2387743
    iput-object v2, v1, Lcom/facebook/fbservice/ops/BlueServiceFragment;->onCompletedListener:LX/4Ae;

    .line 2387744
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 2387745
    if-eqz v0, :cond_0

    .line 2387746
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 2387747
    instance-of v0, v0, LX/Gjd;

    if-eqz v0, :cond_0

    .line 2387748
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 2387749
    check-cast v0, LX/Gjd;

    invoke-interface {v0}, LX/Gjd;->a()V

    .line 2387750
    :cond_0
    :goto_0
    return-void

    .line 2387751
    :cond_1
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "greeting_share_cancelled"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "greeting_card_id"

    iget-object v2, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    iget-object v2, v2, Lcom/facebook/greetingcards/model/GreetingCard;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2387752
    iget-object v1, p0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->h:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 2387753
    :cond_2
    const/4 v0, 0x7

    if-ne p1, v0, :cond_0

    if-ne p2, v1, :cond_0

    .line 2387754
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->m:Z

    .line 2387755
    const-string v0, "args_greeting_card"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/GreetingCard;

    .line 2387756
    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/render/RenderCardFragment;->a(Lcom/facebook/greetingcards/model/GreetingCard;)V

    goto :goto_0
.end method
