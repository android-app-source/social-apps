.class public Lcom/facebook/greetingcards/create/PreviewCardActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2387500
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2387501
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2387502
    if-nez p1, :cond_1

    .line 2387503
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/PreviewCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 2387504
    :goto_0
    new-instance v1, LX/GjV;

    invoke-direct {v1}, LX/GjV;-><init>()V

    .line 2387505
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/PreviewCardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0835f1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/PreviewCardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0835f2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/GjU;->a(LX/0Px;)LX/GjU;

    .line 2387506
    if-eqz v0, :cond_0

    .line 2387507
    invoke-virtual {v1, v0}, LX/GjU;->a(Lcom/facebook/greetingcards/model/GreetingCard;)LX/GjU;

    .line 2387508
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v2, 0x1020002

    invoke-virtual {v1}, LX/GjU;->b()Lcom/facebook/greetingcards/render/RenderCardFragment;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2387509
    :cond_1
    return-void

    .line 2387510
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/PreviewCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "args_greeting_card"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/GreetingCard;

    goto :goto_0
.end method
