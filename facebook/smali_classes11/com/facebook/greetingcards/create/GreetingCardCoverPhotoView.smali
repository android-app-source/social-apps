.class public Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Gik;


# instance fields
.field private a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public b:LX/Giy;

.field private c:Landroid/view/View;

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/greetingcards/model/CardPhoto;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2386818
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2386819
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->a()V

    .line 2386820
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2386815
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2386816
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->a()V

    .line 2386817
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2386812
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2386813
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->a()V

    .line 2386814
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2386805
    const-class v0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;

    invoke-static {v0, p0}, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2386806
    const v0, 0x7f030887

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2386807
    const v0, 0x7f0d161d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->c:Landroid/view/View;

    .line 2386808
    const v0, 0x7f0d161b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2386809
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v1, 0x3faaaaab

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2386810
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v1, LX/Gix;

    invoke-direct {v1, p0}, LX/Gix;-><init>(Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2386811
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;

    const/16 v1, 0x509

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->d:LX/0Or;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/greetingcards/model/CardPhoto;)V
    .locals 3
    .param p1    # Lcom/facebook/greetingcards/model/CardPhoto;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2386791
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->e:Lcom/facebook/greetingcards/model/CardPhoto;

    if-ne v0, p1, :cond_1

    .line 2386792
    :cond_0
    :goto_0
    return-void

    .line 2386793
    :cond_1
    iput-object p1, p0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->e:Lcom/facebook/greetingcards/model/CardPhoto;

    .line 2386794
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    .line 2386795
    new-instance v1, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 2386796
    iget-object v2, p1, Lcom/facebook/greetingcards/model/CardPhoto;->e:Landroid/graphics/PointF;

    if-eqz v2, :cond_2

    .line 2386797
    iget-object v2, p1, Lcom/facebook/greetingcards/model/CardPhoto;->e:Landroid/graphics/PointF;

    .line 2386798
    iput-object v2, v1, LX/1Uo;->p:Landroid/graphics/PointF;

    .line 2386799
    sget-object v2, LX/1Up;->h:LX/1Up;

    invoke-virtual {v1, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    .line 2386800
    :cond_2
    iget-object v2, p0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2386801
    iget-object v1, p1, Lcom/facebook/greetingcards/model/CardPhoto;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "greeting_cards"

    invoke-static {v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2386802
    iget-object v1, p0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2386803
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->e:Lcom/facebook/greetingcards/model/CardPhoto;

    if-eqz v0, :cond_0

    .line 2386804
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public getCardPhoto()Lcom/facebook/greetingcards/model/CardPhoto;
    .locals 1

    .prologue
    .line 2386790
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->e:Lcom/facebook/greetingcards/model/CardPhoto;

    return-object v0
.end method

.method public setListener(LX/Giy;)V
    .locals 0

    .prologue
    .line 2386788
    iput-object p1, p0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->b:LX/Giy;

    .line 2386789
    return-void
.end method
