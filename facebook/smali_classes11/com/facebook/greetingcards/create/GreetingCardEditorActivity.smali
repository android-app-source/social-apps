.class public Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public A:LX/Gik;

.field private B:Z

.field private p:Lcom/facebook/content/SecureContextHelper;

.field private q:LX/6Hl;

.field private r:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private s:LX/74n;

.field private t:Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;

.field public u:Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;

.field private v:Landroid/widget/EditText;

.field private w:Landroid/view/ViewStub;

.field public final x:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/GjL;",
            ">;"
        }
    .end annotation
.end field

.field public y:Lcom/facebook/greetingcards/create/GreetingCardSelfieView;

.field private z:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2387055
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2387056
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->x:Ljava/util/ArrayList;

    .line 2387057
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->B:Z

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/greetingcards/model/GreetingCard;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2387052
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2387053
    const-string v1, "args_greeting_card"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2387054
    return-object v0
.end method

.method private a()V
    .locals 13

    .prologue
    const/4 v12, -0x1

    const/16 v11, 0x8

    .line 2387025
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->w:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2387026
    iget-object v1, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->w:Landroid/view/ViewStub;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    .line 2387027
    iget-object v1, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->w:Landroid/view/ViewStub;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2387028
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b22e7

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2387029
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b22e9

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2387030
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b22e8

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 2387031
    const/4 v1, 0x0

    move v3, v1

    move v1, v2

    :goto_0
    if-ge v3, v11, :cond_1

    .line 2387032
    new-instance v7, LX/GjL;

    invoke-direct {v7, p0}, LX/GjL;-><init>(Landroid/content/Context;)V

    .line 2387033
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v8, -0x2

    invoke-direct {v2, v12, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2387034
    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2387035
    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2387036
    invoke-virtual {v7, v2}, LX/GjL;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2387037
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0, v7, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 2387038
    iget-object v1, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->x:Ljava/util/ArrayList;

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2387039
    const/4 v1, 0x7

    if-ge v3, v1, :cond_2

    .line 2387040
    new-instance v8, Landroid/view/View;

    const/4 v1, 0x0

    const v9, 0x7f0e0b87

    invoke-direct {v8, p0, v1, v9}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2387041
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a00e9

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-direct {v1, v9}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v8, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2387042
    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    .line 2387043
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v12, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2387044
    iput v6, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 2387045
    iput v6, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 2387046
    invoke-virtual {v8, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2387047
    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v0, v8, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 2387048
    :goto_1
    if-lez v3, :cond_0

    .line 2387049
    invoke-virtual {v7, v11}, LX/GjL;->setVisibility(I)V

    .line 2387050
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2387051
    :cond_1
    return-void

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method private a(LX/6Hl;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/74n;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2387020
    iput-object p1, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->q:LX/6Hl;

    .line 2387021
    iput-object p2, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->p:Lcom/facebook/content/SecureContextHelper;

    .line 2387022
    iput-object p3, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->r:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 2387023
    iput-object p4, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->s:LX/74n;

    .line 2387024
    return-void
.end method

.method private a(Lcom/facebook/greetingcards/model/GreetingCard;)V
    .locals 6
    .param p1    # Lcom/facebook/greetingcards/model/GreetingCard;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 2386998
    if-nez p1, :cond_0

    .line 2386999
    :goto_0
    return-void

    .line 2387000
    :cond_0
    iget-object v0, p1, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    if-eqz v0, :cond_2

    .line 2387001
    iget-object v0, p1, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v0, v0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2387002
    iget-object v1, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->u:Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;

    iget-object v0, p1, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v0, v0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/CardPhoto;

    invoke-virtual {v1, v0}, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->a(Lcom/facebook/greetingcards/model/CardPhoto;)V

    .line 2387003
    :cond_1
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->v:Landroid/widget/EditText;

    iget-object v1, p1, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v1, v1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2387004
    :cond_2
    iget-object v0, p1, Lcom/facebook/greetingcards/model/GreetingCard;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    .line 2387005
    const/16 v0, 0x8

    invoke-static {v4, v0}, LX/0PB;->checkPositionIndex(II)I

    move v2, v3

    .line 2387006
    :goto_1
    if-ge v2, v4, :cond_3

    .line 2387007
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GjL;

    iget-object v1, p1, Lcom/facebook/greetingcards/model/GreetingCard;->b:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    .line 2387008
    invoke-static {v0}, LX/GjL;->c(LX/GjL;)V

    .line 2387009
    iget-object v5, v0, LX/GjL;->c:Lcom/facebook/greetingcards/create/MomentView;

    invoke-virtual {v5, v1}, Lcom/facebook/greetingcards/create/MomentView;->setSlide(Lcom/facebook/greetingcards/model/GreetingCard$Slide;)V

    .line 2387010
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2387011
    :cond_3
    iget-object v0, p1, Lcom/facebook/greetingcards/model/GreetingCard;->c:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    if-eqz v0, :cond_5

    .line 2387012
    iget-object v0, p1, Lcom/facebook/greetingcards/model/GreetingCard;->c:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v0, v0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2387013
    iget-object v1, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->y:Lcom/facebook/greetingcards/create/GreetingCardSelfieView;

    iget-object v0, p1, Lcom/facebook/greetingcards/model/GreetingCard;->c:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v0, v0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/CardPhoto;

    invoke-virtual {v1, v0}, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;->a(Lcom/facebook/greetingcards/model/CardPhoto;)V

    .line 2387014
    :cond_4
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->z:Landroid/widget/EditText;

    iget-object v1, p1, Lcom/facebook/greetingcards/model/GreetingCard;->c:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v1, v1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2387015
    :cond_5
    iget-object v0, p1, Lcom/facebook/greetingcards/model/GreetingCard;->d:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 2387016
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->t:Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;

    iget-object v1, p1, Lcom/facebook/greetingcards/model/GreetingCard;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->a(Ljava/lang/String;)V

    .line 2387017
    iget-object v0, p1, Lcom/facebook/greetingcards/model/GreetingCard;->e:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 2387018
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->t:Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;

    iget-object v1, p1, Lcom/facebook/greetingcards/model/GreetingCard;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->setSelectedTheme(Ljava/lang/String;)V

    .line 2387019
    :cond_6
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->m()V

    goto/16 :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;

    invoke-static {v3}, LX/6Hl;->b(LX/0QB;)LX/6Hl;

    move-result-object v0

    check-cast v0, LX/6Hl;

    invoke-static {v3}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v3}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v2

    check-cast v2, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v3}, LX/74n;->b(LX/0QB;)LX/74n;

    move-result-object v3

    check-cast v3, LX/74n;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->a(LX/6Hl;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/74n;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2386990
    new-instance v0, LX/6WS;

    invoke-direct {v0, p0}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2386991
    invoke-virtual {v0}, LX/5OM;->c()LX/5OG;

    move-result-object v1

    .line 2386992
    instance-of v2, p1, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;

    if-eqz v2, :cond_0

    .line 2386993
    const v2, 0x7f0835da

    invoke-virtual {v1, v2}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v2

    new-instance v3, LX/Gj2;

    invoke-direct {v3, p0}, LX/Gj2;-><init>(Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;)V

    invoke-virtual {v2, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2386994
    :cond_0
    const v2, 0x7f0835db

    invoke-virtual {v1, v2}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v2

    new-instance v3, LX/Gj3;

    invoke-direct {v3, p0}, LX/Gj3;-><init>(Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;)V

    invoke-virtual {v2, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2386995
    const v2, 0x7f0835de

    invoke-virtual {v1, v2}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v1

    new-instance v2, LX/Gj4;

    invoke-direct {v2, p0}, LX/Gj4;-><init>(Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;)V

    invoke-virtual {v1, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2386996
    invoke-virtual {v0, p1}, LX/0ht;->a(Landroid/view/View;)V

    .line 2386997
    return-void
.end method

.method private b()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 2386977
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->u:Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;

    new-instance v2, LX/Giz;

    invoke-direct {v2, p0}, LX/Giz;-><init>(Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;)V

    .line 2386978
    iput-object v2, v0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->b:LX/Giy;

    .line 2386979
    move v0, v1

    .line 2386980
    :goto_0
    const/16 v2, 0x8

    if-ge v0, v2, :cond_0

    .line 2386981
    invoke-direct {p0, v0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->b(I)V

    .line 2386982
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2386983
    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->y:Lcom/facebook/greetingcards/create/GreetingCardSelfieView;

    new-instance v2, LX/Gj0;

    invoke-direct {v2, p0}, LX/Gj0;-><init>(Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;)V

    .line 2386984
    iput-object v2, v0, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;->c:LX/Giy;

    .line 2386985
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c0072

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2386986
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0073

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .line 2386987
    iget-object v3, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->v:Landroid/widget/EditText;

    new-instance v4, LX/Gil;

    iget-object v5, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->v:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0835ef

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v1

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, p0, v5, v0, v6}, LX/Gil;-><init>(Landroid/content/Context;Landroid/widget/EditText;ILjava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2386988
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->z:Landroid/widget/EditText;

    new-instance v3, LX/Gil;

    iget-object v4, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->z:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0835f0

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, p0, v4, v2, v1}, LX/Gil;-><init>(Landroid/content/Context;Landroid/widget/EditText;ILjava/lang/String;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2386989
    return-void
.end method

.method private b(I)V
    .locals 6

    .prologue
    .line 2386969
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/GjL;

    .line 2386970
    invoke-virtual {v2}, LX/GjL;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 2386971
    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v5

    .line 2386972
    new-instance v0, LX/Gj1;

    move-object v1, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, LX/Gj1;-><init>(Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;LX/GjL;ILandroid/view/ViewGroup;I)V

    .line 2386973
    iput-object v0, v2, LX/GjL;->a:LX/Gj1;

    .line 2386974
    iget-object v1, v2, LX/GjL;->c:Lcom/facebook/greetingcards/create/MomentView;

    .line 2386975
    iput-object v0, v1, Lcom/facebook/greetingcards/create/MomentView;->g:LX/Gj1;

    .line 2386976
    return-void
.end method

.method public static synthetic h(Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;)V
    .locals 0

    .prologue
    .line 2386968
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    return-void
.end method

.method private l()Lcom/facebook/greetingcards/model/GreetingCard;
    .locals 7

    .prologue
    .line 2387058
    new-instance v1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->v:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->u:Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;

    .line 2387059
    iget-object v4, v0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->e:Lcom/facebook/greetingcards/model/CardPhoto;

    move-object v0, v4

    .line 2387060
    if-nez v0, :cond_1

    .line 2387061
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2387062
    :goto_0
    invoke-direct {v1, v2, v3, v0}, Lcom/facebook/greetingcards/model/GreetingCard$Slide;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    .line 2387063
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2387064
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->x:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_2

    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GjL;

    .line 2387065
    invoke-virtual {v0}, LX/GjL;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2387066
    invoke-virtual {v0}, LX/GjL;->getDisplayedChild()I

    move-result v5

    if-nez v5, :cond_4

    .line 2387067
    const/4 v5, 0x0

    .line 2387068
    :goto_2
    move-object v0, v5

    .line 2387069
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2387070
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2387071
    :cond_1
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->u:Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;

    .line 2387072
    iget-object v4, v0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->e:Lcom/facebook/greetingcards/model/CardPhoto;

    move-object v0, v4

    .line 2387073
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 2387074
    :cond_2
    new-instance v3, Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->z:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v5, ""

    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->y:Lcom/facebook/greetingcards/create/GreetingCardSelfieView;

    .line 2387075
    iget-object v6, v0, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;->d:Lcom/facebook/greetingcards/model/CardPhoto;

    move-object v0, v6

    .line 2387076
    if-nez v0, :cond_3

    .line 2387077
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2387078
    :goto_3
    invoke-direct {v3, v2, v5, v0}, Lcom/facebook/greetingcards/model/GreetingCard$Slide;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    .line 2387079
    new-instance v0, Lcom/facebook/greetingcards/model/GreetingCard;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    iget-object v4, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->t:Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;

    .line 2387080
    iget-object v5, v4, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2387081
    iget-object v5, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->t:Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;

    .line 2387082
    iget-object v6, v5, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->e:Ljava/lang/String;

    move-object v5, v6

    .line 2387083
    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/facebook/greetingcards/model/GreetingCard;-><init>(Lcom/facebook/greetingcards/model/GreetingCard$Slide;LX/0Px;Lcom/facebook/greetingcards/model/GreetingCard$Slide;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 2387084
    :cond_3
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->y:Lcom/facebook/greetingcards/create/GreetingCardSelfieView;

    .line 2387085
    iget-object v6, v0, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;->d:Lcom/facebook/greetingcards/model/CardPhoto;

    move-object v0, v6

    .line 2387086
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_3

    :cond_4
    iget-object v5, v0, LX/GjL;->c:Lcom/facebook/greetingcards/create/MomentView;

    invoke-virtual {v5}, Lcom/facebook/greetingcards/create/MomentView;->getComposedSlide()Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    move-result-object v5

    goto :goto_2
.end method

.method private m()V
    .locals 2

    .prologue
    .line 2386871
    iget-boolean v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->B:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->u:Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;

    .line 2386872
    iget-object v1, v0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;->e:Lcom/facebook/greetingcards/model/CardPhoto;

    move-object v0, v1

    .line 2386873
    if-eqz v0, :cond_0

    .line 2386874
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->n()V

    .line 2386875
    :cond_0
    return-void
.end method

.method private n()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2386876
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2386877
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const/4 v2, 0x0

    .line 2386878
    iput v2, v1, LX/108;->a:I

    .line 2386879
    move-object v1, v1

    .line 2386880
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0835dd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2386881
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2386882
    move-object v1, v1

    .line 2386883
    iput-boolean v4, v1, LX/108;->d:Z

    .line 2386884
    move-object v1, v1

    .line 2386885
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2386886
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2386887
    iput-boolean v4, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->B:Z

    .line 2386888
    return-void
.end method

.method public static o(Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;)V
    .locals 3

    .prologue
    .line 2386889
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->r:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    sget-object v1, LX/0ax;->fh:Ljava/lang/String;

    invoke-interface {v0, p0, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2386890
    iget-object v1, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->p:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x26b9

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2386891
    return-void
.end method

.method public static p(Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;)V
    .locals 3

    .prologue
    .line 2386892
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->HOLIDAY_CARDS:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v1}, LX/8AA;->i()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->j()LX/8AA;

    move-result-object v1

    sget-object v2, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v1, v2}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    .line 2386893
    iget-object v1, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->p:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x7

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2386894
    return-void
.end method

.method public static q(Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2386895
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->q:LX/6Hl;

    .line 2386896
    iget-object v2, v0, LX/6Hl;->e:Landroid/content/Intent;

    const-string v3, "extra_no_composer"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2386897
    move-object v0, v0

    .line 2386898
    invoke-virtual {v0, v1}, LX/6Hl;->b(Z)LX/6Hl;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/6Hl;->a(I)LX/6Hl;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/6Hl;->c(Z)LX/6Hl;

    move-result-object v0

    .line 2386899
    iput-object p0, v0, LX/6Hl;->b:Landroid/app/Activity;

    .line 2386900
    move-object v0, v0

    .line 2386901
    invoke-virtual {v0}, LX/6Hl;->a()Landroid/content/Intent;

    move-result-object v0

    .line 2386902
    iget-object v1, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->p:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x6

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2386903
    return-void
.end method

.method private r()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2386904
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2386905
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2386906
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0835dc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2386907
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 2386908
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    .line 2386909
    iput v4, v1, LX/108;->a:I

    .line 2386910
    move-object v1, v1

    .line 2386911
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0835dd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2386912
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2386913
    move-object v1, v1

    .line 2386914
    iput-boolean v4, v1, LX/108;->d:Z

    .line 2386915
    move-object v1, v1

    .line 2386916
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2386917
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2386918
    new-instance v1, LX/Gj5;

    invoke-direct {v1, p0}, LX/Gj5;-><init>(Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2386919
    return-void
.end method

.method public static s(Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;)V
    .locals 3

    .prologue
    .line 2386920
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2386921
    const-string v1, "args_greeting_card"

    invoke-direct {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->l()Lcom/facebook/greetingcards/model/GreetingCard;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2386922
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->setResult(ILandroid/content/Intent;)V

    .line 2386923
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->finish()V

    .line 2386924
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2386925
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2386926
    invoke-static {p0, p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2386927
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    move-object v1, v0

    .line 2386928
    :goto_0
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2386929
    const v0, 0x7f030888

    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->setContentView(I)V

    .line 2386930
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->r()V

    .line 2386931
    const v0, 0x7f0d1620

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;

    iput-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->t:Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;

    .line 2386932
    const v0, 0x7f0d161e

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;

    iput-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->u:Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;

    .line 2386933
    const v0, 0x7f0d161f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->v:Landroid/widget/EditText;

    .line 2386934
    const v0, 0x7f0d1621

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->w:Landroid/view/ViewStub;

    .line 2386935
    const v0, 0x7f0d1622

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;

    iput-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->y:Lcom/facebook/greetingcards/create/GreetingCardSelfieView;

    .line 2386936
    const v0, 0x7f0d1623

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->z:Landroid/widget/EditText;

    .line 2386937
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->a()V

    .line 2386938
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->b()V

    .line 2386939
    invoke-direct {p0, v1}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->a(Lcom/facebook/greetingcards/model/GreetingCard;)V

    .line 2386940
    return-void

    .line 2386941
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "args_greeting_card"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/GreetingCard;

    move-object v1, v0

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v1, -0x1

    .line 2386942
    const/4 v0, 0x6

    if-ne p1, v0, :cond_3

    const/4 v0, 0x5

    if-ne p2, v0, :cond_3

    .line 2386943
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->s:LX/74n;

    invoke-static {}, Lcom/facebook/camera/activity/CameraActivity;->l()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, LX/74j;->DEFAULT:LX/74j;

    invoke-virtual {v0, v1, v2}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 2386944
    if-nez v0, :cond_1

    .line 2386945
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "Mediaitem is null"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2386946
    :cond_0
    :goto_0
    return-void

    .line 2386947
    :cond_1
    invoke-static {v0}, Lcom/facebook/greetingcards/model/CardPhoto;->a(Lcom/facebook/ipc/media/MediaItem;)Lcom/facebook/greetingcards/model/CardPhoto;

    move-result-object v0

    .line 2386948
    iget-object v1, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->A:LX/Gik;

    invoke-interface {v1, v0}, LX/Gik;->a(Lcom/facebook/greetingcards/model/CardPhoto;)V

    .line 2386949
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->A:LX/Gik;

    iget-object v1, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->u:Lcom/facebook/greetingcards/create/GreetingCardCoverPhotoView;

    if-ne v0, v1, :cond_0

    .line 2386950
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->m()V

    goto :goto_0

    .line 2386951
    :cond_3
    if-ne p2, v1, :cond_4

    const/4 v0, 0x7

    if-ne p1, v0, :cond_4

    .line 2386952
    const-string v0, "extra_media_items"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2386953
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 2386954
    invoke-static {v0}, Lcom/facebook/greetingcards/model/CardPhoto;->a(Lcom/facebook/ipc/media/MediaItem;)Lcom/facebook/greetingcards/model/CardPhoto;

    move-result-object v0

    .line 2386955
    iget-object v1, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->A:LX/Gik;

    invoke-interface {v1, v0}, LX/Gik;->a(Lcom/facebook/greetingcards/model/CardPhoto;)V

    goto :goto_1

    .line 2386956
    :cond_4
    if-ne p2, v1, :cond_2

    const/16 v0, 0x26b9

    if-ne p1, v0, :cond_2

    .line 2386957
    const-string v0, "photo"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2386958
    if-nez v0, :cond_5

    .line 2386959
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "PICKER_RESULT_PHOTO is not passed for greeting card"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2386960
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->G()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v1

    if-nez v1, :cond_6

    const/4 v0, 0x0

    :goto_2
    invoke-static {v2, v3, v0}, Lcom/facebook/greetingcards/model/CardPhoto;->a(Landroid/net/Uri;Ljava/lang/String;Landroid/graphics/PointF;)Lcom/facebook/greetingcards/model/CardPhoto;

    move-result-object v0

    .line 2386961
    iget-object v1, p0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->A:LX/Gik;

    invoke-interface {v1, v0}, LX/Gik;->a(Lcom/facebook/greetingcards/model/CardPhoto;)V

    goto :goto_1

    .line 2386962
    :cond_6
    new-instance v1, Landroid/graphics/PointF;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->G()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVect2;->a()D

    move-result-wide v4

    double-to-float v4, v4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->G()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVect2;->b()D

    move-result-wide v6

    double-to-float v0, v6

    invoke-direct {v1, v4, v0}, Landroid/graphics/PointF;-><init>(FF)V

    move-object v0, v1

    goto :goto_2
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    .line 2386963
    new-instance v0, LX/0ju;

    invoke-direct {v0, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080017

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f0835ea

    new-instance v2, LX/Gj6;

    invoke-direct {v2, p0}, LX/Gj6;-><init>(Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0835e9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2386964
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2386965
    const-string v0, "card"

    invoke-direct {p0}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->l()Lcom/facebook/greetingcards/model/GreetingCard;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2386966
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2386967
    return-void
.end method
