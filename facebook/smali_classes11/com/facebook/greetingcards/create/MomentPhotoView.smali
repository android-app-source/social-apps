.class public Lcom/facebook/greetingcards/create/MomentPhotoView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

.field public c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public d:Landroid/widget/ImageView;

.field public e:LX/GjH;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2387304
    const-class v0, Lcom/facebook/greetingcards/create/MomentPhotoView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/greetingcards/create/MomentPhotoView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2387305
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2387306
    const-class v0, Lcom/facebook/greetingcards/create/MomentPhotoView;

    invoke-static {v0, p0}, Lcom/facebook/greetingcards/create/MomentPhotoView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2387307
    const-string v1, "greeting_cards"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-static {p0, v1, p1}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/Class;)V

    .line 2387308
    const v0, 0x7f030b2c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2387309
    const v0, 0x7f0d1c28

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/greetingcards/create/MomentPhotoView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2387310
    const v0, 0x7f0d1c29

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/greetingcards/create/MomentPhotoView;->d:Landroid/widget/ImageView;

    .line 2387311
    iget-object v0, p0, Lcom/facebook/greetingcards/create/MomentPhotoView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v1, LX/GjF;

    invoke-direct {v1, p0}, LX/GjF;-><init>(Lcom/facebook/greetingcards/create/MomentPhotoView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2387312
    iget-object v0, p0, Lcom/facebook/greetingcards/create/MomentPhotoView;->d:Landroid/widget/ImageView;

    new-instance v1, LX/GjG;

    invoke-direct {v1, p0}, LX/GjG;-><init>(Lcom/facebook/greetingcards/create/MomentPhotoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2387313
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/greetingcards/create/MomentPhotoView;

    invoke-static {p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object p0

    check-cast p0, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object p0, p1, Lcom/facebook/greetingcards/create/MomentPhotoView;->b:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    return-void
.end method
