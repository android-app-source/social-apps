.class public final Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2386641
    const-class v0, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;

    new-instance v1, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2386642
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2386643
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2386644
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2386645
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2386646
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2386647
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2386648
    if-eqz v2, :cond_0

    .line 2386649
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2386650
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2386651
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2386652
    if-eqz v2, :cond_1

    .line 2386653
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2386654
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2386655
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2386656
    if-eqz v2, :cond_3

    .line 2386657
    const-string p0, "template_preview_images"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2386658
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2386659
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_2

    .line 2386660
    invoke-virtual {v1, v2, p0}, LX/15i;->q(II)I

    move-result v0

    invoke-static {v1, v0, p1, p2}, LX/Giq;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2386661
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 2386662
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2386663
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2386664
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2386665
    check-cast p1, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel$Serializer;->a(Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
