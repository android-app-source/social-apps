.class public final Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xa30e80f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2386680
    const-class v0, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2386681
    const-class v0, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2386682
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2386683
    return-void
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2386702
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2386703
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2386704
    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2386684
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2386685
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;->l()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2386686
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2386687
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;->k()LX/2uF;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v2

    .line 2386688
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2386689
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2386690
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2386691
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2386692
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2386693
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2386694
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2386695
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;->k()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2386696
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;->k()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 2386697
    if-eqz v1, :cond_0

    .line 2386698
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;

    .line 2386699
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;->g:LX/3Sb;

    .line 2386700
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2386701
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2386678
    new-instance v0, LX/Gio;

    invoke-direct {v0, p1}, LX/Gio;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2386679
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2386676
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2386677
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2386675
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2386672
    new-instance v0, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;

    invoke-direct {v0}, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;-><init>()V

    .line 2386673
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2386674
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2386671
    const v0, 0x11597ae4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2386670
    const v0, 0x252222

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2386668
    iget-object v0, p0, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;->f:Ljava/lang/String;

    .line 2386669
    iget-object v0, p0, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTemplatePreviewImages"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2386666
    iget-object v0, p0, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;->g:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x2

    const v4, -0x3ec62003

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;->g:LX/3Sb;

    .line 2386667
    iget-object v0, p0, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;->g:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method
