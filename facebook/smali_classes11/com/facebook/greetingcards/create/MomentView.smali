.class public Lcom/facebook/greetingcards/create/MomentView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/EditText;

.field private b:Landroid/widget/EditText;

.field private c:Lcom/facebook/greetingcards/create/MomentPhotosContainer;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/EditText;

.field public g:LX/Gj1;

.field public h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/greetingcards/model/CardPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2387489
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2387490
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/greetingcards/create/MomentView;->i:I

    .line 2387491
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/MomentView;->a()V

    .line 2387492
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2387443
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2387444
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/greetingcards/create/MomentView;->i:I

    .line 2387445
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/MomentView;->a()V

    .line 2387446
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2387475
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->h:Ljava/util/ArrayList;

    .line 2387476
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/create/MomentView;->setOrientation(I)V

    .line 2387477
    const v0, 0x7f03088a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2387478
    const v0, 0x7f0d1624

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->a:Landroid/widget/EditText;

    .line 2387479
    const v0, 0x7f0d1625

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->b:Landroid/widget/EditText;

    .line 2387480
    const v0, 0x7f0d1626

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/create/MomentPhotosContainer;

    iput-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->c:Lcom/facebook/greetingcards/create/MomentPhotosContainer;

    .line 2387481
    const v0, 0x7f0d1624

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->f:Landroid/widget/EditText;

    .line 2387482
    const v0, 0x7f0d1627

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->d:Landroid/widget/TextView;

    .line 2387483
    const v0, 0x7f0d1628

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->e:Landroid/view/View;

    .line 2387484
    iget-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->c:Lcom/facebook/greetingcards/create/MomentPhotosContainer;

    new-instance v1, LX/GjM;

    invoke-direct {v1, p0}, LX/GjM;-><init>(Lcom/facebook/greetingcards/create/MomentView;)V

    .line 2387485
    iput-object v1, v0, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->a:LX/GjI;

    .line 2387486
    invoke-static {p0}, Lcom/facebook/greetingcards/create/MomentView;->d$redex0(Lcom/facebook/greetingcards/create/MomentView;)V

    .line 2387487
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/MomentView;->b()V

    .line 2387488
    return-void
.end method

.method private b()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2387468
    iget-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->d:Landroid/widget/TextView;

    new-instance v1, LX/GjN;

    invoke-direct {v1, p0}, LX/GjN;-><init>(Lcom/facebook/greetingcards/create/MomentView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2387469
    iget-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->e:Landroid/view/View;

    new-instance v1, LX/GjP;

    invoke-direct {v1, p0}, LX/GjP;-><init>(Lcom/facebook/greetingcards/create/MomentView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2387470
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/MomentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0072

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2387471
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/MomentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0073

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 2387472
    iget-object v2, p0, Lcom/facebook/greetingcards/create/MomentView;->a:Landroid/widget/EditText;

    new-instance v3, LX/Gil;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/MomentView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/greetingcards/create/MomentView;->f:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/MomentView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0835ef

    new-array v8, v11, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v5, v0, v6}, LX/Gil;-><init>(Landroid/content/Context;Landroid/widget/EditText;ILjava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2387473
    iget-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->b:Landroid/widget/EditText;

    new-instance v2, LX/Gil;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/MomentView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/greetingcards/create/MomentView;->b:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/MomentView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0835f0

    new-array v7, v11, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v4, v1, v5}, LX/Gil;-><init>(Landroid/content/Context;Landroid/widget/EditText;ILjava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2387474
    return-void
.end method

.method public static c$redex0(Lcom/facebook/greetingcards/create/MomentView;)V
    .locals 3

    .prologue
    .line 2387462
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/greetingcards/create/MomentView;->i:I

    .line 2387463
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->h:Ljava/util/ArrayList;

    .line 2387464
    iget-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->a:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/MomentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0835d0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2387465
    iget-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->b:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2387466
    invoke-static {p0}, Lcom/facebook/greetingcards/create/MomentView;->d$redex0(Lcom/facebook/greetingcards/create/MomentView;)V

    .line 2387467
    return-void
.end method

.method public static d$redex0(Lcom/facebook/greetingcards/create/MomentView;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x4

    .line 2387493
    iget-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->c:Lcom/facebook/greetingcards/create/MomentPhotosContainer;

    iget-object v1, p0, Lcom/facebook/greetingcards/create/MomentView;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->a(Ljava/util/List;)V

    .line 2387494
    iget-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v5, :cond_0

    .line 2387495
    iget-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->d:Landroid/widget/TextView;

    const v1, 0x7f0835d7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2387496
    iget-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 2387497
    :goto_0
    return-void

    .line 2387498
    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/MomentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0835e8

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2387499
    iget-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/greetingcards/model/CardPhoto;)V
    .locals 2

    .prologue
    .line 2387457
    iget v0, p0, Lcom/facebook/greetingcards/create/MomentView;->i:I

    iget-object v1, p0, Lcom/facebook/greetingcards/create/MomentView;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2387458
    iget-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->h:Ljava/util/ArrayList;

    iget v1, p0, Lcom/facebook/greetingcards/create/MomentView;->i:I

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2387459
    :goto_0
    invoke-static {p0}, Lcom/facebook/greetingcards/create/MomentView;->d$redex0(Lcom/facebook/greetingcards/create/MomentView;)V

    .line 2387460
    return-void

    .line 2387461
    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getComposedSlide()Lcom/facebook/greetingcards/model/GreetingCard$Slide;
    .locals 4

    .prologue
    .line 2387456
    new-instance v0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v1, p0, Lcom/facebook/greetingcards/create/MomentView;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/greetingcards/create/MomentView;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/greetingcards/create/MomentView;->h:Ljava/util/ArrayList;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/greetingcards/model/GreetingCard$Slide;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    return-object v0
.end method

.method public setListener(LX/Gj1;)V
    .locals 0

    .prologue
    .line 2387454
    iput-object p1, p0, Lcom/facebook/greetingcards/create/MomentView;->g:LX/Gj1;

    .line 2387455
    return-void
.end method

.method public setSlide(Lcom/facebook/greetingcards/model/GreetingCard$Slide;)V
    .locals 5

    .prologue
    .line 2387447
    iget-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->a:Landroid/widget/EditText;

    iget-object v1, p1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2387448
    iget-object v0, p0, Lcom/facebook/greetingcards/create/MomentView;->b:Landroid/widget/EditText;

    iget-object v1, p1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2387449
    iget-object v2, p1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/CardPhoto;

    .line 2387450
    iget-object v4, p0, Lcom/facebook/greetingcards/create/MomentView;->h:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2387451
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2387452
    :cond_0
    invoke-static {p0}, Lcom/facebook/greetingcards/create/MomentView;->d$redex0(Lcom/facebook/greetingcards/create/MomentView;)V

    .line 2387453
    return-void
.end method
