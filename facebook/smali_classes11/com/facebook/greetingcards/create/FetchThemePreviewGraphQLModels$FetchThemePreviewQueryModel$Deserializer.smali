.class public final Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2386637
    const-class v0, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;

    new-instance v1, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2386638
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2386636
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2386571
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2386572
    const/4 v2, 0x0

    .line 2386573
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_8

    .line 2386574
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2386575
    :goto_0
    move v1, v2

    .line 2386576
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2386577
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2386578
    new-instance v1, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;

    invoke-direct {v1}, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;-><init>()V

    .line 2386579
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2386580
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2386581
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2386582
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2386583
    :cond_0
    return-object v1

    .line 2386584
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2386585
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_7

    .line 2386586
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2386587
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2386588
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_2

    if-eqz v5, :cond_2

    .line 2386589
    const-string v6, "__type__"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "__typename"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2386590
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    goto :goto_1

    .line 2386591
    :cond_4
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2386592
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 2386593
    :cond_5
    const-string v6, "template_preview_images"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2386594
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2386595
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_6

    .line 2386596
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_6

    .line 2386597
    const/4 v6, 0x0

    .line 2386598
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_d

    .line 2386599
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2386600
    :goto_3
    move v5, v6

    .line 2386601
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2386602
    :cond_6
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 2386603
    goto :goto_1

    .line 2386604
    :cond_7
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 2386605
    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2386606
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2386607
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2386608
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_8
    move v1, v2

    move v3, v2

    move v4, v2

    goto/16 :goto_1

    .line 2386609
    :cond_9
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2386610
    :cond_a
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_c

    .line 2386611
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2386612
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2386613
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, p0, :cond_a

    if-eqz v8, :cond_a

    .line 2386614
    const-string v9, "image"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 2386615
    const/4 v8, 0x0

    .line 2386616
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v9, :cond_11

    .line 2386617
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2386618
    :goto_5
    move v7, v8

    .line 2386619
    goto :goto_4

    .line 2386620
    :cond_b
    const-string v9, "theme"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 2386621
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_4

    .line 2386622
    :cond_c
    const/4 v8, 0x2

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 2386623
    invoke-virtual {v0, v6, v7}, LX/186;->b(II)V

    .line 2386624
    const/4 v6, 0x1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 2386625
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto :goto_3

    :cond_d
    move v5, v6

    move v7, v6

    goto :goto_4

    .line 2386626
    :cond_e
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2386627
    :cond_f
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_10

    .line 2386628
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2386629
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2386630
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_f

    if-eqz v9, :cond_f

    .line 2386631
    const-string p0, "uri"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 2386632
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_6

    .line 2386633
    :cond_10
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 2386634
    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 2386635
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    goto :goto_5

    :cond_11
    move v7, v8

    goto :goto_6
.end method
