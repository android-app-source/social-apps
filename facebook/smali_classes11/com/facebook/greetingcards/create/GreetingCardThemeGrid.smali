.class public Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;
.super Landroid/widget/TableLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0tX;

.field private c:Ljava/util/concurrent/Executor;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field private final f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2387209
    invoke-direct {p0, p1}, Landroid/widget/TableLayout;-><init>(Landroid/content/Context;)V

    .line 2387210
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->f:Ljava/util/HashMap;

    .line 2387211
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->a()V

    .line 2387212
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2387205
    invoke-direct {p0, p1, p2}, Landroid/widget/TableLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2387206
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->f:Ljava/util/HashMap;

    .line 2387207
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->a()V

    .line 2387208
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 4

    .prologue
    .line 2387193
    new-instance v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2387194
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2387195
    new-instance v0, LX/Gj8;

    invoke-direct {v0, p0, p1}, LX/Gj8;-><init>(Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2387196
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const v3, -0x333334

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2387197
    iput-object v2, v0, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2387198
    move-object v0, v0

    .line 2387199
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020cb8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2387200
    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2387201
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    .line 2387202
    invoke-virtual {v0, p2}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "greeting_cards"

    invoke-static {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2387203
    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2387204
    return-object v1
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2387191
    const-class v0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;

    invoke-static {v0, p0}, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2387192
    return-void
.end method

.method private a(LX/0Or;LX/0tX;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/0tX;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2387187
    iput-object p1, p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->a:LX/0Or;

    .line 2387188
    iput-object p2, p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->b:LX/0tX;

    .line 2387189
    iput-object p3, p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->c:Ljava/util/concurrent/Executor;

    .line 2387190
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;

    const/16 v0, 0x509

    invoke-static {v1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {v1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-direct {p0, v2, v0, v1}, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->a(LX/0Or;LX/0tX;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method public static setupCells(Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;LX/2uF;)V
    .locals 11

    .prologue
    const/4 v7, -0x1

    const/4 v5, -0x2

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 2387153
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->removeAllViews()V

    .line 2387154
    new-instance v0, Landroid/widget/TableRow;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 2387155
    new-instance v3, Landroid/widget/TableLayout$LayoutParams;

    invoke-direct {v3, v5, v7}, Landroid/widget/TableLayout$LayoutParams;-><init>(II)V

    .line 2387156
    iput v6, v3, Landroid/widget/TableLayout$LayoutParams;->weight:F

    .line 2387157
    invoke-virtual {v0, v3}, Landroid/widget/TableRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2387158
    new-instance v4, Landroid/widget/TableRow$LayoutParams;

    invoke-direct {v4, v7, v5}, Landroid/widget/TableRow$LayoutParams;-><init>(II)V

    .line 2387159
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b22e4

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2387160
    invoke-virtual {v4, v2, v2, v2, v2}, Landroid/widget/TableRow$LayoutParams;->setMargins(IIII)V

    .line 2387161
    iput v6, v4, Landroid/widget/TableRow$LayoutParams;->weight:F

    .line 2387162
    invoke-virtual {p1}, LX/3Sa;->e()LX/3Sh;

    move-result-object v5

    move-object v2, v0

    move v0, v1

    :cond_0
    :goto_0
    invoke-interface {v5}, LX/2sN;->a()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2387163
    invoke-interface {v5}, LX/2sN;->b()LX/1vs;

    move-result-object v6

    .line 2387164
    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 2387165
    invoke-virtual {v7, v6, v1}, LX/15i;->g(II)I

    move-result v8

    .line 2387166
    invoke-virtual {v7, v6, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v9, v8}, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v8

    .line 2387167
    invoke-virtual {v8, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2387168
    invoke-virtual {v2, v8}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 2387169
    iget-object v9, p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->f:Ljava/util/HashMap;

    invoke-virtual {v7, v6, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9, v6, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2387170
    add-int/lit8 v0, v0, 0x1

    const/4 v6, 0x3

    if-ne v0, v6, :cond_0

    .line 2387171
    invoke-virtual {p0, v2}, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->addView(Landroid/view/View;)V

    .line 2387172
    new-instance v2, Landroid/widget/TableRow;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 2387173
    invoke-virtual {v2, v3}, Landroid/widget/TableRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move v0, v1

    .line 2387174
    goto :goto_0

    .line 2387175
    :cond_1
    if-lez v0, :cond_3

    .line 2387176
    :goto_1
    const/4 v3, 0x3

    if-ge v0, v3, :cond_2

    .line 2387177
    new-instance v3, Landroid/view/View;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v5}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2387178
    invoke-virtual {v3, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2387179
    invoke-virtual {v2, v3}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 2387180
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2387181
    :cond_2
    invoke-virtual {p0, v2}, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->addView(Landroid/view/View;)V

    .line 2387182
    :cond_3
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->e:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->f:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2387183
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->f:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setSelected(Z)V

    .line 2387184
    :cond_4
    :goto_2
    return-void

    .line 2387185
    :cond_5
    invoke-virtual {p1}, LX/39O;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2387186
    invoke-virtual {p1, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->setSelectedTheme(Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2387146
    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v0

    .line 2387147
    new-instance v1, LX/Gim;

    invoke-direct {v1}, LX/Gim;-><init>()V

    move-object v1, v1

    .line 2387148
    const-string v2, "scale"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    const-string v2, "rootID"

    invoke-virtual {v0, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2387149
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/32 v2, 0x93a80

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 2387150
    iget-object v1, p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2387151
    new-instance v1, LX/Gj9;

    invoke-direct {v1, p0}, LX/Gj9;-><init>(Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;)V

    iget-object v2, p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->c:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2387152
    return-void
.end method

.method public getSelectedTheme()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2387136
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getTemplateId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2387145
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->d:Ljava/lang/String;

    return-object v0
.end method

.method public setSelectedTheme(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2387137
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2387138
    if-eqz v0, :cond_0

    .line 2387139
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 2387140
    iget-object v1, p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->f:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 2387141
    if-eqz v1, :cond_0

    if-eq v1, v0, :cond_0

    .line 2387142
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setSelected(Z)V

    .line 2387143
    :cond_0
    iput-object p1, p0, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->e:Ljava/lang/String;

    .line 2387144
    return-void
.end method
