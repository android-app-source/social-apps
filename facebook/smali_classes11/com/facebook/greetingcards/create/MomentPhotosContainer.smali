.class public Lcom/facebook/greetingcards/create/MomentPhotosContainer;
.super LX/GjJ;
.source ""


# instance fields
.field public a:LX/GjI;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2387327
    invoke-direct {p0, p1}, LX/GjJ;-><init>(Landroid/content/Context;)V

    .line 2387328
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b22e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->b:I

    .line 2387329
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->setOrientation(I)V

    .line 2387330
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 2387378
    invoke-direct {p0, p1, p2}, LX/GjJ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2387379
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b22e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->b:I

    .line 2387380
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->setOrientation(I)V

    .line 2387381
    return-void
.end method

.method private a()Landroid/widget/LinearLayout$LayoutParams;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2387375
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 2387376
    iget v1, p0, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->b:I

    invoke-virtual {v0, v3, v3, v3, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2387377
    return-object v0
.end method

.method private a(Lcom/facebook/greetingcards/create/MomentPhotoView;Lcom/facebook/greetingcards/create/MomentPhotoView;)Landroid/widget/LinearLayout;
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 2387364
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2387365
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2387366
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v3, v5, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 2387367
    iget v2, p0, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->b:I

    invoke-virtual {v1, v3, v3, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2387368
    invoke-virtual {p1, v1}, Lcom/facebook/greetingcards/create/MomentPhotoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2387369
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2387370
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v3, v5, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 2387371
    iget v2, p0, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->b:I

    invoke-virtual {v1, v2, v3, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2387372
    invoke-virtual {p2, v1}, Lcom/facebook/greetingcards/create/MomentPhotoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2387373
    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2387374
    return-object v0
.end method

.method private a(Lcom/facebook/greetingcards/model/CardPhoto;I)Lcom/facebook/greetingcards/create/MomentPhotoView;
    .locals 2
    .param p1    # Lcom/facebook/greetingcards/model/CardPhoto;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2387382
    new-instance v0, Lcom/facebook/greetingcards/create/MomentPhotoView;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/greetingcards/create/MomentPhotoView;-><init>(Landroid/content/Context;)V

    .line 2387383
    new-instance v1, LX/GjH;

    invoke-direct {v1, p0, p2}, LX/GjH;-><init>(Lcom/facebook/greetingcards/create/MomentPhotosContainer;I)V

    .line 2387384
    iput-object v1, v0, Lcom/facebook/greetingcards/create/MomentPhotoView;->e:LX/GjH;

    .line 2387385
    if-nez p1, :cond_1

    .line 2387386
    iget-object v1, v0, Lcom/facebook/greetingcards/create/MomentPhotoView;->d:Landroid/widget/ImageView;

    const/16 p0, 0x8

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2387387
    :cond_0
    :goto_0
    return-object v0

    .line 2387388
    :cond_1
    iget-object v1, v0, Lcom/facebook/greetingcards/create/MomentPhotoView;->d:Landroid/widget/ImageView;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2387389
    iget-object v1, v0, Lcom/facebook/greetingcards/create/MomentPhotoView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object p0, p1, Lcom/facebook/greetingcards/model/CardPhoto;->a:Landroid/net/Uri;

    sget-object p2, Lcom/facebook/greetingcards/create/MomentPhotoView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, p0, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2387390
    iget-object v1, p1, Lcom/facebook/greetingcards/model/CardPhoto;->e:Landroid/graphics/PointF;

    if-eqz v1, :cond_0

    .line 2387391
    new-instance v1, LX/1Uo;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/create/MomentPhotoView;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-direct {v1, p0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    iget-object p0, p1, Lcom/facebook/greetingcards/model/CardPhoto;->e:Landroid/graphics/PointF;

    .line 2387392
    iput-object p0, v1, LX/1Uo;->p:Landroid/graphics/PointF;

    .line 2387393
    move-object v1, v1

    .line 2387394
    sget-object p0, LX/1Up;->h:LX/1Up;

    invoke-virtual {v1, p0}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v1

    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    .line 2387395
    iget-object p0, v0, Lcom/facebook/greetingcards/create/MomentPhotoView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    goto :goto_0
.end method

.method private b()Landroid/widget/LinearLayout$LayoutParams;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2387361
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 2387362
    iget v1, p0, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->b:I

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2387363
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/greetingcards/model/CardPhoto;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2387333
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2387334
    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->removeAllViews()V

    .line 2387335
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2387336
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/CardPhoto;

    invoke-direct {p0, v0, v1}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->a(Lcom/facebook/greetingcards/model/CardPhoto;I)Lcom/facebook/greetingcards/create/MomentPhotoView;

    move-result-object v1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/CardPhoto;

    invoke-direct {p0, v0, v2}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->a(Lcom/facebook/greetingcards/model/CardPhoto;I)Lcom/facebook/greetingcards/create/MomentPhotoView;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->a(Lcom/facebook/greetingcards/create/MomentPhotoView;Lcom/facebook/greetingcards/create/MomentPhotoView;)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 2387337
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->a()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2387338
    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->addView(Landroid/view/View;)V

    .line 2387339
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/CardPhoto;

    invoke-direct {p0, v0, v4}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->a(Lcom/facebook/greetingcards/model/CardPhoto;I)Lcom/facebook/greetingcards/create/MomentPhotoView;

    move-result-object v1

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/CardPhoto;

    invoke-direct {p0, v0, v5}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->a(Lcom/facebook/greetingcards/model/CardPhoto;I)Lcom/facebook/greetingcards/create/MomentPhotoView;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->a(Lcom/facebook/greetingcards/create/MomentPhotoView;Lcom/facebook/greetingcards/create/MomentPhotoView;)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 2387340
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->b()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2387341
    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->addView(Landroid/view/View;)V

    .line 2387342
    :goto_0
    return-void

    .line 2387343
    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->a(Lcom/facebook/greetingcards/model/CardPhoto;I)Lcom/facebook/greetingcards/create/MomentPhotoView;

    move-result-object v0

    .line 2387344
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2387345
    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 2387346
    :pswitch_1
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/CardPhoto;

    invoke-direct {p0, v0, v1}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->a(Lcom/facebook/greetingcards/model/CardPhoto;I)Lcom/facebook/greetingcards/create/MomentPhotoView;

    move-result-object v0

    .line 2387347
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2387348
    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 2387349
    :pswitch_2
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/CardPhoto;

    invoke-direct {p0, v0, v1}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->a(Lcom/facebook/greetingcards/model/CardPhoto;I)Lcom/facebook/greetingcards/create/MomentPhotoView;

    move-result-object v0

    .line 2387350
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->a()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2387351
    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->addView(Landroid/view/View;)V

    .line 2387352
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/CardPhoto;

    invoke-direct {p0, v0, v2}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->a(Lcom/facebook/greetingcards/model/CardPhoto;I)Lcom/facebook/greetingcards/create/MomentPhotoView;

    move-result-object v0

    .line 2387353
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->b()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2387354
    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 2387355
    :pswitch_3
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/CardPhoto;

    invoke-direct {p0, v0, v1}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->a(Lcom/facebook/greetingcards/model/CardPhoto;I)Lcom/facebook/greetingcards/create/MomentPhotoView;

    move-result-object v0

    .line 2387356
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->a()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2387357
    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->addView(Landroid/view/View;)V

    .line 2387358
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/CardPhoto;

    invoke-direct {p0, v0, v2}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->a(Lcom/facebook/greetingcards/model/CardPhoto;I)Lcom/facebook/greetingcards/create/MomentPhotoView;

    move-result-object v1

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/CardPhoto;

    invoke-direct {p0, v0, v4}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->a(Lcom/facebook/greetingcards/model/CardPhoto;I)Lcom/facebook/greetingcards/create/MomentPhotoView;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->a(Lcom/facebook/greetingcards/create/MomentPhotoView;Lcom/facebook/greetingcards/create/MomentPhotoView;)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 2387359
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->b()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2387360
    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setListener(LX/GjI;)V
    .locals 0

    .prologue
    .line 2387331
    iput-object p1, p0, Lcom/facebook/greetingcards/create/MomentPhotosContainer;->a:LX/GjI;

    .line 2387332
    return-void
.end method
