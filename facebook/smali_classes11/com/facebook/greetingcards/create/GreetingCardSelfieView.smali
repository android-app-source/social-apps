.class public Lcom/facebook/greetingcards/create/GreetingCardSelfieView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Gik;


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/facebook/drawee/view/DraweeView;

.field public c:LX/Giy;

.field public d:Lcom/facebook/greetingcards/model/CardPhoto;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2387090
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2387091
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;->a()V

    .line 2387092
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2387093
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2387094
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;->a()V

    .line 2387095
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2387096
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2387097
    invoke-direct {p0}, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;->a()V

    .line 2387098
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2387099
    const-class v0, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;

    invoke-static {v0, p0}, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2387100
    const v0, 0x7f03088b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2387101
    const v0, 0x7f0d1629

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/DraweeView;

    iput-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;->b:Lcom/facebook/drawee/view/DraweeView;

    .line 2387102
    new-instance v0, LX/Gj7;

    invoke-direct {v0, p0}, LX/Gj7;-><init>(Lcom/facebook/greetingcards/create/GreetingCardSelfieView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2387103
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;

    const/16 v1, 0x509

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;->a:LX/0Or;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/greetingcards/model/CardPhoto;)V
    .locals 3
    .param p1    # Lcom/facebook/greetingcards/model/CardPhoto;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2387104
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;->d:Lcom/facebook/greetingcards/model/CardPhoto;

    if-ne v0, p1, :cond_0

    .line 2387105
    :goto_0
    return-void

    .line 2387106
    :cond_0
    iput-object p1, p0, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;->d:Lcom/facebook/greetingcards/model/CardPhoto;

    .line 2387107
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v1

    .line 2387108
    iput-object v1, v0, LX/1Uo;->u:LX/4Ab;

    .line 2387109
    move-object v0, v0

    .line 2387110
    iget-object v1, p1, Lcom/facebook/greetingcards/model/CardPhoto;->e:Landroid/graphics/PointF;

    if-eqz v1, :cond_1

    .line 2387111
    iget-object v1, p1, Lcom/facebook/greetingcards/model/CardPhoto;->e:Landroid/graphics/PointF;

    .line 2387112
    iput-object v1, v0, LX/1Uo;->p:Landroid/graphics/PointF;

    .line 2387113
    sget-object v1, LX/1Up;->h:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    .line 2387114
    :cond_1
    iget-object v1, p0, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;->b:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2387115
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    .line 2387116
    iget-object v1, p1, Lcom/facebook/greetingcards/model/CardPhoto;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "greeting_cards"

    invoke-static {v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2387117
    iget-object v1, p0, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;->b:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    goto :goto_0
.end method

.method public getCardPhoto()Lcom/facebook/greetingcards/model/CardPhoto;
    .locals 1

    .prologue
    .line 2387118
    iget-object v0, p0, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;->d:Lcom/facebook/greetingcards/model/CardPhoto;

    return-object v0
.end method

.method public setListener(LX/Giy;)V
    .locals 0

    .prologue
    .line 2387119
    iput-object p1, p0, Lcom/facebook/greetingcards/create/GreetingCardSelfieView;->c:LX/Giy;

    .line 2387120
    return-void
.end method
