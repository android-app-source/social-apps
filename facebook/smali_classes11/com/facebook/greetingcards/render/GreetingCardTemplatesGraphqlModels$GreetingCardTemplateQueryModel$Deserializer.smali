.class public final Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2388244
    const-class v0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;

    new-instance v1, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2388245
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2388246
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2388247
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2388248
    const/4 v2, 0x0

    .line 2388249
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_9

    .line 2388250
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2388251
    :goto_0
    move v1, v2

    .line 2388252
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2388253
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2388254
    new-instance v1, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;

    invoke-direct {v1}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;-><init>()V

    .line 2388255
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2388256
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2388257
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2388258
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2388259
    :cond_0
    return-object v1

    .line 2388260
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2388261
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_8

    .line 2388262
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2388263
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2388264
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_2

    if-eqz v6, :cond_2

    .line 2388265
    const-string v7, "__type__"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    const-string v7, "__typename"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2388266
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v5

    goto :goto_1

    .line 2388267
    :cond_4
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2388268
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2388269
    :cond_5
    const-string v7, "template_data"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2388270
    invoke-static {p1, v0}, LX/Gjj;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2388271
    :cond_6
    const-string v7, "template_images"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2388272
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2388273
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_7

    .line 2388274
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_7

    .line 2388275
    const/4 v7, 0x0

    .line 2388276
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v8, :cond_e

    .line 2388277
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2388278
    :goto_3
    move v6, v7

    .line 2388279
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2388280
    :cond_7
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 2388281
    goto/16 :goto_1

    .line 2388282
    :cond_8
    const/4 v6, 0x4

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 2388283
    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2388284
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2388285
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2388286
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2388287
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_9
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    goto/16 :goto_1

    .line 2388288
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2388289
    :cond_b
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_d

    .line 2388290
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2388291
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2388292
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_b

    if-eqz v9, :cond_b

    .line 2388293
    const-string p0, "name"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_c

    .line 2388294
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_4

    .line 2388295
    :cond_c
    const-string p0, "uri"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 2388296
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_4

    .line 2388297
    :cond_d
    const/4 v9, 0x2

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 2388298
    invoke-virtual {v0, v7, v8}, LX/186;->b(II)V

    .line 2388299
    const/4 v7, 0x1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 2388300
    invoke-virtual {v0}, LX/186;->d()I

    move-result v7

    goto :goto_3

    :cond_e
    move v6, v7

    move v8, v7

    goto :goto_4
.end method
