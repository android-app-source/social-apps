.class public final Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3938749b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2388434
    const-class v0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2388433
    const-class v0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2388431
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2388432
    return-void
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2388428
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2388429
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2388430
    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2388426
    iget-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->f:Ljava/lang/String;

    .line 2388427
    iget-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2388414
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2388415
    invoke-direct {p0}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->l()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2388416
    invoke-direct {p0}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2388417
    invoke-virtual {p0}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->j()Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2388418
    invoke-virtual {p0}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->k()LX/2uF;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v3

    .line 2388419
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2388420
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2388421
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2388422
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2388423
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2388424
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2388425
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2388401
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2388402
    invoke-virtual {p0}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->j()Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2388403
    invoke-virtual {p0}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->j()Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;

    .line 2388404
    invoke-virtual {p0}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->j()Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2388405
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;

    .line 2388406
    iput-object v0, v1, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->g:Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;

    .line 2388407
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->k()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2388408
    invoke-virtual {p0}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->k()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 2388409
    if-eqz v2, :cond_1

    .line 2388410
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;

    .line 2388411
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->h:LX/3Sb;

    move-object v1, v0

    .line 2388412
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2388413
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2388400
    new-instance v0, LX/Gjh;

    invoke-direct {v0, p1}, LX/Gjh;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2388387
    invoke-direct {p0}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2388398
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2388399
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2388397
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2388394
    new-instance v0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;

    invoke-direct {v0}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;-><init>()V

    .line 2388395
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2388396
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2388393
    const v0, 0x28533a77

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2388392
    const v0, 0x252222

    return v0
.end method

.method public final j()Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2388390
    iget-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->g:Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;

    iput-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->g:Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;

    .line 2388391
    iget-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->g:Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;

    return-object v0
.end method

.method public final k()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTemplateImages"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2388388
    iget-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->h:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x3

    const v4, -0xb27f5fa

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->h:LX/3Sb;

    .line 2388389
    iget-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->h:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method
