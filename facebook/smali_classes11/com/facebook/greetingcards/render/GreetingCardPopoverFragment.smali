.class public Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;
.super Lcom/facebook/widget/popover/PopoverFragment;
.source ""

# interfaces
.implements LX/Gjd;


# instance fields
.field public m:Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;

.field private n:LX/8qU;

.field public o:LX/0Zb;

.field private p:LX/Gjt;

.field private q:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2388149
    invoke-direct {p0}, Lcom/facebook/widget/popover/PopoverFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/greetingcards/render/RenderCardFragment;LX/0gc;Landroid/view/Window;Landroid/view/View;)Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;
    .locals 1

    .prologue
    .line 2388150
    new-instance v0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;

    invoke-direct {v0}, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;-><init>()V

    .line 2388151
    iput-object p0, v0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->m:Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;

    .line 2388152
    move-object v0, v0

    .line 2388153
    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/widget/popover/PopoverFragment;->a(LX/0gc;Landroid/view/Window;Landroid/view/View;)V

    .line 2388154
    return-object v0
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 2388155
    iget-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->m:Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->m:Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2388156
    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->p:LX/Gjt;

    invoke-virtual {v0}, LX/Gjt;->b()V

    .line 2388157
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->S_()Z

    .line 2388158
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2388159
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->q:Z

    .line 2388160
    return-void
.end method

.method public final a(Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;)V
    .locals 2

    .prologue
    .line 2388145
    iput-object p1, p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->m:Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;

    .line 2388146
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d0807

    invoke-virtual {v0, v1, p1}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2388147
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 2388148
    const v0, 0x7f030873

    return v0
.end method

.method public final o()LX/8qU;
    .locals 1

    .prologue
    .line 2388142
    iget-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->n:LX/8qU;

    if-nez v0, :cond_0

    .line 2388143
    new-instance v0, LX/Gje;

    invoke-direct {v0, p0}, LX/Gje;-><init>(Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;)V

    iput-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->n:LX/8qU;

    .line 2388144
    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->n:LX/8qU;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1c3a695c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2388139
    invoke-super {p0, p1}, Lcom/facebook/widget/popover/PopoverFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2388140
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;

    invoke-static {v1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    iput-object v1, p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->o:LX/0Zb;

    .line 2388141
    const/16 v1, 0x2b

    const v2, -0x285783ae

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x606af9b4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2388131
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/widget/popover/PopoverFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 2388132
    new-instance v2, LX/Gjt;

    .line 2388133
    iget-object v3, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v3, v3

    .line 2388134
    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-direct {v2, v3}, LX/Gjt;-><init>(Landroid/view/Window;)V

    iput-object v2, p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->p:LX/Gjt;

    .line 2388135
    iget-object v2, p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->p:LX/Gjt;

    invoke-virtual {v2}, LX/Gjt;->a()V

    .line 2388136
    iget-object v2, p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->m:Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;

    if-eqz v2, :cond_0

    .line 2388137
    iget-object v2, p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->m:Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;

    invoke-virtual {p0, v2}, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->a(Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;)V

    .line 2388138
    :cond_0
    const/16 v2, 0x2b

    const v3, -0x29deeade

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4db6a777    # 3.83053536E8f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2388126
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->onDestroy()V

    .line 2388127
    iget-object v1, p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->o:LX/0Zb;

    const-string v2, "greeting_card_dismissed"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2388128
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2388129
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2388130
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x2c2238dd

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2efb3af5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2388119
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->onResume()V

    .line 2388120
    iget-boolean v1, p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->q:Z

    if-eqz v1, :cond_0

    .line 2388121
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2388122
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->q:Z

    .line 2388123
    :cond_0
    iget-object v1, p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->p:LX/Gjt;

    if-eqz v1, :cond_1

    .line 2388124
    iget-object v1, p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->p:LX/Gjt;

    invoke-virtual {v1}, LX/Gjt;->a()V

    .line 2388125
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x3ecc1f0d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 2388118
    const/4 v0, 0x0

    return v0
.end method
