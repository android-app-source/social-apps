.class public final Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2388340
    const-class v0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;

    new-instance v1, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2388341
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2388339
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2388303
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2388304
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v3, 0x0

    .line 2388305
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2388306
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 2388307
    if-eqz v2, :cond_0

    .line 2388308
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2388309
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2388310
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2388311
    if-eqz v2, :cond_1

    .line 2388312
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2388313
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2388314
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2388315
    if-eqz v2, :cond_2

    .line 2388316
    const-string v3, "template_data"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2388317
    invoke-static {v1, v2, p1}, LX/Gjj;->a(LX/15i;ILX/0nX;)V

    .line 2388318
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2388319
    if-eqz v2, :cond_6

    .line 2388320
    const-string v3, "template_images"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2388321
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2388322
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_5

    .line 2388323
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    .line 2388324
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2388325
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2388326
    if-eqz v0, :cond_3

    .line 2388327
    const-string p2, "name"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2388328
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2388329
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2388330
    if-eqz v0, :cond_4

    .line 2388331
    const-string p2, "uri"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2388332
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2388333
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2388334
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2388335
    :cond_5
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2388336
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2388337
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2388338
    check-cast p1, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$Serializer;->a(Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
