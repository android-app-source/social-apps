.class public Lcom/facebook/greetingcards/render/FoldingPopoverFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""

# interfaces
.implements LX/Gjd;


# static fields
.field public static final m:Ljava/lang/String;


# instance fields
.field private A:Z

.field public B:Lcom/facebook/greetingcards/render/RenderCardFragment;

.field public n:Lcom/facebook/greetingcards/render/DraggableView;

.field public o:Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;

.field public p:LX/GjY;

.field public q:Landroid/graphics/Rect;

.field public r:Landroid/graphics/Bitmap;

.field public s:LX/Gjt;

.field public t:LX/03V;

.field public u:Z

.field public v:LX/GjZ;

.field public w:LX/8YK;

.field public x:LX/JOF;

.field public y:LX/0Zb;

.field public z:LX/0gc;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2388115
    const-class v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2388113
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2388114
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object p0

    check-cast p0, LX/0Zb;

    iput-object v1, p1, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->t:LX/03V;

    iput-object p0, p1, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->y:LX/0Zb;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/greetingcards/render/FoldingPopoverFragment;D)V
    .locals 5

    .prologue
    .line 2388036
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->v:LX/GjZ;

    if-eqz v0, :cond_0

    .line 2388037
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->v:LX/GjZ;

    invoke-virtual {v0}, LX/GjZ;->c()V

    .line 2388038
    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->w:LX/8YK;

    invoke-virtual {v0, p1, p2}, LX/8YK;->c(D)LX/8YK;

    move-result-object v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/8YK;->b(D)LX/8YK;

    .line 2388039
    return-void
.end method

.method public static b$redex0(Lcom/facebook/greetingcards/render/FoldingPopoverFragment;D)V
    .locals 5

    .prologue
    .line 2388110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->u:Z

    .line 2388111
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->w:LX/8YK;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/8YK;->b(D)LX/8YK;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/8YK;->c(D)LX/8YK;

    .line 2388112
    return-void
.end method

.method public static k(Lcom/facebook/greetingcards/render/FoldingPopoverFragment;)V
    .locals 2

    .prologue
    .line 2388108
    const-wide/16 v0, 0x0

    invoke-static {p0, v0, v1}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->a$redex0(Lcom/facebook/greetingcards/render/FoldingPopoverFragment;D)V

    .line 2388109
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 2388103
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->o:Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->p:LX/GjY;

    if-nez v0, :cond_2

    .line 2388104
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2388105
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2388106
    :cond_2
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->o:Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->u:Z

    if-nez v0, :cond_1

    .line 2388107
    invoke-virtual {p0}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->b()V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 2388102
    new-instance v0, LX/Gjc;

    invoke-direct {v0, p0}, LX/Gjc;-><init>(Lcom/facebook/greetingcards/render/FoldingPopoverFragment;)V

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2388100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->A:Z

    .line 2388101
    return-void
.end method

.method public final a(Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;)V
    .locals 2

    .prologue
    .line 2388095
    if-eqz p1, :cond_1

    .line 2388096
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->o:Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;

    if-eq v0, p1, :cond_0

    .line 2388097
    iput-object p1, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->o:Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;

    .line 2388098
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d11db

    invoke-virtual {v0, v1, p1}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2388099
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2388093
    const-wide/16 v0, 0x0

    invoke-static {p0, v0, v1}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->b$redex0(Lcom/facebook/greetingcards/render/FoldingPopoverFragment;D)V

    .line 2388094
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2388081
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->n:Lcom/facebook/greetingcards/render/DraggableView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/greetingcards/render/DraggableView;->setVisibility(I)V

    .line 2388082
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->r:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 2388083
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->r:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2388084
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2388085
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->z:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 2388086
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->u:Z

    .line 2388087
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->v:LX/GjZ;

    if-eqz v0, :cond_1

    .line 2388088
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->v:LX/GjZ;

    const/high16 p0, 0x3f800000    # 1.0f

    .line 2388089
    iget-object v1, v0, LX/GjZ;->b:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 2388090
    iget-object v1, v0, LX/GjZ;->b:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setScaleX(F)V

    .line 2388091
    iget-object v1, v0, LX/GjZ;->b:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setScaleY(F)V

    .line 2388092
    :cond_1
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/16 v0, 0x2a

    const v1, -0x5907a8d9

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2388073
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2388074
    const-class v1, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    invoke-static {v1, p0}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2388075
    invoke-static {}, LX/8YM;->b()LX/8YM;

    move-result-object v1

    invoke-virtual {v1}, LX/8YH;->a()LX/8YK;

    move-result-object v1

    const/4 v2, 0x1

    .line 2388076
    iput-boolean v2, v1, LX/8YK;->c:Z

    .line 2388077
    move-object v1, v1

    .line 2388078
    new-instance v2, LX/8YL;

    const-wide v4, 0x4066800000000000L    # 180.0

    const-wide/high16 v6, 0x403b000000000000L    # 27.0

    invoke-direct {v2, v4, v5, v6, v7}, LX/8YL;-><init>(DD)V

    invoke-virtual {v1, v2}, LX/8YK;->a(LX/8YL;)LX/8YK;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->w:LX/8YK;

    .line 2388079
    iget-object v1, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->w:LX/8YK;

    new-instance v2, LX/Gjb;

    invoke-direct {v2, p0}, LX/Gjb;-><init>(Lcom/facebook/greetingcards/render/FoldingPopoverFragment;)V

    invoke-virtual {v1, v2}, LX/8YK;->a(LX/7hQ;)LX/8YK;

    .line 2388080
    const/16 v1, 0x2b

    const v2, 0x6582a13d

    invoke-static {v8, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x7ba18e4c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2388060
    const v0, 0x7f030681

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/render/DraggableView;

    iput-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->n:Lcom/facebook/greetingcards/render/DraggableView;

    .line 2388061
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->n:Lcom/facebook/greetingcards/render/DraggableView;

    new-instance v2, LX/Gja;

    invoke-direct {v2, p0}, LX/Gja;-><init>(Lcom/facebook/greetingcards/render/FoldingPopoverFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/greetingcards/render/DraggableView;->setDragListener(LX/39D;)V

    .line 2388062
    new-instance v0, LX/Gjt;

    .line 2388063
    iget-object v2, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v2, v2

    .line 2388064
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-direct {v0, v2}, LX/Gjt;-><init>(Landroid/view/Window;)V

    iput-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->s:LX/Gjt;

    .line 2388065
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->s:LX/Gjt;

    invoke-virtual {v0}, LX/Gjt;->a()V

    .line 2388066
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->v:LX/GjZ;

    if-eqz v0, :cond_0

    .line 2388067
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->n:Lcom/facebook/greetingcards/render/DraggableView;

    iget-object v2, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->v:LX/GjZ;

    invoke-virtual {v2}, LX/GjZ;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v0, v2}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2388068
    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->o:Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;

    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->a(Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;)V

    .line 2388069
    new-instance v0, LX/GjY;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/GjY;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->p:LX/GjY;

    .line 2388070
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->r:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->q:Landroid/graphics/Rect;

    if-eqz v0, :cond_1

    .line 2388071
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->n:Lcom/facebook/greetingcards/render/DraggableView;

    new-instance v2, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$1;

    invoke-direct {v2, p0}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$1;-><init>(Lcom/facebook/greetingcards/render/FoldingPopoverFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/greetingcards/render/DraggableView;->post(Ljava/lang/Runnable;)Z

    .line 2388072
    :cond_1
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->n:Lcom/facebook/greetingcards/render/DraggableView;

    const/16 v2, 0x2b

    const v3, 0x2c274f18

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x1df24c3e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2388051
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroy()V

    .line 2388052
    iget-object v1, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->v:LX/GjZ;

    if-eqz v1, :cond_0

    .line 2388053
    iget-object v1, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->v:LX/GjZ;

    .line 2388054
    iget-object v2, v1, LX/GjZ;->c:Landroid/view/Window;

    if-eqz v2, :cond_0

    .line 2388055
    iget-object v2, v1, LX/GjZ;->c:Landroid/view/Window;

    iget-object v3, v1, LX/GjZ;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2388056
    :cond_0
    iget-object v1, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->y:LX/0Zb;

    const-string v2, "greeting_card_dismissed"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2388057
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2388058
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2388059
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x1fc27f98

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2db645e3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2388044
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onResume()V

    .line 2388045
    iget-boolean v1, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->A:Z

    if-eqz v1, :cond_0

    .line 2388046
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2388047
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->A:Z

    .line 2388048
    :cond_0
    iget-object v1, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->v:LX/GjZ;

    if-eqz v1, :cond_1

    .line 2388049
    iget-object v1, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->v:LX/GjZ;

    invoke-virtual {v1}, LX/GjZ;->c()V

    .line 2388050
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x56f48480

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2388040
    iget-boolean v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->u:Z

    if-eqz v0, :cond_0

    .line 2388041
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2388042
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2388043
    return-void
.end method
