.class public final Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5eacbf8f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2388383
    const-class v0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2388386
    const-class v0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2388384
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2388385
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2388378
    iget-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;->e:Ljava/lang/String;

    .line 2388379
    iget-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2388369
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2388370
    invoke-direct {p0}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2388371
    invoke-virtual {p0}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2388372
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2388373
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2388374
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2388375
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2388376
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2388380
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2388381
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2388382
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2388377
    invoke-direct {p0}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2388366
    new-instance v0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;

    invoke-direct {v0}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;-><init>()V

    .line 2388367
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2388368
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2388365
    const v0, -0x71a42eb9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2388364
    const v0, -0x26468f13

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2388362
    iget-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;->f:Ljava/lang/String;

    .line 2388363
    iget-object v0, p0, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;->f:Ljava/lang/String;

    return-object v0
.end method
