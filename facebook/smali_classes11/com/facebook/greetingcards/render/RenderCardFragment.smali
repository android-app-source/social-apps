.class public Lcom/facebook/greetingcards/render/RenderCardFragment;
.super Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;
.source ""


# static fields
.field public static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/greetingcards/model/GreetingCard;

.field public b:Ljava/util/concurrent/Executor;

.field public d:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

.field public e:LX/Gju;

.field public f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/03V;

.field private h:Landroid/widget/ProgressBar;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field public k:Lcom/facebook/greetingcards/verve/render/VerveContentView;

.field private l:Landroid/widget/Button;

.field public m:LX/GjR;

.field public n:LX/CEO;

.field public o:LX/Gk2;

.field public p:Z

.field private q:I

.field public r:LX/0Zb;

.field public s:LX/0gh;

.field private t:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public u:LX/Gjp;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2387575
    const-class v0, Lcom/facebook/greetingcards/render/RenderCardFragment;

    sput-object v0, Lcom/facebook/greetingcards/render/RenderCardFragment;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2387571
    invoke-direct {p0}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;-><init>()V

    .line 2387572
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->p:Z

    .line 2387573
    sget-object v0, LX/Gjp;->NO_GREETING_CARD:LX/Gjp;

    iput-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->u:LX/Gjp;

    .line 2387574
    return-void
.end method

.method private static a(Lcom/facebook/greetingcards/model/GreetingCard$Slide;)Ljava/util/HashMap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/greetingcards/model/GreetingCard$Slide;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2387685
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 2387686
    iget-object v3, p0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/CardPhoto;

    .line 2387687
    sget-object v5, LX/5QD;->REMOTE:LX/5QD;

    iget-object v6, v0, Lcom/facebook/greetingcards/model/CardPhoto;->b:LX/5QD;

    invoke-virtual {v5, v6}, LX/5QD;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    sget-object v5, LX/5QD;->LOCAL_UPLOADED:LX/5QD;

    iget-object v6, v0, Lcom/facebook/greetingcards/model/CardPhoto;->b:LX/5QD;

    invoke-virtual {v5, v6}, LX/5QD;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2387688
    :cond_0
    iget-object v5, v0, Lcom/facebook/greetingcards/model/CardPhoto;->a:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v0, v0, Lcom/facebook/greetingcards/model/CardPhoto;->c:Ljava/lang/String;

    invoke-virtual {v2, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2387689
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2387690
    :cond_2
    return-object v2
.end method

.method public static a$redex0(Lcom/facebook/greetingcards/render/RenderCardFragment;LX/Gjp;)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2387671
    iget-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->u:LX/Gjp;

    if-ne p1, v0, :cond_0

    .line 2387672
    :goto_0
    return-void

    .line 2387673
    :cond_0
    iput-object p1, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->u:LX/Gjp;

    .line 2387674
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2387675
    const-string v3, "args_mode"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->u:LX/Gjp;

    sget-object v3, LX/Gjp;->LOAD_COMPLETE:LX/Gjp;

    if-ne v0, v3, :cond_2

    const/4 v0, 0x1

    .line 2387676
    :goto_1
    iget-object v3, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->l:Landroid/widget/Button;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 2387677
    iget-object v3, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->j:Landroid/view/View;

    iget-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->u:LX/Gjp;

    sget-object v4, LX/Gjp;->LOAD_COMPLETE:LX/Gjp;

    if-ne v0, v4, :cond_4

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2387678
    iget-object v3, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->h:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->u:LX/Gjp;

    sget-object v4, LX/Gjp;->LOADING:LX/Gjp;

    if-eq v0, v4, :cond_1

    iget-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->u:LX/Gjp;

    sget-object v4, LX/Gjp;->NO_GREETING_CARD:LX/Gjp;

    if-ne v0, v4, :cond_5

    :cond_1
    move v0, v1

    :goto_4
    invoke-virtual {v3, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2387679
    iget-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->i:Landroid/view/View;

    iget-object v3, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->u:LX/Gjp;

    sget-object v4, LX/Gjp;->ERROR:LX/Gjp;

    if-ne v3, v4, :cond_6

    :goto_5
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2387680
    goto :goto_1

    :cond_3
    move v0, v2

    .line 2387681
    goto :goto_2

    :cond_4
    move v0, v2

    .line 2387682
    goto :goto_3

    :cond_5
    move v0, v2

    .line 2387683
    goto :goto_4

    :cond_6
    move v1, v2

    .line 2387684
    goto :goto_5
.end method

.method public static a$redex0(Lcom/facebook/greetingcards/render/RenderCardFragment;LX/Gk2;Lcom/facebook/greetingcards/model/GreetingCard;)V
    .locals 12

    .prologue
    const/high16 v11, 0x3f000000    # 0.5f

    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 2387642
    sget-object v0, LX/Gjp;->LOAD_COMPLETE:LX/Gjp;

    invoke-static {p0, v0}, Lcom/facebook/greetingcards/render/RenderCardFragment;->a$redex0(Lcom/facebook/greetingcards/render/RenderCardFragment;LX/Gjp;)V

    .line 2387643
    iput-object p1, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->o:LX/Gk2;

    .line 2387644
    invoke-direct {p0}, Lcom/facebook/greetingcards/render/RenderCardFragment;->l()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 2387645
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2387646
    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 2387647
    int-to-float v4, v2

    iget-object v0, p1, LX/Gk2;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMDeck;->size:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    div-float/2addr v4, v0

    .line 2387648
    int-to-float v5, v3

    iget-object v0, p1, LX/Gk2;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMDeck;->size:LX/0Px;

    invoke-virtual {v0, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    div-float/2addr v5, v0

    .line 2387649
    sub-float v0, v4, v5

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v6, v0

    const-wide v8, 0x3f847ae147ae147bL    # 0.01

    cmpg-double v0, v6, v8

    if-gez v0, :cond_0

    .line 2387650
    iget-object v0, p1, LX/Gk2;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMDeck;->size:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    mul-float v3, v0, v4

    .line 2387651
    iget-object v0, p1, LX/Gk2;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMDeck;->size:LX/0Px;

    invoke-virtual {v0, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    mul-float v4, v0, v5

    .line 2387652
    :goto_0
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v6

    .line 2387653
    iget-object v0, p2, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    invoke-static {v0}, Lcom/facebook/greetingcards/render/RenderCardFragment;->a(Lcom/facebook/greetingcards/model/GreetingCard$Slide;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 2387654
    iget-object v2, p2, Lcom/facebook/greetingcards/model/GreetingCard;->b:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    :goto_1
    if-ge v1, v5, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    .line 2387655
    invoke-static {v0}, Lcom/facebook/greetingcards/render/RenderCardFragment;->a(Lcom/facebook/greetingcards/model/GreetingCard$Slide;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 2387656
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2387657
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0b22ea

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2387658
    mul-int/lit8 v4, v0, 0x2

    sub-int/2addr v2, v4

    .line 2387659
    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v3, v0

    .line 2387660
    int-to-float v2, v2

    iget-object v0, p1, LX/Gk2;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMDeck;->size:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    div-float/2addr v2, v0

    .line 2387661
    int-to-float v3, v3

    iget-object v0, p1, LX/Gk2;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMDeck;->size:LX/0Px;

    invoke-virtual {v0, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    div-float v0, v3, v0

    .line 2387662
    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 2387663
    iget-object v0, p1, LX/Gk2;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMDeck;->size:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    mul-float v3, v0, v2

    .line 2387664
    iget-object v0, p1, LX/Gk2;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMDeck;->size:LX/0Px;

    invoke-virtual {v0, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    mul-float v4, v0, v2

    .line 2387665
    iget-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->j:Landroid/view/View;

    const v2, 0x7f020cb3

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 2387666
    :cond_1
    iget-object v0, p2, Lcom/facebook/greetingcards/model/GreetingCard;->c:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    invoke-static {v0}, Lcom/facebook/greetingcards/render/RenderCardFragment;->a(Lcom/facebook/greetingcards/model/GreetingCard$Slide;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 2387667
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    add-float v1, v3, v11

    float-to-int v1, v1

    add-float v2, v4, v11

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2387668
    iget-object v1, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->k:Lcom/facebook/greetingcards/verve/render/VerveContentView;

    invoke-virtual {v1, v0}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2387669
    iget-object v7, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->k:Lcom/facebook/greetingcards/verve/render/VerveContentView;

    iget-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->e:LX/Gju;

    iget-object v1, p1, LX/Gk2;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    iget-object v5, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->t:LX/0Px;

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, LX/Gju;->a(Lcom/facebook/greetingcards/verve/model/VMDeck;Lcom/facebook/greetingcards/model/GreetingCard;FFLX/0Px;)Lcom/facebook/greetingcards/verve/model/VMDeck;

    move-result-object v0

    new-instance v1, Lcom/facebook/greetingcards/render/GreetingCardVerveViewSupplier;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v6}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->f:LX/0Or;

    invoke-direct {v1, p1, v2, v3, v4}, Lcom/facebook/greetingcards/render/GreetingCardVerveViewSupplier;-><init>(LX/Gk2;Landroid/content/res/Resources;LX/0P1;LX/0Or;)V

    iget-object v2, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->n:LX/CEO;

    new-instance v3, LX/Gjo;

    invoke-direct {v3, p0}, LX/Gjo;-><init>(Lcom/facebook/greetingcards/render/RenderCardFragment;)V

    invoke-virtual {v7, v0, v1, v2, v3}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->a(Lcom/facebook/greetingcards/verve/model/VMDeck;LX/CEl;LX/CEO;LX/CEU;)V

    .line 2387670
    return-void
.end method

.method public static b(Lcom/facebook/greetingcards/render/RenderCardFragment;Lcom/facebook/greetingcards/model/GreetingCard;)V
    .locals 14

    .prologue
    .line 2387633
    sget-object v0, LX/Gjp;->LOADING:LX/Gjp;

    invoke-static {p0, v0}, Lcom/facebook/greetingcards/render/RenderCardFragment;->a$redex0(Lcom/facebook/greetingcards/render/RenderCardFragment;LX/Gjp;)V

    .line 2387634
    invoke-direct {p0}, Lcom/facebook/greetingcards/render/RenderCardFragment;->l()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 2387635
    iget-object v1, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->d:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    iget-object v2, p1, Lcom/facebook/greetingcards/model/GreetingCard;->d:Ljava/lang/String;

    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v4

    .line 2387636
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v12

    .line 2387637
    sget-object v10, LX/0zS;->a:LX/0zS;

    move-object v5, v1

    move-object v6, v2

    move v7, v3

    move v8, v0

    move-object v9, v4

    invoke-static/range {v5 .. v10}, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->a$redex0(Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;Ljava/lang/String;IILX/0wC;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v13

    .line 2387638
    new-instance v5, LX/Gjv;

    move-object v6, v1

    move-object v7, v12

    move-object v8, v2

    move v9, v3

    move v10, v0

    move-object v11, v4

    invoke-direct/range {v5 .. v11}, LX/Gjv;-><init>(Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/String;IILX/0wC;)V

    iget-object v6, v1, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->e:LX/0TD;

    invoke-static {v13, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2387639
    move-object v0, v12

    .line 2387640
    new-instance v1, LX/Gjn;

    invoke-direct {v1, p0, p1}, LX/Gjn;-><init>(Lcom/facebook/greetingcards/render/RenderCardFragment;Lcom/facebook/greetingcards/model/GreetingCard;)V

    iget-object v2, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2387641
    return-void
.end method

.method private l()Landroid/util/DisplayMetrics;
    .locals 3

    .prologue
    .line 2387628
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 2387629
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    .line 2387630
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 2387631
    :goto_0
    return-object v0

    .line 2387632
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2387627
    const-string v0, "tinsel_view_card"

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 2387620
    invoke-super {p0, p1}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;->a(Landroid/os/Bundle;)V

    .line 2387621
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/greetingcards/render/RenderCardFragment;

    invoke-static {v0}, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->a(LX/0QB;)Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    move-result-object v3

    check-cast v3, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    const/16 v5, 0xf2f

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    new-instance v7, LX/Gju;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-direct {v7, v6}, LX/Gju;-><init>(Landroid/content/res/Resources;)V

    move-object v6, v7

    check-cast v6, LX/Gju;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v8

    check-cast v8, LX/0Zb;

    invoke-static {v0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v0

    check-cast v0, LX/0gh;

    iput-object v3, v2, Lcom/facebook/greetingcards/render/RenderCardFragment;->d:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    iput-object v4, v2, Lcom/facebook/greetingcards/render/RenderCardFragment;->b:Ljava/util/concurrent/Executor;

    iput-object v5, v2, Lcom/facebook/greetingcards/render/RenderCardFragment;->f:LX/0Or;

    iput-object v6, v2, Lcom/facebook/greetingcards/render/RenderCardFragment;->e:LX/Gju;

    iput-object v7, v2, Lcom/facebook/greetingcards/render/RenderCardFragment;->g:LX/03V;

    iput-object v8, v2, Lcom/facebook/greetingcards/render/RenderCardFragment;->r:LX/0Zb;

    iput-object v0, v2, Lcom/facebook/greetingcards/render/RenderCardFragment;->s:LX/0gh;

    .line 2387622
    if-eqz p1, :cond_0

    .line 2387623
    const-string v0, "extra_greeting_card"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/GreetingCard;

    .line 2387624
    if-eqz v0, :cond_0

    .line 2387625
    iput-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    .line 2387626
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/greetingcards/model/GreetingCard;)V
    .locals 1

    .prologue
    .line 2387691
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2387692
    iget-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    if-eq v0, p1, :cond_0

    .line 2387693
    iput-object p1, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    .line 2387694
    iget-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->k:Lcom/facebook/greetingcards/verve/render/VerveContentView;

    if-eqz v0, :cond_0

    .line 2387695
    iget-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    invoke-static {p0, v0}, Lcom/facebook/greetingcards/render/RenderCardFragment;->b(Lcom/facebook/greetingcards/render/RenderCardFragment;Lcom/facebook/greetingcards/model/GreetingCard;)V

    .line 2387696
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 2387619
    const/4 v0, 0x0

    return v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x624129ee

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2387616
    if-nez p2, :cond_0

    .line 2387617
    const/4 v0, 0x0

    const/16 v2, 0x2b

    const v3, -0x6016f878

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2387618
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f030874

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v2, 0x7b43026b

    invoke-static {v2, v1}, LX/02F;->f(II)V

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x8355b6c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2387613
    iget-object v1, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->s:LX/0gh;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/render/RenderCardFragment;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0gh;->d(Ljava/lang/String;)V

    .line 2387614
    invoke-super {p0}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;->onDestroyView()V

    .line 2387615
    const/16 v1, 0x2b

    const v2, -0x12826128

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6187c426

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2387610
    invoke-super {p0}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;->onPause()V

    .line 2387611
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget v2, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->q:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    .line 2387612
    const/16 v1, 0x2b

    const v2, 0x1ef86a90

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x12989544

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2387606
    invoke-super {p0}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;->onResume()V

    .line 2387607
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getRequestedOrientation()I

    move-result v1

    iput v1, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->q:I

    .line 2387608
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    .line 2387609
    const/16 v1, 0x2b

    const v2, 0x4df1477a    # 5.05999168E8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2387603
    invoke-super {p0, p1}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2387604
    const-string v0, "extra_greeting_card"

    iget-object v1, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2387605
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2387576
    invoke-super {p0, p1, p2}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2387577
    const v0, 0x7f0d1607

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->h:Landroid/widget/ProgressBar;

    .line 2387578
    const v0, 0x7f0d1608

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->i:Landroid/view/View;

    .line 2387579
    const v0, 0x7f0d1609

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->j:Landroid/view/View;

    .line 2387580
    const v0, 0x7f0d160a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/render/VerveContentView;

    iput-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->k:Lcom/facebook/greetingcards/verve/render/VerveContentView;

    .line 2387581
    const v0, 0x7f0d160b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->l:Landroid/widget/Button;

    .line 2387582
    iget-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->l:Landroid/widget/Button;

    new-instance v1, LX/Gjm;

    invoke-direct {v1, p0}, LX/Gjm;-><init>(Lcom/facebook/greetingcards/render/RenderCardFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2387583
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2387584
    const-string v1, "args_button"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2387585
    if-eqz v0, :cond_0

    .line 2387586
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->t:LX/0Px;

    .line 2387587
    :cond_0
    const/4 v0, 0x0

    .line 2387588
    iget-object v1, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    if-eqz v1, :cond_1

    .line 2387589
    iget-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->o:LX/Gk2;

    if-eqz v0, :cond_2

    .line 2387590
    iget-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->o:LX/Gk2;

    iget-object v1, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    invoke-static {p0, v0, v1}, Lcom/facebook/greetingcards/render/RenderCardFragment;->a$redex0(Lcom/facebook/greetingcards/render/RenderCardFragment;LX/Gk2;Lcom/facebook/greetingcards/model/GreetingCard;)V

    .line 2387591
    :goto_0
    iget-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    iget-object v0, v0, Lcom/facebook/greetingcards/model/GreetingCard;->f:Ljava/lang/String;

    .line 2387592
    :cond_1
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "greeting_card_opened"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "greeting_card_id"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "source"

    .line 2387593
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2387594
    const-string v3, "args_source"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    const-string v0, "feed"

    :goto_1
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2387595
    iget-object v1, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->r:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2387596
    iget-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->s:LX/0gh;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/render/RenderCardFragment;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/0gh;->a(Ljava/lang/String;Z)V

    .line 2387597
    return-void

    .line 2387598
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->p:Z

    if-nez v0, :cond_3

    .line 2387599
    iget-object v0, p0, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    invoke-static {p0, v0}, Lcom/facebook/greetingcards/render/RenderCardFragment;->b(Lcom/facebook/greetingcards/render/RenderCardFragment;Lcom/facebook/greetingcards/model/GreetingCard;)V

    goto :goto_0

    .line 2387600
    :cond_3
    sget-object v0, LX/Gjp;->LOADING:LX/Gjp;

    invoke-static {p0, v0}, Lcom/facebook/greetingcards/render/RenderCardFragment;->a$redex0(Lcom/facebook/greetingcards/render/RenderCardFragment;LX/Gjp;)V

    goto :goto_0

    .line 2387601
    :cond_4
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2387602
    const-string v3, "args_source"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
