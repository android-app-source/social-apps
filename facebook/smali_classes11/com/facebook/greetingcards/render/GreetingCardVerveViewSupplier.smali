.class public Lcom/facebook/greetingcards/render/GreetingCardVerveViewSupplier;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/CEl;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/Gk2;

.field private final c:Landroid/content/res/Resources;

.field public final d:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2388531
    const-class v0, Lcom/facebook/greetingcards/render/GreetingCardVerveViewSupplier;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/greetingcards/render/GreetingCardVerveViewSupplier;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/Gk2;Landroid/content/res/Resources;LX/0P1;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Gk2;",
            "Landroid/content/res/Resources;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2388525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2388526
    iput-object p1, p0, Lcom/facebook/greetingcards/render/GreetingCardVerveViewSupplier;->b:LX/Gk2;

    .line 2388527
    iput-object p2, p0, Lcom/facebook/greetingcards/render/GreetingCardVerveViewSupplier;->c:Landroid/content/res/Resources;

    .line 2388528
    iput-object p3, p0, Lcom/facebook/greetingcards/render/GreetingCardVerveViewSupplier;->d:LX/0P1;

    .line 2388529
    iput-object p4, p0, Lcom/facebook/greetingcards/render/GreetingCardVerveViewSupplier;->e:LX/0Or;

    .line 2388530
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ":",
            "LX/CEj;",
            ">()",
            "LX/0Rf",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2388523
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2388524
    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/greetingcards/verve/model/VMView;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 2388522
    new-instance v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-direct {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/greetingcards/verve/model/VMView;)Ljava/lang/Enum;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ":",
            "LX/CEj;",
            ">(",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ")TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2388521
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/greetingcards/verve/model/VMView;LX/CEe;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            "LX/CEe;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2388520
    return-void
.end method

.method public final a(Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMSlide;LX/0P1;Landroid/graphics/drawable/Drawable;Landroid/widget/ImageView;)V
    .locals 5
    .param p4    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            "Lcom/facebook/greetingcards/verve/model/VMSlide;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/graphics/drawable/Drawable;",
            "Landroid/widget/ImageView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2388498
    iget-object v1, p1, Lcom/facebook/greetingcards/verve/model/VMView;->src:Ljava/lang/String;

    .line 2388499
    invoke-virtual {p3, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2388500
    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/facebook/greetingcards/render/GreetingCardVerveViewSupplier;->b:LX/Gk2;

    iget-object v2, v2, LX/Gk2;->b:LX/0P1;

    invoke-virtual {v2, v0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2388501
    check-cast p5, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2388502
    iget-object v1, p0, Lcom/facebook/greetingcards/render/GreetingCardVerveViewSupplier;->b:LX/Gk2;

    iget-object v1, v1, LX/Gk2;->b:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    sget-object v1, Lcom/facebook/greetingcards/render/GreetingCardVerveViewSupplier;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p5, v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2388503
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p5

    .line 2388504
    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2388505
    new-instance v2, LX/1Uo;

    iget-object v3, p0, Lcom/facebook/greetingcards/render/GreetingCardVerveViewSupplier;->c:Landroid/content/res/Resources;

    invoke-direct {v2, v3}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 2388506
    if-eqz p4, :cond_2

    .line 2388507
    iput-object p4, v2, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2388508
    :cond_2
    if-eqz v1, :cond_3

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->isAbsolute()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2388509
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/facebook/greetingcards/render/GreetingCardVerveViewSupplier;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2388510
    iget-object v3, p1, Lcom/facebook/greetingcards/verve/model/VMView;->b:Landroid/graphics/PointF;

    if-eqz v3, :cond_3

    .line 2388511
    iget-object v3, p1, Lcom/facebook/greetingcards/verve/model/VMView;->b:Landroid/graphics/PointF;

    .line 2388512
    iput-object v3, v2, LX/1Uo;->p:Landroid/graphics/PointF;

    .line 2388513
    sget-object v3, LX/1Up;->h:LX/1Up;

    invoke-virtual {v2, v3}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    .line 2388514
    :cond_3
    const-string v3, "Back"

    iget-object v4, p2, Lcom/facebook/greetingcards/verve/model/VMSlide;->className:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "media1"

    iget-object v4, p1, Lcom/facebook/greetingcards/verve/model/VMView;->valueName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2388515
    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v3

    .line 2388516
    iput-object v3, v2, LX/1Uo;->u:LX/4Ab;

    .line 2388517
    :cond_4
    invoke-virtual {v2}, LX/1Uo;->u()LX/1af;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2388518
    iget-object v2, p0, Lcom/facebook/greetingcards/render/GreetingCardVerveViewSupplier;->d:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2388519
    new-instance v2, LX/Gjl;

    invoke-direct {v2, p0, v1, p5}, LX/Gjl;-><init>(Lcom/facebook/greetingcards/render/GreetingCardVerveViewSupplier;Ljava/lang/String;Landroid/widget/ImageView;)V

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lcom/facebook/greetingcards/verve/model/VMView;)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ":",
            "LX/CEe;",
            ">(",
            "Landroid/content/Context;",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 2388473
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(Landroid/content/Context;Lcom/facebook/greetingcards/verve/model/VMView;)Landroid/widget/Button;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2388474
    new-instance v0, Landroid/widget/Button;

    const/4 v1, 0x0

    .line 2388475
    const-string v2, "small"

    iget-object p0, p2, Lcom/facebook/greetingcards/verve/model/VMView;->buttonSize:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2388476
    const-string v2, "special"

    iget-object p0, p2, Lcom/facebook/greetingcards/verve/model/VMView;->buttonStyle:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2388477
    const v2, 0x7f0101ef

    .line 2388478
    :goto_0
    move v2, v2

    .line 2388479
    invoke-direct {v0, p1, v1, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2388480
    iget-object v1, p2, Lcom/facebook/greetingcards/verve/model/VMView;->title:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2388481
    iget-object v1, p2, Lcom/facebook/greetingcards/verve/model/VMView;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2388482
    :cond_0
    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/Button;->setPadding(IIII)V

    .line 2388483
    return-object v0

    .line 2388484
    :cond_1
    const-string v2, "primary"

    iget-object p0, p2, Lcom/facebook/greetingcards/verve/model/VMView;->buttonStyle:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2388485
    const v2, 0x7f0101e9

    goto :goto_0

    .line 2388486
    :cond_2
    const v2, 0x7f0101e3

    goto :goto_0

    .line 2388487
    :cond_3
    const-string v2, "medium"

    iget-object p0, p2, Lcom/facebook/greetingcards/verve/model/VMView;->buttonSize:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2388488
    const-string v2, "special"

    iget-object p0, p2, Lcom/facebook/greetingcards/verve/model/VMView;->buttonStyle:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2388489
    const v2, 0x7f0101f0

    goto :goto_0

    .line 2388490
    :cond_4
    const-string v2, "primary"

    iget-object p0, p2, Lcom/facebook/greetingcards/verve/model/VMView;->buttonStyle:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2388491
    const v2, 0x7f0101ea

    goto :goto_0

    .line 2388492
    :cond_5
    const v2, 0x7f0101e4

    goto :goto_0

    .line 2388493
    :cond_6
    const-string v2, "special"

    iget-object p0, p2, Lcom/facebook/greetingcards/verve/model/VMView;->buttonStyle:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2388494
    const v2, 0x7f0101f1

    goto :goto_0

    .line 2388495
    :cond_7
    const-string v2, "primary"

    iget-object p0, p2, Lcom/facebook/greetingcards/verve/model/VMView;->buttonStyle:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2388496
    const v2, 0x7f0101eb

    goto :goto_0

    .line 2388497
    :cond_8
    const v2, 0x7f0101e5

    goto :goto_0
.end method
