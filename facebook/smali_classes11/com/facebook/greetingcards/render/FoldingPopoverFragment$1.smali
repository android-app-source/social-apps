.class public final Lcom/facebook/greetingcards/render/FoldingPopoverFragment$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/greetingcards/render/FoldingPopoverFragment;)V
    .locals 0

    .prologue
    .line 2387843
    iput-object p1, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$1;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2387844
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$1;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v0, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->n:Lcom/facebook/greetingcards/render/DraggableView;

    invoke-static {v0}, LX/1r0;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 2387845
    new-instance v1, Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$1;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2387846
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v3, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$1;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v3, v3, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->q:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$1;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v4, v4, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->q:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    const/16 v5, 0x30

    invoke-direct {v2, v3, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 2387847
    iget-object v3, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$1;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v3, v3, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$1;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v4, v4, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->q:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int v0, v4, v0

    invoke-virtual {v2, v3, v0, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 2387848
    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2387849
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$1;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$1;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v3, v3, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->r:Landroid/graphics/Bitmap;

    invoke-direct {v0, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-static {v1, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2387850
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$1;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v0, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->n:Lcom/facebook/greetingcards/render/DraggableView;

    iget-object v2, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$1;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v2, v2, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->p:LX/GjY;

    invoke-virtual {v0, v2}, Lcom/facebook/greetingcards/render/DraggableView;->addView(Landroid/view/View;)V

    .line 2387851
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$1;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v0, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->p:LX/GjY;

    iget-object v2, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$1;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v2, v2, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->n:Lcom/facebook/greetingcards/render/DraggableView;

    const v3, 0x7f0d11db

    invoke-virtual {v2, v3}, Lcom/facebook/greetingcards/render/DraggableView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 2387852
    iput-object v1, v0, LX/GjY;->b:Landroid/view/View;

    .line 2387853
    iget-object v3, v0, LX/GjY;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, LX/GjY;->addView(Landroid/view/View;)V

    .line 2387854
    iput-object v2, v0, LX/GjY;->c:Landroid/view/View;

    .line 2387855
    iget-object v3, v0, LX/GjY;->c:Landroid/view/View;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2387856
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2387857
    iget-object v0, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$1;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v0, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->p:LX/GjY;

    new-instance v1, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$1$1;

    invoke-direct {v1, p0}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment$1$1;-><init>(Lcom/facebook/greetingcards/render/FoldingPopoverFragment$1;)V

    invoke-virtual {v0, v1}, LX/GjY;->post(Ljava/lang/Runnable;)Z

    .line 2387858
    return-void
.end method
