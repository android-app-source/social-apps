.class public Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;


# instance fields
.field private final b:LX/Gk1;

.field public final c:LX/Gk0;

.field private final d:LX/0tX;

.field public final e:LX/0TD;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2388649
    const-class v0, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    sput-object v0, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/Gk1;LX/Gk0;LX/0tX;LX/0TD;)V
    .locals 0
    .param p4    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2388650
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2388651
    iput-object p1, p0, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->b:LX/Gk1;

    .line 2388652
    iput-object p2, p0, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->c:LX/Gk0;

    .line 2388653
    iput-object p3, p0, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->d:LX/0tX;

    .line 2388654
    iput-object p4, p0, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->e:LX/0TD;

    .line 2388655
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;
    .locals 7

    .prologue
    .line 2388656
    sget-object v0, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->f:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    if-nez v0, :cond_1

    .line 2388657
    const-class v1, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    monitor-enter v1

    .line 2388658
    :try_start_0
    sget-object v0, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->f:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2388659
    if-eqz v2, :cond_0

    .line 2388660
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2388661
    new-instance p0, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    invoke-static {v0}, LX/Gk1;->a(LX/0QB;)LX/Gk1;

    move-result-object v3

    check-cast v3, LX/Gk1;

    invoke-static {v0}, LX/Gk0;->a(LX/0QB;)LX/Gk0;

    move-result-object v4

    check-cast v4, LX/Gk0;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, LX/0TD;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;-><init>(LX/Gk1;LX/Gk0;LX/0tX;LX/0TD;)V

    .line 2388662
    move-object v0, p0

    .line 2388663
    sput-object v0, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->f:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2388664
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2388665
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2388666
    :cond_1
    sget-object v0, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->f:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    return-object v0

    .line 2388667
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2388668
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;Landroid/net/Uri;)Lcom/facebook/greetingcards/verve/model/VMDeck;
    .locals 4

    .prologue
    .line 2388669
    const-string v0, "FTE.fetchTemplateDataInternal"

    const v1, -0x3b287b56

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2388670
    :try_start_0
    new-instance v1, LX/Gk3;

    invoke-direct {v1, p1}, LX/Gk3;-><init>(Landroid/net/Uri;)V

    .line 2388671
    iget-object v0, p0, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->c:LX/Gk0;

    invoke-virtual {v0, v1}, LX/2Vx;->b(LX/2WG;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMDeck;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2388672
    if-eqz v0, :cond_0

    .line 2388673
    const v1, 0x4fe3913e

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_0
    return-object v0

    .line 2388674
    :cond_0
    :try_start_1
    new-instance v0, LX/34X;

    new-instance v2, LX/Gjy;

    invoke-direct {v2, p0, v1}, LX/Gjy;-><init>(Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;LX/Gk3;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-direct {v0, p1, v2, v1}, LX/34X;-><init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2388675
    :try_start_2
    iget-object v1, p0, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->b:LX/Gk1;

    invoke-virtual {v1, v0}, LX/3AP;->a(LX/34X;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMDeck;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2388676
    const v1, 0x35d1cf55

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 2388677
    :catch_0
    move-exception v0

    .line 2388678
    :try_start_3
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2388679
    :catchall_0
    move-exception v0

    const v1, -0x3a9c684

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;)LX/Gk2;
    .locals 7

    .prologue
    .line 2388680
    const-string v0, "FTE.fetchTemplateDataAsTemplateResult"

    const v1, -0x7e06f370

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2388681
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->j()Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel$TemplateDataModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->a(Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;Landroid/net/Uri;)Lcom/facebook/greetingcards/verve/model/VMDeck;

    move-result-object v0

    .line 2388682
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 2388683
    invoke-virtual {p1}, Lcom/facebook/greetingcards/render/GreetingCardTemplatesGraphqlModels$GreetingCardTemplateQueryModel;->k()LX/2uF;

    move-result-object v2

    invoke-virtual {v2}, LX/3Sa;->e()LX/3Sh;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, LX/2sN;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2388684
    invoke-interface {v2}, LX/2sN;->b()LX/1vs;

    move-result-object v3

    .line 2388685
    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2388686
    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v4, v3, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2388687
    :catchall_0
    move-exception v0

    const v1, -0x3b92fe2a

    invoke-static {v1}, LX/02m;->a(I)V

    .line 2388688
    sget-object v1, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->a:Ljava/lang/Class;

    invoke-static {v1}, LX/0PR;->a(Ljava/lang/Class;)V

    throw v0

    .line 2388689
    :cond_0
    :try_start_1
    new-instance v2, LX/Gk2;

    invoke-static {v1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v1

    invoke-direct {v2, v0, v1}, LX/Gk2;-><init>(Lcom/facebook/greetingcards/verve/model/VMDeck;LX/0P1;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2388690
    const v0, -0x68e63859

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2388691
    sget-object v0, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->a:Ljava/lang/Class;

    invoke-static {v0}, LX/0PR;->a(Ljava/lang/Class;)V

    return-object v2
.end method

.method public static a$redex0(Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;Ljava/lang/String;IILX/0wC;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "LX/0wC;",
            "LX/0zS;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Gk2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2388692
    new-instance v0, LX/Gjf;

    invoke-direct {v0}, LX/Gjf;-><init>()V

    move-object v0, v0

    .line 2388693
    const-string v1, "rootID"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "width"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "height"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "scale"

    invoke-virtual {v1, v2, p4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2388694
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/16 v2, 0xe10

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 2388695
    iget-object v1, p0, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->d:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2388696
    new-instance v1, LX/Gjx;

    invoke-direct {v1, p0}, LX/Gjx;-><init>(Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;)V

    iget-object v2, p0, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->e:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
