.class public Lcom/facebook/greetingcards/render/DraggableView;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public a:LX/1wz;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2387794
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/greetingcards/render/DraggableView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2387795
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2387792
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/greetingcards/render/DraggableView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2387793
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2387789
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2387790
    invoke-direct {p0}, Lcom/facebook/greetingcards/render/DraggableView;->b()V

    .line 2387791
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/greetingcards/render/DraggableView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/greetingcards/render/DraggableView;

    invoke-static {v0}, LX/1wx;->b(LX/0QB;)LX/1wz;

    move-result-object v0

    check-cast v0, LX/1wz;

    iput-object v0, p0, Lcom/facebook/greetingcards/render/DraggableView;->a:LX/1wz;

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2387796
    const-class v0, Lcom/facebook/greetingcards/render/DraggableView;

    invoke-static {v0, p0}, Lcom/facebook/greetingcards/render/DraggableView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2387797
    iget-object v0, p0, Lcom/facebook/greetingcards/render/DraggableView;->a:LX/1wz;

    sget-object v1, LX/31M;->DOWN:LX/31M;

    invoke-virtual {v1}, LX/31M;->flag()I

    move-result v1

    sget-object v2, LX/31M;->UP:LX/31M;

    invoke-virtual {v2}, LX/31M;->flag()I

    move-result v2

    or-int/2addr v1, v2

    .line 2387798
    iput v1, v0, LX/1wz;->p:I

    .line 2387799
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2387787
    iget-object v0, p0, Lcom/facebook/greetingcards/render/DraggableView;->a:LX/1wz;

    invoke-virtual {v0}, LX/1wz;->c()V

    .line 2387788
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2387786
    iget-object v0, p0, Lcom/facebook/greetingcards/render/DraggableView;->a:LX/1wz;

    invoke-virtual {v0, p1}, LX/1wz;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0xc12e943

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2387785
    iget-object v1, p0, Lcom/facebook/greetingcards/render/DraggableView;->a:LX/1wz;

    invoke-virtual {v1, p1}, LX/1wz;->b(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, -0x2a8eaeac

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public setDragListener(LX/39D;)V
    .locals 1

    .prologue
    .line 2387782
    iget-object v0, p0, Lcom/facebook/greetingcards/render/DraggableView;->a:LX/1wz;

    .line 2387783
    iput-object p1, v0, LX/1wz;->r:LX/39D;

    .line 2387784
    return-void
.end method
