.class public Lcom/facebook/mobileconfig/ui/QEGKDefinitions_GroupDefSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GroupDef;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2415267
    const-class v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GroupDef;

    new-instance v1, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_GroupDefSerializer;

    invoke-direct {v1}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_GroupDefSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2415268
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2415273
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GroupDef;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2415274
    if-nez p0, :cond_0

    .line 2415275
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2415276
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2415277
    invoke-static {p0, p1, p2}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_GroupDefSerializer;->b(Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GroupDef;LX/0nX;LX/0my;)V

    .line 2415278
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2415279
    return-void
.end method

.method private static b(Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GroupDef;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2415270
    const-string v0, "name"

    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GroupDef;->name:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2415271
    const-string v0, "params"

    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GroupDef;->params:Ljava/util/List;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 2415272
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2415269
    check-cast p1, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GroupDef;

    invoke-static {p1, p2, p3}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_GroupDefSerializer;->a(Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GroupDef;LX/0nX;LX/0my;)V

    return-void
.end method
