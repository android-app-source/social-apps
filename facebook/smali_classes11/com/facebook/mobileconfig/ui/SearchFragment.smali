.class public Lcom/facebook/mobileconfig/ui/SearchFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ww;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/5eF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private e:LX/1OR;

.field private f:LX/H1X;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/H0v;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2415475
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2415476
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/mobileconfig/ui/SearchFragment;->g:Ljava/util/List;

    .line 2415477
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/mobileconfig/ui/SearchFragment;->h:Ljava/lang/CharSequence;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2415478
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2415479
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/mobileconfig/ui/SearchFragment;

    const-class v2, Landroid/content/Context;

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {v0}, LX/0Ww;->a(LX/0QB;)LX/0Ww;

    move-result-object p1

    check-cast p1, LX/0Ww;

    invoke-static {v0}, LX/5eG;->b(LX/0QB;)LX/5eF;

    move-result-object v0

    check-cast v0, LX/5eF;

    iput-object v2, p0, Lcom/facebook/mobileconfig/ui/SearchFragment;->a:Landroid/content/Context;

    iput-object p1, p0, Lcom/facebook/mobileconfig/ui/SearchFragment;->b:LX/0Ww;

    iput-object v0, p0, Lcom/facebook/mobileconfig/ui/SearchFragment;->c:LX/5eF;

    .line 2415480
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2415481
    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2415482
    iget-object v0, p0, Lcom/facebook/mobileconfig/ui/SearchFragment;->f:LX/H1X;

    .line 2415483
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, LX/H1X;->d:Ljava/lang/String;

    .line 2415484
    invoke-virtual {v0}, LX/H1X;->getFilter()Landroid/widget/Filter;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 2415485
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 22

    .prologue
    .line 2415486
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/mobileconfig/ui/SearchFragment;->g:Ljava/util/List;

    .line 2415487
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/mobileconfig/ui/SearchFragment;->a:Landroid/content/Context;

    check-cast v2, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    iget-object v7, v2, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->p:LX/4gq;

    .line 2415488
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/mobileconfig/ui/SearchFragment;->b:LX/0Ww;

    invoke-static {v2}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;->a(LX/0Wx;)Lcom/facebook/mobileconfig/ui/QEGKDefinitions;

    move-result-object v17

    .line 2415489
    new-instance v18, Ljava/util/HashMap;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashMap;-><init>()V

    .line 2415490
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;->gatekeepers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GatekeeperDef;

    .line 2415491
    new-instance v4, Landroid/util/Pair;

    iget-object v5, v2, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GatekeeperDef;->config:Ljava/lang/String;

    iget v6, v2, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GatekeeperDef;->key:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v2, v2, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GatekeeperDef;->name:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-interface {v0, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2415492
    :cond_0
    new-instance v19, Ljava/util/HashMap;

    invoke-direct/range {v19 .. v19}, Ljava/util/HashMap;-><init>()V

    .line 2415493
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/mobileconfig/ui/SearchFragment;->c:LX/5eF;

    iget-object v0, v2, LX/5eF;->a:LX/0Px;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, LX/0Px;->size()I

    move-result v21

    const/4 v2, 0x0

    move v8, v2

    :goto_1
    move/from16 v0, v21

    if-ge v8, v0, :cond_2

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4gs;

    .line 2415494
    iget-object v5, v2, LX/4gs;->a:Ljava/lang/String;

    .line 2415495
    iget-object v4, v2, LX/4gs;->b:Ljava/lang/String;

    .line 2415496
    iget v6, v2, LX/4gs;->c:I

    .line 2415497
    new-instance v3, Landroid/util/Pair;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-direct {v3, v5, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 2415498
    if-nez v12, :cond_1

    .line 2415499
    invoke-virtual {v2}, LX/4gs;->a()J

    move-result-wide v2

    invoke-static/range {v2 .. v7}, LX/H0w;->a(JLjava/lang/String;Ljava/lang/String;ILX/4gq;)LX/H0w;

    move-result-object v9

    .line 2415500
    :goto_2
    new-instance v2, Landroid/util/Pair;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v2, v5, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v0, v19

    invoke-interface {v0, v2, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2415501
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/mobileconfig/ui/SearchFragment;->g:Ljava/util/List;

    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2415502
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_1

    .line 2415503
    :cond_1
    new-instance v9, LX/H0z;

    invoke-virtual {v2}, LX/4gs;->a()J

    move-result-wide v10

    move-object v13, v5

    move-object v14, v4

    move v15, v6

    move-object/from16 v16, v7

    invoke-direct/range {v9 .. v16}, LX/H0z;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/4gq;)V

    goto :goto_2

    .line 2415504
    :cond_2
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;->universes:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_3
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;

    .line 2415505
    new-instance v2, LX/H1b;

    iget-object v3, v8, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->name:Ljava/lang/String;

    iget-object v4, v8, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->currentExperiment:Ljava/lang/String;

    iget-object v5, v8, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->currentGroup:Ljava/lang/String;

    iget-object v6, v8, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->overrideExperiment:Ljava/lang/String;

    iget-object v7, v8, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->overrideGroup:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, LX/H1b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2415506
    iget-object v3, v8, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->experiments:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ExperimentDef;

    .line 2415507
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2415508
    iget-object v4, v3, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ExperimentDef;->groups:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GroupDef;

    .line 2415509
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 2415510
    iget-object v5, v4, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GroupDef;->params:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_4
    :goto_6
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ParamDef;

    .line 2415511
    new-instance v6, Landroid/util/Pair;

    iget-object v13, v5, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ParamDef;->config:Ljava/lang/String;

    iget v14, v5, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ParamDef;->key:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-direct {v6, v13, v14}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/H0w;

    .line 2415512
    if-eqz v6, :cond_4

    .line 2415513
    new-instance v13, Landroid/util/Pair;

    iget-object v5, v5, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ParamDef;->value:Ljava/lang/Object;

    invoke-direct {v13, v6, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v11, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 2415514
    :cond_5
    new-instance v5, LX/H1T;

    iget-object v4, v4, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GroupDef;->name:Ljava/lang/String;

    invoke-direct {v5, v4, v11}, LX/H1T;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 2415515
    :cond_6
    new-instance v4, LX/H1U;

    iget-object v3, v3, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ExperimentDef;->name:Ljava/lang/String;

    invoke-direct {v4, v3, v2, v8}, LX/H1U;-><init>(Ljava/lang/String;LX/H1b;Ljava/util/List;)V

    .line 2415516
    invoke-virtual {v2, v4}, LX/H1b;->a(LX/H1U;)V

    .line 2415517
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/mobileconfig/ui/SearchFragment;->g:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2415518
    :cond_7
    invoke-virtual {v2}, LX/H1b;->h()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    cmp-long v3, v4, v6

    if-lez v3, :cond_3

    .line 2415519
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/mobileconfig/ui/SearchFragment;->g:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 2415520
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/mobileconfig/ui/SearchFragment;->g:Ljava/util/List;

    new-instance v3, LX/H19;

    invoke-direct {v3}, LX/H19;-><init>()V

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2415521
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/mobileconfig/ui/SearchFragment;->f:LX/H1X;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/mobileconfig/ui/SearchFragment;->g:Ljava/util/List;

    invoke-virtual {v2, v3}, LX/H1X;->a(Ljava/util/List;)V

    .line 2415522
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x67feed17

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2415523
    const v0, 0x7f030b28

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2415524
    const v0, 0x7f0d1c1f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/mobileconfig/ui/SearchFragment;->d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2415525
    new-instance v0, LX/H1X;

    iget-object v3, p0, Lcom/facebook/mobileconfig/ui/SearchFragment;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/facebook/mobileconfig/ui/SearchFragment;->g:Ljava/util/List;

    invoke-direct {v0, v3, v4}, LX/H1X;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/facebook/mobileconfig/ui/SearchFragment;->f:LX/H1X;

    .line 2415526
    new-instance v0, LX/1P1;

    iget-object v3, p0, Lcom/facebook/mobileconfig/ui/SearchFragment;->a:Landroid/content/Context;

    invoke-direct {v0, v3}, LX/1P1;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/mobileconfig/ui/SearchFragment;->e:LX/1OR;

    .line 2415527
    invoke-virtual {p0}, Lcom/facebook/mobileconfig/ui/SearchFragment;->b()V

    .line 2415528
    iget-object v0, p0, Lcom/facebook/mobileconfig/ui/SearchFragment;->h:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/facebook/mobileconfig/ui/SearchFragment;->a(Ljava/lang/CharSequence;)V

    .line 2415529
    iget-object v0, p0, Lcom/facebook/mobileconfig/ui/SearchFragment;->d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/mobileconfig/ui/SearchFragment;->e:LX/1OR;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2415530
    iget-object v0, p0, Lcom/facebook/mobileconfig/ui/SearchFragment;->d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/mobileconfig/ui/SearchFragment;->f:LX/H1X;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2415531
    const/16 v0, 0x2b

    const v3, 0x5d1734e3

    invoke-static {v5, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method
