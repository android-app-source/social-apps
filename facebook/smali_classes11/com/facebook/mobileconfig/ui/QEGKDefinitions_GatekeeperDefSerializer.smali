.class public Lcom/facebook/mobileconfig/ui/QEGKDefinitions_GatekeeperDefSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GatekeeperDef;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2415231
    const-class v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GatekeeperDef;

    new-instance v1, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_GatekeeperDefSerializer;

    invoke-direct {v1}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_GatekeeperDefSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2415232
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2415233
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GatekeeperDef;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2415234
    if-nez p0, :cond_0

    .line 2415235
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2415236
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2415237
    invoke-static {p0, p1, p2}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_GatekeeperDefSerializer;->b(Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GatekeeperDef;LX/0nX;LX/0my;)V

    .line 2415238
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2415239
    return-void
.end method

.method private static b(Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GatekeeperDef;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2415240
    const-string v0, "name"

    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GatekeeperDef;->name:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2415241
    const-string v0, "config"

    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GatekeeperDef;->config:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2415242
    const-string v0, "key"

    iget v1, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GatekeeperDef;->key:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2415243
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2415244
    check-cast p1, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GatekeeperDef;

    invoke-static {p1, p2, p3}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_GatekeeperDefSerializer;->a(Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GatekeeperDef;LX/0nX;LX/0my;)V

    return-void
.end method
