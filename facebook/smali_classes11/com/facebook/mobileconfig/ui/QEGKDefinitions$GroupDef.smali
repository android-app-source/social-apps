.class public final Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GroupDef;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation


# instance fields
.field public name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public params:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "params"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ParamDef;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2415110
    const-class v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_GroupDefDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2415111
    const-class v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_GroupDefSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2415112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
