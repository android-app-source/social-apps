.class public Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0Ww;",
            ">;"
        }
    .end annotation
.end field

.field private q:LX/3gT;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private r:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private t:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field private u:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Yb;",
            ">;"
        }
    .end annotation
.end field

.field private v:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private w:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/init/MobileConfigInit;",
            ">;"
        }
    .end annotation
.end field

.field private x:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/contextual/ContextualResolver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2415041
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;)V
    .locals 3

    .prologue
    .line 2415042
    iget-object v0, p0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ww;

    .line 2415043
    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->w:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/mobileconfig/init/MobileConfigInit;

    .line 2415044
    iget-object v2, p0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->v:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v1, v2}, Lcom/facebook/mobileconfig/init/MobileConfigInit;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/0Wx;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Ww;->a(LX/0Wx;)V

    .line 2415045
    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->u:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/tigon/iface/TigonServiceHolder;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/0Ww;->setTigonService(Lcom/facebook/tigon/iface/TigonServiceHolder;Z)V

    .line 2415046
    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->x:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0uf;

    invoke-virtual {v1}, LX/0uf;->b()V

    .line 2415047
    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->t:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0W3;

    .line 2415048
    invoke-virtual {v1}, LX/0W3;->c()V

    .line 2415049
    invoke-virtual {v0}, LX/0Ww;->clearCurrentUserData()V

    .line 2415050
    return-void
.end method

.method private static a(Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;LX/0Or;LX/3gT;Ljava/util/concurrent/ExecutorService;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Or;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;",
            "LX/0Or",
            "<",
            "LX/0Ww;",
            ">;",
            "LX/3gT;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2Yb;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/init/MobileConfigInit;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/contextual/ContextualResolver;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2415051
    iput-object p1, p0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->p:LX/0Or;

    iput-object p2, p0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->q:LX/3gT;

    iput-object p3, p0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->r:Ljava/util/concurrent/ExecutorService;

    iput-object p4, p0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->s:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object p5, p0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->t:LX/0Or;

    iput-object p6, p0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->u:LX/0Or;

    iput-object p7, p0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->v:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->w:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->x:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 11

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;

    const/16 v1, 0xdf5

    invoke-static {v9, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v9}, LX/3gT;->b(LX/0QB;)LX/3gT;

    move-result-object v2

    check-cast v2, LX/3gT;

    invoke-static {v9}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v9}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v4

    check-cast v4, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v5, 0xdf4

    invoke-static {v9, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0xb8b

    invoke-static {v9, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x19e

    invoke-static {v9, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xdf8

    invoke-static {v9, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v10, 0x45e

    invoke-static {v9, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {v0 .. v9}, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->a(Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;LX/0Or;LX/3gT;Ljava/util/concurrent/ExecutorService;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Or;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method public static b(Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;)Z
    .locals 3

    .prologue
    .line 2415052
    const-class v0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;

    const-string v1, "Syncing mobile configs"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2415053
    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->q:LX/3gT;

    iget-object v0, p0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Wx;

    const/16 v2, 0x1388

    invoke-virtual {v1, v0, v2}, LX/3gT;->a(LX/0Wx;I)Z

    move-result v1

    .line 2415054
    const-class v2, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;

    if-eqz v1, :cond_0

    const-string v0, "Successfully synced configs"

    :goto_0
    invoke-static {v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2415055
    return v1

    .line 2415056
    :cond_0
    const-string v0, "Failed to sync configs"

    goto :goto_0
.end method

.method public static l(Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;)Z
    .locals 3

    .prologue
    .line 2415057
    const-class v0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;

    const-string v1, "Syncing QEs"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2415058
    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->q:LX/3gT;

    iget-object v0, p0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Wx;

    const/16 v2, 0x1388

    invoke-virtual {v1, v0, v2}, LX/3gT;->b(LX/0Wx;I)Z

    move-result v1

    .line 2415059
    const-class v2, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;

    if-eqz v1, :cond_0

    const-string v0, "Successfully synced QEs"

    :goto_0
    invoke-static {v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2415060
    return v1

    .line 2415061
    :cond_0
    const-string v0, "Failed to sync QEs"

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2415062
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2415063
    invoke-static {p0, p0}, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2415064
    new-instance v0, LX/H1H;

    invoke-direct {v0, p0}, LX/H1H;-><init>(Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;)V

    .line 2415065
    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->r:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity$2;

    invoke-direct {v2, p0, v0}, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity$2;-><init>(Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;Landroid/os/Handler;)V

    const v0, 0x438200e5    # 260.007f

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2415066
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2415067
    const-string v1, "Syncing settings..."

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2415068
    invoke-virtual {p0, v0}, Lcom/facebook/mobileconfig/ui/MobileConfigSyncActivity;->setContentView(Landroid/view/View;)V

    .line 2415069
    return-void
.end method
