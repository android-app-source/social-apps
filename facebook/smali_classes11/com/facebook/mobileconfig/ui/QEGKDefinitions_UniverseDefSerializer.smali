.class public Lcom/facebook/mobileconfig/ui/QEGKDefinitions_UniverseDefSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2415343
    const-class v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;

    new-instance v1, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_UniverseDefSerializer;

    invoke-direct {v1}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_UniverseDefSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2415344
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2415345
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2415346
    if-nez p0, :cond_0

    .line 2415347
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2415348
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2415349
    invoke-static {p0, p1, p2}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_UniverseDefSerializer;->b(Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;LX/0nX;LX/0my;)V

    .line 2415350
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2415351
    return-void
.end method

.method private static b(Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2415352
    const-string v0, "name"

    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->name:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2415353
    const-string v0, "current_experiment"

    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->currentExperiment:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2415354
    const-string v0, "current_group"

    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->currentGroup:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2415355
    const-string v0, "override_experiment"

    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->overrideExperiment:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2415356
    const-string v0, "override_group"

    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->overrideGroup:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2415357
    const-string v0, "experiments"

    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->experiments:Ljava/util/List;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 2415358
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2415359
    check-cast p1, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;

    invoke-static {p1, p2, p3}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_UniverseDefSerializer;->a(Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;LX/0nX;LX/0my;)V

    return-void
.end method
