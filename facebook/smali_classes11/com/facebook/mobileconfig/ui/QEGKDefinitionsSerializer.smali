.class public Lcom/facebook/mobileconfig/ui/QEGKDefinitionsSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/mobileconfig/ui/QEGKDefinitions;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2415160
    const-class v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;

    new-instance v1, Lcom/facebook/mobileconfig/ui/QEGKDefinitionsSerializer;

    invoke-direct {v1}, Lcom/facebook/mobileconfig/ui/QEGKDefinitionsSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2415161
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2415162
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/mobileconfig/ui/QEGKDefinitions;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2415163
    if-nez p0, :cond_0

    .line 2415164
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2415165
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2415166
    invoke-static {p0, p1, p2}, Lcom/facebook/mobileconfig/ui/QEGKDefinitionsSerializer;->b(Lcom/facebook/mobileconfig/ui/QEGKDefinitions;LX/0nX;LX/0my;)V

    .line 2415167
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2415168
    return-void
.end method

.method private static b(Lcom/facebook/mobileconfig/ui/QEGKDefinitions;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2415169
    const-string v0, "universes"

    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;->universes:Ljava/util/List;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 2415170
    const-string v0, "gatekeepers"

    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;->gatekeepers:Ljava/util/List;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 2415171
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2415172
    check-cast p1, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;

    invoke-static {p1, p2, p3}, Lcom/facebook/mobileconfig/ui/QEGKDefinitionsSerializer;->a(Lcom/facebook/mobileconfig/ui/QEGKDefinitions;LX/0nX;LX/0my;)V

    return-void
.end method
