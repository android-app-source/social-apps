.class public Lcom/facebook/mobileconfig/ui/QEGKDefinitions_ParamDefSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ParamDef;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2415315
    const-class v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ParamDef;

    new-instance v1, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_ParamDefSerializer;

    invoke-direct {v1}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_ParamDefSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2415316
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2415314
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ParamDef;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2415308
    if-nez p0, :cond_0

    .line 2415309
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2415310
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2415311
    invoke-static {p0, p1, p2}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_ParamDefSerializer;->b(Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ParamDef;LX/0nX;LX/0my;)V

    .line 2415312
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2415313
    return-void
.end method

.method private static b(Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ParamDef;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2415304
    const-string v0, "config"

    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ParamDef;->config:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2415305
    const-string v0, "key"

    iget v1, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ParamDef;->key:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2415306
    const-string v0, "value"

    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ParamDef;->value:Ljava/lang/Object;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2415307
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2415303
    check-cast p1, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ParamDef;

    invoke-static {p1, p2, p3}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_ParamDefSerializer;->a(Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ParamDef;LX/0nX;LX/0my;)V

    return-void
.end method
