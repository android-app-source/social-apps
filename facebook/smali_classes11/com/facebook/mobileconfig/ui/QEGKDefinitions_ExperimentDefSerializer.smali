.class public Lcom/facebook/mobileconfig/ui/QEGKDefinitions_ExperimentDefSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ExperimentDef;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2415206
    const-class v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ExperimentDef;

    new-instance v1, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_ExperimentDefSerializer;

    invoke-direct {v1}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_ExperimentDefSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2415207
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2415205
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ExperimentDef;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2415199
    if-nez p0, :cond_0

    .line 2415200
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2415201
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2415202
    invoke-static {p0, p1, p2}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_ExperimentDefSerializer;->b(Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ExperimentDef;LX/0nX;LX/0my;)V

    .line 2415203
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2415204
    return-void
.end method

.method private static b(Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ExperimentDef;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2415195
    const-string v0, "name"

    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ExperimentDef;->name:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2415196
    const-string v0, "groups"

    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ExperimentDef;->groups:Ljava/util/List;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 2415197
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2415198
    check-cast p1, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ExperimentDef;

    invoke-static {p1, p2, p3}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_ExperimentDefSerializer;->a(Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ExperimentDef;LX/0nX;LX/0my;)V

    return-void
.end method
