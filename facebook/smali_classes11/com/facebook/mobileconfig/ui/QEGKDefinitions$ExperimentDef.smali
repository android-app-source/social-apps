.class public final Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ExperimentDef;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation


# instance fields
.field public groups:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "groups"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GroupDef;",
            ">;"
        }
    .end annotation
.end field

.field public name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2415104
    const-class v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_ExperimentDefDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2415105
    const-class v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_ExperimentDefSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2415106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
