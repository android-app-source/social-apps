.class public Lcom/facebook/mobileconfig/ui/MainFragment;
.super Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;
.source ""


# instance fields
.field public a:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ww;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3gT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Uh;
    .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:LX/H16;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2414767
    invoke-direct {p0}, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;-><init>()V

    .line 2414768
    return-void
.end method

.method private a(Landroid/view/ViewGroup;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2414769
    new-instance v0, Lcom/facebook/fig/sectionheader/FigSectionHeader;

    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/MainFragment;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/fig/sectionheader/FigSectionHeader;-><init>(Landroid/content/Context;)V

    .line 2414770
    invoke-virtual {v0, p2}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2414771
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2414772
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2414773
    invoke-super {p0, p1}, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->a(Landroid/os/Bundle;)V

    .line 2414774
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/mobileconfig/ui/MainFragment;

    invoke-static {p1}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-static {p1}, LX/0Ww;->a(LX/0QB;)LX/0Ww;

    move-result-object v4

    check-cast v4, LX/0Ww;

    invoke-static {p1}, LX/3gT;->b(LX/0QB;)LX/3gT;

    move-result-object v5

    check-cast v5, LX/3gT;

    invoke-static {p1}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {p1}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    iput-object v3, v2, Lcom/facebook/mobileconfig/ui/MainFragment;->a:LX/0W3;

    iput-object v4, v2, Lcom/facebook/mobileconfig/ui/MainFragment;->b:LX/0Ww;

    iput-object v5, v2, Lcom/facebook/mobileconfig/ui/MainFragment;->c:LX/3gT;

    iput-object v6, v2, Lcom/facebook/mobileconfig/ui/MainFragment;->d:Ljava/util/concurrent/ExecutorService;

    iput-object v7, v2, Lcom/facebook/mobileconfig/ui/MainFragment;->e:LX/0Uh;

    iput-object p1, v2, Lcom/facebook/mobileconfig/ui/MainFragment;->f:Landroid/content/Context;

    .line 2414775
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, -0x5dcf6b0e

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2414776
    const v0, 0x7f030b26

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2414777
    const v1, 0x7f0d1c19

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 2414778
    const v3, 0x7f08361e    # 1.81056E38f

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lcom/facebook/mobileconfig/ui/MainFragment;->a(Landroid/view/ViewGroup;Ljava/lang/String;)V

    .line 2414779
    new-instance v4, Lcom/facebook/fig/listitem/FigListItem;

    iget-object v3, p0, Lcom/facebook/mobileconfig/ui/MainFragment;->f:Landroid/content/Context;

    const/4 v6, 0x3

    invoke-direct {v4, v3, v6}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;I)V

    .line 2414780
    new-instance v6, Landroid/content/ComponentName;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v7, Lcom/facebook/mobileconfig/init/MobileConfigEnableReceiver;

    invoke-direct {v6, v3, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2414781
    iget-object v3, p0, Lcom/facebook/mobileconfig/ui/MainFragment;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 2414782
    const/4 v7, 0x2

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v7, v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-virtual {v4, v3}, Lcom/facebook/fig/listitem/FigListItem;->setActionState(Z)V

    .line 2414783
    const v3, 0x7f083614

    invoke-virtual {v4, v3}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(I)V

    .line 2414784
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v7, 0x7f083615

    invoke-virtual {p0, v7}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "\n"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 2414785
    new-instance v7, Landroid/content/ComponentName;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    const-class p2, Lcom/facebook/mobileconfig/init/MobileConfigEnableReceiver;

    invoke-direct {v7, p1, p2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2414786
    iget-object p1, p0, Lcom/facebook/mobileconfig/ui/MainFragment;->b:LX/0Ww;

    invoke-virtual {p1}, LX/0Ww;->isValid()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 2414787
    const-string v7, "Current status: enabled"

    .line 2414788
    :goto_1
    move-object v7, v7

    .line 2414789
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2414790
    new-instance v3, LX/H11;

    invoke-direct {v3, p0, v6}, LX/H11;-><init>(Lcom/facebook/mobileconfig/ui/MainFragment;Landroid/content/ComponentName;)V

    invoke-virtual {v4, v3}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2414791
    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2414792
    new-instance v3, Lcom/facebook/fig/listitem/FigListItem;

    iget-object v4, p0, Lcom/facebook/mobileconfig/ui/MainFragment;->f:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;)V

    .line 2414793
    const v4, 0x7f08361b

    invoke-virtual {v3, v4}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(I)V

    .line 2414794
    const v4, 0x7f08361c

    invoke-virtual {v3, v4}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(I)V

    .line 2414795
    new-instance v4, LX/H14;

    invoke-direct {v4, p0}, LX/H14;-><init>(Lcom/facebook/mobileconfig/ui/MainFragment;)V

    invoke-virtual {v3, v4}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2414796
    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2414797
    new-instance v3, Lcom/facebook/fig/listitem/FigListItem;

    iget-object v4, p0, Lcom/facebook/mobileconfig/ui/MainFragment;->f:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;)V

    .line 2414798
    const v4, 0x7f083612

    invoke-virtual {v3, v4}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(I)V

    .line 2414799
    new-instance v4, LX/H12;

    invoke-direct {v4, p0}, LX/H12;-><init>(Lcom/facebook/mobileconfig/ui/MainFragment;)V

    invoke-virtual {v3, v4}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2414800
    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2414801
    const v3, 0x7f083618

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lcom/facebook/mobileconfig/ui/MainFragment;->a(Landroid/view/ViewGroup;Ljava/lang/String;)V

    .line 2414802
    const v1, 0x7f0d1c1a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    .line 2414803
    new-instance v3, LX/H16;

    iget-object v4, p0, Lcom/facebook/mobileconfig/ui/MainFragment;->f:Landroid/content/Context;

    invoke-direct {v3, v4}, LX/H16;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/facebook/mobileconfig/ui/MainFragment;->g:LX/H16;

    .line 2414804
    iget-object v3, p0, Lcom/facebook/mobileconfig/ui/MainFragment;->g:LX/H16;

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2414805
    new-instance v3, LX/1P1;

    iget-object v4, p0, Lcom/facebook/mobileconfig/ui/MainFragment;->f:Landroid/content/Context;

    invoke-direct {v3, v4}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2414806
    const/16 v1, 0x2b

    const v3, -0x7d1ef455

    invoke-static {v5, v1, v3, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0

    .line 2414807
    :cond_0
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 2414808
    :cond_1
    const-string p1, "off_killswitch"

    iget-object p2, p0, Lcom/facebook/mobileconfig/ui/MainFragment;->b:LX/0Ww;

    invoke-virtual {p2}, LX/0Ww;->getFrameworkStatus()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 2414809
    const-string v7, "Current status: disabled by the killswitch"

    goto/16 :goto_1

    .line 2414810
    :cond_2
    iget-object p1, p0, Lcom/facebook/mobileconfig/ui/MainFragment;->e:LX/0Uh;

    const/16 p2, 0xd

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, LX/0Uh;->a(IZ)Z

    move-result p1

    if-nez p1, :cond_3

    .line 2414811
    const-string v7, "Current status: disabled by the gatekeeper"

    goto/16 :goto_1

    .line 2414812
    :cond_3
    const/4 p1, 0x2

    iget-object p2, p0, Lcom/facebook/mobileconfig/ui/MainFragment;->f:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p2

    invoke-virtual {p2, v7}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v7

    if-ne p1, v7, :cond_4

    .line 2414813
    const-string v7, "Current status: disabled by user in internal setting"

    goto/16 :goto_1

    .line 2414814
    :cond_4
    const-string v7, "Current status: disabled for unknown reasons"

    goto/16 :goto_1
.end method
