.class public final Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation


# instance fields
.field public currentExperiment:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "current_experiment"
    .end annotation
.end field

.field public currentGroup:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "current_group"
    .end annotation
.end field

.field public experiments:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "experiments"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ExperimentDef;",
            ">;"
        }
    .end annotation
.end field

.field public name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public overrideExperiment:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "override_experiment"
    .end annotation
.end field

.field public overrideGroup:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "override_group"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2415116
    const-class v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_UniverseDefDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2415117
    const-class v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions_UniverseDefSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2415118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2415119
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->overrideExperiment:Ljava/lang/String;

    .line 2415120
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->overrideGroup:Ljava/lang/String;

    return-void
.end method
