.class public Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/4gq;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/H0v;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/0Ww;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private s:LX/5eF;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:LX/3gT;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private u:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public v:LX/0gc;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2415026
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2415027
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->q:Ljava/util/List;

    return-void
.end method

.method private a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)LX/3oW;
    .locals 1

    .prologue
    .line 2415025
    invoke-virtual {p0, p1}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->a(Ljava/lang/CharSequence;)LX/3oW;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, LX/3oW;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)LX/3oW;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;LX/0Ww;LX/5eF;LX/4gq;LX/3gT;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .prologue
    .line 2415024
    iput-object p1, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->r:LX/0Ww;

    iput-object p2, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->s:LX/5eF;

    iput-object p3, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->p:LX/4gq;

    iput-object p4, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->t:LX/3gT;

    iput-object p5, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->u:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    invoke-static {v5}, LX/0Ww;->a(LX/0QB;)LX/0Ww;

    move-result-object v1

    check-cast v1, LX/0Ww;

    invoke-static {v5}, LX/5eG;->b(LX/0QB;)LX/5eF;

    move-result-object v2

    check-cast v2, LX/5eF;

    new-instance p0, LX/4gq;

    invoke-static {v5}, LX/0Ww;->a(LX/0QB;)LX/0Ww;

    move-result-object v3

    check-cast v3, LX/0Ww;

    invoke-static {v5}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    invoke-direct {p0, v3, v4}, LX/4gq;-><init>(LX/0Ww;LX/0W3;)V

    move-object v3, p0

    check-cast v3, LX/4gq;

    invoke-static {v5}, LX/3gT;->b(LX/0QB;)LX/3gT;

    move-result-object v4

    check-cast v4, LX/3gT;

    invoke-static {v5}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {v0 .. v5}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->a(Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;LX/0Ww;LX/5eF;LX/4gq;LX/3gT;Ljava/util/concurrent/ExecutorService;)V

    return-void
.end method

.method private b()V
    .locals 21

    .prologue
    .line 2414989
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->q:Ljava/util/List;

    .line 2414990
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->r:LX/0Ww;

    invoke-static {v2}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;->a(LX/0Wx;)Lcom/facebook/mobileconfig/ui/QEGKDefinitions;

    move-result-object v16

    .line 2414991
    new-instance v17, Ljava/util/HashMap;

    invoke-direct/range {v17 .. v17}, Ljava/util/HashMap;-><init>()V

    .line 2414992
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;->gatekeepers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GatekeeperDef;

    .line 2414993
    new-instance v4, Landroid/util/Pair;

    iget-object v5, v2, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GatekeeperDef;->config:Ljava/lang/String;

    iget v6, v2, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GatekeeperDef;->key:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v2, v2, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GatekeeperDef;->name:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-interface {v0, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2414994
    :cond_0
    new-instance v18, Ljava/util/HashMap;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashMap;-><init>()V

    .line 2414995
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->s:LX/5eF;

    iget-object v0, v2, LX/5eF;->a:LX/0Px;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, LX/0Px;->size()I

    move-result v20

    const/4 v2, 0x0

    move v15, v2

    :goto_1
    move/from16 v0, v20

    if-ge v15, v0, :cond_2

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4gs;

    .line 2414996
    iget-object v5, v2, LX/4gs;->a:Ljava/lang/String;

    .line 2414997
    iget-object v4, v2, LX/4gs;->b:Ljava/lang/String;

    .line 2414998
    iget v6, v2, LX/4gs;->c:I

    .line 2414999
    new-instance v3, Landroid/util/Pair;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {v3, v5, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 2415000
    if-nez v10, :cond_1

    .line 2415001
    invoke-virtual {v2}, LX/4gs;->a()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->p:LX/4gq;

    invoke-static/range {v2 .. v7}, LX/H0w;->a(JLjava/lang/String;Ljava/lang/String;ILX/4gq;)LX/H0w;

    move-result-object v7

    .line 2415002
    :goto_2
    new-instance v2, Landroid/util/Pair;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v2, v5, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v0, v18

    invoke-interface {v0, v2, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2415003
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->q:Ljava/util/List;

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2415004
    add-int/lit8 v2, v15, 0x1

    move v15, v2

    goto :goto_1

    .line 2415005
    :cond_1
    new-instance v7, LX/H0z;

    invoke-virtual {v2}, LX/4gs;->a()J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->p:LX/4gq;

    move-object v11, v5

    move-object v12, v4

    move v13, v6

    invoke-direct/range {v7 .. v14}, LX/H0z;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/4gq;)V

    goto :goto_2

    .line 2415006
    :cond_2
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;->universes:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_3
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;

    .line 2415007
    new-instance v2, LX/H1b;

    iget-object v3, v8, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->name:Ljava/lang/String;

    iget-object v4, v8, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->currentExperiment:Ljava/lang/String;

    iget-object v5, v8, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->currentGroup:Ljava/lang/String;

    iget-object v6, v8, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->overrideExperiment:Ljava/lang/String;

    iget-object v7, v8, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->overrideGroup:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, LX/H1b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2415008
    iget-object v3, v8, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->experiments:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ExperimentDef;

    .line 2415009
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2415010
    iget-object v4, v3, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ExperimentDef;->groups:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GroupDef;

    .line 2415011
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 2415012
    iget-object v5, v4, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GroupDef;->params:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_4
    :goto_6
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ParamDef;

    .line 2415013
    new-instance v6, Landroid/util/Pair;

    iget-object v13, v5, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ParamDef;->config:Ljava/lang/String;

    iget v14, v5, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ParamDef;->key:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-direct {v6, v13, v14}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v0, v18

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/H0w;

    .line 2415014
    if-eqz v6, :cond_4

    .line 2415015
    new-instance v13, Landroid/util/Pair;

    iget-object v5, v5, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ParamDef;->value:Ljava/lang/Object;

    invoke-direct {v13, v6, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v11, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 2415016
    :cond_5
    new-instance v5, LX/H1T;

    iget-object v4, v4, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GroupDef;->name:Ljava/lang/String;

    invoke-direct {v5, v4, v11}, LX/H1T;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 2415017
    :cond_6
    new-instance v4, LX/H1U;

    iget-object v3, v3, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$ExperimentDef;->name:Ljava/lang/String;

    invoke-direct {v4, v3, v2, v8}, LX/H1U;-><init>(Ljava/lang/String;LX/H1b;Ljava/util/List;)V

    .line 2415018
    invoke-virtual {v2, v4}, LX/H1b;->a(LX/H1U;)V

    .line 2415019
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->q:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2415020
    :cond_7
    invoke-virtual {v2}, LX/H1b;->h()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    cmp-long v3, v4, v6

    if-lez v3, :cond_3

    .line 2415021
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->q:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 2415022
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->q:Ljava/util/List;

    new-instance v3, LX/H19;

    invoke-direct {v3}, LX/H19;-><init>()V

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2415023
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)LX/3oW;
    .locals 3

    .prologue
    .line 2414985
    const v0, 0x7f0d1c0a

    invoke-virtual {p0, v0}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, LX/3oW;->a(Landroid/view/View;Ljava/lang/CharSequence;I)LX/3oW;

    move-result-object v1

    .line 2414986
    iget-object v0, v1, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    move-object v0, v0

    .line 2414987
    const v2, 0x7f0d0c51

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2414988
    return-object v1
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 2414885
    iget-object v0, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->p:LX/4gq;

    invoke-virtual {v0}, LX/4gq;->a()V

    .line 2414886
    iget-object v0, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->v:LX/0gc;

    const-string v1, "search"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/mobileconfig/ui/SearchFragment;

    .line 2414887
    if-eqz v0, :cond_0

    .line 2414888
    invoke-virtual {v0}, Lcom/facebook/mobileconfig/ui/SearchFragment;->b()V

    .line 2414889
    :cond_0
    const-string v0, "Restart the app for changes to take effect."

    invoke-virtual {p0, v0}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->b(Ljava/lang/CharSequence;)LX/3oW;

    move-result-object v0

    invoke-virtual {v0}, LX/3oW;->b()V

    .line 2414890
    return-void
.end method

.method public final a(LX/H0w;)V
    .locals 4

    .prologue
    .line 2414978
    const/4 v0, 0x0

    iput-boolean v0, p1, LX/H0w;->k:Z

    .line 2414979
    iget-object v0, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->p:LX/4gq;

    iget-wide v2, p1, LX/H0w;->h:J

    .line 2414980
    iget-object v1, v0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    if-eqz v1, :cond_0

    .line 2414981
    iget-object v1, v0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->removeOverrideForParam(J)V

    .line 2414982
    :cond_0
    iget-object v0, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->p:LX/4gq;

    invoke-virtual {p1, v0}, LX/H0w;->a(LX/4gq;)V

    .line 2414983
    const-string v0, "Override removed! Restart the app for changes to take effect."

    invoke-virtual {p0, v0}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->b(Ljava/lang/CharSequence;)LX/3oW;

    move-result-object v0

    invoke-virtual {v0}, LX/3oW;->b()V

    .line 2414984
    return-void
.end method

.method public final a(LX/H0w;Ljava/lang/Object;)V
    .locals 6
    .param p2    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2414948
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 2414949
    :cond_0
    :goto_0
    return-void

    .line 2414950
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p1, LX/H0w;->k:Z

    .line 2414951
    instance-of v0, p1, LX/H0x;

    if-eqz v0, :cond_3

    .line 2414952
    iget-object v0, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->p:LX/4gq;

    iget-wide v2, p1, LX/H0w;->h:J

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 2414953
    iget-object v4, v0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    if-eqz v4, :cond_2

    .line 2414954
    iget-object v4, v0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    .line 2414955
    invoke-virtual {v4, v2, v3, v1}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->updateOverrideForBool(JZ)V

    .line 2414956
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->p:LX/4gq;

    invoke-virtual {p1, v0}, LX/H0w;->a(LX/4gq;)V

    .line 2414957
    const-string v0, "Override set! Restart the app for changes to take effect."

    invoke-virtual {p0, v0}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->b(Ljava/lang/CharSequence;)LX/3oW;

    move-result-object v0

    invoke-virtual {v0}, LX/3oW;->b()V

    goto :goto_0

    .line 2414958
    :cond_3
    instance-of v0, p1, LX/H10;

    if-eqz v0, :cond_5

    .line 2414959
    instance-of v0, p2, Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 2414960
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2414961
    :goto_2
    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->p:LX/4gq;

    iget-wide v2, p1, LX/H0w;->h:J

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 2414962
    iget-object v0, v1, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    if-eqz v0, :cond_4

    .line 2414963
    iget-object v0, v1, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    .line 2414964
    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->updateOverrideForInt(JJ)V

    .line 2414965
    :cond_4
    goto :goto_1

    .line 2414966
    :cond_5
    instance-of v0, p1, LX/H0y;

    if-eqz v0, :cond_7

    .line 2414967
    iget-object v0, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->p:LX/4gq;

    iget-wide v2, p1, LX/H0w;->h:J

    check-cast p2, Ljava/lang/Double;

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    .line 2414968
    iget-object v1, v0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    if-eqz v1, :cond_6

    .line 2414969
    iget-object v1, v0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    .line 2414970
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->updateOverrideForDouble(JD)V

    .line 2414971
    :cond_6
    goto :goto_1

    .line 2414972
    :cond_7
    instance-of v0, p1, LX/H1Y;

    if-eqz v0, :cond_2

    .line 2414973
    iget-object v0, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->p:LX/4gq;

    iget-wide v2, p1, LX/H0w;->h:J

    check-cast p2, Ljava/lang/String;

    .line 2414974
    iget-object v1, v0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    if-eqz v1, :cond_8

    .line 2414975
    iget-object v1, v0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    .line 2414976
    invoke-virtual {v1, v2, v3, p2}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->updateOverrideForString(JLjava/lang/String;)V

    .line 2414977
    :cond_8
    goto :goto_1

    :cond_9
    move-object v0, p2

    goto :goto_2
.end method

.method public final a(LX/H1U;)V
    .locals 5

    .prologue
    .line 2414937
    iget-object v0, p1, LX/H1U;->a:LX/H1b;

    const-string v1, ""

    iput-object v1, v0, LX/H1b;->g:Ljava/lang/String;

    .line 2414938
    iget-object v0, p1, LX/H1U;->a:LX/H1b;

    const-string v1, ""

    iput-object v1, v0, LX/H1b;->h:Ljava/lang/String;

    .line 2414939
    iget-object v0, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->r:LX/0Ww;

    invoke-static {v0}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;->a(LX/0Wx;)Lcom/facebook/mobileconfig/ui/QEGKDefinitions;

    move-result-object v1

    .line 2414940
    iget-object v0, v1, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;->universes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;

    .line 2414941
    iget-object v3, v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->name:Ljava/lang/String;

    iget-object v4, p1, LX/H1U;->a:LX/H1b;

    iget-object v4, v4, LX/H0v;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2414942
    const-string v2, ""

    iput-object v2, v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->overrideExperiment:Ljava/lang/String;

    .line 2414943
    const-string v2, ""

    iput-object v2, v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->overrideGroup:Ljava/lang/String;

    .line 2414944
    iget-object v0, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->r:LX/0Ww;

    invoke-virtual {v1, v0}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;->b(LX/0Wx;)V

    .line 2414945
    :cond_1
    iget-object v0, p1, LX/H1U;->a:LX/H1b;

    iget-object v0, v0, LX/H1b;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H0w;

    .line 2414946
    invoke-virtual {p0, v0}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->a(LX/H0w;)V

    goto :goto_0

    .line 2414947
    :cond_2
    return-void
.end method

.method public final a(LX/H1U;LX/H1T;)Z
    .locals 6

    .prologue
    .line 2414915
    iget-object v0, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->r:LX/0Ww;

    invoke-static {v0}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;->a(LX/0Wx;)Lcom/facebook/mobileconfig/ui/QEGKDefinitions;

    move-result-object v0

    .line 2414916
    iget-object v1, p1, LX/H1U;->a:LX/H1b;

    iget-object v1, v1, LX/H0v;->b:Ljava/lang/String;

    .line 2414917
    iget-object v2, v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;->universes:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;

    .line 2414918
    iget-object v4, v2, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->name:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2414919
    :goto_0
    move-object v1, v2

    .line 2414920
    if-eqz v1, :cond_1

    .line 2414921
    iget-object v2, p1, LX/H0v;->b:Ljava/lang/String;

    iput-object v2, v1, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->overrideExperiment:Ljava/lang/String;

    .line 2414922
    iget-object v2, p2, LX/H1T;->a:Ljava/lang/String;

    iput-object v2, v1, Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;->overrideGroup:Ljava/lang/String;

    .line 2414923
    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->r:LX/0Ww;

    invoke-virtual {v0, v1}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;->b(LX/0Wx;)V

    .line 2414924
    iget-object v0, p1, LX/H1U;->a:LX/H1b;

    iget-object v1, p1, LX/H0v;->b:Ljava/lang/String;

    iput-object v1, v0, LX/H1b;->g:Ljava/lang/String;

    .line 2414925
    iget-object v0, p1, LX/H1U;->a:LX/H1b;

    iget-object v1, p2, LX/H1T;->a:Ljava/lang/String;

    iput-object v1, v0, LX/H1b;->h:Ljava/lang/String;

    .line 2414926
    iget-object v0, p1, LX/H1U;->a:LX/H1b;

    iget-object v0, v0, LX/H1b;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H0w;

    .line 2414927
    sget-object v2, LX/H1G;->a:[I

    iget-wide v4, v0, LX/H0w;->h:J

    invoke-static {v4, v5}, LX/0X6;->c(J)LX/0oE;

    move-result-object v3

    invoke-virtual {v3}, LX/0oE;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_1

    .line 2414928
    :pswitch_0
    iget-wide v2, v0, LX/H0w;->h:J

    invoke-static {v2, v3}, LX/0ok;->a(J)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->a(LX/H0w;Ljava/lang/Object;)V

    goto :goto_1

    .line 2414929
    :cond_1
    const/4 v0, 0x0

    .line 2414930
    :goto_2
    return v0

    .line 2414931
    :pswitch_1
    iget-wide v2, v0, LX/H0w;->h:J

    invoke-static {v2, v3}, LX/0ok;->c(J)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->a(LX/H0w;Ljava/lang/Object;)V

    goto :goto_1

    .line 2414932
    :pswitch_2
    iget-wide v2, v0, LX/H0w;->h:J

    invoke-static {v2, v3}, LX/0ok;->b(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->a(LX/H0w;Ljava/lang/Object;)V

    goto :goto_1

    .line 2414933
    :pswitch_3
    iget-wide v2, v0, LX/H0w;->h:J

    invoke-static {v2, v3}, LX/0ok;->d(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->a(LX/H0w;Ljava/lang/Object;)V

    goto :goto_1

    .line 2414934
    :cond_2
    iget-object v0, p2, LX/H1T;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 2414935
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, LX/H0w;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {p0, v1, v0}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->a(LX/H0w;Ljava/lang/Object;)V

    goto :goto_3

    .line 2414936
    :cond_3
    const/4 v0, 0x1

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b(Ljava/lang/CharSequence;)LX/3oW;
    .locals 2

    .prologue
    .line 2414914
    const-string v0, "Crash now!"

    new-instance v1, LX/H1D;

    invoke-direct {v1, p0}, LX/H1D;-><init>(Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)LX/3oW;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2414900
    invoke-static {p0, p0}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2414901
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2414902
    const v0, 0x7f030b22

    invoke-virtual {p0, v0}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->setContentView(I)V

    .line 2414903
    iget-object v0, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->u:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity$1;

    invoke-direct {v1, p0}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity$1;-><init>(Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;)V

    const v2, -0x11c527da

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2414904
    invoke-direct {p0}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->b()V

    .line 2414905
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->v:LX/0gc;

    .line 2414906
    new-instance v0, Lcom/facebook/mobileconfig/ui/MainFragment;

    invoke-direct {v0}, Lcom/facebook/mobileconfig/ui/MainFragment;-><init>()V

    .line 2414907
    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->v:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d1c0e

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2414908
    const v0, 0x7f0d1c0c

    invoke-virtual {p0, v0}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    .line 2414909
    new-instance v1, LX/H1A;

    invoke-direct {v1, p0}, LX/H1A;-><init>(Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2414910
    new-instance v1, LX/H1B;

    invoke-direct {v1, p0}, LX/H1B;-><init>(Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2414911
    const v0, 0x7f0d1c0d

    invoke-virtual {p0, v0}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    .line 2414912
    new-instance v1, LX/H1C;

    invoke-direct {v1, p0}, LX/H1C;-><init>(Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2414913
    return-void
.end method

.method public final c(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 2414898
    new-instance v0, LX/0ju;

    invoke-direct {v0, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Relaunch the app for changes to take effect."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    const-string v1, "Close"

    new-instance v2, LX/H1F;

    invoke-direct {v2, p0}, LX/H1F;-><init>(Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const-string v1, "Crash Now"

    new-instance v2, LX/H1E;

    invoke-direct {v2, p0}, LX/H1E;-><init>(Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2414899
    return-void
.end method

.method public displayDetailView(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2414891
    new-instance v0, Lcom/facebook/mobileconfig/ui/DetailFragment;

    invoke-direct {v0}, Lcom/facebook/mobileconfig/ui/DetailFragment;-><init>()V

    .line 2414892
    iput-object p1, v0, Lcom/facebook/mobileconfig/ui/DetailFragment;->b:Landroid/view/View;

    .line 2414893
    iget-object v1, p0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->v:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d1c0e

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2414894
    invoke-virtual {p0}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 2414895
    if-eqz v0, :cond_0

    .line 2414896
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 2414897
    :cond_0
    return-void
.end method
