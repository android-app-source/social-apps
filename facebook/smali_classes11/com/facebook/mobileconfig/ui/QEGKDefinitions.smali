.class public Lcom/facebook/mobileconfig/ui/QEGKDefinitions;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;


# instance fields
.field public gatekeepers:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "gatekeepers"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/mobileconfig/ui/QEGKDefinitions$GatekeeperDef;",
            ">;"
        }
    .end annotation
.end field

.field public universes:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "universes"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/mobileconfig/ui/QEGKDefinitions$UniverseDef;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2415121
    const-class v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitionsDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2415122
    const-class v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitionsSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2415123
    const-class v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;

    sput-object v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2415124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2415125
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;->universes:Ljava/util/List;

    .line 2415126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;->gatekeepers:Ljava/util/List;

    return-void
.end method

.method public static a(LX/0Wx;)Lcom/facebook/mobileconfig/ui/QEGKDefinitions;
    .locals 3

    .prologue
    .line 2415127
    invoke-interface {p0}, LX/0Wx;->getQEInfoFilename()Ljava/lang/String;

    move-result-object v0

    .line 2415128
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2415129
    :cond_0
    new-instance v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;

    invoke-direct {v0}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;-><init>()V

    .line 2415130
    :goto_0
    return-object v0

    .line 2415131
    :cond_1
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-class v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;

    invoke-virtual {v1, v2, v0}, LX/0lC;->a(Ljava/io/File;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2415132
    :catch_0
    new-instance v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;

    invoke-direct {v0}, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final b(LX/0Wx;)V
    .locals 3

    .prologue
    .line 2415133
    invoke-interface {p1}, LX/0Wx;->getQEInfoFilename()Ljava/lang/String;

    move-result-object v0

    .line 2415134
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2415135
    :cond_0
    :goto_0
    return-void

    .line 2415136
    :cond_1
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, p0}, LX/0lC;->a(Ljava/io/File;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2415137
    :catch_0
    sget-object v0, Lcom/facebook/mobileconfig/ui/QEGKDefinitions;->a:Ljava/lang/Class;

    const-string v1, "failed to write QE/GK metadata back to disk"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0
.end method
