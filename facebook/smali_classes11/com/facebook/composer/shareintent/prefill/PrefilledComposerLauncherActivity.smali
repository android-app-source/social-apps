.class public Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field public static final u:Ljava/lang/String;

.field private static final v:LX/21D;


# instance fields
.field private A:Landroid/net/Uri;

.field private B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Landroid/widget/ProgressBar;

.field public H:Lcom/facebook/widget/error/GenericErrorView;

.field public p:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/929;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1kR;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private x:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Nq;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5oW;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2368453
    const-class v0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->u:Ljava/lang/String;

    .line 2368454
    sget-object v0, LX/21D;->URI_HANDLER:LX/21D;

    sput-object v0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->v:LX/21D;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2368444
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2368445
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2368446
    iput-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->w:LX/0Ot;

    .line 2368447
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2368448
    iput-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->x:LX/0Ot;

    .line 2368449
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2368450
    iput-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->y:LX/0Ot;

    .line 2368451
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2368452
    iput-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->z:LX/0Ot;

    return-void
.end method

.method public static synthetic a(Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;)Lcom/facebook/productionprompts/model/ProductionPrompt;
    .locals 1

    .prologue
    .line 2368443
    invoke-direct {p0, p1, p2}, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->a(Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;)Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;)Lcom/facebook/productionprompts/model/ProductionPrompt;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/productionprompts/model/ProductionPrompt;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2368434
    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p1}, LX/1kR;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p1}, LX/1kR;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 2368435
    :goto_0
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    move v2, v3

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;

    .line 2368436
    invoke-static {v0, v3}, Lcom/facebook/productionprompts/model/ProductionPrompt;->a(Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;Z)Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v0

    .line 2368437
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2368438
    :goto_2
    return-object v0

    .line 2368439
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2368440
    move-object v1, v0

    goto :goto_0

    .line 2368441
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2368442
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private static a(Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;LX/1Kf;Ljava/util/concurrent/ExecutorService;LX/929;Ljava/lang/Boolean;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/03V;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;",
            "LX/1Kf;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/929;",
            "Ljava/lang/Boolean;",
            "LX/0Ot",
            "<",
            "LX/1kR;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Nq;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/5oW;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2368433
    iput-object p1, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->p:LX/1Kf;

    iput-object p2, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->q:Ljava/util/concurrent/ExecutorService;

    iput-object p3, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->r:LX/929;

    iput-object p4, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->s:Ljava/lang/Boolean;

    iput-object p5, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->w:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->x:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->y:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->z:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->t:LX/03V;

    return-void
.end method

.method private a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2368431
    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v1, "FETCH_PROMPT"

    new-instance v2, LX/Gab;

    invoke-direct {v2, p0, p2}, LX/Gab;-><init>(Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;Ljava/lang/String;)V

    new-instance v3, LX/Gac;

    invoke-direct {v3, p0, p2, p1}, LX/Gac;-><init>(Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2368432
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    invoke-static {v9}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v1

    check-cast v1, LX/1Kf;

    invoke-static {v9}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {v9}, LX/929;->b(LX/0QB;)LX/929;

    move-result-object v3

    check-cast v3, LX/929;

    invoke-static {v9}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    const/16 v5, 0xfd3

    invoke-static {v9, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x12b1

    invoke-static {v9, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xbdd

    invoke-static {v9, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2ff9

    invoke-static {v9, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v9}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static/range {v0 .. v9}, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->a(Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;LX/1Kf;Ljava/util/concurrent/ExecutorService;LX/929;Ljava/lang/Boolean;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/03V;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;)V
    .locals 3

    .prologue
    .line 2368429
    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->p:LX/1Kf;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-interface {v0, v1, v2, p0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 2368430
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2368417
    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2368418
    const v1, 0x7f031004

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->setContentView(Landroid/view/View;)V

    .line 2368419
    const v0, 0x7f0d0a7f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2368420
    if-eqz v0, :cond_0

    .line 2368421
    iget-object v1, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->s:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f0813ff

    :goto_0
    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 2368422
    new-instance v1, LX/GaX;

    invoke-direct {v1, p0}, LX/GaX;-><init>(Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 2368423
    :cond_0
    const v0, 0x7f0d0446

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->G:Landroid/widget/ProgressBar;

    .line 2368424
    const v0, 0x7f0d0ab3

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/error/GenericErrorView;

    iput-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->H:Lcom/facebook/widget/error/GenericErrorView;

    .line 2368425
    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->H:Lcom/facebook/widget/error/GenericErrorView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/error/GenericErrorView;->setClickable(Z)V

    .line 2368426
    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->H:Lcom/facebook/widget/error/GenericErrorView;

    new-instance v1, LX/GaY;

    invoke-direct {v1, p0}, LX/GaY;-><init>(Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/error/GenericErrorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2368427
    return-void

    .line 2368428
    :cond_1
    const v1, 0x7f0813fe

    goto :goto_0
.end method

.method public static l(Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;)V
    .locals 7

    .prologue
    .line 2368401
    invoke-direct {p0}, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2368402
    :goto_0
    return-void

    .line 2368403
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->E:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2368404
    new-instance v0, LX/170;

    invoke-direct {v0}, LX/170;-><init>()V

    iget-object v1, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->E:Ljava/lang/String;

    .line 2368405
    iput-object v1, v0, LX/170;->o:Ljava/lang/String;

    .line 2368406
    move-object v0, v0

    .line 2368407
    invoke-virtual {v0}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    .line 2368408
    sget-object v1, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->v:LX/21D;

    const-string v2, "share_composer_from_uri"

    invoke-static {v0}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v0

    invoke-virtual {v0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    invoke-static {v1, v2, v0}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    move-object v6, v0

    .line 2368409
    :goto_1
    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->D:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2368410
    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->D:Ljava/lang/String;

    invoke-static {v0}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v0

    invoke-virtual {v0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2368411
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->F:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2368412
    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->F:Ljava/lang/String;

    invoke-direct {p0, v6, v0}, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;Ljava/lang/String;)V

    goto :goto_0

    .line 2368413
    :cond_2
    sget-object v0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->v:LX/21D;

    const-string v1, "status_composer_from_uri"

    invoke-static {v0, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    move-object v6, v0

    goto :goto_1

    .line 2368414
    :cond_3
    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->B:Ljava/lang/String;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->C:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 2368415
    :cond_4
    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->r:LX/929;

    const-string v1, "single_minutiae"

    iget-object v2, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->q:Ljava/util/concurrent/ExecutorService;

    iget-object v3, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->B:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->C:Ljava/lang/String;

    new-instance v5, LX/GaZ;

    invoke-direct {v5, p0, v6}, LX/GaZ;-><init>(Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;)V

    invoke-virtual/range {v0 .. v5}, LX/929;->a(Ljava/lang/String;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;Ljava/lang/String;LX/0Ve;)V

    goto :goto_0

    .line 2368416
    :cond_5
    invoke-static {p0, v6}, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->a$redex0(Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;)V

    goto :goto_0
.end method

.method private m()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2368395
    iget-object v2, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->D:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->E:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 2368396
    iget-object v2, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->t:LX/03V;

    sget-object v3, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->u:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Multiple exclusive parameters found in uri: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->A:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2368397
    iget-object v1, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->H:Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v1}, Lcom/facebook/widget/error/GenericErrorView;->b()V

    .line 2368398
    iget-object v1, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->G:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2368399
    iget-object v1, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->H:Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/error/GenericErrorView;->setVisibility(I)V

    .line 2368400
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2368379
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2368380
    invoke-static {p0, p0}, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2368381
    invoke-direct {p0}, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->b()V

    .line 2368382
    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2368383
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->A:Landroid/net/Uri;

    .line 2368384
    const-string v0, "minutiae"

    iget-object v1, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->A:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2368385
    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->A:Landroid/net/Uri;

    const-string v1, "og_action"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->B:Ljava/lang/String;

    .line 2368386
    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->A:Landroid/net/Uri;

    const-string v1, "og_object"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->C:Ljava/lang/String;

    .line 2368387
    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->A:Landroid/net/Uri;

    const-string v1, "link"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->D:Ljava/lang/String;

    .line 2368388
    :goto_0
    invoke-static {p0}, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->l(Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;)V

    .line 2368389
    return-void

    .line 2368390
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->A:Landroid/net/Uri;

    const-string v1, "minutiae_og_action"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->B:Ljava/lang/String;

    .line 2368391
    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->A:Landroid/net/Uri;

    const-string v1, "minutiae_og_object"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->C:Ljava/lang/String;

    .line 2368392
    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->A:Landroid/net/Uri;

    const-string v1, "link"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->D:Ljava/lang/String;

    .line 2368393
    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->A:Landroid/net/Uri;

    const-string v1, "shareid"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->E:Ljava/lang/String;

    .line 2368394
    iget-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->A:Landroid/net/Uri;

    const-string v1, "prompt_id"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->F:Ljava/lang/String;

    goto :goto_0
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x9479819

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2368375
    iget-object v1, p0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->r:LX/929;

    .line 2368376
    iget-object v2, v1, LX/929;->b:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 2368377
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 2368378
    const/16 v1, 0x23

    const v2, -0x575c9ac0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
