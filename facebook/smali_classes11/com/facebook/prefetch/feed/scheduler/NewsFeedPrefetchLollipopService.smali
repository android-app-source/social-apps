.class public Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchLollipopService;
.super Landroid/app/job/JobService;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2242097
    const-class v0, Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchLollipopService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchLollipopService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2242096
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    return-void
.end method


# virtual methods
.method public final onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 2

    .prologue
    .line 2242092
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    .line 2242093
    invoke-static {v0}, LX/FSX;->b(LX/0QB;)LX/FSX;

    move-result-object v0

    check-cast v0, LX/FSX;

    .line 2242094
    invoke-virtual {v0}, LX/FSX;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/FSY;

    invoke-direct {v1, p0, p1}, LX/FSY;-><init>(Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchLollipopService;Landroid/app/job/JobParameters;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2242095
    const/4 v0, 0x1

    return v0
.end method

.method public final onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 1

    .prologue
    .line 2242091
    const/4 v0, 0x1

    return v0
.end method
