.class public Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchGCMService;
.super LX/2fD;
.source ""


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FSX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2242072
    const-class v0, Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchGCMService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchGCMService;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2242071
    invoke-direct {p0}, LX/2fD;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchGCMService;

    const/16 v1, 0x2fb5

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchGCMService;->a:LX/0Or;

    return-void
.end method


# virtual methods
.method public final a(LX/2fH;)I
    .locals 1

    .prologue
    .line 2242068
    iget-object v0, p0, Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchGCMService;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FSX;

    .line 2242069
    invoke-virtual {v0}, LX/FSX;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2242070
    const/4 v0, 0x0

    return v0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x2504cea

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2242058
    invoke-super {p0}, LX/2fD;->onCreate()V

    .line 2242059
    invoke-static {p0, p0}, Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchGCMService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2242060
    const/16 v1, 0x25

    const v2, -0x66488104

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, -0x22d302e8

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2242061
    if-nez p1, :cond_0

    .line 2242062
    :try_start_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v3, "Received a null intent, did you ever return START_STICKY?"

    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x2

    const/16 v4, 0x25

    const v5, 0x444062f

    invoke-static {v3, v4, v5, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2242063
    :catch_0
    move-exception v1

    .line 2242064
    sget-object v3, Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchGCMService;->b:Ljava/lang/String;

    const-string v4, "Unexpected service start parameters"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v1, v4, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2242065
    invoke-virtual {p0, p3}, Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchGCMService;->stopSelf(I)V

    .line 2242066
    const v1, -0x6ab962f1

    invoke-static {v1, v2}, LX/02F;->d(II)V

    :goto_0
    return v0

    .line 2242067
    :cond_0
    :try_start_1
    invoke-super {p0, p1, p2, p3}, LX/2fD;->onStartCommand(Landroid/content/Intent;II)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    const v1, 0x6be39175

    invoke-static {v1, v2}, LX/02F;->d(II)V

    goto :goto_0
.end method
