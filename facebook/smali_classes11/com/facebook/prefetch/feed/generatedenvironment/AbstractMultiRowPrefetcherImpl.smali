.class public abstract Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pt;
.implements LX/1Pu;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1Rb;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/1HI;

.field private final c:LX/1BF;

.field private d:LX/1Rb;


# direct methods
.method public constructor <init>(LX/1HI;LX/1BF;)V
    .locals 1

    .prologue
    .line 2241801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2241802
    iput-object p1, p0, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->b:LX/1HI;

    .line 2241803
    iput-object p2, p0, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->c:LX/1BF;

    .line 2241804
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->a:Ljava/util/Map;

    .line 2241805
    return-void
.end method

.method public static a$redex0(Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 2241799
    iget-object v0, p0, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->c:LX/1BF;

    invoke-virtual {v0, p1, p2}, LX/1BF;->b(Ljava/lang/String;Z)V

    .line 2241800
    return-void
.end method

.method private b(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 6

    .prologue
    .line 2241786
    invoke-virtual {p0}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->o()LX/1Rb;

    move-result-object v1

    .line 2241787
    const/4 v0, 0x0

    .line 2241788
    if-eqz v1, :cond_0

    .line 2241789
    iget-object v0, p0, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2241790
    :cond_0
    iget-object v1, p1, LX/1bf;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 2241791
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2241792
    if-eqz v0, :cond_1

    .line 2241793
    iget-object v2, p0, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->c:LX/1BF;

    invoke-virtual {v2, v0, v1}, LX/1BF;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2241794
    :cond_1
    const-string v0, "prefetch_newsfeed_multirow_in_background"

    invoke-virtual {p0}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->b()Ljava/lang/String;

    move-result-object v2

    .line 2241795
    new-instance v3, Lcom/facebook/common/callercontext/CallerContext;

    iget-object v4, p2, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    iget-object v5, p2, Lcom/facebook/common/callercontext/CallerContext;->d:Ljava/lang/String;

    invoke-direct {v3, v4, v0, v2, v5}, Lcom/facebook/common/callercontext/CallerContext;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v3

    .line 2241796
    iget-object v2, p0, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->b:LX/1HI;

    invoke-virtual {v2, p1, v0}, LX/1HI;->e(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 2241797
    new-instance v2, LX/FSL;

    invoke-direct {v2, p0, v1}, LX/FSL;-><init>(Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;Ljava/lang/String;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v1

    invoke-interface {v0, v2, v1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 2241798
    return-void
.end method


# virtual methods
.method public final a(LX/1Rb;)V
    .locals 0

    .prologue
    .line 2241784
    iput-object p1, p0, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->d:LX/1Rb;

    .line 2241785
    return-void
.end method

.method public final a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0

    .prologue
    .line 2241782
    invoke-direct {p0, p1, p2}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->b(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2241783
    return-void
.end method

.method public final a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0

    .prologue
    .line 2241780
    invoke-direct {p0, p2, p3}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->b(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2241781
    return-void
.end method

.method public final a(Ljava/lang/String;LX/1Rb;)V
    .locals 1

    .prologue
    .line 2241778
    iget-object v0, p0, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->a:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2241779
    return-void
.end method

.method public abstract a()Z
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2241776
    iget-object v0, p0, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2241777
    return-void
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2241775
    invoke-virtual {p0}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->a()Z

    move-result v0

    return v0
.end method

.method public final m()LX/1f9;
    .locals 1

    .prologue
    .line 2241774
    const/4 v0, 0x0

    return-object v0
.end method

.method public final o()LX/1Rb;
    .locals 1

    .prologue
    .line 2241773
    iget-object v0, p0, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->d:LX/1Rb;

    return-object v0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 2241770
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->d:LX/1Rb;

    .line 2241771
    return-void
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 2241772
    const/4 v0, 0x1

    return v0
.end method
