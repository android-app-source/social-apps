.class public final Lcom/facebook/videocodec/trimmer/VideoTrimmer$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/G5p;

.field public final synthetic b:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic c:LX/G5o;


# direct methods
.method public constructor <init>(LX/G5o;LX/G5p;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 2319374
    iput-object p1, p0, Lcom/facebook/videocodec/trimmer/VideoTrimmer$1;->c:LX/G5o;

    iput-object p2, p0, Lcom/facebook/videocodec/trimmer/VideoTrimmer$1;->a:LX/G5p;

    iput-object p3, p0, Lcom/facebook/videocodec/trimmer/VideoTrimmer$1;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2319375
    iget-object v0, p0, Lcom/facebook/videocodec/trimmer/VideoTrimmer$1;->c:LX/G5o;

    iget-object v0, v0, LX/G5o;->b:LX/0Sj;

    const-string v1, "VideoTrimmer"

    const-string v2, "start"

    invoke-interface {v0, v1, v2}, LX/0Sj;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0cV;

    move-result-object v1

    .line 2319376
    if-eqz v1, :cond_0

    .line 2319377
    invoke-interface {v1}, LX/0cV;->a()V

    .line 2319378
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/videocodec/trimmer/VideoTrimmer$1;->c:LX/G5o;

    iget-object v0, v0, LX/G5o;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G5n;

    .line 2319379
    iget-object v2, p0, Lcom/facebook/videocodec/trimmer/VideoTrimmer$1;->a:LX/G5p;

    invoke-virtual {v0, v2}, LX/G5n;->a(LX/G5p;)V

    .line 2319380
    iget-object v0, p0, Lcom/facebook/videocodec/trimmer/VideoTrimmer$1;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v2, 0x0

    const v3, 0x54518e8a

    invoke-static {v0, v2, v3}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2319381
    if-eqz v1, :cond_1

    .line 2319382
    invoke-interface {v1}, LX/0cV;->b()V

    .line 2319383
    :cond_1
    :goto_0
    return-void

    .line 2319384
    :catch_0
    move-exception v0

    .line 2319385
    :try_start_1
    iget-object v2, p0, Lcom/facebook/videocodec/trimmer/VideoTrimmer$1;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v2, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2319386
    if-eqz v1, :cond_1

    .line 2319387
    invoke-interface {v1}, LX/0cV;->b()V

    goto :goto_0

    .line 2319388
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 2319389
    invoke-interface {v1}, LX/0cV;->b()V

    :cond_2
    throw v0
.end method
