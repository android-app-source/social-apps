.class public final Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7e481a07
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J

.field private g:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2253853
    const-class v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2253852
    const-class v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2253850
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2253851
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 17

    .prologue
    .line 2253825
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2253826
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->a()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x469fd521

    invoke-static {v3, v2, v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2253827
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->k()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2253828
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2253829
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->m()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 2253830
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->n()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 2253831
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->o()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 2253832
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->p()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x7dc072ad

    invoke-static {v4, v3, v5}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 2253833
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->q()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x439668bc

    invoke-static {v4, v3, v5}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 2253834
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->r()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 2253835
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->s()Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v16

    .line 2253836
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2253837
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 2253838
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->f:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2253839
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2253840
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2253841
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2253842
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2253843
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2253844
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2253845
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 2253846
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2253847
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2253848
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2253849
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2253778
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2253779
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2253780
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x469fd521

    invoke-static {v2, v0, v3}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2253781
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2253782
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;

    .line 2253783
    iput v3, v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->e:I

    move-object v1, v0

    .line 2253784
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->k()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2253785
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->k()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;

    .line 2253786
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->k()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2253787
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;

    .line 2253788
    iput-object v0, v1, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->g:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;

    .line 2253789
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2253790
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2253791
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2253792
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;

    .line 2253793
    iput-object v0, v1, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2253794
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->m()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2253795
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->m()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;

    .line 2253796
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->m()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2253797
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;

    .line 2253798
    iput-object v0, v1, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->i:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;

    .line 2253799
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->n()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2253800
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->n()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;

    .line 2253801
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->n()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 2253802
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;

    .line 2253803
    iput-object v0, v1, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->j:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;

    .line 2253804
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->o()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2253805
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->o()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    .line 2253806
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->o()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 2253807
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;

    .line 2253808
    iput-object v0, v1, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->k:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    .line 2253809
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->p()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    .line 2253810
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->p()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x7dc072ad

    invoke-static {v2, v0, v3}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2253811
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->p()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2253812
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;

    .line 2253813
    iput v3, v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->l:I

    move-object v1, v0

    .line 2253814
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->q()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_7

    .line 2253815
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->q()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x439668bc

    invoke-static {v2, v0, v3}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2253816
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->q()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2253817
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;

    .line 2253818
    iput v3, v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->m:I

    move-object v1, v0

    .line 2253819
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2253820
    if-nez v1, :cond_8

    :goto_0
    return-object p0

    .line 2253821
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 2253822
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 2253823
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_8
    move-object p0, v1

    .line 2253824
    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAttributionText"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2253776
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2253777
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2253770
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2253771
    const/4 v0, 0x0

    const v1, 0x469fd521

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->e:I

    .line 2253772
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->f:J

    .line 2253773
    const/4 v0, 0x7

    const v1, 0x7dc072ad

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->l:I

    .line 2253774
    const/16 v0, 0x8

    const v1, 0x439668bc

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->m:I

    .line 2253775
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2253767
    new-instance v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;

    invoke-direct {v0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;-><init>()V

    .line 2253768
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2253769
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2253766
    const v0, 0x6533a2ed

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2253854
    const v0, -0x939b30f

    return v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 2253746
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2253747
    iget-wide v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->f:J

    return-wide v0
.end method

.method public final k()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGlobalShare"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2253750
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->g:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;

    iput-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->g:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;

    .line 2253751
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->g:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2253752
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2253753
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final m()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2253748
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->i:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;

    iput-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->i:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;

    .line 2253749
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->i:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;

    return-object v0
.end method

.method public final n()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2253754
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->j:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;

    iput-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->j:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;

    .line 2253755
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->j:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;

    return-object v0
.end method

.method public final o()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSourceContainer"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2253756
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->k:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    iput-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->k:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    .line 2253757
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->k:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    return-object v0
.end method

.method public final p()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSubtitleText"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2253758
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2253759
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->l:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final q()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2253760
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2253761
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->m:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2253762
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->n:Ljava/lang/String;

    .line 2253763
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2253764
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->o:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    iput-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->o:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    .line 2253765
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->o:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    return-object v0
.end method
