.class public final Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x24b835b5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2253444
    const-class v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2253443
    const-class v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2253415
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2253416
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2253441
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;->e:Ljava/lang/String;

    .line 2253442
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2253433
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2253434
    invoke-direct {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2253435
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;->j()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2253436
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2253437
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2253438
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2253439
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2253440
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2253425
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2253426
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;->j()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2253427
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;->j()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;

    .line 2253428
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;->j()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2253429
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;

    .line 2253430
    iput-object v0, v1, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;->f:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;

    .line 2253431
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2253432
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2253424
    invoke-direct {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2253421
    new-instance v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;

    invoke-direct {v0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;-><init>()V

    .line 2253422
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2253423
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2253420
    const v0, 0x33fea940

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2253419
    const v0, 0x1eaef984

    return v0
.end method

.method public final j()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getInstantArticle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2253417
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;->f:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;

    iput-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;->f:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;

    .line 2253418
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;->f:Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;

    return-object v0
.end method
