.class public final Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2252961
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2252962
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2252959
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2252960
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 2252908
    if-nez p1, :cond_0

    .line 2252909
    :goto_0
    return v0

    .line 2252910
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2252911
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2252912
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 2252913
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2252914
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 2252915
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2252916
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2252917
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2252918
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2252919
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2252920
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2252921
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2252922
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2252923
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2252924
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2252925
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2252926
    :sswitch_3
    invoke-virtual {p0, p1, v3}, LX/15i;->p(II)I

    move-result v0

    .line 2252927
    const v1, -0x1062c448

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2252928
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2252929
    invoke-virtual {p3, v3, v0}, LX/186;->b(II)V

    .line 2252930
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2252931
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2252932
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2252933
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2252934
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2252935
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2252936
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2252937
    invoke-virtual {p3, v3, v2}, LX/186;->b(II)V

    .line 2252938
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2252939
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2252940
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2252941
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2252942
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2252943
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2252944
    :sswitch_6
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2252945
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2252946
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2252947
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2252948
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2252949
    :sswitch_7
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2252950
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2252951
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2252952
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2252953
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2252954
    :sswitch_8
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2252955
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2252956
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2252957
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2252958
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x76a61799 -> :sswitch_3
        -0x356414ee -> :sswitch_0
        -0x1062c448 -> :sswitch_4
        -0x93e7346 -> :sswitch_8
        0x21b6fcd1 -> :sswitch_5
        0x33a76f0d -> :sswitch_2
        0x439668bc -> :sswitch_7
        0x469fd521 -> :sswitch_1
        0x7dc072ad -> :sswitch_6
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2252907
    new-instance v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2252902
    sparse-switch p2, :sswitch_data_0

    .line 2252903
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2252904
    :sswitch_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2252905
    const v1, -0x1062c448

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2252906
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x76a61799 -> :sswitch_0
        -0x356414ee -> :sswitch_1
        -0x1062c448 -> :sswitch_1
        -0x93e7346 -> :sswitch_1
        0x21b6fcd1 -> :sswitch_1
        0x33a76f0d -> :sswitch_1
        0x439668bc -> :sswitch_1
        0x469fd521 -> :sswitch_1
        0x7dc072ad -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2252896
    if-eqz p1, :cond_0

    .line 2252897
    invoke-static {p0, p1, p2}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 2252898
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;

    .line 2252899
    if-eq v0, v1, :cond_0

    .line 2252900
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2252901
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2252895
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2252892
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2252893
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2252963
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2252964
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2252965
    :cond_0
    iput-object p1, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2252966
    iput p2, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;->b:I

    .line 2252967
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2252894
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2252891
    new-instance v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2252888
    iget v0, p0, LX/1vt;->c:I

    .line 2252889
    move v0, v0

    .line 2252890
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2252885
    iget v0, p0, LX/1vt;->c:I

    .line 2252886
    move v0, v0

    .line 2252887
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2252882
    iget v0, p0, LX/1vt;->b:I

    .line 2252883
    move v0, v0

    .line 2252884
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2252879
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2252880
    move-object v0, v0

    .line 2252881
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2252870
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2252871
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2252872
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2252873
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2252874
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2252875
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2252876
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2252877
    invoke-static {v3, v9, v2}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2252878
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2252867
    iget v0, p0, LX/1vt;->c:I

    .line 2252868
    move v0, v0

    .line 2252869
    return v0
.end method
