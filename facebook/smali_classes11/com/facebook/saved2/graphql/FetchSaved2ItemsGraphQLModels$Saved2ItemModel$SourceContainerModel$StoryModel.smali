.class public final Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$StoryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$StoryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$StoryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2253679
    const-class v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$StoryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2253678
    const-class v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$StoryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2253676
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2253677
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2253670
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2253671
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$StoryModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2253672
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2253673
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2253674
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2253675
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2253667
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2253668
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2253669
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2253655
    new-instance v0, LX/FX4;

    invoke-direct {v0, p1}, LX/FX4;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2253666
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$StoryModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2253664
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2253665
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2253663
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2253660
    new-instance v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$StoryModel;

    invoke-direct {v0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$StoryModel;-><init>()V

    .line 2253661
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2253662
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2253659
    const v0, 0x199c4b7e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2253658
    const v0, 0x4c808d5

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2253656
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$StoryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$StoryModel;->e:Ljava/lang/String;

    .line 2253657
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$StoryModel;->e:Ljava/lang/String;

    return-object v0
.end method
