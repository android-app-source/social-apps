.class public final Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x74462335
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemsEdgeModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2253023
    const-class v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2253024
    const-class v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2253025
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2253026
    return-void
.end method

.method private j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2253035
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2253036
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2253027
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2253028
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2253029
    invoke-direct {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x356414ee    # -5109129.0f

    invoke-static {v2, v1, v3}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2253030
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2253031
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2253032
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2253033
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2253034
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEdges"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemsEdgeModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2253021
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemsEdgeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;->e:Ljava/util/List;

    .line 2253022
    iget-object v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2252998
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2252999
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2253000
    invoke-virtual {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2253001
    if-eqz v1, :cond_2

    .line 2253002
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;

    .line 2253003
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 2253004
    :goto_0
    invoke-direct {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2253005
    invoke-direct {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x356414ee    # -5109129.0f

    invoke-static {v2, v0, v3}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2253006
    invoke-direct {p0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2253007
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;

    .line 2253008
    iput v3, v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;->f:I

    move-object v1, v0

    .line 2253009
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2253010
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    .line 2253011
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move-object p0, v1

    .line 2253012
    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2253018
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2253019
    const/4 v0, 0x1

    const v1, -0x356414ee    # -5109129.0f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;->f:I

    .line 2253020
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2253015
    new-instance v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;

    invoke-direct {v0}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;-><init>()V

    .line 2253016
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2253017
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2253014
    const v0, -0x2277076b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2253013
    const v0, -0x3a56723e

    return v0
.end method
