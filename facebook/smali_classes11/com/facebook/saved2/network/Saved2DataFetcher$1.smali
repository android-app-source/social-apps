.class public final Lcom/facebook/saved2/network/Saved2DataFetcher$1;
.super Ljava/lang/Thread;
.source ""


# instance fields
.field public final synthetic a:LX/FXN;


# direct methods
.method public constructor <init>(LX/FXN;)V
    .locals 0

    .prologue
    .line 2254859
    iput-object p1, p0, Lcom/facebook/saved2/network/Saved2DataFetcher$1;->a:LX/FXN;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 2254860
    iget-object v1, p0, Lcom/facebook/saved2/network/Saved2DataFetcher$1;->a:LX/FXN;

    iget-object v1, v1, LX/FXN;->d:LX/Eke;

    invoke-virtual {v1}, LX/Eke;->a()LX/Ekd;

    move-result-object v3

    .line 2254861
    new-instance v1, LX/BOE;

    invoke-direct {v1}, LX/BOE;-><init>()V

    invoke-virtual {v3, v1}, LX/Ekd;->a(LX/AU6;)LX/EkY;

    move-result-object v4

    .line 2254862
    :try_start_0
    iget-object v1, p0, Lcom/facebook/saved2/network/Saved2DataFetcher$1;->a:LX/FXN;

    iget-object v1, v1, LX/FXN;->g:LX/1Sd;

    invoke-virtual {v1}, LX/1Sd;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "unread_count"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2254863
    iget-object v1, p0, Lcom/facebook/saved2/network/Saved2DataFetcher$1;->a:LX/FXN;

    iget-object v1, v1, LX/FXN;->c:LX/79a;

    invoke-virtual {v1}, LX/79a;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v0

    :goto_0
    if-ge v2, v6, :cond_0

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79Y;

    .line 2254864
    invoke-virtual {v4}, LX/EkY;->a()LX/AU2;

    move-result-object v1

    check-cast v1, LX/BOC;

    .line 2254865
    iget-object v7, v0, LX/79Y;->a:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-object v0, v7

    .line 2254866
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, LX/BOC;->a(Ljava/lang/String;)LX/BOC;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, LX/BOC;->a(Ljava/lang/Integer;)LX/BOC;

    move-result-object v0

    invoke-interface {v0}, LX/AU2;->a()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2254867
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2254868
    :cond_0
    invoke-virtual {v3}, LX/Ekd;->a()V

    .line 2254869
    iget-object v0, p0, Lcom/facebook/saved2/network/Saved2DataFetcher$1;->a:LX/FXN;

    iget-object v0, v0, LX/FXN;->f:LX/FWt;

    invoke-virtual {v0}, LX/FWt;->i()V

    .line 2254870
    return-void

    .line 2254871
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, LX/Ekd;->a()V

    .line 2254872
    iget-object v1, p0, Lcom/facebook/saved2/network/Saved2DataFetcher$1;->a:LX/FXN;

    iget-object v1, v1, LX/FXN;->f:LX/FWt;

    invoke-virtual {v1}, LX/FWt;->i()V

    throw v0
.end method
