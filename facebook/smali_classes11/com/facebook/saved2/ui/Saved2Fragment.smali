.class public Lcom/facebook/saved2/ui/Saved2Fragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public a:LX/FXs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/FWt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/AUH;
    .annotation runtime Lcom/facebook/saved2/ui/Saved2QueryExecutor;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/FXx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/16H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1Sk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/FWS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0kb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0tQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BUA;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Yb;

.field public p:LX/AUC;

.field public q:LX/FXk;

.field public r:Landroid/support/v4/view/ViewPager;

.field public s:LX/FXw;

.field public t:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field public u:Z

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final x:LX/AUK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/AUK",
            "<",
            "LX/FWv;",
            ">;"
        }
    .end annotation
.end field

.field private final y:LX/AUK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/AUK",
            "<",
            "LX/FWu;",
            ">;"
        }
    .end annotation
.end field

.field private final z:LX/0hc;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2255529
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2255530
    new-instance v0, LX/FXn;

    invoke-direct {v0, p0}, LX/FXn;-><init>(Lcom/facebook/saved2/ui/Saved2Fragment;)V

    iput-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->x:LX/AUK;

    .line 2255531
    new-instance v0, LX/FXo;

    invoke-direct {v0, p0}, LX/FXo;-><init>(Lcom/facebook/saved2/ui/Saved2Fragment;)V

    iput-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->y:LX/AUK;

    .line 2255532
    new-instance v0, LX/FXp;

    invoke-direct {v0, p0}, LX/FXp;-><init>(Lcom/facebook/saved2/ui/Saved2Fragment;)V

    iput-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->z:LX/0hc;

    return-void
.end method

.method private static a(Lcom/facebook/saved2/ui/Saved2Fragment;LX/FXs;LX/FWt;LX/AUH;LX/0kL;LX/FXx;LX/16H;LX/1Sk;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/FWS;LX/0kb;LX/0Xl;LX/0Or;LX/0tQ;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/saved2/ui/Saved2Fragment;",
            "LX/FXs;",
            "LX/FWt;",
            "LX/AUH;",
            "LX/0kL;",
            "LX/FXx;",
            "LX/16H;",
            "LX/1Sk;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/FWS;",
            "LX/0kb;",
            "LX/0Xl;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/0tQ;",
            "LX/0Ot",
            "<",
            "LX/BUA;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2255445
    iput-object p1, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->a:LX/FXs;

    iput-object p2, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->b:LX/FWt;

    iput-object p3, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->c:LX/AUH;

    iput-object p4, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->d:LX/0kL;

    iput-object p5, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->e:LX/FXx;

    iput-object p6, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->f:LX/16H;

    iput-object p7, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->g:LX/1Sk;

    iput-object p8, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p9, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->i:LX/FWS;

    iput-object p10, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->j:LX/0kb;

    iput-object p11, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->k:LX/0Xl;

    iput-object p12, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->l:LX/0Or;

    iput-object p13, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->m:LX/0tQ;

    iput-object p14, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->n:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 16

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v14

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/saved2/ui/Saved2Fragment;

    invoke-static {v14}, LX/FXs;->b(LX/0QB;)LX/FXs;

    move-result-object v1

    check-cast v1, LX/FXs;

    invoke-static {v14}, LX/FWt;->a(LX/0QB;)LX/FWt;

    move-result-object v2

    check-cast v2, LX/FWt;

    invoke-static {v14}, LX/FXl;->a(LX/0QB;)LX/AUH;

    move-result-object v3

    check-cast v3, LX/AUH;

    invoke-static {v14}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v4

    check-cast v4, LX/0kL;

    const-class v5, LX/FXx;

    invoke-interface {v14, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/FXx;

    invoke-static {v14}, LX/16H;->a(LX/0QB;)LX/16H;

    move-result-object v6

    check-cast v6, LX/16H;

    invoke-static {v14}, LX/1Sk;->a(LX/0QB;)LX/1Sk;

    move-result-object v7

    check-cast v7, LX/1Sk;

    invoke-static {v14}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-class v9, LX/FWS;

    invoke-interface {v14, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/FWS;

    invoke-static {v14}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v10

    check-cast v10, LX/0kb;

    invoke-static {v14}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v11

    check-cast v11, LX/0Xl;

    const/16 v12, 0x122d

    invoke-static {v14, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static {v14}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v13

    check-cast v13, LX/0tQ;

    const/16 v15, 0x37e9

    invoke-static {v14, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static/range {v0 .. v14}, Lcom/facebook/saved2/ui/Saved2Fragment;->a(Lcom/facebook/saved2/ui/Saved2Fragment;LX/FXs;LX/FWt;LX/AUH;LX/0kL;LX/FXx;LX/16H;LX/1Sk;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/FWS;LX/0kb;LX/0Xl;LX/0Or;LX/0tQ;LX/0Ot;)V

    return-void
.end method

.method public static e(Lcom/facebook/saved2/ui/Saved2Fragment;)V
    .locals 5

    .prologue
    .line 2255520
    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->r:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->s:LX/FXw;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    if-lez v0, :cond_0

    .line 2255521
    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->v:Ljava/lang/String;

    .line 2255522
    iget-object v1, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->s:LX/FXw;

    invoke-virtual {v1}, LX/FXP;->e()LX/AU0;

    move-result-object v1

    check-cast v1, LX/BOG;

    .line 2255523
    invoke-static {v1, v0}, LX/FXQ;->a(LX/BOG;Ljava/lang/String;)I

    move-result v1

    .line 2255524
    if-gez v1, :cond_1

    .line 2255525
    const-string v1, "Saved2Fragment"

    const-string v2, "Cannot display requested section \'%s\': not found"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2255526
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->v:Ljava/lang/String;

    .line 2255527
    :cond_0
    return-void

    .line 2255528
    :cond_1
    iget-object v2, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->r:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2255476
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2255477
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/saved2/ui/Saved2Fragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2255478
    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->b:LX/FWt;

    .line 2255479
    iget-object v1, v0, LX/FWt;->a:LX/11i;

    sget-object v2, LX/FWs;->a:LX/FWr;

    invoke-interface {v1, v2}, LX/11i;->a(LX/0Pq;)LX/11o;

    .line 2255480
    const-string v1, "DASH_START"

    invoke-static {v0, v1}, LX/FWt;->a(LX/FWt;Ljava/lang/String;)V

    .line 2255481
    new-instance v0, LX/AUC;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getLoaderManager()LX/0k5;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->c:LX/AUH;

    new-instance v3, LX/14r;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, LX/14r;-><init>(I)V

    invoke-direct {v0, v1, v2, v3}, LX/AUC;-><init>(LX/0k5;LX/AU8;LX/14r;)V

    iput-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->p:LX/AUC;

    .line 2255482
    new-instance v0, LX/FXk;

    iget-object v1, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->p:LX/AUC;

    iget-object v2, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->a:LX/FXs;

    iget-object v3, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->b:LX/FWt;

    iget-object v4, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v5, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->d:LX/0kL;

    invoke-direct/range {v0 .. v5}, LX/FXk;-><init>(LX/AUC;LX/FXs;LX/FWt;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0kL;)V

    iput-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->q:LX/FXk;

    .line 2255483
    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->e:LX/FXx;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->q:LX/FXk;

    .line 2255484
    new-instance v4, LX/FXw;

    const-class v3, LX/FYj;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/FYj;

    invoke-direct {v4, v1, v2, v3}, LX/FXw;-><init>(Landroid/support/v4/app/FragmentActivity;LX/FXk;LX/FYj;)V

    .line 2255485
    move-object v0, v4

    .line 2255486
    iput-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->s:LX/FXw;

    .line 2255487
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2255488
    const-string v1, "extra_section_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2255489
    if-eqz p1, :cond_0

    .line 2255490
    const-string v1, "extra_section_name"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2255491
    :cond_0
    if-eqz v1, :cond_4

    :goto_0
    move-object v0, v1

    .line 2255492
    iput-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->v:Ljava/lang/String;

    .line 2255493
    if-nez p1, :cond_1

    .line 2255494
    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->f:LX/16H;

    .line 2255495
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2255496
    const-string v2, "extra_referer"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->v:Ljava/lang/String;

    .line 2255497
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "saved_dashboard_imp"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "saved_dashboard"

    .line 2255498
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2255499
    move-object v3, v3

    .line 2255500
    const-string v4, "referrer_name"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "section_type"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "event_id"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2255501
    iget-object v4, v0, LX/16H;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2255502
    :cond_1
    sget-object v0, LX/FWw;->a:LX/AUg;

    iget-object v1, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->y:LX/AUK;

    invoke-virtual {v0, v1}, LX/AUg;->a(LX/AUK;)I

    .line 2255503
    sget-object v0, LX/FWw;->b:LX/AUg;

    iget-object v1, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->x:LX/AUK;

    invoke-virtual {v0, v1}, LX/AUg;->a(LX/AUK;)I

    .line 2255504
    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->b:LX/FWt;

    invoke-virtual {v0}, LX/FWt;->f()V

    .line 2255505
    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->p:LX/AUC;

    new-instance v1, LX/BOH;

    invoke-direct {v1}, LX/BOH;-><init>()V

    new-instance v2, LX/FXm;

    invoke-direct {v2, p0}, LX/FXm;-><init>(Lcom/facebook/saved2/ui/Saved2Fragment;)V

    invoke-virtual {v0, v1, v2}, LX/AUC;->a(LX/AU9;LX/AUN;)I

    .line 2255506
    iget-object v1, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->q:LX/FXk;

    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->v:Ljava/lang/String;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/FXk;->a(Ljava/lang/String;)LX/FXj;

    move-result-object v0

    invoke-virtual {v0}, LX/FXj;->b()V

    .line 2255507
    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->m:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->B()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2255508
    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BUA;

    invoke-virtual {v0}, LX/BUA;->b()V

    .line 2255509
    :cond_2
    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->i:LX/FWS;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/FWS;->a(Landroid/content/Context;)LX/FWR;

    move-result-object v0

    invoke-virtual {v0}, LX/FWR;->a()V

    .line 2255510
    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->k:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    invoke-interface {v0, v1, p0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->o:LX/0Yb;

    .line 2255511
    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->o:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2255512
    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->m:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2255513
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2255514
    const-string v1, "extra_referer"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->VIDEO_DOWNLOAD_LOCAL_PUSH_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2255515
    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v1, "1288475134498479"

    .line 2255516
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 2255517
    move-object v0, v0

    .line 2255518
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gt;->a(Landroid/content/Context;)V

    .line 2255519
    :cond_3
    return-void

    :cond_4
    const-string v1, "ALL"

    goto/16 :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x44a13f81

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2255475
    const v1, 0x7f03124f

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x112bda16

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1490a3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2255469
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2255470
    sget-object v1, LX/FWw;->a:LX/AUg;

    iget-object v2, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->y:LX/AUK;

    invoke-virtual {v1, v2}, LX/AUg;->b(LX/AUK;)I

    .line 2255471
    sget-object v1, LX/FWw;->b:LX/AUg;

    iget-object v2, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->x:LX/AUK;

    invoke-virtual {v1, v2}, LX/AUg;->b(LX/AUK;)I

    .line 2255472
    iget-object v1, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->g:LX/1Sk;

    invoke-virtual {v1}, LX/1Sk;->c()V

    .line 2255473
    iget-object v1, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->o:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2255474
    const/16 v1, 0x2b

    const v2, -0x6dba2f12

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, -0x2098eb0d

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2255464
    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->j:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->u:Z

    if-eqz v0, :cond_0

    .line 2255465
    iget-object v2, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->a:LX/FXs;

    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ALL"

    :goto_0
    const/4 v3, 0x0

    .line 2255466
    invoke-static {v2, v0, v3}, LX/FXs;->d(LX/FXs;Ljava/lang/String;LX/FXS;)V

    .line 2255467
    :cond_0
    const v0, 0x7e8ff6ec

    invoke-static {v0, v1}, LX/02F;->e(II)V

    return-void

    .line 2255468
    :cond_1
    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->w:Ljava/lang/String;

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2255461
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2255462
    const-string v0, "extra_section_name"

    iget-object v1, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->w:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2255463
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1ae82937

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2255455
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2255456
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2255457
    if-eqz v0, :cond_0

    .line 2255458
    const v2, 0x7f081a8e

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2255459
    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/1ZF;->k_(Z)V

    .line 2255460
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x6e8ab92f

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2255446
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2255447
    const v0, 0x7f0d2b0e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->r:Landroid/support/v4/view/ViewPager;

    .line 2255448
    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->r:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->s:LX/FXw;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2255449
    const v0, 0x7f0d2b0d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->t:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2255450
    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->t:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->r:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2255451
    iget-object v0, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->t:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/saved2/ui/Saved2Fragment;->z:LX/0hc;

    .line 2255452
    iput-object v1, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 2255453
    invoke-static {p0}, Lcom/facebook/saved2/ui/Saved2Fragment;->e(Lcom/facebook/saved2/ui/Saved2Fragment;)V

    .line 2255454
    return-void
.end method
