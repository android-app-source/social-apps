.class public final Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/FYI;


# direct methods
.method public constructor <init>(LX/FYI;JLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2255927
    iput-object p1, p0, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;->d:LX/FYI;

    iput-wide p2, p0, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;->a:J

    iput-object p4, p0, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;->b:Ljava/lang/String;

    iput-object p5, p0, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    .line 2255928
    iget-object v0, p0, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;->d:LX/FYI;

    iget-wide v2, p0, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;->a:J

    iget-object v1, p0, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;->b:Ljava/lang/String;

    .line 2255929
    const-string v4, "ARCHIVED"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2255930
    iget-object v4, v0, LX/FYI;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Sc;

    invoke-virtual {v4, v2, v3, v1}, LX/1Sc;->a(JLjava/lang/String;)V

    .line 2255931
    :goto_0
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1$1;

    invoke-direct {v1, p0}, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1$1;-><init>(Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;)V

    const v2, -0x2e7e726

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2255932
    return-void

    .line 2255933
    :cond_0
    iget-object v4, v0, LX/FYI;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Sc;

    const/4 v7, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 2255934
    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "is_unarchived_client"

    aput-object v6, v5, v9

    const-string v6, "saved_state"

    aput-object v6, v5, v8

    const-string v6, "time_saved_ms"

    aput-object v6, v5, v10

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLSavedState;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    iget-object v7, v4, LX/1Sc;->c:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v4, v2, v3, v5, v6}, LX/1Sc;->a(LX/1Sc;J[Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2255935
    goto :goto_0
.end method
