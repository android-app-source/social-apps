.class public Lcom/facebook/react/views/scroll/ReactScrollViewManager;
.super Lcom/facebook/react/uimanager/ViewGroupManager;
.source ""

# interfaces
.implements LX/FUp;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RCTScrollView"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/ViewGroupManager",
        "<",
        "LX/FUq;",
        ">;",
        "LX/FUp",
        "<",
        "LX/FUq;",
        ">;"
    }
.end annotation


# instance fields
.field private b:LX/F6L;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2250172
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/react/views/scroll/ReactScrollViewManager;-><init>(LX/F6L;)V

    .line 2250173
    return-void
.end method

.method private constructor <init>(LX/F6L;)V
    .locals 1
    .param p1    # LX/F6L;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2250174
    invoke-direct {p0}, Lcom/facebook/react/uimanager/ViewGroupManager;-><init>()V

    .line 2250175
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/react/views/scroll/ReactScrollViewManager;->b:LX/F6L;

    .line 2250176
    iput-object p1, p0, Lcom/facebook/react/views/scroll/ReactScrollViewManager;->b:LX/F6L;

    .line 2250177
    return-void
.end method

.method public static A()Ljava/util/Map;
    .locals 4

    .prologue
    .line 2250178
    invoke-static {}, LX/5ps;->b()LX/5pr;

    move-result-object v0

    sget-object v1, LX/FUv;->SCROLL:LX/FUv;

    invoke-virtual {v1}, LX/FUv;->getJSEventName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "registrationName"

    const-string v3, "onScroll"

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    sget-object v1, LX/FUv;->BEGIN_DRAG:LX/FUv;

    invoke-virtual {v1}, LX/FUv;->getJSEventName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "registrationName"

    const-string v3, "onScrollBeginDrag"

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    sget-object v1, LX/FUv;->END_DRAG:LX/FUv;

    invoke-virtual {v1}, LX/FUv;->getJSEventName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "registrationName"

    const-string v3, "onScrollEndDrag"

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    sget-object v1, LX/FUv;->ANIMATION_END:LX/FUv;

    invoke-virtual {v1}, LX/FUv;->getJSEventName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "registrationName"

    const-string v3, "onScrollAnimationEnd"

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    sget-object v1, LX/FUv;->MOMENTUM_BEGIN:LX/FUv;

    invoke-virtual {v1}, LX/FUv;->getJSEventName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "registrationName"

    const-string v3, "onMomentumScrollBegin"

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    sget-object v1, LX/FUv;->MOMENTUM_END:LX/FUv;

    invoke-virtual {v1}, LX/FUv;->getJSEventName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "registrationName"

    const-string v3, "onMomentumScrollEnd"

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    invoke-virtual {v0}, LX/5pr;->a()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/FUq;LX/FUr;)V
    .locals 2

    .prologue
    .line 2250182
    iget-boolean v0, p1, LX/FUr;->c:Z

    if-eqz v0, :cond_0

    .line 2250183
    iget v0, p1, LX/FUr;->a:I

    iget v1, p1, LX/FUr;->b:I

    invoke-virtual {p0, v0, v1}, LX/FUq;->smoothScrollTo(II)V

    .line 2250184
    :goto_0
    return-void

    .line 2250185
    :cond_0
    iget v0, p1, LX/FUr;->a:I

    iget v1, p1, LX/FUr;->b:I

    invoke-virtual {p0, v0, v1}, LX/FUq;->scrollTo(II)V

    goto :goto_0
.end method


# virtual methods
.method public synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2250179
    invoke-virtual {p0, p1}, Lcom/facebook/react/views/scroll/ReactScrollViewManager;->b(LX/5rJ;)LX/FUq;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;ILX/5pC;)V
    .locals 0
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2250180
    check-cast p1, LX/FUq;

    invoke-static {p0, p1, p2, p3}, LX/FUs;->a(LX/FUp;Ljava/lang/Object;ILX/5pC;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/FUr;)V
    .locals 0

    .prologue
    .line 2250181
    check-cast p1, LX/FUq;

    invoke-static {p1, p2}, Lcom/facebook/react/views/scroll/ReactScrollViewManager;->a(LX/FUq;LX/FUr;)V

    return-void
.end method

.method public b(LX/5rJ;)LX/FUq;
    .locals 2

    .prologue
    .line 2250170
    new-instance v0, LX/FUq;

    iget-object v1, p0, Lcom/facebook/react/views/scroll/ReactScrollViewManager;->b:LX/F6L;

    invoke-direct {v0, p1, v1}, LX/FUq;-><init>(LX/5pX;LX/F6L;)V

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2250171
    const-string v0, "RCTScrollView"

    return-object v0
.end method

.method public setBottomFillColor(LX/FUq;I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        c = 0x0
        customType = "Color"
        name = "endFillColor"
    .end annotation

    .prologue
    .line 2250168
    invoke-virtual {p1, p2}, LX/FUq;->setEndFillColor(I)V

    .line 2250169
    return-void
.end method

.method public setRemoveClippedSubviews(LX/FUq;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "removeClippedSubviews"
    .end annotation

    .prologue
    .line 2250166
    invoke-virtual {p1, p2}, LX/FUq;->setRemoveClippedSubviews(Z)V

    .line 2250167
    return-void
.end method

.method public setScrollEnabled(LX/FUq;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        d = true
        name = "scrollEnabled"
    .end annotation

    .prologue
    .line 2250164
    iput-boolean p2, p1, LX/FUq;->j:Z

    .line 2250165
    return-void
.end method

.method public setScrollPerfTag(LX/FUq;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "scrollPerfTag"
    .end annotation

    .prologue
    .line 2250162
    iput-object p2, p1, LX/FUq;->m:Ljava/lang/String;

    .line 2250163
    return-void
.end method

.method public setSendMomentumEvents(LX/FUq;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "sendMomentumEvents"
    .end annotation

    .prologue
    .line 2250160
    iput-boolean p2, p1, LX/FUq;->k:Z

    .line 2250161
    return-void
.end method

.method public setShowsVerticalScrollIndicator(LX/FUq;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "showsVerticalScrollIndicator"
    .end annotation

    .prologue
    .line 2250158
    invoke-virtual {p1, p2}, LX/FUq;->setVerticalScrollBarEnabled(Z)V

    .line 2250159
    return-void
.end method

.method public final v()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2250157
    invoke-static {}, LX/FUs;->a()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final x()Ljava/util/Map;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2250156
    invoke-static {}, Lcom/facebook/react/views/scroll/ReactScrollViewManager;->A()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
