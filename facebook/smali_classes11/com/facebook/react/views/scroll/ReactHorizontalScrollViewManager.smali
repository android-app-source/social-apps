.class public Lcom/facebook/react/views/scroll/ReactHorizontalScrollViewManager;
.super Lcom/facebook/react/uimanager/ViewGroupManager;
.source ""

# interfaces
.implements LX/FUp;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "AndroidHorizontalScrollView"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/ViewGroupManager",
        "<",
        "LX/FUo;",
        ">;",
        "LX/FUp",
        "<",
        "LX/FUo;",
        ">;"
    }
.end annotation


# instance fields
.field private b:LX/F6L;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2249892
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/react/views/scroll/ReactHorizontalScrollViewManager;-><init>(LX/F6L;)V

    .line 2249893
    return-void
.end method

.method private constructor <init>(LX/F6L;)V
    .locals 1
    .param p1    # LX/F6L;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2249888
    invoke-direct {p0}, Lcom/facebook/react/uimanager/ViewGroupManager;-><init>()V

    .line 2249889
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/react/views/scroll/ReactHorizontalScrollViewManager;->b:LX/F6L;

    .line 2249890
    iput-object p1, p0, Lcom/facebook/react/views/scroll/ReactHorizontalScrollViewManager;->b:LX/F6L;

    .line 2249891
    return-void
.end method

.method private static a(LX/FUo;LX/FUr;)V
    .locals 2

    .prologue
    .line 2249884
    iget-boolean v0, p1, LX/FUr;->c:Z

    if-eqz v0, :cond_0

    .line 2249885
    iget v0, p1, LX/FUr;->a:I

    iget v1, p1, LX/FUr;->b:I

    invoke-virtual {p0, v0, v1}, LX/FUo;->smoothScrollTo(II)V

    .line 2249886
    :goto_0
    return-void

    .line 2249887
    :cond_0
    iget v0, p1, LX/FUr;->a:I

    iget v1, p1, LX/FUr;->b:I

    invoke-virtual {p0, v0, v1}, LX/FUo;->scrollTo(II)V

    goto :goto_0
.end method

.method private b(LX/5rJ;)LX/FUo;
    .locals 2

    .prologue
    .line 2249883
    new-instance v0, LX/FUo;

    iget-object v1, p0, Lcom/facebook/react/views/scroll/ReactHorizontalScrollViewManager;->b:LX/F6L;

    invoke-direct {v0, p1, v1}, LX/FUo;-><init>(Landroid/content/Context;LX/F6L;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2249882
    invoke-direct {p0, p1}, Lcom/facebook/react/views/scroll/ReactHorizontalScrollViewManager;->b(LX/5rJ;)LX/FUo;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;ILX/5pC;)V
    .locals 0
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2249880
    check-cast p1, LX/FUo;

    invoke-static {p0, p1, p2, p3}, LX/FUs;->a(LX/FUp;Ljava/lang/Object;ILX/5pC;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/FUr;)V
    .locals 0

    .prologue
    .line 2249879
    check-cast p1, LX/FUo;

    invoke-static {p1, p2}, Lcom/facebook/react/views/scroll/ReactHorizontalScrollViewManager;->a(LX/FUo;LX/FUr;)V

    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2249881
    const-string v0, "AndroidHorizontalScrollView"

    return-object v0
.end method

.method public setBottomFillColor(LX/FUo;I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        c = 0x0
        customType = "Color"
        name = "endFillColor"
    .end annotation

    .prologue
    .line 2249877
    invoke-virtual {p1, p2}, LX/FUo;->setEndFillColor(I)V

    .line 2249878
    return-void
.end method

.method public setPagingEnabled(LX/FUo;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "pagingEnabled"
    .end annotation

    .prologue
    .line 2249875
    iput-boolean p2, p1, LX/FUo;->e:Z

    .line 2249876
    return-void
.end method

.method public setRemoveClippedSubviews(LX/FUo;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "removeClippedSubviews"
    .end annotation

    .prologue
    .line 2249865
    invoke-virtual {p1, p2}, LX/FUo;->setRemoveClippedSubviews(Z)V

    .line 2249866
    return-void
.end method

.method public setScrollEnabled(LX/FUo;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        d = true
        name = "scrollEnabled"
    .end annotation

    .prologue
    .line 2249873
    iput-boolean p2, p1, LX/FUo;->h:Z

    .line 2249874
    return-void
.end method

.method public setScrollPerfTag(LX/FUo;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "scrollPerfTag"
    .end annotation

    .prologue
    .line 2249871
    iput-object p2, p1, LX/FUo;->k:Ljava/lang/String;

    .line 2249872
    return-void
.end method

.method public setSendMomentumEvents(LX/FUo;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "sendMomentumEvents"
    .end annotation

    .prologue
    .line 2249869
    iput-boolean p2, p1, LX/FUo;->i:Z

    .line 2249870
    return-void
.end method

.method public setShowsHorizontalScrollIndicator(LX/FUo;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "showsHorizontalScrollIndicator"
    .end annotation

    .prologue
    .line 2249867
    invoke-virtual {p1, p2}, LX/FUo;->setHorizontalScrollBarEnabled(Z)V

    .line 2249868
    return-void
.end method
