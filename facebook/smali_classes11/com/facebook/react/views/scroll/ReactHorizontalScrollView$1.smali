.class public final Lcom/facebook/react/views/scroll/ReactHorizontalScrollView$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/FUo;

.field private b:Z


# direct methods
.method public constructor <init>(LX/FUo;)V
    .locals 1

    .prologue
    .line 2249657
    iput-object p1, p0, Lcom/facebook/react/views/scroll/ReactHorizontalScrollView$1;->a:LX/FUo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2249658
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/react/views/scroll/ReactHorizontalScrollView$1;->b:Z

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2249659
    iget-object v2, p0, Lcom/facebook/react/views/scroll/ReactHorizontalScrollView$1;->a:LX/FUo;

    iget-boolean v2, v2, LX/FUo;->b:Z

    if-eqz v2, :cond_1

    .line 2249660
    iget-object v1, p0, Lcom/facebook/react/views/scroll/ReactHorizontalScrollView$1;->a:LX/FUo;

    .line 2249661
    iput-boolean v0, v1, LX/FUo;->b:Z

    .line 2249662
    :cond_0
    iget-object v0, p0, Lcom/facebook/react/views/scroll/ReactHorizontalScrollView$1;->a:LX/FUo;

    const-wide/16 v2, 0x14

    invoke-virtual {v0, p0, v2, v3}, LX/FUo;->postOnAnimationDelayed(Ljava/lang/Runnable;J)V

    .line 2249663
    :goto_0
    return-void

    .line 2249664
    :cond_1
    iget-object v2, p0, Lcom/facebook/react/views/scroll/ReactHorizontalScrollView$1;->a:LX/FUo;

    iget-boolean v2, v2, LX/FUo;->e:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/facebook/react/views/scroll/ReactHorizontalScrollView$1;->b:Z

    if-nez v2, :cond_3

    .line 2249665
    iput-boolean v1, p0, Lcom/facebook/react/views/scroll/ReactHorizontalScrollView$1;->b:Z

    .line 2249666
    iget-object v1, p0, Lcom/facebook/react/views/scroll/ReactHorizontalScrollView$1;->a:LX/FUo;

    .line 2249667
    invoke-static {v1, v0}, LX/FUo;->a$redex0(LX/FUo;I)V

    .line 2249668
    :goto_1
    if-eqz v0, :cond_0

    .line 2249669
    iget-object v0, p0, Lcom/facebook/react/views/scroll/ReactHorizontalScrollView$1;->a:LX/FUo;

    iget-boolean v0, v0, LX/FUo;->i:Z

    if-eqz v0, :cond_2

    .line 2249670
    iget-object v0, p0, Lcom/facebook/react/views/scroll/ReactHorizontalScrollView$1;->a:LX/FUo;

    invoke-static {v0}, LX/FUt;->e(Landroid/view/ViewGroup;)V

    .line 2249671
    :cond_2
    iget-object v0, p0, Lcom/facebook/react/views/scroll/ReactHorizontalScrollView$1;->a:LX/FUo;

    const/4 v1, 0x0

    .line 2249672
    iput-object v1, v0, LX/FUo;->f:Ljava/lang/Runnable;

    .line 2249673
    iget-object v0, p0, Lcom/facebook/react/views/scroll/ReactHorizontalScrollView$1;->a:LX/FUo;

    invoke-static {v0}, LX/FUo;->c(LX/FUo;)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method
