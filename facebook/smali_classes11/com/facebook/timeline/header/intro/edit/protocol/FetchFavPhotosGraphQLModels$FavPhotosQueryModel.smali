.class public final Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6793ffa3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2304134
    const-class v0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2304133
    const-class v0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2304131
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2304132
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2304125
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2304126
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;->a()Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2304127
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2304128
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2304129
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2304130
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2304117
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2304118
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;->a()Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2304119
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;->a()Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;

    .line 2304120
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;->a()Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2304121
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;

    .line 2304122
    iput-object v0, v1, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;->e:Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;

    .line 2304123
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2304124
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2304135
    new-instance v0, LX/Fwy;

    invoke-direct {v0, p1}, LX/Fwy;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2304115
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;->e:Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;->e:Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;

    .line 2304116
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;->e:Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2304113
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2304114
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2304112
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2304109
    new-instance v0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;

    invoke-direct {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;-><init>()V

    .line 2304110
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2304111
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2304108
    const v0, 0x50b539a9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2304107
    const v0, 0x285feb

    return v0
.end method
