.class public Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile d:Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;


# instance fields
.field public final c:LX/0tX;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2303737
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 2303738
    sput-object v0, Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;->a:Ljava/util/Set;

    const-string v1, "com.facebook.timeline.header.intro.protocol.bioQueryTag"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2303739
    const-class v0, Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2303740
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2303741
    iput-object p1, p0, Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;->c:LX/0tX;

    .line 2303742
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;
    .locals 4

    .prologue
    .line 2303743
    sget-object v0, Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;->d:Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;

    if-nez v0, :cond_1

    .line 2303744
    const-class v1, Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;

    monitor-enter v1

    .line 2303745
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;->d:Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2303746
    if-eqz v2, :cond_0

    .line 2303747
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2303748
    new-instance p0, Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-direct {p0, v3}, Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;-><init>(LX/0tX;)V

    .line 2303749
    move-object v0, p0

    .line 2303750
    sput-object v0, Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;->d:Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2303751
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2303752
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2303753
    :cond_1
    sget-object v0, Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;->d:Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;

    return-object v0

    .line 2303754
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2303755
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
