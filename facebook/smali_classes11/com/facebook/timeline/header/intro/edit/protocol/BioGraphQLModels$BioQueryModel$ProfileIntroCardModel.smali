.class public final Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x507103f2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2303905
    const-class v0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2303871
    const-class v0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2303903
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2303904
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2303894
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2303895
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2303896
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 2303897
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2303898
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2303899
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2303900
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2303901
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2303902
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2303886
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2303887
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2303888
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2303889
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2303890
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;

    .line 2303891
    iput-object v0, v1, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2303892
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2303893
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2303884
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2303885
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2303881
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2303882
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->g:Z

    .line 2303883
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2303878
    new-instance v0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;

    invoke-direct {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;-><init>()V

    .line 2303879
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2303880
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2303877
    const v0, 0x2b334dcb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2303876
    const v0, 0x7d57e813

    return v0
.end method

.method public final j()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2303874
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->f:Ljava/util/List;

    .line 2303875
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 2303872
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2303873
    iget-boolean v0, p0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->g:Z

    return v0
.end method
