.class public final Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5b295d90
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2303938
    const-class v0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2303939
    const-class v0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2303946
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2303947
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2303940
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2303941
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;->a()Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2303942
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2303943
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2303944
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2303945
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2303929
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2303930
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;->a()Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2303931
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;->a()Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;

    .line 2303932
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;->a()Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2303933
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;

    .line 2303934
    iput-object v0, v1, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;->e:Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;

    .line 2303935
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2303936
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2303937
    new-instance v0, LX/Fws;

    invoke-direct {v0, p1}, LX/Fws;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2303927
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;->e:Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;->e:Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;

    .line 2303928
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;->e:Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2303925
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2303926
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2303919
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2303922
    new-instance v0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;

    invoke-direct {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;-><init>()V

    .line 2303923
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2303924
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2303921
    const v0, -0x5d30c6da

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2303920
    const v0, 0x285feb

    return v0
.end method
