.class public final Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/5vO;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x52a8fc2c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2304093
    const-class v0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2304092
    const-class v0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2304090
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2304091
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2304083
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2304084
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;->a()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2304085
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2304086
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2304087
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;->f:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2304088
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2304089
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2304075
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2304076
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;->a()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2304077
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;->a()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    .line 2304078
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;->a()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2304079
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;

    .line 2304080
    iput-object v0, v1, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;->e:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    .line 2304081
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2304082
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2304073
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;->e:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;->e:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    .line 2304074
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;->e:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2304070
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2304071
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;->f:Z

    .line 2304072
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2304067
    new-instance v0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;

    invoke-direct {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;-><init>()V

    .line 2304068
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2304069
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2304066
    const v0, -0x31f01599    # -6.0362592E8f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2304063
    const v0, 0x7d57e813

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 2304064
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2304065
    iget-boolean v0, p0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;->f:Z

    return v0
.end method
