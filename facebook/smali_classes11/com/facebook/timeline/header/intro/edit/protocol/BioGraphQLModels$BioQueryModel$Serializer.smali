.class public final Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2303906
    const-class v0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;

    new-instance v1, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2303907
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2303908
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2303909
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2303910
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2303911
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2303912
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2303913
    if-eqz v2, :cond_0

    .line 2303914
    const-string p0, "profile_intro_card"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2303915
    invoke-static {v1, v2, p1, p2}, LX/Fwu;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2303916
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2303917
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2303918
    check-cast p1, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$Serializer;->a(Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
