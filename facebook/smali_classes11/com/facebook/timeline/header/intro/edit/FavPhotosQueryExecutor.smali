.class public Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile f:Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;


# instance fields
.field public final c:LX/0tX;

.field public final d:LX/0rq;

.field public final e:LX/0se;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2303756
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 2303757
    sput-object v0, Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;->a:Ljava/util/Set;

    const-string v1, "com.facebook.timeline.header.intro.protocol.favPhotosQueryTag"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2303758
    const-class v0, Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/0rq;LX/0se;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2303759
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2303760
    iput-object p1, p0, Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;->c:LX/0tX;

    .line 2303761
    iput-object p2, p0, Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;->d:LX/0rq;

    .line 2303762
    iput-object p3, p0, Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;->e:LX/0se;

    .line 2303763
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;
    .locals 6

    .prologue
    .line 2303764
    sget-object v0, Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;->f:Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;

    if-nez v0, :cond_1

    .line 2303765
    const-class v1, Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;

    monitor-enter v1

    .line 2303766
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;->f:Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2303767
    if-eqz v2, :cond_0

    .line 2303768
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2303769
    new-instance p0, Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v4

    check-cast v4, LX/0rq;

    invoke-static {v0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v5

    check-cast v5, LX/0se;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;-><init>(LX/0tX;LX/0rq;LX/0se;)V

    .line 2303770
    move-object v0, p0

    .line 2303771
    sput-object v0, Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;->f:Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2303772
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2303773
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2303774
    :cond_1
    sget-object v0, Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;->f:Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;

    return-object v0

    .line 2303775
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2303776
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
