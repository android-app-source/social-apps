.class public abstract Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "LoadedResultType:Ljava/lang/Object;",
        ">",
        "Lcom/facebook/base/activity/FbFragmentActivity;"
    }
.end annotation


# instance fields
.field public p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/timeline/header/intro/edit/TimelineLoadingFragment;

.field public t:LX/1Mv;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2303545
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;

    const/16 v1, 0x259

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x2ca

    invoke-static {v0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x1430

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v1, p0, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->p:LX/0Ot;

    iput-object v2, p0, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->q:LX/0Or;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->r:LX/0Ot;

    return-void
.end method

.method public static b(Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 2303546
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1, p1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 2303547
    return-void
.end method

.method public static r(Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 2303549
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public static u(Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 2303548
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public abstract a()Landroid/support/v4/app/Fragment;
.end method

.method public abstract a(Ljava/lang/Object;Landroid/os/Bundle;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(T",
            "LoadedResultType;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation
.end method

.method public final attachBaseContext(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2303542
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->attachBaseContext(Landroid/content/Context;)V

    .line 2303543
    invoke-static {p0, p0}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2303544
    return-void
.end method

.method public abstract b()I
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2303519
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2303520
    const v0, 0x7f03098b

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->setContentView(I)V

    .line 2303521
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2303522
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2303523
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->b()I

    move-result p1

    invoke-interface {v0, p1}, LX/0h5;->setTitle(I)V

    .line 2303524
    new-instance p1, LX/Fwo;

    invoke-direct {p1, p0}, LX/Fwo;-><init>(Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;)V

    invoke-interface {v0, p1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2303525
    invoke-static {p0}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->r(Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2303526
    if-eqz v0, :cond_2

    instance-of v0, v0, Lcom/facebook/timeline/header/intro/edit/TimelineLoadingFragment;

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2303527
    if-nez v0, :cond_1

    .line 2303528
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2303529
    invoke-static {p0}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->r(Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/edit/TimelineLoadingFragment;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->s:Lcom/facebook/timeline/header/intro/edit/TimelineLoadingFragment;

    .line 2303530
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->s:Lcom/facebook/timeline/header/intro/edit/TimelineLoadingFragment;

    if-nez v0, :cond_0

    .line 2303531
    new-instance v0, Lcom/facebook/timeline/header/intro/edit/TimelineLoadingFragment;

    invoke-direct {v0}, Lcom/facebook/timeline/header/intro/edit/TimelineLoadingFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->s:Lcom/facebook/timeline/header/intro/edit/TimelineLoadingFragment;

    .line 2303532
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->n()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2303533
    new-instance p1, LX/Fwp;

    invoke-direct {p1, p0}, LX/Fwp;-><init>(Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;)V

    .line 2303534
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1, p1, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2303535
    new-instance v0, LX/1Mv;

    invoke-direct {v0, v1, p1}, LX/1Mv;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->t:LX/1Mv;

    .line 2303536
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->s:Lcom/facebook/timeline/header/intro/edit/TimelineLoadingFragment;

    invoke-static {p0, v0}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->b(Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;Landroid/support/v4/app/Fragment;)V

    .line 2303537
    :cond_1
    :goto_1
    new-instance v0, LX/Fwn;

    invoke-direct {v0, p0}, LX/Fwn;-><init>(Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;)V

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    .line 2303538
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 2303539
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2303540
    invoke-static {p0}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->u(Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2303541
    invoke-static {p0, v0}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->b(Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;Landroid/support/v4/app/Fragment;)V

    goto :goto_1
.end method

.method public abstract d(Landroid/os/Bundle;)V
.end method

.method public abstract l()V
.end method

.method public abstract m()V
.end method

.method public abstract n()Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<T",
            "LoadedResultType;",
            ">;"
        }
    .end annotation
.end method

.method public abstract o()Z
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x5d1af285

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2303515
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2303516
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->t:LX/1Mv;

    if-eqz v1, :cond_0

    .line 2303517
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->t:LX/1Mv;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/1Mv;->a(Z)V

    .line 2303518
    :cond_0
    const/16 v1, 0x23

    const v2, 0x71be3fe4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
