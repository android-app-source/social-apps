.class public Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditActivity;
.super Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2303591
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/executor/GraphQLResult;Landroid/os/Bundle;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2303576
    if-eqz p0, :cond_2

    .line 2303577
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2303578
    if-eqz v0, :cond_2

    .line 2303579
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2303580
    check-cast v0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;->a()Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2303581
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2303582
    check-cast v0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;->a()Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;

    move-result-object v0

    .line 2303583
    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2303584
    const-string v1, "initial_bio_text"

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2303585
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2303586
    const-string v1, "bio_prompts"

    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->j()LX/0Px;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2303587
    :cond_1
    const-string v1, "initial_is_feed_sharing_switch_checked"

    .line 2303588
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2303589
    check-cast v0, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;->a()Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel$ProfileIntroCardModel;->k()Z

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2303590
    :cond_2
    return-void
.end method

.method private static a(Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditActivity;LX/0Ot;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditActivity;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2303575
    iput-object p1, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditActivity;->s:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditActivity;->t:LX/0Or;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditActivity;

    const/16 v1, 0x3676

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x15e7

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditActivity;->a(Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditActivity;LX/0Ot;LX/0Or;)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 2303574
    new-instance v0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;

    invoke-direct {v0}, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;-><init>()V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2303573
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-static {p1, p2}, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditActivity;->a(Lcom/facebook/graphql/executor/GraphQLResult;Landroid/os/Bundle;)V

    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2303572
    const v0, 0x7f081530

    return v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2303550
    invoke-static {p0, p0}, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2303551
    invoke-super {p0, p1}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->b(Landroid/os/Bundle;)V

    .line 2303552
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2303568
    const-string v2, "should_open_new_timeline_activity_on_save_success"

    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "surface"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "profile_edit"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2303569
    const-string v0, "show_feed_sharing_switch_extra"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2303570
    return-void

    .line 2303571
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()V
    .locals 0

    .prologue
    .line 2303567
    return-void
.end method

.method public final m()V
    .locals 0

    .prologue
    .line 2303566
    return-void
.end method

.method public final n()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/timeline/header/intro/edit/protocol/BioGraphQLModels$BioQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2303554
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditActivity;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;

    iget-object v1, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditActivity;->t:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2303555
    new-instance v4, LX/Fwq;

    invoke-direct {v4}, LX/Fwq;-><init>()V

    move-object v4, v4

    .line 2303556
    const-string v5, "profile_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    check-cast v4, LX/Fwq;

    .line 2303557
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    sget-object v5, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v4, v5}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v4

    sget-object v5, Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 2303558
    iput-object v5, v4, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2303559
    move-object v4, v4

    .line 2303560
    sget-object v5, Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;->a:Ljava/util/Set;

    .line 2303561
    iput-object v5, v4, LX/0zO;->d:Ljava/util/Set;

    .line 2303562
    move-object v4, v4

    .line 2303563
    sget-object v5, LX/0zS;->a:LX/0zS;

    invoke-virtual {v4, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    const-wide/16 v6, 0xe10

    invoke-virtual {v4, v6, v7}, LX/0zO;->a(J)LX/0zO;

    move-result-object v4

    .line 2303564
    iget-object v5, v0, Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;->c:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    move-object v0, v4

    .line 2303565
    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 2303553
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "profileId"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
