.class public Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1mR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation runtime Lcom/facebook/work/config/community/WorkCommunityName;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/1e4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/63c;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private l:I

.field private m:I

.field private n:LX/63b;

.field public o:Lcom/facebook/resources/ui/FbEditText;

.field private p:LX/0h5;

.field private q:Lcom/facebook/resources/ui/FbTextView;

.field public r:Landroid/animation/ValueAnimator;

.field public s:Ljava/lang/CharSequence;

.field public t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public u:Z

.field public v:Z

.field private final w:LX/63W;

.field private final x:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2303734
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2303735
    new-instance v0, LX/Fwj;

    invoke-direct {v0, p0}, LX/Fwj;-><init>(Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;)V

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->w:LX/63W;

    .line 2303736
    new-instance v0, LX/Fwk;

    invoke-direct {v0, p0}, LX/Fwk;-><init>(Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;)V

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->x:Landroid/text/TextWatcher;

    return-void
.end method

.method public static k(Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;)V
    .locals 5

    .prologue
    .line 2303720
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->p:LX/0h5;

    if-nez v0, :cond_0

    .line 2303721
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0d00bc

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->p:LX/0h5;

    .line 2303722
    :cond_0
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2303723
    iget-boolean v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->u:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->s:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->o:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    move v0, v2

    .line 2303724
    :goto_0
    iget-object v3, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->o:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    const/16 v4, 0x65

    if-gt v3, v4, :cond_4

    iget-object v3, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->o:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2303725
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_5

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_5

    const/4 v4, 0x1

    :goto_1
    move v3, v4

    .line 2303726
    if-nez v3, :cond_4

    if-eqz v0, :cond_4

    :goto_2
    move v0, v2

    .line 2303727
    if-eqz v0, :cond_2

    .line 2303728
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->p:LX/0h5;

    iget-object v1, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->n:LX/63b;

    invoke-virtual {v1}, LX/63b;->a()LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2303729
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->p:LX/0h5;

    iget-object v1, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->w:LX/63W;

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2303730
    :goto_3
    return-void

    .line 2303731
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->p:LX/0h5;

    iget-object v1, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->n:LX/63b;

    invoke-virtual {v1}, LX/63b;->b()LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    goto :goto_3

    :cond_3
    move v0, v1

    .line 2303732
    goto :goto_0

    :cond_4
    move v2, v1

    .line 2303733
    goto :goto_2

    :cond_5
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static n(Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;)V
    .locals 4

    .prologue
    const/16 v3, 0x65

    .line 2303713
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->o:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 2303714
    if-le v0, v3, :cond_0

    .line 2303715
    rsub-int/lit8 v0, v0, 0x65

    .line 2303716
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->q:Lcom/facebook/resources/ui/FbTextView;

    iget v2, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->m:I

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2303717
    :goto_0
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->q:Lcom/facebook/resources/ui/FbTextView;

    const-string v2, "%d / %d"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2303718
    return-void

    .line 2303719
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->q:Lcom/facebook/resources/ui/FbTextView;

    iget v2, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->l:I

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public static o(Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;)V
    .locals 1

    .prologue
    .line 2303707
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->r:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    .line 2303708
    :goto_0
    return-void

    .line 2303709
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->o:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 2303710
    if-nez v0, :cond_1

    .line 2303711
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->r:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 2303712
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->r:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2303648
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2303649
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;

    invoke-static {p1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    const/16 v5, 0xc49

    invoke-static {p1, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x455

    invoke-static {p1, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x15e7

    invoke-static {p1, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {p1}, LX/1mR;->a(LX/0QB;)LX/1mR;

    move-result-object v8

    check-cast v8, LX/1mR;

    invoke-static {p1}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-static {p1}, LX/4oy;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-static {p1}, LX/1e4;->a(LX/0QB;)LX/1e4;

    move-result-object v11

    check-cast v11, LX/1e4;

    invoke-static {p1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    const-class v0, LX/63c;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/63c;

    iput-object v3, v2, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->a:LX/0tX;

    iput-object v4, v2, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->b:Ljava/util/concurrent/Executor;

    iput-object v5, v2, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->c:LX/0Ot;

    iput-object v6, v2, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->d:LX/0Ot;

    iput-object v7, v2, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->e:LX/0Or;

    iput-object v8, v2, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->f:LX/1mR;

    iput-object v9, v2, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->g:Ljava/lang/Boolean;

    iput-object v10, v2, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->h:Ljava/lang/String;

    iput-object v11, v2, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->i:LX/1e4;

    iput-object v12, v2, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->j:LX/0ad;

    iput-object p1, v2, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->k:LX/63c;

    .line 2303650
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->k:LX/63c;

    const v1, 0x7f0815bd

    invoke-virtual {v0, v1}, LX/63c;->a(I)LX/63b;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->n:LX/63b;

    .line 2303651
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->m:I

    .line 2303652
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->l:I

    .line 2303653
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x70becbae

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2303668
    const v0, 0x7f0314c5

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2303669
    const v0, 0x7f0d185f

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2303670
    iget-object v3, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->g:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2303671
    const v3, 0x7f0815e9

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->h:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2303672
    :goto_0
    move-object v3, v3

    .line 2303673
    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2303674
    const v0, 0x7f0d185e

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2303675
    iget-object v4, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->i:LX/1e4;

    iget-object v3, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->g:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "work_list"

    :goto_1
    invoke-virtual {v4, v3}, LX/1e4;->a(Ljava/lang/String;)I

    move-result v3

    move v3, v3

    .line 2303676
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2303677
    const v0, 0x7f0d2f18

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->o:Lcom/facebook/resources/ui/FbEditText;

    .line 2303678
    const v0, 0x7f0d2f19

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->q:Lcom/facebook/resources/ui/FbTextView;

    .line 2303679
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->s:Ljava/lang/CharSequence;

    .line 2303680
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2303681
    if-eqz v0, :cond_0

    .line 2303682
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2303683
    const-string v3, "initial_bio_text"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->s:Ljava/lang/CharSequence;

    .line 2303684
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2303685
    const-string v3, "bio_prompts"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->t:Ljava/util/List;

    .line 2303686
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2303687
    const-string v3, "came_from_suggested_bio"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->u:Z

    .line 2303688
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2303689
    const-string v3, "should_open_new_timeline_activity_on_save_success"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->v:Z

    .line 2303690
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->o:Lcom/facebook/resources/ui/FbEditText;

    .line 2303691
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 2303692
    const v3, 0x7fffffff

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setMaxLines(I)V

    .line 2303693
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setHorizontallyScrolling(Z)V

    .line 2303694
    const/4 v3, 0x6

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 2303695
    if-eqz p3, :cond_3

    .line 2303696
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->o:Lcom/facebook/resources/ui/FbEditText;

    const-string v3, "bio_text"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2303697
    :goto_2
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->o:Lcom/facebook/resources/ui/FbEditText;

    iget-object v3, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->o:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbEditText;->setSelection(I)V

    .line 2303698
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->o:Lcom/facebook/resources/ui/FbEditText;

    iget-object v3, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->x:Landroid/text/TextWatcher;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2303699
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->t:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2303700
    iget-object v3, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->o:Lcom/facebook/resources/ui/FbEditText;

    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->t:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Lcom/facebook/resources/ui/FbEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2303701
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x1

    if-le v0, v3, :cond_2

    .line 2303702
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->r:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_1

    .line 2303703
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->o:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getCurrentHintTextColor()I

    move-result v0

    const/4 v3, -0x1

    new-instance v4, LX/Fwl;

    invoke-direct {v4, p0}, LX/Fwl;-><init>(Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;)V

    iget-object v5, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->j:LX/0ad;

    sget p1, LX/0wf;->i:I

    const/16 p2, 0x5dc

    invoke-interface {v5, p1, p2}, LX/0ad;->a(II)I

    move-result v5

    invoke-static {v0, v3, v4, v5}, LX/EQP;->a(IILX/EQO;I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->r:Landroid/animation/ValueAnimator;

    .line 2303704
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->r:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2303705
    :cond_2
    const v0, -0x564735ab

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-object v2

    .line 2303706
    :cond_3
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->o:Lcom/facebook/resources/ui/FbEditText;

    iget-object v3, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->s:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_4
    const v3, 0x7f0815e8

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    :cond_5
    const-string v3, "everyone"

    goto/16 :goto_1
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4b8cb070

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2303662
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->r:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->r:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2303663
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->r:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->end()V

    .line 2303664
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 2303665
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->o:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2303666
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2303667
    const/16 v1, 0x2b

    const v2, 0xa14b589

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x42345507

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2303657
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2303658
    invoke-static {p0}, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->k(Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;)V

    .line 2303659
    invoke-static {p0}, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->n(Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;)V

    .line 2303660
    invoke-static {p0}, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->o(Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;)V

    .line 2303661
    const/16 v1, 0x2b

    const v2, -0x4d1526c7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2303654
    const-string v0, "initial_bio_text"

    iget-object v1, p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->o:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 2303655
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2303656
    return-void
.end method
