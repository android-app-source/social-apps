.class public Lcom/facebook/timeline/header/intro/bio/TimelineHeaderSuggestedBioView;
.super Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;
.source ""


# instance fields
.field private a:Landroid/view/View;

.field private b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2303513
    invoke-direct {p0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2303514
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2303511
    invoke-direct {p0, p1, p2}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2303512
    return-void
.end method


# virtual methods
.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x37a424dd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2303507
    invoke-super {p0}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->onFinishInflate()V

    .line 2303508
    const v0, 0x7f0d2f5f

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderSuggestedBioView;->a:Landroid/view/View;

    .line 2303509
    const v0, 0x7f0d2f61

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderSuggestedBioView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2303510
    const/16 v0, 0x2d

    const v2, 0x5433edb9

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setOnCloseListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2303505
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderSuggestedBioView;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2303506
    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2303503
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderSuggestedBioView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2303504
    return-void
.end method
