.class public Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field private c:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2303497
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2303498
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2303471
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2303472
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2303492
    const-class v0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2303493
    const v0, 0x7f0d2f1f

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2303494
    const v0, 0x7f0d2f20

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2303495
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->c:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->a:LX/23P;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2303496
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v0

    check-cast v0, LX/23P;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->a:LX/23P;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;ZZ)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2303476
    if-eqz p2, :cond_0

    .line 2303477
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->c:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2303478
    :goto_0
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2303479
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2303480
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2303481
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setGravity(I)V

    .line 2303482
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00f9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2303483
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2303484
    if-eqz p3, :cond_1

    .line 2303485
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->b:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setMaxLines(I)V

    .line 2303486
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->b:Lcom/facebook/resources/ui/FbTextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 2303487
    :goto_1
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2303488
    return-void

    .line 2303489
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->c:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0

    .line 2303490
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->b:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setMaxLines(I)V

    .line 2303491
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_1
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x4b61267

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2303473
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onFinishInflate()V

    .line 2303474
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->a()V

    .line 2303475
    const/16 v1, 0x2d

    const v2, -0x55b2f1d9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
