.class public Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public a:LX/Fxf;

.field public b:F

.field public c:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2304728
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2304729
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2304730
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2304731
    return-void
.end method

.method private a(Landroid/view/View;II)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 2304732
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2304733
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v2, v3, :cond_0

    :goto_0
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v0, v3, :cond_1

    :goto_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->measureChild(Landroid/view/View;II)V

    .line 2304734
    return-void

    :cond_0
    move p2, v1

    .line 2304735
    goto :goto_0

    :cond_1
    move p3, v1

    goto :goto_1
.end method

.method private static b(Landroid/view/View;II)V
    .locals 2

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    .line 2304736
    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->measure(II)V

    .line 2304737
    return-void
.end method


# virtual methods
.method public getLastRawX()F
    .locals 1

    .prologue
    .line 2304738
    iget v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->b:F

    return v0
.end method

.method public getLastRawY()F
    .locals 1

    .prologue
    .line 2304739
    iget v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->c:F

    return v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 2304740
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->a:LX/Fxf;

    if-nez v0, :cond_0

    .line 2304741
    const/4 v0, 0x0

    .line 2304742
    :goto_0
    return v0

    .line 2304743
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 2304744
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->a:LX/Fxf;

    invoke-virtual {v0}, LX/Fxf;->c()Z

    move-result v0

    goto :goto_0

    .line 2304745
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->b:F

    .line 2304746
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->c:F

    .line 2304747
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->a:LX/Fxf;

    .line 2304748
    iget-boolean v1, v0, LX/Fxf;->c:Z

    move v0, v1

    .line 2304749
    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 2304750
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 2304751
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2304752
    invoke-direct {p0, v0, p1, p2}, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->a(Landroid/view/View;II)V

    .line 2304753
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 2304754
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 2304755
    if-le v2, v1, :cond_0

    .line 2304756
    invoke-static {v0, v1, v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->b(Landroid/view/View;II)V

    .line 2304757
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->setMeasuredDimension(II)V

    .line 2304758
    return-void
.end method

.method public setAnimationController(LX/Fxf;)V
    .locals 0

    .prologue
    .line 2304759
    iput-object p1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->a:LX/Fxf;

    .line 2304760
    return-void
.end method
