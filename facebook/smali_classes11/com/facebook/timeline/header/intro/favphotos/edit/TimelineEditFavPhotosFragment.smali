.class public Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:LX/0h5;

.field public B:LX/2yQ;

.field public C:LX/2dD;

.field public D:I

.field public E:Lcom/facebook/drawee/drawable/AutoRotateDrawable;

.field public F:J

.field public G:Z

.field public H:Z

.field public b:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/FxJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FxP;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/BQ9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/63c;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation runtime Lcom/facebook/work/config/community/WorkCommunityName;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/1e4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/Fxh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;",
            ">;"
        }
    .end annotation
.end field

.field public s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/FxZ;",
            ">;"
        }
    .end annotation
.end field

.field public t:Lcom/facebook/fig/button/FigButton;

.field public u:Landroid/view/ViewGroup;

.field public v:Landroid/view/View$OnClickListener;

.field private w:LX/63b;

.field public x:LX/FxI;

.field public y:Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

.field public z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2305294
    const-class v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2305295
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2305296
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->D:I

    .line 2305297
    return-void
.end method

.method public static A(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V
    .locals 2

    .prologue
    .line 2305298
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    .line 2305299
    :goto_0
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->t:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v1, v0}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2305300
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->t:Lcom/facebook/fig/button/FigButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->v:Landroid/view/View$OnClickListener;

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2305301
    return-void

    .line 2305302
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2305303
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;)Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2305304
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;->b()LX/5vo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;->b()LX/5vo;

    move-result-object v0

    invoke-interface {v0}, LX/5vo;->aj_()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;->b()LX/5vo;

    move-result-object v0

    invoke-interface {v0}, LX/5vo;->aj_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2305305
    :cond_0
    const/4 v0, 0x0

    .line 2305306
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;->b()LX/5vo;

    move-result-object v0

    invoke-interface {v0}, LX/5vo;->aj_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;Ljava/lang/Integer;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2305307
    new-instance v0, LX/FxX;

    invoke-direct {v0, p0, p2, p1}, LX/FxX;-><init>(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;Ljava/lang/Integer;Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;)V

    return-object v0
.end method

.method private a(Lcom/facebook/ipc/media/MediaItem;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2305308
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 2305309
    iget-object v1, v0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v0, v1

    .line 2305310
    sget-object v1, LX/4gQ;->Video:LX/4gQ;

    if-ne v0, v1, :cond_1

    .line 2305311
    new-instance v0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;-><init>(Landroid/net/Uri;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;)V

    .line 2305312
    new-instance v1, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    const/4 v2, 0x1

    invoke-direct {v1, v2, v0, v3}, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;-><init>(ILcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;)V

    .line 2305313
    invoke-static {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->u(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V

    .line 2305314
    invoke-static {p0, v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->b(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;)V

    .line 2305315
    :cond_0
    :goto_0
    return-void

    .line 2305316
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 2305317
    iget-object v1, v0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v0, v1

    .line 2305318
    sget-object v1, LX/4gQ;->Photo:LX/4gQ;

    if-ne v0, v1, :cond_0

    .line 2305319
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;

    .line 2305320
    iget-object v1, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;->b:LX/0TD;

    new-instance v2, LX/Fxi;

    invoke-direct {v2, v0, p1}, LX/Fxi;-><init>(Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;Lcom/facebook/ipc/media/MediaItem;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2305321
    new-instance v1, LX/FxT;

    invoke-direct {v1, p0}, LX/FxT;-><init>(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V

    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->f:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;LX/23P;LX/FxJ;LX/0Or;LX/0Or;Ljava/util/concurrent/Executor;LX/BQ9;LX/0Ot;LX/0Ot;LX/0Or;LX/63c;Ljava/lang/Boolean;Ljava/lang/String;LX/0ad;LX/0hL;LX/1e4;LX/Fxh;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;",
            "LX/23P;",
            "LX/FxJ;",
            "LX/0Or",
            "<",
            "Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FxP;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            "LX/BQ9;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/63c;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "LX/0ad;",
            "LX/0hL;",
            "LX/1e4;",
            "LX/Fxh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2305322
    iput-object p1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->b:LX/23P;

    iput-object p2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->c:LX/FxJ;

    iput-object p3, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->d:LX/0Or;

    iput-object p4, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->e:LX/0Or;

    iput-object p5, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->f:Ljava/util/concurrent/Executor;

    iput-object p6, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->g:LX/BQ9;

    iput-object p7, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->h:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->i:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->j:LX/0Or;

    iput-object p10, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->k:LX/63c;

    iput-object p11, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->l:Ljava/lang/Boolean;

    iput-object p12, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->m:Ljava/lang/String;

    iput-object p13, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->n:LX/0ad;

    iput-object p14, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->o:LX/0hL;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->p:LX/1e4;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->q:LX/Fxh;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 18

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v17

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    invoke-static/range {v17 .. v17}, LX/23P;->a(LX/0QB;)LX/23P;

    move-result-object v2

    check-cast v2, LX/23P;

    const-class v3, LX/FxJ;

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/FxJ;

    const/16 v4, 0x3685

    move-object/from16 v0, v17

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x3683

    move-object/from16 v0, v17

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static/range {v17 .. v17}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static/range {v17 .. v17}, LX/BQ9;->a(LX/0QB;)LX/BQ9;

    move-result-object v7

    check-cast v7, LX/BQ9;

    const/16 v8, 0xc49

    move-object/from16 v0, v17

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x455

    move-object/from16 v0, v17

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x15e7

    move-object/from16 v0, v17

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const-class v11, LX/63c;

    move-object/from16 v0, v17

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/63c;

    invoke-static/range {v17 .. v17}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v12

    check-cast v12, Ljava/lang/Boolean;

    invoke-static/range {v17 .. v17}, LX/4oy;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-static/range {v17 .. v17}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v14

    check-cast v14, LX/0ad;

    invoke-static/range {v17 .. v17}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v15

    check-cast v15, LX/0hL;

    invoke-static/range {v17 .. v17}, LX/1e4;->a(LX/0QB;)LX/1e4;

    move-result-object v16

    check-cast v16, LX/1e4;

    invoke-static/range {v17 .. v17}, LX/Fxh;->a(LX/0QB;)LX/Fxh;

    move-result-object v17

    check-cast v17, LX/Fxh;

    invoke-static/range {v1 .. v17}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;LX/23P;LX/FxJ;LX/0Or;LX/0Or;Ljava/util/concurrent/Executor;LX/BQ9;LX/0Ot;LX/0Ot;LX/0Or;LX/63c;Ljava/lang/Boolean;Ljava/lang/String;LX/0ad;LX/0hL;LX/1e4;LX/Fxh;)V

    return-void
.end method

.method private a(ZLcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;)V
    .locals 3

    .prologue
    .line 2305281
    if-nez p1, :cond_1

    .line 2305282
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->y:Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    if-eqz v0, :cond_0

    .line 2305283
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    iget v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->z:I

    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->y:Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2305284
    iget v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->z:I

    invoke-static {p0, v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->f(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;I)V

    .line 2305285
    :goto_0
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a$redex0(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;Z)V

    .line 2305286
    invoke-static {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->A(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V

    .line 2305287
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->y:Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    .line 2305288
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->H:Z

    .line 2305289
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->l()V

    .line 2305290
    return-void

    .line 2305291
    :cond_0
    iget v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->z:I

    invoke-direct {p0, v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->e(I)V

    goto :goto_0

    .line 2305292
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    iget v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->z:I

    invoke-interface {v0, v1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2305293
    iget v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->z:I

    invoke-static {p0, v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->f(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;I)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2305323
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FxP;

    .line 2305324
    iget-object v1, v0, LX/FxP;->b:LX/Fx4;

    .line 2305325
    iget-object v2, v1, LX/Fx4;->e:LX/0ad;

    sget-short v3, LX/0wf;->G:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2305326
    iget-object v2, v1, LX/Fx4;->a:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->c()I

    move-result v2

    move v2, v2

    .line 2305327
    :goto_0
    move v1, v2

    .line 2305328
    new-instance v2, LX/5yy;

    invoke-direct {v2}, LX/5yy;-><init>()V

    move-object v2, v2

    .line 2305329
    const-string v3, "photo_fbId"

    invoke-virtual {v2, v3, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "photo_width"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "photo_height"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    check-cast v1, LX/5yy;

    move-object v1, v1

    .line 2305330
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2305331
    iget-object v2, v0, LX/FxP;->a:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    new-instance v2, LX/FxO;

    invoke-direct {v2, v0, p1}, LX/FxO;-><init>(LX/FxP;Ljava/lang/String;)V

    invoke-static {v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2305332
    new-instance v1, LX/FxU;

    invoke-direct {v1, p0}, LX/FxU;-><init>(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V

    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->f:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2305333
    return-void

    .line 2305334
    :cond_0
    iget-object v2, v1, LX/Fx4;->c:LX/G0r;

    invoke-virtual {v2}, LX/G0r;->a()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    .line 2305335
    iget-object v3, v1, LX/Fx4;->a:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->c()I

    move-result v3

    sub-int v2, v3, v2

    div-int/lit8 v2, v2, 0x3

    .line 2305336
    move v2, v2

    .line 2305337
    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;Z)V
    .locals 2

    .prologue
    .line 2305338
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FxZ;

    .line 2305339
    iget-object v0, v0, LX/FxZ;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 2305340
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2305341
    iput-boolean v3, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->H:Z

    .line 2305342
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    .line 2305343
    iget-object v2, p1, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->b:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    move-object v2, v2

    .line 2305344
    if-eqz v2, :cond_0

    .line 2305345
    iget-object v2, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->b:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    move-object v2, v2

    .line 2305346
    if-eqz v2, :cond_0

    .line 2305347
    iget-object v2, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->b:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    move-object v2, v2

    .line 2305348
    invoke-virtual {v2}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;->b()LX/5vo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2305349
    iget-object v2, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->b:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    move-object v2, v2

    .line 2305350
    invoke-virtual {v2}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;->b()LX/5vo;

    move-result-object v2

    invoke-interface {v2}, LX/5vo;->d()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2305351
    iget-object v2, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->b:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    move-object v0, v2

    .line 2305352
    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;->b()LX/5vo;

    move-result-object v0

    invoke-interface {v0}, LX/5vo;->d()Ljava/lang/String;

    move-result-object v0

    .line 2305353
    iget-object v2, p1, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->b:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    move-object v2, v2

    .line 2305354
    invoke-virtual {v2}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;->b()LX/5vo;

    move-result-object v2

    invoke-interface {v2}, LX/5vo;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2305355
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f081603

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2305356
    const/4 v0, 0x0

    invoke-direct {p0, v3, v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(ZLcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;)V

    .line 2305357
    :goto_0
    return-void

    .line 2305358
    :cond_1
    invoke-direct {p0, v4, p1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(ZLcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;)V

    goto :goto_0
.end method

.method private e(I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2305359
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v0, p1, :cond_2

    const/4 v0, 0x1

    .line 2305360
    :goto_0
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2305361
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2305362
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->u:Landroid/view/ViewGroup;

    invoke-virtual {v2, p1}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 2305363
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->n()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2305364
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {p0, v2}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->g(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;I)V

    .line 2305365
    :cond_0
    if-nez v0, :cond_1

    .line 2305366
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->d()V

    .line 2305367
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2305368
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FxZ;

    invoke-virtual {v0}, LX/FxZ;->a()Landroid/view/View;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {p0, v0, v3}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;Ljava/lang/Integer;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2305369
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 2305370
    goto :goto_0

    .line 2305371
    :cond_3
    invoke-static {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->A(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V

    .line 2305372
    return-void
.end method

.method public static f(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;I)V
    .locals 7

    .prologue
    .line 2305373
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FxZ;

    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    const/4 v2, 0x1

    .line 2305374
    iget v3, v1, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->a:I

    move v3, v3

    .line 2305375
    if-ne v3, v2, :cond_0

    .line 2305376
    :goto_0
    if-eqz v2, :cond_1

    .line 2305377
    iget-object v2, v0, LX/FxZ;->a:Landroid/view/View;

    .line 2305378
    iget-object v3, v1, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->c:Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;

    move-object v3, v3

    .line 2305379
    const v4, 0x7f0d2f34

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/timeline/favmediapicker/ui/views/FavoriteMediaPickerVideoView;

    .line 2305380
    new-instance v5, LX/2oE;

    invoke-direct {v5}, LX/2oE;-><init>()V

    .line 2305381
    iget-object v6, v3, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;->a:Landroid/net/Uri;

    move-object v6, v6

    .line 2305382
    iput-object v6, v5, LX/2oE;->a:Landroid/net/Uri;

    .line 2305383
    move-object v5, v5

    .line 2305384
    sget-object v6, LX/097;->FROM_STREAM:LX/097;

    .line 2305385
    iput-object v6, v5, LX/2oE;->e:LX/097;

    .line 2305386
    move-object v5, v5

    .line 2305387
    invoke-virtual {v5}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v5

    .line 2305388
    new-instance v6, LX/2oH;

    invoke-direct {v6}, LX/2oH;-><init>()V

    invoke-virtual {v6, v5}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v5

    const/4 v6, 0x1

    .line 2305389
    iput-boolean v6, v5, LX/2oH;->g:Z

    .line 2305390
    move-object v5, v5

    .line 2305391
    invoke-virtual {v5}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v5

    .line 2305392
    new-instance v6, LX/2pZ;

    invoke-direct {v6}, LX/2pZ;-><init>()V

    .line 2305393
    iput-object v5, v6, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 2305394
    move-object v5, v6

    .line 2305395
    invoke-virtual {v5}, LX/2pZ;->b()LX/2pa;

    move-result-object v5

    .line 2305396
    invoke-virtual {v4, v5}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 2305397
    invoke-virtual {v4}, LX/2oW;->jj_()V

    .line 2305398
    move-object v2, v4

    .line 2305399
    iput-object v2, v0, LX/FxZ;->c:Lcom/facebook/timeline/favmediapicker/ui/views/FavoriteMediaPickerVideoView;

    .line 2305400
    :goto_1
    invoke-virtual {v0}, LX/FxZ;->a()Landroid/view/View;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {p0, v1, v3}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;Ljava/lang/Integer;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2305401
    iget-object v2, v0, LX/FxZ;->d:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2305402
    return-void

    .line 2305403
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 2305404
    :cond_1
    iget-object v2, v1, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->b:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    move-object v2, v2

    .line 2305405
    invoke-virtual {p0, v0, v2}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(LX/FxZ;Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;)V

    goto :goto_1
.end method

.method public static g(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;I)V
    .locals 4

    .prologue
    .line 2305406
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0314c3

    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->u:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2305407
    const-string v1, "tile"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2305408
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->v:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2305409
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p0, v0, p1, v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(Landroid/view/View;II)V

    .line 2305410
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->u:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 2305411
    return-void
.end method

.method public static h(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;I)LX/FxZ;
    .locals 6
    .param p0    # Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;
        .annotation build Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo$MediaType;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2305412
    if-ne p1, v0, :cond_1

    .line 2305413
    :goto_0
    new-instance v3, LX/FxZ;

    invoke-direct {v3}, LX/FxZ;-><init>()V

    .line 2305414
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    if-eqz v0, :cond_2

    .line 2305415
    const v2, 0x7f0314d5

    move v2, v2

    .line 2305416
    :goto_1
    iget-object v5, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->u:Landroid/view/ViewGroup;

    invoke-virtual {v4, v2, v5, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, v3, LX/FxZ;->a:Landroid/view/View;

    .line 2305417
    if-nez v0, :cond_0

    .line 2305418
    iget-object v0, v3, LX/FxZ;->a:Landroid/view/View;

    const v1, 0x7f0d2f32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, v3, LX/FxZ;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2305419
    iget-object v0, v3, LX/FxZ;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v1, LX/1Uo;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->E:Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    .line 2305420
    iput-object v2, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2305421
    move-object v1, v1

    .line 2305422
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0168

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v2, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2305423
    iput-object v2, v1, LX/1Uo;->r:Landroid/graphics/drawable/Drawable;

    .line 2305424
    move-object v1, v1

    .line 2305425
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a054c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v2, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, LX/1Uo;->g(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v1

    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2305426
    iget-object v0, v3, LX/FxZ;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v1, 0x7f0815e4

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2305427
    :cond_0
    iget-object v1, v3, LX/FxZ;->a:Landroid/view/View;

    const/4 v0, 0x1

    if-ne p1, v0, :cond_3

    const v0, 0x7f0d2f35

    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v3, LX/FxZ;->d:Landroid/view/View;

    .line 2305428
    iget-object v0, v3, LX/FxZ;->d:Landroid/view/View;

    const v1, 0x7f0815fe

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2305429
    iget-object v0, v3, LX/FxZ;->d:Landroid/view/View;

    new-instance v1, LX/FxY;

    invoke-direct {v1, p0}, LX/FxY;-><init>(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2305430
    return-object v3

    :cond_1
    move v0, v1

    .line 2305431
    goto/16 :goto_0

    .line 2305432
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->m()I

    move-result v2

    goto/16 :goto_1

    .line 2305433
    :cond_3
    const v0, 0x7f0d2f33

    goto :goto_2
.end method

.method public static s(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)LX/63b;
    .locals 2

    .prologue
    .line 2305434
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->w:LX/63b;

    if-nez v0, :cond_0

    .line 2305435
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->k:LX/63c;

    const v1, 0x7f0815bd

    invoke-virtual {v0, v1}, LX/63c;->a(I)LX/63b;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->w:LX/63b;

    .line 2305436
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->w:LX/63b;

    return-object v0
.end method

.method public static u(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V
    .locals 2

    .prologue
    .line 2305437
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->A:LX/0h5;

    invoke-static {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)LX/63b;

    move-result-object v1

    invoke-virtual {v1}, LX/63b;->a()LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2305438
    return-void
.end method

.method private v()V
    .locals 2

    .prologue
    .line 2305439
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->A:LX/0h5;

    invoke-static {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)LX/63b;

    move-result-object v1

    invoke-virtual {v1}, LX/63b;->b()LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2305440
    return-void
.end method

.method public static x(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V
    .locals 3

    .prologue
    .line 2305441
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f081602

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2305442
    invoke-static {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->u(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V

    .line 2305443
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(ZLcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;)V

    .line 2305444
    return-void
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p1    # Ljava/util/ArrayList;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLInterfaces$FavoritePhoto;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLInterfaces$FavoritePhoto;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2305107
    return-object p1
.end method

.method public a(I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "LX/4Ig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2305108
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(LX/FxZ;Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;)V
    .locals 3

    .prologue
    .line 2305109
    invoke-static {p2}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;)Landroid/net/Uri;

    move-result-object v0

    .line 2305110
    iget-object v1, p1, LX/FxZ;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v2, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2305111
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 2305112
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2305113
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2305114
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->F:J

    .line 2305115
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2305116
    const-string v1, "should_open_new_timeline_activity_on_save_success"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->G:Z

    .line 2305117
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->c:LX/FxJ;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2305118
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2305119
    invoke-virtual {v0, v1, v3}, LX/FxJ;->a(Landroid/content/Context;Landroid/os/Bundle;)LX/FxI;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->x:LX/FxI;

    .line 2305120
    new-instance v0, LX/FxQ;

    invoke-direct {v0, p0}, LX/FxQ;-><init>(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->B:LX/2yQ;

    .line 2305121
    new-instance v0, LX/FxR;

    invoke-direct {v0, p0}, LX/FxR;-><init>(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->C:LX/2dD;

    .line 2305122
    new-instance v0, LX/FxS;

    invoke-direct {v0, p0}, LX/FxS;-><init>(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->v:Landroid/view/View$OnClickListener;

    .line 2305123
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2305124
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    .line 2305125
    :cond_0
    if-nez p1, :cond_3

    .line 2305126
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->n:LX/0ad;

    sget-short v1, LX/0wf;->A:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2305127
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2305128
    const-string v1, "fav_media_extra"

    invoke-static {v0, v1}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 2305129
    if-eqz v0, :cond_4

    .line 2305130
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_4

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;

    .line 2305131
    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    const v6, 0x4ed245b

    if-ne v5, v6, :cond_1

    .line 2305132
    new-instance v5, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;

    invoke-direct {v5, v7, v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;-><init>(Landroid/net/Uri;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;)V

    .line 2305133
    new-instance v1, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    const/4 v6, 0x1

    invoke-direct {v1, v6, v5, v7}, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;-><init>(ILcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;)V

    .line 2305134
    :goto_1
    iget-object v5, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2305135
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 2305136
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;->e()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, LX/Fx4;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    move-result-object v5

    .line 2305137
    new-instance v1, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    invoke-direct {v1, v2, v7, v5}, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;-><init>(ILcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;)V

    goto :goto_1

    .line 2305138
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2305139
    const-string v1, "fav_photos_extra"

    invoke-static {v0, v1}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 2305140
    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    .line 2305141
    if-eqz v3, :cond_4

    .line 2305142
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_2
    if-ge v1, v4, :cond_4

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    .line 2305143
    new-instance v5, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    invoke-direct {v5, v2, v7, v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;-><init>(ILcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;)V

    .line 2305144
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2305145
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2305146
    :cond_3
    const-string v0, "saved_fav_photos"

    invoke-static {p1, v0}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    .line 2305147
    const-string v0, "index_of_photo_to_be_replaced"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->D:I

    .line 2305148
    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    .line 2305149
    new-instance v0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f021af6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/16 v2, 0x3e8

    invoke-direct {v0, v1, v2}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->E:Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    .line 2305150
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2305151
    return-void
.end method

.method public a(Landroid/view/View;II)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2305152
    new-instance v0, LX/G6g;

    rem-int/lit8 v1, p2, 0x3

    div-int/lit8 v2, p2, 0x3

    invoke-direct {v0, v1, v2, v3, v3}, LX/G6g;-><init>(IIII)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2305153
    return-void
.end method

.method public a(Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;)V
    .locals 2

    .prologue
    .line 2305154
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2305155
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2305156
    invoke-direct {p0, v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->e(I)V

    .line 2305157
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->l()V

    .line 2305158
    :cond_0
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v1, 0x3

    .line 2305159
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->u:Landroid/view/ViewGroup;

    check-cast v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;

    .line 2305160
    invoke-virtual {v0, v1, v1}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->a(II)V

    .line 2305161
    const/4 v1, 0x1

    .line 2305162
    iput-boolean v1, v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->c:Z

    .line 2305163
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0e10

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2305164
    invoke-virtual {v0, v1, v1}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->b(II)V

    .line 2305165
    return-void
.end method

.method public c()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 2305166
    const v0, 0x7f0314cd

    return v0
.end method

.method public d(I)I
    .locals 0

    .prologue
    .line 2305167
    return p1
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 2305168
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2305169
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FxZ;

    iget-object v0, v0, LX/FxZ;->a:Landroid/view/View;

    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(Landroid/view/View;II)V

    .line 2305170
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2305171
    :cond_0
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 2305172
    const/4 v0, 0x0

    return v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 2305173
    const/4 v0, 0x0

    return v0
.end method

.method public l()V
    .locals 0

    .prologue
    .line 2305174
    return-void
.end method

.method public m()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 2305175
    const v0, 0x7f0314d3

    return v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 2305176
    const/4 v0, 0x1

    return v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x17958e39

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2305177
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2305178
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0d00bc

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/0h5;

    iput-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->A:LX/0h5;

    .line 2305179
    invoke-static {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)LX/63b;

    move-result-object v1

    invoke-virtual {v1}, LX/63b;->a()LX/0Px;

    move-result-object v1

    .line 2305180
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->A:LX/0h5;

    invoke-interface {v2, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2305181
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->A:LX/0h5;

    new-instance v2, LX/FxV;

    invoke-direct {v2, p0}, LX/FxV;-><init>(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V

    invoke-interface {v1, v2}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2305182
    const/16 v1, 0x2b

    const v2, 0x56ada900

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 2305183
    if-eq p2, v1, :cond_1

    .line 2305184
    iput v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->D:I

    .line 2305185
    :cond_0
    :goto_0
    return-void

    .line 2305186
    :cond_1
    const/4 v7, 0x0

    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 2305187
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->H:Z

    .line 2305188
    new-instance v1, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    const/4 v2, 0x0

    const/4 v8, 0x0

    const/4 p2, 0x0

    .line 2305189
    invoke-static {p2, p2}, LX/Fx4;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    move-result-object p2

    move-object p2, p2

    .line 2305190
    invoke-direct {v1, v2, v8, p2}, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;-><init>(ILcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;)V

    move-object v2, v1

    .line 2305191
    iget v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->D:I

    if-ne v1, v6, :cond_b

    .line 2305192
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->k()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2305193
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v1, v5, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2305194
    iput v5, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->z:I

    .line 2305195
    iput-object v7, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->y:Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    .line 2305196
    :goto_1
    invoke-static {p0, v5}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->h(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;I)LX/FxZ;

    move-result-object v1

    .line 2305197
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->n()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2305198
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->u:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 2305199
    :cond_2
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->u:Landroid/view/ViewGroup;

    iget-object v5, v1, LX/FxZ;->a:Landroid/view/View;

    iget v6, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->z:I

    invoke-virtual {v2, v5, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 2305200
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    iget v5, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->z:I

    invoke-interface {v2, v5, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2305201
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->d()V

    .line 2305202
    :goto_2
    iget v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->z:I

    invoke-static {p0, v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->f(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;I)V

    .line 2305203
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 2305204
    :pswitch_0
    iput-boolean v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->H:Z

    .line 2305205
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "photo"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2305206
    if-nez v0, :cond_3

    .line 2305207
    invoke-static {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->x(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V

    goto :goto_0

    .line 2305208
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    .line 2305209
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a$redex0(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2305210
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/Fx4;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    move-result-object v0

    .line 2305211
    new-instance v1, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    invoke-direct {v1, v3, v4, v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;-><init>(ILcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;)V

    .line 2305212
    invoke-static {p0, v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->b(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;)V

    goto/16 :goto_0

    .line 2305213
    :pswitch_1
    iput-boolean v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->H:Z

    .line 2305214
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->v()V

    .line 2305215
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_media_items"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2305216
    if-eqz v0, :cond_6

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 2305217
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-direct {p0, v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(Lcom/facebook/ipc/media/MediaItem;)V

    goto/16 :goto_0

    .line 2305218
    :cond_6
    invoke-static {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->x(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V

    goto/16 :goto_0

    .line 2305219
    :pswitch_2
    iput-boolean v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->H:Z

    .line 2305220
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_media_items"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2305221
    if-eqz v0, :cond_7

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 2305222
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->v()V

    .line 2305223
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-direct {p0, v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(Lcom/facebook/ipc/media/MediaItem;)V

    goto/16 :goto_0

    .line 2305224
    :cond_7
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "suggested_media_fb_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2305225
    if-eqz v0, :cond_0

    .line 2305226
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "suggested_media_uri"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2305227
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->e()Z

    move-result v2

    if-nez v2, :cond_8

    if-nez v1, :cond_9

    .line 2305228
    :cond_8
    invoke-static {p0, v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a$redex0(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2305229
    :cond_9
    invoke-static {v0, v1}, LX/Fx4;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    move-result-object v0

    .line 2305230
    new-instance v1, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    invoke-direct {v1, v3, v4, v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;-><init>(ILcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;)V

    .line 2305231
    invoke-static {p0, v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->b(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;)V

    goto/16 :goto_0

    .line 2305232
    :cond_a
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2305233
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->z:I

    .line 2305234
    iput-object v7, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->y:Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    goto/16 :goto_1

    .line 2305235
    :cond_b
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    iget v5, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->D:I

    invoke-interface {v1, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    iput-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->y:Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    .line 2305236
    iget v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->D:I

    iput v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->z:I

    .line 2305237
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    iget v5, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->z:I

    invoke-interface {v1, v5, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2305238
    iput v6, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->D:I

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x7e64f7f0

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2305239
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->c()I

    move-result v0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2305240
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2305241
    const v0, 0x7f0d185e

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    .line 2305242
    const v3, 0x7f02078c

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2305243
    :cond_0
    const v0, 0x7f0d185f

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2305244
    iget-object v3, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->l:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2305245
    const v3, 0x7f0815f7

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object p1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->m:Ljava/lang/String;

    invoke-static {v3, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2305246
    :goto_0
    move-object v3, v3

    .line 2305247
    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2305248
    const v0, 0x7f0d185e

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2305249
    iget-object p1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->p:LX/1e4;

    iget-object v3, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->l:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "work_list"

    :goto_1
    invoke-virtual {p1, v3}, LX/1e4;->a(Ljava/lang/String;)I

    move-result v3

    move v3, v3

    .line 2305250
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2305251
    const v0, 0x7f0d2f2b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->t:Lcom/facebook/fig/button/FigButton;

    .line 2305252
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->t:Lcom/facebook/fig/button/FigButton;

    iget-object v3, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->b:LX/23P;

    invoke-virtual {v0, v3}, Lcom/facebook/fig/button/FigButton;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2305253
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->t:Lcom/facebook/fig/button/FigButton;

    const-string v3, "button"

    invoke-virtual {v0, v3}, Lcom/facebook/fig/button/FigButton;->setTag(Ljava/lang/Object;)V

    .line 2305254
    const v0, 0x7f0d2f2c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->u:Landroid/view/ViewGroup;

    .line 2305255
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->b()V

    .line 2305256
    invoke-virtual {p0, v2}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(Landroid/view/View;)V

    .line 2305257
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->u:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2305258
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2305259
    const/4 v0, 0x0

    move v3, v0

    :goto_2
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 2305260
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    .line 2305261
    iget p1, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->a:I

    move v0, p1

    .line 2305262
    invoke-static {p0, v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->h(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;I)LX/FxZ;

    move-result-object v0

    .line 2305263
    iget-object p1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2305264
    invoke-static {p0, v3}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->f(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;I)V

    .line 2305265
    iget-object p1, v0, LX/FxZ;->a:Landroid/view/View;

    iget-object p2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-virtual {p0, p1, v3, p2}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(Landroid/view/View;II)V

    .line 2305266
    iget-object p1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->u:Landroid/view/ViewGroup;

    iget-object v0, v0, LX/FxZ;->a:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2305267
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 2305268
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2305269
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_3
    const/4 v3, 0x5

    if-ge v0, v3, :cond_2

    .line 2305270
    invoke-static {p0, v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->g(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;I)V

    .line 2305271
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2305272
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->l()V

    .line 2305273
    invoke-static {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->A(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V

    .line 2305274
    const/16 v0, 0x2b

    const v3, 0x1aa25011

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2

    :cond_3
    const v3, 0x7f0815f6

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    :cond_4
    const-string v3, "everyone"

    goto/16 :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2305275
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2305276
    iget-boolean v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->H:Z

    if-eqz v0, :cond_0

    .line 2305277
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(ZLcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;)V

    .line 2305278
    :cond_0
    const-string v0, "saved_fav_photos"

    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 2305279
    const-string v0, "index_of_photo_to_be_replaced"

    iget v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->D:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2305280
    return-void
.end method
