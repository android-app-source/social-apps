.class public Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G0q;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field private final d:LX/G0r;

.field private final e:LX/Fx6;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0yc;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2304452
    const-class v0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/G0r;LX/Fx6;Landroid/content/Context;LX/0Ot;LX/0yc;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/G0q;",
            ">;",
            "LX/G0r;",
            "LX/Fx6;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0yc;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2304444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2304445
    iput-object p1, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->b:LX/0Or;

    .line 2304446
    iput-object p2, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->d:LX/G0r;

    .line 2304447
    iput-object p3, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->e:LX/Fx6;

    .line 2304448
    iput-object p4, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->c:Landroid/content/Context;

    .line 2304449
    iput-object p5, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->f:LX/0Ot;

    .line 2304450
    iput-object p6, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->g:LX/0yc;

    .line 2304451
    return-void
.end method

.method public static synthetic a(Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;LX/0Px;LX/Fx7;)LX/0Px;
    .locals 4

    .prologue
    .line 2304437
    const/4 v1, 0x0

    .line 2304438
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    move v0, v1

    .line 2304439
    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 2304440
    const/4 v3, 0x1

    new-array v3, v3, [LX/1U8;

    invoke-interface {p2, p1, v0}, LX/Fx7;->a(LX/0Px;I)LX/5vo;

    move-result-object p0

    aput-object p0, v3, v1

    invoke-virtual {v2, v3}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    .line 2304441
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2304442
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2304443
    return-object v0
.end method

.method private a(LX/1U8;LX/5vn;LX/0Px;LX/Fx7;)Landroid/view/View$OnClickListener;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1U8;",
            "LX/5vn;",
            "LX/0Px",
            "<+TT;>;",
            "LX/Fx7",
            "<TT;>;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 2304453
    new-instance v0, LX/FxA;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/FxA;-><init>(Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;LX/1U8;LX/5vn;LX/0Px;LX/Fx7;)V

    return-object v0
.end method

.method public static b(LX/0QB;)Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;
    .locals 8

    .prologue
    .line 2304432
    new-instance v0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;

    const/16 v1, 0x369e

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0}, LX/G0r;->a(LX/0QB;)LX/G0r;

    move-result-object v2

    check-cast v2, LX/G0r;

    .line 2304433
    new-instance v6, LX/Fx6;

    const/16 v3, 0x509

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {p0}, LX/Fx4;->a(LX/0QB;)LX/Fx4;

    move-result-object v3

    check-cast v3, LX/Fx4;

    invoke-static {p0}, LX/1BK;->a(LX/0QB;)LX/1BK;

    move-result-object v4

    check-cast v4, LX/1BK;

    invoke-static {p0}, LX/EQL;->a(LX/0QB;)LX/EQL;

    move-result-object v5

    check-cast v5, LX/EQL;

    invoke-direct {v6, v7, v3, v4, v5}, LX/Fx6;-><init>(LX/0Or;LX/Fx4;LX/1BK;LX/EQL;)V

    .line 2304434
    move-object v3, v6

    .line 2304435
    check-cast v3, LX/Fx6;

    const-class v4, Landroid/content/Context;

    invoke-interface {p0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x259

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {p0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v6

    check-cast v6, LX/0yc;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;-><init>(LX/0Or;LX/G0r;LX/Fx6;Landroid/content/Context;LX/0Ot;LX/0yc;)V

    .line 2304436
    return-object v0
.end method

.method public static synthetic b(Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;LX/0Px;LX/Fx7;)Ljava/util/List;
    .locals 2

    .prologue
    .line 2304426
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2304427
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p0

    if-ge v0, p0, :cond_0

    .line 2304428
    invoke-interface {p2, p1, v0}, LX/Fx7;->b(LX/0Px;I)LX/5vn;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2304429
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2304430
    :cond_0
    move-object v0, v1

    .line 2304431
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/fig/mediagrid/FigMediaGrid;LX/0Px;LX/Fx7;)V
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lcom/facebook/fig/mediagrid/FigMediaGrid;",
            "LX/0Px",
            "<+TT;>;",
            "LX/Fx7",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 2304392
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2304393
    invoke-static {}, LX/6WP;->newBuilder()LX/6WO;

    move-result-object v0

    const/4 v2, 0x6

    .line 2304394
    iput v2, v0, LX/6WO;->c:I

    .line 2304395
    move-object v2, v0

    .line 2304396
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 2304397
    invoke-interface {p4, p3, v0}, LX/Fx7;->a(LX/0Px;I)LX/5vo;

    move-result-object v3

    .line 2304398
    invoke-interface {p4, p3, v0}, LX/Fx7;->b(LX/0Px;I)LX/5vn;

    move-result-object v4

    .line 2304399
    iget-object v5, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->d:LX/G0r;

    invoke-virtual {v5, v3, v4}, LX/G0r;->a(LX/1U8;LX/5vn;)LX/1bf;

    move-result-object v3

    .line 2304400
    iget-object v5, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->d:LX/G0r;

    sget-object v6, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v6, v3}, LX/G0r;->a(Lcom/facebook/common/callercontext/CallerContext;LX/1bf;)LX/1aZ;

    move-result-object v5

    .line 2304401
    invoke-virtual {v1, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2304402
    iget-object v6, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->e:LX/Fx6;

    invoke-virtual {v6, p1, v5, v3}, LX/Fx6;->a(Ljava/lang/String;LX/1aZ;LX/1bf;)V

    .line 2304403
    iget-object v3, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->d:LX/G0r;

    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v5

    invoke-virtual {v3, v5, v4, v2}, LX/G0r;->a(ILX/5vn;LX/6WO;)V

    .line 2304404
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2304405
    :cond_0
    new-instance v0, LX/Fx9;

    invoke-direct {v0, p0, p4, p3, p2}, LX/Fx9;-><init>(Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;LX/Fx7;LX/0Px;Lcom/facebook/fig/mediagrid/FigMediaGrid;)V

    invoke-virtual {p2, v0}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a(LX/6WM;)Lcom/facebook/fig/mediagrid/FigMediaGrid;

    .line 2304406
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v2}, LX/6WO;->a()LX/6WP;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a(LX/0Px;LX/6WP;)Lcom/facebook/fig/mediagrid/FigMediaGrid;

    .line 2304407
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;LX/0Px;LX/Fx7;)V
    .locals 9
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;",
            "LX/0Px",
            "<+TT;>;",
            "LX/Fx7",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 2304408
    invoke-virtual {p2}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;->removeAllViews()V

    .line 2304409
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->e:LX/Fx6;

    .line 2304410
    if-nez p1, :cond_2

    .line 2304411
    :goto_0
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 2304412
    invoke-interface {p4, p3, v0}, LX/Fx7;->a(LX/0Px;I)LX/5vo;

    move-result-object v1

    .line 2304413
    invoke-interface {p4, p3, v0}, LX/Fx7;->b(LX/0Px;I)LX/5vn;

    move-result-object v2

    .line 2304414
    iget-object v3, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->d:LX/G0r;

    invoke-virtual {v3, v1, v2}, LX/G0r;->a(LX/1U8;LX/5vn;)LX/1bf;

    move-result-object v3

    .line 2304415
    iget-object v4, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->d:LX/G0r;

    sget-object v5, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v5, v3}, LX/G0r;->a(Lcom/facebook/common/callercontext/CallerContext;LX/1bf;)LX/1aZ;

    move-result-object v4

    .line 2304416
    iget-object v5, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->e:LX/Fx6;

    iget-object v6, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->c:Landroid/content/Context;

    invoke-interface {v1}, LX/5vo;->k()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1}, LX/Fx6;->a(LX/1U8;)Landroid/graphics/PointF;

    move-result-object v8

    invoke-virtual {v5, v6, v4, v7, v8}, LX/Fx6;->a(Landroid/content/Context;LX/1aZ;Ljava/lang/String;Landroid/graphics/PointF;)Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v5

    .line 2304417
    iget-object v6, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->e:LX/Fx6;

    invoke-virtual {v6, p1, v4, v3}, LX/Fx6;->a(Ljava/lang/String;LX/1aZ;LX/1bf;)V

    .line 2304418
    iget-object v3, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->g:LX/0yc;

    invoke-virtual {v3}, LX/0yc;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2304419
    sget-object v3, LX/Fx6;->a:LX/G6g;

    invoke-virtual {v5, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2304420
    :goto_2
    invoke-direct {p0, v1, v2, p3, p4}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->a(LX/1U8;LX/5vn;LX/0Px;LX/Fx7;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2304421
    invoke-virtual {p2, v5}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;->addView(Landroid/view/View;)V

    .line 2304422
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2304423
    :cond_0
    invoke-static {v2}, LX/Fx6;->a(LX/5vn;)LX/G6g;

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2

    .line 2304424
    :cond_1
    return-void

    .line 2304425
    :cond_2
    iget-object v1, v0, LX/Fx6;->d:LX/1BK;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "featured_photos_unit"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1BL;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
