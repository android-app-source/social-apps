.class public Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;
.super Lcom/facebook/widget/mosaic/MosaicGridLayout;
.source ""

# interfaces
.implements LX/1a7;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2304530
    invoke-direct {p0, p1}, Lcom/facebook/widget/mosaic/MosaicGridLayout;-><init>(Landroid/content/Context;)V

    .line 2304531
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;->a()V

    .line 2304532
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2304533
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/mosaic/MosaicGridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2304534
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;->a()V

    .line 2304535
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2304536
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/mosaic/MosaicGridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2304537
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;->a()V

    .line 2304538
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2304539
    const/4 v0, 0x1

    .line 2304540
    iput-boolean v0, p0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->c:Z

    .line 2304541
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0e10

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2304542
    invoke-virtual {p0, v0, v0}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->b(II)V

    .line 2304543
    return-void
.end method


# virtual methods
.method public final cr_()Z
    .locals 1

    .prologue
    .line 2304544
    const/4 v0, 0x1

    return v0
.end method
