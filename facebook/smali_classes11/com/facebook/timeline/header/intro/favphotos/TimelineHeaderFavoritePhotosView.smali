.class public Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Fxj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FxB;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2304371
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2304372
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->b()V

    .line 2304373
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2304368
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2304369
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->b()V

    .line 2304370
    return-void
.end method

.method private static a(Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;LX/23P;Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;LX/Fxj;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;",
            "LX/23P;",
            "Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;",
            "LX/Fxj;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FxB;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2304367
    iput-object p1, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->a:LX/23P;

    iput-object p2, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->b:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;

    iput-object p3, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->c:LX/Fxj;

    iput-object p4, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->d:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->e:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;

    invoke-static {v5}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v1

    check-cast v1, LX/23P;

    invoke-static {v5}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->b(LX/0QB;)Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;

    move-result-object v2

    check-cast v2, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;

    invoke-static {v5}, LX/Fxj;->a(LX/0QB;)LX/Fxj;

    move-result-object v3

    check-cast v3, LX/Fxj;

    const/16 v4, 0x259

    invoke-static {v5, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v6, 0x367f

    invoke-static {v5, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->a(Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;LX/23P;Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;LX/Fxj;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2304359
    const-class v0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2304360
    const v0, 0x7f0314e0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2304361
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->setOrientation(I)V

    .line 2304362
    const v0, 0x7f0d2f3f

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2304363
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->f:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->a:LX/23P;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2304364
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->f:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2304365
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->c()V

    .line 2304366
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 2304356
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0e07

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2304357
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FxB;

    const v1, 0x7f0d2f3e

    invoke-virtual {p0, v1}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    invoke-virtual {v0, v1, v2}, LX/FxB;->a(Landroid/view/ViewStub;I)V

    .line 2304358
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2304349
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FxB;

    .line 2304350
    iget-object v1, v0, LX/FxB;->b:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;

    move-object v0, v1

    .line 2304351
    if-eqz v0, :cond_0

    .line 2304352
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FxB;

    .line 2304353
    iget-object v1, v0, LX/FxB;->b:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;

    move-object v0, v1

    .line 2304354
    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;->removeAllViews()V

    .line 2304355
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLInterfaces$FavoritePhoto;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2304330
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FxB;

    .line 2304331
    iget-object v1, v0, LX/FxB;->b:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;

    move-object v0, v1

    .line 2304332
    if-eqz v0, :cond_0

    .line 2304333
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->b:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;

    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FxB;

    .line 2304334
    iget-object v2, v0, LX/FxB;->b:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;

    move-object v0, v2

    .line 2304335
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->c:LX/Fxj;

    invoke-virtual {v1, p1, v0, p2, v2}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->a(Ljava/lang/String;Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;LX/0Px;LX/Fx7;)V

    .line 2304336
    :goto_0
    return-void

    .line 2304337
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FxB;

    .line 2304338
    iget-object v1, v0, LX/FxB;->c:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    move-object v0, v1

    .line 2304339
    if-eqz v0, :cond_1

    .line 2304340
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->b:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;

    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FxB;

    .line 2304341
    iget-object v2, v0, LX/FxB;->c:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    move-object v0, v2

    .line 2304342
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->c:LX/Fxj;

    invoke-virtual {v1, p1, v0, p2, v2}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->a(Ljava/lang/String;Lcom/facebook/fig/mediagrid/FigMediaGrid;LX/0Px;LX/Fx7;)V

    goto :goto_0

    .line 2304343
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Favorite photo view not initialized"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setEditClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2304347
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2304348
    return-void
.end method

.method public setEditable(Z)V
    .locals 2

    .prologue
    .line 2304344
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->f:Lcom/facebook/resources/ui/FbTextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2304345
    return-void

    .line 2304346
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
