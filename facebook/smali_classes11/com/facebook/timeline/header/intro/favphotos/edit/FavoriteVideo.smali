.class public Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2304783
    new-instance v0, LX/FxM;

    invoke-direct {v0}, LX/FxM;-><init>()V

    sput-object v0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;)V
    .locals 0
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2304788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2304789
    iput-object p1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;->a:Landroid/net/Uri;

    .line 2304790
    iput-object p2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;

    .line 2304791
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2304792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2304793
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;->a:Landroid/net/Uri;

    .line 2304794
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;

    .line 2304795
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2304787
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2304784
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2304785
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2304786
    return-void
.end method
