.class public Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;
.super Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;
.source ""


# instance fields
.field public A:LX/Fxg;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public B:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public C:LX/1R7;

.field public D:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;",
            ">;"
        }
    .end annotation
.end field

.field private E:Lcom/facebook/resources/ui/FbTextView;

.field public F:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

.field private G:Landroid/widget/FrameLayout;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:LX/Fxf;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private I:[Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public J:I

.field public K:[F

.field public w:LX/FxK;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public x:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FxC;",
            ">;"
        }
    .end annotation
.end field

.field public y:LX/Fx2;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public z:LX/Fx3;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2305909
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;-><init>()V

    .line 2305910
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, 0x5

    .line 2305911
    new-array v0, v4, [Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->I:[Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2305912
    const v0, 0x7f0d2f2d

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2305913
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_1

    .line 2305914
    new-instance v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2305915
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget v5, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->J:I

    iget v6, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->J:I

    invoke-direct {v3, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2305916
    const/16 v5, 0x10

    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2305917
    invoke-virtual {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2305918
    new-instance v3, LX/1Uo;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v3, v5}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    new-instance v5, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a054c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-direct {v5, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v3, v5}, LX/1Uo;->g(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v3

    .line 2305919
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f02195e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    check-cast v5, Landroid/graphics/drawable/LayerDrawable;

    .line 2305920
    const v6, 0x7f0d31f5

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 2305921
    new-instance v7, Landroid/graphics/ColorMatrixColorFilter;

    iget-object p1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->K:[F

    invoke-direct {v7, p1}, Landroid/graphics/ColorMatrixColorFilter;-><init>([F)V

    .line 2305922
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 2305923
    move-object v5, v5

    .line 2305924
    iput-object v5, v3, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2305925
    move-object v3, v3

    .line 2305926
    invoke-virtual {v3}, LX/1Uo;->u()LX/1af;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2305927
    const-string v3, "add_top_row_add_button"

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setTag(Ljava/lang/Object;)V

    .line 2305928
    move-object v2, v2

    .line 2305929
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2305930
    iget-object v3, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->I:[Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    aput-object v2, v3, v1

    .line 2305931
    const/4 v2, 0x4

    if-ge v1, v2, :cond_0

    .line 2305932
    new-instance v2, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2305933
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget v5, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->J:I

    iget v6, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->J:I

    invoke-direct {v3, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2305934
    const/high16 v5, 0x3f800000    # 1.0f

    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2305935
    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2305936
    move-object v2, v2

    .line 2305937
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2305938
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 2305939
    :cond_1
    return-void
.end method

.method private v()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 2305940
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->I:[Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v0, :cond_1

    .line 2305941
    :cond_0
    return-void

    :cond_1
    move v1, v2

    .line 2305942
    :goto_0
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2305943
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    .line 2305944
    iget v3, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->a:I

    move v3, v3

    .line 2305945
    if-nez v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 2305946
    iget-object v3, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->I:[Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    aget-object v3, v3, v1

    .line 2305947
    invoke-virtual {v3, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2305948
    iget-object v4, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->b:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    move-object v0, v4

    .line 2305949
    invoke-static {v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;)Landroid/net/Uri;

    move-result-object v0

    .line 2305950
    sget-object v4, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2305951
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v3, v2

    .line 2305952
    goto :goto_1

    .line 2305953
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->I:[Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    array-length v0, v0

    if-ge v1, v0, :cond_0

    .line 2305954
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->I:[Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    aget-object v0, v0, v1

    .line 2305955
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->v:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2305956
    sget-object v2, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v5, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2305957
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public static x(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;)LX/0Px;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLInterfaces$SuggestedPhoto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2305958
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2305959
    const/4 v0, 0x2

    move v1, v0

    .line 2305960
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2305961
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2305962
    new-instance v3, LX/5wV;

    invoke-direct {v3}, LX/5wV;-><init>()V

    new-instance v4, LX/5vr;

    invoke-direct {v4}, LX/5vr;-><init>()V

    const-string v5, "f2f2f4"

    .line 2305963
    iput-object v5, v4, LX/5vr;->b:Ljava/lang/String;

    .line 2305964
    move-object v4, v4

    .line 2305965
    invoke-virtual {v4}, LX/5vr;->a()Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;

    move-result-object v4

    .line 2305966
    iput-object v4, v3, LX/5wV;->a:Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;

    .line 2305967
    move-object v3, v3

    .line 2305968
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 2305969
    new-instance v6, LX/186;

    const/16 v7, 0x80

    invoke-direct {v6, v7}, LX/186;-><init>(I)V

    .line 2305970
    iget-object v7, v3, LX/5wV;->a:Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;

    invoke-static {v6, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2305971
    invoke-virtual {v6, v10}, LX/186;->c(I)V

    .line 2305972
    invoke-virtual {v6, v9, v7}, LX/186;->b(II)V

    .line 2305973
    invoke-virtual {v6}, LX/186;->d()I

    move-result v7

    .line 2305974
    invoke-virtual {v6, v7}, LX/186;->d(I)V

    .line 2305975
    invoke-virtual {v6}, LX/186;->e()[B

    move-result-object v6

    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 2305976
    invoke-virtual {v7, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2305977
    new-instance v6, LX/15i;

    move-object v9, v8

    move-object v11, v8

    invoke-direct/range {v6 .. v11}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2305978
    new-instance v7, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$SuggestedPhotoModel;

    invoke-direct {v7, v6}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$SuggestedPhotoModel;-><init>(LX/15i;)V

    .line 2305979
    move-object v3, v7

    .line 2305980
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2305981
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2305982
    :cond_0
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private y()Z
    .locals 3

    .prologue
    .line 2305983
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->B:LX/0ad;

    sget-short v1, LX/0wf;->z:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method private z()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2305879
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FxZ;

    iget-object v0, v0, LX/FxZ;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->G:Landroid/widget/FrameLayout;

    if-ne v0, v2, :cond_0

    .line 2305880
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->H:LX/Fxf;

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 2305881
    iget-boolean v2, v0, LX/Fxf;->q:Z

    if-eqz v2, :cond_1

    .line 2305882
    :goto_0
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    move-object v0, v0

    .line 2305883
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2305884
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    move-object v0, v0

    .line 2305885
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FxZ;

    .line 2305886
    iget-object v2, v0, LX/FxZ;->a:Landroid/view/View;

    iget-object v3, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {p0, v2, v1, v3}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->a(Landroid/view/View;II)V

    .line 2305887
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->u:Landroid/view/ViewGroup;

    iget-object v0, v0, LX/FxZ;->a:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2305888
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2305889
    :cond_0
    return-void

    .line 2305890
    :cond_1
    iput-boolean v5, v0, LX/Fxf;->c:Z

    .line 2305891
    iput-object v3, v0, LX/Fxf;->d:Ljava/util/List;

    .line 2305892
    iput-object v3, v0, LX/Fxf;->e:Ljava/util/List;

    .line 2305893
    iput-object v3, v0, LX/Fxf;->f:Ljava/util/List;

    .line 2305894
    iput-object v3, v0, LX/Fxf;->g:Ljava/util/List;

    .line 2305895
    iput-object v3, v0, LX/Fxf;->j:Ljava/util/Map;

    .line 2305896
    iget-object v2, v0, LX/Fxf;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 2305897
    iget-object v4, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    invoke-virtual {v4}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->o()Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->removeView(Landroid/view/View;)V

    goto :goto_1

    .line 2305898
    :cond_2
    iget-object v2, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    .line 2305899
    iget-object v3, v2, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    move-object v2, v3

    .line 2305900
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FxZ;

    .line 2305901
    iget-object v4, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    invoke-virtual {v4}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->o()Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;

    move-result-object v4

    iget-object v2, v2, LX/FxZ;->a:Landroid/view/View;

    invoke-virtual {v4, v2}, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->removeView(Landroid/view/View;)V

    goto :goto_2

    .line 2305902
    :cond_3
    iget-object v2, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    invoke-virtual {v2}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->p()Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;->setVisibility(I)V

    .line 2305903
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/Fxf;->q:Z

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1
    .param p1    # Ljava/util/ArrayList;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLInterfaces$FavoritePhoto;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLInterfaces$FavoritePhoto;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2306003
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->w:LX/FxK;

    invoke-virtual {v0, p1}, LX/FxK;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "LX/4Ig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2305984
    if-lez p1, :cond_1

    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->w:LX/FxK;

    .line 2305985
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2305986
    invoke-virtual {v0, p1}, LX/FxK;->a(I)[LX/5vn;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 2305987
    new-instance v6, LX/4Ig;

    invoke-direct {v6}, LX/4Ig;-><init>()V

    invoke-interface {v5}, LX/5vn;->c()D

    move-result-wide v7

    double-to-int v7, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 2305988
    const-string v8, "x"

    invoke-virtual {v6, v8, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2305989
    move-object v6, v6

    .line 2305990
    invoke-interface {v5}, LX/5vn;->d()D

    move-result-wide v7

    double-to-int v7, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 2305991
    const-string v8, "y"

    invoke-virtual {v6, v8, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2305992
    move-object v6, v6

    .line 2305993
    invoke-interface {v5}, LX/5vn;->b()D

    move-result-wide v7

    double-to-int v7, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 2305994
    const-string v8, "width"

    invoke-virtual {v6, v8, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2305995
    move-object v6, v6

    .line 2305996
    invoke-interface {v5}, LX/5vn;->a()D

    move-result-wide v7

    double-to-int v5, v7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 2305997
    const-string v7, "height"

    invoke-virtual {v6, v7, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2305998
    move-object v5, v6

    .line 2305999
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2306000
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2306001
    :cond_0
    move-object v0, v2

    .line 2306002
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(LX/FxZ;Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;)V
    .locals 2

    .prologue
    .line 2306025
    invoke-super {p0, p1, p2}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(LX/FxZ;Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;)V

    .line 2306026
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->H:LX/Fxf;

    if-eqz v0, :cond_0

    .line 2306027
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->H:LX/Fxf;

    iget-object v1, p1, LX/FxZ;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2306028
    iget-object p0, v0, LX/Fxf;->b:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2306029
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x5

    const/4 v0, 0x0

    .line 2306030
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    move-object v4, p0

    check-cast v4, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    new-instance v6, LX/FxK;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {v6, v5}, LX/FxK;-><init>(LX/03V;)V

    move-object v5, v6

    check-cast v5, LX/FxK;

    const/16 v6, 0x3680

    invoke-static {v1, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v1}, LX/Fx2;->a(LX/0QB;)LX/Fx2;

    move-result-object v7

    check-cast v7, LX/Fx2;

    invoke-static {v1}, LX/Fx3;->a(LX/0QB;)LX/Fx3;

    move-result-object v8

    check-cast v8, LX/Fx3;

    const-class v9, LX/Fxg;

    invoke-interface {v1, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/Fxg;

    invoke-static {v1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    iput-object v5, v4, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->w:LX/FxK;

    iput-object v6, v4, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->x:LX/0Ot;

    iput-object v7, v4, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->y:LX/Fx2;

    iput-object v8, v4, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->z:LX/Fx3;

    iput-object v9, v4, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->A:LX/Fxg;

    iput-object v1, v4, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->B:LX/0ad;

    .line 2306031
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0e0d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->J:I

    .line 2306032
    const/4 v7, 0x0

    .line 2306033
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a008a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 2306034
    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v2

    .line 2306035
    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v4

    .line 2306036
    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    .line 2306037
    const/16 v5, 0x14

    new-array v5, v5, [F

    const/4 v6, 0x0

    aput v7, v5, v6

    const/4 v6, 0x1

    aput v7, v5, v6

    const/4 v6, 0x2

    aput v7, v5, v6

    const/4 v6, 0x3

    aput v7, v5, v6

    const/4 v6, 0x4

    int-to-float v2, v2

    aput v2, v5, v6

    const/4 v2, 0x5

    aput v7, v5, v2

    const/4 v2, 0x6

    aput v7, v5, v2

    const/4 v2, 0x7

    aput v7, v5, v2

    const/16 v2, 0x8

    aput v7, v5, v2

    const/16 v2, 0x9

    int-to-float v4, v4

    aput v4, v5, v2

    const/16 v2, 0xa

    aput v7, v5, v2

    const/16 v2, 0xb

    aput v7, v5, v2

    const/16 v2, 0xc

    aput v7, v5, v2

    const/16 v2, 0xd

    aput v7, v5, v2

    const/16 v2, 0xe

    int-to-float v1, v1

    aput v1, v5, v2

    const/16 v1, 0xf

    aput v7, v5, v1

    const/16 v1, 0x10

    aput v7, v5, v1

    const/16 v1, 0x11

    aput v7, v5, v1

    const/16 v1, 0x12

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v5, v1

    const/16 v1, 0x13

    aput v7, v5, v1

    iput-object v5, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->K:[F

    .line 2306038
    invoke-static {v3}, LX/1R7;->a(I)LX/1R7;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->C:LX/1R7;

    .line 2306039
    if-nez p1, :cond_0

    .line 2306040
    :goto_0
    if-ge v0, v3, :cond_1

    .line 2306041
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->C:LX/1R7;

    invoke-virtual {v1, v0}, LX/1R7;->b(I)V

    .line 2306042
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2306043
    :cond_0
    const-string v1, "permutation"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v1

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 2306044
    iget-object v4, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->C:LX/1R7;

    invoke-virtual {v4, v3}, LX/1R7;->b(I)V

    .line 2306045
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2306046
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 2306047
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->y()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2306048
    new-instance v0, LX/Fxf;

    invoke-direct {v0, p0}, LX/Fxf;-><init>(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;)V

    .line 2306049
    move-object v0, v0

    .line 2306050
    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->H:LX/Fxf;

    .line 2306051
    :cond_2
    invoke-super {p0, p1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(Landroid/os/Bundle;)V

    .line 2306052
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    .line 2306008
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2306009
    const v0, 0x7f0d2f2e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->G:Landroid/widget/FrameLayout;

    .line 2306010
    :cond_0
    new-instance v1, LX/0zw;

    const v0, 0x7f0d2f2f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->D:LX/0zw;

    .line 2306011
    const v0, 0x7f0d2f2a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->E:Lcom/facebook/resources/ui/FbTextView;

    .line 2306012
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->E:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->y()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f0815f9

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2306013
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->y:LX/Fx2;

    .line 2306014
    iget-boolean v1, v0, LX/Fwg;->b:Z

    move v1, v1

    .line 2306015
    if-nez v1, :cond_4

    invoke-virtual {v0}, LX/Fwg;->g()LX/0i1;

    move-result-object v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 2306016
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_3

    .line 2306017
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->t:Lcom/facebook/fig/button/FigButton;

    new-instance v1, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment$1;

    invoke-direct {v1, p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment$1;-><init>(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;)V

    invoke-virtual {v0, v1, v4, v5}, Lcom/facebook/fig/button/FigButton;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2306018
    :cond_1
    :goto_2
    return-void

    .line 2306019
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f0815fa

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2306020
    :cond_3
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->G:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->z:LX/Fx3;

    .line 2306021
    iget-boolean v1, v0, LX/Fwg;->b:Z

    move v1, v1

    .line 2306022
    if-nez v1, :cond_5

    invoke-virtual {v0}, LX/Fwg;->g()LX/0i1;

    move-result-object v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_3
    move v0, v1

    .line 2306023
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 2306024
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->u:Landroid/view/ViewGroup;

    new-instance v1, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment$2;

    invoke-direct {v1, p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment$2;-><init>(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/view/ViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public final a(Landroid/view/View;II)V
    .locals 1

    .prologue
    .line 2306004
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->w:LX/FxK;

    invoke-virtual {v0, p3}, LX/FxK;->a(I)[LX/5vn;

    move-result-object v0

    .line 2306005
    if-eqz v0, :cond_0

    .line 2306006
    aget-object v0, v0, p2

    invoke-static {v0}, LX/Fx6;->a(LX/5vn;)LX/G6g;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2306007
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;)V
    .locals 0

    .prologue
    .line 2305904
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->z()V

    .line 2305905
    invoke-super {p0, p1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;)V

    .line 2305906
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->d()V

    .line 2305907
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2305908
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2305857
    const v0, 0x7f0314ce

    return v0
.end method

.method public final d(I)I
    .locals 1

    .prologue
    .line 2305856
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->C:LX/1R7;

    invoke-virtual {v0, p1}, LX/1R7;->d(I)I

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2305855
    const/4 v0, 0x1

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 2305854
    const/4 v0, 0x1

    return v0
.end method

.method public final l()V
    .locals 13

    .prologue
    .line 2305832
    const/4 v6, 0x0

    .line 2305833
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2305834
    :cond_0
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->F:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    if-nez v2, :cond_1

    .line 2305835
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->D:LX/0zw;

    invoke-virtual {v2}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    iput-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->F:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    .line 2305836
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->F:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    const-string v3, "empty_state_view"

    invoke-virtual {v2, v3}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->setTag(Ljava/lang/Object;)V

    .line 2305837
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->x:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FxC;

    iget-object v3, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->F:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    invoke-static {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->x(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;)LX/0Px;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->v:Landroid/view/View$OnClickListener;

    const/4 v8, 0x0

    .line 2305838
    move-object v7, v2

    move-object v9, v3

    move-object v10, v4

    move-object v11, v5

    move-object v12, v8

    invoke-virtual/range {v7 .. v12}, LX/FxC;->a(Ljava/lang/String;Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;LX/0Px;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 2305839
    invoke-virtual {v3}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->b()V

    .line 2305840
    invoke-virtual {v3}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->a()V

    .line 2305841
    invoke-virtual {v3}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->c()V

    .line 2305842
    invoke-virtual {v3, v5}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2305843
    :cond_1
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->u:Landroid/view/ViewGroup;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2305844
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->F:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    invoke-virtual {v2, v6}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->setVisibility(I)V

    .line 2305845
    :goto_0
    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->E:Lcom/facebook/resources/ui/FbTextView;

    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2305846
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->v()V

    .line 2305847
    return-void

    .line 2305848
    :cond_2
    const/16 v0, 0x8

    goto :goto_1

    .line 2305849
    :cond_3
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->u:Landroid/view/ViewGroup;

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2305850
    iget-object v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->D:LX/0zw;

    .line 2305851
    if-eqz v2, :cond_4

    .line 2305852
    invoke-virtual {v2}, LX/0zw;->c()V

    .line 2305853
    :cond_4
    goto :goto_0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 2305831
    const v0, 0x7f0314d4

    return v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 2305830
    const/4 v0, 0x0

    return v0
.end method

.method public final o()Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2305858
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->G:Landroid/widget/FrameLayout;

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2305859
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2305860
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->z()V

    .line 2305861
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2305862
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x33c7520e    # -4.8412616E7f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2305863
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 2305864
    invoke-direct {p0, v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->b(Landroid/view/View;)V

    .line 2305865
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->v()V

    .line 2305866
    const/16 v2, 0x2b

    const v3, -0x55503c1d

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2305867
    invoke-super {p0, p1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2305868
    const-string v0, "permutation"

    iget-object v1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->C:LX/1R7;

    invoke-virtual {v1}, LX/1R7;->d()[I

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 2305869
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2305870
    invoke-super {p0, p1, p2}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2305871
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->H:LX/Fxf;

    if-eqz v0, :cond_0

    .line 2305872
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->G:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0vv;->e(Landroid/view/View;I)V

    .line 2305873
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->H:LX/Fxf;

    .line 2305874
    iget-object v1, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->o()Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;

    move-result-object v1

    .line 2305875
    iput-object v0, v1, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->a:LX/Fxf;

    .line 2305876
    iget-object v1, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->o()Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;

    move-result-object v1

    new-instance p0, LX/Fxb;

    invoke-direct {p0, v0}, LX/Fxb;-><init>(LX/Fxf;)V

    invoke-virtual {v1, p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2305877
    :cond_0
    return-void
.end method

.method public final p()Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;
    .locals 1

    .prologue
    .line 2305878
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->u:Landroid/view/ViewGroup;

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;

    return-object v0
.end method
