.class public Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FxB;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2304573
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2304574
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->d()V

    .line 2304575
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2304596
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2304597
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->d()V

    .line 2304598
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    const/16 v1, 0x367f

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->a:LX/0Ot;

    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 2304599
    const-class v0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2304600
    const v0, 0x7f0314e2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2304601
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->setOrientation(I)V

    .line 2304602
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->e()V

    .line 2304603
    const v0, 0x7f0d2f41

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->b:Landroid/view/View;

    .line 2304604
    const v0, 0x7f0d2f42

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->c:Landroid/view/View;

    .line 2304605
    const v0, 0x7f0d2f43

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2304606
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 2304607
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FxB;

    const v1, 0x7f0d2f40

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/FxB;->a(Landroid/view/ViewStub;I)V

    .line 2304608
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2304592
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2304593
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2304594
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->d:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2304595
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2304590
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->e:Z

    .line 2304591
    return-void
.end method

.method public getSuggestedPhotosFigView()Lcom/facebook/fig/mediagrid/FigMediaGrid;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2304587
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FxB;

    .line 2304588
    iget-object p0, v0, LX/FxB;->c:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    move-object v0, p0

    .line 2304589
    return-object v0
.end method

.method public getSuggestedPhotosMosaicView()Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2304584
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FxB;

    .line 2304585
    iget-object p0, v0, LX/FxB;->b:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;

    move-object v0, p0

    .line 2304586
    return-object v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2304583
    iget-boolean v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->e:Z

    return v0
.end method

.method public setOnCloseListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 2304578
    if-eqz p1, :cond_0

    .line 2304579
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2304580
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->c:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2304581
    :goto_0
    return-void

    .line 2304582
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->c:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setOnEditListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2304576
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2304577
    return-void
.end method
