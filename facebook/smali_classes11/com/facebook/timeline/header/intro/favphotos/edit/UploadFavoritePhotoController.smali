.class public Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/0TD;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/8OJ;

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2306065
    const-class v0, Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0TD;LX/0Or;LX/8OJ;)V
    .locals 1
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TD;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/8OJ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2306066
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2306067
    iput-object p1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;->b:LX/0TD;

    .line 2306068
    iput-object p2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;->c:LX/0Or;

    .line 2306069
    iput-object p3, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;->d:LX/8OJ;

    .line 2306070
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;->e:Ljava/lang/String;

    .line 2306071
    return-void
.end method
