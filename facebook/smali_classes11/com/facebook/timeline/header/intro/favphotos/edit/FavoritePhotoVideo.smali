.class public Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I
    .annotation build Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo$MediaType;
    .end annotation
.end field

.field public final b:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2304764
    new-instance v0, LX/FxL;

    invoke-direct {v0}, LX/FxL;-><init>()V

    sput-object v0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;)V
    .locals 0
    .param p1    # I
        .annotation build Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo$MediaType;
        .end annotation
    .end param

    .prologue
    .line 2304765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2304766
    iput p1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->a:I

    .line 2304767
    iput-object p2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->c:Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;

    .line 2304768
    iput-object p3, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->b:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    .line 2304769
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2304770
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2304771
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->a:I

    .line 2304772
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->b:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    .line 2304773
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->c:Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;

    .line 2304774
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2304775
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2304776
    iget v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2304777
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->b:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2304778
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->c:Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2304779
    return-void
.end method
