.class public Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;
.super Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public s:LX/BQ9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private w:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2304933
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/executor/GraphQLResult;Landroid/os/Bundle;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2304918
    if-eqz p0, :cond_1

    .line 2304919
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2304920
    if-eqz v0, :cond_1

    .line 2304921
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2304922
    check-cast v0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;->a()Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2304923
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2304924
    check-cast v0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;->a()Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;

    move-result-object v0

    .line 2304925
    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;->a()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;->a()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2304926
    const-string v1, "fav_photos_extra"

    .line 2304927
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2304928
    check-cast v0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;->a()Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;->a()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 2304929
    :cond_0
    const-string v1, "initial_is_feed_sharing_switch_checked"

    .line 2304930
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2304931
    check-cast v0, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;->a()Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel$ProfileIntroCardModel;->j()Z

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2304932
    :cond_1
    return-void
.end method

.method private static a(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;LX/BQ9;LX/0Ot;LX/0Or;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;",
            "LX/BQ9;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2304917
    iput-object p1, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;->s:LX/BQ9;

    iput-object p2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;->t:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;->u:LX/0Or;

    iput-object p4, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;->v:LX/0ad;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;

    invoke-static {v1}, LX/BQ9;->a(LX/0QB;)LX/BQ9;

    move-result-object v0

    check-cast v0, LX/BQ9;

    const/16 v2, 0x3677

    invoke-static {v1, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x15e7

    invoke-static {v1, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {v1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0, v0, v2, v3, v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;->a(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;LX/BQ9;LX/0Ot;LX/0Or;LX/0ad;)V

    return-void
.end method

.method private p()V
    .locals 11

    .prologue
    .line 2304912
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;->s:LX/BQ9;

    iget-wide v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;->w:J

    .line 2304913
    const/4 v8, 0x0

    sget-object v9, LX/9lQ;->SELF:LX/9lQ;

    const-string v10, "fav_photos_edit_cancel_click"

    move-object v5, v0

    move-wide v6, v2

    invoke-static/range {v5 .. v10}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2304914
    if-eqz v4, :cond_0

    .line 2304915
    iget-object v5, v0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2304916
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 2304911
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;->v:LX/0ad;

    sget-short v1, LX/0wf;->G:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    invoke-direct {v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    invoke-direct {v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;-><init>()V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2304934
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-static {p1, p2}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;->a(Lcom/facebook/graphql/executor/GraphQLResult;Landroid/os/Bundle;)V

    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2304910
    const v0, 0x7f0815f4

    return v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2304906
    invoke-static {p0, p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2304907
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;->u:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;->w:J

    .line 2304908
    invoke-super {p0, p1}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->b(Landroid/os/Bundle;)V

    .line 2304909
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2304904
    const-string v0, "show_feed_sharing_switch_extra"

    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "show_feed_sharing_switch_extra"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2304905
    return-void
.end method

.method public final l()V
    .locals 0

    .prologue
    .line 2304902
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;->p()V

    .line 2304903
    return-void
.end method

.method public final m()V
    .locals 0

    .prologue
    .line 2304900
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;->p()V

    .line 2304901
    return-void
.end method

.method public final n()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2304886
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;

    iget-wide v2, p0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;->w:J

    .line 2304887
    new-instance v4, LX/Fww;

    invoke-direct {v4}, LX/Fww;-><init>()V

    move-object v4, v4

    .line 2304888
    const-string v5, "profile_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2304889
    iget-object v5, v0, Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;->e:LX/0se;

    iget-object v6, v0, Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;->d:LX/0rq;

    invoke-virtual {v6}, LX/0rq;->c()LX/0wF;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, LX/0se;->a(LX/0gW;LX/0wF;)LX/0gW;

    .line 2304890
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    sget-object v5, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v4, v5}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v4

    sget-object v5, Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 2304891
    iput-object v5, v4, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2304892
    move-object v4, v4

    .line 2304893
    sget-object v5, Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;->a:Ljava/util/Set;

    .line 2304894
    iput-object v5, v4, LX/0zO;->d:Ljava/util/Set;

    .line 2304895
    move-object v4, v4

    .line 2304896
    sget-object v5, LX/0zS;->a:LX/0zS;

    invoke-virtual {v4, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    const-wide/16 v6, 0xe10

    invoke-virtual {v4, v6, v7}, LX/0zO;->a(J)LX/0zO;

    move-result-object v4

    .line 2304897
    iget-object v5, v0, Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;->c:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    move-object v0, v4

    .line 2304898
    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 2304899
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "profileId"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
