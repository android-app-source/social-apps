.class public Lcom/facebook/timeline/header/TimelineContextItemsSection;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/FrA;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2300709
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2300710
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2300707
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2300708
    return-void
.end method


# virtual methods
.method public getAdapter()LX/FrA;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2300711
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineContextItemsSection;->a:LX/FrA;

    return-object v0
.end method

.method public setAdapter(LX/FrA;)V
    .locals 3
    .param p1    # LX/FrA;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2300698
    iput-object p1, p0, Lcom/facebook/timeline/header/TimelineContextItemsSection;->a:LX/FrA;

    .line 2300699
    invoke-virtual {p0}, Lcom/facebook/timeline/header/TimelineContextItemsSection;->removeAllViews()V

    .line 2300700
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/FrA;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 2300701
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/TimelineContextItemsSection;->setVisibility(I)V

    .line 2300702
    :goto_0
    return-void

    :cond_1
    move v0, v1

    .line 2300703
    :goto_1
    invoke-virtual {p1}, LX/FrA;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 2300704
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, p0}, LX/1Cv;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/facebook/timeline/header/TimelineContextItemsSection;->addView(Landroid/view/View;)V

    .line 2300705
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2300706
    :cond_2
    invoke-virtual {p0, v1}, Lcom/facebook/timeline/header/TimelineContextItemsSection;->setVisibility(I)V

    goto :goto_0
.end method
