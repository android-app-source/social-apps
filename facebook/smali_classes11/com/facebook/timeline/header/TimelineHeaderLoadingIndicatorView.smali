.class public Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/Fsr;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private c:LX/23P;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;

.field private e:Lcom/facebook/resources/ui/FbTextView;

.field private f:Lcom/facebook/fig/button/FigButton;

.field private g:Lcom/facebook/resources/ui/FbTextView;

.field private final h:Landroid/view/View$OnClickListener;

.field private i:Landroid/text/Spanned;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2301188
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2301189
    new-instance v0, LX/FvF;

    invoke-direct {v0, p0}, LX/FvF;-><init>(Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;)V

    iput-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->h:Landroid/view/View$OnClickListener;

    .line 2301190
    invoke-direct {p0}, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->c()V

    .line 2301191
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2301184
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2301185
    new-instance v0, LX/FvF;

    invoke-direct {v0, p0}, LX/FvF;-><init>(Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;)V

    iput-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->h:Landroid/view/View$OnClickListener;

    .line 2301186
    invoke-direct {p0}, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->c()V

    .line 2301187
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2301180
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2301181
    new-instance v0, LX/FvF;

    invoke-direct {v0, p0}, LX/FvF;-><init>(Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;)V

    iput-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->h:Landroid/view/View$OnClickListener;

    .line 2301182
    invoke-direct {p0}, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->c()V

    .line 2301183
    return-void
.end method

.method private static a(Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;LX/0ad;LX/Fsr;LX/23P;)V
    .locals 0

    .prologue
    .line 2301150
    iput-object p1, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->a:LX/0ad;

    iput-object p2, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->b:LX/Fsr;

    iput-object p3, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->c:LX/23P;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;

    invoke-static {v2}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {v2}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v1

    check-cast v1, LX/Fsr;

    invoke-static {v2}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v2

    check-cast v2, LX/23P;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->a(Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;LX/0ad;LX/Fsr;LX/23P;)V

    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 2301178
    const-class v0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2301179
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 2301173
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->d:Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;

    iget-object v1, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->a:LX/0ad;

    sget v2, LX/0wf;->ac:I

    const/4 v3, 0x3

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->setNumberOfDots(I)V

    .line 2301174
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->d:Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;

    iget-object v1, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->a:LX/0ad;

    sget v2, LX/0wf;->Y:I

    const/16 v3, 0x1f4

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->setAnimationTime(I)V

    .line 2301175
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->d:Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;

    iget-object v1, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->a:LX/0ad;

    sget-short v2, LX/0wf;->Z:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->setBounceEnabled(Z)V

    .line 2301176
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->a:LX/0ad;

    sget-char v1, LX/0wf;->aa:C

    const-string v2, ""

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->i:Landroid/text/Spanned;

    .line 2301177
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2301167
    invoke-virtual {p0, v1}, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->setVisibility(I)V

    .line 2301168
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->d:Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->setVisibility(I)V

    .line 2301169
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->e:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->i:Landroid/text/Spanned;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2301170
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2301171
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->f:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2301172
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2301158
    invoke-virtual {p0, v3}, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->setVisibility(I)V

    .line 2301159
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->d:Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->setVisibility(I)V

    .line 2301160
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081637

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2301161
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2301162
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081638

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2301163
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->f:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v3}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2301164
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->f:Lcom/facebook/fig/button/FigButton;

    iget-object v1, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->c:LX/23P;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2301165
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->f:Lcom/facebook/fig/button/FigButton;

    iget-object v1, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2301166
    return-void
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x2c35ffd2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2301151
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onFinishInflate()V

    .line 2301152
    const v0, 0x7f0d2f26

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;

    iput-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->d:Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;

    .line 2301153
    const v0, 0x7f0d2f28

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2301154
    const v0, 0x7f0d2f27

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2301155
    const v0, 0x7f0d2f29

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->f:Lcom/facebook/fig/button/FigButton;

    .line 2301156
    invoke-direct {p0}, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->d()V

    .line 2301157
    const/16 v0, 0x2d

    const v2, -0x2e190ac4

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
