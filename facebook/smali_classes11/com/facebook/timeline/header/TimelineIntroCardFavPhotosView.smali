.class public Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# static fields
.field private static final f:LX/Fwd;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FxC;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FyY;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/fig/button/FigButton;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2302286
    new-instance v0, LX/Fwd;

    invoke-direct {v0, v2, v2, v1, v1}, LX/Fwd;-><init>(ZZZZ)V

    sput-object v0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->f:LX/Fwd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2302287
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2302288
    invoke-direct {p0}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->g()V

    .line 2302289
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2302290
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2302291
    invoke-direct {p0}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->g()V

    .line 2302292
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2302283
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2302284
    invoke-direct {p0}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->g()V

    .line 2302285
    return-void
.end method

.method private static a(LX/0zw;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zw",
            "<+",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2302293
    if-eqz p0, :cond_0

    .line 2302294
    invoke-virtual {p0}, LX/0zw;->c()V

    .line 2302295
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;",
            "LX/0Ot",
            "<",
            "LX/FxC;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FyY;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2302296
    iput-object p1, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->a:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->b:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;

    const/16 v1, 0x3680

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x368c

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->a(Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    .line 2302297
    const-class v0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2302298
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->setWillNotDraw(Z)V

    .line 2302299
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 2302300
    new-instance v1, LX/0zw;

    const v0, 0x7f0d2f54

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->c:LX/0zw;

    .line 2302301
    new-instance v1, LX/0zw;

    const v0, 0x7f0d2f55

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->d:LX/0zw;

    .line 2302302
    new-instance v1, LX/0zw;

    const v0, 0x7f0d2f56

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->e:LX/0zw;

    .line 2302303
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 4

    .prologue
    .line 2302304
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->c:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    .line 2302305
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    move-object v0, v1

    .line 2302306
    check-cast v0, Lcom/facebook/fig/button/FigButton;

    .line 2302307
    invoke-virtual {p0}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0815f1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2302308
    const v2, 0x7f020958

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setGlyph(I)V

    .line 2302309
    invoke-virtual {v1, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2302310
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0Px;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLInterfaces$SuggestedPhoto;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2302311
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    .line 2302312
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->setVisibility(I)V

    .line 2302313
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FxC;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, LX/FxC;->a(Ljava/lang/String;Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;LX/0Px;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 2302314
    return-void
.end method

.method public final a(Ljava/lang/String;ZLX/0Px;Landroid/view/View$OnClickListener;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "LX/0Px",
            "<+",
            "Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLInterfaces$FavoritePhoto;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2302254
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;

    .line 2302255
    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->a()V

    .line 2302256
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->setVisibility(I)V

    .line 2302257
    if-eqz p2, :cond_0

    :goto_0
    invoke-virtual {v0, p4}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->setEditClickListener(Landroid/view/View$OnClickListener;)V

    .line 2302258
    invoke-virtual {v0, p2}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->setEditable(Z)V

    .line 2302259
    invoke-virtual {v0, p1, p3}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;->a(Ljava/lang/String;LX/0Px;)V

    .line 2302260
    return-void

    .line 2302261
    :cond_0
    const/4 p4, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2302252
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->d:LX/0zw;

    invoke-static {v0}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->a(LX/0zw;)V

    .line 2302253
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2302262
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->c:LX/0zw;

    invoke-static {v0}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->a(LX/0zw;)V

    .line 2302263
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2302264
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->e:LX/0zw;

    invoke-static {v0}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->a(LX/0zw;)V

    .line 2302265
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2302266
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->g:Z

    .line 2302267
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FyY;

    sget-object v1, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->f:LX/Fwd;

    invoke-virtual {v0, p0, v1}, LX/FyY;->a(Landroid/view/View;LX/Fwd;)V

    .line 2302268
    invoke-virtual {p0}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->invalidate()V

    .line 2302269
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2302270
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->g:Z

    .line 2302271
    invoke-virtual {p0}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->invalidate()V

    .line 2302272
    return-void
.end method

.method public getFavPhotosEmptyView()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2302273
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->c:LX/0zw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->c:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->c:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFavPhotosView()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2302274
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->d:LX/0zw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFavoritePhotosView;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSuggestedPhotosView()Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2302275
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->e:LX/0zw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 2302276
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2302277
    iget-boolean v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->g:Z

    if-nez v0, :cond_0

    .line 2302278
    :goto_0
    return-void

    .line 2302279
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FyY;

    sget-object v1, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->f:LX/Fwd;

    invoke-virtual {v0, p0, p1, v1}, LX/FyY;->a(Landroid/view/View;Landroid/graphics/Canvas;LX/Fwd;)V

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x2282b06f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2302280
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onFinishInflate()V

    .line 2302281
    invoke-virtual {p0}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->a()V

    .line 2302282
    const/16 v1, 0x2d

    const v2, -0x334072c4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
