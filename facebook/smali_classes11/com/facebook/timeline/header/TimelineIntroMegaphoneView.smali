.class public Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# static fields
.field private static final b:LX/Fwd;


# instance fields
.field public a:LX/FyY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/graphics/Paint;

.field private d:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2302422
    new-instance v0, LX/Fwd;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v2, v1}, LX/Fwd;-><init>(ZZZZ)V

    sput-object v0, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->b:LX/Fwd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2302419
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2302420
    invoke-direct {p0}, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->a()V

    .line 2302421
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2302416
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2302417
    invoke-direct {p0}, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->a()V

    .line 2302418
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2302413
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2302414
    invoke-direct {p0}, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->a()V

    .line 2302415
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2302407
    const-class v0, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2302408
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->setWillNotDraw(Z)V

    .line 2302409
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->c:Landroid/graphics/Paint;

    .line 2302410
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0543

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2302411
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->c:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2302412
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;

    invoke-static {v0}, LX/FyY;->a(LX/0QB;)LX/FyY;

    move-result-object v0

    check-cast v0, LX/FyY;

    iput-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->a:LX/FyY;

    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2302401
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2302402
    invoke-virtual {p0}, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2302403
    invoke-virtual {p0}, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->getWidth()I

    move-result v1

    add-int/lit8 v3, v1, -0x1

    .line 2302404
    const/high16 v1, 0x3f800000    # 1.0f

    int-to-float v2, v0

    int-to-float v3, v3

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2302405
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->a:LX/FyY;

    sget-object v1, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->b:LX/Fwd;

    invoke-virtual {v0, p0, p1, v1}, LX/FyY;->a(Landroid/view/View;Landroid/graphics/Canvas;LX/Fwd;)V

    .line 2302406
    return-void
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x71fd4b1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2302396
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onFinishInflate()V

    .line 2302397
    const v1, 0x7f0d2f1d

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->d:Landroid/view/View;

    .line 2302398
    const/16 v1, 0x2d

    const v2, -0x2741210b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setOnDismissListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2302399
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2302400
    return-void
.end method
