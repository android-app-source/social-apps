.class public Lcom/facebook/timeline/header/TimelineProfileImageFrameController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final c:LX/BRr;

.field private final d:LX/Bas;

.field private final e:LX/Fsr;

.field public final f:LX/BQB;

.field private final g:LX/0Uh;

.field public h:Z

.field public i:LX/BQ1;

.field private j:LX/1bf;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:LX/1bf;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2302592
    const-class v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;

    sput-object v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->a:Ljava/lang/Class;

    .line 2302593
    const-class v0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    const-string v1, "timeline"

    const-string v2, "profile_pic"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/BRr;LX/Bas;LX/Fsr;LX/BQB;LX/0Uh;)V
    .locals 1
    .param p1    # LX/BRr;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2302594
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2302595
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->h:Z

    .line 2302596
    iput-object p1, p0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->c:LX/BRr;

    .line 2302597
    iput-object p2, p0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->d:LX/Bas;

    .line 2302598
    iput-object p4, p0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->f:LX/BQB;

    .line 2302599
    iput-object p3, p0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->e:LX/Fsr;

    .line 2302600
    iput-object p5, p0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->g:LX/0Uh;

    .line 2302601
    return-void
.end method

.method private a(LX/5SB;LX/BQ1;ZZ)I
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2302602
    if-nez p4, :cond_0

    invoke-virtual {p1}, LX/5SB;->i()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->i:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->w()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move v0, v1

    .line 2302603
    :cond_1
    :goto_0
    return v0

    .line 2302604
    :cond_2
    iget-object v2, p0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->c:LX/BRr;

    invoke-virtual {v2, p1, p2, p3, p4}, LX/BRr;->a(LX/5SB;LX/BQ1;ZZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2302605
    iget-object v2, p0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->c:LX/BRr;

    .line 2302606
    iget v3, v2, LX/BRr;->g:I

    move v2, v3

    .line 2302607
    const/4 v3, 0x2

    if-eq v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->c:LX/BRr;

    .line 2302608
    iget v3, v2, LX/BRr;->g:I

    move v2, v3

    .line 2302609
    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    :cond_3
    move v2, v0

    .line 2302610
    :goto_1
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    :cond_4
    move v2, v1

    .line 2302611
    goto :goto_1
.end method

.method public static a(Lcom/facebook/timeline/header/TimelineProfileImageFrameController;LX/BPA;)V
    .locals 1

    .prologue
    .line 2302612
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->e:LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->A()LX/FvM;

    move-result-object v0

    invoke-interface {v0, p1}, LX/FvM;->b(LX/BPA;)V

    .line 2302613
    return-void
.end method

.method private static a(LX/BQ1;)Z
    .locals 1

    .prologue
    .line 2302614
    invoke-virtual {p0}, LX/BQ1;->N()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/BQ1;->w()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/BQ1;LX/5SB;ZZZLandroid/view/View$OnClickListener;Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;LX/0zw;Z)V
    .locals 17
    .param p6    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BQ1;",
            "LX/5SB;",
            "ZZZ",
            "Landroid/view/View$OnClickListener;",
            "Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;",
            "LX/0zw",
            "<+",
            "Landroid/view/View;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2302615
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->i:LX/BQ1;

    .line 2302616
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->g:LX/0Uh;

    const/16 v6, 0x45d

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, LX/0Uh;->a(IZ)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->i:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->R()Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v11, 0x1

    .line 2302617
    :goto_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->c:LX/BRr;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->i:LX/BQ1;

    move-object/from16 v0, p2

    move/from16 v1, p4

    move/from16 v2, p5

    invoke-virtual {v5, v0, v6, v1, v2}, LX/BRr;->a(LX/5SB;LX/BQ1;ZZ)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2302618
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->c:LX/BRr;

    invoke-virtual {v5}, LX/BRr;->b()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 2302619
    sget-object v5, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->a:Ljava/lang/Class;

    const-string v6, "unexpected animation state: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->c:LX/BRr;

    invoke-virtual {v9}, LX/BRr;->b()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2302620
    :goto_1
    :pswitch_0
    if-eqz p3, :cond_0

    if-nez p9, :cond_1

    .line 2302621
    :cond_0
    const/4 v5, 0x0

    move-object/from16 v0, p7

    invoke-virtual {v0, v5}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->setVisibility(I)V

    .line 2302622
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->i:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->N()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 2302623
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->i:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->M()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v5

    if-eqz v5, :cond_4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->i:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->M()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->d()Ljava/lang/String;

    move-result-object v5

    .line 2302624
    :goto_2
    if-eqz v6, :cond_5

    invoke-static {v6}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v6

    invoke-static {v5}, LX/1ny;->a(Ljava/lang/String;)LX/1ny;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/1bX;->a(LX/1ny;)LX/1bX;

    move-result-object v5

    invoke-virtual {v5}, LX/1bX;->n()LX/1bf;

    move-result-object v5

    .line 2302625
    :goto_3
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->i:LX/BQ1;

    invoke-virtual {v6}, LX/BPy;->n()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2302626
    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->k:LX/1bf;

    .line 2302627
    :goto_4
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    move/from16 v3, p4

    move/from16 v4, p5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->a(LX/5SB;LX/BQ1;ZZ)I

    move-result v5

    .line 2302628
    if-nez v5, :cond_7

    .line 2302629
    invoke-virtual/range {p8 .. p8}, LX/0zw;->c()V

    .line 2302630
    :goto_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->d:LX/Bas;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->j:LX/1bf;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->k:LX/1bf;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->i:LX/BQ1;

    invoke-virtual {v8}, LX/BQ1;->w()Z

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->i:LX/BQ1;

    invoke-static {v9}, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->a(LX/BQ1;)Z

    move-result v9

    invoke-virtual/range {p2 .. p2}, LX/5SB;->b()Z

    move-result v10

    const/4 v15, 0x0

    sget-object v12, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->b:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v11, :cond_8

    const/4 v13, 0x0

    :goto_6
    new-instance v14, LX/Fw3;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->i:LX/BQ1;

    invoke-virtual {v11}, LX/BPy;->n()Z

    move-result v11

    if-nez v11, :cond_9

    const/4 v11, 0x1

    :goto_7
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v14, v0, v11, v1}, LX/Fw3;-><init>(Lcom/facebook/timeline/header/TimelineProfileImageFrameController;ZLandroid/view/View;)V

    move v11, v15

    move-object/from16 v15, p7

    invoke-virtual/range {v5 .. v15}, LX/Bas;->a(LX/1bf;LX/1bf;ZZZZLcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;LX/1cC;Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;)V

    .line 2302631
    return-void

    .line 2302632
    :cond_2
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 2302633
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->c:LX/BRr;

    new-instance v5, LX/Fw2;

    move-object/from16 v6, p0

    move-object/from16 v7, p2

    move/from16 v8, p3

    move/from16 v9, p4

    move/from16 v10, p5

    move-object/from16 v12, p6

    move-object/from16 v13, p7

    move-object/from16 v14, p8

    invoke-direct/range {v5 .. v14}, LX/Fw2;-><init>(Lcom/facebook/timeline/header/TimelineProfileImageFrameController;LX/5SB;ZZZZLandroid/view/View$OnClickListener;Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;LX/0zw;)V

    invoke-virtual {v15, v5}, LX/BRr;->a(LX/Fw2;)V

    .line 2302634
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->c:LX/BRr;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->i:LX/BQ1;

    invoke-virtual {v5, v6}, LX/BRr;->a(LX/BQ1;)V

    goto/16 :goto_1

    .line 2302635
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->c:LX/BRr;

    invoke-virtual {v5}, LX/BRr;->a()V

    goto/16 :goto_1

    .line 2302636
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->c:LX/BRr;

    invoke-virtual {v5}, LX/BRr;->a()V

    goto/16 :goto_1

    .line 2302637
    :cond_4
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 2302638
    :cond_5
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 2302639
    :cond_6
    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->j:LX/1bf;

    goto/16 :goto_4

    .line 2302640
    :cond_7
    invoke-virtual/range {p8 .. p8}, LX/0zw;->a()Landroid/view/View;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2302641
    invoke-virtual/range {p8 .. p8}, LX/0zw;->a()Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0d2723

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 2302642
    const v6, 0x7f02150d

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2302643
    invoke-virtual {v5}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    check-cast v5, Landroid/graphics/drawable/AnimationDrawable;

    .line 2302644
    const/16 v6, 0xc8

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/AnimationDrawable;->setEnterFadeDuration(I)V

    .line 2302645
    const/16 v6, 0xc8

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/AnimationDrawable;->setExitFadeDuration(I)V

    .line 2302646
    invoke-virtual {v5}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    goto/16 :goto_5

    :cond_8
    move-object/from16 v13, p6

    .line 2302647
    goto/16 :goto_6

    :cond_9
    const/4 v11, 0x0

    goto/16 :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
