.class public final Lcom/facebook/timeline/header/SeeFirstInterstitialHelper$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/view/ViewGroup;

.field public final synthetic b:LX/8DE;

.field public final synthetic c:LX/Fuy;


# direct methods
.method public constructor <init>(LX/Fuy;Landroid/view/ViewGroup;LX/8DE;)V
    .locals 0

    .prologue
    .line 2300665
    iput-object p1, p0, Lcom/facebook/timeline/header/SeeFirstInterstitialHelper$1;->c:LX/Fuy;

    iput-object p2, p0, Lcom/facebook/timeline/header/SeeFirstInterstitialHelper$1;->a:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/facebook/timeline/header/SeeFirstInterstitialHelper$1;->b:LX/8DE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2300666
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/timeline/header/SeeFirstInterstitialHelper$1;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2300667
    iget-object v0, p0, Lcom/facebook/timeline/header/SeeFirstInterstitialHelper$1;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2300668
    instance-of v2, v0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;

    if-eqz v2, :cond_1

    .line 2300669
    check-cast v0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;

    .line 2300670
    invoke-virtual {v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/timeline/header/SeeFirstInterstitialHelper$1;->a:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f08153f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2300671
    new-instance v1, LX/0hs;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2300672
    const/4 v2, -0x1

    .line 2300673
    iput v2, v1, LX/0hs;->t:I

    .line 2300674
    const v2, 0x7f081348

    invoke-virtual {v1, v2}, LX/0hs;->a(I)V

    .line 2300675
    const v2, 0x7f081349

    invoke-virtual {v1, v2}, LX/0hs;->b(I)V

    .line 2300676
    const v2, 0x3e4ccccd    # 0.2f

    invoke-virtual {v1, v2}, LX/0ht;->b(F)V

    .line 2300677
    invoke-virtual {v1, v0}, LX/0ht;->a(Landroid/view/View;)V

    .line 2300678
    iget-object v0, p0, Lcom/facebook/timeline/header/SeeFirstInterstitialHelper$1;->c:LX/Fuy;

    iget-object v0, v0, LX/Fuy;->a:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/timeline/header/SeeFirstInterstitialHelper$1;->b:LX/8DE;

    invoke-virtual {v1}, LX/8DE;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2300679
    iget-object v0, p0, Lcom/facebook/timeline/header/SeeFirstInterstitialHelper$1;->c:LX/Fuy;

    iget-object v0, v0, LX/Fuy;->b:LX/82T;

    .line 2300680
    iget-object v1, v0, LX/82T;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/82T;->a:LX/0Tn;

    iget-object v3, v0, LX/82T;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/82T;->a:LX/0Tn;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2300681
    iget-object v0, p0, Lcom/facebook/timeline/header/SeeFirstInterstitialHelper$1;->c:LX/Fuy;

    const/4 v1, 0x1

    .line 2300682
    iput-boolean v1, v0, LX/Fuy;->c:Z

    .line 2300683
    :cond_0
    return-void

    .line 2300684
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0
.end method
