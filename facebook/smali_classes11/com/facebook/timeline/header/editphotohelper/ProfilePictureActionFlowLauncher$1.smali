.class public final Lcom/facebook/timeline/header/editphotohelper/ProfilePictureActionFlowLauncher$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

.field public final synthetic b:LX/FwR;


# direct methods
.method public constructor <init>(LX/FwR;Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;)V
    .locals 0

    .prologue
    .line 2303128
    iput-object p1, p0, Lcom/facebook/timeline/header/editphotohelper/ProfilePictureActionFlowLauncher$1;->b:LX/FwR;

    iput-object p2, p0, Lcom/facebook/timeline/header/editphotohelper/ProfilePictureActionFlowLauncher$1;->a:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 2303129
    iget-object v0, p0, Lcom/facebook/timeline/header/editphotohelper/ProfilePictureActionFlowLauncher$1;->b:LX/FwR;

    iget-object v0, v0, LX/FwR;->a:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FwT;

    iget-object v1, p0, Lcom/facebook/timeline/header/editphotohelper/ProfilePictureActionFlowLauncher$1;->b:LX/FwR;

    iget-object v2, p0, Lcom/facebook/timeline/header/editphotohelper/ProfilePictureActionFlowLauncher$1;->a:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    .line 2303130
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->TEMPORARY:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    if-ne v2, v4, :cond_0

    .line 2303131
    iget-object v4, v1, LX/FwR;->c:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/32 v6, 0x5265c00

    add-long/2addr v4, v6

    .line 2303132
    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 2303133
    :goto_0
    move-wide v2, v4

    .line 2303134
    invoke-virtual {v0, v2, v3}, LX/FwT;->a(J)V

    .line 2303135
    return-void

    :cond_0
    const-wide/16 v4, 0x0

    goto :goto_0
.end method
