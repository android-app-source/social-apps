.class public Lcom/facebook/timeline/header/TimelineIntroCardView;
.super Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;
.source ""


# instance fields
.field public f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/23P;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/timeline/header/TimelineContextItemsSection;

.field private h:Lcom/facebook/fig/button/FigButton;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/widget/text/BetterTextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2302380
    invoke-direct {p0, p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;-><init>(Landroid/content/Context;)V

    .line 2302381
    invoke-direct {p0}, Lcom/facebook/timeline/header/TimelineIntroCardView;->l()V

    .line 2302382
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2302383
    invoke-direct {p0, p1, p2}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2302384
    invoke-direct {p0}, Lcom/facebook/timeline/header/TimelineIntroCardView;->l()V

    .line 2302385
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2302386
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2302387
    invoke-direct {p0}, Lcom/facebook/timeline/header/TimelineIntroCardView;->l()V

    .line 2302388
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/header/TimelineIntroCardView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/timeline/header/TimelineIntroCardView;

    const/16 v1, 0x5cb

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->f:LX/0Or;

    return-void
.end method

.method private l()V
    .locals 1

    .prologue
    .line 2302389
    const-class v0, Lcom/facebook/timeline/header/TimelineIntroCardView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/header/TimelineIntroCardView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2302390
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/TimelineIntroCardView;->setWillNotDraw(Z)V

    .line 2302391
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2302392
    new-instance v1, LX/0zw;

    const v0, 0x7f0d2f4b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->c:LX/0zw;

    .line 2302393
    new-instance v1, LX/0zw;

    const v0, 0x7f0d2f3e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->d:LX/0zw;

    .line 2302394
    new-instance v1, LX/0zw;

    const v0, 0x7f0d2f40

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->e:LX/0zw;

    .line 2302395
    return-void
.end method

.method public final a(LX/FrA;)V
    .locals 1

    .prologue
    .line 2302365
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->g:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    if-eqz v0, :cond_0

    .line 2302366
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->g:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    invoke-virtual {v0, p1}, Lcom/facebook/timeline/header/TimelineContextItemsSection;->setAdapter(LX/FrA;)V

    .line 2302367
    :cond_0
    return-void
.end method

.method public final b(Landroid/view/View$OnClickListener;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2302368
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->g:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/TimelineContextItemsSection;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2302369
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->h:Lcom/facebook/fig/button/FigButton;

    if-nez v0, :cond_0

    .line 2302370
    invoke-virtual {p0}, Lcom/facebook/timeline/header/TimelineIntroCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0314e1

    iget-object v3, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->g:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    invoke-virtual {v0, v2, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->h:Lcom/facebook/fig/button/FigButton;

    .line 2302371
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->h:Lcom/facebook/fig/button/FigButton;

    .line 2302372
    invoke-virtual {p0}, Lcom/facebook/timeline/header/TimelineIntroCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0815ef

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2302373
    const v2, 0x7f020970

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setGlyph(I)V

    .line 2302374
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->h:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, p1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2302375
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->g:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    iget-object v2, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->h:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v2}, Lcom/facebook/timeline/header/TimelineContextItemsSection;->addView(Landroid/view/View;)V

    .line 2302376
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->h:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2302377
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->g:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/header/TimelineContextItemsSection;->setVisibility(I)V

    .line 2302378
    return-void

    :cond_1
    move v0, v1

    .line 2302379
    goto :goto_0
.end method

.method public final c(Landroid/view/View$OnClickListener;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2302341
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->g:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/TimelineContextItemsSection;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2302342
    invoke-virtual {p0}, Lcom/facebook/timeline/header/TimelineIntroCardView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2302343
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->i:Lcom/facebook/widget/text/BetterTextView;

    if-nez v0, :cond_0

    .line 2302344
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f0314ee

    iget-object v4, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->g:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    invoke-virtual {v0, v3, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->i:Lcom/facebook/widget/text/BetterTextView;

    .line 2302345
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0e01

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 2302346
    invoke-static {v2}, LX/0hL;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2302347
    iget-object v2, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->i:Lcom/facebook/widget/text/BetterTextView;

    iget-object v3, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3}, Lcom/facebook/widget/text/BetterTextView;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v4}, Lcom/facebook/widget/text/BetterTextView;->getPaddingTop()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v5}, Lcom/facebook/widget/text/BetterTextView;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v2, v3, v4, v0, v5}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    .line 2302348
    :goto_1
    iget-object v2, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->i:Lcom/facebook/widget/text/BetterTextView;

    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/method/TransformationMethod;

    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/BetterTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2302349
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2302350
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->g:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    iget-object v2, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/timeline/header/TimelineContextItemsSection;->addView(Landroid/view/View;)V

    .line 2302351
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2302352
    return-void

    :cond_1
    move v0, v1

    .line 2302353
    goto :goto_0

    .line 2302354
    :cond_2
    iget-object v2, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->i:Lcom/facebook/widget/text/BetterTextView;

    iget-object v3, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3}, Lcom/facebook/widget/text/BetterTextView;->getPaddingTop()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v4}, Lcom/facebook/widget/text/BetterTextView;->getPaddingRight()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v5}, Lcom/facebook/widget/text/BetterTextView;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    goto :goto_1
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 2302338
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->h:Lcom/facebook/fig/button/FigButton;

    if-eqz v0, :cond_0

    .line 2302339
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->h:Lcom/facebook/fig/button/FigButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2302340
    :cond_0
    return-void
.end method

.method public getPublicAboutItemsView()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 2302364
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->g:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    return-object v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 2302335
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->i:Lcom/facebook/widget/text/BetterTextView;

    if-eqz v0, :cond_0

    .line 2302336
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->i:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2302337
    :cond_0
    return-void
.end method

.method public final i()V
    .locals 4

    .prologue
    .line 2302355
    invoke-virtual {p0}, Lcom/facebook/timeline/header/TimelineIntroCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0df6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2302356
    iget-object v1, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->g:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    iget-object v2, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->g:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    invoke-virtual {v2}, Lcom/facebook/timeline/header/TimelineContextItemsSection;->getPaddingLeft()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->g:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    invoke-virtual {v3}, Lcom/facebook/timeline/header/TimelineContextItemsSection;->getPaddingRight()I

    move-result v3

    invoke-virtual {v1, v2, v0, v3, v0}, Lcom/facebook/timeline/header/TimelineContextItemsSection;->setPadding(IIII)V

    .line 2302357
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 2302358
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->g:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/TimelineContextItemsSection;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 2302359
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->g:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/TimelineContextItemsSection;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2302360
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->g:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    invoke-virtual {v1, v0}, Lcom/facebook/timeline/header/TimelineContextItemsSection;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, LX/FyX;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x517aea8d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2302361
    invoke-super {p0}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->onFinishInflate()V

    .line 2302362
    const v0, 0x7f0d2f4a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/TimelineContextItemsSection;

    iput-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardView;->g:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    .line 2302363
    const/16 v0, 0x2d

    const v2, -0x6e4e49d8

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
