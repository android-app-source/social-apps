.class public Lcom/facebook/timeline/header/TimelineIntroCardBioView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# static fields
.field private static final d:LX/Fwd;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fwi;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FyY;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/timeline/header/intro/bio/TimelineHeaderSuggestedBioView;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/fig/button/AnimatableContentFigButton;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z

.field private i:I

.field public j:Landroid/animation/ValueAnimator;

.field private k:Landroid/view/View$OnAttachStateChangeListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2301974
    new-instance v0, LX/Fwd;

    invoke-direct {v0, v2, v2, v1, v1}, LX/Fwd;-><init>(ZZZZ)V

    sput-object v0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->d:LX/Fwd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2301996
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2301997
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->i:I

    .line 2301998
    invoke-direct {p0}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->h()V

    .line 2301999
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2301992
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2301993
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->i:I

    .line 2301994
    invoke-direct {p0}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->h()V

    .line 2301995
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2301988
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2301989
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->i:I

    .line 2301990
    invoke-direct {p0}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->h()V

    .line 2301991
    return-void
.end method

.method private static a(LX/0zw;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zw",
            "<+",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2301985
    if-eqz p0, :cond_0

    .line 2301986
    invoke-virtual {p0}, LX/0zw;->c()V

    .line 2301987
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/fig/button/AnimatableContentFigButton;LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/fig/button/AnimatableContentFigButton;",
            "LX/0Px",
            "<+",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2301976
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->j:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 2301977
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->j:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 2301978
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/facebook/fig/button/AnimatableContentFigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2301979
    iget v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->i:I

    const/4 v1, -0x1

    new-instance v2, LX/Fvk;

    invoke-direct {v2, p0, p2, p1}, LX/Fvk;-><init>(Lcom/facebook/timeline/header/TimelineIntroCardBioView;LX/0Px;Lcom/facebook/fig/button/AnimatableContentFigButton;)V

    iget-object v3, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->c:LX/0ad;

    sget v4, LX/0wf;->i:I

    const/16 v5, 0x5dc

    invoke-interface {v3, v4, v5}, LX/0ad;->a(II)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, LX/EQP;->a(IILX/EQO;I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->j:Landroid/animation/ValueAnimator;

    .line 2301980
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->j:Landroid/animation/ValueAnimator;

    new-instance v1, LX/Fvl;

    invoke-direct {v1, p0, p1}, LX/Fvl;-><init>(Lcom/facebook/timeline/header/TimelineIntroCardBioView;Lcom/facebook/fig/button/AnimatableContentFigButton;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2301981
    new-instance v0, LX/Fvm;

    invoke-direct {v0, p0}, LX/Fvm;-><init>(Lcom/facebook/timeline/header/TimelineIntroCardBioView;)V

    iput-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->k:Landroid/view/View$OnAttachStateChangeListener;

    .line 2301982
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->k:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {p1, v0}, Lcom/facebook/fig/button/AnimatableContentFigButton;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 2301983
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->j:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2301984
    return-void
.end method

.method private static a(Lcom/facebook/timeline/header/TimelineIntroCardBioView;LX/0Ot;LX/0Ot;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/header/TimelineIntroCardBioView;",
            "LX/0Ot",
            "<",
            "LX/Fwi;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FyY;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2301975
    iput-object p1, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->a:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->b:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->c:LX/0ad;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;

    const/16 v1, 0x3675

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x368c

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {p0, v1, v2, v0}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->a(Lcom/facebook/timeline/header/TimelineIntroCardBioView;LX/0Ot;LX/0Ot;LX/0ad;)V

    return-void
.end method

.method private b(Landroid/view/View$OnClickListener;)Lcom/facebook/fig/button/AnimatableContentFigButton;
    .locals 3

    .prologue
    .line 2301967
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->g:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/AnimatableContentFigButton;

    .line 2301968
    const v1, 0x7f020845

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setGlyph(I)V

    .line 2301969
    invoke-virtual {v0, p1}, Lcom/facebook/fig/button/AnimatableContentFigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2301970
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/AnimatableContentFigButton;->setVisibility(I)V

    .line 2301971
    iget v1, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->i:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 2301972
    invoke-virtual {v0}, Lcom/facebook/fig/button/AnimatableContentFigButton;->getCurrentTextColor()I

    move-result v1

    iput v1, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->i:I

    .line 2301973
    :cond_0
    return-object v0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 2301964
    const-class v0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2301965
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->setWillNotDraw(Z)V

    .line 2301966
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2301962
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->e:LX/0zw;

    invoke-static {v0}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->a(LX/0zw;)V

    .line 2301963
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 2302000
    invoke-virtual {p0}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0815eb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->a(Landroid/view/View$OnClickListener;Ljava/lang/String;)V

    .line 2302001
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View$OnClickListener;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2301959
    invoke-direct {p0, p1}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->b(Landroid/view/View$OnClickListener;)Lcom/facebook/fig/button/AnimatableContentFigButton;

    move-result-object v0

    .line 2301960
    invoke-direct {p0, v0, p2}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->a(Lcom/facebook/fig/button/AnimatableContentFigButton;LX/0Px;)V

    .line 2301961
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2301956
    invoke-direct {p0, p1}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->b(Landroid/view/View$OnClickListener;)Lcom/facebook/fig/button/AnimatableContentFigButton;

    move-result-object v0

    .line 2301957
    invoke-virtual {v0, p2}, Lcom/facebook/fig/button/AnimatableContentFigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2301958
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 4

    .prologue
    .line 2301946
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->f:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderSuggestedBioView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderSuggestedBioView;->setVisibility(I)V

    .line 2301947
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fwi;

    iget-object v1, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->f:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderSuggestedBioView;

    .line 2301948
    const/16 p0, 0x65

    .line 2301949
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-le v2, p0, :cond_0

    .line 2301950
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, LX/Fwi;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    .line 2301951
    :cond_0
    move-object v2, p1

    .line 2301952
    invoke-virtual {v1, v2}, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderSuggestedBioView;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 2301953
    invoke-virtual {v1, p2}, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderSuggestedBioView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2301954
    invoke-virtual {v1, p3}, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderSuggestedBioView;->setOnCloseListener(Landroid/view/View$OnClickListener;)V

    .line 2301955
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;ZLandroid/view/View$OnClickListener;Z)V
    .locals 2

    .prologue
    .line 2301940
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;

    const v1, 0x7f0219a0

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->setBackgroundResource(I)V

    .line 2301941
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;

    invoke-virtual {v0, p1, p2, p4}, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->a(Ljava/lang/CharSequence;ZZ)V

    .line 2301942
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;

    if-eqz p2, :cond_0

    :goto_0
    invoke-virtual {v0, p3}, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2301943
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->setVisibility(I)V

    .line 2301944
    return-void

    .line 2301945
    :cond_0
    const/4 p3, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2301938
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->f:LX/0zw;

    invoke-static {v0}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->a(LX/0zw;)V

    .line 2301939
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2301934
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->h:Z

    .line 2301935
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FyY;

    sget-object v1, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->d:LX/Fwd;

    invoke-virtual {v0, p0, v1}, LX/FyY;->a(Landroid/view/View;LX/Fwd;)V

    .line 2301936
    invoke-virtual {p0}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->invalidate()V

    .line 2301937
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2301931
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->h:Z

    .line 2301932
    invoke-virtual {p0}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->invalidate()V

    .line 2301933
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 2301924
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->g:LX/0zw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->g:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2301925
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->k:Landroid/view/View$OnAttachStateChangeListener;

    if-eqz v0, :cond_0

    .line 2301926
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->g:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/AnimatableContentFigButton;

    iget-object v1, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->k:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/AnimatableContentFigButton;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 2301927
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->g:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/AnimatableContentFigButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/AnimatableContentFigButton;->setVisibility(I)V

    .line 2301928
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->j:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    .line 2301929
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->j:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 2301930
    :cond_2
    return-void
.end method

.method public getBioViewForLogging()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2301923
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->e:LX/0zw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 2301919
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2301920
    iget-boolean v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->h:Z

    if-nez v0, :cond_0

    .line 2301921
    :goto_0
    return-void

    .line 2301922
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FyY;

    sget-object v1, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->d:LX/Fwd;

    invoke-virtual {v0, p0, p1, v1}, LX/FyY;->a(Landroid/view/View;Landroid/graphics/Canvas;LX/Fwd;)V

    goto :goto_0
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x3b438d29

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2301914
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onFinishInflate()V

    .line 2301915
    new-instance v2, LX/0zw;

    const v0, 0x7f0d2f50

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v2, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v2, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->e:LX/0zw;

    .line 2301916
    new-instance v2, LX/0zw;

    const v0, 0x7f0d2f51

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v2, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v2, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->f:LX/0zw;

    .line 2301917
    new-instance v2, LX/0zw;

    const v0, 0x7f0d2f4f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v2, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v2, p0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->g:LX/0zw;

    .line 2301918
    const/16 v0, 0x2d

    const v2, -0x368fc1ef

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
