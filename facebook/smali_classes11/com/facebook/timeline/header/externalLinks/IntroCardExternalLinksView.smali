.class public Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/widget/FlowLayout;

.field private c:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2303335
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2303336
    invoke-direct {p0}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->d()V

    .line 2303337
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2303332
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2303333
    invoke-direct {p0}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->d()V

    .line 2303334
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v0

    check-cast v0, LX/23P;

    iput-object v0, p0, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->a:LX/23P;

    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2303326
    const-class v0, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2303327
    const v0, 0x7f0314dd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2303328
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->setOrientation(I)V

    .line 2303329
    const v0, 0x7f0d2f3a

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FlowLayout;

    iput-object v0, p0, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->b:Lcom/facebook/widget/FlowLayout;

    .line 2303330
    new-instance v1, LX/0zw;

    const v0, 0x7f0d2f3b

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->c:LX/0zw;

    .line 2303331
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2303324
    iget-object v0, p0, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->b:Lcom/facebook/widget/FlowLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/FlowLayout;->removeAllViews()V

    .line 2303325
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 2303320
    iget-object v0, p0, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->c:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2303321
    iget-object v1, p0, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->a:LX/23P;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2303322
    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2303323
    return-void
.end method

.method public final a(Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;)V
    .locals 1

    .prologue
    .line 2303311
    iget-object v0, p0, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->b:Lcom/facebook/widget/FlowLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/FlowLayout;->addView(Landroid/view/View;)V

    .line 2303312
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2303318
    iget-object v0, p0, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->c:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 2303319
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 2303316
    invoke-virtual {p0}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0df8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->setPadding(IIII)V

    .line 2303317
    return-void
.end method

.method public setForceFirstItemSeparateLine(Z)V
    .locals 1

    .prologue
    .line 2303313
    iget-object v0, p0, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->b:Lcom/facebook/widget/FlowLayout;

    .line 2303314
    iput-boolean p1, v0, Lcom/facebook/widget/FlowLayout;->b:Z

    .line 2303315
    return-void
.end method
