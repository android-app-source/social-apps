.class public Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public c:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2303242
    const-class v0, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2303243
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2303244
    const/4 p1, 0x0

    .line 2303245
    const v0, 0x7f0314d1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2303246
    invoke-virtual {p0, p1}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;->setOrientation(I)V

    .line 2303247
    invoke-virtual {p0}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f02068e

    invoke-static {v0, v1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2303248
    invoke-virtual {p0}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0e11

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2303249
    invoke-virtual {p0, v0, p1, v0, p1}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;->setPadding(IIII)V

    .line 2303250
    const v0, 0x7f0d2f31

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2303251
    const v0, 0x7f0d05f2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2303252
    return-void
.end method
