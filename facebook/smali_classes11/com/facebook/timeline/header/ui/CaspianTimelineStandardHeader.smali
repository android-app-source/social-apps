.class public Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;
.super LX/Ban;
.source ""

# interfaces
.implements LX/BP9;
.implements LX/1a7;


# static fields
.field private static final D:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:LX/0Or;
    .annotation runtime Lcom/facebook/timeline/util/IsWorkUserBadgeEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/FwQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:Lcom/facebook/timeline/header/TimelineProfileImageFrameController;

.field private F:LX/D2H;

.field public G:LX/BP0;

.field public H:LX/BQ1;

.field public I:Z

.field public J:I

.field public K:I

.field public L:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public M:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private N:Landroid/view/View$OnClickListener;

.field private O:LX/D2J;

.field private final P:Landroid/view/View$OnClickListener;

.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FwN;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/FvS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/FyN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fxm;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/BRs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/Fw4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/BQB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/D2I;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/Fsr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/Fyd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/FwP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2306691
    const-class v0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    const-string v1, "timeline"

    const-string v2, "cover_photo"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->D:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 11

    .prologue
    .line 2306692
    invoke-direct {p0, p1}, LX/Ban;-><init>(Landroid/content/Context;)V

    .line 2306693
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->K:I

    .line 2306694
    new-instance v0, LX/FyP;

    invoke-direct {v0, p0}, LX/FyP;-><init>(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)V

    iput-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->P:Landroid/view/View$OnClickListener;

    .line 2306695
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2306696
    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->x(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, LX/Bam;->NARROW:LX/Bam;

    :goto_0
    iput-object v1, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->d:LX/Bam;

    .line 2306697
    sget-object v1, LX/Bap;->IMAGE:LX/Bap;

    invoke-virtual {p0, v1}, LX/Ban;->setCoverType(LX/Bap;)V

    .line 2306698
    invoke-virtual {p0}, LX/Ban;->f()V

    .line 2306699
    iget-object v1, p0, LX/Ban;->k:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 2306700
    invoke-virtual {p0}, LX/Ban;->getProfileEditIconViewStub()Landroid/view/ViewStub;

    move-result-object v2

    .line 2306701
    const v1, 0x7f0d2d72

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 2306702
    iget-object v3, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->u:LX/0ad;

    sget-short v4, LX/0wf;->W:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    .line 2306703
    invoke-static {p0, v2, v1, v3}, LX/Bah;->a(LX/Ban;Landroid/view/ViewStub;Landroid/view/ViewStub;Z)V

    .line 2306704
    new-instance v3, LX/0zw;

    invoke-direct {v3, v2}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v3, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->L:LX/0zw;

    .line 2306705
    new-instance v2, LX/0zw;

    invoke-direct {v2, v1}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->M:LX/0zw;

    .line 2306706
    const v3, 0x7f0d2d7d

    invoke-virtual {p0, v3}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewStub;

    .line 2306707
    const v4, 0x7f031058

    invoke-virtual {v3, v4}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2306708
    new-instance v4, LX/0zw;

    invoke-direct {v4, v3}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    .line 2306709
    iget-object v3, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->q:LX/BRs;

    .line 2306710
    new-instance v8, LX/BRr;

    invoke-static {v3}, LX/BRn;->a(LX/0QB;)LX/BRn;

    move-result-object v5

    check-cast v5, LX/BRn;

    invoke-static {v3}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v3}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-direct {v8, v5, v6, v7, v4}, LX/BRr;-><init>(LX/BRn;Landroid/content/res/Resources;LX/0SG;LX/0zw;)V

    .line 2306711
    move-object v3, v8

    .line 2306712
    iget-object v4, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->r:LX/Fw4;

    .line 2306713
    new-instance v5, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;

    invoke-static {v4}, LX/Bas;->b(LX/0QB;)LX/Bas;

    move-result-object v7

    check-cast v7, LX/Bas;

    invoke-static {v4}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v8

    check-cast v8, LX/Fsr;

    invoke-static {v4}, LX/BQB;->a(LX/0QB;)LX/BQB;

    move-result-object v9

    check-cast v9, LX/BQB;

    invoke-static {v4}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    move-object v6, v3

    invoke-direct/range {v5 .. v10}, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;-><init>(LX/BRr;LX/Bas;LX/Fsr;LX/BQB;LX/0Uh;)V

    .line 2306714
    move-object v3, v5

    .line 2306715
    iput-object v3, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->E:Lcom/facebook/timeline/header/TimelineProfileImageFrameController;

    .line 2306716
    return-void

    .line 2306717
    :cond_0
    sget-object v1, LX/Bam;->STANDARD:LX/Bam;

    goto/16 :goto_0
.end method

.method private static a(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;LX/0Ot;LX/FvS;LX/FyN;LX/0Ot;LX/BRs;LX/Fw4;LX/BQB;LX/D2I;LX/0ad;LX/0W9;LX/Fsr;LX/Fyd;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/FwP;LX/0Or;LX/FwQ;Ljava/util/concurrent/Executor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;",
            "LX/0Ot",
            "<",
            "LX/FwN;",
            ">;",
            "LX/FvS;",
            "LX/FyN;",
            "LX/0Ot",
            "<",
            "LX/Fxm;",
            ">;",
            "LX/BRs;",
            "LX/Fw4;",
            "LX/BQB;",
            "LX/D2I;",
            "LX/0ad;",
            "LX/0W9;",
            "LX/Fsr;",
            "LX/Fyd;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/FwP;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/FwQ;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2306718
    iput-object p1, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->m:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->n:LX/FvS;

    iput-object p3, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->o:LX/FyN;

    iput-object p4, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->p:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->q:LX/BRs;

    iput-object p6, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->r:LX/Fw4;

    iput-object p7, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->s:LX/BQB;

    iput-object p8, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->t:LX/D2I;

    iput-object p9, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->u:LX/0ad;

    iput-object p10, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->v:LX/0W9;

    iput-object p11, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->w:LX/Fsr;

    iput-object p12, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->x:LX/Fyd;

    iput-object p13, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->y:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p14, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->z:LX/FwP;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->A:LX/0Or;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->B:LX/FwQ;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->C:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 20

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v19

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    const/16 v3, 0x3670

    move-object/from16 v0, v19

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static/range {v19 .. v19}, LX/FvS;->a(LX/0QB;)LX/FvS;

    move-result-object v4

    check-cast v4, LX/FvS;

    invoke-static/range {v19 .. v19}, LX/FyN;->a(LX/0QB;)LX/FyN;

    move-result-object v5

    check-cast v5, LX/FyN;

    const/16 v6, 0x3687

    move-object/from16 v0, v19

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const-class v7, LX/BRs;

    move-object/from16 v0, v19

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/BRs;

    const-class v8, LX/Fw4;

    move-object/from16 v0, v19

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/Fw4;

    invoke-static/range {v19 .. v19}, LX/BQB;->a(LX/0QB;)LX/BQB;

    move-result-object v9

    check-cast v9, LX/BQB;

    const-class v10, LX/D2I;

    move-object/from16 v0, v19

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/D2I;

    invoke-static/range {v19 .. v19}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-static/range {v19 .. v19}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v12

    check-cast v12, LX/0W9;

    invoke-static/range {v19 .. v19}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v13

    check-cast v13, LX/Fsr;

    const-class v14, LX/Fyd;

    move-object/from16 v0, v19

    invoke-interface {v0, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/Fyd;

    invoke-static/range {v19 .. v19}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v15

    check-cast v15, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-class v16, LX/FwP;

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/FwP;

    const/16 v17, 0x36a

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    invoke-static/range {v19 .. v19}, LX/FwQ;->a(LX/0QB;)LX/FwQ;

    move-result-object v18

    check-cast v18, LX/FwQ;

    invoke-static/range {v19 .. v19}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v19

    check-cast v19, Ljava/util/concurrent/Executor;

    invoke-static/range {v2 .. v19}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->a(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;LX/0Ot;LX/FvS;LX/FyN;LX/0Ot;LX/BRs;LX/Fw4;LX/BQB;LX/D2I;LX/0ad;LX/0W9;LX/Fsr;LX/Fyd;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/FwP;LX/0Or;LX/FwQ;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;Ljava/lang/String;Ljava/lang/String;LX/1bf;LX/FyW;)V
    .locals 8

    .prologue
    .line 2306719
    if-eqz p1, :cond_2

    .line 2306720
    const/4 v1, 0x0

    .line 2306721
    sget-object v2, LX/FyW;->PROFILE_PHOTO:LX/FyW;

    if-ne p4, v2, :cond_3

    .line 2306722
    sget-object v3, LX/74S;->TIMELINE_PROFILE_PHOTO:LX/74S;

    .line 2306723
    invoke-virtual {p0}, LX/Ban;->getProfileImageView()Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    move-result-object v2

    .line 2306724
    iget-object v1, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->M()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    :cond_0
    :goto_0
    move-object v6, v1

    move-object v7, v3

    .line 2306725
    :goto_1
    iget-object v1, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FwN;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v1 .. v7}, LX/FwN;->a(Lcom/facebook/drawee/view/DraweeView;Ljava/lang/String;Ljava/lang/String;LX/1bf;LX/1Fb;LX/74S;)V

    .line 2306726
    :cond_1
    :goto_2
    return-void

    .line 2306727
    :cond_2
    sget-object v0, LX/FyW;->PROFILE_PHOTO:LX/FyW;

    if-ne p4, v0, :cond_1

    .line 2306728
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->G()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    .line 2306729
    :goto_3
    goto :goto_2

    .line 2306730
    :cond_3
    sget-object v2, LX/FyW;->COVER_PHOTO:LX/FyW;

    if-ne p4, v2, :cond_4

    .line 2306731
    sget-object v3, LX/74S;->TIMELINE_COVER_PHOTO:LX/74S;

    .line 2306732
    iget-object v2, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    .line 2306733
    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->u(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v1, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->T()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;->e()LX/1Fb;

    move-result-object v1

    goto :goto_0

    :cond_4
    move-object v6, v1

    move-object v2, v1

    move-object v7, v1

    goto :goto_1

    .line 2306734
    :cond_5
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->w:LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->p()LX/FwE;

    move-result-object v0

    invoke-virtual {p0}, LX/Ban;->getProfileImageView()Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->G()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v3}, LX/BQ1;->M()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v3

    invoke-interface {v0, v1, v2, p3, v3}, LX/FwE;->a(Lcom/facebook/drawee/view/DraweeView;Ljava/lang/String;LX/1bf;LX/1Fb;)V

    goto :goto_3
.end method

.method public static b(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;II)Ljava/lang/CharSequence;
    .locals 10

    .prologue
    .line 2306735
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->s()Z

    move-result v0

    move v1, v0

    .line 2306736
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->v:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 2306737
    const-string v2, "kr"

    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "ar"

    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "hu"

    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "tr"

    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v3, v0

    .line 2306738
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    .line 2306739
    iget-object v4, v0, LX/BQ1;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    if-nez v4, :cond_6

    .line 2306740
    const/4 v4, 0x0

    .line 2306741
    :goto_1
    move-object v4, v4

    .line 2306742
    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->x(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0e0697

    :goto_2
    invoke-virtual {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v2, v4, p1, v0, v5}, LX/EQR;->a(Ljava/lang/String;Ljava/lang/String;IILandroid/content/Context;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    .line 2306743
    if-eqz v1, :cond_1

    .line 2306744
    invoke-virtual {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0815e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->x(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f0e0699

    :goto_3
    invoke-virtual {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 2306745
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2306746
    new-instance v6, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v6, v4, v0}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 2306747
    const/4 v7, 0x0

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v8

    const/16 v9, 0x21

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2306748
    move-object v0, v5

    .line 2306749
    if-eqz v3, :cond_4

    .line 2306750
    const-string v1, "\n"

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    .line 2306751
    :cond_1
    :goto_4
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    .line 2306752
    iget-object v1, v0, LX/BQ1;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    if-nez v1, :cond_7

    .line 2306753
    const/4 v1, 0x0

    .line 2306754
    :goto_5
    move v0, v1

    .line 2306755
    iget-object v1, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->q()Z

    move-result v1

    const/4 v3, 0x0

    const v4, 0x7f021a24

    const v5, 0x7f021afe

    invoke-virtual {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0d94

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iget-object v9, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->A:LX/0Or;

    move v8, p2

    invoke-static/range {v0 .. v9}, LX/EQR;->a(ZZLandroid/text/SpannableStringBuilder;Ljava/lang/String;IILandroid/content/Context;IILX/0Or;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0

    .line 2306756
    :cond_2
    const v0, 0x7f0e0696

    goto :goto_2

    .line 2306757
    :cond_3
    const v0, 0x7f0e0698

    goto :goto_3

    .line 2306758
    :cond_4
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    goto :goto_4

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_6
    iget-object v4, v0, LX/BQ1;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    invoke-virtual {v4}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->z()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    :cond_7
    iget-object v1, v0, LX/BQ1;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->C()Z

    move-result v1

    goto :goto_5
.end method

.method public static getProfilePhotoOrVideoClickListener(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2306604
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->N:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 2306605
    new-instance v0, LX/FyR;

    invoke-direct {v0, p0}, LX/FyR;-><init>(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)V

    iput-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->N:Landroid/view/View$OnClickListener;

    .line 2306606
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->N:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public static getProfileVideoController(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)LX/D2H;
    .locals 6

    .prologue
    .line 2306759
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->F:LX/D2H;

    if-nez v0, :cond_0

    .line 2306760
    new-instance v0, LX/FyV;

    invoke-direct {v0, p0}, LX/FyV;-><init>(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)V

    iput-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->O:LX/D2J;

    .line 2306761
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->t:LX/D2I;

    invoke-virtual {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->G:LX/BP0;

    invoke-virtual {v2}, LX/5SB;->i()Z

    move-result v3

    iget-object v4, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    iget-object v5, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->O:LX/D2J;

    move-object v2, p0

    invoke-virtual/range {v0 .. v5}, LX/D2I;->a(Landroid/content/Context;LX/Ban;ZLX/5w5;LX/D2J;)LX/D2H;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->F:LX/D2H;

    .line 2306762
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->F:LX/D2H;

    return-object v0
.end method

.method public static i(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)V
    .locals 12

    .prologue
    .line 2306763
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    if-nez v0, :cond_1

    .line 2306764
    :cond_0
    :goto_0
    return-void

    .line 2306765
    :cond_1
    iget-object v1, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->x(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)Z

    move-result v0

    if-eqz v0, :cond_9

    const v0, 0x7f0e068d

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setTitleTextAppearance(I)V

    .line 2306766
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2306767
    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->x(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)Z

    move-result v0

    if-eqz v0, :cond_a

    const v0, 0x7f0e068f

    :goto_2
    invoke-virtual {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0dd0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->b(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2306768
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/16 v2, 0x1e

    if-le v1, v2, :cond_2

    .line 2306769
    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->x(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)Z

    move-result v0

    if-eqz v0, :cond_b

    const v0, 0x7f0e0691

    :goto_3
    invoke-virtual {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0dd1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->b(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2306770
    :cond_2
    iget-object v1, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v1, v0}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2306771
    :cond_3
    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->x(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->t()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_c

    .line 2306772
    :cond_4
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2306773
    :goto_4
    iget-object v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->G:LX/BP0;

    if-nez v2, :cond_13

    .line 2306774
    :cond_5
    :goto_5
    iget-boolean v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->I:Z

    if-nez v0, :cond_6

    .line 2306775
    invoke-direct {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->l()V

    .line 2306776
    :cond_6
    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getProfileVideoController(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)LX/D2H;

    move-result-object v0

    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getProfilePhotoOrVideoClickListener(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/D2H;->a(Landroid/view/View$OnClickListener;)V

    .line 2306777
    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getProfileVideoController(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)LX/D2H;

    move-result-object v0

    invoke-virtual {v0}, LX/D2H;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2306778
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->s:LX/BQB;

    const/4 v1, 0x0

    .line 2306779
    iget-boolean v2, v0, LX/BQB;->l:Z

    if-nez v2, :cond_8

    .line 2306780
    new-instance v3, LX/0P2;

    invoke-direct {v3}, LX/0P2;-><init>()V

    const-string v4, "is_optimistic"

    if-eqz v1, :cond_16

    const-string v2, "1"

    :goto_6
    invoke-virtual {v3, v4, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    .line 2306781
    iget-object v3, v0, LX/BQB;->c:LX/BQD;

    const-string v4, "TimelineProfileVideoLoad"

    .line 2306782
    iget-object v5, v3, LX/BQD;->a:LX/11i;

    sget-object v6, LX/BQF;->a:LX/BQE;

    invoke-interface {v5, v6}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v6

    .line 2306783
    if-eqz v6, :cond_7

    .line 2306784
    iget-object v5, v3, LX/BQD;->c:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v9

    const v11, 0x44174c8a

    move-object v7, v4

    move-object v8, v2

    invoke-static/range {v6 .. v11}, LX/096;->a(LX/11o;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 2306785
    :cond_7
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/BQB;->l:Z

    .line 2306786
    :cond_8
    goto/16 :goto_0

    .line 2306787
    :cond_9
    const v0, 0x7f0e068c

    goto/16 :goto_1

    .line 2306788
    :cond_a
    const v0, 0x7f0e068e

    goto/16 :goto_2

    .line 2306789
    :cond_b
    const v0, 0x7f0e0690

    goto/16 :goto_3

    .line 2306790
    :cond_c
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->t()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemsConnectionFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v0

    .line 2306791
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 2306792
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 2306793
    :cond_d
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    .line 2306794
    invoke-virtual {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->G:LX/BP0;

    invoke-virtual {v2}, LX/5SB;->i()Z

    move-result v2

    iget-object v3, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->w:LX/Fsr;

    const/16 v9, 0x21

    const/4 v5, 0x0

    .line 2306795
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->d()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;

    move-result-object v4

    if-eqz v4, :cond_e

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->d()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_f

    .line 2306796
    :cond_e
    new-instance v4, Landroid/text/SpannableString;

    const-string v5, ""

    invoke-direct {v4, v5}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2306797
    :goto_7
    move-object v0, v4

    .line 2306798
    iget-object v1, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v1, v0}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 2306799
    :cond_f
    new-instance v6, LX/Fuz;

    invoke-direct {v6, v3, v0, v2}, LX/Fuz;-><init>(LX/Fsr;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;Z)V

    .line 2306800
    if-nez v2, :cond_10

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;

    move-result-object v4

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;

    if-ne v4, v7, :cond_11

    :cond_10
    const/4 v4, 0x1

    .line 2306801
    :goto_8
    new-instance v7, Landroid/text/style/TextAppearanceSpan;

    if-eqz v4, :cond_12

    const v4, 0x7f0e0693

    :goto_9
    invoke-direct {v7, v1, v4}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 2306802
    new-instance v4, Landroid/text/SpannableString;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->d()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2306803
    invoke-interface {v4}, Landroid/text/Spannable;->length()I

    move-result v8

    invoke-interface {v4, v7, v5, v8, v9}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2306804
    invoke-interface {v4}, Landroid/text/Spannable;->length()I

    move-result v7

    invoke-interface {v4, v6, v5, v7, v9}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_7

    :cond_11
    move v4, v5

    .line 2306805
    goto :goto_8

    .line 2306806
    :cond_12
    const v4, 0x7f0e0692

    goto :goto_9

    .line 2306807
    :cond_13
    iget-object v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->E:Lcom/facebook/timeline/header/TimelineProfileImageFrameController;

    iget-object v3, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    iget-object v4, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->G:LX/BP0;

    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getProfileVideoController(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)LX/D2H;

    move-result-object v5

    invoke-virtual {v5}, LX/D2H;->b()Z

    move-result v5

    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getProfileVideoController(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)LX/D2H;

    move-result-object v6

    invoke-virtual {v6}, LX/D2H;->c()Z

    move-result v6

    iget-boolean v7, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->I:Z

    iget-boolean v8, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->I:Z

    if-eqz v8, :cond_14

    const/4 v8, 0x0

    :goto_a
    invoke-virtual {p0}, LX/Ban;->getProfileImageView()Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    move-result-object v9

    iget-object v10, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->L:LX/0zw;

    const/4 v11, 0x1

    invoke-virtual/range {v2 .. v11}, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->a(LX/BQ1;LX/5SB;ZZZLandroid/view/View$OnClickListener;Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;LX/0zw;Z)V

    .line 2306808
    iget-boolean v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->I:Z

    if-nez v2, :cond_5

    .line 2306809
    iget-object v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->n:LX/FvS;

    iget-object v3, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->G:LX/BP0;

    iget-object v4, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {p0}, LX/Ban;->getProfileImageView()Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/FvS;->a(LX/5SB;LX/BQ1;Landroid/view/View;)V

    .line 2306810
    iget-object v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->G:LX/BP0;

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->G:LX/BP0;

    invoke-virtual {v2}, LX/5SB;->i()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 2306811
    iget-object v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->M:LX/0zw;

    invoke-virtual {v2}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2306812
    iget-object v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->M:LX/0zw;

    invoke-virtual {v2}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/Ban;->a(Landroid/view/View;)V

    goto/16 :goto_5

    .line 2306813
    :cond_14
    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getProfilePhotoOrVideoClickListener(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)Landroid/view/View$OnClickListener;

    move-result-object v8

    goto :goto_a

    .line 2306814
    :cond_15
    iget-object v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->M:LX/0zw;

    invoke-virtual {v2}, LX/0zw;->c()V

    goto/16 :goto_5

    .line 2306815
    :cond_16
    const-string v2, "0"

    goto/16 :goto_6
.end method

.method private l()V
    .locals 15

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 2306669
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->T()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->T()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->a()LX/1f8;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2306670
    new-instance v6, Landroid/graphics/PointF;

    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->T()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->a()LX/1f8;

    move-result-object v0

    invoke-interface {v0}, LX/1f8;->a()D

    move-result-wide v0

    double-to-float v0, v0

    iget-object v1, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->T()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->a()LX/1f8;

    move-result-object v1

    invoke-interface {v1}, LX/1f8;->b()D

    move-result-wide v8

    double-to-float v1, v8

    invoke-direct {v6, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2306671
    :goto_0
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v0}, LX/BPy;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->T()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/BQ3;->b(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2306672
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->B:LX/FwQ;

    .line 2306673
    sget-object v1, LX/BPA;->PHOTO_NONE:LX/BPA;

    invoke-static {v0, v1}, LX/FwQ;->a(LX/FwQ;LX/BPA;)V

    .line 2306674
    :cond_0
    iget-object v0, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    iget-object v1, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->B:LX/FwQ;

    iget-object v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    .line 2306675
    new-instance v5, LX/FwO;

    invoke-direct {v5, v0, v1, v2}, LX/FwO;-><init>(Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;LX/FwQ;LX/BQ1;)V

    .line 2306676
    move-object v12, v5

    .line 2306677
    iget-object v0, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {p0}, LX/Ban;->getScreenWidth()I

    move-result v1

    iget v2, p0, LX/Ban;->c:I

    iget-object v5, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->V()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v5

    iget-object v8, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->G:LX/BP0;

    invoke-virtual {v8}, LX/5SB;->b()Z

    move-result v8

    iget-object v9, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    .line 2306678
    iget-object v10, v9, LX/BQ1;->g:LX/BPw;

    .line 2306679
    iget-object v9, v10, LX/BPw;->d:[Ljava/lang/String;

    move-object v10, v9

    .line 2306680
    move-object v9, v10

    .line 2306681
    sget-object v10, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->D:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v11, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->P:Landroid/view/View$OnClickListener;

    iget-object v13, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v13}, LX/BPy;->j()Z

    move-result v13

    if-nez v13, :cond_2

    move v14, v3

    :goto_1
    move v13, v7

    invoke-virtual/range {v0 .. v14}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a(IIZLX/1bf;LX/1bf;Landroid/graphics/PointF;ZZ[Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;LX/1cC;ZZ)V

    .line 2306682
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->y:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/BQO;->c:LX/0Tn;

    invoke-interface {v0, v1, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2306683
    iget-object v0, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    iget-object v1, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->x:LX/Fyd;

    iget-object v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    .line 2306684
    new-instance v5, LX/Fyc;

    invoke-static {v1}, LX/BPC;->a(LX/0QB;)LX/BPC;

    move-result-object v3

    check-cast v3, LX/BPC;

    invoke-static {v1}, LX/BQ7;->a(LX/0QB;)LX/BQ7;

    move-result-object v4

    check-cast v4, LX/BQ7;

    invoke-direct {v5, v2, v3, v4}, LX/Fyc;-><init>(LX/BQ1;LX/BPC;LX/BQ7;)V

    .line 2306685
    move-object v1, v5

    .line 2306686
    invoke-virtual {v0, v1}, LX/1af;->a(Landroid/graphics/drawable/Drawable;)V

    .line 2306687
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->Z()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    if-nez v0, :cond_4

    .line 2306688
    :goto_2
    return-void

    :cond_2
    move v14, v7

    .line 2306689
    goto :goto_1

    :cond_3
    move-object v6, v4

    goto/16 :goto_0

    .line 2306690
    :cond_4
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->Z()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/FyQ;

    invoke-direct {v1, p0}, LX/FyQ;-><init>(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)V

    iget-object v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->C:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_2
.end method

.method public static p(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)V
    .locals 12

    .prologue
    .line 2306816
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->N()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->N()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    .line 2306817
    :goto_0
    iget-object v1, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->K()Ljava/lang/String;

    move-result-object v1

    .line 2306818
    iget-object v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    .line 2306819
    iget-object v3, v2, LX/BQ1;->f:LX/BQ0;

    .line 2306820
    iget-object v2, v3, LX/BQ0;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfilePhotoFieldsModel;

    if-eqz v2, :cond_0

    iget-object v2, v3, LX/BQ0;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfilePhotoFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfilePhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfilePhotoFieldsModel$AlbumModel;

    move-result-object v2

    if-nez v2, :cond_3

    .line 2306821
    :cond_0
    const/4 v2, 0x0

    .line 2306822
    :goto_1
    move-object v3, v2

    .line 2306823
    move-object v2, v3

    .line 2306824
    iget-object v3, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->G:LX/BP0;

    invoke-virtual {v3}, LX/5SB;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2306825
    iget-object v4, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->o:LX/FyN;

    invoke-virtual {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->G:LX/BP0;

    iget-object v7, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v7}, LX/BQ1;->w()Z

    move-result v7

    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->r(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)Z

    move-result v8

    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getProfileVideoController(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)LX/D2H;

    move-result-object v9

    invoke-virtual {v9}, LX/D2H;->b()Z

    move-result v9

    iget-object v10, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v10}, LX/BQ1;->x()Z

    move-result v10

    new-instance v11, LX/FyU;

    invoke-direct {v11, p0, v1, v2, v0}, LX/FyU;-><init>(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;Ljava/lang/String;Ljava/lang/String;LX/1bf;)V

    invoke-virtual/range {v4 .. v11}, LX/FyN;->a(Landroid/content/Context;LX/5SB;ZZZZLandroid/view/MenuItem$OnMenuItemClickListener;)LX/5OM;

    move-result-object v4

    .line 2306826
    iget-object v5, p0, LX/Ban;->l:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    invoke-virtual {v4, v5}, LX/0ht;->a(Landroid/view/View;)V

    .line 2306827
    :goto_2
    return-void

    .line 2306828
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2306829
    :cond_2
    sget-object v3, LX/FyW;->PROFILE_PHOTO:LX/FyW;

    invoke-static {p0, v1, v2, v0, v3}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->a$redex0(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;Ljava/lang/String;Ljava/lang/String;LX/1bf;LX/FyW;)V

    goto :goto_2

    :cond_3
    iget-object v2, v3, LX/BQ0;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfilePhotoFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfilePhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfilePhotoFieldsModel$AlbumModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfilePhotoFieldsModel$AlbumModel;->b()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public static q(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)V
    .locals 1

    .prologue
    .line 2306666
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->w:LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->p()LX/FwE;

    move-result-object v0

    invoke-interface {v0}, LX/FwE;->g()V

    .line 2306667
    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getProfileVideoController(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)LX/D2H;

    move-result-object v0

    invoke-virtual {v0}, LX/D2H;->e()V

    .line 2306668
    return-void
.end method

.method public static r(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2306658
    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getProfileVideoController(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)LX/D2H;

    move-result-object v1

    invoke-virtual {v1}, LX/D2H;->c()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    .line 2306659
    iget-object v2, v1, LX/BQ1;->f:LX/BQ0;

    .line 2306660
    iget-object v1, v2, LX/BQ0;->c:Landroid/net/Uri;

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v2, v1

    .line 2306661
    move v1, v2

    .line 2306662
    if-eqz v1, :cond_1

    .line 2306663
    :cond_0
    :goto_1
    return v0

    .line 2306664
    :cond_1
    iget-object v1, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->N()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2306665
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->w()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static u(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)Z
    .locals 1

    .prologue
    .line 2306657
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->T()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->T()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static x(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)Z
    .locals 3

    .prologue
    .line 2306656
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->u:LX/0ad;

    sget-short v1, LX/0wf;->W:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/BP0;LX/BQ1;)Z
    .locals 5

    .prologue
    .line 2306640
    const-string v0, "CaspianTimelineStandardHeader.bindModel"

    const v1, 0x50961a97    # 2.01465999E10f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2306641
    :try_start_0
    const/4 v0, 0x0

    .line 2306642
    if-nez p2, :cond_1

    .line 2306643
    :cond_0
    :goto_0
    move v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2306644
    const v1, -0x5ac8b797

    invoke-static {v1}, LX/02m;->a(I)V

    return v0

    :catchall_0
    move-exception v0

    const v1, 0x22579d3e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2306645
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    .line 2306646
    iget v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->J:I

    if-ne v2, v1, :cond_2

    iget v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->K:I

    iget-object v3, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    .line 2306647
    iget v4, v3, LX/BPy;->c:I

    move v3, v4

    .line 2306648
    if-ge v2, v3, :cond_0

    .line 2306649
    :cond_2
    iget v0, p2, LX/BPy;->c:I

    move v0, v0

    .line 2306650
    iput v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->K:I

    .line 2306651
    iput v1, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->J:I

    .line 2306652
    iput-object p1, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->G:LX/BP0;

    .line 2306653
    iput-object p2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    .line 2306654
    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->i(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)V

    .line 2306655
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2306619
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->Z()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2306620
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->Z()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2306621
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->E:Lcom/facebook/timeline/header/TimelineProfileImageFrameController;

    if-eqz v0, :cond_3

    .line 2306622
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->E:Lcom/facebook/timeline/header/TimelineProfileImageFrameController;

    .line 2306623
    iget-object v1, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->c:LX/BRr;

    .line 2306624
    iget-object v3, v1, LX/BRr;->d:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2306625
    iget-object v3, v1, LX/BRr;->d:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->a()Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;

    invoke-virtual {v3}, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;->clearAnimation()V

    .line 2306626
    iget-object v3, v1, LX/BRr;->e:Ljava/lang/Runnable;

    if-eqz v3, :cond_1

    .line 2306627
    iget-object v3, v1, LX/BRr;->d:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->a()Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;

    iget-object v4, v1, LX/BRr;->e:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2306628
    :cond_1
    iget-object v3, v1, LX/BRr;->f:Ljava/lang/Runnable;

    if-eqz v3, :cond_2

    .line 2306629
    iget-object v3, v1, LX/BRr;->d:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->a()Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;

    iget-object v4, v1, LX/BRr;->f:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2306630
    :cond_2
    const/4 v3, 0x1

    iput-boolean v3, v1, LX/BRr;->h:Z

    .line 2306631
    const/4 v3, 0x0

    iput-object v3, v1, LX/BRr;->i:LX/Fw2;

    .line 2306632
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->h:Z

    .line 2306633
    :cond_3
    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getProfileVideoController(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)LX/D2H;

    move-result-object v0

    .line 2306634
    iget-object v1, v0, LX/D2H;->c:LX/Ban;

    invoke-virtual {v1}, LX/Ban;->getLazyProfileVideoView()LX/0zw;

    move-result-object v1

    invoke-virtual {v1}, LX/0zw;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2306635
    iget-object v1, v0, LX/D2H;->c:LX/Ban;

    invoke-virtual {v1}, LX/Ban;->getLazyProfileVideoView()LX/0zw;

    move-result-object v1

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 2306636
    :cond_4
    iput-object v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    .line 2306637
    iput-object v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->E:Lcom/facebook/timeline/header/TimelineProfileImageFrameController;

    .line 2306638
    iput-object v2, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->G:LX/BP0;

    .line 2306639
    return-void
.end method

.method public final cr_()Z
    .locals 1

    .prologue
    .line 2306618
    const/4 v0, 0x1

    return v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2306613
    invoke-super {p0, p1}, LX/Ban;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2306614
    invoke-virtual {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->b:I

    .line 2306615
    invoke-virtual {p0}, LX/Ban;->f()V

    .line 2306616
    invoke-static {p0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->i(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)V

    .line 2306617
    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2306607
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->M:LX/0zw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->M:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2306608
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->M:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 2306609
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->M:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2306610
    iget-object v1, p0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->M:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2306611
    :cond_0
    invoke-super {p0, p1, p2}, LX/Ban;->onMeasure(II)V

    .line 2306612
    return-void
.end method
