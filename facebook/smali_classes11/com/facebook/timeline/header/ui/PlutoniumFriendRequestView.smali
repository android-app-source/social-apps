.class public Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/BP9;


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BPp;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/widget/Button;

.field private d:Landroid/widget/Button;

.field public e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Landroid/widget/TextView;

.field public h:LX/BPy;

.field public i:LX/5SB;

.field private j:I

.field private final k:Landroid/view/View$OnClickListener;

.field private final l:Landroid/view/View$OnClickListener;

.field private final m:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2306908
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2306909
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->j:I

    .line 2306910
    new-instance v0, LX/FyZ;

    invoke-direct {v0, p0}, LX/FyZ;-><init>(Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;)V

    iput-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->k:Landroid/view/View$OnClickListener;

    .line 2306911
    new-instance v0, LX/Fya;

    invoke-direct {v0, p0}, LX/Fya;-><init>(Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;)V

    iput-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->l:Landroid/view/View$OnClickListener;

    .line 2306912
    new-instance v0, LX/Fyb;

    invoke-direct {v0, p0}, LX/Fyb;-><init>(Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;)V

    iput-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->m:Landroid/view/View$OnClickListener;

    .line 2306913
    invoke-direct {p0}, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->a()V

    .line 2306914
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2306959
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2306960
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->j:I

    .line 2306961
    new-instance v0, LX/FyZ;

    invoke-direct {v0, p0}, LX/FyZ;-><init>(Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;)V

    iput-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->k:Landroid/view/View$OnClickListener;

    .line 2306962
    new-instance v0, LX/Fya;

    invoke-direct {v0, p0}, LX/Fya;-><init>(Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;)V

    iput-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->l:Landroid/view/View$OnClickListener;

    .line 2306963
    new-instance v0, LX/Fyb;

    invoke-direct {v0, p0}, LX/Fyb;-><init>(Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;)V

    iput-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->m:Landroid/view/View$OnClickListener;

    .line 2306964
    invoke-direct {p0}, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->a()V

    .line 2306965
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2306967
    const-class v0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2306968
    const v0, 0x7f030fe2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2306969
    const v0, 0x7f0d2651

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->g:Landroid/widget/TextView;

    .line 2306970
    const v0, 0x7f0d2652

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->e:Landroid/view/View;

    .line 2306971
    const v0, 0x7f0d2653

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->c:Landroid/widget/Button;

    .line 2306972
    const v0, 0x7f0d2654

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->d:Landroid/widget/Button;

    .line 2306973
    const v0, 0x7f0208f0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->setBackgroundResource(I)V

    .line 2306974
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->setOrientation(I)V

    .line 2306975
    return-void
.end method

.method private static a(Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;LX/0Or;LX/23P;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;",
            "LX/0Or",
            "<",
            "LX/BPp;",
            ">;",
            "LX/23P;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2306966
    iput-object p1, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->b:LX/23P;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;

    const/16 v1, 0x3645

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v0

    check-cast v0, LX/23P;

    invoke-static {p0, v1, v0}, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->a(Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;LX/0Or;LX/23P;)V

    return-void
.end method

.method public static b(Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2306949
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->f:Landroid/view/View;

    if-nez v0, :cond_0

    .line 2306950
    const v0, 0x7f0d2655

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->f:Landroid/view/View;

    .line 2306951
    invoke-virtual {p0}, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0da7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2306952
    invoke-virtual {p0}, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0da2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2306953
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v0, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2306954
    invoke-virtual {v2, v3, v1, v3, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2306955
    const/16 v0, 0x11

    iput v0, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2306956
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2306957
    :goto_0
    return-void

    .line 2306958
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private b(LX/BPy;LX/5SB;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2306924
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 2306925
    :cond_0
    invoke-direct {p0}, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->d()V

    .line 2306926
    :cond_1
    :goto_0
    return v0

    .line 2306927
    :cond_2
    iget-object v2, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->h:LX/BPy;

    if-ne v2, p1, :cond_3

    iget-object v2, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->i:LX/5SB;

    if-eq v2, p2, :cond_6

    :cond_3
    move v2, v1

    .line 2306928
    :goto_1
    if-nez v2, :cond_4

    iget v2, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->j:I

    iget-object v3, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->h:LX/BPy;

    .line 2306929
    iget v4, v3, LX/BPy;->d:I

    move v3, v4

    .line 2306930
    if-ge v2, v3, :cond_1

    .line 2306931
    :cond_4
    iput-object p1, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->h:LX/BPy;

    .line 2306932
    iput-object p2, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->i:LX/5SB;

    .line 2306933
    iget-object v2, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->f:Landroid/view/View;

    if-eqz v2, :cond_5

    .line 2306934
    iget-object v2, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->f:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2306935
    :cond_5
    iget-object v2, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->e:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2306936
    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->setVisibility(I)V

    .line 2306937
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->g:Landroid/widget/TextView;

    const v2, 0x7f0815c3    # 1.80888E38f

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 2306938
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->c:Landroid/widget/Button;

    const v2, 0x7f08153c

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 2306939
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->c:Landroid/widget/Button;

    sget-object v2, LX/2na;->CONFIRM:LX/2na;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 2306940
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->c:Landroid/widget/Button;

    iget-object v2, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2306941
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->d:Landroid/widget/Button;

    const v2, 0x7f081593

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 2306942
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->d:Landroid/widget/Button;

    sget-object v2, LX/2na;->REJECT:LX/2na;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 2306943
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->d:Landroid/widget/Button;

    iget-object v2, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2306944
    iget-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->h:LX/BPy;

    .line 2306945
    iget v2, v0, LX/BPy;->d:I

    move v0, v2

    .line 2306946
    iput v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->j:I

    move v0, v1

    .line 2306947
    goto :goto_0

    :cond_6
    move v2, v0

    .line 2306948
    goto :goto_1
.end method

.method private d()V
    .locals 1

    .prologue
    .line 2306922
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->setVisibility(I)V

    .line 2306923
    return-void
.end method


# virtual methods
.method public final a(LX/BPy;LX/5SB;)Z
    .locals 2

    .prologue
    .line 2306919
    const-string v0, "PlutoniumFriendRequestView.bindModel"

    const v1, -0x2e5f6a1d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2306920
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->b(LX/BPy;LX/5SB;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2306921
    const v1, -0x1490fbaa

    invoke-static {v1}, LX/02m;->a(I)V

    return v0

    :catchall_0
    move-exception v0

    const v1, 0x54884f9d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2306915
    iput-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->h:LX/BPy;

    .line 2306916
    iput-object v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->i:LX/5SB;

    .line 2306917
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->j:I

    .line 2306918
    return-void
.end method
