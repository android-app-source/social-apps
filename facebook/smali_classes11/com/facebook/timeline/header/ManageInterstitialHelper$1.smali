.class public final Lcom/facebook/timeline/header/ManageInterstitialHelper$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/view/ViewGroup;

.field public final synthetic b:LX/3kS;

.field public final synthetic c:LX/BQ1;

.field public final synthetic d:LX/Fur;


# direct methods
.method public constructor <init>(LX/Fur;Landroid/view/ViewGroup;LX/3kS;LX/BQ1;)V
    .locals 0

    .prologue
    .line 2300566
    iput-object p1, p0, Lcom/facebook/timeline/header/ManageInterstitialHelper$1;->d:LX/Fur;

    iput-object p2, p0, Lcom/facebook/timeline/header/ManageInterstitialHelper$1;->a:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/facebook/timeline/header/ManageInterstitialHelper$1;->b:LX/3kS;

    iput-object p4, p0, Lcom/facebook/timeline/header/ManageInterstitialHelper$1;->c:LX/BQ1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2300567
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/facebook/timeline/header/ManageInterstitialHelper$1;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2300568
    iget-object v0, p0, Lcom/facebook/timeline/header/ManageInterstitialHelper$1;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2300569
    instance-of v3, v0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;

    if-eqz v3, :cond_1

    .line 2300570
    check-cast v0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;

    .line 2300571
    invoke-virtual {v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f081543

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2300572
    iget-object v1, p0, Lcom/facebook/timeline/header/ManageInterstitialHelper$1;->b:LX/3kS;

    const/4 v3, 0x1

    .line 2300573
    iput-boolean v3, v1, LX/3kS;->d:Z

    .line 2300574
    iget-object v1, p0, Lcom/facebook/timeline/header/ManageInterstitialHelper$1;->d:LX/Fur;

    iget-object v3, p0, Lcom/facebook/timeline/header/ManageInterstitialHelper$1;->c:LX/BQ1;

    .line 2300575
    if-nez v0, :cond_2

    .line 2300576
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/facebook/timeline/header/ManageInterstitialHelper$1;->b:LX/3kS;

    .line 2300577
    iput-boolean v2, v0, LX/3kS;->d:Z

    .line 2300578
    return-void

    .line 2300579
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2300580
    :cond_2
    iget-object v4, v1, LX/Fur;->a:LX/0iA;

    new-instance v5, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v6, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v5, v6}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v6, LX/3kS;

    invoke-virtual {v4, v5, v6}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v4

    check-cast v4, LX/3kS;

    .line 2300581
    if-eqz v4, :cond_0

    .line 2300582
    invoke-virtual {v3}, LX/BQ1;->z()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, LX/3kS;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 2300583
    iget-object v5, v1, LX/Fur;->a:LX/0iA;

    invoke-virtual {v5}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v5

    invoke-virtual {v4}, LX/3kS;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2300584
    invoke-virtual {v4}, LX/3kS;->d()V

    goto :goto_1
.end method
