.class public final Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x73c595a6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$PhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$UserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2309049
    const-class v0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2309029
    const-class v0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2309047
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2309048
    return-void
.end method

.method private j()Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$UserModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2309045
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;->f:Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$UserModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$UserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$UserModel;

    iput-object v0, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;->f:Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$UserModel;

    .line 2309046
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;->f:Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$UserModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2309037
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2309038
    invoke-virtual {p0}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;->a()Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$PhotoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2309039
    invoke-direct {p0}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;->j()Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$UserModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2309040
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2309041
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2309042
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2309043
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2309044
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2309050
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2309051
    invoke-virtual {p0}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;->a()Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$PhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2309052
    invoke-virtual {p0}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;->a()Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$PhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$PhotoModel;

    .line 2309053
    invoke-virtual {p0}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;->a()Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$PhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2309054
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;

    .line 2309055
    iput-object v0, v1, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;->e:Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$PhotoModel;

    .line 2309056
    :cond_0
    invoke-direct {p0}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;->j()Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$UserModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2309057
    invoke-direct {p0}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;->j()Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$UserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$UserModel;

    .line 2309058
    invoke-direct {p0}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;->j()Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$UserModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2309059
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;

    .line 2309060
    iput-object v0, v1, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;->f:Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$UserModel;

    .line 2309061
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2309062
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$PhotoModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2309035
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;->e:Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$PhotoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$PhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$PhotoModel;

    iput-object v0, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;->e:Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$PhotoModel;

    .line 2309036
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;->e:Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$PhotoModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2309032
    new-instance v0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;

    invoke-direct {v0}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;-><init>()V

    .line 2309033
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2309034
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2309031
    const v0, -0x564db1d5

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2309030
    const v0, 0x7527c804

    return v0
.end method
