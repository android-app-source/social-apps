.class public final Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2310188
    const-class v0, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;

    new-instance v1, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2310189
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2310212
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2310191
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2310192
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x2

    .line 2310193
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2310194
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2310195
    if-eqz v2, :cond_0

    .line 2310196
    const-string p0, "can_viewer_act_as_memorial_contact"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2310197
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2310198
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2310199
    if-eqz v2, :cond_1

    .line 2310200
    const-string p0, "first_name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2310201
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2310202
    :cond_1
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result v2

    .line 2310203
    if-eqz v2, :cond_2

    .line 2310204
    const-string v2, "gender"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2310205
    invoke-virtual {v1, v0, p2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2310206
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2310207
    if-eqz v2, :cond_3

    .line 2310208
    const-string p0, "is_memorialized"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2310209
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2310210
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2310211
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2310190
    check-cast p1, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel$Serializer;->a(Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;LX/0nX;LX/0my;)V

    return-void
.end method
