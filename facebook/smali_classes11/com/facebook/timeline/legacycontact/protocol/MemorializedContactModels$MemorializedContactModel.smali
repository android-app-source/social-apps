.class public final Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x456c5e78
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLGender;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2310229
    const-class v0, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2310230
    const-class v0, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2310231
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2310232
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2310233
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2310234
    invoke-virtual {p0}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2310235
    invoke-virtual {p0}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->k()Lcom/facebook/graphql/enums/GraphQLGender;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2310236
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2310237
    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->e:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 2310238
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2310239
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2310240
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2310241
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2310242
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2310248
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2310249
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2310250
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2310243
    new-instance v0, LX/G0k;

    invoke-direct {v0, p1}, LX/G0k;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2310244
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2310245
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->e:Z

    .line 2310246
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->h:Z

    .line 2310247
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2310227
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2310228
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2310226
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2310213
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2310214
    iget-boolean v0, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2310223
    new-instance v0, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;

    invoke-direct {v0}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;-><init>()V

    .line 2310224
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2310225
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2310222
    const v0, 0x1b5756b3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2310221
    const v0, 0x285feb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2310219
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->f:Ljava/lang/String;

    .line 2310220
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLGender;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2310217
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->g:Lcom/facebook/graphql/enums/GraphQLGender;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGender;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGender;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGender;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGender;

    iput-object v0, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->g:Lcom/facebook/graphql/enums/GraphQLGender;

    .line 2310218
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->g:Lcom/facebook/graphql/enums/GraphQLGender;

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 2310215
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2310216
    iget-boolean v0, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->h:Z

    return v0
.end method
