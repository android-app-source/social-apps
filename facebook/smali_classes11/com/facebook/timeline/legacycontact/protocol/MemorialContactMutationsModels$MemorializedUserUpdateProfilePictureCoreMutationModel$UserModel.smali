.class public final Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserUpdateProfilePictureCoreMutationModel$UserModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserUpdateProfilePictureCoreMutationModel$UserModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserUpdateProfilePictureCoreMutationModel$UserModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2309564
    const-class v0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserUpdateProfilePictureCoreMutationModel$UserModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2309551
    const-class v0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserUpdateProfilePictureCoreMutationModel$UserModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2309565
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2309566
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2309567
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserUpdateProfilePictureCoreMutationModel$UserModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserUpdateProfilePictureCoreMutationModel$UserModel;->e:Ljava/lang/String;

    .line 2309568
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserUpdateProfilePictureCoreMutationModel$UserModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2309569
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2309570
    invoke-direct {p0}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserUpdateProfilePictureCoreMutationModel$UserModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2309571
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2309572
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2309573
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2309574
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2309560
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2309561
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2309562
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2309563
    new-instance v0, LX/G0T;

    invoke-direct {v0, p1}, LX/G0T;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2309559
    invoke-direct {p0}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserUpdateProfilePictureCoreMutationModel$UserModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2309557
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2309558
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2309556
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2309553
    new-instance v0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserUpdateProfilePictureCoreMutationModel$UserModel;

    invoke-direct {v0}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserUpdateProfilePictureCoreMutationModel$UserModel;-><init>()V

    .line 2309554
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2309555
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2309552
    const v0, -0x3ee4665e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2309550
    const v0, 0x285feb

    return v0
.end method
