.class public final Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2310149
    const-class v0, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;

    new-instance v1, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2310150
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2310151
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2310152
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2310153
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2310154
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 2310155
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2310156
    :goto_0
    move v1, v2

    .line 2310157
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2310158
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2310159
    new-instance v1, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;

    invoke-direct {v1}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;-><init>()V

    .line 2310160
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2310161
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2310162
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2310163
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2310164
    :cond_0
    return-object v1

    .line 2310165
    :cond_1
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_6

    .line 2310166
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2310167
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2310168
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_1

    if-eqz v9, :cond_1

    .line 2310169
    const-string p0, "can_viewer_act_as_memorial_contact"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 2310170
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v4

    move v8, v4

    move v4, v3

    goto :goto_1

    .line 2310171
    :cond_2
    const-string p0, "first_name"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2310172
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 2310173
    :cond_3
    const-string p0, "gender"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 2310174
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLGender;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGender;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    goto :goto_1

    .line 2310175
    :cond_4
    const-string p0, "is_memorialized"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 2310176
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v5, v1

    move v1, v3

    goto :goto_1

    .line 2310177
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2310178
    :cond_6
    const/4 v9, 0x4

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 2310179
    if-eqz v4, :cond_7

    .line 2310180
    invoke-virtual {v0, v2, v8}, LX/186;->a(IZ)V

    .line 2310181
    :cond_7
    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 2310182
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 2310183
    if-eqz v1, :cond_8

    .line 2310184
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v5}, LX/186;->a(IZ)V

    .line 2310185
    :cond_8
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_9
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    goto/16 :goto_1
.end method
