.class public final Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2308879
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2308880
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2308877
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2308878
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 2308853
    if-nez p1, :cond_0

    .line 2308854
    :goto_0
    return v0

    .line 2308855
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2308856
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2308857
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2308858
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2308859
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2308860
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2308861
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2308862
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2308863
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2308864
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2308865
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2308866
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2308867
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2308868
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2308869
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2308870
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2308871
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2308872
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2308873
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2308874
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2308875
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2308876
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6827f7fb -> :sswitch_3
        0x4df16cf -> :sswitch_1
        0x4f2cb05 -> :sswitch_2
        0x7f663c91 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2308852
    new-instance v0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2308849
    sparse-switch p0, :sswitch_data_0

    .line 2308850
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2308851
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x6827f7fb -> :sswitch_0
        0x4df16cf -> :sswitch_0
        0x4f2cb05 -> :sswitch_0
        0x7f663c91 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2308848
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2308815
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$DraculaImplementation;->b(I)V

    .line 2308816
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2308843
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2308844
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2308845
    :cond_0
    iput-object p1, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$DraculaImplementation;->a:LX/15i;

    .line 2308846
    iput p2, p0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$DraculaImplementation;->b:I

    .line 2308847
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2308842
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2308841
    new-instance v0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2308838
    iget v0, p0, LX/1vt;->c:I

    .line 2308839
    move v0, v0

    .line 2308840
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2308835
    iget v0, p0, LX/1vt;->c:I

    .line 2308836
    move v0, v0

    .line 2308837
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2308832
    iget v0, p0, LX/1vt;->b:I

    .line 2308833
    move v0, v0

    .line 2308834
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2308829
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2308830
    move-object v0, v0

    .line 2308831
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2308820
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2308821
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2308822
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2308823
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2308824
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2308825
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2308826
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2308827
    invoke-static {v3, v9, v2}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2308828
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2308817
    iget v0, p0, LX/1vt;->c:I

    .line 2308818
    move v0, v0

    .line 2308819
    return v0
.end method
