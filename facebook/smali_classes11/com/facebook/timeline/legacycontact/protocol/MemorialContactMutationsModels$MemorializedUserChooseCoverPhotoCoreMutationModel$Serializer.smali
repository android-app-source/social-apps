.class public final Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2308980
    const-class v0, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;

    new-instance v1, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2308981
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2308979
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2308965
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2308966
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2308967
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2308968
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2308969
    if-eqz v2, :cond_0

    .line 2308970
    const-string p0, "photo"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2308971
    invoke-static {v1, v2, p1, p2}, LX/G0X;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2308972
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2308973
    if-eqz v2, :cond_1

    .line 2308974
    const-string p0, "user"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2308975
    invoke-static {v1, v2, p1}, LX/G0Y;->a(LX/15i;ILX/0nX;)V

    .line 2308976
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2308977
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2308978
    check-cast p1, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel$Serializer;->a(Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseCoverPhotoCoreMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
