.class public Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;
.super Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# static fields
.field public static final v:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public s:LX/2dj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/2hS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/EwG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private w:Landroid/view/View;

.field public x:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

.field private y:LX/2hs;

.field public z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Ewi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2308596
    const-class v0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->v:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2308595
    invoke-direct {p0}, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;LX/2dj;LX/2hS;LX/EwG;)V
    .locals 0

    .prologue
    .line 2308594
    iput-object p1, p0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->s:LX/2dj;

    iput-object p2, p0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->t:LX/2hS;

    iput-object p3, p0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->u:LX/EwG;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    invoke-static {v2}, LX/2dj;->b(LX/0QB;)LX/2dj;

    move-result-object v0

    check-cast v0, LX/2dj;

    const-class v1, LX/2hS;

    invoke-interface {v2, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/2hS;

    invoke-static {v2}, LX/EwG;->b(LX/0QB;)LX/EwG;

    move-result-object v2

    check-cast v2, LX/EwG;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->a(Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;LX/2dj;LX/2hS;LX/EwG;)V

    return-void
.end method

.method public static b$redex0(Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;Z)V
    .locals 4

    .prologue
    .line 2308591
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->y:LX/2hs;

    new-instance v1, LX/G07;

    invoke-direct {v1, p0, p1}, LX/G07;-><init>(Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;Z)V

    .line 2308592
    iget-object v2, v0, LX/2hs;->h:LX/1Ck;

    const-string v3, "CLEAR_FRIEND_REQUEST_CACHE_TASK"

    new-instance p0, LX/83g;

    invoke-direct {p0, v0}, LX/83g;-><init>(LX/2hs;)V

    new-instance p1, LX/83h;

    invoke-direct {p1, v0, v1}, LX/83h;-><init>(LX/2hs;LX/0TF;)V

    invoke-virtual {v2, v3, p0, p1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2308593
    return-void
.end method

.method public static c(Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;Z)V
    .locals 5

    .prologue
    .line 2308588
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->s:LX/2dj;

    invoke-virtual {v0}, LX/2dj;->f()Z

    move-result v0

    .line 2308589
    iget-object v1, p0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->q:LX/1Ck;

    const-string v2, "FETCH_FRIEND_REQUESTS_TASK"

    new-instance v3, LX/G08;

    invoke-direct {v3, p0, p1}, LX/G08;-><init>(Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;Z)V

    new-instance v4, LX/G09;

    invoke-direct {v4, p0, v0, p1}, LX/G09;-><init>(Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;ZZ)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2308590
    return-void
.end method

.method private m()Lcom/facebook/widget/listview/BetterListView;
    .locals 2

    .prologue
    .line 2308585
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    .line 2308586
    new-instance v1, LX/G06;

    invoke-direct {v1, p0}, LX/G06;-><init>(Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2308587
    return-object v0
.end method

.method public static n(Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;)V
    .locals 2

    .prologue
    .line 2308583
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->w:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2308584
    return-void
.end method

.method public static o(Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;)V
    .locals 2

    .prologue
    .line 2308581
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->w:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2308582
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 2308558
    sget-object v0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->v:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public final a(Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;)V
    .locals 1

    .prologue
    .line 2308579
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->b$redex0(Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;Z)V

    .line 2308580
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2308578
    const v0, 0x7f0309de

    return v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2308559
    invoke-super {p0, p1}, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->b(Landroid/os/Bundle;)V

    .line 2308560
    invoke-static {p0, p0}, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2308561
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->t:LX/2hS;

    iget-object v1, p0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->q:LX/1Ck;

    invoke-virtual {v0, v1}, LX/2hS;->a(LX/1Ck;)LX/2hs;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->y:LX/2hs;

    .line 2308562
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->z:Ljava/util/List;

    .line 2308563
    invoke-direct {p0}, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->m()Lcom/facebook/widget/listview/BetterListView;

    move-result-object v0

    .line 2308564
    new-instance v1, LX/2iI;

    invoke-direct {v1, v0}, LX/2iI;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    .line 2308565
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->u:LX/EwG;

    invoke-interface {v1, v0}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2308566
    invoke-interface {v1, v2}, LX/0g8;->c(Z)V

    .line 2308567
    invoke-interface {v1}, LX/0g8;->k()V

    .line 2308568
    const v0, 0x7f0d12cc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    iput-object v0, p0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->x:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    .line 2308569
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->x:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    new-instance v1, LX/G04;

    invoke-direct {v1, p0}, LX/G04;-><init>(Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;)V

    invoke-virtual {v0, v1}, LX/62l;->setOnRefreshListener(LX/62n;)V

    .line 2308570
    const v0, 0x1020004

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->w:Landroid/view/View;

    .line 2308571
    const v0, 0x7f0d12ca

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2308572
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2308573
    const v1, 0x7f0818ab

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2308574
    new-instance v1, LX/G05;

    invoke-direct {v1, p0}, LX/G05;-><init>(Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2308575
    invoke-static {p0}, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->o(Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;)V

    .line 2308576
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->u:LX/EwG;

    invoke-virtual {v0, v2}, LX/EwG;->a(Z)V

    .line 2308577
    return-void
.end method
