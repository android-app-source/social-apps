.class public Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;
.super Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# static fields
.field private static final u:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public s:Lcom/facebook/user/model/User;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2308764
    const-class v0, Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;->u:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2308763
    invoke-direct {p0}, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;Lcom/facebook/user/model/User;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 2308762
    iput-object p1, p0, Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;->s:Lcom/facebook/user/model/User;

    iput-object p2, p0, Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;->t:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;

    invoke-static {v1}, LX/0XE;->b(LX/0QB;)Lcom/facebook/user/model/User;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0, v0, v1}, Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;->a(Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;Lcom/facebook/user/model/User;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 2308761
    sget-object v0, Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;->u:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public final a(Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2308752
    const v0, 0x7f0d190c

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2308753
    const v1, 0x7f083353

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2308754
    const v0, 0x7f0d190d

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2308755
    const v1, 0x7f083354

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;->s:Lcom/facebook/user/model/User;

    invoke-virtual {v3}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v2}, Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2308756
    const v0, 0x7f0d190b

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 2308757
    invoke-virtual {v0, v4}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2308758
    const v0, 0x7f0d0525

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 2308759
    new-instance v1, LX/G0I;

    invoke-direct {v1, p0, p0}, LX/G0I;-><init>(Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2308760
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2308751
    const v0, 0x7f0309dc

    return v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2308748
    invoke-super {p0, p1}, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->b(Landroid/os/Bundle;)V

    .line 2308749
    invoke-static {p0, p0}, Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2308750
    return-void
.end method
