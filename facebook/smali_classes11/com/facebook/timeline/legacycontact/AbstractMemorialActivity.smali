.class public abstract Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public p:LX/Fzo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Ljava/lang/String;

.field public s:LX/0h5;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2308122
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;

    invoke-static {v1}, LX/Fzo;->a(LX/0QB;)LX/Fzo;

    move-result-object v0

    check-cast v0, LX/Fzo;

    invoke-static {v1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    iput-object v0, p0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->p:LX/Fzo;

    iput-object v1, p0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->q:LX/1Ck;

    return-void
.end method


# virtual methods
.method public abstract a()Lcom/facebook/common/callercontext/CallerContext;
.end method

.method public abstract a(Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;)V
.end method

.method public abstract b()I
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2308123
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2308124
    invoke-static {p0, p0}, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2308125
    invoke-virtual {p0}, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->r:Ljava/lang/String;

    .line 2308126
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->r:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2308127
    invoke-virtual {p0}, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->b()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->setContentView(I)V

    .line 2308128
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2308129
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->s:LX/0h5;

    .line 2308130
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->s:LX/0h5;

    new-instance v1, LX/FzU;

    invoke-direct {v1, p0}, LX/FzU;-><init>(Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2308131
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->q:LX/1Ck;

    const-string v1, "FETCH_MEMORIAL_CONTACT_TASK"

    new-instance v2, LX/FzV;

    invoke-direct {v2, p0}, LX/FzV;-><init>(Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;)V

    new-instance v3, LX/FzW;

    invoke-direct {v3, p0}, LX/FzW;-><init>(Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2308132
    return-void
.end method
