.class public Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;
.super Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final t:Lcom/facebook/common/callercontext/CallerContext;

.field public static final u:Ljava/lang/String;

.field public static final v:Ljava/lang/String;


# instance fields
.field public s:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2308144
    const-class v0, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;->t:Lcom/facebook/common/callercontext/CallerContext;

    .line 2308145
    const-string v0, "faceweb/f?href=/help/1568013990080948"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;->u:Ljava/lang/String;

    .line 2308146
    const-string v0, "profile/%s"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;->v:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2308147
    invoke-direct {p0}, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;-><init>()V

    .line 2308148
    return-void
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLGender;)I
    .locals 2

    .prologue
    .line 2308149
    sget-object v0, LX/FzZ;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLGender;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2308150
    const v0, 0x7f083356

    :goto_0
    return v0

    .line 2308151
    :pswitch_0
    const v0, 0x7f083357

    goto :goto_0

    .line 2308152
    :pswitch_1
    const v0, 0x7f083358

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;->s:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 2308153
    sget-object v0, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;->t:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public final a(Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2308154
    const v0, 0x7f0d1903

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2308155
    const v1, 0x7f0d1904

    invoke-virtual {p0, v1}, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2308156
    const v2, 0x7f0d1905

    invoke-virtual {p0, v2}, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 2308157
    const v3, 0x7f0d1902

    invoke-virtual {p0, v3}, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ScrollView;

    .line 2308158
    const v4, 0x7f0d0525

    invoke-virtual {p0, v4}, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    .line 2308159
    const v5, 0x7f0d1906

    invoke-virtual {p0, v5}, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 2308160
    invoke-virtual {p1}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->k()Lcom/facebook/graphql/enums/GraphQLGender;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;->a(Lcom/facebook/graphql/enums/GraphQLGender;)I

    move-result v6

    .line 2308161
    const v7, 0x7f083355

    new-array v8, v11, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->j()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {p0, v7, v8}, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2308162
    new-array v0, v11, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->j()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v0, v10

    invoke-virtual {p0, v6, v0}, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2308163
    const v0, 0x7f083359

    new-array v1, v11, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->j()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v10

    invoke-virtual {p0, v0, v1}, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2308164
    const v0, 0x7f08335a

    new-array v1, v11, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v10

    invoke-virtual {p0, v0, v1}, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2308165
    new-instance v0, LX/FzX;

    invoke-direct {v0, p0, p0}, LX/FzX;-><init>(Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;Landroid/content/Context;)V

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2308166
    new-instance v0, LX/FzY;

    invoke-direct {v0, p0, p0}, LX/FzY;-><init>(Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;Landroid/content/Context;)V

    invoke-virtual {v5, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2308167
    invoke-virtual {v3, v10}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2308168
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2308169
    const v0, 0x7f0309da

    return v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2308170
    invoke-super {p0, p1}, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->b(Landroid/os/Bundle;)V

    .line 2308171
    invoke-static {p0, p0}, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2308172
    return-void
.end method
