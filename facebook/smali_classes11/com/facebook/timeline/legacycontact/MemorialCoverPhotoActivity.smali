.class public Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;
.super Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# static fields
.field public static final v:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public s:LX/G0C;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/Fzf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/Fze;

.field public x:LX/G0B;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2308471
    const-class v0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->v:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2308469
    invoke-direct {p0}, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;-><init>()V

    .line 2308470
    return-void
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLGender;)I
    .locals 2

    .prologue
    .line 2308453
    sget-object v0, LX/G03;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLGender;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2308454
    const v0, 0x7f083366

    :goto_0
    return v0

    .line 2308455
    :pswitch_0
    const v0, 0x7f083367

    goto :goto_0

    .line 2308456
    :pswitch_1
    const v0, 0x7f083368

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;LX/G0C;Lcom/facebook/content/SecureContextHelper;LX/Fzf;)V
    .locals 0

    .prologue
    .line 2308468
    iput-object p1, p0, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->s:LX/G0C;

    iput-object p2, p0, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->t:Lcom/facebook/content/SecureContextHelper;

    iput-object p3, p0, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->u:LX/Fzf;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;

    const-class v0, LX/G0C;

    invoke-interface {v2, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/G0C;

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    const-class v3, LX/Fzf;

    invoke-interface {v2, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/Fzf;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->a(Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;LX/G0C;Lcom/facebook/content/SecureContextHelper;LX/Fzf;)V

    return-void
.end method

.method private m()LX/0Vd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Vd",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2308472
    new-instance v0, LX/G02;

    invoke-direct {v0, p0}, LX/G02;-><init>(Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 2308467
    sget-object v0, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->v:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public final a(Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2308458
    const v0, 0x7f0d1908

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2308459
    const v1, 0x7f083365

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2308460
    const v0, 0x7f0d190a

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2308461
    invoke-virtual {p1}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->k()Lcom/facebook/graphql/enums/GraphQLGender;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->a(Lcom/facebook/graphql/enums/GraphQLGender;)I

    move-result v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2308462
    const v0, 0x7f0d1907

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbScrollView;

    .line 2308463
    invoke-virtual {v0, v4}, Lcom/facebook/widget/FbScrollView;->setVisibility(I)V

    .line 2308464
    const v0, 0x7f0d1909

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 2308465
    new-instance v1, LX/G00;

    invoke-direct {v1, p0, p1, p0}, LX/G00;-><init>(Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2308466
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2308457
    const v0, 0x7f0309db

    return v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2308447
    invoke-super {p0, p1}, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->b(Landroid/os/Bundle;)V

    .line 2308448
    invoke-static {p0, p0}, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2308449
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->u:LX/Fzf;

    iget-object v1, p0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->q:LX/1Ck;

    invoke-virtual {v0, v1}, LX/Fzf;->a(LX/1Ck;)LX/Fze;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->w:LX/Fze;

    .line 2308450
    iget-object v1, p0, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->s:LX/G0C;

    const v0, 0x7f0d2d7b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    sget-object v2, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->v:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, p0, v0, v2}, LX/G0C;->a(Landroid/content/Context;Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;Lcom/facebook/common/callercontext/CallerContext;)LX/G0B;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->x:LX/G0B;

    .line 2308451
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->q:LX/1Ck;

    const-string v1, "FETCH_MEMORIAL_COVER_PHOTO_TASK"

    new-instance v2, LX/Fzz;

    invoke-direct {v2, p0}, LX/Fzz;-><init>(Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;)V

    iget-object v3, p0, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->x:LX/G0B;

    invoke-virtual {v3}, LX/G0B;->a()LX/0Vd;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2308452
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2308403
    const/16 v0, 0x713

    if-ne p1, v0, :cond_3

    if-eqz p2, :cond_3

    .line 2308404
    const-string v0, "photo"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    .line 2308405
    const-string v0, "extra_media_items"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    .line 2308406
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2308407
    if-eq v3, v4, :cond_1

    move v0, v1

    :goto_1
    const-string v5, "Attempt to change memorial cover photo with isPreviousPhoto: %b, isNewPhoto: %b"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v6, v2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v6, v1

    invoke-static {v0, v5, v6}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2308408
    iget-object v0, p0, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->x:LX/G0B;

    const v1, 0x7f08336b

    invoke-virtual {v0, v1}, LX/G0B;->a(I)V

    .line 2308409
    if-eqz v4, :cond_2

    .line 2308410
    invoke-static {p3}, LX/G0B;->a(Landroid/content/Intent;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 2308411
    iget-object v1, p0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->q:LX/1Ck;

    const-string v2, "UPDATE_MEMORIAL_COVER_PHOTO_TASK"

    iget-object v3, p0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->p:LX/Fzo;

    iget-object v4, p0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->r:Ljava/lang/String;

    new-instance v5, Ljava/io/File;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 2308412
    iget-object v6, v0, Lcom/facebook/ipc/media/data/MediaData;->mMimeType:Lcom/facebook/ipc/media/data/MimeType;

    move-object v0, v6

    .line 2308413
    invoke-virtual {v0}, Lcom/facebook/ipc/media/data/MimeType;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2308414
    new-instance v6, LX/4ct;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v6, v5, v0, p1}, LX/4ct;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 2308415
    iget-object p1, v3, LX/Fzo;->b:LX/Fzy;

    .line 2308416
    new-instance p2, LX/4H1;

    invoke-direct {p2}, LX/4H1;-><init>()V

    .line 2308417
    const-string p3, "user_id"

    invoke-virtual {p2, p3, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2308418
    move-object p2, p2

    .line 2308419
    sget-object p3, LX/Fzy;->a:LX/4IT;

    .line 2308420
    const-string v5, "focus"

    invoke-virtual {p2, v5, p3}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2308421
    move-object p2, p2

    .line 2308422
    new-instance p3, LX/G0L;

    invoke-direct {p3}, LX/G0L;-><init>()V

    move-object p3, p3

    .line 2308423
    const-string v5, "input"

    invoke-virtual {p3, v5, p2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2308424
    new-instance p2, LX/4cQ;

    const-string v5, "photo"

    invoke-direct {p2, v5, v6}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    .line 2308425
    new-instance v5, LX/399;

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p2

    .line 2308426
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2308427
    invoke-direct {v5, p3, p2, v0}, LX/399;-><init>(LX/0zP;LX/0Px;LX/0Rf;)V

    invoke-static {p1, v5}, LX/Fzy;->a(LX/Fzy;LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p2

    new-instance p3, LX/Fzx;

    invoke-direct {p3, p1}, LX/Fzx;-><init>(LX/Fzy;)V

    iget-object v5, p1, LX/Fzy;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {p2, p3, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p2

    move-object v6, p2

    .line 2308428
    new-instance p1, LX/Fzn;

    invoke-direct {p1, v3}, LX/Fzn;-><init>(LX/Fzo;)V

    iget-object p2, v3, LX/Fzo;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v6, p1, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v0, v6

    .line 2308429
    invoke-direct {p0}, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->m()LX/0Vd;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2308430
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 2308431
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 2308432
    goto/16 :goto_1

    .line 2308433
    :cond_2
    invoke-static {p3}, LX/G0B;->b(Landroid/content/Intent;)Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    .line 2308434
    iget-object v1, p0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->q:LX/1Ck;

    const-string v2, "UPDATE_MEMORIAL_COVER_PHOTO_TASK"

    iget-object v3, p0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->p:LX/Fzo;

    iget-object v4, p0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->r:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v0

    .line 2308435
    iget-object v5, v3, LX/Fzo;->b:LX/Fzy;

    .line 2308436
    new-instance v6, LX/4Gz;

    invoke-direct {v6}, LX/4Gz;-><init>()V

    .line 2308437
    const-string p1, "user_id"

    invoke-virtual {v6, p1, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2308438
    move-object v6, v6

    .line 2308439
    const-string p1, "photo_id"

    invoke-virtual {v6, p1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2308440
    move-object v6, v6

    .line 2308441
    new-instance p1, LX/G0J;

    invoke-direct {p1}, LX/G0J;-><init>()V

    move-object p1, p1

    .line 2308442
    const-string p2, "input"

    invoke-virtual {p1, p2, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2308443
    invoke-static {p1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v6

    invoke-static {v5, v6}, LX/Fzy;->a(LX/Fzy;LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    new-instance p1, LX/Fzw;

    invoke-direct {p1, v5}, LX/Fzw;-><init>(LX/Fzy;)V

    iget-object p2, v5, LX/Fzy;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v6, p1, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v5, v6

    .line 2308444
    new-instance v6, LX/Fzm;

    invoke-direct {v6, v3}, LX/Fzm;-><init>(LX/Fzo;)V

    iget-object p1, v3, LX/Fzo;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v5, v6, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v0, v5

    .line 2308445
    invoke-direct {p0}, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->m()LX/0Vd;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_2

    .line 2308446
    :cond_3
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_2
.end method
