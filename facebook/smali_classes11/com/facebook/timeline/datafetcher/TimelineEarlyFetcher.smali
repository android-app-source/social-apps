.class public Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;
.super LX/98h;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/98h",
        "<",
        "LX/FrY;",
        "LX/FrX;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private static volatile m:Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G13;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FsN;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FsH;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fq9;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1HI;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/Fwh;

.field private final l:LX/Fx5;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2295782
    const-class v0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Ot;LX/0Or;LX/Fwh;LX/Fx5;)V
    .locals 0
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0ad;",
            ">;",
            "LX/0Or",
            "<",
            "LX/G13;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FsN;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FsH;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Fq9;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1HI;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0tX;",
            ">;",
            "LX/Fwh;",
            "LX/Fx5;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2295769
    invoke-direct {p0}, LX/98h;-><init>()V

    .line 2295770
    iput-object p1, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->b:LX/0Or;

    .line 2295771
    iput-object p2, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->c:LX/0Or;

    .line 2295772
    iput-object p3, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->d:LX/0Or;

    .line 2295773
    iput-object p4, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->e:LX/0Or;

    .line 2295774
    iput-object p5, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->f:LX/0Or;

    .line 2295775
    iput-object p6, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->g:LX/0Or;

    .line 2295776
    iput-object p7, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->h:LX/0Or;

    .line 2295777
    iput-object p8, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->i:LX/0Ot;

    .line 2295778
    iput-object p9, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->j:LX/0Or;

    .line 2295779
    iput-object p10, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->k:LX/Fwh;

    .line 2295780
    iput-object p11, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->l:LX/Fx5;

    .line 2295781
    return-void
.end method

.method private a(JLandroid/os/Bundle;)LX/98g;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Landroid/os/Bundle;",
            ")",
            "LX/98g",
            "<",
            "LX/FrY;",
            "LX/FrX;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 2295750
    iget-object v2, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->h:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fq9;

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, LX/Fq9;->b(J)I

    move-result v2

    if-ne v2, v9, :cond_0

    .line 2295751
    const/4 v2, 0x0

    .line 2295752
    :goto_0
    return-object v2

    .line 2295753
    :cond_0
    const-string v2, "timeline_context_item_type"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v6

    .line 2295754
    const-string v2, "timeline_has_unseen_section"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v11

    .line 2295755
    iget-object v2, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/G13;

    iget-object v2, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->k:LX/Fwh;

    invoke-virtual {v2}, LX/Fwh;->h()Z

    move-result v7

    iget-object v2, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->l:LX/Fx5;

    invoke-virtual {v2}, LX/Fx5;->h()Z

    move-result v8

    move-wide/from16 v4, p1

    invoke-virtual/range {v3 .. v8}, LX/G13;->a(JLX/0am;ZZ)Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;

    move-result-object v4

    .line 2295756
    invoke-static/range {p3 .. p3}, LX/5ve;->a(Landroid/os/Bundle;)LX/36O;

    move-result-object v8

    .line 2295757
    new-instance v12, LX/FrY;

    move-wide/from16 v0, p1

    invoke-direct {v12, v0, v1}, LX/FrY;-><init>(J)V

    .line 2295758
    iget-object v2, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-short v3, LX/0wf;->J:S

    invoke-interface {v2, v3, v10}, LX/0ad;->a(SZ)Z

    move-result v5

    .line 2295759
    new-instance v3, LX/0v6;

    const-string v2, "TimelineEarlyFetcherRequest"

    invoke-direct {v3, v2}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 2295760
    if-eqz v5, :cond_1

    sget-object v2, LX/0vU;->PHASED:LX/0vU;

    :goto_1
    invoke-virtual {v3, v2}, LX/0v6;->a(LX/0vU;)V

    .line 2295761
    iget-object v2, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FsN;

    sget-object v6, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, v4, v8, v6}, LX/FsN;->a(LX/0v6;Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;LX/36O;Lcom/facebook/common/callercontext/CallerContext;)LX/FsE;

    move-result-object v13

    .line 2295762
    if-eqz v5, :cond_2

    move v7, v9

    :goto_2
    move-object v2, p0

    move-wide/from16 v4, p1

    move v6, v11

    invoke-static/range {v2 .. v7}, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->a(Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;LX/0v6;JZI)LX/FsJ;

    move-result-object v4

    .line 2295763
    iget-object v2, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->j:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-virtual {v2, v3}, LX/0tX;->a(LX/0v6;)V

    .line 2295764
    iget-object v2, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FsN;

    invoke-virtual {v2, v13}, LX/FsN;->a(LX/FsE;)V

    .line 2295765
    invoke-static {p0, v8}, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->a(Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;LX/36O;)V

    .line 2295766
    new-instance v2, LX/98g;

    new-instance v3, LX/FrX;

    invoke-direct {v3, v13, v4}, LX/FrX;-><init>(LX/FsE;LX/FsJ;)V

    invoke-direct {v2, v12, v3}, LX/98g;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 2295767
    :cond_1
    sget-object v2, LX/0vU;->UNSPECIFIED:LX/0vU;

    goto :goto_1

    :cond_2
    move v7, v10

    .line 2295768
    goto :goto_2
.end method

.method private static a(Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;LX/0v6;JZI)LX/FsJ;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2295747
    iget-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/0wf;->I:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2295748
    const/4 v0, 0x0

    .line 2295749
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FsH;

    iget-object v1, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    cmp-long v1, p2, v2

    if-nez v1, :cond_1

    const/4 v4, 0x1

    :cond_1
    sget-object v7, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->a:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, p1

    move-wide v2, p2

    move v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v7}, LX/FsH;->a(LX/0v6;JZZILcom/facebook/common/callercontext/CallerContext;)LX/FsJ;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;
    .locals 15

    .prologue
    .line 2295734
    sget-object v0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->m:Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;

    if-nez v0, :cond_1

    .line 2295735
    const-class v1, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;

    monitor-enter v1

    .line 2295736
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->m:Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2295737
    if-eqz v2, :cond_0

    .line 2295738
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2295739
    new-instance v3, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;

    const/16 v4, 0x1032

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x36ac

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x3635

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x3633

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x15e7

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x103d

    invoke-static {v0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x35fc

    invoke-static {v0, v10}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0xb99

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0xafd

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static {v0}, LX/Fwh;->a(LX/0QB;)LX/Fwh;

    move-result-object v13

    check-cast v13, LX/Fwh;

    invoke-static {v0}, LX/Fx5;->a(LX/0QB;)LX/Fx5;

    move-result-object v14

    check-cast v14, LX/Fx5;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;-><init>(LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Ot;LX/0Or;LX/Fwh;LX/Fx5;)V

    .line 2295740
    move-object v0, v3

    .line 2295741
    sput-object v0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->m:Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2295742
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2295743
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2295744
    :cond_1
    sget-object v0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->m:Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;

    return-object v0

    .line 2295745
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2295746
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;LX/36O;)V
    .locals 3

    .prologue
    .line 2295729
    if-eqz p1, :cond_1

    invoke-interface {p1}, LX/36O;->cp_()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, LX/36O;->cp_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HI;

    invoke-interface {p1}, LX/36O;->cp_()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1HI;->b(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2295730
    if-nez v0, :cond_0

    .line 2295731
    :goto_1
    return-void

    .line 2295732
    :cond_0
    invoke-interface {p1}, LX/36O;->cp_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v1

    .line 2295733
    iget-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HI;

    sget-object v2, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/1HI;->d(LX/1bf;Ljava/lang/Object;)LX/1ca;

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;)LX/98g;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/Intent;",
            ")",
            "LX/98g",
            "<",
            "LX/FrY;",
            "LX/FrX;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const v3, 0x1a0026

    .line 2295718
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 2295719
    if-nez v2, :cond_0

    .line 2295720
    const/4 v0, 0x0

    .line 2295721
    :goto_0
    return-object v0

    .line 2295722
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2295723
    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2295724
    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 2295725
    :goto_1
    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->a(JLandroid/os/Bundle;)LX/98g;

    move-result-object v1

    .line 2295726
    iget-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v2, 0x2

    invoke-interface {v0, v3, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    move-object v0, v1

    .line 2295727
    goto :goto_0

    .line 2295728
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2295713
    check-cast p1, LX/FrX;

    .line 2295714
    if-eqz p1, :cond_0

    .line 2295715
    const/4 v0, 0x1

    iget-object v1, p1, LX/FrX;->a:LX/FsE;

    invoke-static {v0, v1}, LX/Fsw;->a(ZLX/FsE;)V

    .line 2295716
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2295717
    const/4 v0, 0x1

    return v0
.end method
