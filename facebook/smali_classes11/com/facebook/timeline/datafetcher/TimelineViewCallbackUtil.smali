.class public Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final a:LX/5SB;

.field public final b:LX/FqO;

.field public final c:LX/G4x;

.field public final d:LX/03V;

.field private final e:LX/0hx;

.field private final f:LX/1JD;

.field private g:LX/1ly;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5SB;LX/FqO;LX/G4x;LX/03V;LX/0hx;LX/1JD;)V
    .locals 0

    .prologue
    .line 2296406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2296407
    iput-object p1, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->a:LX/5SB;

    .line 2296408
    iput-object p2, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->b:LX/FqO;

    .line 2296409
    iput-object p3, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    .line 2296410
    iput-object p4, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->d:LX/03V;

    .line 2296411
    iput-object p5, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->e:LX/0hx;

    .line 2296412
    iput-object p6, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->f:LX/1JD;

    .line 2296413
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTimelineSection;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2296414
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2296415
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)LX/1ly;
    .locals 3

    .prologue
    .line 2296416
    iget-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->g:LX/1ly;

    if-nez v0, :cond_0

    .line 2296417
    iget-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->f:LX/1JD;

    sget-object v1, LX/1Li;->TIMELINE:LX/1Li;

    const-class v2, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1JD;->a(LX/1Li;Lcom/facebook/common/callercontext/CallerContext;)LX/1ly;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->g:LX/1ly;

    .line 2296418
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->g:LX/1ly;

    return-object v0
.end method


# virtual methods
.method public final a(LX/Fso;)V
    .locals 2

    .prologue
    .line 2296419
    iget-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    sget-object v1, LX/G5A;->LOADING:LX/G5A;

    invoke-virtual {v0, p1, v1}, LX/G4x;->a(LX/Fso;LX/G5A;)V

    .line 2296420
    iget-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->e:LX/0hx;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hx;->a(Z)V

    .line 2296421
    iget-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->b:LX/FqO;

    invoke-interface {v0, p1}, LX/FqO;->a(LX/Fso;)V

    .line 2296422
    iget-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->b:LX/FqO;

    invoke-interface {v0}, LX/FqO;->lo_()V

    .line 2296423
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;Lcom/facebook/timeline/protocol/ResultSource;)V
    .locals 13

    .prologue
    .line 2296424
    iget-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->b:LX/FqO;

    invoke-interface {v0}, LX/FqO;->K()V

    .line 2296425
    iget-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    sget-object v1, LX/G5A;->COMPLETED:LX/G5A;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/G4x;->a(LX/G5A;LX/Fsp;)V

    .line 2296426
    iget-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;->a()LX/0Px;

    move-result-object v1

    .line 2296427
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_3

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2296428
    const/4 v10, 0x0

    .line 2296429
    invoke-static {v2}, LX/G4x;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2296430
    new-instance v6, LX/G59;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v7

    invoke-virtual {v7, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v8

    invoke-virtual {v8, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v8

    sget-object v9, LX/G58;->YEAR_SECTION:LX/G58;

    iget-object v11, v0, LX/G4x;->b:LX/0So;

    const/4 v12, 0x0

    invoke-direct/range {v6 .. v12}, LX/G59;-><init>(Ljava/lang/String;Ljava/lang/String;LX/G58;ZLX/0So;LX/0qm;)V

    .line 2296431
    iget-object v7, v0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2296432
    :cond_0
    :goto_1
    invoke-static {v2}, LX/G4x;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 2296433
    iget-object v5, v0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 2296434
    const/4 v5, 0x0

    .line 2296435
    :goto_2
    move-object v5, v5

    .line 2296436
    if-eqz v5, :cond_1

    .line 2296437
    invoke-static {v5}, LX/G4x;->a(LX/G59;)V

    .line 2296438
    invoke-virtual {v5, v2}, LX/G59;->b(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2296439
    :cond_1
    iget-object v5, v0, LX/G4x;->g:LX/G4v;

    if-eqz v5, :cond_2

    .line 2296440
    iget-object v5, v0, LX/G4x;->g:LX/G4v;

    invoke-virtual {v5, v2}, LX/G4v;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2296441
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2296442
    :cond_3
    invoke-static {v0}, LX/G4x;->e(LX/G4x;)V

    .line 2296443
    iget-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    .line 2296444
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v2

    if-nez v2, :cond_c

    .line 2296445
    :cond_4
    const/4 v2, 0x0

    .line 2296446
    :goto_3
    move-object v1, v2

    .line 2296447
    iget-object v2, v0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    if-nez v1, :cond_e

    .line 2296448
    :cond_5
    :goto_4
    iget-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->b:LX/FqO;

    invoke-interface {v0}, LX/FqO;->lo_()V

    .line 2296449
    iget-object v0, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->b:LX/FqO;

    invoke-interface {v0}, LX/FqO;->L()V

    .line 2296450
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;->a()LX/0Px;

    move-result-object v0

    .line 2296451
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_5
    if-ge v2, v3, :cond_6

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2296452
    invoke-static {p0}, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->b(Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)LX/1ly;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/1ly;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2296453
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_5

    .line 2296454
    :cond_6
    return-void

    .line 2296455
    :cond_7
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 2296456
    iget-object v6, v0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ne v6, v7, :cond_a

    sget-object v6, LX/G58;->UNSEEN_SECTION:LX/G58;

    invoke-static {v0, v6}, LX/G4x;->a(LX/G4x;LX/G58;)LX/G59;

    move-result-object v6

    if-eqz v6, :cond_a

    move v6, v7

    .line 2296457
    :goto_6
    iget-object v9, v0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_8

    if-eqz v6, :cond_9

    :cond_8
    move v8, v7

    :cond_9
    move v6, v8

    .line 2296458
    if-eqz v6, :cond_0

    .line 2296459
    new-instance v6, LX/G59;

    sget-object v7, LX/G58;->RECENT_SECTION:LX/G58;

    invoke-virtual {v7}, LX/G58;->toString()Ljava/lang/String;

    move-result-object v7

    sget-object v8, LX/G58;->RECENT_SECTION:LX/G58;

    invoke-virtual {v8}, LX/G58;->toString()Ljava/lang/String;

    move-result-object v8

    sget-object v9, LX/G58;->RECENT_SECTION:LX/G58;

    iget-object v11, v0, LX/G4x;->b:LX/0So;

    iget-object v12, v0, LX/G4x;->e:LX/0qm;

    invoke-direct/range {v6 .. v12}, LX/G59;-><init>(Ljava/lang/String;Ljava/lang/String;LX/G58;ZLX/0So;LX/0qm;)V

    .line 2296460
    iget-object v7, v0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_a
    move v6, v8

    .line 2296461
    goto :goto_6

    :cond_b
    iget-object v5, v0, LX/G4x;->c:Ljava/util/List;

    iget-object v6, v0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/G59;

    goto/16 :goto_2

    .line 2296462
    :cond_c
    sget-object v2, Lcom/facebook/timeline/protocol/ResultSource;->SERVER:Lcom/facebook/timeline/protocol/ResultSource;

    if-ne p2, v2, :cond_d

    .line 2296463
    new-instance v3, LX/Fsp;

    iget-object v2, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->a:LX/5SB;

    .line 2296464
    iget-wide v7, v2, LX/5SB;->b:J

    move-wide v4, v7

    .line 2296465
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->a:LX/5SB;

    invoke-virtual {v6}, LX/5SB;->d()Z

    move-result v6

    invoke-direct {v3, v4, v5, v2, v6}, LX/Fsp;-><init>(JLjava/lang/String;Z)V

    .line 2296466
    new-instance v2, LX/G51;

    invoke-direct {v2, v3}, LX/G51;-><init>(LX/Fsp;)V

    goto/16 :goto_3

    .line 2296467
    :cond_d
    new-instance v3, LX/Fsp;

    iget-object v2, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->a:LX/5SB;

    .line 2296468
    iget-wide v7, v2, LX/5SB;->b:J

    move-wide v4, v7

    .line 2296469
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->a:LX/5SB;

    invoke-virtual {v6}, LX/5SB;->d()Z

    move-result v6

    invoke-direct {v3, v4, v5, v2, v6}, LX/Fsp;-><init>(JLjava/lang/String;Z)V

    .line 2296470
    new-instance v2, LX/G4z;

    invoke-direct {v2, v3}, LX/G4z;-><init>(LX/Fsp;)V

    goto/16 :goto_3

    .line 2296471
    :cond_e
    iget-object v2, v0, LX/G4x;->c:Ljava/util/List;

    iget-object v3, v0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/G59;

    invoke-virtual {v2, v1}, LX/G59;->b(LX/G4y;)V

    goto/16 :goto_4
.end method
