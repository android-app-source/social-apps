.class public Lcom/facebook/timeline/inforeview/InfoReviewItemView;
.super Lcom/facebook/fbui/widget/contentview/ContentView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final l:Lcom/facebook/resources/ui/FbTextView;

.field private final m:Lcom/facebook/resources/ui/FbTextView;

.field public n:LX/Fyh;

.field public o:Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

.field public p:LX/Fz2;

.field public q:LX/BPp;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2307101
    const-class v0, Lcom/facebook/timeline/inforeview/InfoReviewItemView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2307102
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/timeline/inforeview/InfoReviewItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2307103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2307104
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2307105
    const v0, 0x7f0314e3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2307106
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2307107
    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0dc2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v3, v3, v3, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b(IIII)V

    .line 2307108
    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a055c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setBorderColor(I)V

    .line 2307109
    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0dc3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 2307110
    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMaxLinesFromThumbnailSize(Z)V

    .line 2307111
    const v0, 0x7f0d2f44

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2307112
    const v0, 0x7f0d2f45

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 2307113
    const v0, 0x7f0d2f46

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 2307114
    const v0, 0x7f0d2f47

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2307115
    new-instance v1, LX/Fyi;

    invoke-direct {v1, p0}, LX/Fyi;-><init>(Lcom/facebook/timeline/inforeview/InfoReviewItemView;)V

    invoke-virtual {p0, v1}, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2307116
    new-instance v1, LX/Fyj;

    invoke-direct {v1, p0}, LX/Fyj;-><init>(Lcom/facebook/timeline/inforeview/InfoReviewItemView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2307117
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;LX/Fyh;LX/Fz2;LX/BPp;)V
    .locals 4
    .param p1    # Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # LX/Fyh;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # LX/Fz2;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # LX/BPp;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    .line 2307118
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->m()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->co_()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->c()LX/1Fb;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2307119
    :cond_0
    invoke-virtual {p0, v3}, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->setVisibility(I)V

    .line 2307120
    :cond_1
    :goto_0
    return-void

    .line 2307121
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->setVisibility(I)V

    .line 2307122
    iput-object p1, p0, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->o:Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    .line 2307123
    iput-object p2, p0, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->n:LX/Fyh;

    .line 2307124
    iput-object p3, p0, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->p:LX/Fz2;

    .line 2307125
    iput-object p4, p0, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->q:LX/BPp;

    .line 2307126
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->c()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2307127
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->m()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2307128
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->k()LX/174;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2307129
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->k()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2307130
    :cond_3
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->p:LX/Fz2;

    .line 2307131
    iget-object v1, v0, LX/Fz2;->f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    move-object v0, v1

    .line 2307132
    iget-boolean v1, v0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->i:Z

    move v0, v1

    .line 2307133
    if-nez v0, :cond_1

    .line 2307134
    const v0, 0x7f0d2f47

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2307135
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
