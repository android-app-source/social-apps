.class public Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/widget/ImageView;

.field private final c:Landroid/widget/Spinner;

.field public d:LX/0Sh;

.field public e:LX/03V;

.field private f:LX/8Sa;

.field private g:LX/0wM;

.field public h:Lcom/facebook/privacy/PrivacyOperationsClient;

.field private i:LX/3R3;

.field public j:LX/FzB;

.field private k:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field public l:LX/Fym;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2307633
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2307634
    const-class v0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2307635
    const v0, 0x7f0308ce

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2307636
    const v0, 0x7f0d16df

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->a:Landroid/view/View;

    .line 2307637
    const v0, 0x7f0d16e0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->b:Landroid/widget/ImageView;

    .line 2307638
    const v0, 0x7f0d16e1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->c:Landroid/widget/Spinner;

    .line 2307639
    new-instance v0, LX/Fz9;

    invoke-direct {v0, p0}, LX/Fz9;-><init>(Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2307640
    return-void
.end method

.method private a(LX/0Sh;LX/03V;LX/8Sa;LX/0wM;Lcom/facebook/privacy/PrivacyOperationsClient;LX/3R3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2307687
    iput-object p1, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->d:LX/0Sh;

    .line 2307688
    iput-object p2, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->e:LX/03V;

    .line 2307689
    iput-object p3, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->f:LX/8Sa;

    .line 2307690
    iput-object p4, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->g:LX/0wM;

    .line 2307691
    iput-object p5, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->h:Lcom/facebook/privacy/PrivacyOperationsClient;

    .line 2307692
    iput-object p6, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->i:LX/3R3;

    .line 2307693
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;

    invoke-static {v6}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {v6}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {v6}, LX/8Sa;->a(LX/0QB;)LX/8Sa;

    move-result-object v3

    check-cast v3, LX/8Sa;

    invoke-static {v6}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v4

    check-cast v4, LX/0wM;

    invoke-static {v6}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0QB;)Lcom/facebook/privacy/PrivacyOperationsClient;

    move-result-object v5

    check-cast v5, Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-static {v6}, LX/3R3;->b(LX/0QB;)LX/3R3;

    move-result-object v6

    check-cast v6, LX/3R3;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->a(LX/0Sh;LX/03V;LX/8Sa;LX/0wM;Lcom/facebook/privacy/PrivacyOperationsClient;LX/3R3;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;LX/8QJ;)V
    .locals 9

    .prologue
    .line 2307657
    if-nez p1, :cond_0

    .line 2307658
    :goto_0
    return-void

    .line 2307659
    :cond_0
    new-instance v0, LX/FzB;

    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->e:LX/03V;

    iget-object v5, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->f:LX/8Sa;

    iget-object v6, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->g:LX/0wM;

    iget-object v7, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->i:LX/3R3;

    invoke-direct/range {v0 .. v7}, LX/FzB;-><init>(Landroid/content/Context;Landroid/content/res/Resources;Landroid/view/LayoutInflater;LX/03V;LX/8Sa;LX/0wM;LX/3R3;)V

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->j:LX/FzB;

    .line 2307660
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->j:LX/FzB;

    invoke-virtual {v0, p1}, LX/8Sc;->a(LX/8QJ;)V

    .line 2307661
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->c:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->j:LX/FzB;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 2307662
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->c:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->j:LX/FzB;

    iget-object v2, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->k:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const/4 v4, -0x1

    .line 2307663
    invoke-static {v1, v2}, LX/FzB;->b(LX/FzB;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)I

    move-result v3

    .line 2307664
    if-ne v3, v4, :cond_2

    .line 2307665
    iget-object v3, v1, LX/FzB;->i:LX/8QJ;

    .line 2307666
    new-instance v5, LX/8QI;

    invoke-direct {v5, v3}, LX/8QI;-><init>(LX/8QJ;)V

    move-object v3, v5

    .line 2307667
    invoke-static {v2}, LX/3R3;->a(LX/1oU;)Z

    move-result v5

    invoke-static {v5}, LX/0PB;->checkArgument(Z)V

    .line 2307668
    iget-object v5, v3, LX/8QI;->a:LX/0Px;

    invoke-static {v5, v2}, LX/2cA;->a(Ljava/util/Collection;LX/1oS;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, v3, LX/8QI;->b:LX/0Px;

    invoke-static {v5, v2}, LX/2cA;->a(Ljava/util/Collection;LX/1oS;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 2307669
    const/4 v5, 0x1

    iput-boolean v5, v3, LX/8QI;->d:Z

    .line 2307670
    :goto_1
    iput-object v2, v3, LX/8QI;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2307671
    move-object v3, v3

    .line 2307672
    iget-object v5, v3, LX/8QI;->a:LX/0Px;

    .line 2307673
    iget-boolean v6, v3, LX/8QI;->d:Z

    if-eqz v6, :cond_1

    .line 2307674
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    iget-object v6, v3, LX/8QI;->a:LX/0Px;

    invoke-virtual {v5, v6}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v5

    iget-object v6, v3, LX/8QI;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v5, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v5

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 2307675
    :cond_1
    new-instance v6, LX/8QJ;

    iget-object v7, v3, LX/8QI;->b:LX/0Px;

    iget-object v8, v3, LX/8QI;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget-boolean p1, v3, LX/8QI;->d:Z

    invoke-direct {v6, v5, v7, v8, p1}, LX/8QJ;-><init>(LX/0Px;LX/0Px;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Z)V

    move-object v3, v6

    .line 2307676
    invoke-virtual {v1, v3}, LX/8Sc;->a(LX/8QJ;)V

    .line 2307677
    invoke-static {v1, v2}, LX/FzB;->b(LX/FzB;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)I

    move-result v3

    .line 2307678
    if-ne v3, v4, :cond_2

    .line 2307679
    iget-object v4, v1, LX/FzB;->f:LX/03V;

    const-string v5, "identitygrowth"

    const-string v6, "Still cannot find this privacy option even after inserting it. Please update inserting method accordingly."

    invoke-virtual {v4, v5, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2307680
    :cond_2
    move v1, v3

    .line 2307681
    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 2307682
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->c:Landroid/widget/Spinner;

    new-instance v1, LX/FzA;

    invoke-direct {v1, p0}, LX/FzA;-><init>(Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 2307683
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2307684
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->c:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 2307685
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->performClick()Z

    goto/16 :goto_0

    .line 2307686
    :cond_3
    const/4 v5, 0x0

    iput-boolean v5, v3, LX/8QI;->d:Z

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/Fym;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V
    .locals 5
    .param p1    # LX/Fym;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/model/GraphQLPrivacyOption;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2307647
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2307648
    iput-object p1, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->l:LX/Fym;

    .line 2307649
    invoke-interface {p1}, LX/Fym;->f()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2307650
    invoke-interface {p1}, LX/Fym;->f()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->k:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2307651
    :goto_0
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2307652
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->c:Landroid/widget/Spinner;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 2307653
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->b:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->g:LX/0wM;

    iget-object v2, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->f:LX/8Sa;

    iget-object v3, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->k:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    sget-object v4, LX/8SZ;->GLYPH:LX/8SZ;

    invoke-virtual {v2, v3, v4}, LX/8Sa;->a(Lcom/facebook/graphql/model/GraphQLImage;LX/8SZ;)I

    move-result v2

    const v3, -0x958e80

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2307654
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->b:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->k:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2307655
    return-void

    .line 2307656
    :cond_0
    iput-object p2, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->k:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    goto :goto_0
.end method

.method public getSelectedPrivacyJson()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2307644
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->l:LX/Fym;

    invoke-interface {v0}, LX/Fym;->f()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2307645
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->l:LX/Fym;

    invoke-interface {v0}, LX/Fym;->f()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v0

    .line 2307646
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->k:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSelectedPrivacyRow()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2307641
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->l:LX/Fym;

    invoke-interface {v0}, LX/Fym;->f()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2307642
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->l:LX/Fym;

    invoke-interface {v0}, LX/Fym;->f()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v0

    .line 2307643
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->k:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v0

    goto :goto_0
.end method
