.class public Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/Fz4;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public final c:LX/FzE;

.field private d:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

.field public e:LX/FzF;

.field public f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

.field public g:LX/Fyv;

.field private h:Z

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2307602
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2307603
    const-class v0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2307604
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->setOrientation(I)V

    .line 2307605
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->a:Ljava/util/ArrayList;

    .line 2307606
    new-instance v0, LX/FzE;

    invoke-direct {v0, p1}, LX/FzE;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->c:LX/FzE;

    .line 2307607
    new-instance v0, LX/FzC;

    invoke-direct {v0, p1}, LX/FzC;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2307608
    return-void
.end method

.method private a(LX/Fz4;)V
    .locals 1
    .param p1    # LX/Fz4;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2307531
    new-instance v0, LX/Fz6;

    invoke-direct {v0, p0, p1}, LX/Fz6;-><init>(Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;LX/Fz4;)V

    invoke-virtual {p1, v0}, LX/Fz4;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2307532
    invoke-virtual {p0, p1}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->addView(Landroid/view/View;)V

    .line 2307533
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2307534
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;

    invoke-static {v0}, LX/FzF;->a(LX/0QB;)LX/FzF;

    move-result-object v0

    check-cast v0, LX/FzF;

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->e:LX/FzF;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p0    # Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2307598
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2307599
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    .line 2307600
    iput-boolean p3, v0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->c:Z

    .line 2307601
    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2307574
    iget-boolean v1, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->h:Z

    if-eqz v1, :cond_0

    .line 2307575
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2307576
    iget-object v2, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->d:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;->cm_()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2307577
    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0d80

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    .line 2307578
    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0d81

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    .line 2307579
    invoke-virtual {v1, v2, v3, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 2307580
    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00e5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2307581
    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0050

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2307582
    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2307583
    invoke-virtual {p0, v1}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->addView(Landroid/view/View;)V

    .line 2307584
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->d:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;->m()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2307585
    iget-object v1, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 2307586
    iget-object v1, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->d:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;->m()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;

    .line 2307587
    new-instance v4, LX/Fz5;

    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, LX/Fz5;-><init>(Landroid/content/Context;)V

    .line 2307588
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;->a()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    move-result-object v0

    .line 2307589
    invoke-virtual {v4, v0}, LX/Fz5;->setInferenceData(Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;)V

    .line 2307590
    iget-boolean v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->i:Z

    invoke-virtual {v4, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2307591
    invoke-direct {p0, v4}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->a(LX/Fz4;)V

    .line 2307592
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2307593
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->c:LX/FzE;

    iget-object v1, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    iget-object v2, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->d:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    invoke-virtual {v0, v1, v2}, LX/FzE;->a(Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;)V

    .line 2307594
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->c:LX/FzE;

    new-instance v1, LX/Fz7;

    invoke-direct {v1, p0}, LX/Fz7;-><init>(Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;)V

    invoke-virtual {v0, v1}, LX/FzE;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2307595
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->c:LX/FzE;

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->addView(Landroid/view/View;)V

    .line 2307596
    invoke-static {p0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->e(Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;)V

    .line 2307597
    return-void
.end method

.method public static d(Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;)V
    .locals 2

    .prologue
    .line 2307609
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->g:LX/Fyv;

    iget-object v1, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    .line 2307610
    iget-object p0, v1, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->a:Ljava/lang/String;

    move-object v1, p0

    .line 2307611
    invoke-interface {v0, v1}, LX/Fyv;->a(Ljava/lang/String;)V

    .line 2307612
    return-void
.end method

.method public static e(Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2307555
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_1

    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fz4;

    .line 2307556
    iget-object v1, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    .line 2307557
    iget-boolean v5, v1, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->c:Z

    move v1, v5

    .line 2307558
    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    .line 2307559
    iget-object v5, v1, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->a:Ljava/lang/String;

    move-object v1, v5

    .line 2307560
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    .line 2307561
    iget-object v5, v1, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->a:Ljava/lang/String;

    move-object v1, v5

    .line 2307562
    iget-object v5, v0, LX/Fz4;->j:Ljava/lang/String;

    move-object v5, v5

    .line 2307563
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 2307564
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move v1, v2

    .line 2307565
    goto :goto_1

    .line 2307566
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->c:LX/FzE;

    iget-object v1, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    .line 2307567
    iget-boolean v2, v1, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->c:Z

    move v1, v2

    .line 2307568
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 2307569
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    .line 2307570
    iget-boolean v1, v0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->c:Z

    move v0, v1

    .line 2307571
    if-nez v0, :cond_2

    .line 2307572
    invoke-direct {p0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->f()V

    .line 2307573
    :cond_2
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2307550
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->a(Ljava/lang/String;)V

    .line 2307551
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    .line 2307552
    iput-object v1, v0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->e:Ljava/lang/String;

    .line 2307553
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->c:LX/FzE;

    iget-object v1, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    iget-object v2, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->d:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    invoke-virtual {v0, v1, v2}, LX/FzE;->a(Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;)V

    .line 2307554
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2307548
    const/4 v0, 0x0

    invoke-static {p0, v1, v1, v0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->a$redex0(Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2307549
    return-void
.end method

.method public final a(Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;ZZ)V
    .locals 1

    .prologue
    .line 2307539
    iput-object p1, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->d:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    .line 2307540
    iput-object p2, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    .line 2307541
    iput-boolean p3, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->h:Z

    .line 2307542
    iput-boolean p4, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->i:Z

    .line 2307543
    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->removeAllViews()V

    .line 2307544
    goto :goto_0

    .line 2307545
    :goto_0
    invoke-direct {p0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->b()V

    .line 2307546
    return-void
    .line 2307547
.end method

.method public setOnSelectionChangedListener(LX/Fyv;)V
    .locals 0

    .prologue
    .line 2307537
    iput-object p1, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->g:LX/Fyv;

    .line 2307538
    return-void
.end method

.method public setOnShowMoreClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2307535
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2307536
    return-void
.end method
