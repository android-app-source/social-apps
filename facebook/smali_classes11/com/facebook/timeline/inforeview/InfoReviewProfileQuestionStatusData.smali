.class public Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/Fym;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field private f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field public g:Z

.field public h:Z

.field public i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2307186
    new-instance v0, LX/Fyl;

    invoke-direct {v0}, LX/Fyl;-><init>()V

    sput-object v0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2307185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLPrivacyOption;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2307183
    iput-object p1, p0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2307184
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2307177
    iput-object p1, p0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->d:Ljava/lang/String;

    .line 2307178
    if-eqz p1, :cond_0

    .line 2307179
    const-string v0, "page"

    invoke-virtual {p0, p1, v0}, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2307180
    const/4 v0, 0x1

    .line 2307181
    iput-boolean v0, p0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->c:Z

    .line 2307182
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2307174
    iput-object p1, p0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->a:Ljava/lang/String;

    .line 2307175
    iput-object p2, p0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->b:Ljava/lang/String;

    .line 2307176
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2307173
    const/4 v0, 0x0

    return v0
.end method

.method public final f()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2307164
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2307165
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2307166
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2307167
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2307168
    iget-boolean v0, p0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2307169
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2307170
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2307171
    return-void

    .line 2307172
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
