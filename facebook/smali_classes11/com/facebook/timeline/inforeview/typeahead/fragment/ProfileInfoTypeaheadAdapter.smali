.class public Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLInterfaces$TypeaheadResultPage$;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z

.field private d:Landroid/view/LayoutInflater;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2307958
    const-class v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;

    const-string v1, "growth"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/LayoutInflater;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2307953
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2307954
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;->b:Ljava/util/List;

    .line 2307955
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;->c:Z

    .line 2307956
    iput-object p1, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;->d:Landroid/view/LayoutInflater;

    .line 2307957
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2307948
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2307949
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2307950
    invoke-virtual {p0, v0}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;->a(I)Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2307951
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2307952
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;
    .locals 1

    .prologue
    .line 2307947
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2307946
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2307945
    invoke-virtual {p0, p1}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;->a(I)Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2307914
    invoke-virtual {p0, p1}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;->a(I)Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2307916
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gt p1, v0, :cond_7

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2307917
    if-nez p2, :cond_0

    .line 2307918
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;->d:Landroid/view/LayoutInflater;

    const v3, 0x7f031059

    invoke-virtual {v0, v3, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 2307919
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;->a(I)Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;

    move-result-object v3

    .line 2307920
    if-eqz v3, :cond_6

    .line 2307921
    const v0, 0x7f0d272c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2307922
    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->ck_()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-nez v4, :cond_8

    :cond_1
    :goto_1
    if-eqz v1, :cond_9

    const/4 v1, 0x0

    .line 2307923
    :goto_2
    sget-object v2, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2307924
    const v0, 0x7f0d272d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2307925
    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2307926
    const v0, 0x7f0d272e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2307927
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 2307928
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 2307929
    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->b()LX/0Px;

    move-result-object p1

    .line 2307930
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2307931
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2307932
    :cond_2
    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->e()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_b

    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->e()LX/1vs;

    move-result-object v1

    iget-object p1, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {p1, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    move v1, v2

    :goto_3
    if-eqz v1, :cond_3

    .line 2307933
    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->e()LX/1vs;

    move-result-object v1

    iget-object p1, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {p1, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2307934
    :cond_3
    iget-boolean v1, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;->c:Z

    if-eqz v1, :cond_5

    .line 2307935
    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->e()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_4

    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->e()LX/1vs;

    move-result-object v1

    iget-object p1, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {p1, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    move v4, v2

    :cond_4
    if-eqz v4, :cond_5

    .line 2307936
    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->e()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v4, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2307937
    :cond_5
    const-string v1, " \u00b7 "

    invoke-interface {v5}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2307938
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2307939
    :cond_6
    return-object p2

    :cond_7
    move v0, v2

    .line 2307940
    goto/16 :goto_0

    .line 2307941
    :cond_8
    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->ck_()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    invoke-virtual {v5, v4, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    move v1, v2

    goto/16 :goto_1

    .line 2307942
    :cond_9
    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->ck_()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2307943
    invoke-virtual {v4, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto/16 :goto_2

    :cond_a
    move v1, v4

    .line 2307944
    goto :goto_3

    :cond_b
    move v1, v4

    goto :goto_3
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 2307915
    const/4 v0, 0x1

    return v0
.end method
