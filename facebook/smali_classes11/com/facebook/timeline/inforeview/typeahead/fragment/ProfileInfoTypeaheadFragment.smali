.class public Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private a:Landroid/widget/ListView;

.field private b:Landroid/view/View;

.field public c:Landroid/widget/EditText;

.field public d:LX/03V;

.field public e:LX/FzK;

.field public f:Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;

.field public g:LX/FzM;

.field public h:LX/FzP;

.field public i:LX/0kL;

.field public j:LX/0Zb;

.field private final k:Landroid/os/Handler;

.field private l:Ljava/lang/Runnable;

.field public m:Ljava/lang/String;

.field private n:Z

.field public o:Ljava/lang/String;

.field public p:I

.field private q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field public volatile r:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2308092
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2308093
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->k:Landroid/os/Handler;

    return-void
.end method

.method public static a(Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 2308087
    const-string v0, "high_school"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "college"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "work_history"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "current_city"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "hometown"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2308088
    if-nez v0, :cond_1

    .line 2308089
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->d:LX/03V;

    const-string v1, "identitygrowth"

    const-string v2, "Failed to fetch typeahead results because of invalid profile section"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2308090
    const/4 v0, 0x0

    .line 2308091
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;Ljava/util/List;)V
    .locals 3
    .param p0    # Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLInterfaces$TypeaheadResultPage$;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2308078
    if-nez p1, :cond_0

    .line 2308079
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object p1

    .line 2308080
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->f:Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;

    iget-object v1, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->m:Ljava/lang/String;

    .line 2308081
    const-string v2, "current_city"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "hometown"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 2308082
    iput-boolean v1, v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;->c:Z

    .line 2308083
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->f:Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;

    .line 2308084
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;->b:Ljava/util/List;

    .line 2308085
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->f:Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;

    const v1, 0x5560fe5

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2308086
    return-void

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;)V
    .locals 13

    .prologue
    .line 2308059
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->m:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->a(Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2308060
    :goto_0
    return-void

    .line 2308061
    :cond_0
    const-string v0, "current_city"

    iget-object v1, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->n:Z

    if-eqz v0, :cond_1

    .line 2308062
    iget-object v2, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->e:LX/FzK;

    const/4 v3, 0x5

    new-instance v4, LX/FzR;

    invoke-direct {v4, p0}, LX/FzR;-><init>(Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;)V

    .line 2308063
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2308064
    iget-boolean v5, v2, LX/FzK;->c:Z

    if-eqz v5, :cond_2

    .line 2308065
    :goto_1
    goto :goto_0

    .line 2308066
    :cond_1
    invoke-static {p0}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->e(Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;)V

    goto :goto_0

    .line 2308067
    :cond_2
    const/4 v5, 0x1

    iput-boolean v5, v2, LX/FzK;->c:Z

    .line 2308068
    iget-object v5, v2, LX/FzK;->a:LX/0y2;

    invoke-virtual {v5}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v5

    .line 2308069
    if-nez v5, :cond_3

    .line 2308070
    const/4 v5, 0x0

    iput-boolean v5, v2, LX/FzK;->c:Z

    .line 2308071
    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2308072
    :cond_3
    new-instance v6, LX/FzG;

    invoke-direct {v6, v2, v4}, LX/FzG;-><init>(LX/FzK;LX/0TF;)V

    .line 2308073
    new-instance v7, LX/5xu;

    invoke-direct {v7}, LX/5xu;-><init>()V

    move-object v7, v7

    .line 2308074
    const-string v8, "user_latitude"

    invoke-virtual {v5}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v8

    const-string v9, "user_longitude"

    invoke-virtual {v5}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v8

    const-string v9, "results_num"

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2308075
    new-instance v8, LX/FzI;

    invoke-direct {v8, v2}, LX/FzI;-><init>(LX/FzK;)V

    .line 2308076
    invoke-virtual {v2, v7, v6, v8}, LX/FzJ;->a(LX/0gW;LX/0TF;LX/FzH;)V

    .line 2308077
    goto :goto_1
.end method

.method public static e(Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;)V
    .locals 7

    .prologue
    .line 2308047
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->q:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2308048
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->a$redex0(Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;Ljava/util/List;)V

    .line 2308049
    :goto_0
    return-void

    .line 2308050
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->h:LX/FzP;

    invoke-virtual {v0}, LX/FzJ;->a()V

    .line 2308051
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->r:Z

    .line 2308052
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->g:LX/FzM;

    iget-object v1, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->m:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->q:Ljava/util/ArrayList;

    new-instance v3, LX/FzS;

    invoke-direct {v3, p0}, LX/FzS;-><init>(Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;)V

    .line 2308053
    invoke-static {v0, v2}, LX/FzM;->a(LX/FzM;Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 2308054
    new-instance v5, LX/5xv;

    invoke-direct {v5}, LX/5xv;-><init>()V

    move-object v5, v5

    .line 2308055
    const-string v6, "profile_section"

    invoke-virtual {v5, v6, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string p0, "existing_profile_inputs"

    invoke-virtual {v6, p0, v4}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v4

    const-string v6, "results_num"

    const/16 p0, 0xa

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, v6, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2308056
    new-instance v4, LX/FzL;

    invoke-direct {v4, v0}, LX/FzL;-><init>(LX/FzM;)V

    .line 2308057
    invoke-virtual {v0, v5, v3, v4}, LX/FzJ;->a(LX/0gW;LX/0TF;LX/FzH;)V

    .line 2308058
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2308044
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2308045
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    new-instance p1, LX/FzK;

    invoke-static {v0}, LX/9kE;->b(LX/0QB;)LX/9kE;

    move-result-object v4

    check-cast v4, LX/9kE;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object v6

    check-cast v6, LX/0y2;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v7

    check-cast v7, LX/0kL;

    invoke-direct {p1, v4, v5, v6, v7}, LX/FzK;-><init>(LX/9kE;LX/0tX;LX/0y2;LX/0kL;)V

    move-object v4, p1

    check-cast v4, LX/FzK;

    new-instance v6, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;

    invoke-static {v0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    invoke-direct {v6, v5}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;-><init>(Landroid/view/LayoutInflater;)V

    move-object v5, v6

    check-cast v5, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;

    new-instance v1, LX/FzM;

    invoke-static {v0}, LX/9kE;->b(LX/0QB;)LX/9kE;

    move-result-object v6

    check-cast v6, LX/9kE;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p1

    check-cast p1, LX/03V;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object p0

    check-cast p0, LX/0lB;

    invoke-direct {v1, v6, v7, p1, p0}, LX/FzM;-><init>(LX/9kE;LX/0tX;LX/03V;LX/0lB;)V

    move-object v6, v1

    check-cast v6, LX/FzM;

    new-instance p0, LX/FzP;

    invoke-static {v0}, LX/9kE;->b(LX/0QB;)LX/9kE;

    move-result-object v7

    check-cast v7, LX/9kE;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object p1

    check-cast p1, LX/0tX;

    invoke-direct {p0, v7, p1}, LX/FzP;-><init>(LX/9kE;LX/0tX;)V

    move-object v7, p0

    check-cast v7, LX/FzP;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object p1

    check-cast p1, LX/0kL;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    iput-object v3, v2, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->d:LX/03V;

    iput-object v4, v2, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->e:LX/FzK;

    iput-object v5, v2, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->f:Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;

    iput-object v6, v2, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->g:LX/FzM;

    iput-object v7, v2, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->h:LX/FzP;

    iput-object p1, v2, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->i:LX/0kL;

    const/4 p0, 0x0

    iput-boolean p0, v2, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->r:Z

    iput-object v0, v2, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->j:LX/0Zb;

    .line 2308046
    return-void
.end method

.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    .line 2307997
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->l:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2307998
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->k:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->l:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2307999
    :cond_0
    new-instance v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment$1;

    invoke-direct {v0, p0}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment$1;-><init>(Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;)V

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->l:Ljava/lang/Runnable;

    .line 2308000
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->k:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->l:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    const v4, 0x2c38853e

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2308001
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2308043
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x469a3ef

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2308042
    const v1, 0x7f03105a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x2d2c6cd9

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2308030
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->f:Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;

    invoke-virtual {v0, p3}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;->a(I)Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;

    move-result-object v0

    .line 2308031
    new-instance v1, LX/8yQ;

    const-string v2, "profile_question_object_selected"

    const-string v3, "profile_experience_picker"

    invoke-direct {v1, v2, v3}, LX/8yQ;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2308032
    iget-object v2, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/8yQ;->h(Ljava/lang/String;)LX/8yQ;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->f:Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;

    invoke-virtual {v2}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/8yQ;->a(LX/0Px;)LX/8yQ;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/8yQ;->d(Ljava/lang/String;)LX/8yQ;

    move-result-object v1

    iget v2, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->p:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/8yQ;->e(Ljava/lang/String;)LX/8yQ;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/8yQ;->b(Ljava/lang/String;)LX/8yQ;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/8yQ;->c(I)LX/8yQ;

    move-result-object v1

    .line 2308033
    iget-object v2, v1, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v1, v2

    .line 2308034
    iget-object v2, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->j:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2308035
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2308036
    const-string v2, "typeahead_selected_page_name"

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2308037
    const-string v2, "typeahead_selected_page_id"

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2308038
    const-string v0, "profile_section"

    iget-object v2, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->m:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2308039
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2308040
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2308041
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x226cc3e9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2308027
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2308028
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2308029
    const/16 v1, 0x2b

    const v2, 0x61f6be62

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7960729b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2308021
    iget-object v1, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->k:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->l:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2308022
    iget-object v1, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->g:LX/FzM;

    invoke-virtual {v1}, LX/FzJ;->a()V

    .line 2308023
    iget-object v1, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->h:LX/FzP;

    invoke-virtual {v1}, LX/FzJ;->a()V

    .line 2308024
    iget-object v1, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->e:LX/FzK;

    invoke-virtual {v1}, LX/FzJ;->a()V

    .line 2308025
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2308026
    const/16 v1, 0x2b

    const v2, -0x8368848

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2308020
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2308002
    const v0, 0x7f0d272f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->b:Landroid/view/View;

    .line 2308003
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->b:Landroid/view/View;

    const v1, 0x7f0d2730

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->c:Landroid/widget/EditText;

    .line 2308004
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->c:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2308005
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2308006
    const v0, 0x102000a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->a:Landroid/widget/ListView;

    .line 2308007
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2308008
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->f:Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2308009
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2308010
    const-string v1, "profile_section"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->m:Ljava/lang/String;

    .line 2308011
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2308012
    const-string v1, "typeahead_existing_fields"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->q:Ljava/util/ArrayList;

    .line 2308013
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2308014
    const-string v1, "typeahead_use_current_city_inference"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->n:Z

    .line 2308015
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2308016
    const-string v1, "logging_surfaces"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->o:Ljava/lang/String;

    .line 2308017
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->p:I

    .line 2308018
    invoke-static {p0}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->c(Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;)V

    .line 2308019
    return-void
.end method
