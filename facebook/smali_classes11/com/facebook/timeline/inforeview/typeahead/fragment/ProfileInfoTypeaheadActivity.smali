.class public Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2307913
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;ZLjava/lang/String;)Landroid/content/Intent;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;Z",
            "Ljava/lang/String;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 2307874
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2307875
    const-string v1, "typeahead_title_bar_text"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2307876
    const-string v1, "profile_section"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2307877
    const-string v1, "typeahead_existing_fields"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2307878
    const-string v1, "typeahead_use_current_city_inference"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2307879
    const-string v1, "logging_surfaces"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2307880
    return-object v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2307881
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2307882
    const v0, 0x7f03105b

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadActivity;->setContentView(I)V

    .line 2307883
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2307884
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2307885
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0h5;->setHasFbLogo(Z)V

    .line 2307886
    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "typeahead_title_bar_text"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2307887
    if-eqz v1, :cond_0

    .line 2307888
    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2307889
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;

    .line 2307890
    if-nez v0, :cond_1

    .line 2307891
    new-instance v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;

    invoke-direct {v0}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;-><init>()V

    .line 2307892
    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "profile_section"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2307893
    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "typeahead_existing_fields"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 2307894
    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "typeahead_use_current_city_inference"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 2307895
    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "logging_surfaces"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2307896
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2307897
    const-string p1, "profile_section"

    invoke-virtual {v5, p1, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2307898
    const-string p1, "typeahead_existing_fields"

    invoke-virtual {v5, p1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2307899
    const-string p1, "typeahead_use_current_city_inference"

    invoke-virtual {v5, p1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2307900
    const-string p1, "logging_surfaces"

    invoke-virtual {v5, p1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2307901
    move-object v1, v5

    .line 2307902
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2307903
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2307904
    :cond_1
    return-void
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    .line 2307905
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2307906
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;

    .line 2307907
    if-eqz v0, :cond_0

    .line 2307908
    new-instance v1, LX/8yQ;

    const-string v2, "profile_question_tapped_cancel"

    const-string p0, "profile_experience_picker"

    invoke-direct {v1, v2, p0}, LX/8yQ;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2307909
    iget-object v2, v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/8yQ;->h(Ljava/lang/String;)LX/8yQ;

    move-result-object v1

    iget-object v2, v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->f:Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;

    invoke-virtual {v2}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadAdapter;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/8yQ;->a(LX/0Px;)LX/8yQ;

    move-result-object v1

    iget-object v2, v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/8yQ;->d(Ljava/lang/String;)LX/8yQ;

    move-result-object v1

    iget v2, v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->p:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/8yQ;->e(Ljava/lang/String;)LX/8yQ;

    move-result-object v1

    .line 2307910
    iget-object v2, v1, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v1, v2

    .line 2307911
    iget-object v2, v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->j:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2307912
    :cond_0
    return-void
.end method
