.class public final Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 2307959
    iput-object p1, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment$1;->a:Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 2307960
    iget-object v0, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment$1;->a:Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;

    iget-object v1, p0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment$1;->a:Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2307961
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2307962
    iget-boolean v2, v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->r:Z

    if-nez v2, :cond_0

    .line 2307963
    invoke-static {v0}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->c(Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;)V

    .line 2307964
    :cond_0
    :goto_0
    return-void

    .line 2307965
    :cond_1
    iget-object v2, v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->m:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->a(Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2307966
    iget-object v2, v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->h:LX/FzP;

    invoke-virtual {v2}, LX/FzJ;->a()V

    .line 2307967
    iget-object v2, v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->h:LX/FzP;

    iget-object v3, v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->m:Ljava/lang/String;

    iget v4, v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->p:I

    new-instance v5, LX/FzT;

    invoke-direct {v5, v0}, LX/FzT;-><init>(Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;)V

    .line 2307968
    new-instance v6, LX/5y4;

    invoke-direct {v6}, LX/5y4;-><init>()V

    move-object v6, v6

    .line 2307969
    const-string v7, "search_query"

    invoke-virtual {v6, v7, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string p0, "profile_section"

    invoke-virtual {v7, p0, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string p0, "typeahead_session_id"

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, p0, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string p0, "results_num"

    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, p0, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2307970
    new-instance v7, LX/FzO;

    invoke-direct {v7, v2}, LX/FzO;-><init>(LX/FzP;)V

    .line 2307971
    iget-object p0, v2, LX/FzP;->a:LX/0zT;

    invoke-virtual {v2, v6, v5, v7, p0}, LX/FzJ;->a(LX/0gW;LX/0TF;LX/FzH;LX/0zT;)V

    .line 2307972
    goto :goto_0
.end method
