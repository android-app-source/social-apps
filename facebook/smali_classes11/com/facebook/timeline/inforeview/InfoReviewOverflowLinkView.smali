.class public Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;
.super Lcom/facebook/fbui/widget/contentview/ContentView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public l:LX/Fyh;

.field public m:Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2307150
    const-class v0, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2307140
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;)V

    .line 2307141
    const v0, 0x7f0314e4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2307142
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2307143
    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0dc2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v3, v3, v3, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b(IIII)V

    .line 2307144
    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a055c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setBorderColor(I)V

    .line 2307145
    const v0, 0x7f0e0687

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 2307146
    invoke-virtual {p0}, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0dc3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 2307147
    const v0, 0x7f0d2f48

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2307148
    new-instance v0, LX/Fyk;

    invoke-direct {v0, p0}, LX/Fyk;-><init>(Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2307149
    return-void
.end method
