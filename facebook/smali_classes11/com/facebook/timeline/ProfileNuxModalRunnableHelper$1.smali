.class public final Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/app/Activity;

.field public final synthetic b:LX/BQ1;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/Fq4;


# direct methods
.method public constructor <init>(LX/Fq4;Landroid/app/Activity;LX/BQ1;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2292268
    iput-object p1, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->d:LX/Fq4;

    iput-object p2, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->a:Landroid/app/Activity;

    iput-object p3, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->b:LX/BQ1;

    iput-object p4, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2292269
    new-instance v4, Landroid/content/Intent;

    iget-object v0, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->a:Landroid/app/Activity;

    const-class v5, Lcom/facebook/timeline/profilenux/ProfileNuxModalActivity;

    invoke-direct {v4, v0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2292270
    new-instance v0, LX/G3o;

    invoke-direct {v0}, LX/G3o;-><init>()V

    iget-object v5, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->b:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v5

    .line 2292271
    iput-object v5, v0, LX/G3o;->a:Ljava/lang/String;

    .line 2292272
    move-object v0, v0

    .line 2292273
    iget-object v5, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->c:Ljava/lang/String;

    .line 2292274
    iput-object v5, v0, LX/G3o;->b:Ljava/lang/String;

    .line 2292275
    move-object v0, v0

    .line 2292276
    iget-object v5, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->b:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->N()Ljava/lang/String;

    move-result-object v5

    .line 2292277
    iput-object v5, v0, LX/G3o;->c:Ljava/lang/String;

    .line 2292278
    move-object v0, v0

    .line 2292279
    iget-object v5, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->b:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->V()Ljava/lang/String;

    move-result-object v5

    .line 2292280
    iput-object v5, v0, LX/G3o;->d:Ljava/lang/String;

    .line 2292281
    move-object v0, v0

    .line 2292282
    iput-boolean v2, v0, LX/G3o;->i:Z

    .line 2292283
    move-object v0, v0

    .line 2292284
    iget-object v5, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->b:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->Y()LX/2rX;

    move-result-object v5

    .line 2292285
    iput-object v5, v0, LX/G3o;->j:LX/2rX;

    .line 2292286
    move-object v5, v0

    .line 2292287
    iget-object v0, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->b:LX/BQ1;

    .line 2292288
    iget-object v6, v0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v0, v6

    .line 2292289
    if-eqz v0, :cond_0

    .line 2292290
    iget-object v0, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->b:LX/BQ1;

    .line 2292291
    iget-object v6, v0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v0, v6

    .line 2292292
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->e()Z

    move-result v0

    .line 2292293
    iput-boolean v0, v5, LX/G3o;->e:Z

    .line 2292294
    move-object v0, v5

    .line 2292295
    iget-object v6, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->b:LX/BQ1;

    .line 2292296
    iget-object v7, v6, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v6, v7

    .line 2292297
    invoke-virtual {v6}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->m()Z

    move-result v6

    .line 2292298
    iput-boolean v6, v0, LX/G3o;->f:Z

    .line 2292299
    move-object v0, v0

    .line 2292300
    iget-object v6, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->b:LX/BQ1;

    .line 2292301
    iget-object v7, v6, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v6, v7

    .line 2292302
    invoke-virtual {v6}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->ci_()Z

    move-result v6

    .line 2292303
    iput-boolean v6, v0, LX/G3o;->g:Z

    .line 2292304
    move-object v0, v0

    .line 2292305
    iget-object v6, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->b:LX/BQ1;

    .line 2292306
    iget-object v7, v6, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v6, v7

    .line 2292307
    invoke-virtual {v6}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->n()Z

    move-result v6

    .line 2292308
    iput-boolean v6, v0, LX/G3o;->h:Z

    .line 2292309
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->b:LX/BQ1;

    .line 2292310
    iget-object v6, v0, LX/BQ1;->l:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;

    move-object v0, v6

    .line 2292311
    if-eqz v0, :cond_3

    .line 2292312
    iget-object v0, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->b:LX/BQ1;

    .line 2292313
    iget-object v6, v0, LX/BQ1;->l:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;

    move-object v0, v6

    .line 2292314
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->b()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2292315
    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    .line 2292316
    iget-object v0, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->b:LX/BQ1;

    .line 2292317
    iget-object v6, v0, LX/BQ1;->l:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;

    move-object v0, v6

    .line 2292318
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->b()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2292319
    invoke-virtual {v6, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2292320
    :goto_1
    iget-object v6, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->b:LX/BQ1;

    .line 2292321
    iget-object v7, v6, LX/BQ1;->l:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;

    move-object v6, v7

    .line 2292322
    if-eqz v6, :cond_6

    .line 2292323
    iget-object v6, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->b:LX/BQ1;

    .line 2292324
    iget-object v7, v6, LX/BQ1;->l:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;

    move-object v6, v7

    .line 2292325
    invoke-virtual {v6}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->a()LX/1vs;

    move-result-object v6

    iget v6, v6, LX/1vs;->b:I

    .line 2292326
    if-eqz v6, :cond_5

    :goto_2
    if-eqz v1, :cond_1

    .line 2292327
    iget-object v1, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->b:LX/BQ1;

    .line 2292328
    iget-object v3, v1, LX/BQ1;->l:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;

    move-object v1, v3

    .line 2292329
    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->a()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2292330
    invoke-virtual {v3, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 2292331
    :cond_1
    const-string v1, "nux_modal_title"

    invoke-virtual {v4, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2292332
    const-string v0, "nux_modal_text"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2292333
    const-string v0, "refresher_configuration"

    invoke-virtual {v5}, LX/G3o;->a()Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2292334
    const-string v0, "profile_id"

    iget-object v1, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->c:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2292335
    iget-object v0, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->d:LX/Fq4;

    iget-object v0, v0, LX/Fq4;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;->a:Landroid/app/Activity;

    invoke-interface {v0, v4, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2292336
    return-void

    :cond_2
    move v0, v2

    .line 2292337
    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move-object v0, v3

    goto :goto_1

    :cond_5
    move v1, v2

    .line 2292338
    goto :goto_2

    :cond_6
    move v1, v2

    goto :goto_2
.end method
