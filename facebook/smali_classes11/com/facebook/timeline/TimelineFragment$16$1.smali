.class public final Lcom/facebook/timeline/TimelineFragment$16$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/8Kd;

.field public final synthetic b:LX/FqG;


# direct methods
.method public constructor <init>(LX/FqG;LX/8Kd;)V
    .locals 0

    .prologue
    .line 2292612
    iput-object p1, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->b:LX/FqG;

    iput-object p2, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->a:LX/8Kd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 2292613
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->a:LX/8Kd;

    .line 2292614
    iget-object v1, v0, LX/0b5;->b:LX/8KZ;

    move-object v0, v1

    .line 2292615
    sget-object v1, LX/8KZ;->UPLOAD_SUCCESS:LX/8KZ;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->b:LX/FqG;

    iget-object v0, v0, LX/FqG;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    if-eqz v0, :cond_2

    .line 2292616
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->b:LX/FqG;

    iget-object v0, v0, LX/FqG;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    invoke-virtual {v0}, LX/FrV;->c()V

    .line 2292617
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->a:LX/8Kd;

    .line 2292618
    iget-object v1, v0, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v1

    .line 2292619
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->a:LX/8Kd;

    .line 2292620
    iget-object v1, v0, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v1

    .line 2292621
    iget-object v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->x:Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    move-object v0, v1

    .line 2292622
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->a:LX/8Kd;

    .line 2292623
    iget-object v1, v0, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v1

    .line 2292624
    iget-object v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->x:Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    move-object v0, v1

    .line 2292625
    iget-object v0, v0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->startDate:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2292626
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->b:LX/FqG;

    iget-object v0, v0, LX/FqG;->a:Lcom/facebook/timeline/TimelineFragment;

    .line 2292627
    iget-object v1, v0, Lcom/facebook/timeline/BaseTimelineFragment;->n:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BPq;

    move-object v0, v1

    .line 2292628
    new-instance v1, LX/G4g;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->b:LX/FqG;

    iget-object v2, v2, LX/FqG;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v2, v2, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    .line 2292629
    iget-object v3, v2, LX/5SB;->d:Landroid/os/ParcelUuid;

    move-object v2, v3

    .line 2292630
    iget-object v3, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->a:LX/8Kd;

    .line 2292631
    iget-object v4, v3, LX/8Kd;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2292632
    invoke-direct {v1, v2, v3}, LX/G4g;-><init>(Landroid/os/ParcelUuid;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2292633
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->b:LX/FqG;

    iget-object v0, v0, LX/FqG;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/TimelineFragment;->S:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2iz;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->a:LX/8Kd;

    .line 2292634
    iget-object v2, v1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v2

    .line 2292635
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->x:Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    move-object v1, v2

    .line 2292636
    iget-object v1, v1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->composerSessionId:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->b:LX/FqG;

    iget-object v2, v2, LX/FqG;->a:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0, v1, v2}, LX/2iz;->b(Ljava/lang/String;Lcom/facebook/base/fragment/FbFragment;)Z

    .line 2292637
    return-void

    .line 2292638
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->a:LX/8Kd;

    .line 2292639
    iget-object v1, v0, LX/0b5;->b:LX/8KZ;

    move-object v0, v1

    .line 2292640
    sget-object v1, LX/8KZ;->UPLOAD_FAILED:LX/8KZ;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->b:LX/FqG;

    iget-object v0, v0, LX/FqG;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/TimelineFragment;->F:LX/G4x;

    if-eqz v0, :cond_0

    .line 2292641
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->b:LX/FqG;

    iget-object v0, v0, LX/FqG;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/TimelineFragment;->F:LX/G4x;

    sget-object v1, LX/G5A;->FAILED:LX/G5A;

    invoke-virtual {v0, v1}, LX/G4x;->a(LX/G5A;)V

    .line 2292642
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->b:LX/FqG;

    iget-object v0, v0, LX/FqG;->a:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    goto/16 :goto_0

    .line 2292643
    :cond_3
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->b:LX/FqG;

    iget-object v0, v0, LX/FqG;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    if-eqz v0, :cond_1

    .line 2292644
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->b:LX/FqG;

    iget-object v0, v0, LX/FqG;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    invoke-virtual {v0}, LX/FrV;->b()LX/Frd;

    move-result-object v0

    invoke-virtual {v0}, LX/Frd;->e()LX/FsJ;

    move-result-object v0

    .line 2292645
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment$16$1;->b:LX/FqG;

    iget-object v1, v1, LX/FqG;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v1, v1, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    invoke-virtual {v1}, LX/FrV;->b()LX/Frd;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/Frd;->b(LX/FsJ;)V

    goto :goto_1
.end method
