.class public abstract Lcom/facebook/timeline/BaseTimelineFragment;
.super Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0fx;
.implements LX/0fw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TAdapter::",
        "LX/1OP;",
        ">",
        "Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;",
        "LX/0fh;",
        "Lcom/facebook/timeline/units/controller/TimelineUnitSubscriberImpl$Listener;",
        "LX/0fx;",
        "LX/0fw;"
    }
.end annotation


# static fields
.field public static final c:Ljava/lang/String;


# instance fields
.field private A:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kx;",
            ">;"
        }
    .end annotation
.end field

.field public B:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/967;",
            ">;"
        }
    .end annotation
.end field

.field private C:Z

.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/14w;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/G4w;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/G4v;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ajn;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/1Yh;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:LX/1Le;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/1Yg;

.field private k:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0wq;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0bH;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/1B1;

.field public n:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BPq;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/1B1;

.field public p:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fqs;",
            ">;"
        }
    .end annotation
.end field

.field private q:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1L1;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/99v;",
            ">;"
        }
    .end annotation
.end field

.field private s:LX/Fpy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/timeline/BaseTimelineFragment",
            "<TTAdapter;>.DeletePhotoEventSubscriber;"
        }
    .end annotation
.end field

.field private t:LX/FuP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private u:LX/FuO;

.field private v:LX/FuM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private w:LX/FuL;

.field private x:LX/1Lg;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private y:LX/1M7;

.field private z:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2292243
    const-class v0, Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/BaseTimelineFragment;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2292136
    invoke-direct {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;-><init>()V

    .line 2292137
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2292138
    iput-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->g:LX/0Ot;

    .line 2292139
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2292140
    iput-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->z:LX/0Ot;

    .line 2292141
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2292142
    iput-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->A:LX/0Ot;

    .line 2292143
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2292144
    iput-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->B:LX/0Ot;

    .line 2292145
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->C:Z

    .line 2292146
    return-void
.end method

.method private static a(Lcom/facebook/timeline/BaseTimelineFragment;LX/0Or;LX/0Or;LX/G4w;LX/0Or;LX/0Ot;LX/1Le;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/FuP;LX/FuM;LX/1Lg;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/BaseTimelineFragment;",
            "LX/0Or",
            "<",
            "LX/14w;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0ad;",
            ">;",
            "LX/G4w;",
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ajn;",
            ">;",
            "LX/1Le;",
            "LX/0Or",
            "<",
            "LX/0wq;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0bH;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BPq;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Fqs;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1L1;",
            ">;",
            "LX/0Or",
            "<",
            "LX/99v;",
            ">;",
            "LX/FuP;",
            "LX/FuM;",
            "LX/1Lg;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Kx;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/967;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2292147
    iput-object p1, p0, Lcom/facebook/timeline/BaseTimelineFragment;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/timeline/BaseTimelineFragment;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/timeline/BaseTimelineFragment;->d:LX/G4w;

    iput-object p4, p0, Lcom/facebook/timeline/BaseTimelineFragment;->f:LX/0Or;

    iput-object p5, p0, Lcom/facebook/timeline/BaseTimelineFragment;->g:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/timeline/BaseTimelineFragment;->i:LX/1Le;

    iput-object p7, p0, Lcom/facebook/timeline/BaseTimelineFragment;->k:LX/0Or;

    iput-object p8, p0, Lcom/facebook/timeline/BaseTimelineFragment;->l:LX/0Or;

    iput-object p9, p0, Lcom/facebook/timeline/BaseTimelineFragment;->n:LX/0Or;

    iput-object p10, p0, Lcom/facebook/timeline/BaseTimelineFragment;->p:LX/0Or;

    iput-object p11, p0, Lcom/facebook/timeline/BaseTimelineFragment;->q:LX/0Or;

    iput-object p12, p0, Lcom/facebook/timeline/BaseTimelineFragment;->r:LX/0Or;

    iput-object p13, p0, Lcom/facebook/timeline/BaseTimelineFragment;->t:LX/FuP;

    iput-object p14, p0, Lcom/facebook/timeline/BaseTimelineFragment;->v:LX/FuM;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->x:LX/1Lg;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->z:LX/0Ot;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->A:LX/0Ot;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->B:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 22

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v20

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/timeline/BaseTimelineFragment;

    const/16 v3, 0xb23

    move-object/from16 v0, v20

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x1032

    move-object/from16 v0, v20

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const-class v5, LX/G4w;

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/G4w;

    const/16 v6, 0x259

    move-object/from16 v0, v20

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x1c98

    move-object/from16 v0, v20

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const-class v8, LX/1Le;

    move-object/from16 v0, v20

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/1Le;

    const/16 v9, 0x12e5

    move-object/from16 v0, v20

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x787

    move-object/from16 v0, v20

    invoke-static {v0, v10}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x3646

    move-object/from16 v0, v20

    invoke-static {v0, v11}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v12, 0x3622

    move-object/from16 v0, v20

    invoke-static {v0, v12}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    const/16 v13, 0xf2b

    move-object/from16 v0, v20

    invoke-static {v0, v13}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    const/16 v14, 0x1d67

    move-object/from16 v0, v20

    invoke-static {v0, v14}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    const-class v15, LX/FuP;

    move-object/from16 v0, v20

    invoke-interface {v0, v15}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/FuP;

    const-class v16, LX/FuM;

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/FuM;

    invoke-static/range {v20 .. v20}, LX/1Lf;->a(LX/0QB;)LX/1Lg;

    move-result-object v17

    check-cast v17, LX/1Lg;

    const/16 v18, 0x19c6

    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x77a

    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v21, 0x1a39

    invoke-static/range {v20 .. v21}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v20

    invoke-static/range {v2 .. v20}, Lcom/facebook/timeline/BaseTimelineFragment;->a(Lcom/facebook/timeline/BaseTimelineFragment;LX/0Or;LX/0Or;LX/G4w;LX/0Or;LX/0Ot;LX/1Le;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/FuP;LX/FuM;LX/1Lg;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method private b()LX/G4x;
    .locals 1

    .prologue
    .line 2292148
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->o()LX/G4x;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2292149
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->m:LX/1B1;

    .line 2292150
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->m:LX/1B1;

    new-instance v1, LX/Fpp;

    invoke-direct {v1, p0}, LX/Fpp;-><init>(Lcom/facebook/timeline/BaseTimelineFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2292151
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->m:LX/1B1;

    new-instance v1, LX/Fpq;

    invoke-direct {v1, p0}, LX/Fpq;-><init>(Lcom/facebook/timeline/BaseTimelineFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2292152
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->m:LX/1B1;

    new-instance v1, LX/Fpr;

    invoke-direct {v1, p0}, LX/Fpr;-><init>(Lcom/facebook/timeline/BaseTimelineFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2292153
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->m:LX/1B1;

    new-instance v1, LX/Fps;

    invoke-direct {v1, p0}, LX/Fps;-><init>(Lcom/facebook/timeline/BaseTimelineFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2292154
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->m:LX/1B1;

    new-instance v1, LX/Fpt;

    invoke-direct {v1, p0}, LX/Fpt;-><init>(Lcom/facebook/timeline/BaseTimelineFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2292155
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->m:LX/1B1;

    new-instance v1, LX/Fpu;

    invoke-direct {v1, p0}, LX/Fpu;-><init>(Lcom/facebook/timeline/BaseTimelineFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2292156
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->m:LX/1B1;

    new-instance v1, LX/Fpv;

    invoke-direct {v1, p0}, LX/Fpv;-><init>(Lcom/facebook/timeline/BaseTimelineFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2292157
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->m:LX/1B1;

    new-instance v1, LX/Fpw;

    invoke-direct {v1, p0}, LX/Fpw;-><init>(Lcom/facebook/timeline/BaseTimelineFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2292158
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kx;

    new-instance v1, LX/Fpx;

    invoke-direct {v1, p0}, LX/Fpx;-><init>(Lcom/facebook/timeline/BaseTimelineFragment;)V

    .line 2292159
    iput-object v1, v0, LX/1Kx;->c:LX/0TF;

    .line 2292160
    iget-object v1, p0, Lcom/facebook/timeline/BaseTimelineFragment;->m:LX/1B1;

    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kx;

    .line 2292161
    iget-object p0, v0, LX/1Kx;->b:LX/1Ky;

    move-object v0, p0

    .line 2292162
    invoke-virtual {v1, v0}, LX/1B1;->a(LX/0b2;)Z

    .line 2292163
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 2292164
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->m:LX/1B1;

    if-eqz v0, :cond_0

    .line 2292165
    iget-object v1, p0, Lcom/facebook/timeline/BaseTimelineFragment;->m:LX/1B1;

    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0b4;

    invoke-virtual {v1, v0}, LX/1B1;->a(LX/0b4;)V

    .line 2292166
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->o:LX/1B1;

    if-eqz v0, :cond_1

    .line 2292167
    iget-object v1, p0, Lcom/facebook/timeline/BaseTimelineFragment;->o:LX/1B1;

    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->n:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0b4;

    invoke-virtual {v1, v0}, LX/1B1;->a(LX/0b4;)V

    .line 2292168
    :cond_1
    return-void
.end method


# virtual methods
.method public final C()Lcom/facebook/composer/publish/ComposerPublishServiceHelper;
    .locals 1

    .prologue
    .line 2292169
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    return-object v0
.end method

.method public abstract a(I)I
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2292170
    const-string v0, "native_timeline"

    return-object v0
.end method

.method public final a(III)V
    .locals 4

    .prologue
    .line 2292211
    invoke-virtual {p0, p1}, Lcom/facebook/timeline/BaseTimelineFragment;->a(I)I

    move-result v0

    .line 2292212
    add-int v1, p1, p2

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/facebook/timeline/BaseTimelineFragment;->a(I)I

    move-result v1

    .line 2292213
    if-gez v0, :cond_0

    if-gez v1, :cond_0

    .line 2292214
    :goto_0
    return-void

    .line 2292215
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->o()LX/G4x;

    move-result-object v2

    .line 2292216
    sub-int v3, v1, v0

    add-int/lit8 v3, v3, 0x1

    .line 2292217
    iget-object p1, p0, Lcom/facebook/timeline/BaseTimelineFragment;->b:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/0ad;

    .line 2292218
    sget p2, LX/0wf;->aR:I

    const/4 v1, 0x7

    invoke-interface {p1, p2, v1}, LX/0ad;->a(II)I

    move-result p1

    move p1, p1

    .line 2292219
    const/4 p2, 0x0

    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result p2

    move p3, p2

    .line 2292220
    :goto_1
    invoke-virtual {v2}, LX/G4x;->size()I

    move-result p2

    if-ge p3, p2, :cond_6

    add-int p2, v0, v3

    add-int/2addr p2, p1

    if-ge p3, p2, :cond_6

    .line 2292221
    invoke-virtual {v2, p3}, LX/G4x;->a(I)Ljava/lang/Object;

    move-result-object p2

    .line 2292222
    instance-of v1, p2, LX/G51;

    if-eqz v1, :cond_5

    .line 2292223
    check-cast p2, LX/G51;

    .line 2292224
    :goto_2
    move-object v3, p2

    .line 2292225
    if-nez v3, :cond_2

    .line 2292226
    :cond_1
    :goto_3
    goto :goto_0

    .line 2292227
    :cond_2
    iget-object p1, v2, LX/G4x;->c:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_4
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/G59;

    .line 2292228
    iget-object p3, p1, LX/G59;->d:Ljava/util/List;

    invoke-interface {p3, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2292229
    goto :goto_4

    .line 2292230
    :cond_3
    invoke-static {v2}, LX/G4x;->e(LX/G4x;)V

    .line 2292231
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2292232
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->t()LX/Frd;

    move-result-object v2

    .line 2292233
    if-eqz v2, :cond_1

    .line 2292234
    iget-object p1, p0, Lcom/facebook/timeline/BaseTimelineFragment;->b:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    .line 2292235
    iget-object p1, v3, LX/G51;->a:LX/Fso;

    if-eqz p1, :cond_4

    .line 2292236
    iget-object p1, v3, LX/G51;->a:LX/Fso;

    invoke-virtual {v2, p1}, LX/Frd;->a(LX/Fso;)LX/0zX;

    move-result-object p1

    .line 2292237
    iget-object v3, v3, LX/G51;->a:LX/Fso;

    invoke-virtual {v2, v3, p1}, LX/Frd;->a(LX/Fso;LX/0zX;)V

    goto :goto_3

    .line 2292238
    :cond_4
    iget-object p1, v3, LX/G51;->b:LX/Fsp;

    if-eqz p1, :cond_1

    .line 2292239
    iget-object v3, v3, LX/G51;->b:LX/Fsp;

    invoke-virtual {v2, v3}, LX/Frd;->a(LX/Fsp;)LX/0zX;

    move-result-object v3

    .line 2292240
    invoke-virtual {v2, v3}, LX/Frd;->a(LX/0zX;)V

    goto :goto_3

    .line 2292241
    :cond_5
    add-int/lit8 p2, p3, 0x1

    move p3, p2

    goto :goto_1

    .line 2292242
    :cond_6
    const/4 p2, 0x0

    goto :goto_2
.end method

.method public a(LX/0g8;I)V
    .locals 0

    .prologue
    .line 2292210
    return-void
.end method

.method public a(LX/0g8;III)V
    .locals 4

    .prologue
    .line 2292118
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v0

    .line 2292119
    if-nez v0, :cond_1

    .line 2292120
    :cond_0
    invoke-virtual {p0, p2, p3, p4}, Lcom/facebook/timeline/BaseTimelineFragment;->a(III)V

    .line 2292121
    return-void

    .line 2292122
    :cond_1
    const/4 v0, 0x0

    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2292123
    add-int v1, v0, p3

    invoke-interface {p1}, LX/0g8;->s()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v3

    move v2, v0

    .line 2292124
    :goto_0
    if-ge v2, v3, :cond_0

    .line 2292125
    invoke-interface {p1, v2}, LX/0g8;->f(I)Ljava/lang/Object;

    move-result-object v1

    .line 2292126
    instance-of v0, v1, LX/1Rk;

    if-eqz v0, :cond_3

    move-object v0, v1

    check-cast v0, LX/1Rk;

    invoke-virtual {v0}, LX/1Rk;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2292127
    check-cast v1, LX/1Rk;

    invoke-virtual {v1}, LX/1Rk;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    move-object v0, v1

    .line 2292128
    :goto_1
    iget-object v1, p0, Lcom/facebook/timeline/BaseTimelineFragment;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    .line 2292129
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/14w;->n(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2292130
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(LX/G4e;)V
    .locals 4
    .param p1    # LX/G4e;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2292183
    invoke-direct {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->d()V

    .line 2292184
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->o:LX/1B1;

    .line 2292185
    if-eqz p1, :cond_0

    .line 2292186
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->o:LX/1B1;

    .line 2292187
    new-instance v1, LX/G4b;

    iget-object v2, p1, LX/G4e;->a:LX/5SB;

    .line 2292188
    iget-object v3, v2, LX/5SB;->d:Landroid/os/ParcelUuid;

    move-object v2, v3

    .line 2292189
    invoke-direct {v1, p1, v2}, LX/G4b;-><init>(LX/G4e;Landroid/os/ParcelUuid;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2292190
    new-instance v1, LX/G4d;

    iget-object v2, p1, LX/G4e;->a:LX/5SB;

    .line 2292191
    iget-object v3, v2, LX/5SB;->d:Landroid/os/ParcelUuid;

    move-object v2, v3

    .line 2292192
    invoke-direct {v1, p1, v2}, LX/G4d;-><init>(LX/G4e;Landroid/os/ParcelUuid;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2292193
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->o:LX/1B1;

    new-instance v1, LX/Fpj;

    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->y()LX/5SB;

    move-result-object v2

    .line 2292194
    iget-object v3, v2, LX/5SB;->d:Landroid/os/ParcelUuid;

    move-object v2, v3

    .line 2292195
    invoke-direct {v1, p0, v2}, LX/Fpj;-><init>(Lcom/facebook/timeline/BaseTimelineFragment;Landroid/os/ParcelUuid;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2292196
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->o:LX/1B1;

    new-instance v1, LX/Fpk;

    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->y()LX/5SB;

    move-result-object v2

    .line 2292197
    iget-object v3, v2, LX/5SB;->d:Landroid/os/ParcelUuid;

    move-object v2, v3

    .line 2292198
    invoke-direct {v1, p0, v2}, LX/Fpk;-><init>(Lcom/facebook/timeline/BaseTimelineFragment;Landroid/os/ParcelUuid;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2292199
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->o:LX/1B1;

    new-instance v1, LX/Fpl;

    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->y()LX/5SB;

    move-result-object v2

    .line 2292200
    iget-object v3, v2, LX/5SB;->d:Landroid/os/ParcelUuid;

    move-object v2, v3

    .line 2292201
    invoke-direct {v1, p0, v2}, LX/Fpl;-><init>(Lcom/facebook/timeline/BaseTimelineFragment;Landroid/os/ParcelUuid;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2292202
    invoke-direct {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->m()V

    .line 2292203
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->d:LX/G4w;

    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->lm_()LX/G4x;

    move-result-object v1

    .line 2292204
    new-instance v2, LX/G4v;

    const/16 v3, 0xafc

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 p1, 0x36f7

    invoke-static {v0, p1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    invoke-direct {v2, v1, p0, v3, p1}, LX/G4v;-><init>(LX/G4x;Lcom/facebook/timeline/BaseTimelineFragment;LX/0Or;LX/0Or;)V

    .line 2292205
    move-object v0, v2

    .line 2292206
    iput-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->e:LX/G4v;

    .line 2292207
    invoke-direct {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->b()LX/G4x;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/timeline/BaseTimelineFragment;->e:LX/G4v;

    .line 2292208
    iput-object v1, v0, LX/G4x;->g:LX/G4v;

    .line 2292209
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2292172
    const-string v0, "BaseTimelineFragment.onFragmentCreate"

    const v1, -0x6f45e718

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2292173
    :try_start_0
    invoke-super {p0, p1}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->a(Landroid/os/Bundle;)V

    .line 2292174
    const-string v0, "BaseTimelineFragment.onFragmentCreate.injectMe"

    const v1, 0x5357cd9c

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2292175
    :try_start_1
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/timeline/BaseTimelineFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2292176
    const v0, -0x7a3a9d61

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V

    .line 2292177
    new-instance v0, LX/Fpy;

    invoke-direct {v0, p0}, LX/Fpy;-><init>(Lcom/facebook/timeline/BaseTimelineFragment;)V

    iput-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->s:LX/Fpy;

    .line 2292178
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->x:LX/1Lg;

    sget-object v1, LX/1Li;->TIMELINE:LX/1Li;

    invoke-virtual {v0, v1}, LX/1Lg;->a(LX/1Li;)LX/1M7;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->y:LX/1M7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2292179
    const v0, -0x367d8b34

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2292180
    return-void

    .line 2292181
    :catchall_0
    move-exception v0

    const v1, 0x26fa5783

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2292182
    :catchall_1
    move-exception v0

    const v1, 0x523027df

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final lm_()LX/G4x;
    .locals 1

    .prologue
    .line 2292171
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->o()LX/G4x;

    move-result-object v0

    return-object v0
.end method

.method public final ln_()LX/Frd;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2292131
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->t()LX/Frd;

    move-result-object v0

    return-object v0
.end method

.method public final lo_()V
    .locals 1

    .prologue
    .line 2292132
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->v()LX/1OP;

    move-result-object v0

    .line 2292133
    if-eqz v0, :cond_0

    .line 2292134
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->v()LX/1OP;

    move-result-object v0

    invoke-interface {v0}, LX/1OP;->notifyDataSetChanged()V

    .line 2292135
    :cond_0
    return-void
.end method

.method public final lp_()V
    .locals 11

    .prologue
    .line 2292003
    const-string v0, "BaseTimelineFragment.setupImagePrefetchingForStories"

    const v1, 0x418cf8fd

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2292004
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->k()LX/0g8;

    move-result-object v1

    .line 2292005
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->v()LX/1OP;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 2292006
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 2292007
    :cond_0
    const v0, 0x7bbbb91d

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2292008
    :goto_0
    return-void

    .line 2292009
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ajn;

    .line 2292010
    new-instance v3, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;

    iget-object v6, v0, LX/Ajn;->b:LX/22q;

    iget-object v7, v0, LX/Ajn;->e:LX/Ajs;

    iget-object v8, v0, LX/Ajn;->d:LX/0Sy;

    iget-object v9, v0, LX/Ajn;->f:Ljava/util/concurrent/Executor;

    invoke-interface {v1}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v10

    move-object v4, v1

    move-object v5, v2

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;-><init>(LX/0g8;LX/1OP;LX/22q;LX/Ajk;LX/0Sy;Ljava/util/concurrent/Executor;Lcom/facebook/common/callercontext/CallerContext;)V

    move-object v0, v3

    .line 2292011
    iput-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->h:LX/1Yh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2292012
    const v0, -0x22f8124c

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x488a5f3

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final lq_()V
    .locals 3

    .prologue
    .line 2292013
    const-string v0, "BaseTimelineFragment.setupVideoPrefetchingForStories"

    const v1, 0x629c096

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2292014
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->k()LX/0g8;

    move-result-object v1

    .line 2292015
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->v()LX/1OP;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 2292016
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 2292017
    :cond_0
    const v0, 0x1a87649c

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2292018
    :goto_0
    return-void

    .line 2292019
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wq;

    iget-boolean v0, v0, LX/0wq;->s:Z

    if-eqz v0, :cond_2

    .line 2292020
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->i:LX/1Le;

    invoke-virtual {v0, v1, v2}, LX/1Le;->a(LX/0g8;LX/1OP;)LX/1Yg;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->j:LX/1Yg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2292021
    :cond_2
    const v0, -0x528d4b8

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x39451d02

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final lr_()V
    .locals 0

    .prologue
    .line 2292022
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->ls_()V

    .line 2292023
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->lr_()V

    .line 2292024
    return-void
.end method

.method public final ls_()V
    .locals 1

    .prologue
    .line 2292025
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->e:LX/G4v;

    if-eqz v0, :cond_0

    .line 2292026
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->e:LX/G4v;

    .line 2292027
    iget-object p0, v0, LX/G4v;->e:LX/1My;

    if-eqz p0, :cond_0

    .line 2292028
    iget-object p0, v0, LX/G4v;->e:LX/1My;

    invoke-virtual {p0}, LX/1My;->e()V

    .line 2292029
    :cond_0
    return-void
.end method

.method public final lt_()V
    .locals 1

    .prologue
    .line 2292030
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->e:LX/G4v;

    if-eqz v0, :cond_0

    .line 2292031
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->e:LX/G4v;

    .line 2292032
    iget-object p0, v0, LX/G4v;->e:LX/1My;

    if-eqz p0, :cond_0

    .line 2292033
    iget-object p0, v0, LX/G4v;->e:LX/1My;

    invoke-virtual {p0}, LX/1My;->d()V

    .line 2292034
    :cond_0
    return-void
.end method

.method public abstract o()LX/G4x;
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2292035
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ajn;

    .line 2292036
    iget-object p0, v0, LX/Ajn;->a:LX/1qa;

    move-object v0, p0

    .line 2292037
    invoke-virtual {v0}, LX/1qa;->a()V

    .line 2292038
    return-void
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x65611653

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2292039
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->e:LX/G4v;

    if-eqz v0, :cond_0

    .line 2292040
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->e:LX/G4v;

    .line 2292041
    iget-object v2, v0, LX/G4v;->e:LX/1My;

    if-eqz v2, :cond_0

    .line 2292042
    iget-object v2, v0, LX/G4v;->e:LX/1My;

    invoke-virtual {v2}, LX/1My;->a()V

    .line 2292043
    :cond_0
    invoke-direct {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->b()LX/G4x;

    move-result-object v0

    .line 2292044
    if-eqz v0, :cond_1

    .line 2292045
    iput-object v3, v0, LX/G4x;->g:LX/G4v;

    .line 2292046
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->o()LX/G4x;

    move-result-object v0

    .line 2292047
    if-eqz v0, :cond_2

    .line 2292048
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/G4x;->a(Z)V

    .line 2292049
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kx;

    invoke-virtual {v0}, LX/1Kx;->b()V

    .line 2292050
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->y:LX/1M7;

    invoke-interface {v0}, LX/1M7;->b()V

    .line 2292051
    iput-object v3, p0, Lcom/facebook/timeline/BaseTimelineFragment;->y:LX/1M7;

    .line 2292052
    iput-object v3, p0, Lcom/facebook/timeline/BaseTimelineFragment;->e:LX/G4v;

    .line 2292053
    iput-object v3, p0, Lcom/facebook/timeline/BaseTimelineFragment;->m:LX/1B1;

    .line 2292054
    iput-object v3, p0, Lcom/facebook/timeline/BaseTimelineFragment;->h:LX/1Yh;

    .line 2292055
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onDestroy()V

    .line 2292056
    const/16 v0, 0x2b

    const v2, -0x5d3e9c7f

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x19b3509e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2292057
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onDestroyView()V

    .line 2292058
    iget-object v1, p0, Lcom/facebook/timeline/BaseTimelineFragment;->h:LX/1Yh;

    if-eqz v1, :cond_0

    .line 2292059
    iget-object v1, p0, Lcom/facebook/timeline/BaseTimelineFragment;->h:LX/1Yh;

    invoke-virtual {v1}, LX/1Yh;->a()V

    .line 2292060
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/BaseTimelineFragment;->j:LX/1Yg;

    if-eqz v1, :cond_1

    .line 2292061
    iget-object v1, p0, Lcom/facebook/timeline/BaseTimelineFragment;->j:LX/1Yg;

    invoke-virtual {v1}, LX/1Yh;->a()V

    .line 2292062
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x7765daa5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onPause()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6570c664

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2292063
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->m:LX/1B1;

    if-eqz v0, :cond_0

    .line 2292064
    iget-object v2, p0, Lcom/facebook/timeline/BaseTimelineFragment;->m:LX/1B1;

    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0b4;

    invoke-virtual {v2, v0}, LX/1B1;->b(LX/0b4;)V

    .line 2292065
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->o:LX/1B1;

    if-eqz v0, :cond_1

    .line 2292066
    iget-object v2, p0, Lcom/facebook/timeline/BaseTimelineFragment;->o:LX/1B1;

    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->n:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0b4;

    invoke-virtual {v2, v0}, LX/1B1;->b(LX/0b4;)V

    .line 2292067
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->C:Z

    .line 2292068
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1L1;

    iget-object v2, p0, Lcom/facebook/timeline/BaseTimelineFragment;->s:LX/Fpy;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2292069
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->lt_()V

    .line 2292070
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->u:LX/FuO;

    if-eqz v0, :cond_2

    .line 2292071
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->u:LX/FuO;

    .line 2292072
    iget-object v2, v0, LX/FuO;->g:LX/6Vi;

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_0
    const-string v5, "Trying to stop TimelineScrubberClickEventProcessor that was not started"

    invoke-static {v2, v5}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2292073
    iget-object v2, v0, LX/FuO;->f:LX/1K9;

    iget-object v5, v0, LX/FuO;->g:LX/6Vi;

    invoke-virtual {v2, v5}, LX/1K9;->a(LX/6Vi;)V

    .line 2292074
    const/4 v2, 0x0

    iput-object v2, v0, LX/FuO;->g:LX/6Vi;

    .line 2292075
    iput-object v4, p0, Lcom/facebook/timeline/BaseTimelineFragment;->u:LX/FuO;

    .line 2292076
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->w:LX/FuL;

    if-eqz v0, :cond_3

    .line 2292077
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->w:LX/FuL;

    .line 2292078
    iget-object v2, v0, LX/FuL;->d:LX/6Vi;

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_1
    const-string v5, "Trying to stop TimelineLoadingIndicatorClickProcessor that was not started"

    invoke-static {v2, v5}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2292079
    iget-object v2, v0, LX/FuL;->c:LX/1K9;

    iget-object v5, v0, LX/FuL;->d:LX/6Vi;

    invoke-virtual {v2, v5}, LX/1K9;->a(LX/6Vi;)V

    .line 2292080
    const/4 v2, 0x0

    iput-object v2, v0, LX/FuL;->d:LX/6Vi;

    .line 2292081
    iput-object v4, p0, Lcom/facebook/timeline/BaseTimelineFragment;->w:LX/FuL;

    .line 2292082
    :cond_3
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->y:LX/1M7;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, LX/1M7;->a(Z)V

    .line 2292083
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onPause()V

    .line 2292084
    const/16 v0, 0x2b

    const v2, -0x25766cb1

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2292085
    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    .line 2292086
    :cond_5
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public onResume()V
    .locals 13

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x56e9f94a

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2292087
    const-string v0, "BaseTimelineFragment.onResume"

    const v2, 0x184898a3

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2292088
    :try_start_0
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onResume()V

    .line 2292089
    iget-boolean v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->C:Z

    if-eqz v0, :cond_0

    .line 2292090
    invoke-direct {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->m()V

    .line 2292091
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->C:Z

    .line 2292092
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1L1;

    iget-object v2, p0, Lcom/facebook/timeline/BaseTimelineFragment;->s:LX/Fpy;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2292093
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->t:LX/FuP;

    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->o()LX/G4x;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->t()LX/Frd;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->y()LX/5SB;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->v()LX/1OP;

    move-result-object v5

    .line 2292094
    new-instance v6, LX/FuO;

    const/16 v7, 0x91

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v12

    check-cast v12, LX/1K9;

    move-object v7, v2

    move-object v8, v3

    move-object v9, v4

    move-object v10, v5

    invoke-direct/range {v6 .. v12}, LX/FuO;-><init>(LX/G4x;LX/Frd;LX/5SB;LX/1OP;LX/0Ot;LX/1K9;)V

    .line 2292095
    move-object v0, v6

    .line 2292096
    iput-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->u:LX/FuO;

    .line 2292097
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->u:LX/FuO;

    .line 2292098
    iget-object v2, v0, LX/FuO;->g:LX/6Vi;

    if-nez v2, :cond_1

    .line 2292099
    iget-object v2, v0, LX/FuO;->f:LX/1K9;

    const-class v3, LX/Fu3;

    .line 2292100
    new-instance v4, LX/FuN;

    invoke-direct {v4, v0}, LX/FuN;-><init>(LX/FuO;)V

    move-object v4, v4

    .line 2292101
    invoke-virtual {v2, v3, v4}, LX/1K9;->a(Ljava/lang/Class;LX/6Ve;)LX/6Vi;

    move-result-object v2

    iput-object v2, v0, LX/FuO;->g:LX/6Vi;

    .line 2292102
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->v:LX/FuM;

    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->t()LX/Frd;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->y()LX/5SB;

    move-result-object v3

    .line 2292103
    new-instance v5, LX/FuL;

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v4

    check-cast v4, LX/1K9;

    invoke-direct {v5, v2, v3, v4}, LX/FuL;-><init>(LX/Frd;LX/5SB;LX/1K9;)V

    .line 2292104
    move-object v0, v5

    .line 2292105
    iput-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->w:LX/FuL;

    .line 2292106
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->w:LX/FuL;

    .line 2292107
    iget-object v2, v0, LX/FuL;->d:LX/6Vi;

    if-nez v2, :cond_2

    .line 2292108
    iget-object v2, v0, LX/FuL;->c:LX/1K9;

    const-class v3, LX/Fu2;

    .line 2292109
    new-instance v4, LX/FuK;

    invoke-direct {v4, v0}, LX/FuK;-><init>(LX/FuL;)V

    move-object v4, v4

    .line 2292110
    invoke-virtual {v2, v3, v4}, LX/1K9;->a(Ljava/lang/Class;LX/6Ve;)LX/6Vi;

    move-result-object v2

    iput-object v2, v0, LX/FuL;->d:LX/6Vi;

    .line 2292111
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ajn;

    .line 2292112
    iget-object v2, v0, LX/Ajn;->a:LX/1qa;

    move-object v0, v2

    .line 2292113
    invoke-virtual {v0}, LX/1qa;->a()V

    .line 2292114
    iget-object v0, p0, Lcom/facebook/timeline/BaseTimelineFragment;->y:LX/1M7;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/1M7;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2292115
    const v0, 0x499e96f3

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2292116
    const v0, -0x19b5d33f

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2292117
    :catchall_0
    move-exception v0

    const v2, 0x755aa1c

    invoke-static {v2}, LX/02m;->a(I)V

    const v2, -0x634ca056

    invoke-static {v2, v1}, LX/02F;->f(II)V

    throw v0
.end method

.method public abstract t()LX/Frd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract v()LX/1OP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TTAdapter;"
        }
    .end annotation
.end method

.method public abstract w()LX/1Kt;
.end method

.method public abstract x()LX/2dj;
.end method

.method public abstract y()LX/5SB;
.end method

.method public abstract z()Ljava/lang/String;
.end method
