.class public Lcom/facebook/timeline/service/TimelineImagePrefetcher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field private final b:LX/1HI;

.field public final c:LX/G4M;

.field public final d:LX/G4R;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2317627
    const-class v0, Lcom/facebook/timeline/service/TimelineImagePrefetcher;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/service/TimelineImagePrefetcher;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1HI;LX/G4M;LX/G4R;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2317601
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2317602
    iput-object p1, p0, Lcom/facebook/timeline/service/TimelineImagePrefetcher;->b:LX/1HI;

    .line 2317603
    iput-object p2, p0, Lcom/facebook/timeline/service/TimelineImagePrefetcher;->c:LX/G4M;

    .line 2317604
    iput-object p3, p0, Lcom/facebook/timeline/service/TimelineImagePrefetcher;->d:LX/G4R;

    .line 2317605
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/service/TimelineImagePrefetcher;
    .locals 6

    .prologue
    .line 2317616
    const-class v1, Lcom/facebook/timeline/service/TimelineImagePrefetcher;

    monitor-enter v1

    .line 2317617
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/service/TimelineImagePrefetcher;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2317618
    sput-object v2, Lcom/facebook/timeline/service/TimelineImagePrefetcher;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2317619
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2317620
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2317621
    new-instance p0, Lcom/facebook/timeline/service/TimelineImagePrefetcher;

    invoke-static {v0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v3

    check-cast v3, LX/1HI;

    invoke-static {v0}, LX/G4M;->a(LX/0QB;)LX/G4M;

    move-result-object v4

    check-cast v4, LX/G4M;

    invoke-static {v0}, LX/G4R;->a(LX/0QB;)LX/G4R;

    move-result-object v5

    check-cast v5, LX/G4R;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/timeline/service/TimelineImagePrefetcher;-><init>(LX/1HI;LX/G4M;LX/G4R;)V

    .line 2317622
    move-object v0, p0

    .line 2317623
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2317624
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/service/TimelineImagePrefetcher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2317625
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2317626
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/timeline/service/TimelineImagePrefetcher;LX/G4P;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/G4P;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2317606
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2317607
    :cond_0
    return-void

    .line 2317608
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    .line 2317609
    iput v0, p1, LX/G4P;->c:I

    .line 2317610
    if-lez v0, :cond_2

    .line 2317611
    iget-object v1, p1, LX/G4P;->a:LX/G4B;

    invoke-virtual {v1}, LX/G4B;->a()V

    .line 2317612
    :cond_2
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 2317613
    iget-object v2, p0, Lcom/facebook/timeline/service/TimelineImagePrefetcher;->b:LX/1HI;

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    sget-object v3, Lcom/facebook/timeline/service/TimelineImagePrefetcher;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v0, v3}, LX/1HI;->e(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 2317614
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 2317615
    invoke-interface {v0, p1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
