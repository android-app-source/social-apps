.class public final Lcom/facebook/timeline/service/ProfileLoadManager$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/G4K;

.field public final synthetic b:LX/G4C;

.field public final synthetic c:Lcom/facebook/timeline/service/ProfileLoadManager;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/service/ProfileLoadManager;LX/G4K;LX/G4C;)V
    .locals 0

    .prologue
    .line 2317190
    iput-object p1, p0, Lcom/facebook/timeline/service/ProfileLoadManager$2;->c:Lcom/facebook/timeline/service/ProfileLoadManager;

    iput-object p2, p0, Lcom/facebook/timeline/service/ProfileLoadManager$2;->a:LX/G4K;

    iput-object p3, p0, Lcom/facebook/timeline/service/ProfileLoadManager$2;->b:LX/G4C;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    .line 2317177
    iget-object v0, p0, Lcom/facebook/timeline/service/ProfileLoadManager$2;->c:Lcom/facebook/timeline/service/ProfileLoadManager;

    iget-object v0, v0, Lcom/facebook/timeline/service/ProfileLoadManager;->a:LX/Fq9;

    iget-object v1, p0, Lcom/facebook/timeline/service/ProfileLoadManager$2;->a:LX/G4K;

    invoke-virtual {v1}, LX/G4K;->a()J

    move-result-wide v2

    const/4 v1, 0x2

    invoke-virtual {v0, v2, v3, v1}, LX/Fq9;->a(JI)V

    .line 2317178
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.facebook.intent.action.ACTION_GET_NOTIFIED_PROFILE_LOADED"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2317179
    const-string v0, "com.facebook.katana.profile.id"

    iget-object v2, p0, Lcom/facebook/timeline/service/ProfileLoadManager$2;->a:LX/G4K;

    invoke-virtual {v2}, LX/G4K;->a()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2317180
    iget-object v0, p0, Lcom/facebook/timeline/service/ProfileLoadManager$2;->c:Lcom/facebook/timeline/service/ProfileLoadManager;

    iget-object v0, v0, Lcom/facebook/timeline/service/ProfileLoadManager;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2317181
    iget-object v0, p0, Lcom/facebook/timeline/service/ProfileLoadManager$2;->c:Lcom/facebook/timeline/service/ProfileLoadManager;

    iget-object v0, v0, Lcom/facebook/timeline/service/ProfileLoadManager;->c:LX/G4H;

    iget-object v1, p0, Lcom/facebook/timeline/service/ProfileLoadManager$2;->a:LX/G4K;

    invoke-virtual {v1}, LX/G4K;->a()J

    move-result-wide v2

    iget-object v1, p0, Lcom/facebook/timeline/service/ProfileLoadManager$2;->b:LX/G4C;

    .line 2317182
    iget-object v4, v1, LX/G4C;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    move-object v1, v4

    .line 2317183
    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->ce_()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/facebook/timeline/service/ProfileLoadManager$2;->a:LX/G4K;

    invoke-virtual {v4}, LX/G4K;->c()Landroid/os/Bundle;

    move-result-object v4

    const/4 v11, 0x0

    .line 2317184
    invoke-static {v0, v2, v3}, LX/G4H;->a(LX/G4H;J)Z

    move-result v5

    if-eqz v5, :cond_0

    const v10, 0x7f081629

    :goto_0
    move-object v6, v0

    move-wide v7, v2

    move-object v9, v1

    move-object v12, v4

    .line 2317185
    invoke-static/range {v6 .. v12}, LX/G4H;->a(LX/G4H;JLjava/lang/String;IZLandroid/os/Bundle;)Landroid/app/Notification;

    move-result-object v5

    .line 2317186
    invoke-static {v0, v5, v2, v3}, LX/G4H;->a(LX/G4H;Landroid/app/Notification;J)V

    .line 2317187
    iget-object v5, v0, LX/G4H;->e:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/BQ9;

    invoke-virtual {v5, v11, v2, v3}, LX/BQ9;->a(IJ)V

    .line 2317188
    return-void

    .line 2317189
    :cond_0
    const v10, 0x7f081628

    goto :goto_0
.end method
