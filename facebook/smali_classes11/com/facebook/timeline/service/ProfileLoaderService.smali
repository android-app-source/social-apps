.class public Lcom/facebook/timeline/service/ProfileLoaderService;
.super LX/42u;
.source ""


# static fields
.field private static final e:Ljava/lang/Class;


# instance fields
.field public b:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Zr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/timeline/service/ProfileLoadManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final f:I

.field private final g:I

.field private h:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field private i:Z

.field private j:Landroid/content/Intent;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2317516
    const-class v0, Lcom/facebook/timeline/service/ProfileLoaderService;

    sput-object v0, Lcom/facebook/timeline/service/ProfileLoaderService;->e:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2317512
    invoke-direct {p0}, LX/42u;-><init>()V

    .line 2317513
    const/4 v0, 0x5

    iput v0, p0, Lcom/facebook/timeline/service/ProfileLoaderService;->f:I

    .line 2317514
    const/4 v0, 0x2

    iput v0, p0, Lcom/facebook/timeline/service/ProfileLoaderService;->g:I

    .line 2317515
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/facebook/timeline/service/ProfileLoaderService;->j:Landroid/content/Intent;

    return-void
.end method

.method private static a(Lcom/facebook/timeline/service/ProfileLoaderService;Ljava/util/concurrent/ScheduledExecutorService;LX/0Zr;Lcom/facebook/timeline/service/ProfileLoadManager;)V
    .locals 0

    .prologue
    .line 2317484
    iput-object p1, p0, Lcom/facebook/timeline/service/ProfileLoaderService;->b:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p2, p0, Lcom/facebook/timeline/service/ProfileLoaderService;->c:LX/0Zr;

    iput-object p3, p0, Lcom/facebook/timeline/service/ProfileLoaderService;->d:Lcom/facebook/timeline/service/ProfileLoadManager;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/timeline/service/ProfileLoaderService;

    invoke-static {v2}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v2}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v1

    check-cast v1, LX/0Zr;

    invoke-static {v2}, Lcom/facebook/timeline/service/ProfileLoadManager;->b(LX/0QB;)Lcom/facebook/timeline/service/ProfileLoadManager;

    move-result-object v2

    check-cast v2, Lcom/facebook/timeline/service/ProfileLoadManager;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/timeline/service/ProfileLoaderService;->a(Lcom/facebook/timeline/service/ProfileLoaderService;Ljava/util/concurrent/ScheduledExecutorService;LX/0Zr;Lcom/facebook/timeline/service/ProfileLoadManager;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2317497
    iget-object v0, p0, Lcom/facebook/timeline/service/ProfileLoaderService;->j:Landroid/content/Intent;

    if-ne v0, p1, :cond_1

    .line 2317498
    :cond_0
    :goto_0
    return-void

    .line 2317499
    :cond_1
    iput-object p1, p0, Lcom/facebook/timeline/service/ProfileLoaderService;->j:Landroid/content/Intent;

    .line 2317500
    if-nez p1, :cond_2

    .line 2317501
    iget-object v0, p0, Lcom/facebook/timeline/service/ProfileLoaderService;->d:Lcom/facebook/timeline/service/ProfileLoadManager;

    .line 2317502
    iget-object v1, v0, Lcom/facebook/timeline/service/ProfileLoadManager;->m:LX/G4L;

    invoke-virtual {v1}, LX/G4L;->b()LX/0Px;

    move-result-object v1

    .line 2317503
    iget-object v2, v0, Lcom/facebook/timeline/service/ProfileLoadManager;->m:LX/G4L;

    invoke-virtual {v2}, LX/G4L;->c()LX/0Px;

    move-result-object v2

    .line 2317504
    iget-object p0, v0, Lcom/facebook/timeline/service/ProfileLoadManager;->m:LX/G4L;

    .line 2317505
    iget-object p1, p0, LX/G4L;->c:Landroid/content/SharedPreferences;

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2317506
    invoke-static {v0, v1}, Lcom/facebook/timeline/service/ProfileLoadManager;->a(Lcom/facebook/timeline/service/ProfileLoadManager;LX/0Px;)V

    .line 2317507
    invoke-static {v0, v2}, Lcom/facebook/timeline/service/ProfileLoadManager;->a(Lcom/facebook/timeline/service/ProfileLoadManager;LX/0Px;)V

    .line 2317508
    goto :goto_0

    .line 2317509
    :cond_2
    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2317510
    new-instance v0, LX/G4K;

    new-instance v1, LX/G4I;

    invoke-direct {v1, p1}, LX/G4I;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v1}, LX/G4I;->a()LX/G4J;

    move-result-object v1

    invoke-direct {v0, v1}, LX/G4K;-><init>(LX/G4J;)V

    .line 2317511
    iget-object v1, p0, Lcom/facebook/timeline/service/ProfileLoaderService;->d:Lcom/facebook/timeline/service/ProfileLoadManager;

    invoke-virtual {v1, v0}, Lcom/facebook/timeline/service/ProfileLoadManager;->a(LX/G4K;)V

    goto :goto_0
.end method

.method public final b()Landroid/os/Looper;
    .locals 2

    .prologue
    .line 2317491
    iget-boolean v0, p0, Lcom/facebook/timeline/service/ProfileLoaderService;->i:Z

    if-nez v0, :cond_0

    .line 2317492
    invoke-static {p0, p0}, Lcom/facebook/timeline/service/ProfileLoaderService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2317493
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/timeline/service/ProfileLoaderService;->i:Z

    .line 2317494
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/service/ProfileLoaderService;->c:LX/0Zr;

    sget-object v1, Lcom/facebook/timeline/service/ProfileLoaderService;->e:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Zr;->a(Ljava/lang/String;)Landroid/os/HandlerThread;

    move-result-object v0

    .line 2317495
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 2317496
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 7

    .prologue
    .line 2317489
    iget-object v0, p0, Lcom/facebook/timeline/service/ProfileLoaderService;->b:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/timeline/service/ProfileLoaderService$1;

    invoke-direct {v1, p0}, Lcom/facebook/timeline/service/ProfileLoaderService$1;-><init>(Lcom/facebook/timeline/service/ProfileLoaderService;)V

    const-wide/16 v2, 0x5

    const-wide/16 v4, 0x2

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/service/ProfileLoaderService;->h:Ljava/util/concurrent/ScheduledFuture;

    .line 2317490
    return-void
.end method

.method public final d()Ljava/lang/Integer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2317488
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 2317485
    iget-object v0, p0, Lcom/facebook/timeline/service/ProfileLoaderService;->h:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2317486
    invoke-super {p0}, LX/42u;->h()V

    .line 2317487
    return-void
.end method
