.class public Lcom/facebook/timeline/service/ProfileLoadManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final p:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/Fq9;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/timeline/service/TimelineImagePrefetcher;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/G4H;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fsf;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FsU;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G13;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FsP;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Or;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Or;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0kb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public m:LX/G4L;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/Fwh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public o:LX/Fx5;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:LX/0Yb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2317350
    const-class v0, Lcom/facebook/timeline/service/ProfileLoadManager;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/service/ProfileLoadManager;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2317295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2317296
    return-void
.end method

.method public static a(LX/0zX;LX/0rl;LX/G4B;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zX",
            "<*>;",
            "LX/0rl;",
            "LX/G4B;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2317346
    if-eqz p0, :cond_0

    .line 2317347
    invoke-virtual {p2}, LX/G4B;->a()V

    .line 2317348
    invoke-virtual {p0, p1}, LX/0zX;->a(LX/0rl;)LX/0zi;

    .line 2317349
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/timeline/service/ProfileLoadManager;LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/G4K;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2317342
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G4K;

    .line 2317343
    invoke-virtual {p0, v0}, Lcom/facebook/timeline/service/ProfileLoadManager;->a(LX/G4K;)V

    .line 2317344
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2317345
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/timeline/service/ProfileLoadManager;LX/Fq9;Lcom/facebook/timeline/service/TimelineImagePrefetcher;LX/G4H;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0kb;LX/G4L;LX/Fwh;LX/Fx5;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/service/ProfileLoadManager;",
            "LX/Fq9;",
            "Lcom/facebook/timeline/service/TimelineImagePrefetcher;",
            "LX/G4H;",
            "LX/0Or",
            "<",
            "LX/Fsf;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FsU;",
            ">;",
            "LX/0Or",
            "<",
            "LX/G13;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FsP;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/0kb;",
            "LX/G4L;",
            "LX/Fwh;",
            "LX/Fx5;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2317341
    iput-object p1, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->a:LX/Fq9;

    iput-object p2, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->b:Lcom/facebook/timeline/service/TimelineImagePrefetcher;

    iput-object p3, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->c:LX/G4H;

    iput-object p4, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->d:LX/0Or;

    iput-object p5, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->e:LX/0Or;

    iput-object p6, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->f:LX/0Or;

    iput-object p7, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->g:LX/0Or;

    iput-object p8, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->h:LX/0Or;

    iput-object p9, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->i:LX/0Or;

    iput-object p10, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->j:LX/0Or;

    iput-object p11, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->k:LX/0Or;

    iput-object p12, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->l:LX/0kb;

    iput-object p13, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->m:LX/G4L;

    iput-object p14, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->n:LX/Fwh;

    iput-object p15, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->o:LX/Fx5;

    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/timeline/service/ProfileLoadManager;
    .locals 17

    .prologue
    .line 2317338
    new-instance v1, Lcom/facebook/timeline/service/ProfileLoadManager;

    invoke-direct {v1}, Lcom/facebook/timeline/service/ProfileLoadManager;-><init>()V

    .line 2317339
    invoke-static/range {p0 .. p0}, LX/Fq9;->a(LX/0QB;)LX/Fq9;

    move-result-object v2

    check-cast v2, LX/Fq9;

    invoke-static/range {p0 .. p0}, Lcom/facebook/timeline/service/TimelineImagePrefetcher;->a(LX/0QB;)Lcom/facebook/timeline/service/TimelineImagePrefetcher;

    move-result-object v3

    check-cast v3, Lcom/facebook/timeline/service/TimelineImagePrefetcher;

    invoke-static/range {p0 .. p0}, LX/G4H;->a(LX/0QB;)LX/G4H;

    move-result-object v4

    check-cast v4, LX/G4H;

    const/16 v5, 0x363d

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x3639

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x36ac

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x3637

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0xafd

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x15e7

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x1430

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v12, 0x1ce

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v13

    check-cast v13, LX/0kb;

    invoke-static/range {p0 .. p0}, LX/G4L;->a(LX/0QB;)LX/G4L;

    move-result-object v14

    check-cast v14, LX/G4L;

    invoke-static/range {p0 .. p0}, LX/Fwh;->a(LX/0QB;)LX/Fwh;

    move-result-object v15

    check-cast v15, LX/Fwh;

    invoke-static/range {p0 .. p0}, LX/Fx5;->a(LX/0QB;)LX/Fx5;

    move-result-object v16

    check-cast v16, LX/Fx5;

    invoke-static/range {v1 .. v16}, Lcom/facebook/timeline/service/ProfileLoadManager;->a(Lcom/facebook/timeline/service/ProfileLoadManager;LX/Fq9;Lcom/facebook/timeline/service/TimelineImagePrefetcher;LX/G4H;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0kb;LX/G4L;LX/Fwh;LX/Fx5;)V

    .line 2317340
    return-object v1
.end method

.method private declared-synchronized b(LX/G4K;)V
    .locals 1

    .prologue
    .line 2317335
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->m:LX/G4L;

    invoke-virtual {v0, p1}, LX/G4L;->a(LX/G4K;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2317336
    monitor-exit p0

    return-void

    .line 2317337
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized c$redex0(Lcom/facebook/timeline/service/ProfileLoadManager;LX/G4K;)V
    .locals 1

    .prologue
    .line 2317332
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->m:LX/G4L;

    invoke-virtual {v0, p1}, LX/G4L;->b(LX/G4K;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2317333
    monitor-exit p0

    return-void

    .line 2317334
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(LX/G4K;)V
    .locals 14

    .prologue
    .line 2317297
    iget-object v0, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->m:LX/G4L;

    invoke-virtual {v0, p1}, LX/G4L;->d(LX/G4K;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2317298
    :goto_0
    return-void

    .line 2317299
    :cond_0
    invoke-virtual {p1}, LX/G4K;->a()J

    move-result-wide v0

    .line 2317300
    iget-object v2, p1, LX/G4K;->a:LX/G4J;

    iget-object v2, v2, LX/G4J;->c:Ljava/lang/String;

    invoke-static {v2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    move-object v2, v2

    .line 2317301
    iget-object v7, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->a:LX/Fq9;

    invoke-virtual {v7, v0, v1}, LX/Fq9;->a(J)LX/1Zp;

    move-result-object v7

    .line 2317302
    if-nez v7, :cond_1

    .line 2317303
    iget-object v7, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->g:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/FsP;

    iget-object v8, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->f:LX/0Or;

    invoke-interface {v8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/G13;

    iget-object v9, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->n:LX/Fwh;

    invoke-virtual {v9}, LX/Fwh;->h()Z

    move-result v12

    iget-object v9, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->o:LX/Fx5;

    invoke-virtual {v9}, LX/Fx5;->h()Z

    move-result v13

    move-wide v9, v0

    move-object v11, v2

    invoke-virtual/range {v8 .. v13}, LX/G13;->a(JLX/0am;ZZ)Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;

    move-result-object v8

    sget-object v9, LX/0zS;->d:LX/0zS;

    sget-object v10, Lcom/facebook/timeline/service/ProfileLoadManager;->p:Lcom/facebook/common/callercontext/CallerContext;

    sget-object v11, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v7, v8, v9, v10, v11}, LX/FsP;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;LX/0zS;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v8

    .line 2317304
    iget-object v7, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->h:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-virtual {v7, v8}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v7

    .line 2317305
    :cond_1
    move-object v0, v7

    .line 2317306
    iget-object v1, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->a:LX/Fq9;

    invoke-virtual {p1}, LX/G4K;->a()J

    move-result-wide v2

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, LX/Fq9;->a(JI)V

    .line 2317307
    new-instance v1, LX/G4C;

    invoke-direct {v1}, LX/G4C;-><init>()V

    .line 2317308
    new-instance v2, LX/G4B;

    new-instance v3, LX/G4D;

    invoke-direct {v3, p0, p1}, LX/G4D;-><init>(Lcom/facebook/timeline/service/ProfileLoadManager;LX/G4K;)V

    invoke-direct {v2, v3, v1}, LX/G4B;-><init>(LX/G4D;Ljava/lang/Object;)V

    .line 2317309
    iget-object v1, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->c:LX/G4H;

    invoke-virtual {p1}, LX/G4K;->a()J

    move-result-wide v4

    invoke-virtual {p1}, LX/G4K;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LX/G4K;->c()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {v1, v4, v5, v3, v6}, LX/G4H;->a(JLjava/lang/String;Landroid/os/Bundle;)V

    .line 2317310
    invoke-direct {p0, p1}, Lcom/facebook/timeline/service/ProfileLoadManager;->b(LX/G4K;)V

    .line 2317311
    new-instance v1, LX/G4E;

    invoke-direct {v1, p0, v2, p1}, LX/G4E;-><init>(Lcom/facebook/timeline/service/ProfileLoadManager;LX/G4B;LX/G4K;)V

    .line 2317312
    sget-object v3, LX/131;->INSTANCE:LX/131;

    move-object v3, v3

    .line 2317313
    invoke-static {v0, v1, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2317314
    invoke-virtual {p1}, LX/G4K;->a()J

    move-result-wide v0

    .line 2317315
    iget-object v3, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->i:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v7, 0x0

    .line 2317316
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2317317
    iget-object v4, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->d:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/FrR;

    .line 2317318
    :goto_1
    new-instance v5, LX/G12;

    const/4 v6, 0x0

    invoke-direct {v5, v0, v1, v6, v7}, LX/G12;-><init>(JLjava/lang/String;Z)V

    .line 2317319
    sget-object v6, Lcom/facebook/timeline/service/ProfileLoadManager;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v4, v7, v5, v6}, LX/FrR;->a(ZLX/G12;Lcom/facebook/common/callercontext/CallerContext;)LX/FsJ;

    move-result-object v4

    move-object v3, v4

    .line 2317320
    new-instance v4, LX/G4G;

    invoke-direct {v4, p0, v2}, LX/G4G;-><init>(Lcom/facebook/timeline/service/ProfileLoadManager;LX/G4B;)V

    .line 2317321
    iget-object v5, v3, LX/FsJ;->a:LX/0zX;

    invoke-static {v5, v4, v2}, Lcom/facebook/timeline/service/ProfileLoadManager;->a(LX/0zX;LX/0rl;LX/G4B;)V

    .line 2317322
    iget-object v5, v3, LX/FsJ;->c:LX/0zX;

    invoke-static {v5, v4, v2}, Lcom/facebook/timeline/service/ProfileLoadManager;->a(LX/0zX;LX/0rl;LX/G4B;)V

    .line 2317323
    iget-object v5, v3, LX/FsJ;->f:LX/0zX;

    invoke-static {v5, v4, v2}, Lcom/facebook/timeline/service/ProfileLoadManager;->a(LX/0zX;LX/0rl;LX/G4B;)V

    .line 2317324
    iget-object v5, v3, LX/FsJ;->d:LX/0zX;

    invoke-static {v5, v4, v2}, Lcom/facebook/timeline/service/ProfileLoadManager;->a(LX/0zX;LX/0rl;LX/G4B;)V

    .line 2317325
    iget-object v5, v3, LX/FsJ;->g:LX/0zX;

    invoke-static {v5, v4, v2}, Lcom/facebook/timeline/service/ProfileLoadManager;->a(LX/0zX;LX/0rl;LX/G4B;)V

    .line 2317326
    iget-object v5, v3, LX/FsJ;->e:LX/0zX;

    invoke-static {v5, v4, v2}, Lcom/facebook/timeline/service/ProfileLoadManager;->a(LX/0zX;LX/0rl;LX/G4B;)V

    .line 2317327
    iget-object v5, v3, LX/FsJ;->h:LX/0zX;

    invoke-static {v5, v4, v2}, Lcom/facebook/timeline/service/ProfileLoadManager;->a(LX/0zX;LX/0rl;LX/G4B;)V

    .line 2317328
    const/4 v0, 0x1

    iput-boolean v0, v2, LX/G4B;->d:Z

    .line 2317329
    invoke-static {v2}, LX/G4B;->e(LX/G4B;)V

    .line 2317330
    goto/16 :goto_0

    .line 2317331
    :cond_2
    iget-object v4, p0, Lcom/facebook/timeline/service/ProfileLoadManager;->e:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/FrR;

    goto :goto_1
.end method
