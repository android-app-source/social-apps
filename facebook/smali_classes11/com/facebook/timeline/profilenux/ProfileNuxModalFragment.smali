.class public Lcom/facebook/timeline/profilenux/ProfileNuxModalFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fuu;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/resources/ui/FbButton;

.field private e:Lcom/facebook/resources/ui/FbButton;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2310642
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2310643
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2310644
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/timeline/profilenux/ProfileNuxModalFragment;

    const/16 v0, 0x365f

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/timeline/profilenux/ProfileNuxModalFragment;->a:LX/0Or;

    .line 2310645
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 13

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x592eb9e8

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v11

    .line 2310646
    const v0, 0x7f03106e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v12

    .line 2310647
    const v0, 0x7f0d2758

    invoke-static {v12, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/timeline/profilenux/ProfileNuxModalFragment;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2310648
    const v0, 0x7f0d2759

    invoke-static {v12, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/timeline/profilenux/ProfileNuxModalFragment;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2310649
    const v0, 0x7f0d275a

    invoke-static {v12, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/timeline/profilenux/ProfileNuxModalFragment;->d:Lcom/facebook/resources/ui/FbButton;

    .line 2310650
    const v0, 0x7f0d275b

    invoke-static {v12, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/timeline/profilenux/ProfileNuxModalFragment;->e:Lcom/facebook/resources/ui/FbButton;

    .line 2310651
    iget-object v0, p0, Lcom/facebook/timeline/profilenux/ProfileNuxModalFragment;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fuu;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "refresher_configuration"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "profile_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/timeline/profilenux/ProfileNuxModalFragment;->e:Lcom/facebook/resources/ui/FbButton;

    iget-object v4, p0, Lcom/facebook/timeline/profilenux/ProfileNuxModalFragment;->d:Lcom/facebook/resources/ui/FbButton;

    iget-object v5, p0, Lcom/facebook/timeline/profilenux/ProfileNuxModalFragment;->b:Lcom/facebook/resources/ui/FbTextView;

    iget-object v6, p0, Lcom/facebook/timeline/profilenux/ProfileNuxModalFragment;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "nux_modal_title"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "nux_modal_text"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v10

    invoke-virtual/range {v0 .. v10}, LX/Fuu;->a(Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;Ljava/lang/String;Lcom/facebook/resources/ui/FbButton;Lcom/facebook/resources/ui/FbButton;Lcom/facebook/resources/ui/FbTextView;Lcom/facebook/resources/ui/FbTextView;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;Landroid/app/Activity;)V

    .line 2310652
    const/4 v0, 0x2

    const/16 v1, 0x2b

    const v2, 0x2a2bf196

    invoke-static {v0, v1, v2, v11}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v12
.end method
