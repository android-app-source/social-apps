.class public Lcom/facebook/timeline/profilenux/ProfileNuxModalActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2310641
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2310626
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2310627
    const v0, 0x7f0400c8

    const v1, 0x7f0400ca

    invoke-virtual {p0, v0, v1}, Lcom/facebook/timeline/profilenux/ProfileNuxModalActivity;->overridePendingTransition(II)V

    .line 2310628
    const v0, 0x7f03106d

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/profilenux/ProfileNuxModalActivity;->setContentView(I)V

    .line 2310629
    if-nez p1, :cond_0

    .line 2310630
    invoke-virtual {p0}, Lcom/facebook/timeline/profilenux/ProfileNuxModalActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2310631
    new-instance v1, Lcom/facebook/timeline/profilenux/ProfileNuxModalFragment;

    invoke-direct {v1}, Lcom/facebook/timeline/profilenux/ProfileNuxModalFragment;-><init>()V

    .line 2310632
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2310633
    const-string v3, "nux_modal_title"

    const-string p1, "nux_modal_title"

    invoke-virtual {v0, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2310634
    const-string v3, "nux_modal_text"

    const-string p1, "nux_modal_text"

    invoke-virtual {v0, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2310635
    const-string v3, "profile_id"

    const-string p1, "profile_id"

    invoke-virtual {v0, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2310636
    const-string v3, "refresher_configuration"

    const-string p1, "refresher_configuration"

    invoke-virtual {v0, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2310637
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2310638
    move-object v0, v1

    .line 2310639
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d2757

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2310640
    :cond_0
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2310623
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 2310624
    :goto_0
    return-void

    .line 2310625
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    goto :goto_0
.end method
