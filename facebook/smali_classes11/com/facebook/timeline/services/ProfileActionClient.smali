.class public Lcom/facebook/timeline/services/ProfileActionClient;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/0aG;

.field public final c:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2317672
    const-class v0, Lcom/facebook/timeline/services/ProfileActionClient;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/services/ProfileActionClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0aG;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2317679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2317680
    iput-object p1, p0, Lcom/facebook/timeline/services/ProfileActionClient;->b:LX/0aG;

    .line 2317681
    iput-object p2, p0, Lcom/facebook/timeline/services/ProfileActionClient;->c:Ljava/util/concurrent/ExecutorService;

    .line 2317682
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/services/ProfileActionClient;
    .locals 1

    .prologue
    .line 2317683
    invoke-static {p0}, Lcom/facebook/timeline/services/ProfileActionClient;->b(LX/0QB;)Lcom/facebook/timeline/services/ProfileActionClient;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)Lcom/facebook/timeline/services/ProfileActionClient;
    .locals 3

    .prologue
    .line 2317677
    new-instance v2, Lcom/facebook/timeline/services/ProfileActionClient;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v2, v0, v1}, Lcom/facebook/timeline/services/ProfileActionClient;-><init>(LX/0aG;Ljava/util/concurrent/ExecutorService;)V

    .line 2317678
    return-object v2
.end method


# virtual methods
.method public final a(JJ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2317673
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2317674
    const-string v0, "pokeUser"

    new-instance v1, Lcom/facebook/timeline/profileprotocol/PokeUserMethod$Params;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/facebook/timeline/profileprotocol/PokeUserMethod$Params;-><init>(JJ)V

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2317675
    iget-object v0, p0, Lcom/facebook/timeline/services/ProfileActionClient;->b:LX/0aG;

    const-string v1, "profile_poke_user"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v4, Lcom/facebook/timeline/services/ProfileActionClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, 0x76a5c0bb

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2317676
    const/4 v1, 0x0

    invoke-static {v1}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/services/ProfileActionClient;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
