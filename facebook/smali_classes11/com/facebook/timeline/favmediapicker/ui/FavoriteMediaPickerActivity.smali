.class public Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2299266
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 2299257
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2299258
    new-instance v1, LX/74k;

    invoke-direct {v1}, LX/74k;-><init>()V

    new-instance v2, LX/4gN;

    invoke-direct {v2}, LX/4gN;-><init>()V

    new-instance v3, LX/4gP;

    invoke-direct {v3}, LX/4gP;-><init>()V

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4gP;->a(Ljava/lang/String;)LX/4gP;

    move-result-object v3

    sget-object v4, LX/4gQ;->Photo:LX/4gQ;

    invoke-virtual {v3, v4}, LX/4gP;->a(LX/4gQ;)LX/4gP;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/4gP;->a(Landroid/net/Uri;)LX/4gP;

    move-result-object v3

    invoke-virtual {v3}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v2

    invoke-virtual {v2}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v2

    .line 2299259
    iput-object v2, v1, LX/74k;->f:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 2299260
    move-object v1, v1

    .line 2299261
    invoke-virtual {v1}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v1

    .line 2299262
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2299263
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2299264
    const-string v1, "extra_media_items"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2299265
    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2299253
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2299254
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2299255
    new-instance v1, LX/Ftp;

    invoke-direct {v1, p0}, LX/Ftp;-><init>(Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2299256
    return-void
.end method

.method private static b(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 2299244
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2299245
    new-instance v1, LX/74m;

    invoke-direct {v1}, LX/74m;-><init>()V

    new-instance v2, LX/4gN;

    invoke-direct {v2}, LX/4gN;-><init>()V

    new-instance v3, LX/4gP;

    invoke-direct {v3}, LX/4gP;-><init>()V

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4gP;->a(Ljava/lang/String;)LX/4gP;

    move-result-object v3

    sget-object v4, LX/4gQ;->Video:LX/4gQ;

    invoke-virtual {v3, v4}, LX/4gP;->a(LX/4gQ;)LX/4gP;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/4gP;->a(Landroid/net/Uri;)LX/4gP;

    move-result-object v3

    invoke-virtual {v3}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v2

    invoke-virtual {v2}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v2

    .line 2299246
    iput-object v2, v1, LX/74m;->e:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 2299247
    move-object v1, v1

    .line 2299248
    invoke-virtual {v1}, LX/74m;->a()Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v1

    .line 2299249
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2299250
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2299251
    const-string v1, "extra_media_items"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2299252
    return-object v0
.end method

.method private static b(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2299241
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2299242
    const-string v1, "suggested_media_fb_id"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2299243
    return-object v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2299235
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2299236
    const v0, 0x7f0305f4

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;->setContentView(I)V

    .line 2299237
    invoke-direct {p0}, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;->a()V

    .line 2299238
    if-nez p1, :cond_0

    .line 2299239
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d105f

    new-instance v2, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;

    invoke-direct {v2}, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2299240
    :cond_0
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 2299217
    if-eq p2, v2, :cond_0

    .line 2299218
    :goto_0
    return-void

    .line 2299219
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 2299220
    :pswitch_0
    invoke-virtual {p0, v2, p3}, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 2299221
    invoke-virtual {p0}, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;->finish()V

    goto :goto_0

    .line 2299222
    :pswitch_1
    const-string v0, "creative_cam_result_extra"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/creativecam/CreativeCamResult;

    .line 2299223
    iget-object v1, v0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 2299224
    if-eqz v1, :cond_2

    .line 2299225
    iget-object v1, v0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->b:Landroid/net/Uri;

    move-object v0, v1

    .line 2299226
    invoke-static {v0}, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;->a(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 2299227
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;->finish()V

    goto :goto_0

    .line 2299228
    :cond_2
    iget-object v1, v0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->a:Landroid/net/Uri;

    move-object v1, v1

    .line 2299229
    if-eqz v1, :cond_1

    .line 2299230
    iget-object v1, v0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->a:Landroid/net/Uri;

    move-object v0, v1

    .line 2299231
    invoke-static {v0}, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;->b(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_1

    .line 2299232
    :pswitch_2
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediaSetActivity;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2299233
    invoke-static {v0}, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;->b(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 2299234
    invoke-virtual {p0}, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
