.class public Lcom/facebook/timeline/favmediapicker/ui/views/FavoriteMediaPickerVideoView;
.super LX/2oW;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2299382
    invoke-direct {p0, p1}, LX/2oW;-><init>(Landroid/content/Context;)V

    .line 2299383
    invoke-direct {p0}, Lcom/facebook/timeline/favmediapicker/ui/views/FavoriteMediaPickerVideoView;->c()V

    .line 2299384
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2299368
    invoke-direct {p0, p1, p2}, LX/2oW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2299369
    invoke-direct {p0}, Lcom/facebook/timeline/favmediapicker/ui/views/FavoriteMediaPickerVideoView;->c()V

    .line 2299370
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2299379
    invoke-direct {p0, p1, p2, p3}, LX/2oW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2299380
    invoke-direct {p0}, Lcom/facebook/timeline/favmediapicker/ui/views/FavoriteMediaPickerVideoView;->c()V

    .line 2299381
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 2299375
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 2299376
    const/4 v0, 0x0

    .line 2299377
    iput-boolean v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->C:Z

    .line 2299378
    return-void
.end method


# virtual methods
.method public getDefaultPlayerOrigin()LX/04D;
    .locals 1

    .prologue
    .line 2299374
    sget-object v0, LX/04D;->PROFILE_FAVORITE_MEDIA_PICKER:LX/04D;

    return-object v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 2299371
    invoke-super {p0}, LX/2oW;->h()V

    .line 2299372
    sget-object v0, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 2299373
    return-void
.end method
