.class public Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1b0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/FtW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1Db;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/FtU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/Ftz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/FtF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/1Rq;

.field private m:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/media/util/model/MediaModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public n:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2299314
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static e(Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;)V
    .locals 3

    .prologue
    .line 2299315
    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->i:LX/Ftz;

    .line 2299316
    iget-object v1, v0, LX/Ftz;->e:LX/0TD;

    new-instance v2, LX/Fty;

    invoke-direct {v2, v0}, LX/Fty;-><init>(LX/Ftz;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2299317
    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->m:LX/0Vd;

    iget-object v2, p0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2299318
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2299319
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2299320
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/1az;->a(LX/0QB;)LX/1az;

    move-result-object v6

    check-cast v6, LX/1b0;

    const-class v7, LX/0i4;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/0i4;

    const-class v8, LX/FtW;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/FtW;

    invoke-static {v0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v9

    check-cast v9, LX/1Db;

    invoke-static {v0}, LX/FtU;->a(LX/0QB;)LX/FtU;

    move-result-object v10

    check-cast v10, LX/FtU;

    invoke-static {v0}, LX/Ftz;->a(LX/0QB;)LX/Ftz;

    move-result-object v11

    check-cast v11, LX/Ftz;

    invoke-static {v0}, Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;->a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;

    move-result-object p1

    check-cast p1, Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;

    invoke-static {v0}, LX/FtF;->a(LX/0QB;)LX/FtF;

    move-result-object v0

    check-cast v0, LX/FtF;

    iput-object v3, v2, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->a:Ljava/util/concurrent/Executor;

    iput-object v4, v2, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->b:LX/1Ck;

    iput-object v5, v2, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->c:LX/0ad;

    iput-object v6, v2, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->d:LX/1b0;

    iput-object v7, v2, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->e:LX/0i4;

    iput-object v8, v2, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->f:LX/FtW;

    iput-object v9, v2, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->g:LX/1Db;

    iput-object v10, v2, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->h:LX/FtU;

    iput-object v11, v2, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->i:LX/Ftz;

    iput-object p1, v2, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->j:Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;

    iput-object v0, v2, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->k:LX/FtF;

    .line 2299321
    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->h:LX/FtU;

    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->f:LX/FtW;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2299322
    new-instance v3, LX/FtV;

    invoke-static {v1}, LX/1QB;->b(LX/0QB;)LX/1QB;

    move-result-object v5

    check-cast v5, LX/1QB;

    invoke-static {v1}, LX/99Q;->a(LX/0QB;)LX/99Q;

    move-result-object v6

    check-cast v6, LX/99Q;

    invoke-static {v1}, LX/1QF;->b(LX/0QB;)LX/1QF;

    move-result-object v7

    check-cast v7, LX/1QF;

    const-class v4, LX/1Q6;

    invoke-interface {v1, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/1Q6;

    move-object v4, v2

    invoke-direct/range {v3 .. v8}, LX/FtV;-><init>(Landroid/content/Context;LX/1QB;LX/99Q;LX/1QF;LX/1Q6;)V

    .line 2299323
    move-object v1, v3

    .line 2299324
    iget-object v2, v0, LX/FtU;->a:LX/1DS;

    iget-object v3, v0, LX/FtU;->b:LX/0Ot;

    iget-object v4, v0, LX/FtU;->c:LX/FtF;

    invoke-virtual {v2, v3, v4}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v2

    .line 2299325
    iput-object v1, v2, LX/1Ql;->f:LX/1PW;

    .line 2299326
    move-object v2, v2

    .line 2299327
    invoke-virtual {v2}, LX/1Ql;->d()LX/1Rq;

    move-result-object v2

    move-object v0, v2

    .line 2299328
    iput-object v0, p0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->l:LX/1Rq;

    .line 2299329
    new-instance v0, LX/Ftq;

    invoke-direct {v0, p0}, LX/Ftq;-><init>(Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;)V

    move-object v0, v0

    .line 2299330
    iput-object v0, p0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->m:LX/0Vd;

    .line 2299331
    new-instance v0, LX/Ftr;

    invoke-direct {v0, p0}, LX/Ftr;-><init>(Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;)V

    move-object v0, v0

    .line 2299332
    iput-object v0, p0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->n:LX/0Vd;

    .line 2299333
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 2299334
    invoke-static {p0}, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->e(Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;)V

    .line 2299335
    :goto_0
    const/4 v0, 0x0

    .line 2299336
    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->j:Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;

    invoke-virtual {v1, v0}, Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->n:LX/0Vd;

    iget-object v3, p0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->a:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2299337
    return-void

    .line 2299338
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->e:LX/0i4;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v3, v1, v2

    new-instance v2, LX/Fts;

    invoke-direct {v2, p0}, LX/Fts;-><init>(Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x5540f43d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2299339
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2299340
    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->c:LX/0ad;

    sget-short v2, LX/0wf;->F:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2299341
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0d00bc

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/0h5;

    .line 2299342
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const p1, 0x7f021839

    invoke-static {v3, p1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 2299343
    iput-object v3, v2, LX/108;->b:Landroid/graphics/drawable/Drawable;

    .line 2299344
    move-object v2, v2

    .line 2299345
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p1, 0x7f081369

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2299346
    iput-object v3, v2, LX/108;->j:Ljava/lang/String;

    .line 2299347
    move-object v2, v2

    .line 2299348
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2299349
    invoke-interface {v1, v2}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2299350
    new-instance v2, LX/Ftt;

    invoke-direct {v2, p0}, LX/Ftt;-><init>(Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;)V

    invoke-interface {v1, v2}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2299351
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x4d1ae409    # 1.62414736E8f

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x2ab1bc95

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2299352
    const v1, 0x7f0305f5

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2299353
    const v2, 0x102000a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2299354
    new-instance v3, LX/1P0;

    invoke-virtual {v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v3, p1}, LX/1P0;-><init>(Landroid/content/Context;)V

    .line 2299355
    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2299356
    invoke-virtual {v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->l()V

    .line 2299357
    new-instance p1, LX/Ftu;

    invoke-direct {p1, p0}, LX/Ftu;-><init>(Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;)V

    move-object p1, p1

    .line 2299358
    invoke-virtual {v2, p1}, Landroid/support/v7/widget/RecyclerView;->setRecyclerListener(LX/1OU;)V

    .line 2299359
    new-instance p1, LX/Ftv;

    invoke-direct {p1, p0, v3}, LX/Ftv;-><init>(Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;LX/1P1;)V

    move-object v3, p1

    .line 2299360
    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2299361
    new-instance v3, LX/1ON;

    iget-object p1, p0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->l:LX/1Rq;

    invoke-direct {v3, p1}, LX/1ON;-><init>(LX/1OO;)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2299362
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setEmptyView(Landroid/view/View;)V

    .line 2299363
    const/16 v2, 0x2b

    const v3, 0x4d04b9d1    # 1.39173136E8f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4b1ce683    # 1.0282627E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2299364
    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->m:LX/0Vd;

    invoke-virtual {v1}, LX/0Vd;->dispose()V

    .line 2299365
    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->n:LX/0Vd;

    invoke-virtual {v1}, LX/0Vd;->dispose()V

    .line 2299366
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2299367
    const/16 v1, 0x2b

    const v2, 0x6eabb31a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
