.class public Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/FtE;",
        "Ljava/lang/Void;",
        "Lcom/facebook/timeline/favmediapicker/rows/environments/FavoriteMediaPickerEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;

.field private final b:Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;

.field private final c:Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Landroid/content/res/Resources;

.field public final f:LX/0ad;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;LX/0Or;Landroid/content/res/Resources;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;",
            "Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;",
            "Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "Landroid/content/res/Resources;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2298971
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2298972
    iput-object p1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;->a:Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;

    .line 2298973
    iput-object p2, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;->b:Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;

    .line 2298974
    iput-object p3, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;->c:Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;

    .line 2298975
    iput-object p4, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;->d:LX/0Or;

    .line 2298976
    iput-object p5, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;->e:Landroid/content/res/Resources;

    .line 2298977
    iput-object p6, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;->f:LX/0ad;

    .line 2298978
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;
    .locals 10

    .prologue
    .line 2298979
    const-class v1, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;

    monitor-enter v1

    .line 2298980
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2298981
    sput-object v2, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2298982
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2298983
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2298984
    new-instance v3, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;

    const/16 v7, 0x455

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;-><init>(Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;LX/0Or;Landroid/content/res/Resources;LX/0ad;)V

    .line 2298985
    move-object v0, v3

    .line 2298986
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2298987
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2298988
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2298989
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2298990
    check-cast p2, LX/FtE;

    const/4 v0, 0x0

    .line 2298991
    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;->a:Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;

    .line 2298992
    new-instance v2, LX/Fti;

    iget-object v3, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;->e:Landroid/content/res/Resources;

    const p3, 0x7f08340a

    invoke-virtual {v3, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance p3, LX/Ftd;

    invoke-direct {p3, p0}, LX/Ftd;-><init>(Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;)V

    invoke-direct {v2, v3, p3}, LX/Fti;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    move-object v2, v2

    .line 2298993
    invoke-virtual {p1, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2298994
    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;->f:LX/0ad;

    sget-short v2, LX/0wf;->F:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2298995
    :goto_0
    mul-int/lit8 v1, v0, 0x3

    .line 2298996
    iget-object v2, p2, LX/FtE;->a:Ljava/util/List;

    move-object v2, v2

    .line 2298997
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 2298998
    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;->c:Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;

    new-instance v2, LX/FtY;

    invoke-direct {v2, p2, v0}, LX/FtY;-><init>(LX/FtE;I)V

    invoke-virtual {p1, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2298999
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2299000
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;->b:Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299001
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2299002
    check-cast p1, LX/FtE;

    const/4 v0, 0x0

    .line 2299003
    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;->f:LX/0ad;

    sget-short v2, LX/0wf;->F:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2299004
    iget-object v1, p1, LX/FtE;->a:Ljava/util/List;

    move-object v1, v1

    .line 2299005
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method
