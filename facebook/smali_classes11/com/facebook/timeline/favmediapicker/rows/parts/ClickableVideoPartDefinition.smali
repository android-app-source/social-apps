.class public Lcom/facebook/timeline/favmediapicker/rows/parts/ClickableVideoPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/Ftg;",
        "LX/2pa;",
        "Lcom/facebook/timeline/favmediapicker/rows/environments/FavoriteMediaPickerEnvironment;",
        "Lcom/facebook/widget/CustomFrameLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2299054
    new-instance v0, LX/Ftf;

    const v1, 0x7f0305fa

    invoke-direct {v0, v1}, LX/Ftf;-><init>(I)V

    sput-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickableVideoPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2299051
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 2299052
    iput-object p1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickableVideoPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2299053
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/ClickableVideoPartDefinition;
    .locals 4

    .prologue
    .line 2299016
    const-class v1, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickableVideoPartDefinition;

    monitor-enter v1

    .line 2299017
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickableVideoPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2299018
    sput-object v2, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickableVideoPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2299019
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2299020
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2299021
    new-instance p0, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickableVideoPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickableVideoPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V

    .line 2299022
    move-object v0, p0

    .line 2299023
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2299024
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickableVideoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2299025
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2299026
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2299050
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickableVideoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2299032
    check-cast p2, LX/Ftg;

    .line 2299033
    new-instance v0, LX/2oE;

    invoke-direct {v0}, LX/2oE;-><init>()V

    iget-object v1, p2, LX/Ftg;->a:Landroid/net/Uri;

    .line 2299034
    iput-object v1, v0, LX/2oE;->a:Landroid/net/Uri;

    .line 2299035
    move-object v0, v0

    .line 2299036
    sget-object v1, LX/097;->FROM_STREAM:LX/097;

    .line 2299037
    iput-object v1, v0, LX/2oE;->e:LX/097;

    .line 2299038
    move-object v0, v0

    .line 2299039
    invoke-virtual {v0}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v0

    .line 2299040
    new-instance v1, LX/2oH;

    invoke-direct {v1}, LX/2oH;-><init>()V

    invoke-virtual {v1, v0}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v0

    const/4 v1, 0x1

    .line 2299041
    iput-boolean v1, v0, LX/2oH;->g:Z

    .line 2299042
    move-object v0, v0

    .line 2299043
    invoke-virtual {v0}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    .line 2299044
    new-instance v1, LX/2pZ;

    invoke-direct {v1}, LX/2pZ;-><init>()V

    .line 2299045
    iput-object v0, v1, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 2299046
    move-object v0, v1

    .line 2299047
    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    .line 2299048
    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickableVideoPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v2, p2, LX/Ftg;->b:Landroid/view/View$OnClickListener;

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2299049
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x42aa3985

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2299027
    check-cast p2, LX/2pa;

    check-cast p4, Lcom/facebook/widget/CustomFrameLayout;

    .line 2299028
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/widget/CustomFrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/favmediapicker/ui/views/FavoriteMediaPickerVideoView;

    .line 2299029
    invoke-virtual {v1, p2}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 2299030
    sget-object v2, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 2299031
    const/16 v1, 0x1f

    const v2, 0x635668df    # 3.95516E21f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
