.class public Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/timeline/favmediapicker/rows/environments/FavoriteMediaPickerEnvironment;",
        ":",
        "LX/1Pp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/0jW;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field public final c:Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;

.field private final d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "Lcom/facebook/timeline/favmediapicker/rows/environments/FavoriteMediaPickerEnvironment;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

.field public final f:LX/2dq;

.field public final g:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2299172
    new-instance v0, LX/Ftl;

    invoke-direct {v0}, LX/Ftl;-><init>()V

    sput-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->a:LX/0jW;

    .line 2299173
    const-class v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;

    const-string v1, "favorite_media_picker"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;LX/2dq;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2299136
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2299137
    iput-object p1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->c:Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;

    .line 2299138
    iput-object p2, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2299139
    iput-object p3, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->e:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    .line 2299140
    iput-object p4, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->f:LX/2dq;

    .line 2299141
    iput-object p5, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->g:Landroid/content/res/Resources;

    .line 2299142
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;
    .locals 9

    .prologue
    .line 2299161
    const-class v1, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;

    monitor-enter v1

    .line 2299162
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2299163
    sput-object v2, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2299164
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2299165
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2299166
    new-instance v3, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v7

    check-cast v7, LX/2dq;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;-><init>(Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;LX/2dq;Landroid/content/res/Resources;)V

    .line 2299167
    move-object v0, v3

    .line 2299168
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2299169
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2299170
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2299171
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2299160
    sget-object v0, LX/2eA;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2299147
    check-cast p2, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;

    .line 2299148
    invoke-virtual {p2}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;->k()Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel$MediaListModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2299149
    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-virtual {p2}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;->k()Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel$MediaListModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel$MediaListModel;->a()LX/0Px;

    move-result-object v1

    .line 2299150
    iget-object v2, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->f:LX/2dq;

    iget-object v3, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->g:Landroid/content/res/Resources;

    const v4, 0x7f0b2292

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    iget-object v4, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->g:Landroid/content/res/Resources;

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v3, v4

    const/high16 v4, 0x41000000    # 8.0f

    add-float/2addr v3, v4

    invoke-static {}, LX/1UY;->c()LX/1UY;

    move-result-object v4

    invoke-virtual {v4}, LX/1UY;->i()LX/1Ua;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v3

    .line 2299151
    new-instance v2, LX/2eG;

    const/4 v4, 0x0

    .line 2299152
    new-instance v5, LX/Ftm;

    invoke-direct {v5, p0, v1}, LX/Ftm;-><init>(Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;LX/0Px;)V

    move-object v5, v5

    .line 2299153
    sget-object v6, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->a:LX/0jW;

    invoke-interface {v6}, LX/0jW;->g()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->a:LX/0jW;

    invoke-direct/range {v2 .. v7}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    move-object v1, v2

    .line 2299154
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2299155
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->e:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    .line 2299156
    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->g:Landroid/content/res/Resources;

    const v2, 0x7f0b2293

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2299157
    new-instance v2, LX/1ds;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->g:Landroid/content/res/Resources;

    const v5, 0x7f0b2294

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-direct {v2, v1, v3, v1, v4}, LX/1ds;-><init>(IIII)V

    move-object v1, v2

    .line 2299158
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2299159
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x559b84fd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2299144
    check-cast p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2299145
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p4}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const p0, 0x7f0a00d5

    invoke-static {v2, p0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p4, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2299146
    const/16 v1, 0x1f

    const v2, -0x67ea7b64

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2299143
    const/4 v0, 0x1

    return v0
.end method
