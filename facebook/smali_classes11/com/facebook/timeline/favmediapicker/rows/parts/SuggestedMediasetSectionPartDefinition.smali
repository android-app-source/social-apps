.class public Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;",
        "Ljava/lang/Void;",
        "Lcom/facebook/timeline/favmediapicker/rows/environments/FavoriteMediaPickerEnvironment;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;

.field private final d:Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;

.field private final e:Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;

.field public final f:Ljava/lang/String;

.field private final g:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2299201
    const-class v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;

    const-string v1, "favorite_media_picker"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;LX/0Or;LX/0ad;)V
    .locals 1
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;",
            "Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;",
            "Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2299193
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2299194
    iput-object p1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;->b:LX/0Or;

    .line 2299195
    iput-object p2, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;->c:Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;

    .line 2299196
    iput-object p3, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;->d:Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;

    .line 2299197
    iput-object p4, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;->e:Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;

    .line 2299198
    invoke-interface {p5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;->f:Ljava/lang/String;

    .line 2299199
    iput-object p6, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;->g:LX/0ad;

    .line 2299200
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;
    .locals 10

    .prologue
    .line 2299202
    const-class v1, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;

    monitor-enter v1

    .line 2299203
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2299204
    sput-object v2, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2299205
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2299206
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2299207
    new-instance v3, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;

    const/16 v4, 0x455

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;

    const/16 v8, 0x15e7

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;-><init>(LX/0Or;Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;LX/0Or;LX/0ad;)V

    .line 2299208
    move-object v0, v3

    .line 2299209
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2299210
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2299211
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2299212
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2299179
    check-cast p2, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 2299180
    invoke-virtual {p2}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_1

    move-object v0, v1

    .line 2299181
    :goto_0
    iget-object v3, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;->c:Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;

    new-instance v4, LX/Fti;

    .line 2299182
    invoke-virtual {p2}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;->k()Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel$MediaListModel;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {p2}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;->k()Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel$MediaListModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel$MediaListModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    const/4 p3, 0x6

    if-ge v5, p3, :cond_4

    .line 2299183
    :cond_0
    const/4 v5, 0x0

    .line 2299184
    :goto_1
    move-object v5, v5

    .line 2299185
    invoke-direct {v4, v0, v5}, LX/Fti;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1, v3, v4}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299186
    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;->g:LX/0ad;

    sget-short v3, LX/0wf;->F:S

    invoke-interface {v0, v3, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 2299187
    :goto_2
    mul-int/lit8 v2, v0, 0x3

    invoke-virtual {p2}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;->k()Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel$MediaListModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel$MediaListModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 2299188
    iget-object v2, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;->e:Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;

    new-instance v3, LX/Ftk;

    invoke-direct {v3, p2, v0}, LX/Ftk;-><init>(Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;I)V

    invoke-virtual {p1, v2, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299189
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2299190
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;->l()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2299191
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;->d:Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299192
    :cond_3
    return-object v1

    :cond_4
    new-instance v5, LX/Fto;

    invoke-direct {v5, p0, p2}, LX/Fto;-><init>(Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2299177
    check-cast p1, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;

    .line 2299178
    invoke-virtual {p1}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;->k()Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel$MediaListModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;->k()Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel$MediaListModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel$MediaListModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
