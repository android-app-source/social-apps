.class public Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/FtE;",
        "Ljava/lang/Void;",
        "Lcom/facebook/timeline/favmediapicker/rows/environments/FavoriteMediaPickerEnvironment;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/0jW;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static j:LX/0Xm;


# instance fields
.field public final c:Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;

.field public final d:Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;

.field public final e:Lcom/facebook/timeline/favmediapicker/rows/parts/ClickableVideoPartDefinition;

.field private final f:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "Lcom/facebook/timeline/favmediapicker/rows/environments/FavoriteMediaPickerEnvironment;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

.field public final h:LX/2dq;

.field public final i:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2298922
    new-instance v0, LX/FtZ;

    invoke-direct {v0}, LX/FtZ;-><init>()V

    sput-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->a:LX/0jW;

    .line 2298923
    const-class v0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;

    const-string v1, "favorite_media_picker"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;Lcom/facebook/timeline/favmediapicker/rows/parts/ClickableVideoPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;LX/2dq;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2298924
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2298925
    iput-object p1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->c:Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;

    .line 2298926
    iput-object p2, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->d:Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;

    .line 2298927
    iput-object p3, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->e:Lcom/facebook/timeline/favmediapicker/rows/parts/ClickableVideoPartDefinition;

    .line 2298928
    iput-object p4, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->f:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2298929
    iput-object p5, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->g:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    .line 2298930
    iput-object p6, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->h:LX/2dq;

    .line 2298931
    iput-object p7, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->i:Landroid/content/res/Resources;

    .line 2298932
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;
    .locals 11

    .prologue
    .line 2298933
    const-class v1, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;

    monitor-enter v1

    .line 2298934
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2298935
    sput-object v2, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2298936
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2298937
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2298938
    new-instance v3, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickableVideoPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/ClickableVideoPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickableVideoPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v9

    check-cast v9, LX/2dq;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v10

    check-cast v10, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;-><init>(Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;Lcom/facebook/timeline/favmediapicker/rows/parts/ClickableVideoPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;LX/2dq;Landroid/content/res/Resources;)V

    .line 2298939
    move-object v0, v3

    .line 2298940
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2298941
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2298942
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2298943
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2298944
    sget-object v0, LX/2eA;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2298945
    check-cast p2, LX/FtE;

    .line 2298946
    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->f:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2298947
    iget-object v1, p2, LX/FtE;->a:Ljava/util/List;

    move-object v1, v1

    .line 2298948
    iget-object v2, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->h:LX/2dq;

    iget-object v3, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->i:Landroid/content/res/Resources;

    const v4, 0x7f0b2292

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    iget-object v4, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->i:Landroid/content/res/Resources;

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v3, v4

    const/high16 v4, 0x41000000    # 8.0f

    add-float/2addr v3, v4

    invoke-static {}, LX/1UY;->c()LX/1UY;

    move-result-object v4

    invoke-virtual {v4}, LX/1UY;->i()LX/1Ua;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v3

    .line 2298949
    new-instance v2, LX/2eG;

    const/4 v4, 0x0

    .line 2298950
    new-instance v5, LX/Fta;

    invoke-direct {v5, p0, v1}, LX/Fta;-><init>(Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;Ljava/util/List;)V

    move-object v5, v5

    .line 2298951
    sget-object v6, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->a:LX/0jW;

    invoke-interface {v6}, LX/0jW;->g()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->a:LX/0jW;

    invoke-direct/range {v2 .. v7}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    move-object v1, v2

    .line 2298952
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2298953
    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->g:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    .line 2298954
    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->i:Landroid/content/res/Resources;

    const v2, 0x7f0b2293

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2298955
    new-instance v2, LX/1ds;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->i:Landroid/content/res/Resources;

    const v5, 0x7f0b2294

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-direct {v2, v1, v3, v1, v4}, LX/1ds;-><init>(IIII)V

    move-object v1, v2

    .line 2298956
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2298957
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x548cdd1d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2298958
    check-cast p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2298959
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p4}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const p0, 0x7f0a00d5

    invoke-static {v2, p0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p4, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2298960
    const/16 v1, 0x1f

    const v2, 0x619ed0b9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2298961
    const/4 v0, 0x1

    return v0
.end method
