.class public Lcom/facebook/timeline/favmediapicker/rows/parts/FavoriteMediaPickerRootGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Lcom/facebook/timeline/favmediapicker/rows/environments/FavoriteMediaPickerEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/1T5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1T5",
            "<",
            "Lcom/facebook/timeline/favmediapicker/rows/environments/FavoriteMediaPickerEnvironment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedForYouLabelPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/favmediapicker/rows/parts/LoadingIndicatorPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2299055
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2299056
    invoke-static {}, LX/1T5;->a()LX/1T5;

    move-result-object v0

    const-class v1, LX/FtE;

    invoke-virtual {v0, v1, p1}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, LX/FtG;

    invoke-virtual {v0, v1, p2}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;

    invoke-virtual {v0, v1, p3}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel$PageInfoModel;

    invoke-virtual {v0, v1, p4}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/FavoriteMediaPickerRootGroupPartDefinition;->a:LX/1T5;

    .line 2299057
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/FavoriteMediaPickerRootGroupPartDefinition;
    .locals 7

    .prologue
    .line 2299058
    const-class v1, Lcom/facebook/timeline/favmediapicker/rows/parts/FavoriteMediaPickerRootGroupPartDefinition;

    monitor-enter v1

    .line 2299059
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/FavoriteMediaPickerRootGroupPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2299060
    sput-object v2, Lcom/facebook/timeline/favmediapicker/rows/parts/FavoriteMediaPickerRootGroupPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2299061
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2299062
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2299063
    new-instance v3, Lcom/facebook/timeline/favmediapicker/rows/parts/FavoriteMediaPickerRootGroupPartDefinition;

    const/16 v4, 0x364b

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1253

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x364f

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x1250

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, v6, p0}, Lcom/facebook/timeline/favmediapicker/rows/parts/FavoriteMediaPickerRootGroupPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2299064
    move-object v0, v3

    .line 2299065
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2299066
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/favmediapicker/rows/parts/FavoriteMediaPickerRootGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2299067
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2299068
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2299069
    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/FavoriteMediaPickerRootGroupPartDefinition;->a:LX/1T5;

    invoke-virtual {v0, p1, p2}, LX/1T5;->a(LX/1RF;Ljava/lang/Object;)Z

    .line 2299070
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2299071
    const/4 v0, 0x1

    return v0
.end method
