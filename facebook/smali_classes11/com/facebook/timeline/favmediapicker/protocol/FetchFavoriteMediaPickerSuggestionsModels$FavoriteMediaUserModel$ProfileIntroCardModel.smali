.class public final Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4940400c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2298357
    const-class v0, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2298356
    const-class v0, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2298354
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2298355
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2298348
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2298349
    invoke-virtual {p0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel;->a()Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2298350
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2298351
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2298352
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2298353
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2298340
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2298341
    invoke-virtual {p0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel;->a()Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2298342
    invoke-virtual {p0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel;->a()Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel;

    .line 2298343
    invoke-virtual {p0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel;->a()Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2298344
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel;

    .line 2298345
    iput-object v0, v1, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel;->e:Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel;

    .line 2298346
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2298347
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFeaturedMediasetsSuggestions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2298338
    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel;->e:Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel;

    iput-object v0, p0, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel;->e:Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel;

    .line 2298339
    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel;->e:Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2298335
    new-instance v0, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel;

    invoke-direct {v0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel;-><init>()V

    .line 2298336
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2298337
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2298334
    const v0, -0x7aa2bf55

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2298333
    const v0, 0x7d57e813

    return v0
.end method
