.class public Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field private final b:LX/Fu1;

.field private final c:LX/0tX;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2299439
    const-class v0, Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;

    const-string v1, "favorite_media_picker"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/Fu1;LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2299440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2299441
    iput-object p1, p0, Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;->b:LX/Fu1;

    .line 2299442
    iput-object p2, p0, Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;->c:LX/0tX;

    .line 2299443
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;
    .locals 5

    .prologue
    .line 2299444
    const-class v1, Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;

    monitor-enter v1

    .line 2299445
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2299446
    sput-object v2, Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2299447
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2299448
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2299449
    new-instance p0, Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;

    invoke-static {v0}, LX/Fu1;->a(LX/0QB;)LX/Fu1;

    move-result-object v3

    check-cast v3, LX/Fu1;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-direct {p0, v3, v4}, Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;-><init>(LX/Fu1;LX/0tX;)V

    .line 2299450
    move-object v0, p0

    .line 2299451
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2299452
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2299453
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2299454
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2299455
    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;->b:LX/Fu1;

    invoke-virtual {v0}, LX/Fu1;->a()I

    move-result v0

    .line 2299456
    new-instance v1, LX/FtH;

    invoke-direct {v1}, LX/FtH;-><init>()V

    move-object v1, v1

    .line 2299457
    const-string v2, "photo_size"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2299458
    const-string v2, "mediaset_count"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2299459
    if-eqz p1, :cond_0

    .line 2299460
    const-string v2, "cursor"

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2299461
    :cond_0
    move-object v0, v1

    .line 2299462
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2299463
    iput-object v1, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2299464
    move-object v0, v0

    .line 2299465
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    const-wide/32 v2, 0x15180

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-string v1, "com.facebook.timeline.favmediapicker.utils.favoriteMediaSuggestionsCacheTag"

    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    .line 2299466
    iput-object v1, v0, LX/0zO;->d:Ljava/util/Set;

    .line 2299467
    move-object v0, v0

    .line 2299468
    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/Fu0;

    invoke-direct {v1, p0}, LX/Fu0;-><init>(Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;)V

    .line 2299469
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 2299470
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
