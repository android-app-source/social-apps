.class public Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/G2o;",
        "Landroid/graphics/drawable/Drawable;",
        "LX/1PW;",
        "Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2314727
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2314728
    iput-object p1, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;->a:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2314729
    iput-object p2, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;->b:Landroid/content/res/Resources;

    .line 2314730
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;
    .locals 5

    .prologue
    .line 2314716
    const-class v1, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;

    monitor-enter v1

    .line 2314717
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2314718
    sput-object v2, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2314719
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2314720
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2314721
    new-instance p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4}, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;Landroid/content/res/Resources;)V

    .line 2314722
    move-object v0, p0

    .line 2314723
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2314724
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2314725
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2314726
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2314731
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2314714
    const v0, 0x7f0d27a4

    iget-object v1, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;->a:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;->b:Landroid/content/res/Resources;

    const v3, 0x7f080f77

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2314715
    iget-object v0, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;->b:Landroid/content/res/Resources;

    const v1, 0x7f02142c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3e0ab6bf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2314710
    check-cast p2, Landroid/graphics/drawable/Drawable;

    check-cast p4, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;

    .line 2314711
    invoke-virtual {p4, p2}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2314712
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->setFocusable(Z)V

    .line 2314713
    const/16 v1, 0x1f

    const v2, 0x4c2fdb1b    # 4.6099564E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2314709
    const/4 v0, 0x1

    return v0
.end method
