.class public Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/G2v;",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        "TE;",
        "Lcom/facebook/friends/ui/SmartButtonLite;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/23P;

.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

.field private final d:Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

.field public final e:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/23P;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2314609
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2314610
    iput-object p1, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->a:LX/23P;

    .line 2314611
    iput-object p2, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2314612
    iput-object p3, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->c:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    .line 2314613
    iput-object p4, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->d:Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

    .line 2314614
    iput-object p5, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->e:Landroid/content/res/Resources;

    .line 2314615
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;
    .locals 9

    .prologue
    .line 2314637
    const-class v1, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;

    monitor-enter v1

    .line 2314638
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2314639
    sput-object v2, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2314640
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2314641
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2314642
    new-instance v3, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v4

    check-cast v4, LX/23P;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;-><init>(LX/23P;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;Landroid/content/res/Resources;)V

    .line 2314643
    move-object v0, v3

    .line 2314644
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2314645
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2314646
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2314647
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Z
    .locals 1

    .prologue
    .line 2314648
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2314621
    check-cast p2, LX/G2v;

    check-cast p3, LX/1Pc;

    .line 2314622
    new-instance v1, LX/2en;

    iget-object v0, p2, LX/G2v;->a:Ljava/lang/String;

    iget-object v2, p2, LX/G2v;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {v1, v0, v2}, LX/2en;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    move-object v0, p3

    .line 2314623
    check-cast v0, LX/1Pr;

    iget-object v2, p2, LX/G2v;->d:LX/0jW;

    invoke-interface {v0, v1, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2ep;

    iget-object v0, v0, LX/2ep;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2314624
    invoke-static {v0}, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2314625
    const/4 v10, 0x0

    const/4 v7, 0x0

    .line 2314626
    sget-object v5, LX/G2u;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 2314627
    new-instance v5, LX/2f5;

    move-object v6, v7

    move v8, v10

    move-object v9, v7

    move v11, v10

    invoke-direct/range {v5 .. v11}, LX/2f5;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/Integer;II)V

    :goto_0
    move-object v2, v5

    .line 2314628
    iget-object v3, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->c:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    iget-object v4, v2, LX/2f5;->a:Ljava/lang/CharSequence;

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2314629
    iget-object v3, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2314630
    new-instance v4, LX/G2t;

    invoke-direct {v4, p0, p3, v1, p2}, LX/G2t;-><init>(Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;LX/1Pc;LX/2en;LX/G2v;)V

    move-object v1, v4

    .line 2314631
    invoke-interface {p1, v3, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2314632
    iget-object v1, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->d:Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2314633
    :cond_0
    return-object v0

    .line 2314634
    :pswitch_0
    new-instance v5, LX/2f5;

    iget-object v6, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->a:LX/23P;

    iget-object v8, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->e:Landroid/content/res/Resources;

    const v9, 0x7f080f7b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8, v7}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v6

    iget-object v8, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->a:LX/23P;

    iget-object v9, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->e:Landroid/content/res/Resources;

    const v11, 0x7f080f7c

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v7}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v7

    const v8, 0x7f020b77

    iget-object v9, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->e:Landroid/content/res/Resources;

    const v11, 0x7f0a00d5

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const v11, 0x7f020b08

    invoke-direct/range {v5 .. v11}, LX/2f5;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/Integer;II)V

    goto :goto_0

    .line 2314635
    :pswitch_1
    new-instance v5, LX/2f5;

    iget-object v6, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->a:LX/23P;

    iget-object v8, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->e:Landroid/content/res/Resources;

    const v9, 0x7f080f7d

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8, v7}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v6

    iget-object v8, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->a:LX/23P;

    iget-object v9, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->e:Landroid/content/res/Resources;

    const v11, 0x7f080017

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v7}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v7

    const v8, 0x7f02089f

    iget-object v9, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->e:Landroid/content/res/Resources;

    const v11, 0x7f0a00a3

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const v11, 0x7f020b0b

    invoke-direct/range {v5 .. v11}, LX/2f5;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/Integer;II)V

    goto/16 :goto_0

    .line 2314636
    :pswitch_2
    new-instance v5, LX/2f5;

    iget-object v6, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->a:LX/23P;

    iget-object v8, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->e:Landroid/content/res/Resources;

    const v9, 0x7f080f76

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8, v7}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v6

    const v8, 0x7f020b7c

    iget-object v9, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->e:Landroid/content/res/Resources;

    const v11, 0x7f0a00a3

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const v11, 0x7f020b0b

    invoke-direct/range {v5 .. v11}, LX/2f5;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/Integer;II)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1d9a08f6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2314616
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    check-cast p4, Lcom/facebook/friends/ui/SmartButtonLite;

    .line 2314617
    invoke-static {p2}, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2314618
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 2314619
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x50a2f4a2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2314620
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    goto :goto_0
.end method
