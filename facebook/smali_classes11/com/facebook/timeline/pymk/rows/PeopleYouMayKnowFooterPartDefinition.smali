.class public Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/G2o;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/timeline/protiles/views/ProtilesFooterView;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final b:LX/17W;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17W;Lcom/facebook/multirow/parts/TextPartDefinition;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2314567
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2314568
    iput-object p1, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2314569
    iput-object p2, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->b:LX/17W;

    .line 2314570
    iput-object p3, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2314571
    iput-object p4, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->d:Landroid/content/res/Resources;

    .line 2314572
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;
    .locals 7

    .prologue
    .line 2314578
    const-class v1, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;

    monitor-enter v1

    .line 2314579
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2314580
    sput-object v2, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2314581
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2314582
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2314583
    new-instance p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17W;Lcom/facebook/multirow/parts/TextPartDefinition;Landroid/content/res/Resources;)V

    .line 2314584
    move-object v0, p0

    .line 2314585
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2314586
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2314587
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2314588
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/timeline/protiles/views/ProtilesFooterView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2314577
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2314574
    const v0, 0x7f0d279d

    iget-object v1, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->d:Landroid/content/res/Resources;

    const v3, 0x7f080fb2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2314575
    iget-object v0, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/G2r;

    invoke-direct {v1, p0}, LX/G2r;-><init>(Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2314576
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2314573
    const/4 v0, 0x1

    return v0
.end method
