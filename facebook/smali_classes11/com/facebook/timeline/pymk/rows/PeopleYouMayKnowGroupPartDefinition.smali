.class public Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/G2o;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;

.field private final b:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;

.field private final c:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2314649
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2314650
    iput-object p1, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowGroupPartDefinition;->a:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;

    .line 2314651
    iput-object p2, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowGroupPartDefinition;->b:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;

    .line 2314652
    iput-object p3, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowGroupPartDefinition;->c:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    .line 2314653
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowGroupPartDefinition;
    .locals 6

    .prologue
    .line 2314654
    const-class v1, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowGroupPartDefinition;

    monitor-enter v1

    .line 2314655
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2314656
    sput-object v2, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2314657
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2314658
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2314659
    new-instance p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowGroupPartDefinition;-><init>(Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;)V

    .line 2314660
    move-object v0, p0

    .line 2314661
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2314662
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2314663
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2314664
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2314665
    check-cast p2, LX/G2o;

    .line 2314666
    iget-object v0, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowGroupPartDefinition;->b:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2314667
    iget-object v0, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowGroupPartDefinition;->c:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2314668
    iget-object v0, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowGroupPartDefinition;->a:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2314669
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2314670
    const/4 v0, 0x1

    return v0
.end method
