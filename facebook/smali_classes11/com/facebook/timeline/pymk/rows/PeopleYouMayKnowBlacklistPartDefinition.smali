.class public Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/G2q;",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        "TE;",
        "Lcom/facebook/friends/ui/SmartButtonLite;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:LX/23P;

.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final c:LX/2dj;

.field public final d:LX/2do;

.field private final e:Landroid/content/res/Resources;

.field private final f:Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;


# direct methods
.method public constructor <init>(LX/23P;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/2dj;LX/2do;Landroid/content/res/Resources;Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2314534
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2314535
    iput-object p1, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->a:LX/23P;

    .line 2314536
    iput-object p2, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2314537
    iput-object p3, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->c:LX/2dj;

    .line 2314538
    iput-object p4, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->d:LX/2do;

    .line 2314539
    iput-object p5, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->e:Landroid/content/res/Resources;

    .line 2314540
    iput-object p6, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->f:Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

    .line 2314541
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;
    .locals 10

    .prologue
    .line 2314542
    const-class v1, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;

    monitor-enter v1

    .line 2314543
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2314544
    sput-object v2, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2314545
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2314546
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2314547
    new-instance v3, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v4

    check-cast v4, LX/23P;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/2dj;->b(LX/0QB;)LX/2dj;

    move-result-object v6

    check-cast v6, LX/2dj;

    invoke-static {v0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v7

    check-cast v7, LX/2do;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;-><init>(LX/23P;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/2dj;LX/2do;Landroid/content/res/Resources;Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;)V

    .line 2314548
    move-object v0, v3

    .line 2314549
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2314550
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2314551
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2314552
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2314553
    check-cast p2, LX/G2q;

    check-cast p3, LX/1Pr;

    const/4 v2, 0x0

    .line 2314554
    iget-object v0, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v1, p2, LX/G2q;->a:Ljava/lang/String;

    iget-object v3, p2, LX/G2q;->c:LX/G2x;

    .line 2314555
    new-instance v4, LX/G2p;

    invoke-direct {v4, p0, v3, v1}, LX/G2p;-><init>(Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;LX/G2x;Ljava/lang/String;)V

    move-object v1, v4

    .line 2314556
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2314557
    iget-object v7, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->f:Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

    new-instance v0, LX/2f5;

    iget-object v1, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->a:LX/23P;

    iget-object v3, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->e:Landroid/content/res/Resources;

    const v4, 0x7f08186b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    const v3, 0x7f02081c

    iget-object v4, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->e:Landroid/content/res/Resources;

    const v5, 0x7f0a00a3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    const v6, 0x7f020b0b

    invoke-direct/range {v0 .. v6}, LX/2f5;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/Integer;II)V

    invoke-interface {p1, v7, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2314558
    new-instance v0, LX/2en;

    iget-object v1, p2, LX/G2q;->a:Ljava/lang/String;

    iget-object v2, p2, LX/G2q;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {v0, v1, v2}, LX/2en;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    iget-object v1, p2, LX/G2q;->d:LX/0jW;

    invoke-interface {p3, v0, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2ep;

    iget-object v0, v0, LX/2ep;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x70e93e15

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2314559
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    check-cast p4, Lcom/facebook/friends/ui/SmartButtonLite;

    .line 2314560
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne p2, v1, :cond_0

    .line 2314561
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 2314562
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x9ad141

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2314563
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    goto :goto_0
.end method
