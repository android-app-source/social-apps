.class public Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/BP1;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->TIMELINE_COVERPHOTO_FRAGMENT:LX/0cQ;
.end annotation


# static fields
.field private static final k:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;",
            ">;"
        }
    .end annotation
.end field

.field private static final l:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/2Qx;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/G13;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/FsP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/Fwh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/Fx5;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private m:Ljava/lang/String;

.field private n:LX/434;

.field private o:J

.field private p:LX/BP0;

.field public q:LX/BQ1;

.field private r:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<*>;>;"
        }
    .end annotation
.end field

.field private s:LX/FrQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2295503
    const-class v0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;

    .line 2295504
    sput-object v0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->k:Ljava/lang/Class;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->l:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2295500
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2295501
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->o:J

    .line 2295502
    return-void
.end method

.method private a(JZ)V
    .locals 7

    .prologue
    .line 2295494
    iget-object v1, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->f:LX/G13;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->i:LX/Fwh;

    invoke-virtual {v0}, LX/Fwh;->h()Z

    move-result v5

    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->j:LX/Fx5;

    invoke-virtual {v0}, LX/Fx5;->h()Z

    move-result v6

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, LX/G13;->a(JLX/0am;ZZ)Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;

    move-result-object v1

    .line 2295495
    if-eqz p3, :cond_0

    sget-object v0, LX/0zS;->d:LX/0zS;

    .line 2295496
    :goto_0
    iget-object v2, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->g:LX/FsP;

    sget-object v3, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->l:Lcom/facebook/common/callercontext/CallerContext;

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v2, v1, v0, v3, v4}, LX/FsP;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;LX/0zS;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 2295497
    iget-object v1, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->d:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->r:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2295498
    return-void

    .line 2295499
    :cond_0
    sget-object v0, LX/0zS;->a:LX/0zS;

    goto :goto_0
.end method

.method public static b$redex0(Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;)V
    .locals 14

    .prologue
    .line 2295482
    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->q:LX/BQ1;

    .line 2295483
    iget-object v1, v0, LX/BPy;->e:LX/BPx;

    move-object v0, v1

    .line 2295484
    sget-object v1, LX/BPx;->UNINITIALIZED:LX/BPx;

    if-eq v0, v1, :cond_0

    .line 2295485
    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->s:LX/FrQ;

    iget-object v1, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->p:LX/BP0;

    iget-object v2, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->q:LX/BQ1;

    .line 2295486
    iget-object v3, v0, LX/FrQ;->c:LX/FvA;

    .line 2295487
    iget-object v4, v1, LX/5SB;->c:Ljava/lang/String;

    move-object v4, v4

    .line 2295488
    iget-wide v12, v1, LX/5SB;->b:J

    move-wide v5, v12

    .line 2295489
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v2}, LX/FvA;->a(Ljava/lang/String;Ljava/lang/Long;LX/BQ1;)LX/Fv9;

    move-result-object v9

    .line 2295490
    iget-object v3, v0, LX/FrQ;->b:LX/Fv5;

    invoke-virtual {v0}, LX/FrQ;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {}, LX/FrI;->a()LX/FrH;

    move-result-object v6

    new-instance v8, LX/G4m;

    invoke-direct {v8}, LX/G4m;-><init>()V

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    const/4 v11, 0x1

    move-object v5, v1

    move-object v7, v2

    invoke-virtual/range {v3 .. v11}, LX/Fv5;->a(Landroid/content/Context;LX/BP0;LX/FrH;LX/BQ1;LX/G4m;LX/Fv9;Ljava/lang/Boolean;Z)LX/FvG;

    move-result-object v3

    iput-object v3, v0, LX/FrQ;->g:LX/FvG;

    .line 2295491
    iget-object v3, v0, LX/FrQ;->f:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v4, LX/1UT;

    iget-object v5, v0, LX/FrQ;->g:LX/FvG;

    iget-object v6, v0, LX/FrQ;->f:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-direct {v4, v5, v6}, LX/1UT;-><init>(LX/1Cw;Landroid/support/v7/widget/RecyclerView;)V

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2295492
    iget-object v3, v0, LX/FrQ;->f:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    new-instance v4, LX/FrP;

    invoke-direct {v4, v0}, LX/FrP;-><init>(LX/FrQ;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2295493
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;
    .locals 8

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 2295442
    new-instance v0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;

    const-wide/16 v1, -0x1

    iget-object v3, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->m:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->s:LX/FrQ;

    .line 2295443
    iget-object v7, v4, LX/FrQ;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    move-object v4, v7

    .line 2295444
    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->s:LX/FrQ;

    .line 2295445
    iget-object v5, v4, LX/FrQ;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    move-object v4, v5

    .line 2295446
    invoke-virtual {v4}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->getNormalizedCropBounds()Landroid/graphics/RectF;

    move-result-object v4

    :goto_0
    iget-object v5, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->n:LX/434;

    iget-wide v6, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->o:J

    invoke-direct/range {v0 .. v7}, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;-><init>(JLjava/lang/String;Landroid/graphics/RectF;LX/434;J)V

    return-object v0

    :cond_0
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v5, v5, v6, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    .line 2295465
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2295466
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v6, v0

    .line 2295467
    const-string v0, "cover_photo_uri"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->m:Ljava/lang/String;

    .line 2295468
    const-string v0, "cover_photo_id"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->o:J

    .line 2295469
    const-string v0, "profile_id"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 2295470
    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, v8, v0

    if-nez v0, :cond_1

    .line 2295471
    :cond_0
    sget-object v0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->k:Ljava/lang/Class;

    const-string v1, "Missing required arguments."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2295472
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2295473
    :goto_0
    return-void

    .line 2295474
    :cond_1
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/2Qx;->a(LX/0QB;)LX/2Qx;

    move-result-object v4

    check-cast v4, LX/2Qx;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/G13;->a(LX/0QB;)LX/G13;

    move-result-object v11

    check-cast v11, LX/G13;

    invoke-static {v0}, LX/FsP;->a(LX/0QB;)LX/FsP;

    move-result-object v12

    check-cast v12, LX/FsP;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    invoke-static {v0}, LX/Fwh;->a(LX/0QB;)LX/Fwh;

    move-result-object p1

    check-cast p1, LX/Fwh;

    invoke-static {v0}, LX/Fx5;->a(LX/0QB;)LX/Fx5;

    move-result-object v0

    check-cast v0, LX/Fx5;

    iput-object v3, v2, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->a:LX/03V;

    iput-object v4, v2, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->b:LX/2Qx;

    iput-object v5, v2, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->c:Ljava/lang/String;

    iput-object v7, v2, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->d:LX/0tX;

    iput-object v10, v2, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->e:Ljava/util/concurrent/ExecutorService;

    iput-object v11, v2, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->f:LX/G13;

    iput-object v12, v2, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->g:LX/FsP;

    iput-object v13, v2, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->h:LX/0ad;

    iput-object p1, v2, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->i:LX/Fwh;

    iput-object v0, v2, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->j:LX/Fx5;

    .line 2295475
    :try_start_0
    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 2295476
    :goto_1
    const/4 v4, 0x0

    new-instance v5, Landroid/os/ParcelUuid;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-direct {v5, v2}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    move-wide v2, v0

    invoke-static/range {v0 .. v5}, LX/BP0;->a(JJLjava/lang/String;Landroid/os/ParcelUuid;)LX/BP0;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->p:LX/BP0;

    .line 2295477
    new-instance v0, LX/BQ1;

    iget-object v1, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->p:LX/BP0;

    invoke-direct {v0, v1}, LX/BQ1;-><init>(LX/5SB;)V

    iput-object v0, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->q:LX/BQ1;

    .line 2295478
    const-string v0, "force_refresh"

    const/4 v1, 0x0

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 2295479
    invoke-direct {p0, v8, v9, v0}, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->a(JZ)V

    goto/16 :goto_0

    .line 2295480
    :catch_0
    const-wide/16 v0, -0x1

    .line 2295481
    iget-object v2, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->a:LX/03V;

    const-string v3, "timeline_invalid_meuser"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "logged in user: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x7c26acb7

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2295455
    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->m:Ljava/lang/String;

    invoke-static {v0}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->n:LX/434;

    .line 2295456
    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->n:LX/434;

    invoke-static {v0}, LX/8K7;->a(LX/434;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2295457
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0815be

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2295458
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/Activity;->setResult(I)V

    .line 2295459
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2295460
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->h:LX/0ad;

    sget-short v2, LX/0wf;->W:S

    invoke-interface {v0, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e05b0

    invoke-direct {v0, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 2295461
    :goto_0
    new-instance v2, LX/FrQ;

    iget-object v3, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->m:Ljava/lang/String;

    invoke-direct {v2, v3, v0}, LX/FrQ;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->s:LX/FrQ;

    .line 2295462
    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->r:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/FrO;

    invoke-direct {v2, p0}, LX/FrO;-><init>(Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;)V

    iget-object v3, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2295463
    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->s:LX/FrQ;

    const v2, 0x5bc243ac

    invoke-static {v2, v1}, LX/02F;->f(II)V

    return-object v0

    .line 2295464
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3bcb9f34

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2295447
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2295448
    iget-object v1, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->s:LX/FrQ;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->s:LX/FrQ;

    .line 2295449
    iget-object v2, v1, LX/FrQ;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    move-object v1, v2

    .line 2295450
    if-eqz v1, :cond_0

    .line 2295451
    iget-object v1, p0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->s:LX/FrQ;

    .line 2295452
    iget-object v2, v1, LX/FrQ;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    move-object v1, v2

    .line 2295453
    invoke-virtual {v1}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->a()V

    .line 2295454
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x6be760de

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
