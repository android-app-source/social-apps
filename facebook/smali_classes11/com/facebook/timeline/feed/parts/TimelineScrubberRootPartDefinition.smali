.class public Lcom/facebook/timeline/feed/parts/TimelineScrubberRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/G52;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;

.field private final b:Lcom/facebook/timeline/feed/parts/TimelineScrubberLoadingIndicatorPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/feed/parts/TimelineScrubberLoadingIndicatorPartDefinition;Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2299816
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2299817
    iput-object p2, p0, Lcom/facebook/timeline/feed/parts/TimelineScrubberRootPartDefinition;->a:Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;

    .line 2299818
    iput-object p1, p0, Lcom/facebook/timeline/feed/parts/TimelineScrubberRootPartDefinition;->b:Lcom/facebook/timeline/feed/parts/TimelineScrubberLoadingIndicatorPartDefinition;

    .line 2299819
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/feed/parts/TimelineScrubberRootPartDefinition;
    .locals 5

    .prologue
    .line 2299801
    const-class v1, Lcom/facebook/timeline/feed/parts/TimelineScrubberRootPartDefinition;

    monitor-enter v1

    .line 2299802
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelineScrubberRootPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2299803
    sput-object v2, Lcom/facebook/timeline/feed/parts/TimelineScrubberRootPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2299804
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2299805
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2299806
    new-instance p0, Lcom/facebook/timeline/feed/parts/TimelineScrubberRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/feed/parts/TimelineScrubberLoadingIndicatorPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/feed/parts/TimelineScrubberLoadingIndicatorPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/timeline/feed/parts/TimelineScrubberLoadingIndicatorPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/timeline/feed/parts/TimelineScrubberRootPartDefinition;-><init>(Lcom/facebook/timeline/feed/parts/TimelineScrubberLoadingIndicatorPartDefinition;Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;)V

    .line 2299807
    move-object v0, p0

    .line 2299808
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2299809
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/feed/parts/TimelineScrubberRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2299810
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2299811
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2299813
    check-cast p2, LX/G52;

    .line 2299814
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/TimelineScrubberRootPartDefinition;->b:Lcom/facebook/timeline/feed/parts/TimelineScrubberLoadingIndicatorPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/timeline/feed/parts/TimelineScrubberRootPartDefinition;->a:Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2299815
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2299812
    const/4 v0, 0x1

    return v0
.end method
