.class public Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/G4z;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;

.field private final b:Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2299653
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2299654
    iput-object p2, p0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorRootPartDefinition;->a:Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;

    .line 2299655
    iput-object p1, p0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorRootPartDefinition;->b:Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;

    .line 2299656
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorRootPartDefinition;
    .locals 5

    .prologue
    .line 2299657
    const-class v1, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorRootPartDefinition;

    monitor-enter v1

    .line 2299658
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorRootPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2299659
    sput-object v2, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorRootPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2299660
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2299661
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2299662
    new-instance p0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorRootPartDefinition;-><init>(Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;)V

    .line 2299663
    move-object v0, p0

    .line 2299664
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2299665
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2299666
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2299667
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2299668
    check-cast p2, LX/G4z;

    .line 2299669
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorRootPartDefinition;->b:Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorRootPartDefinition;->a:Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2299670
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2299671
    const/4 v0, 0x1

    return v0
.end method
