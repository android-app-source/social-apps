.class public Lcom/facebook/timeline/feed/parts/TimelineFeedUnitRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "LX/Ft9;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/1T5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1T5",
            "<",
            "LX/Ft9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/feed/parts/TimelineScrubberRootPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorRootPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/feed/parts/TimelineNoStoriesComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/feed/parts/TimelinePostsLabelComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2299586
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2299587
    invoke-static {}, LX/1T5;->a()LX/1T5;

    move-result-object v0

    const-class v1, Lcom/facebook/graphql/model/HideableUnit;

    .line 2299588
    move-object v2, p4

    .line 2299589
    invoke-virtual {v0, v1, v2}, LX/1T5;->b(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v1, p3}, LX/1T5;->b(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, LX/G52;

    invoke-virtual {v0, v1, p1}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, LX/G4z;

    invoke-virtual {v0, v1, p2}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, LX/G50;

    .line 2299590
    move-object v2, p5

    .line 2299591
    invoke-virtual {v0, v1, v2}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, LX/G54;

    .line 2299592
    move-object v2, p6

    .line 2299593
    invoke-virtual {v0, v1, v2}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v0, v1, p7}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/feed/parts/TimelineFeedUnitRootPartDefinition;->a:LX/1T5;

    .line 2299594
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/feed/parts/TimelineFeedUnitRootPartDefinition;
    .locals 11

    .prologue
    .line 2299575
    const-class v1, Lcom/facebook/timeline/feed/parts/TimelineFeedUnitRootPartDefinition;

    monitor-enter v1

    .line 2299576
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelineFeedUnitRootPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2299577
    sput-object v2, Lcom/facebook/timeline/feed/parts/TimelineFeedUnitRootPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2299578
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2299579
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2299580
    new-instance v3, Lcom/facebook/timeline/feed/parts/TimelineFeedUnitRootPartDefinition;

    const/16 v4, 0x3659

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x3657

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x3655

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x956

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1258

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1259

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x6fa

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, Lcom/facebook/timeline/feed/parts/TimelineFeedUnitRootPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2299581
    move-object v0, v3

    .line 2299582
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2299583
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/feed/parts/TimelineFeedUnitRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2299584
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2299585
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2299595
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/TimelineFeedUnitRootPartDefinition;->a:LX/1T5;

    invoke-virtual {v0, p1, p2}, LX/1T5;->a(LX/1RF;Ljava/lang/Object;)Z

    .line 2299596
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2299574
    const/4 v0, 0x1

    return v0
.end method
