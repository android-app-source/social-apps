.class public Lcom/facebook/timeline/feed/parts/TimelineScrubberLoadingIndicatorPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/G52;",
        "Ljava/lang/Void;",
        "LX/1Po;",
        "Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2299768
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2299769
    iput-object p1, p0, Lcom/facebook/timeline/feed/parts/TimelineScrubberLoadingIndicatorPartDefinition;->a:Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;

    .line 2299770
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/feed/parts/TimelineScrubberLoadingIndicatorPartDefinition;
    .locals 4

    .prologue
    .line 2299771
    const-class v1, Lcom/facebook/timeline/feed/parts/TimelineScrubberLoadingIndicatorPartDefinition;

    monitor-enter v1

    .line 2299772
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelineScrubberLoadingIndicatorPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2299773
    sput-object v2, Lcom/facebook/timeline/feed/parts/TimelineScrubberLoadingIndicatorPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2299774
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2299775
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2299776
    new-instance p0, Lcom/facebook/timeline/feed/parts/TimelineScrubberLoadingIndicatorPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/timeline/feed/parts/TimelineScrubberLoadingIndicatorPartDefinition;-><init>(Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;)V

    .line 2299777
    move-object v0, p0

    .line 2299778
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2299779
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/feed/parts/TimelineScrubberLoadingIndicatorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2299780
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2299781
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2299782
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2299783
    check-cast p2, LX/G52;

    .line 2299784
    check-cast p2, LX/G55;

    .line 2299785
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/TimelineScrubberLoadingIndicatorPartDefinition;->a:Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;

    iget-object v1, p2, LX/G55;->d:LX/G4z;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2299786
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2299787
    check-cast p1, LX/G52;

    const/4 v0, 0x0

    .line 2299788
    instance-of v1, p1, LX/G55;

    if-nez v1, :cond_1

    .line 2299789
    :cond_0
    :goto_0
    return v0

    .line 2299790
    :cond_1
    check-cast p1, LX/G55;

    .line 2299791
    invoke-virtual {p1}, LX/G52;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p1, LX/G55;->e:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
