.class public Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static C:LX/0Xm;


# instance fields
.field private final A:Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

.field private final B:Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;

.field private final a:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

.field private final b:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

.field private final d:Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;

.field private final j:Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field private final l:Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

.field private final m:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lcom/facebook/feed/rows/sections/TimelineStoryTextSelectorPartDefinition;

.field private final o:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Lcom/facebook/timeline/feed/parts/TimelineHeaderComponentPartDefinition;

.field private final q:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

.field private final r:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final s:Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;

.field private final t:Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

.field private final u:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

.field private final v:Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final w:Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final x:Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final y:Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;

.field private final z:Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/timeline/feed/parts/TimelineHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;Lcom/facebook/feed/rows/sections/TimelineStoryTextSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2299499
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2299500
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    .line 2299501
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    .line 2299502
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->c:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    .line 2299503
    iput-object p8, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->d:Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;

    .line 2299504
    iput-object p9, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->e:Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;

    .line 2299505
    iput-object p10, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;

    .line 2299506
    iput-object p11, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->g:Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;

    .line 2299507
    iput-object p12, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->h:Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;

    .line 2299508
    iput-object p13, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->i:Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;

    .line 2299509
    iput-object p14, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->j:Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;

    .line 2299510
    iput-object p6, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->k:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 2299511
    iput-object p7, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->l:Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

    .line 2299512
    iput-object p5, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->m:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    .line 2299513
    iput-object p4, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->n:Lcom/facebook/feed/rows/sections/TimelineStoryTextSelectorPartDefinition;

    .line 2299514
    iput-object p3, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->o:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    .line 2299515
    iput-object p2, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->p:Lcom/facebook/timeline/feed/parts/TimelineHeaderComponentPartDefinition;

    .line 2299516
    iput-object p1, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->q:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    .line 2299517
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->r:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    .line 2299518
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->s:Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;

    .line 2299519
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->t:Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

    .line 2299520
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->v:Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;

    .line 2299521
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->u:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    .line 2299522
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->w:Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;

    .line 2299523
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->x:Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;

    .line 2299524
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->y:Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;

    .line 2299525
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->z:Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;

    .line 2299526
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->A:Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    .line 2299527
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->B:Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;

    .line 2299528
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;
    .locals 3

    .prologue
    .line 2299529
    const-class v1, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;

    monitor-enter v1

    .line 2299530
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->C:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2299531
    sput-object v2, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->C:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2299532
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2299533
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->b(LX/0QB;)Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2299534
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2299535
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2299536
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;
    .locals 29

    .prologue
    .line 2299537
    new-instance v0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/timeline/feed/parts/TimelineHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/feed/parts/TimelineHeaderComponentPartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/timeline/feed/parts/TimelineHeaderComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/TimelineStoryTextSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/TimelineStoryTextSelectorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/TimelineStoryTextSelectorPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;

    move-result-object v13

    check-cast v13, Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;

    move-result-object v14

    check-cast v14, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    move-result-object v15

    check-cast v15, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    move-result-object v16

    check-cast v16, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    move-result-object v17

    check-cast v17, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    move-result-object v18

    check-cast v18, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;

    move-result-object v19

    check-cast v19, Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

    move-result-object v20

    check-cast v20, Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;

    move-result-object v21

    check-cast v21, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    move-result-object v22

    check-cast v22, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;

    move-result-object v23

    check-cast v23, Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;

    move-result-object v24

    check-cast v24, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;

    move-result-object v25

    check-cast v25, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;

    move-result-object v26

    check-cast v26, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    move-result-object v27

    check-cast v27, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;->a(LX/0QB;)Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;

    move-result-object v28

    check-cast v28, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;

    invoke-direct/range {v0 .. v28}, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/timeline/feed/parts/TimelineHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;Lcom/facebook/feed/rows/sections/TimelineStoryTextSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;)V

    .line 2299538
    return-object v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2299539
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2299540
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->z:Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299541
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->q:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299542
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->y:Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299543
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->p:Lcom/facebook/timeline/feed/parts/TimelineHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299544
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->A:Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299545
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->o:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299546
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->l:Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299547
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->n:Lcom/facebook/feed/rows/sections/TimelineStoryTextSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299548
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->m:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299549
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->k:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299550
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->w:Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299551
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->x:Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299552
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->d:Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299553
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->e:Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299554
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299555
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->g:Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299556
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->h:Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299557
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->i:Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299558
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->j:Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299559
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->t:Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299560
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->v:Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299561
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299562
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299563
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->c:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299564
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->B:Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299565
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->r:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299566
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->u:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299567
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->s:Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2299568
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V
    .locals 1

    .prologue
    .line 2299569
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, Ljava/lang/Void;

    check-cast p3, LX/1Pf;

    .line 2299570
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V

    .line 2299571
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->j:Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2299572
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2299573
    const/4 v0, 0x1

    return v0
.end method
