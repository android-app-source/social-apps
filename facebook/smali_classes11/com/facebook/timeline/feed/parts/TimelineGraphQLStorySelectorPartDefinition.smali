.class public Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;

.field private final b:Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/EdgeStoryGroupPartDefinition;

.field private final d:Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;

.field private final e:Lcom/facebook/timeline/feed/parts/TimelineSharedStoryPartDefinition;

.field private final f:Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;Lcom/facebook/feed/rows/sections/EdgeStoryGroupPartDefinition;Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;Lcom/facebook/timeline/feed/parts/TimelineSharedStoryPartDefinition;Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2299597
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2299598
    iput-object p1, p0, Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;->a:Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;

    .line 2299599
    iput-object p2, p0, Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;->b:Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;

    .line 2299600
    iput-object p3, p0, Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;->c:Lcom/facebook/feed/rows/sections/EdgeStoryGroupPartDefinition;

    .line 2299601
    iput-object p4, p0, Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;->d:Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;

    .line 2299602
    iput-object p5, p0, Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;->e:Lcom/facebook/timeline/feed/parts/TimelineSharedStoryPartDefinition;

    .line 2299603
    iput-object p6, p0, Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;->f:Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;

    .line 2299604
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;
    .locals 10

    .prologue
    .line 2299605
    const-class v1, Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;

    monitor-enter v1

    .line 2299606
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2299607
    sput-object v2, Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2299608
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2299609
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2299610
    new-instance v3, Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/EdgeStoryGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/EdgeStoryGroupPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/EdgeStoryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/feed/parts/TimelineSharedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/feed/parts/TimelineSharedStoryPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/timeline/feed/parts/TimelineSharedStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;Lcom/facebook/feed/rows/sections/EdgeStoryGroupPartDefinition;Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;Lcom/facebook/timeline/feed/parts/TimelineSharedStoryPartDefinition;Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;)V

    .line 2299611
    move-object v0, v3

    .line 2299612
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2299613
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2299614
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2299615
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2299616
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2299617
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;->a:Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;->b:Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;->c:Lcom/facebook/feed/rows/sections/EdgeStoryGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;->d:Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;->e:Lcom/facebook/timeline/feed/parts/TimelineSharedStoryPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/timeline/feed/parts/TimelineGraphQLStorySelectorPartDefinition;->f:Lcom/facebook/timeline/feed/parts/BasicTimelineGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2299618
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2299619
    const/4 v0, 0x1

    return v0
.end method
