.class public final Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/Object;

.field public final synthetic b:Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;

.field public final synthetic c:LX/0g8;

.field public final synthetic d:LX/Ft0;


# direct methods
.method public constructor <init>(LX/Ft0;Ljava/lang/Object;Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;LX/0g8;)V
    .locals 0

    .prologue
    .line 2297841
    iput-object p1, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;->d:LX/Ft0;

    iput-object p2, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;->a:Ljava/lang/Object;

    iput-object p3, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;->b:Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;

    iput-object p4, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;->c:LX/0g8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 2297832
    iget-object v0, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;->a:Ljava/lang/Object;

    sget-object v1, LX/Fvt;->FAVORITE_PHOTOS:LX/Fvt;

    if-ne v0, v1, :cond_1

    .line 2297833
    iget-object v0, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;->d:LX/Ft0;

    iget-object v1, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;->b:Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->getFavPhotosView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;->c:LX/0g8;

    invoke-static {v0, v1, v2}, LX/Ft0;->a$redex0(LX/Ft0;Landroid/view/View;LX/0g8;)V

    .line 2297834
    iget-object v0, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;->d:LX/Ft0;

    iget-object v0, v0, LX/Ft0;->i:LX/Fv9;

    iget-object v1, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;->b:Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->getFavPhotosView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Fv9;->a(Landroid/view/View;)V

    .line 2297835
    :cond_0
    :goto_0
    return-void

    .line 2297836
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;->a:Ljava/lang/Object;

    sget-object v1, LX/Fvt;->SUGGESTED_PHOTOS:LX/Fvt;

    if-ne v0, v1, :cond_2

    .line 2297837
    iget-object v0, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;->d:LX/Ft0;

    iget-object v1, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;->b:Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->getSuggestedPhotosView()Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;->c:LX/0g8;

    invoke-static {v0, v1, v2}, LX/Ft0;->b$redex0(LX/Ft0;Landroid/view/View;LX/0g8;)V

    .line 2297838
    iget-object v0, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;->d:LX/Ft0;

    iget-object v0, v0, LX/Ft0;->i:LX/Fv9;

    iget-object v1, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;->b:Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->getSuggestedPhotosView()Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Fv9;->a(Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;)V

    goto :goto_0

    .line 2297839
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;->a:Ljava/lang/Object;

    sget-object v1, LX/Fvt;->EMPTY:LX/Fvt;

    if-ne v0, v1, :cond_0

    .line 2297840
    iget-object v0, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;->d:LX/Ft0;

    iget-object v0, v0, LX/Ft0;->i:LX/Fv9;

    iget-object v1, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;->b:Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->getFavPhotosEmptyView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Fv9;->b(Landroid/view/View;)V

    goto :goto_0
.end method
