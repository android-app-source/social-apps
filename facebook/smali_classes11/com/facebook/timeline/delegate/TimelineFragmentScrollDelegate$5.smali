.class public final Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/header/TimelineIntroCardView;

.field public final synthetic b:LX/0g8;

.field public final synthetic c:LX/Ft0;


# direct methods
.method public constructor <init>(LX/Ft0;Lcom/facebook/timeline/header/TimelineIntroCardView;LX/0g8;)V
    .locals 0

    .prologue
    .line 2297827
    iput-object p1, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$5;->c:LX/Ft0;

    iput-object p2, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$5;->a:Lcom/facebook/timeline/header/TimelineIntroCardView;

    iput-object p3, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$5;->b:LX/0g8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 2297828
    iget-object v0, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$5;->c:LX/Ft0;

    iget-object v0, v0, LX/Ft0;->i:LX/Fv9;

    iget-object v1, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$5;->a:Lcom/facebook/timeline/header/TimelineIntroCardView;

    invoke-virtual {v0, v1}, LX/Fv9;->a(Lcom/facebook/timeline/header/TimelineIntroCardView;)V

    .line 2297829
    iget-object v0, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$5;->c:LX/Ft0;

    iget-object v1, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$5;->a:Lcom/facebook/timeline/header/TimelineIntroCardView;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->getFavPhotosView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$5;->b:LX/0g8;

    invoke-static {v0, v1, v2}, LX/Ft0;->a$redex0(LX/Ft0;Landroid/view/View;LX/0g8;)V

    .line 2297830
    iget-object v0, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$5;->c:LX/Ft0;

    iget-object v1, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$5;->a:Lcom/facebook/timeline/header/TimelineIntroCardView;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->getSuggestedPhotosView()Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$5;->b:LX/0g8;

    invoke-static {v0, v1, v2}, LX/Ft0;->b$redex0(LX/Ft0;Landroid/view/View;LX/0g8;)V

    .line 2297831
    return-void
.end method
