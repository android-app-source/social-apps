.class public final Lcom/facebook/timeline/profileprotocol/PokeUserMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/timeline/profileprotocol/PokeUserMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2310713
    new-instance v0, LX/G16;

    invoke-direct {v0}, LX/G16;-><init>()V

    sput-object v0, Lcom/facebook/timeline/profileprotocol/PokeUserMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 2310714
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2310715
    iput-wide p1, p0, Lcom/facebook/timeline/profileprotocol/PokeUserMethod$Params;->b:J

    .line 2310716
    iput-wide p3, p0, Lcom/facebook/timeline/profileprotocol/PokeUserMethod$Params;->a:J

    .line 2310717
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2310718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2310719
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/timeline/profileprotocol/PokeUserMethod$Params;->b:J

    .line 2310720
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/timeline/profileprotocol/PokeUserMethod$Params;->a:J

    .line 2310721
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2310722
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2310723
    iget-wide v0, p0, Lcom/facebook/timeline/profileprotocol/PokeUserMethod$Params;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2310724
    iget-wide v0, p0, Lcom/facebook/timeline/profileprotocol/PokeUserMethod$Params;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2310725
    return-void
.end method
