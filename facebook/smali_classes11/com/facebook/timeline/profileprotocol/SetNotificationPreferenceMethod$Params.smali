.class public final Lcom/facebook/timeline/profileprotocol/SetNotificationPreferenceMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/timeline/profileprotocol/SetNotificationPreferenceMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2310762
    new-instance v0, LX/G19;

    invoke-direct {v0}, LX/G19;-><init>()V

    sput-object v0, Lcom/facebook/timeline/profileprotocol/SetNotificationPreferenceMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 2310758
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2310759
    iput-wide p1, p0, Lcom/facebook/timeline/profileprotocol/SetNotificationPreferenceMethod$Params;->a:J

    .line 2310760
    iput-boolean p3, p0, Lcom/facebook/timeline/profileprotocol/SetNotificationPreferenceMethod$Params;->b:Z

    .line 2310761
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2310753
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2310754
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/timeline/profileprotocol/SetNotificationPreferenceMethod$Params;->a:J

    .line 2310755
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/timeline/profileprotocol/SetNotificationPreferenceMethod$Params;->b:Z

    .line 2310756
    return-void

    .line 2310757
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2310748
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2310749
    iget-wide v0, p0, Lcom/facebook/timeline/profileprotocol/SetNotificationPreferenceMethod$Params;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2310750
    iget-boolean v0, p0, Lcom/facebook/timeline/profileprotocol/SetNotificationPreferenceMethod$Params;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2310751
    return-void

    .line 2310752
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
