.class public Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/G3E;
.implements LX/G3P;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# static fields
.field private static final t:Landroid/view/animation/Interpolator;


# instance fields
.field public p:Lcom/facebook/user/model/User;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/63c;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:LX/G46;

.field private u:Lcom/facebook/timeline/refresher/ProfileRefresherView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3e800000    # 0.25f

    .line 2315986
    const v0, 0x3dcccccd    # 0.1f

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v2, v0, v2, v1}, LX/2pJ;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->t:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2316038
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;Lcom/facebook/user/model/User;LX/63c;LX/0ad;)V
    .locals 0

    .prologue
    .line 2316037
    iput-object p1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->p:Lcom/facebook/user/model/User;

    iput-object p2, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->q:LX/63c;

    iput-object p3, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->r:LX/0ad;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;

    invoke-static {v2}, LX/0XE;->b(LX/0QB;)Lcom/facebook/user/model/User;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    const-class v1, LX/63c;

    invoke-interface {v2, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/63c;

    invoke-static {v2}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->a(Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;Lcom/facebook/user/model/User;LX/63c;LX/0ad;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2315998
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2315999
    invoke-static {p0, p0}, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2316000
    new-instance v0, LX/G46;

    invoke-direct {v0}, LX/G46;-><init>()V

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->s:LX/G46;

    .line 2316001
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->s:LX/G46;

    sget-object v1, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->t:Landroid/view/animation/Interpolator;

    .line 2316002
    iput-object v1, v0, LX/G46;->a:Landroid/view/animation/Interpolator;

    .line 2316003
    const v0, 0x7f031080

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->setContentView(I)V

    .line 2316004
    const v0, 0x7f0d276d

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->u:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2316005
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->u:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    invoke-virtual {v0, p0}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->setListener(LX/G3P;)V

    .line 2316006
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->r:LX/0ad;

    sget-short v1, LX/0wf;->aC:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2316007
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->u:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->a()V

    .line 2316008
    :goto_0
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->u:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->p:Lcom/facebook/user/model/User;

    .line 2316009
    iget-object v2, v1, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v1, v2

    .line 2316010
    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->setName(Ljava/lang/String;)V

    .line 2316011
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->u:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2316012
    iget-object v1, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->f:Lcom/facebook/resources/ui/FbTextView;

    move-object v0, v1

    .line 2316013
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081616

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2316014
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->u:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2316015
    iget-object v1, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->g:Lcom/facebook/resources/ui/FbTextView;

    move-object v0, v1

    .line 2316016
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081617

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2316017
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->q:LX/63c;

    const v1, 0x7f080030

    invoke-virtual {v0, v1}, LX/63c;->a(I)LX/63b;

    move-result-object v0

    invoke-virtual {v0}, LX/63b;->a()LX/0Px;

    move-result-object v0

    .line 2316018
    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->u:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    invoke-virtual {v1, v0}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->setTitleBarButtonSpecs(Ljava/util/List;)V

    .line 2316019
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->u:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->u:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2316020
    iget-object v2, v1, Lcom/facebook/timeline/refresher/ProfileRefresherView;->v:LX/63W;

    move-object v1, v2

    .line 2316021
    invoke-virtual {v0, v1}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->setTitleBarButtonListener(LX/63W;)V

    .line 2316022
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->u:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2316023
    iget-object v1, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->e:Landroid/view/ViewStub;

    move-object v0, v1

    .line 2316024
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2316025
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2316026
    const-string v1, "extra_is_from_qp"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2316027
    new-instance v1, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    invoke-direct {v1}, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;-><init>()V

    .line 2316028
    if-nez v0, :cond_0

    .line 2316029
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2316030
    :cond_0
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2316031
    move-object v0, v1

    .line 2316032
    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->v:Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    .line 2316033
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d2772

    iget-object v2, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->v:Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2316034
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->s:LX/G46;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->u:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    iget-object v3, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->v:Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/G46;->a(LX/G3c;Lcom/facebook/timeline/refresher/ProfileRefresherView;Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;Z)V

    .line 2316035
    return-void

    .line 2316036
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->u:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->b()V

    goto/16 :goto_0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2315996
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 2315997
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 2315994
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2315995
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 2315992
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->v:Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->e()V

    .line 2315993
    return-void
.end method

.method public final k()V
    .locals 0

    .prologue
    .line 2315991
    return-void
.end method

.method public final l()V
    .locals 0

    .prologue
    .line 2315990
    return-void
.end method

.method public final li_()V
    .locals 1

    .prologue
    .line 2315988
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherProfileStepActivity;->v:Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->l()V

    .line 2315989
    return-void
.end method

.method public final o()LX/G3c;
    .locals 1

    .prologue
    .line 2315987
    const/4 v0, 0x0

    return-object v0
.end method
