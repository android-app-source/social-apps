.class public final Lcom/facebook/timeline/refresher/ProfileRefresherActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/G36;
.implements LX/G3C;
.implements LX/G3D;
.implements LX/G3E;
.implements LX/G3F;


# instance fields
.field public p:LX/G3R;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/G3K;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:LX/G3c;

.field private s:Lcom/facebook/timeline/refresher/ProfileRefresherView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:LX/G3Q;

.field private u:Landroid/os/Bundle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2315111
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/refresher/ProfileRefresherActivity;LX/G3R;LX/G3K;)V
    .locals 0

    .prologue
    .line 2315110
    iput-object p1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->p:LX/G3R;

    iput-object p2, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->q:LX/G3K;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;

    const-class v0, LX/G3R;

    invoke-interface {v1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/G3R;

    new-instance p1, LX/G3K;

    const-class v2, LX/63c;

    invoke-interface {v1, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/63c;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p1, v2, v3}, LX/G3K;-><init>(LX/63c;Landroid/content/res/Resources;)V

    move-object v1, p1

    check-cast v1, LX/G3K;

    invoke-static {p0, v0, v1}, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->a(Lcom/facebook/timeline/refresher/ProfileRefresherActivity;LX/G3R;LX/G3K;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2315107
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->t:LX/G3Q;

    .line 2315108
    invoke-static {v0}, LX/G3Q;->q(LX/G3Q;)V

    .line 2315109
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2315104
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->setResult(I)V

    .line 2315105
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->finish()V

    .line 2315106
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    .line 2315030
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2315031
    invoke-static {p0, p0}, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2315032
    iput-object p1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->u:Landroid/os/Bundle;

    .line 2315033
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "refresher_configuration"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;

    .line 2315034
    iget-object v1, v0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2315035
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2315036
    const-class v0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;

    const-string v1, "Profile id must be set"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2315037
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->setResult(I)V

    .line 2315038
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->finish()V

    .line 2315039
    :goto_0
    return-void

    .line 2315040
    :cond_0
    new-instance v1, LX/G3b;

    invoke-direct {v1}, LX/G3b;-><init>()V

    .line 2315041
    iget-object v2, v0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2315042
    iput-object v2, v1, LX/G3b;->a:Ljava/lang/String;

    .line 2315043
    move-object v1, v1

    .line 2315044
    iget-object v2, v0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2315045
    iput-object v2, v1, LX/G3b;->b:Ljava/lang/String;

    .line 2315046
    move-object v1, v1

    .line 2315047
    iget-object v2, v0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2315048
    iput-object v2, v1, LX/G3b;->c:Ljava/lang/String;

    .line 2315049
    move-object v1, v1

    .line 2315050
    iget-object v2, v0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2315051
    iput-object v2, v1, LX/G3b;->d:Ljava/lang/String;

    .line 2315052
    move-object v1, v1

    .line 2315053
    iget-boolean v2, v0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->e:Z

    move v2, v2

    .line 2315054
    iput-boolean v2, v1, LX/G3b;->e:Z

    .line 2315055
    move-object v1, v1

    .line 2315056
    iget-boolean v2, v0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->f:Z

    move v2, v2

    .line 2315057
    iput-boolean v2, v1, LX/G3b;->f:Z

    .line 2315058
    move-object v1, v1

    .line 2315059
    iget-boolean v2, v0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->g:Z

    move v2, v2

    .line 2315060
    iput-boolean v2, v1, LX/G3b;->g:Z

    .line 2315061
    move-object v1, v1

    .line 2315062
    iget-boolean v2, v0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->h:Z

    move v2, v2

    .line 2315063
    iput-boolean v2, v1, LX/G3b;->h:Z

    .line 2315064
    move-object v1, v1

    .line 2315065
    iget-boolean v2, v0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->i:Z

    move v2, v2

    .line 2315066
    iput-boolean v2, v1, LX/G3b;->i:Z

    .line 2315067
    move-object v1, v1

    .line 2315068
    iget-object v2, v0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->j:LX/2rX;

    move-object v0, v2

    .line 2315069
    iput-object v0, v1, LX/G3b;->j:LX/2rX;

    .line 2315070
    move-object v0, v1

    .line 2315071
    invoke-virtual {v0}, LX/G3b;->a()LX/G3c;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->r:LX/G3c;

    .line 2315072
    const v0, 0x7f031080

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->setContentView(I)V

    .line 2315073
    const v0, 0x7f0d276d

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->s:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315074
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->p:LX/G3R;

    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->q:LX/G3K;

    iget-object v2, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->r:LX/G3c;

    iget-object v3, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->s:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315075
    new-instance v4, LX/G3Q;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/G3G;->a(LX/0QB;)LX/G3G;

    move-result-object v11

    check-cast v11, LX/G3G;

    .line 2315076
    new-instance v6, LX/G3f;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-direct {v6, v5}, LX/G3f;-><init>(LX/0tX;)V

    .line 2315077
    move-object v12, v6

    .line 2315078
    check-cast v12, LX/G3f;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    move-object v5, p0

    move-object v6, v1

    move-object v7, v2

    move-object v8, v3

    invoke-direct/range {v4 .. v13}, LX/G3Q;-><init>(LX/G3C;LX/G3K;LX/G3c;Lcom/facebook/timeline/refresher/ProfileRefresherView;LX/0tX;Ljava/util/concurrent/Executor;LX/G3G;LX/G3f;LX/0ad;)V

    .line 2315079
    move-object v0, v4

    .line 2315080
    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->t:LX/G3Q;

    .line 2315081
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->t:LX/G3Q;

    .line 2315082
    iget-object v4, v0, LX/G3Q;->a:LX/G3K;

    iget-object v5, v0, LX/G3Q;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315083
    iput-object v5, v4, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315084
    iget-object v4, v0, LX/G3Q;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    invoke-virtual {v4, v0}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->setListener(LX/G3P;)V

    .line 2315085
    if-eqz p1, :cond_1

    .line 2315086
    const-string v4, "profile_nux_refresher_model"

    invoke-static {p1, v4}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 2315087
    if-eqz v4, :cond_1

    .line 2315088
    new-instance v5, Lcom/facebook/graphql/executor/GraphQLResult;

    sget-object v6, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v8, 0x0

    invoke-direct {v5, v4, v6, v8, v9}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;J)V

    invoke-static {v0, v5}, LX/G3Q;->c(LX/G3Q;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2315089
    :goto_1
    goto/16 :goto_0

    .line 2315090
    :cond_1
    iget-object v4, v0, LX/G3Q;->b:LX/G3c;

    .line 2315091
    iget-boolean v5, v4, LX/G3c;->j:Z

    move v4, v5

    .line 2315092
    if-eqz v4, :cond_2

    .line 2315093
    new-instance v4, LX/G3t;

    invoke-direct {v4}, LX/G3t;-><init>()V

    move-object v4, v4

    .line 2315094
    const-string v5, "profile_refresher_step_types"

    invoke-static {v0}, LX/G3Q;->m(LX/G3Q;)LX/0Px;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 2315095
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    .line 2315096
    :goto_2
    iget-object v5, v0, LX/G3Q;->f:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    .line 2315097
    new-instance v6, LX/G3L;

    invoke-direct {v6, v0}, LX/G3L;-><init>(LX/G3Q;)V

    .line 2315098
    iget-object v7, v0, LX/G3Q;->g:Ljava/util/concurrent/Executor;

    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2315099
    iget-object v7, v0, LX/G3Q;->h:Ljava/util/List;

    new-instance v8, LX/1Mv;

    invoke-direct {v8, v5, v6}, LX/1Mv;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2315100
    goto :goto_1

    .line 2315101
    :cond_2
    new-instance v4, LX/G3s;

    invoke-direct {v4}, LX/G3s;-><init>()V

    move-object v4, v4

    .line 2315102
    const-string v5, "profile_nux_step_types"

    invoke-static {v0}, LX/G3Q;->m(LX/G3Q;)LX/0Px;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 2315103
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    goto :goto_2
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 2315024
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->t:LX/G3Q;

    .line 2315025
    iget-object v1, v0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v1

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->CORE_PROFILE_FIELD:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-ne v1, p0, :cond_0

    .line 2315026
    iget-object v1, v0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v1}, LX/G3c;->h()V

    .line 2315027
    invoke-static {v0}, LX/G3Q;->r(LX/G3Q;)V

    .line 2315028
    :cond_0
    iget-object v1, v0, LX/G3Q;->a:LX/G3K;

    iget-object p0, v0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v1, p0}, LX/G3K;->b(LX/G3c;)V

    .line 2315029
    return-void
.end method

.method public final m()V
    .locals 3

    .prologue
    .line 2315112
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0815ea

    invoke-virtual {p0, v1}, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2315113
    return-void
.end method

.method public final n()V
    .locals 4

    .prologue
    .line 2315020
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->s:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2315021
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->s:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->r:LX/G3c;

    iget-object v3, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->u:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->a(LX/0gc;LX/G3c;Landroid/os/Bundle;)V

    .line 2315022
    return-void

    .line 2315023
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()LX/G3c;
    .locals 1

    .prologue
    .line 2315019
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->r:LX/G3c;

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2315013
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2315014
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->t:LX/G3Q;

    .line 2315015
    iget-object v1, v0, LX/G3Q;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315016
    iget-object p0, v1, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v1, p0

    .line 2315017
    iget-object p0, v0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {p0}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object p0

    invoke-virtual {v1, p0}, LX/G3d;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2315018
    return-void
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x2ef548ff

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2315006
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2315007
    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->t:LX/G3Q;

    if-eqz v1, :cond_1

    .line 2315008
    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->t:LX/G3Q;

    .line 2315009
    iget-object v2, v1, LX/G3Q;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Mv;

    .line 2315010
    const/4 p0, 0x0

    invoke-virtual {v2, p0}, LX/1Mv;->a(Z)V

    goto :goto_0

    .line 2315011
    :cond_0
    iget-object v2, v1, LX/G3Q;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2315012
    :cond_1
    const/16 v1, 0x23

    const v2, 0x51111315

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0xef4a2ec

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2315002
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2315003
    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->t:LX/G3Q;

    .line 2315004
    iget-object v2, v1, LX/G3Q;->a:LX/G3K;

    iget-object p0, v1, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v2, p0}, LX/G3K;->b(LX/G3c;)V

    .line 2315005
    const/16 v1, 0x23

    const v2, 0x70bf3615

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2314978
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2314979
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->t:LX/G3Q;

    .line 2314980
    iget-object v1, v0, LX/G3Q;->b:LX/G3c;

    .line 2314981
    iget-object v2, v1, LX/G3c;->p:Ljava/lang/Object;

    move-object v1, v2

    .line 2314982
    if-eqz v1, :cond_0

    .line 2314983
    const-string v2, "profile_nux_refresher_model"

    invoke-static {p1, v2, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2314984
    :cond_0
    iget-object v1, v0, LX/G3Q;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2314985
    iget-object v2, v1, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v1, v2

    .line 2314986
    if-eqz v1, :cond_6

    .line 2314987
    iget-object v1, v0, LX/G3Q;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2314988
    iget-object v2, v1, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v1, v2

    .line 2314989
    iget-object v2, v1, LX/G3d;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/Fragment;

    .line 2314990
    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2314991
    instance-of p0, v2, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    if-eqz p0, :cond_2

    .line 2314992
    iget-object p0, v1, LX/G3d;->a:LX/0gc;

    const-string v0, "nux_refresher_header_fragment"

    invoke-virtual {p0, p1, v0, v2}, LX/0gc;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 2314993
    :cond_2
    instance-of p0, v2, Lcom/facebook/timeline/refresher/ProfileNuxRefresherFeaturedPhotosFragment;

    if-eqz p0, :cond_3

    .line 2314994
    iget-object p0, v1, LX/G3d;->a:LX/0gc;

    const-string v0, "nux_refresher_featured_photos_fragment"

    invoke-virtual {p0, p1, v0, v2}, LX/0gc;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 2314995
    :cond_3
    instance-of p0, v2, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;

    if-eqz p0, :cond_4

    .line 2314996
    iget-object p0, v1, LX/G3d;->a:LX/0gc;

    const-string v0, "nux_refresher_info_fragment"

    invoke-virtual {p0, p1, v0, v2}, LX/0gc;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 2314997
    :cond_4
    instance-of p0, v2, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;

    if-eqz p0, :cond_5

    .line 2314998
    iget-object p0, v1, LX/G3d;->a:LX/0gc;

    const-string v0, "nux_refresher_composer_fragment"

    invoke-virtual {p0, p1, v0, v2}, LX/0gc;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 2314999
    :cond_5
    instance-of p0, v2, Lcom/facebook/timeline/refresher/ProfileRefresherFinishedFragment;

    if-eqz p0, :cond_1

    .line 2315000
    iget-object p0, v1, LX/G3d;->a:LX/0gc;

    const-string v0, "nux_refresher_finished_fragment"

    invoke-virtual {p0, p1, v0, v2}, LX/0gc;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 2315001
    :cond_6
    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x1862572d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2314973
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 2314974
    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;->t:LX/G3Q;

    .line 2314975
    iget-object v2, v1, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v2}, LX/G3c;->u()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2314976
    iget-object v2, v1, LX/G3Q;->a:LX/G3K;

    iget-object p0, v1, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v2, p0}, LX/G3K;->c(LX/G3c;)V

    .line 2314977
    :cond_0
    const/16 v1, 0x23

    const v2, -0x2307f69e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
