.class public Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fg;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/user/model/User;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/17W;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/FwU;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:LX/BP8;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public m:Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

.field private n:LX/G3E;

.field public o:LX/G3O;

.field private p:Z

.field private q:LX/FwT;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:LX/BP7;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:LX/0Yb;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2315716
    const-class v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2315789
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2315790
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->o:LX/G3O;

    return-void
.end method

.method public static r(Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;)Z
    .locals 3

    .prologue
    .line 2315787
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    .line 2315788
    sget-short v1, LX/0wf;->aC:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public static s(Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;)V
    .locals 1

    .prologue
    .line 2315784
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->o:LX/G3O;

    if-eqz v0, :cond_0

    .line 2315785
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->o:LX/G3O;

    invoke-interface {v0}, LX/G3O;->f()V

    .line 2315786
    :cond_0
    return-void
.end method

.method public static t(Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;)LX/FwT;
    .locals 4

    .prologue
    .line 2315781
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->q:LX/FwT;

    if-nez v0, :cond_0

    .line 2315782
    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->j:LX/FwU;

    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p0, v2}, LX/FwU;->a(Ljava/lang/Long;Landroid/support/v4/app/Fragment;I)LX/FwT;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->q:LX/FwT;

    .line 2315783
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->q:LX/FwT;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2315772
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2315773
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    const/16 v3, 0x455

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x1032

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x15e7

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/0XE;->b(LX/0QB;)Lcom/facebook/user/model/User;

    move-result-object v6

    check-cast v6, Lcom/facebook/user/model/User;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v7

    check-cast v7, LX/17W;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    const-class v11, LX/FwU;

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/FwU;

    const-class p1, LX/BP8;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/BP8;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v0

    check-cast v0, LX/0Xl;

    iput-object v3, v2, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->a:LX/0Or;

    iput-object v4, v2, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->b:LX/0Or;

    iput-object v5, v2, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->d:LX/0Or;

    iput-object v6, v2, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->e:Lcom/facebook/user/model/User;

    iput-object v7, v2, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->f:LX/17W;

    iput-object v8, v2, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->g:LX/0tX;

    iput-object v9, v2, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->h:Ljava/util/concurrent/Executor;

    iput-object v10, v2, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->i:LX/0ad;

    iput-object v11, v2, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->j:LX/FwU;

    iput-object p1, v2, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->k:LX/BP8;

    iput-object v0, v2, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->l:LX/0Xl;

    .line 2315774
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2315775
    if-eqz v0, :cond_0

    .line 2315776
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2315777
    const-string v1, "extra_is_from_qp"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->p:Z

    .line 2315778
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->l:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.intent.action.COVER_PHOTO_UPDATED"

    new-instance v2, LX/G3T;

    invoke-direct {v2, p0}, LX/G3T;-><init>(Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.intent.action.PROFILE_PIC_UPDATED"

    new-instance v2, LX/G3S;

    invoke-direct {v2, p0}, LX/G3S;-><init>(Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->s:LX/0Yb;

    .line 2315779
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->s:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2315780
    return-void
.end method

.method public final d()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 2315771
    const v0, 0x7f0d277a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 2315769
    invoke-static {p0}, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->t(Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;)LX/FwT;

    move-result-object v0

    invoke-static {p0}, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->r(Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;)Z

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/FwT;->b(ZZ)V

    .line 2315770
    return-void
.end method

.method public final l()V
    .locals 4

    .prologue
    .line 2315767
    invoke-static {p0}, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->t(Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;)LX/FwT;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/FwT;->a(J)V

    .line 2315768
    return-void
.end method

.method public final mJ_()V
    .locals 0

    .prologue
    .line 2315766
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x44634908

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2315750
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2315751
    iget-boolean v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->p:Z

    if-eqz v1, :cond_0

    .line 2315752
    invoke-static {}, LX/0wB;->c()I

    move-result v1

    .line 2315753
    invoke-static {}, LX/G3u;->c()LX/G3r;

    move-result-object v4

    const-string p1, "profile_id"

    iget-object v2, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, p1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v4, "cover_photo_size"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v2, v4, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/G3r;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 2315754
    iget-object v4, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->g:LX/0tX;

    invoke-virtual {v4, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 2315755
    new-instance v4, LX/G3V;

    invoke-direct {v4, p0}, LX/G3V;-><init>(Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;)V

    .line 2315756
    iget-object p1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->h:Ljava/util/concurrent/Executor;

    invoke-static {v2, v4, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2315757
    invoke-static {}, LX/5wK;->b()LX/5wG;

    move-result-object v2

    const-string v4, "profile_id"

    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    check-cast v1, LX/5wG;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2315758
    iget-object v2, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->g:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 2315759
    new-instance v2, LX/G3W;

    invoke-direct {v2, p0}, LX/G3W;-><init>(Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;)V

    .line 2315760
    iget-object v4, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->h:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2315761
    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->m:Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    invoke-virtual {v1}, LX/Ban;->getCoverPhotoView()Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    move-result-object v1

    const v2, 0x3ecccccd    # 0.4f

    invoke-virtual {v1, v2}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->setAlpha(F)V

    .line 2315762
    new-instance v1, LX/G3U;

    invoke-direct {v1, p0}, LX/G3U;-><init>(Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;)V

    .line 2315763
    iput-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->o:LX/G3O;

    .line 2315764
    const v1, 0x7f0d277a

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2315765
    :cond_0
    const/16 v1, 0x2b

    const v2, 0xb761499

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    .line 2315736
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 2315737
    :cond_0
    :goto_0
    return-void

    .line 2315738
    :cond_1
    const/16 v0, 0xc35

    if-eq p1, v0, :cond_2

    const/16 v0, 0x26bb

    if-ne p1, v0, :cond_3

    .line 2315739
    :cond_2
    invoke-static {p0}, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->t(Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;)LX/FwT;

    move-result-object v0

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/FwT;->a(Landroid/os/Bundle;)V

    goto :goto_0

    .line 2315740
    :cond_3
    const/16 v0, 0xc34

    if-eq p1, v0, :cond_4

    const/16 v0, 0x26bc

    if-ne p1, v0, :cond_6

    .line 2315741
    :cond_4
    if-eqz p3, :cond_0

    .line 2315742
    iget-object v2, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->r:LX/BP7;

    if-nez v2, :cond_5

    .line 2315743
    iget-object v3, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->k:LX/BP8;

    iget-object v2, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/BP8;->a(Ljava/lang/Long;)LX/BP7;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->r:LX/BP7;

    .line 2315744
    :cond_5
    iget-object v2, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->r:LX/BP7;

    move-object v0, v2

    .line 2315745
    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->n:LX/G3E;

    invoke-interface {v1}, LX/G3E;->o()LX/G3c;

    move-result-object v1

    .line 2315746
    iget-boolean v2, v1, LX/G3c;->i:Z

    move v1, v2

    .line 2315747
    invoke-virtual {v0, p0, p3, v1}, LX/BP7;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;Z)V

    goto :goto_0

    .line 2315748
    :cond_6
    const/16 v0, 0x71d

    if-ne p1, v0, :cond_0

    .line 2315749
    invoke-static {p0}, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->s(Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;)V

    goto :goto_0
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2315733
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/content/Context;)V

    .line 2315734
    check-cast p1, LX/G3E;

    iput-object p1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->n:LX/G3E;

    .line 2315735
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x4e1a9118

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2315723
    const v0, 0x7f031085

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 2315724
    new-instance v3, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    .line 2315725
    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->i:LX/0ad;

    sget-short v4, LX/0wf;->W:S

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v1

    move v1, v1

    .line 2315726
    if-eqz v1, :cond_0

    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0e05b0

    invoke-direct {v1, v4, v5}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    :goto_0
    invoke-direct {v3, v1}, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->m:Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    .line 2315727
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 2315728
    iget-object v3, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->m:Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    invoke-virtual {v3}, LX/Ban;->getScreenWidth()I

    move-result v3

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 2315729
    const/4 v3, 0x1

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 2315730
    iget-object v3, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->m:Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    invoke-virtual {v0, v3, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2315731
    const v1, 0x87c1dbc

    invoke-static {v1, v2}, LX/02F;->f(II)V

    return-object v0

    .line 2315732
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x31991457

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2315717
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2315718
    iput-object v2, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->o:LX/G3O;

    .line 2315719
    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->s:LX/0Yb;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->s:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2315720
    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->s:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2315721
    :cond_0
    iput-object v2, p0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->s:LX/0Yb;

    .line 2315722
    const/16 v1, 0x2b

    const v2, 0x289d3e01

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
