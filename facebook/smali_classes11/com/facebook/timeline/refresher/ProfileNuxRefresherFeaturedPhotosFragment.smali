.class public Lcom/facebook/timeline/refresher/ProfileNuxRefresherFeaturedPhotosFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/widget/mosaic/MosaicGridLayout;

.field private d:LX/G36;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2314905
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2314906
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2314907
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherFeaturedPhotosFragment;

    const/16 p1, 0xc49

    invoke-static {v2, p1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    const/16 v0, 0x455

    invoke-static {v2, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    iput-object p1, p0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherFeaturedPhotosFragment;->a:LX/0Ot;

    iput-object v2, p0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherFeaturedPhotosFragment;->b:LX/0Or;

    .line 2314908
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2314909
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2314910
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 2314911
    :goto_0
    return-void

    .line 2314912
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherFeaturedPhotosFragment;->d:LX/G36;

    invoke-interface {v0}, LX/G36;->a()V

    goto :goto_0
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2314913
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/content/Context;)V

    .line 2314914
    check-cast p1, LX/G36;

    iput-object p1, p0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherFeaturedPhotosFragment;->d:LX/G36;

    .line 2314915
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, -0x1976baf3

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2314916
    const v0, 0x7f03106f

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2314917
    const v0, 0x7f0d275c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherFeaturedPhotosFragment;->c:Lcom/facebook/widget/mosaic/MosaicGridLayout;

    .line 2314918
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherFeaturedPhotosFragment;->c:Lcom/facebook/widget/mosaic/MosaicGridLayout;

    const/4 v3, 0x6

    const/4 v4, 0x5

    invoke-virtual {v0, v3, v4}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->a(II)V

    .line 2314919
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0e10

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2314920
    iget-object v3, p0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherFeaturedPhotosFragment;->c:Lcom/facebook/widget/mosaic/MosaicGridLayout;

    invoke-virtual {v3, v0, v0}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->b(II)V

    .line 2314921
    const/4 p3, 0x3

    const/4 p2, 0x2

    const/4 v4, 0x0

    .line 2314922
    move v3, v4

    :goto_0
    const/4 v0, 0x5

    if-ge v3, v0, :cond_1

    .line 2314923
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v6, 0x7f0314c3

    iget-object v7, p0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherFeaturedPhotosFragment;->c:Lcom/facebook/widget/mosaic/MosaicGridLayout;

    invoke-virtual {v0, v6, v7, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 2314924
    const v0, 0x7f0d2f17

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    .line 2314925
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const p1, 0x7f0a00a2

    invoke-virtual {v7, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2314926
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const p1, 0x7f0a009a

    invoke-virtual {v7, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-direct {v0, v7}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v6, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2314927
    if-ge v3, p2, :cond_0

    .line 2314928
    new-instance v0, LX/G6g;

    mul-int/lit8 v7, v3, 0x3

    invoke-direct {v0, v7, v4, p3, p3}, LX/G6g;-><init>(IIII)V

    .line 2314929
    :goto_1
    invoke-virtual {v6, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2314930
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherFeaturedPhotosFragment;->c:Lcom/facebook/widget/mosaic/MosaicGridLayout;

    invoke-virtual {v0, v6}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->addView(Landroid/view/View;)V

    .line 2314931
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2314932
    :cond_0
    new-instance v0, LX/G6g;

    add-int/lit8 v7, v3, -0x2

    mul-int/lit8 v7, v7, 0x2

    invoke-direct {v0, v7, p3, p2, p2}, LX/G6g;-><init>(IIII)V

    goto :goto_1

    .line 2314933
    :cond_1
    const/16 v0, 0x2b

    const v3, 0x78345088

    invoke-static {v5, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method
