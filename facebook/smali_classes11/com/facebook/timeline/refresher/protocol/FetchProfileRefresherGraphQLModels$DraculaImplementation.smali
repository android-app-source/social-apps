.class public final Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2316307
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2316308
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2316449
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2316450
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 8

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 2316374
    if-nez p1, :cond_0

    .line 2316375
    :goto_0
    return v0

    .line 2316376
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2316377
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2316378
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2316379
    const v2, -0x45019c9

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2316380
    invoke-virtual {p0, p1, v5}, LX/15i;->p(II)I

    move-result v2

    .line 2316381
    const v3, -0x3832c418

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2316382
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2316383
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2316384
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2316385
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2316386
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2316387
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2316388
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2316389
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2316390
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2316391
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2316392
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2316393
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2316394
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2316395
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2316396
    :sswitch_3
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2316397
    invoke-virtual {p0, p1, v5}, LX/15i;->p(II)I

    move-result v2

    .line 2316398
    const v3, -0x7f5f8fc3

    const/4 v7, 0x0

    .line 2316399
    if-nez v2, :cond_1

    move v4, v7

    .line 2316400
    :goto_1
    move v2, v4

    .line 2316401
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2316402
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2316403
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2316404
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2316405
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2316406
    const v2, 0x64a5c975

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2316407
    invoke-virtual {p0, p1, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v2

    .line 2316408
    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 2316409
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v3

    .line 2316410
    const v4, -0x4694de45

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 2316411
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2316412
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2316413
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2316414
    invoke-virtual {p3, v6, v3}, LX/186;->b(II)V

    .line 2316415
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2316416
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2316417
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2316418
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2316419
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2316420
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2316421
    :sswitch_6
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2316422
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2316423
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2316424
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2316425
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2316426
    :sswitch_7
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2316427
    const v2, 0x3821ec20

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2316428
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2316429
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2316430
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2316431
    :sswitch_8
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2316432
    const v2, 0x7253c121

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2316433
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2316434
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2316435
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2316436
    :sswitch_9
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2316437
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2316438
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2316439
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2316440
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2316441
    :cond_1
    invoke-virtual {p0, v2}, LX/15i;->c(I)I

    move-result p1

    .line 2316442
    if-nez p1, :cond_2

    const/4 v4, 0x0

    .line 2316443
    :goto_2
    if-ge v7, p1, :cond_3

    .line 2316444
    invoke-virtual {p0, v2, v7}, LX/15i;->q(II)I

    move-result p2

    .line 2316445
    invoke-static {p0, p2, v3, p3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v4, v7

    .line 2316446
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 2316447
    :cond_2
    new-array v4, p1, [I

    goto :goto_2

    .line 2316448
    :cond_3
    const/4 v7, 0x1

    invoke-virtual {p3, v4, v7}, LX/186;->a([IZ)I

    move-result v4

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x7f5f8fc3 -> :sswitch_4
        -0x635ebfef -> :sswitch_7
        -0x5e492a93 -> :sswitch_0
        -0x4c13f8e1 -> :sswitch_2
        -0x4694de45 -> :sswitch_6
        -0x3832c418 -> :sswitch_3
        -0x45019c9 -> :sswitch_1
        0x3821ec20 -> :sswitch_8
        0x64a5c975 -> :sswitch_5
        0x7253c121 -> :sswitch_9
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2316373
    new-instance v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 2316349
    sparse-switch p2, :sswitch_data_0

    .line 2316350
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2316351
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2316352
    const v1, -0x45019c9

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2316353
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 2316354
    const v1, -0x3832c418

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2316355
    :goto_0
    :sswitch_1
    return-void

    .line 2316356
    :sswitch_2
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 2316357
    const v1, -0x7f5f8fc3

    .line 2316358
    if-eqz v0, :cond_0

    .line 2316359
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2316360
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 2316361
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2316362
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2316363
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2316364
    :cond_0
    goto :goto_0

    .line 2316365
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2316366
    const v1, 0x64a5c975

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2316367
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2316368
    const v1, -0x4694de45

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2316369
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2316370
    const v1, 0x3821ec20

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2316371
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2316372
    const v1, 0x7253c121

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7f5f8fc3 -> :sswitch_3
        -0x635ebfef -> :sswitch_4
        -0x5e492a93 -> :sswitch_0
        -0x4c13f8e1 -> :sswitch_1
        -0x4694de45 -> :sswitch_1
        -0x3832c418 -> :sswitch_2
        -0x45019c9 -> :sswitch_1
        0x3821ec20 -> :sswitch_5
        0x64a5c975 -> :sswitch_1
        0x7253c121 -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2316343
    if-eqz p1, :cond_0

    .line 2316344
    invoke-static {p0, p1, p2}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 2316345
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;

    .line 2316346
    if-eq v0, v1, :cond_0

    .line 2316347
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2316348
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2316342
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2316340
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2316341
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2316335
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2316336
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2316337
    :cond_0
    iput-object p1, p0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2316338
    iput p2, p0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->b:I

    .line 2316339
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2316334
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2316333
    new-instance v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2316330
    iget v0, p0, LX/1vt;->c:I

    .line 2316331
    move v0, v0

    .line 2316332
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2316327
    iget v0, p0, LX/1vt;->c:I

    .line 2316328
    move v0, v0

    .line 2316329
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2316324
    iget v0, p0, LX/1vt;->b:I

    .line 2316325
    move v0, v0

    .line 2316326
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2316321
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2316322
    move-object v0, v0

    .line 2316323
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2316312
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2316313
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2316314
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2316315
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2316316
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2316317
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2316318
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2316319
    invoke-static {v3, v9, v2}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2316320
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2316309
    iget v0, p0, LX/1vt;->c:I

    .line 2316310
    move v0, v0

    .line 2316311
    return v0
.end method
