.class public final Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x418340ca
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2316471
    const-class v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2316472
    const-class v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2316473
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2316474
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2316475
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2316476
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x4c13f8e1

    invoke-static {v1, v0, v2}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2316477
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x3832c418

    invoke-static {v2, v1, v3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2316478
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2316479
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2316480
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2316481
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2316482
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2316483
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2316484
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2316485
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x4c13f8e1

    invoke-static {v2, v0, v3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2316486
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2316487
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;

    .line 2316488
    iput v3, v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;->e:I

    move-object v1, v0

    .line 2316489
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2316490
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x3832c418

    invoke-static {v2, v0, v3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2316491
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2316492
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;

    .line 2316493
    iput v3, v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;->f:I

    move-object v1, v0

    .line 2316494
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2316495
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2316496
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2316497
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object p0, v1

    .line 2316498
    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCoverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2316499
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2316500
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2316501
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2316502
    const/4 v0, 0x0

    const v1, -0x4c13f8e1

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;->e:I

    .line 2316503
    const/4 v0, 0x1

    const v1, -0x3832c418

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;->f:I

    .line 2316504
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2316505
    new-instance v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;

    invoke-direct {v0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;-><init>()V

    .line 2316506
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2316507
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2316508
    const v0, -0x3b5df088

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2316509
    const v0, 0x7f9f4ad4

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSteps"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2316510
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2316511
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
