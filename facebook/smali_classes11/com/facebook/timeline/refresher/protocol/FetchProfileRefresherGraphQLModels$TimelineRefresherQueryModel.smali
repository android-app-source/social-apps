.class public final Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2bdb9884
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2316821
    const-class v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2316820
    const-class v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2316818
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2316819
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2316812
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2316813
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel;->a()Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2316814
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2316815
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2316816
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2316817
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2316804
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2316805
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel;->a()Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2316806
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel;->a()Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;

    .line 2316807
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel;->a()Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2316808
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel;

    .line 2316809
    iput-object v0, v1, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel;->e:Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;

    .line 2316810
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2316811
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfileWizardRefresher"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2316797
    iget-object v0, p0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel;->e:Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel;->e:Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;

    .line 2316798
    iget-object v0, p0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel;->e:Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2316801
    new-instance v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel;

    invoke-direct {v0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel;-><init>()V

    .line 2316802
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2316803
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2316800
    const v0, 0x26afd814

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2316799
    const v0, -0x6747e1ce

    return v0
.end method
