.class public final Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2316578
    const-class v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;

    new-instance v1, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2316579
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2316512
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2316513
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2316514
    const/4 v2, 0x0

    .line 2316515
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2316516
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2316517
    :goto_0
    move v1, v2

    .line 2316518
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2316519
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2316520
    new-instance v1, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;

    invoke-direct {v1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;-><init>()V

    .line 2316521
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2316522
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2316523
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2316524
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2316525
    :cond_0
    return-object v1

    .line 2316526
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2316527
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2316528
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2316529
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2316530
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2316531
    const-string v4, "cover_photo"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2316532
    const/4 v3, 0x0

    .line 2316533
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 2316534
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2316535
    :goto_2
    move v1, v3

    .line 2316536
    goto :goto_1

    .line 2316537
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2316538
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2316539
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 2316540
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2316541
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 2316542
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2316543
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2316544
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 2316545
    const-string v5, "photo"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2316546
    const/4 v4, 0x0

    .line 2316547
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_c

    .line 2316548
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2316549
    :goto_4
    move v1, v4

    .line 2316550
    goto :goto_3

    .line 2316551
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 2316552
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2316553
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v1, v3

    goto :goto_3

    .line 2316554
    :cond_9
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2316555
    :cond_a
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_b

    .line 2316556
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2316557
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2316558
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, p0, :cond_a

    if-eqz v5, :cond_a

    .line 2316559
    const-string v6, "image"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 2316560
    const/4 v5, 0x0

    .line 2316561
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v6, :cond_10

    .line 2316562
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2316563
    :goto_6
    move v1, v5

    .line 2316564
    goto :goto_5

    .line 2316565
    :cond_b
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 2316566
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2316567
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_4

    :cond_c
    move v1, v4

    goto :goto_5

    .line 2316568
    :cond_d
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2316569
    :cond_e
    :goto_7
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_f

    .line 2316570
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2316571
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2316572
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_e

    if-eqz v6, :cond_e

    .line 2316573
    const-string p0, "uri"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 2316574
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_7

    .line 2316575
    :cond_f
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 2316576
    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 2316577
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_6

    :cond_10
    move v1, v5

    goto :goto_7
.end method
