.class public final Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2316644
    const-class v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel;

    new-instance v1, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2316645
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2316646
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2316647
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2316648
    const/4 v2, 0x0

    .line 2316649
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2316650
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2316651
    :goto_0
    move v1, v2

    .line 2316652
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2316653
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2316654
    new-instance v1, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel;

    invoke-direct {v1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel;-><init>()V

    .line 2316655
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2316656
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2316657
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2316658
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2316659
    :cond_0
    return-object v1

    .line 2316660
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2316661
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2316662
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2316663
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2316664
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2316665
    const-string v4, "profile_wizard_nux"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2316666
    const/4 v3, 0x0

    .line 2316667
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 2316668
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2316669
    :goto_2
    move v1, v3

    .line 2316670
    goto :goto_1

    .line 2316671
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2316672
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2316673
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 2316674
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2316675
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_8

    .line 2316676
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2316677
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2316678
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, p0, :cond_6

    if-eqz v5, :cond_6

    .line 2316679
    const-string v6, "cover_photo"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2316680
    const/4 v5, 0x0

    .line 2316681
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_d

    .line 2316682
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2316683
    :goto_4
    move v4, v5

    .line 2316684
    goto :goto_3

    .line 2316685
    :cond_7
    const-string v6, "steps"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2316686
    invoke-static {p1, v0}, LX/G3y;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_3

    .line 2316687
    :cond_8
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 2316688
    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 2316689
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2316690
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    move v4, v3

    goto :goto_3

    .line 2316691
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2316692
    :cond_b
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_c

    .line 2316693
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2316694
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2316695
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_b

    if-eqz v6, :cond_b

    .line 2316696
    const-string p0, "uri"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2316697
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_5

    .line 2316698
    :cond_c
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 2316699
    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 2316700
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_4

    :cond_d
    move v4, v5

    goto :goto_5
.end method
