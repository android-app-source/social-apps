.class public final Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2316701
    const-class v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel;

    new-instance v1, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2316702
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2316727
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2316704
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2316705
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2316706
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2316707
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2316708
    if-eqz v2, :cond_3

    .line 2316709
    const-string v3, "profile_wizard_nux"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2316710
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2316711
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2316712
    if-eqz v3, :cond_1

    .line 2316713
    const-string p0, "cover_photo"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2316714
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2316715
    const/4 p0, 0x0

    invoke-virtual {v1, v3, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2316716
    if-eqz p0, :cond_0

    .line 2316717
    const-string v0, "uri"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2316718
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2316719
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2316720
    :cond_1
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2316721
    if-eqz v3, :cond_2

    .line 2316722
    const-string p0, "steps"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2316723
    invoke-static {v1, v3, p1, p2}, LX/G3y;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2316724
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2316725
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2316726
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2316703
    check-cast p1, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel$Serializer;->a(Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
