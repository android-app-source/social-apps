.class public final Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2316468
    const-class v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;

    new-instance v1, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2316469
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2316470
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2316465
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2316466
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/G3x;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2316467
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2316464
    check-cast p1, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel$Serializer;->a(Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
