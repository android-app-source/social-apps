.class public Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;
.super LX/Ban;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public n:LX/Bas;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2314971
    const-class v0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2314934
    invoke-direct {p0, p1}, LX/Ban;-><init>(Landroid/content/Context;)V

    .line 2314935
    const-class v0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2314936
    invoke-static {p0}, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->h(Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/Bam;->NARROW:LX/Bam;

    :goto_0
    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->d:LX/Bam;

    .line 2314937
    sget-object v0, LX/Bap;->IMAGE:LX/Bap;

    invoke-virtual {p0, v0}, LX/Ban;->setCoverType(LX/Bap;)V

    .line 2314938
    invoke-virtual {p0}, LX/Ban;->f()V

    .line 2314939
    iget-object v0, p0, LX/Ban;->k:Landroid/view/View;

    const/4 p1, 0x0

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 2314940
    return-void

    .line 2314941
    :cond_0
    sget-object v0, LX/Bam;->STANDARD:LX/Bam;

    goto :goto_0
.end method

.method private a(ILjava/lang/String;)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 2314969
    const/4 v0, 0x0

    const v1, 0x7f0e0696

    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p2, v0, p1, v1, v2}, LX/EQR;->a(Ljava/lang/String;Ljava/lang/String;IILandroid/content/Context;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 2314970
    return-object v0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    invoke-static {p0}, LX/Bas;->b(LX/0QB;)LX/Bas;

    move-result-object v1

    check-cast v1, LX/Bas;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v3

    check-cast v3, LX/0hB;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object p0

    check-cast p0, Landroid/content/res/Resources;

    iput-object v1, p1, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->n:LX/Bas;

    iput-object v2, p1, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->o:LX/0ad;

    iput-object v3, p1, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->p:LX/0hB;

    iput-object p0, p1, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->q:Landroid/content/res/Resources;

    return-void
.end method

.method public static h(Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;)Z
    .locals 3

    .prologue
    .line 2314968
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->o:LX/0ad;

    sget-short v1, LX/0wf;->W:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(II)I
    .locals 2

    .prologue
    .line 2314972
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->q:Landroid/content/res/Resources;

    const v1, 0x7f0b0dea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 17

    .prologue
    .line 2314949
    invoke-static/range {p1 .. p1}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v6

    .line 2314950
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual/range {p0 .. p0}, LX/Ban;->getScreenWidth()I

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, LX/Ban;->c:I

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    sget-object v12, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->m:Lcom/facebook/common/callercontext/CallerContext;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x1

    invoke-virtual/range {v2 .. v16}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a(IIZLX/1bf;LX/1bf;Landroid/graphics/PointF;ZZ[Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;LX/1cC;ZZ)V

    .line 2314951
    if-eqz p3, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/Ban;->h:Landroid/view/ViewStub;

    invoke-virtual {v2}, Landroid/view/ViewStub;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2314952
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setVisibility(I)V

    .line 2314953
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Ban;->h:Landroid/view/ViewStub;

    const v3, 0x7f030c42

    invoke-virtual {v2, v3}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2314954
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Ban;->h:Landroid/view/ViewStub;

    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    .line 2314955
    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2314956
    const v3, 0x3ecccccd    # 0.4f

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setAlpha(F)V

    .line 2314957
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2314958
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Ban;->g:Landroid/view/ViewStub;

    const v3, 0x7f030c43

    invoke-virtual {v2, v3}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2314959
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Ban;->g:Landroid/view/ViewStub;

    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2314960
    :goto_0
    return-void

    .line 2314961
    :cond_0
    invoke-static/range {p0 .. p0}, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->h(Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;)Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz p2, :cond_2

    .line 2314962
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    const v3, 0x7f0e068c

    invoke-virtual {v2, v3}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setTitleTextAppearance(I)V

    .line 2314963
    const v2, 0x7f0e068e

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1}, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->a(ILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 2314964
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v3

    const/16 v4, 0x1e

    if-le v3, v4, :cond_1

    .line 2314965
    const v2, 0x7f0e0690

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1}, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->a(ILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 2314966
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v3, v2}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setTitleText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2314967
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setVisibility(I)V

    goto :goto_0
.end method

.method public getCoverPhotoView()Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;
    .locals 1

    .prologue
    .line 2314948
    iget-object v0, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    return-object v0
.end method

.method public getScreenWidth()I
    .locals 3

    .prologue
    .line 2314947
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->p:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->q:Landroid/content/res/Resources;

    const v2, 0x7f0b0de6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    return v0
.end method

.method public setUpProfilePicture(Ljava/lang/String;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 2314942
    invoke-static {p1}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v2

    .line 2314943
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->n:LX/Bas;

    sget-object v7, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->m:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v4, p0, LX/Ban;->l:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    .line 2314944
    iget-object v5, v4, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->b:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    move-object v10, v5

    .line 2314945
    move v4, v3

    move v5, v3

    move v6, v3

    move-object v8, v1

    move-object v9, v1

    invoke-virtual/range {v0 .. v10}, LX/Bas;->a(LX/1bf;LX/1bf;ZZZZLcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;LX/1cC;Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;)V

    .line 2314946
    return-void
.end method
