.class public Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/G3A;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;

.field private f:Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;

.field private g:LX/G36;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2314901
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2314898
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2314899
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;

    const/16 v2, 0x12cb

    invoke-static {v3, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v2, 0x19c6

    invoke-static {v3, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    new-instance v2, LX/G3A;

    const/16 v6, 0x12cb

    invoke-static {v3, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 p1, 0xbde

    invoke-static {v3, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    const/16 v0, 0x3be

    invoke-static {v3, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    const/16 v1, 0x36f1

    invoke-static {v3, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v2, v6, p1, v0, v1}, LX/G3A;-><init>(LX/0Or;LX/0Ot;LX/0Or;LX/0Or;)V

    move-object v2, v2

    check-cast v2, LX/G3A;

    invoke-static {v3}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v3

    check-cast v3, LX/0W9;

    iput-object v4, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->a:LX/0Or;

    iput-object v5, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->b:LX/0Ot;

    iput-object v2, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->c:LX/G3A;

    iput-object v3, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->d:LX/0W9;

    .line 2314900
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2314888
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2314889
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 2314890
    :goto_0
    return-void

    .line 2314891
    :cond_0
    const/16 v0, 0x6dc

    if-ne p1, v0, :cond_3

    .line 2314892
    const-string v0, "is_uploading_media"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2314893
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->b(Landroid/content/Intent;)V

    .line 2314894
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->g:LX/G36;

    invoke-interface {v0}, LX/G36;->a()V

    goto :goto_0

    .line 2314895
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1

    .line 2314896
    :cond_3
    const/16 v0, 0x6e0

    if-ne p1, v0, :cond_1

    const-string v0, "is_uploading_media"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2314897
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->d(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2314885
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/content/Context;)V

    .line 2314886
    check-cast p1, LX/G36;

    iput-object p1, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->g:LX/G36;

    .line 2314887
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x13d9d140

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2314839
    const v1, 0x7f03106b

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x77c03457

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2314840
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2314841
    const v0, 0x7f0d274e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->e:Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;

    .line 2314842
    const v0, 0x7f0d274f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->f:Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;

    .line 2314843
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2314844
    new-instance v1, LX/1EA;

    invoke-direct {v1}, LX/1EA;-><init>()V

    const v2, 0x7f081405

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2314845
    iput-object v2, v1, LX/1EA;->o:Ljava/lang/String;

    .line 2314846
    move-object v1, v1

    .line 2314847
    const v2, 0x7f08154d

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2314848
    iput-object v2, v1, LX/1EA;->l:Ljava/lang/String;

    .line 2314849
    move-object v1, v1

    .line 2314850
    const v2, 0x7f0815e4

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2314851
    iput-object v2, v1, LX/1EA;->m:Ljava/lang/String;

    .line 2314852
    move-object v1, v1

    .line 2314853
    const v2, 0x7f08154f

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2314854
    iput-object v0, v1, LX/1EA;->n:Ljava/lang/String;

    .line 2314855
    move-object v0, v1

    .line 2314856
    const v1, 0x7f020dfb

    .line 2314857
    iput v1, v0, LX/1EA;->e:I

    .line 2314858
    move-object v0, v0

    .line 2314859
    invoke-virtual {v0}, LX/1EA;->a()LX/1EE;

    move-result-object v1

    .line 2314860
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->d:LX/0W9;

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, LX/1aH;->a(Landroid/content/res/Resources;LX/1EE;LX/0W9;Z)LX/1aH;

    move-result-object v2

    .line 2314861
    iget-object v3, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->e:Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;

    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->a(Ljava/lang/String;)V

    .line 2314862
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->e:Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081405

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->setComposerHintText(Ljava/lang/String;)V

    .line 2314863
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->f:Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;

    iget-object v3, v2, LX/1aH;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v3}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setCheckinButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2314864
    iget-object v3, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->f:Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;

    .line 2314865
    iget-boolean v0, v1, LX/1EE;->c:Z

    move v0, v0

    .line 2314866
    if-eqz v0, :cond_3

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setCheckinButtonVisibility(I)V

    .line 2314867
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->f:Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;

    iget-object v1, v2, LX/1aH;->b:Ljava/lang/String;

    iget-object v3, v2, LX/1aH;->a:Ljava/lang/String;

    iget-object v2, v2, LX/1aH;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v3, v2}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2314868
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2314869
    const-string v1, "post_item_privacy"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2rX;

    .line 2314870
    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->c:LX/G3A;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->e:Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;

    iget-object v4, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->f:Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;

    .line 2314871
    iget-object p1, v1, LX/G3A;->a:Landroid/view/View$OnClickListener;

    if-nez p1, :cond_0

    .line 2314872
    new-instance p1, LX/G37;

    invoke-direct {p1, v1, v0, v2}, LX/G37;-><init>(LX/G3A;LX/2rX;Landroid/support/v4/app/FragmentActivity;)V

    iput-object p1, v1, LX/G3A;->a:Landroid/view/View$OnClickListener;

    .line 2314873
    :cond_0
    iget-object p1, v1, LX/G3A;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, p1}, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->setHeaderSectionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2314874
    iget-object p1, v1, LX/G3A;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, p1}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setStatusButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2314875
    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->c:LX/G3A;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->f:Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;

    .line 2314876
    iget-object v4, v1, LX/G3A;->b:Landroid/view/View$OnClickListener;

    if-nez v4, :cond_1

    .line 2314877
    new-instance v4, LX/G38;

    invoke-direct {v4, v1, v2, v0}, LX/G38;-><init>(LX/G3A;Landroid/support/v4/app/FragmentActivity;LX/2rX;)V

    iput-object v4, v1, LX/G3A;->b:Landroid/view/View$OnClickListener;

    .line 2314878
    :cond_1
    iget-object v4, v1, LX/G3A;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setPhotoButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2314879
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->c:LX/G3A;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;->f:Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;

    .line 2314880
    iget-object v3, v0, LX/G3A;->c:Landroid/view/View$OnClickListener;

    if-nez v3, :cond_2

    .line 2314881
    new-instance v3, LX/G39;

    invoke-direct {v3, v0, v1}, LX/G39;-><init>(LX/G3A;Landroid/support/v4/app/FragmentActivity;)V

    iput-object v3, v0, LX/G3A;->c:Landroid/view/View$OnClickListener;

    .line 2314882
    :cond_2
    iget-object v3, v0, LX/G3A;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setCheckinButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2314883
    return-void

    .line 2314884
    :cond_3
    const/16 v0, 0x8

    goto :goto_0
.end method
