.class public Lcom/facebook/timeline/refresher/ProfileRefresherFinishedFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:LX/G3D;

.field private b:Lcom/facebook/resources/ui/FbButton;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2315669
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2315670
    return-void
.end method


# virtual methods
.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6d644204

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2315665
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2315666
    const v0, 0x7f0d2779

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherFinishedFragment;->b:Lcom/facebook/resources/ui/FbButton;

    .line 2315667
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherFinishedFragment;->b:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2315668
    const/16 v0, 0x2b

    const v2, 0x683cfee1

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5bc73b5e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2315662
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/app/Activity;)V

    .line 2315663
    check-cast p1, LX/G3D;

    iput-object p1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherFinishedFragment;->a:LX/G3D;

    .line 2315664
    const/16 v1, 0x2b

    const v2, 0xb274553

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x7e7dc6ed

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2315659
    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherFinishedFragment;->b:Lcom/facebook/resources/ui/FbButton;

    if-ne p1, v1, :cond_0

    .line 2315660
    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherFinishedFragment;->a:LX/G3D;

    invoke-interface {v1}, LX/G3D;->b()V

    .line 2315661
    :cond_0
    const v1, 0x1fcc64d2

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x67e410b0

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2315658
    const v1, 0x7f031084

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x246e45d3

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method
