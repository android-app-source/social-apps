.class public Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/G3a;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Fz1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/G3F;

.field private d:LX/Fz0;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:LX/G3Z;

.field private f:Landroid/view/ViewGroup;

.field private g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2315815
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2315816
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;

    const-class v1, LX/G3a;

    invoke-interface {v2, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/G3a;

    const-class p0, LX/Fz1;

    invoke-interface {v2, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/Fz1;

    iput-object v1, p1, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->a:LX/G3a;

    iput-object v2, p1, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->b:LX/Fz1;

    return-void
.end method


# virtual methods
.method public final a(LX/Fz2;)V
    .locals 13

    .prologue
    .line 2315866
    invoke-virtual {p1}, LX/BPB;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2315867
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2315868
    if-eqz v0, :cond_0

    .line 2315869
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2315870
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->b:LX/Fz1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2315871
    new-instance v5, LX/Fz0;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v0}, LX/BPp;->a(LX/0QB;)LX/BPp;

    move-result-object v9

    check-cast v9, LX/BPp;

    .line 2315872
    new-instance v12, LX/Fyh;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v6

    check-cast v6, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v10

    check-cast v10, LX/0Sh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-direct {v12, v6, v7, v10, v11}, LX/Fyh;-><init>(Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0tX;LX/0Sh;LX/03V;)V

    .line 2315873
    move-object v10, v12

    .line 2315874
    check-cast v10, LX/Fyh;

    move-object v6, v1

    move-object v7, p1

    invoke-direct/range {v5 .. v10}, LX/Fz0;-><init>(Landroid/content/Context;LX/Fz2;LX/03V;LX/BPp;LX/Fyh;)V

    .line 2315875
    move-object v0, v5

    .line 2315876
    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->d:LX/Fz0;

    .line 2315877
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->d:LX/Fz0;

    invoke-virtual {v0}, LX/Fz0;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2315878
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->c:LX/G3F;

    invoke-interface {v0}, LX/G3F;->l()V

    .line 2315879
    :cond_0
    :goto_0
    return-void

    .line 2315880
    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->d:LX/Fz0;

    invoke-virtual {v1}, LX/Fz0;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 2315881
    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->f:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->d:LX/Fz0;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->f:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0, v3, v4}, LX/1Cv;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2315882
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2315883
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2315849
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2315850
    const-class v0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;

    invoke-static {v0, p0}, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2315851
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->a:LX/G3a;

    .line 2315852
    new-instance p1, LX/G3Z;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/BPp;->a(LX/0QB;)LX/BPp;

    move-result-object v3

    check-cast v3, LX/BPp;

    invoke-direct {p1, p0, v1, v2, v3}, LX/G3Z;-><init>(Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;LX/0tX;Ljava/util/concurrent/Executor;LX/BPp;)V

    .line 2315853
    move-object v0, p1

    .line 2315854
    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->e:LX/G3Z;

    .line 2315855
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->e:LX/G3Z;

    .line 2315856
    iget-object v1, v0, LX/G3Z;->f:LX/1B1;

    new-instance v2, LX/G3X;

    invoke-direct {v2, v0}, LX/G3X;-><init>(LX/G3Z;)V

    invoke-virtual {v1, v2}, LX/1B1;->a(LX/0b2;)Z

    .line 2315857
    new-instance v1, Lcom/facebook/timeline/inforeview/FetchTimelineInfoReviewParams;

    const-string v2, ""

    invoke-direct {v1, v2}, Lcom/facebook/timeline/inforeview/FetchTimelineInfoReviewParams;-><init>(Ljava/lang/String;)V

    .line 2315858
    new-instance v2, LX/5z3;

    invoke-direct {v2}, LX/5z3;-><init>()V

    move-object v2, v2

    .line 2315859
    const-string v3, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v2

    const-string v3, "ref"

    const-string p0, "android_plutonium_expando"

    invoke-virtual {v2, v3, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "session"

    .line 2315860
    iget-object p0, v1, Lcom/facebook/timeline/inforeview/FetchTimelineInfoReviewParams;->a:Ljava/lang/String;

    move-object p0, p0

    .line 2315861
    invoke-virtual {v2, v3, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "surface"

    const-string p0, "native_plutonium_header"

    invoke-virtual {v2, v3, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/5z3;

    move-object v1, v2

    .line 2315862
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2315863
    iget-object v2, v0, LX/G3Z;->b:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 2315864
    new-instance v2, LX/G3Y;

    invoke-direct {v2, v0}, LX/G3Y;-><init>(LX/G3Z;)V

    iget-object v3, v0, LX/G3Z;->c:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2315865
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x16a92244

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2315842
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2315843
    const v0, 0x7f0d277b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->f:Landroid/view/ViewGroup;

    .line 2315844
    const v0, 0x7f0d277c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2315845
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2315846
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->e:LX/G3Z;

    .line 2315847
    iget-object v2, v0, LX/G3Z;->a:Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;

    iget-object p0, v0, LX/G3Z;->e:LX/Fz2;

    invoke-virtual {v2, p0}, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->a(LX/Fz2;)V

    .line 2315848
    const/16 v0, 0x2b

    const v2, 0x3075829e

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2315829
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2315830
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 2315831
    :cond_0
    :goto_0
    return-void

    .line 2315832
    :cond_1
    const/16 v0, 0x7a4

    if-ne p1, v0, :cond_0

    .line 2315833
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->e:LX/G3Z;

    const-string v1, "typeahead_selected_page_id"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "typeahead_selected_page_name"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2315834
    iget-object p0, v0, LX/G3Z;->e:LX/Fz2;

    .line 2315835
    iget-object p1, p0, LX/Fz2;->f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    move-object p0, p1

    .line 2315836
    invoke-virtual {p0, v1}, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->a(Ljava/lang/String;)V

    .line 2315837
    iget-object p0, v0, LX/G3Z;->e:LX/Fz2;

    .line 2315838
    iget-object p1, p0, LX/Fz2;->f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    move-object p0, p1

    .line 2315839
    iput-object v2, p0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->e:Ljava/lang/String;

    .line 2315840
    invoke-virtual {v0}, LX/G3Z;->d()V

    .line 2315841
    goto :goto_0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x12d0eb26

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2315826
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/app/Activity;)V

    .line 2315827
    check-cast p1, LX/G3F;

    iput-object p1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->c:LX/G3F;

    .line 2315828
    const/16 v1, 0x2b

    const v2, -0x4ca5cb53

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x623e4f11

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2315825
    const v1, 0x7f031087

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x15beed84

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x644c2ccd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2315821
    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->e:LX/G3Z;

    .line 2315822
    iget-object v2, v1, LX/G3Z;->f:LX/1B1;

    iget-object v4, v1, LX/G3Z;->d:LX/BPp;

    invoke-virtual {v2, v4}, LX/1B1;->b(LX/0b4;)V

    .line 2315823
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2315824
    const/16 v1, 0x2b

    const v2, -0x5dde987f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1c721c0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2315817
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2315818
    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->e:LX/G3Z;

    .line 2315819
    iget-object v2, v1, LX/G3Z;->f:LX/1B1;

    iget-object p0, v1, LX/G3Z;->d:LX/BPp;

    invoke-virtual {v2, p0}, LX/1B1;->a(LX/0b4;)V

    .line 2315820
    const/16 v1, 0x2b

    const v2, 0x1df8aa2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
