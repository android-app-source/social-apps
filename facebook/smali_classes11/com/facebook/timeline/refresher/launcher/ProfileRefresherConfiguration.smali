.class public Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Z

.field public final f:Z

.field public final g:Z

.field public final h:Z

.field public final i:Z

.field public final j:LX/2rX;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2316204
    new-instance v0, LX/G3n;

    invoke-direct {v0}, LX/G3n;-><init>()V

    sput-object v0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/G3o;)V
    .locals 1

    .prologue
    .line 2316205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2316206
    iget-object v0, p1, LX/G3o;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->a:Ljava/lang/String;

    .line 2316207
    iget-object v0, p1, LX/G3o;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->b:Ljava/lang/String;

    .line 2316208
    iget-object v0, p1, LX/G3o;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->c:Ljava/lang/String;

    .line 2316209
    iget-object v0, p1, LX/G3o;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->d:Ljava/lang/String;

    .line 2316210
    iget-boolean v0, p1, LX/G3o;->e:Z

    iput-boolean v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->e:Z

    .line 2316211
    iget-boolean v0, p1, LX/G3o;->f:Z

    iput-boolean v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->f:Z

    .line 2316212
    iget-boolean v0, p1, LX/G3o;->g:Z

    iput-boolean v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->g:Z

    .line 2316213
    iget-boolean v0, p1, LX/G3o;->h:Z

    iput-boolean v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->h:Z

    .line 2316214
    iget-boolean v0, p1, LX/G3o;->i:Z

    iput-boolean v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->i:Z

    .line 2316215
    iget-object v0, p1, LX/G3o;->j:LX/2rX;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->j:LX/2rX;

    .line 2316216
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2316217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2316218
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->a:Ljava/lang/String;

    .line 2316219
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->b:Ljava/lang/String;

    .line 2316220
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->c:Ljava/lang/String;

    .line 2316221
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->d:Ljava/lang/String;

    .line 2316222
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->e:Z

    .line 2316223
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->f:Z

    .line 2316224
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->g:Z

    .line 2316225
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->h:Z

    .line 2316226
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->i:Z

    .line 2316227
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/2rX;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->j:LX/2rX;

    .line 2316228
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2316229
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2316230
    iget-object v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2316231
    iget-object v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2316232
    iget-object v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2316233
    iget-object v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2316234
    iget-boolean v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2316235
    iget-boolean v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2316236
    iget-boolean v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->g:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2316237
    iget-boolean v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->h:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2316238
    iget-boolean v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->i:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2316239
    iget-object v0, p0, Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;->j:LX/2rX;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2316240
    return-void
.end method
