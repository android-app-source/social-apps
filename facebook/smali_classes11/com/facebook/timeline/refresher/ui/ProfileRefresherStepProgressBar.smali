.class public Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;
.super Landroid/view/View;
.source ""


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/Paint;

.field private d:F

.field private e:I

.field public f:F

.field private g:I

.field private h:I

.field private i:I

.field public j:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2317101
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2317102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2317103
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2317104
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->a:Landroid/graphics/Paint;

    .line 2317105
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->b:Landroid/graphics/Paint;

    .line 2317106
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->c:Landroid/graphics/Paint;

    .line 2317107
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->i:I

    .line 2317108
    invoke-direct {p0}, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->a()V

    .line 2317109
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2317110
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->setWillNotDraw(Z)V

    .line 2317111
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2317112
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->b:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2317113
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2317114
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 4

    .prologue
    .line 2317115
    iput p2, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->h:I

    .line 2317116
    iget v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->i:I

    if-ne p1, v0, :cond_0

    .line 2317117
    :goto_0
    return-void

    .line 2317118
    :cond_0
    iput p1, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->i:I

    .line 2317119
    const/4 v0, 0x3

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 2317120
    new-instance v1, LX/G47;

    invoke-direct {v1, p0}, LX/G47;-><init>(Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2317121
    const-wide/16 v2, 0x320

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2317122
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2317123
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->invalidate()V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x3e4ccccd    # 0.2f
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2317124
    iget v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->h:I

    const/4 v2, 0x2

    if-ge v0, v2, :cond_1

    .line 2317125
    :cond_0
    return-void

    .line 2317126
    :cond_1
    iget v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->i:I

    if-lez v0, :cond_2

    .line 2317127
    new-instance v0, Landroid/graphics/RectF;

    iget v2, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->i:I

    int-to-float v2, v2

    iget v3, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->f:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->i:I

    add-int/lit8 v3, v3, -0x1

    iget v4, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->g:I

    mul-int/2addr v3, v4

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->e:I

    int-to-float v3, v3

    invoke-direct {v0, v1, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget v2, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->e:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget v3, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->e:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget-object v4, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 2317128
    :cond_2
    iget v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->i:I

    if-nez v0, :cond_3

    move v0, v1

    .line 2317129
    :goto_0
    new-instance v2, Landroid/graphics/RectF;

    iget v3, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->d:F

    iget v4, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->e:I

    int-to-float v4, v4

    invoke-direct {v2, v0, v1, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget v3, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->e:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v4, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->e:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    iget-object v5, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 2317130
    new-instance v2, Landroid/graphics/RectF;

    iget v3, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->j:F

    add-float/2addr v3, v0

    iget v4, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->e:I

    int-to-float v4, v4

    invoke-direct {v2, v0, v1, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->e:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v3, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->e:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget-object v4, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v0, v3, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 2317131
    const/4 v0, 0x1

    :goto_1
    iget v2, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->h:I

    if-ge v0, v2, :cond_0

    .line 2317132
    int-to-float v2, v0

    iget v3, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->f:F

    mul-float/2addr v2, v3

    add-int/lit8 v3, v0, -0x1

    iget v4, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->g:I

    mul-int/2addr v3, v4

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->e:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    .line 2317133
    new-instance v3, Landroid/graphics/RectF;

    iget v4, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->g:I

    int-to-float v4, v4

    add-float/2addr v4, v2

    iget v5, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->e:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget v5, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->e:I

    int-to-float v5, v5

    invoke-direct {v3, v2, v1, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget v2, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->e:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget v4, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->e:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    iget-object v5, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v2, v4, v5}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 2317134
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2317135
    :cond_3
    iget v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->i:I

    int-to-float v0, v0

    iget v2, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->f:F

    iget v3, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->g:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    mul-float/2addr v0, v2

    iget v2, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->e:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v0, v2

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 2317136
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0de7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->d:F

    .line 2317137
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0de9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->g:I

    .line 2317138
    iget v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->d:F

    iget v1, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->g:I

    iget v2, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->h:I

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v1, v2

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->h:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->f:F

    .line 2317139
    iget v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->f:F

    iget v1, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->h:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->g:I

    iget v2, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->h:I

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v1, v2

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->d:F

    .line 2317140
    invoke-virtual {p0}, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0de8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->e:I

    .line 2317141
    iget v0, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->d:F

    float-to-int v0, v0

    iget v1, p0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->e:I

    invoke-virtual {p0, v0, v1}, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->setMeasuredDimension(II)V

    .line 2317142
    return-void
.end method
