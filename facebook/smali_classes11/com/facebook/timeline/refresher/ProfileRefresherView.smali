.class public Lcom/facebook/timeline/refresher/ProfileRefresherView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/0h5;

.field public e:Landroid/view/ViewStub;

.field public f:Lcom/facebook/resources/ui/FbTextView;

.field public g:Lcom/facebook/resources/ui/FbTextView;

.field private h:Lcom/facebook/resources/ui/FbButton;

.field private i:Lcom/facebook/resources/ui/FbButton;

.field private j:Lcom/facebook/resources/ui/FbButton;

.field private k:Lcom/facebook/resources/ui/FbButton;

.field public l:Landroid/view/View;

.field public m:LX/G3d;

.field public n:Landroid/widget/FrameLayout;

.field public o:Landroid/widget/LinearLayout;

.field public p:Landroid/view/ViewStub;

.field private q:Landroid/view/ViewStub;

.field public r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public s:LX/G3P;

.field private t:Z

.field private final u:Landroid/view/View$OnClickListener;

.field public final v:LX/63W;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2316094
    const-class v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2316095
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2316096
    new-instance v0, LX/G3g;

    invoke-direct {v0, p0}, LX/G3g;-><init>(Lcom/facebook/timeline/refresher/ProfileRefresherView;)V

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->u:Landroid/view/View$OnClickListener;

    .line 2316097
    new-instance v0, LX/G3h;

    invoke-direct {v0, p0}, LX/G3h;-><init>(Lcom/facebook/timeline/refresher/ProfileRefresherView;)V

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->v:LX/63W;

    .line 2316098
    invoke-direct {p0}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->d()V

    .line 2316099
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2316100
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2316101
    new-instance v0, LX/G3g;

    invoke-direct {v0, p0}, LX/G3g;-><init>(Lcom/facebook/timeline/refresher/ProfileRefresherView;)V

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->u:Landroid/view/View$OnClickListener;

    .line 2316102
    new-instance v0, LX/G3h;

    invoke-direct {v0, p0}, LX/G3h;-><init>(Lcom/facebook/timeline/refresher/ProfileRefresherView;)V

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->v:LX/63W;

    .line 2316103
    invoke-direct {p0}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->d()V

    .line 2316104
    return-void
.end method

.method private static a(Lcom/facebook/timeline/refresher/ProfileRefresherView;LX/23P;LX/0ad;)V
    .locals 0

    .prologue
    .line 2316105
    iput-object p1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->b:LX/23P;

    iput-object p2, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->c:LX/0ad;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;

    invoke-static {v1}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v0

    check-cast v0, LX/23P;

    invoke-static {v1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0, v0, v1}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->a(Lcom/facebook/timeline/refresher/ProfileRefresherView;LX/23P;LX/0ad;)V

    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 2316106
    const-class v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2316107
    const v0, 0x7f031089

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2316108
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->setClickable(Z)V

    .line 2316109
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->c:LX/0ad;

    sget-short v1, LX/0wf;->aC:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->t:Z

    .line 2316110
    invoke-direct {p0}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->e()V

    .line 2316111
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 2316112
    const v0, 0x7f0d276f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2316113
    const v0, 0x7f0d2770

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->e:Landroid/view/ViewStub;

    .line 2316114
    const v0, 0x7f0d2771

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2316115
    const v0, 0x7f0d2772

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->n:Landroid/widget/FrameLayout;

    .line 2316116
    const v0, 0x7f0d276e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->o:Landroid/widget/LinearLayout;

    .line 2316117
    const v0, 0x7f0d277e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->p:Landroid/view/ViewStub;

    .line 2316118
    const v0, 0x7f0d277f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->q:Landroid/view/ViewStub;

    .line 2316119
    const v0, 0x7f0d0a95

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2316120
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 2316121
    invoke-static {p0}, LX/63Z;->a(Landroid/view/View;)Z

    .line 2316122
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->d:LX/0h5;

    .line 2316123
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->d:LX/0h5;

    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->u:Landroid/view/View$OnClickListener;

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2316124
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->d:LX/0h5;

    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->v:LX/63W;

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2316125
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2316064
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->p:Landroid/view/ViewStub;

    const v1, 0x7f031082

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2316065
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->p:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->l:Landroid/view/View;

    .line 2316066
    const v0, 0x7f0d2773

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->j:Lcom/facebook/resources/ui/FbButton;

    .line 2316067
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->j:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/G3i;

    invoke-direct {v1, p0}, LX/G3i;-><init>(Lcom/facebook/timeline/refresher/ProfileRefresherView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2316068
    return-void
.end method

.method public final a(LX/0gc;LX/G3c;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2316126
    const/4 v7, 0x0

    .line 2316127
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2316128
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2316129
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 2316130
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {p2, v0}, LX/G3c;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {p2, v0}, LX/G3c;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {p2, v0}, LX/G3c;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2316131
    :cond_0
    const-string v0, "nux_refresher_header_fragment"

    invoke-static {p1, p3, v0}, LX/G3e;->a(LX/0gc;Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2316132
    instance-of v4, v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    if-eqz v4, :cond_5

    .line 2316133
    check-cast v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    .line 2316134
    :goto_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2316135
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2316136
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2316137
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2316138
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2316139
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2316140
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2316141
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_FEATURED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {p2, v0}, LX/G3c;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2316142
    const-string v0, "nux_refresher_featured_photos_fragment"

    invoke-static {p1, p3, v0}, LX/G3e;->a(LX/0gc;Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2316143
    instance-of v4, v0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherFeaturedPhotosFragment;

    if-eqz v4, :cond_6

    .line 2316144
    check-cast v0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherFeaturedPhotosFragment;

    .line 2316145
    :goto_1
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2316146
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_FEATURED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2316147
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_FEATURED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2316148
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->CORE_PROFILE_FIELD:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {p2, v0}, LX/G3c;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2316149
    const-string v0, "nux_refresher_info_fragment"

    invoke-static {p1, p3, v0}, LX/G3e;->a(LX/0gc;Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2316150
    instance-of v4, v0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;

    if-eqz v4, :cond_7

    .line 2316151
    check-cast v0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;

    .line 2316152
    :goto_2
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2316153
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->CORE_PROFILE_FIELD:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2316154
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->CORE_PROFILE_FIELD:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2316155
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COMPOSER:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {p2, v0}, LX/G3c;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2316156
    const-string v0, "nux_refresher_composer_fragment"

    invoke-static {p1, p3, v0}, LX/G3e;->a(LX/0gc;Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2316157
    instance-of v4, v0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;

    if-eqz v4, :cond_8

    .line 2316158
    check-cast v0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;

    .line 2316159
    :goto_3
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2316160
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COMPOSER:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2316161
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COMPOSER:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2316162
    :cond_4
    const-string v0, "nux_refresher_finished_fragment"

    invoke-static {p1, p3, v0}, LX/G3e;->a(LX/0gc;Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2316163
    instance-of v4, v0, Lcom/facebook/timeline/refresher/ProfileRefresherFinishedFragment;

    if-eqz v4, :cond_9

    .line 2316164
    check-cast v0, Lcom/facebook/timeline/refresher/ProfileRefresherFinishedFragment;

    .line 2316165
    :goto_4
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2316166
    invoke-virtual {v2, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2316167
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2316168
    new-instance v0, LX/G3d;

    invoke-direct {v0, p1, v1, v2, v3}, LX/G3d;-><init>(LX/0gc;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)V

    move-object v0, v0

    .line 2316169
    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    .line 2316170
    return-void

    .line 2316171
    :cond_5
    new-instance v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    invoke-direct {v0}, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;-><init>()V

    goto/16 :goto_0

    .line 2316172
    :cond_6
    new-instance v0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherFeaturedPhotosFragment;

    invoke-direct {v0}, Lcom/facebook/timeline/refresher/ProfileNuxRefresherFeaturedPhotosFragment;-><init>()V

    goto/16 :goto_1

    .line 2316173
    :cond_7
    new-instance v0, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;

    invoke-direct {v0}, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;-><init>()V

    goto :goto_2

    .line 2316174
    :cond_8
    new-instance v0, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;

    invoke-direct {v0}, Lcom/facebook/timeline/refresher/ProfileNuxComposerFragment;-><init>()V

    .line 2316175
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2316176
    const-string v5, "post_item_privacy"

    .line 2316177
    iget-object v6, p2, LX/G3c;->k:LX/2rX;

    move-object v6, v6

    .line 2316178
    invoke-static {v4, v5, v6}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2316179
    invoke-virtual {v0, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_3

    .line 2316180
    :cond_9
    new-instance v0, Lcom/facebook/timeline/refresher/ProfileRefresherFinishedFragment;

    invoke-direct {v0}, Lcom/facebook/timeline/refresher/ProfileRefresherFinishedFragment;-><init>()V

    goto :goto_4
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2316181
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->p:Landroid/view/ViewStub;

    const v1, 0x7f031083

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2316182
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->p:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->l:Landroid/view/View;

    .line 2316183
    const v0, 0x7f0d2774    # 1.87626E38f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->h:Lcom/facebook/resources/ui/FbButton;

    .line 2316184
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->h:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->b:LX/23P;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2316185
    const v0, 0x7f0d2775

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->i:Lcom/facebook/resources/ui/FbButton;

    .line 2316186
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->i:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->b:LX/23P;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2316187
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->h:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/G3j;

    invoke-direct {v1, p0}, LX/G3j;-><init>(Lcom/facebook/timeline/refresher/ProfileRefresherView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2316188
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->i:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/G3k;

    invoke-direct {v1, p0}, LX/G3k;-><init>(Lcom/facebook/timeline/refresher/ProfileRefresherView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2316189
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2316190
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->q:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2316191
    :goto_0
    return-void

    .line 2316192
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->p:Landroid/view/ViewStub;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 2316193
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->q:Landroid/view/ViewStub;

    const v1, 0x7f031082

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2316194
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->q:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->l:Landroid/view/View;

    .line 2316195
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->l:Landroid/view/View;

    const v1, 0x7f0d2773

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->k:Lcom/facebook/resources/ui/FbButton;

    goto :goto_0
.end method

.method public getAdapter()LX/G3d;
    .locals 1

    .prologue
    .line 2316196
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    return-object v0
.end method

.method public getBottomPhotoBarStub()Landroid/view/ViewStub;
    .locals 1

    .prologue
    .line 2316093
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->p:Landroid/view/ViewStub;

    return-object v0
.end method

.method public getHolder()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 2316197
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->n:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public getLoadingIndicatorView()Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;
    .locals 1

    .prologue
    .line 2316063
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    return-object v0
.end method

.method public getPhotoBar()Landroid/view/View;
    .locals 1

    .prologue
    .line 2316069
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->l:Landroid/view/View;

    return-object v0
.end method

.method public getProgressBarViewStub()Landroid/view/ViewStub;
    .locals 1

    .prologue
    .line 2316070
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->e:Landroid/view/ViewStub;

    return-object v0
.end method

.method public getSkipListener()LX/63W;
    .locals 1

    .prologue
    .line 2316071
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->v:LX/63W;

    return-object v0
.end method

.method public getStepDescriptionTextView()Lcom/facebook/resources/ui/FbTextView;
    .locals 1

    .prologue
    .line 2316072
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->g:Lcom/facebook/resources/ui/FbTextView;

    return-object v0
.end method

.method public getStepTextLayout()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 2316073
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->o:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getStepTitleTextView()Lcom/facebook/resources/ui/FbTextView;
    .locals 1

    .prologue
    .line 2316074
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->f:Lcom/facebook/resources/ui/FbTextView;

    return-object v0
.end method

.method public setBottomBioBar(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2316075
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->k:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2316076
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->k:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/G3l;

    invoke-direct {v1, p0}, LX/G3l;-><init>(Lcom/facebook/timeline/refresher/ProfileRefresherView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2316077
    return-void
.end method

.method public setBottomFeaturedPhotosBar(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2316078
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->k:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2316079
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->k:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/G3m;

    invoke-direct {v1, p0}, LX/G3m;-><init>(Lcom/facebook/timeline/refresher/ProfileRefresherView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2316080
    return-void
.end method

.method public setBottomPhotoBarText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2316081
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->j:Lcom/facebook/resources/ui/FbButton;

    if-nez v0, :cond_0

    .line 2316082
    :goto_0
    return-void

    .line 2316083
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->j:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setListener(LX/G3P;)V
    .locals 0

    .prologue
    .line 2316084
    iput-object p1, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->s:LX/G3P;

    .line 2316085
    invoke-direct {p0}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->f()V

    .line 2316086
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2316087
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->d:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2316088
    return-void
.end method

.method public setTitleBarButtonListener(LX/63W;)V
    .locals 1

    .prologue
    .line 2316089
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->d:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2316090
    return-void
.end method

.method public setTitleBarButtonSpecs(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2316091
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->d:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2316092
    return-void
.end method
