.class public Lcom/facebook/timeline/TimelineFragment;
.super Lcom/facebook/timeline/BaseTimelineFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/0fg;
.implements LX/0fn;
.implements LX/0yL;
.implements LX/0gr;
.implements LX/23S;
.implements LX/0o2;
.implements LX/5vN;
.implements LX/Fq3;
.implements LX/FqO;
.implements LX/63S;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->NATIVE_TIMELINE_FRAGMENT:LX/0cQ;
.end annotation

.annotation runtime Lcom/facebook/search/interfaces/GraphSearchTitleSupport;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/timeline/BaseTimelineFragment",
        "<",
        "LX/G0n;",
        ">;",
        "LX/0hF;",
        "LX/0fg;",
        "LX/0fn;",
        "LX/0yL;",
        "LX/0gr;",
        "LX/23S;",
        "LX/0o2;",
        "LX/5vN;",
        "LX/Fq3;",
        "LX/FqO;",
        "Lcom/facebook/timeline/delegate/TimelineFragmentHeaderFetchCallbackDelegate$Listener;",
        "Lcom/facebook/timeline/header/TimelineHeaderPerfController$Listener;",
        "Lcom/facebook/timeline/header/menus/TimelineFriendingClient$ViewCallback;",
        "LX/63S;"
    }
.end annotation


# static fields
.field public static final an:Lcom/facebook/common/callercontext/CallerContext;

.field private static bh:I


# instance fields
.field public A:LX/BPp;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public B:LX/2do;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public C:LX/0b3;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public D:LX/BPs;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public E:LX/BPC;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public F:LX/G4x;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public G:LX/G2a;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public H:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public I:LX/BQ9;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public J:LX/2SI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public K:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fsw;",
            ">;"
        }
    .end annotation
.end field

.field public L:LX/Ft1;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public M:LX/1dr;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public N:LX/Fq8;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public O:LX/0zG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public P:LX/G0o;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public Q:LX/G0p;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public R:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation
.end field

.field public S:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2iz;",
            ">;"
        }
    .end annotation
.end field

.field public T:LX/Fw1;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public U:LX/Fv8;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public V:LX/FvA;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public W:LX/1lu;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public X:LX/FrW;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public Y:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1mR;",
            ">;"
        }
    .end annotation
.end field

.field public Z:Lcom/facebook/common/perftest/PerfTestConfig;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private aA:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fry;",
            ">;"
        }
    .end annotation
.end field

.field private aB:LX/Fy4;

.field public aC:LX/0QR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<",
            "LX/FwT;",
            ">;"
        }
    .end annotation
.end field

.field public aD:LX/1PF;

.field public aE:LX/63T;

.field private aF:Z

.field public aG:LX/0ta;

.field public aH:LX/0ta;

.field public aI:LX/63Q;

.field private aJ:LX/FwE;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aK:LX/Fw9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aL:LX/Fqc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aM:LX/FrF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aN:LX/G0u;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aO:LX/G2b;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aP:Z

.field public aQ:LX/FsE;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aR:LX/FsJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aS:Z

.field public aT:Z

.field private aU:Z

.field public aV:LX/FvM;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aW:LX/1B1;

.field private aX:LX/1B1;

.field public aY:LX/1B1;

.field public aZ:LX/BP0;

.field public aa:LX/FuJ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public ab:LX/FtB;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public ac:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public ad:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public ae:LX/FwS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public af:LX/0yc;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public ag:LX/Fsr;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public ah:LX/G10;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public ai:LX/G49;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public aj:LX/G4p;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public ak:LX/G35;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public al:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/DJ6;",
            ">;"
        }
    .end annotation
.end field

.field public am:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final ao:Landroid/graphics/Rect;

.field public ap:LX/G4q;

.field public aq:Landroid/support/v4/widget/SwipeRefreshLayout;

.field public ar:LX/0g8;

.field private as:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private at:Z

.field private au:Z

.field private av:Z

.field private aw:Z

.field public ax:LX/G0n;

.field public ay:LX/FrV;

.field private az:Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private bA:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/wem/ProfileComposerLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private bB:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field

.field private bC:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1EV;",
            ">;"
        }
    .end annotation
.end field

.field private bD:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private bE:LX/DJ6;

.field public bF:Z

.field public bG:Lcom/facebook/delights/floating/DelightsFireworks;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public bH:LX/1Ar;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private bI:LX/FtA;

.field private bJ:LX/2f1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2f1",
            "<",
            "LX/2f2;",
            ">;"
        }
    .end annotation
.end field

.field private bK:LX/BPt;

.field public bL:LX/8Kc;

.field public ba:LX/BQ1;

.field private bb:LX/G4m;

.field private bc:LX/G1N;

.field public bd:LX/FrH;

.field private be:LX/BQG;

.field private bf:LX/G5B;

.field public bg:LX/BQB;

.field private bi:LX/BQ5;

.field private bj:LX/Ft0;

.field private bk:LX/0QR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<",
            "LX/Fq7;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bl:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1HI;",
            ">;"
        }
    .end annotation
.end field

.field private bm:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private bn:LX/0QR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<",
            "LX/Fw0;",
            ">;"
        }
    .end annotation
.end field

.field public bo:LX/Fv9;

.field private bp:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5Mk;",
            ">;"
        }
    .end annotation
.end field

.field private bq:LX/FuR;

.field private br:LX/FuI;

.field private bs:LX/0Yb;

.field private bt:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/G4A;",
            ">;"
        }
    .end annotation
.end field

.field public bu:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wL;",
            ">;"
        }
    .end annotation
.end field

.field private bv:LX/1Jg;

.field private bw:LX/G48;

.field private bx:LX/G34;

.field public by:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BQT;",
            ">;"
        }
    .end annotation
.end field

.field public bz:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FvX;",
            ">;"
        }
    .end annotation
.end field

.field public volatile e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public volatile f:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fq4;",
            ">;"
        }
    .end annotation
.end field

.field public volatile g:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/callercontexttagger/AnalyticsTagger;",
            ">;"
        }
    .end annotation
.end field

.field public volatile h:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G6O;",
            ">;"
        }
    .end annotation
.end field

.field public volatile i:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G60;",
            ">;"
        }
    .end annotation
.end field

.field public volatile j:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field public volatile k:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G1I;",
            ">;"
        }
    .end annotation
.end field

.field public volatile l:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BQG;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/G4l;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/Fy5;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public o:LX/FwU;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public p:LX/1CF;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:LX/Fqd;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public r:LX/FrG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public s:LX/G0y;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:LX/FvP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public u:LX/G2e;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public v:LX/FyE;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public w:LX/FwB;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public x:LX/FwH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public y:LX/G4f;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public z:LX/FrM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2293607
    const-class v0, Lcom/facebook/timeline/TimelineFragment;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/TimelineFragment;->an:Lcom/facebook/common/callercontext/CallerContext;

    .line 2293608
    const/4 v0, 0x0

    sput v0, Lcom/facebook/timeline/TimelineFragment;->bh:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2293573
    invoke-direct {p0}, Lcom/facebook/timeline/BaseTimelineFragment;-><init>()V

    .line 2293574
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ao:Landroid/graphics/Rect;

    .line 2293575
    iput-boolean v1, p0, Lcom/facebook/timeline/TimelineFragment;->at:Z

    .line 2293576
    iput-boolean v1, p0, Lcom/facebook/timeline/TimelineFragment;->au:Z

    .line 2293577
    iput-boolean v1, p0, Lcom/facebook/timeline/TimelineFragment;->av:Z

    .line 2293578
    iput-boolean v1, p0, Lcom/facebook/timeline/TimelineFragment;->aw:Z

    .line 2293579
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2293580
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aA:LX/0Ot;

    .line 2293581
    iput-boolean v2, p0, Lcom/facebook/timeline/TimelineFragment;->aF:Z

    .line 2293582
    sget-object v0, LX/0ta;->NO_DATA:LX/0ta;

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aG:LX/0ta;

    .line 2293583
    sget-object v0, LX/0ta;->NO_DATA:LX/0ta;

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aH:LX/0ta;

    .line 2293584
    iput-boolean v1, p0, Lcom/facebook/timeline/TimelineFragment;->aP:Z

    .line 2293585
    iput-boolean v2, p0, Lcom/facebook/timeline/TimelineFragment;->aS:Z

    .line 2293586
    iput-boolean v1, p0, Lcom/facebook/timeline/TimelineFragment;->aT:Z

    .line 2293587
    iput-boolean v1, p0, Lcom/facebook/timeline/TimelineFragment;->aU:Z

    .line 2293588
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2293589
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bl:LX/0Ot;

    .line 2293590
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2293591
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bm:LX/0Ot;

    .line 2293592
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2293593
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bp:LX/0Ot;

    .line 2293594
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2293595
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bt:LX/0Ot;

    .line 2293596
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2293597
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bu:LX/0Ot;

    .line 2293598
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2293599
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->by:LX/0Ot;

    .line 2293600
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2293601
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bA:LX/0Ot;

    .line 2293602
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2293603
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bB:LX/0Ot;

    .line 2293604
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2293605
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bC:LX/0Ot;

    .line 2293606
    return-void
.end method

.method private S()V
    .locals 3

    .prologue
    .line 2293570
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    if-nez v0, :cond_1

    .line 2293571
    :cond_0
    :goto_0
    return-void

    .line 2293572
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bB:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    const/16 v1, 0x6dc

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1Kf;->a(ILandroid/app/Activity;)V

    goto :goto_0
.end method

.method private T()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 2293542
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 2293543
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ap:LX/G4q;

    const v1, 0x7f0d2f37

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2293544
    const-string v1, "show_footer"

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2293545
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->i:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/G60;

    .line 2293546
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/G60;->a(Landroid/os/Bundle;Landroid/view/ViewStub;)V

    .line 2293547
    :cond_0
    :goto_0
    return-void

    .line 2293548
    :cond_1
    const-string v1, "show_watermark_footer"

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2293549
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->h:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/G6O;

    .line 2293550
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 2293551
    const-string v3, "analytics_params"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    iput-object v3, v1, LX/G6O;->d:Ljava/util/HashMap;

    .line 2293552
    iget-object v3, v1, LX/G6O;->d:Ljava/util/HashMap;

    if-eqz v3, :cond_2

    iget-object v3, v1, LX/G6O;->d:Ljava/util/HashMap;

    const-string v4, "old_profile_picture"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2293553
    :cond_2
    :goto_1
    goto :goto_0

    .line 2293554
    :cond_3
    iget-object v3, v1, LX/G6O;->d:Ljava/util/HashMap;

    const-string v4, "old_profile_picture"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2293555
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v4

    iput-object v4, v1, LX/G6O;->c:Landroid/view/View;

    .line 2293556
    iget-object v4, v1, LX/G6O;->c:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2293557
    const v5, 0x7f08156c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2293558
    const v6, 0x7f081566

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2293559
    iget-object v4, v1, LX/G6O;->c:Landroid/view/View;

    const v7, 0x7f0d2b03

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 2293560
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2293561
    new-instance v7, LX/47x;

    invoke-virtual {v4}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-direct {v7, p0}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v7, v5}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v7

    const-string p0, "link_click_here"

    .line 2293562
    new-instance v2, LX/G6N;

    invoke-direct {v2, v1, v3, v4}, LX/G6N;-><init>(LX/G6O;Ljava/lang/String;Landroid/widget/TextView;)V

    move-object v2, v2

    .line 2293563
    const/16 v0, 0x21

    invoke-virtual {v7, p0, v6, v2, v0}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v7

    .line 2293564
    invoke-virtual {v7}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v7

    move-object v3, v7

    .line 2293565
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2293566
    iget-object v3, v1, LX/G6O;->c:Landroid/view/View;

    const v4, 0x7f0d2b04

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, LX/G6L;

    invoke-direct {v4, v1}, LX/G6L;-><init>(LX/G6O;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2293567
    iget-object v3, v1, LX/G6O;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/G6Q;

    iget-object v4, v1, LX/G6O;->d:Ljava/util/HashMap;

    .line 2293568
    const-string v5, "fb4a_watermark_viewed_privacy_notice"

    invoke-static {v3, v5, v4}, LX/G6Q;->a(LX/G6Q;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 2293569
    goto :goto_1
.end method

.method private static U(Lcom/facebook/timeline/TimelineFragment;)V
    .locals 11

    .prologue
    .line 2293527
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v0}, LX/BPy;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2293528
    :cond_0
    :goto_0
    return-void

    .line 2293529
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2293530
    const-string v0, "composer"

    const-string v2, "composer"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2293531
    const-string v0, "composer"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 2293532
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bA:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/wem/ProfileComposerLauncher;

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    iget-object v4, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    .line 2293533
    iget-object v5, v0, Lcom/facebook/wem/ProfileComposerLauncher;->d:LX/0Uh;

    const/16 v6, 0x24a

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, LX/0Uh;->a(IZ)Z

    move-result v5

    move v5, v5

    .line 2293534
    if-nez v5, :cond_2

    .line 2293535
    :goto_1
    goto :goto_0

    .line 2293536
    :cond_2
    if-eqz v1, :cond_3

    const-string v5, "attachment_url"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2293537
    const-string v5, "attachment_url"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2293538
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 2293539
    invoke-static {v5}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v5, v0

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    .line 2293540
    invoke-static/range {v5 .. v10}, Lcom/facebook/wem/ProfileComposerLauncher;->a(Lcom/facebook/wem/ProfileComposerLauncher;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;LX/5SB;LX/BQ1;)V

    goto :goto_1

    .line 2293541
    :cond_3
    const/4 v10, 0x0

    move-object v5, v0

    move-object v6, v1

    move-object v7, v2

    move-object v8, v3

    move-object v9, v4

    invoke-static/range {v5 .. v10}, Lcom/facebook/wem/ProfileComposerLauncher;->a$redex0(Lcom/facebook/wem/ProfileComposerLauncher;Landroid/os/Bundle;Landroid/app/Activity;LX/5SB;LX/BQ1;LX/0Px;)V

    goto :goto_1
.end method

.method private V()V
    .locals 2

    .prologue
    .line 2293524
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_action_on_fragment_create_create_profile_video"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2293525
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ag:LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->p()LX/FwE;

    move-result-object v0

    invoke-interface {v0}, LX/FwE;->d()V

    .line 2293526
    :cond_0
    return-void
.end method

.method private X()V
    .locals 3

    .prologue
    .line 2293519
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2293520
    const-string v1, "extra_launch_profile_photo_selector"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    invoke-virtual {v1}, LX/5SB;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2293521
    const-string v1, "extra_launch_profile_photo_selector"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 2293522
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ag:LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->p()LX/FwE;

    move-result-object v0

    invoke-interface {v0}, LX/FwE;->b()V

    .line 2293523
    :cond_0
    return-void
.end method

.method private Y()V
    .locals 3

    .prologue
    .line 2293514
    const-string v0, "intro_card"

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "intro_card"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2293515
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    if-eqz v0, :cond_0

    .line 2293516
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    const/4 v1, 0x1

    .line 2293517
    iput-boolean v1, v0, LX/BQ1;->c:Z

    .line 2293518
    :cond_0
    return-void
.end method

.method private a(JLX/36O;)V
    .locals 5

    .prologue
    .line 2293478
    const-string v0, "TimelineFragment.onFragmentCreate.fetchTimeline"

    const v1, -0x73f7ef9d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2293479
    :try_start_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bd:LX/FrH;

    if-eqz v0, :cond_0

    .line 2293480
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bd:LX/FrH;

    invoke-virtual {v0}, LX/BPB;->g()V

    .line 2293481
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2293482
    new-instance v0, LX/FrY;

    invoke-direct {v0, p1, p2}, LX/FrY;-><init>(J)V

    .line 2293483
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->az:Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;

    invoke-virtual {v1, v0}, LX/98h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FrX;

    .line 2293484
    if-eqz v0, :cond_1

    .line 2293485
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bg:LX/BQB;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->az:Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;

    invoke-virtual {v2}, LX/98h;->d()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/BQB;->a(J)V

    .line 2293486
    :cond_1
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    sget-object v2, Lcom/facebook/timeline/TimelineFragment;->an:Lcom/facebook/common/callercontext/CallerContext;

    .line 2293487
    invoke-virtual {v1}, LX/FrV;->a()LX/Fra;

    move-result-object v3

    .line 2293488
    iget-object v4, v3, LX/Fra;->c:LX/BQB;

    if-eqz v4, :cond_2

    .line 2293489
    iget-object v4, v3, LX/Fra;->c:LX/BQB;

    invoke-virtual {v4}, LX/BQB;->f()V

    .line 2293490
    invoke-static {p3}, LX/FsN;->a(LX/36O;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2293491
    iget-object v4, v3, LX/Fra;->c:LX/BQB;

    .line 2293492
    iget-object p1, v4, LX/BQB;->c:LX/BQD;

    const-string v1, "TimelineFetchProfilePicUri"

    invoke-virtual {p1, v1}, LX/BQD;->a(Ljava/lang/String;)V

    .line 2293493
    :cond_2
    invoke-static {v3, v0, p3, v2}, LX/Fra;->b(LX/Fra;LX/FrX;LX/36O;Lcom/facebook/common/callercontext/CallerContext;)LX/FrX;

    move-result-object v4

    .line 2293494
    invoke-static {v3}, LX/Fra;->e(LX/Fra;)LX/Fs2;

    move-result-object p1

    iget-object v1, v4, LX/FrX;->a:LX/FsE;

    iget-object v1, v1, LX/FsE;->b:LX/1Zp;

    invoke-virtual {p1, v1}, LX/Fs2;->a(LX/1Zp;)V

    .line 2293495
    move-object v3, v4

    .line 2293496
    move-object v1, v3

    .line 2293497
    iget-object v1, v1, LX/FrX;->a:LX/FsE;

    iput-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aQ:LX/FsE;

    .line 2293498
    sget-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->f:Z

    move v1, v1

    .line 2293499
    if-eqz v0, :cond_3

    iget-object v0, v0, LX/FrX;->b:LX/FsJ;

    :goto_0
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aR:LX/FsJ;

    .line 2293500
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->aR:LX/FsJ;

    .line 2293501
    if-eqz v2, :cond_4

    .line 2293502
    invoke-virtual {v0}, LX/FrV;->b()LX/Frd;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/Frd;->a(LX/FsJ;)V

    .line 2293503
    :goto_1
    const/4 v4, 0x0

    .line 2293504
    iget-object v3, v0, LX/FrV;->a:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0ad;

    .line 2293505
    sget-short v1, LX/0wf;->H:S

    invoke-interface {v3, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v3

    if-nez v3, :cond_6

    const/4 v3, 0x1

    :goto_2
    move v3, v3

    .line 2293506
    if-eqz v3, :cond_5

    .line 2293507
    invoke-virtual {v0}, LX/FrV;->b()LX/Frd;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/Frd;->b(LX/FsJ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2293508
    :goto_3
    const v0, -0x7a70f2b0

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2293509
    return-void

    .line 2293510
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 2293511
    :catchall_0
    move-exception v0

    const v1, -0x227cd030

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2293512
    :cond_4
    invoke-virtual {v0}, LX/FrV;->b()LX/Frd;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/Frd;->a(Z)LX/FsJ;

    move-result-object v2

    goto :goto_1

    .line 2293513
    :cond_5
    iput-object v2, v0, LX/FrV;->o:LX/FsJ;

    goto :goto_3

    :cond_6
    move v3, v4

    goto :goto_2
.end method

.method private static a(Lcom/facebook/timeline/TimelineFragment;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;LX/0Ot;LX/G4l;LX/Fy5;LX/FwU;LX/1CF;LX/Fqd;LX/FrG;LX/G0y;LX/FvP;LX/G2e;LX/FyE;LX/FwB;LX/FwH;LX/G4f;LX/FrM;LX/BPp;LX/2do;LX/0b3;LX/BPs;LX/BPC;LX/G4x;LX/G2a;Ljava/lang/String;LX/BQ9;LX/2SI;LX/0Or;LX/Ft1;LX/1dr;LX/Fq8;LX/0zG;LX/G0o;LX/G0p;LX/0Or;LX/0Ot;LX/0Ot;LX/0Or;LX/Fw1;LX/Fv8;LX/FvA;LX/1lu;LX/FrW;LX/0Ot;LX/0Or;Lcom/facebook/common/perftest/PerfTestConfig;LX/FuJ;LX/FtB;LX/0Xl;LX/0Ot;LX/0Ot;Landroid/os/Handler;LX/FwS;LX/0yc;LX/Fsr;LX/G10;LX/G49;LX/G4p;LX/G35;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/0Or;Ljava/lang/Boolean;Lcom/facebook/delights/floating/DelightsFireworks;LX/1Ar;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/TimelineFragment;",
            "LX/0Or",
            "<",
            "LX/FvX;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0ad;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Fq4;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/common/callercontexttagger/AnalyticsTagger;",
            ">;",
            "LX/0Or",
            "<",
            "LX/G6O;",
            ">;",
            "LX/0Or",
            "<",
            "LX/G60;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0SG;",
            ">;",
            "LX/0Or",
            "<",
            "LX/G1I;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BQG;",
            ">;",
            "Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;",
            "LX/0Ot",
            "<",
            "LX/Fry;",
            ">;",
            "LX/G4l;",
            "LX/Fy5;",
            "LX/FwU;",
            "LX/1CF;",
            "LX/Fqd;",
            "LX/FrG;",
            "LX/G0y;",
            "LX/FvP;",
            "LX/G2e;",
            "LX/FyE;",
            "LX/FwB;",
            "LX/FwH;",
            "LX/G4f;",
            "LX/FrM;",
            "LX/BPp;",
            "LX/2do;",
            "LX/0b3;",
            "LX/BPs;",
            "LX/BPC;",
            "LX/G4x;",
            "LX/G2a;",
            "Ljava/lang/String;",
            "LX/BQ9;",
            "LX/2SI;",
            "LX/0Or",
            "<",
            "LX/Fsw;",
            ">;",
            "LX/Ft1;",
            "LX/1dr;",
            "LX/Fq8;",
            "LX/0zG;",
            "LX/G0o;",
            "LX/G0p;",
            "LX/0Or",
            "<",
            "LX/0kb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1HI;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2iz;",
            ">;",
            "LX/Fw1;",
            "LX/Fv8;",
            "LX/FvA;",
            "LX/1lu;",
            "LX/FrW;",
            "LX/0Ot",
            "<",
            "LX/5Mk;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1mR;",
            ">;",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            "LX/FuJ;",
            "LX/FtB;",
            "LX/0Xl;",
            "LX/0Ot",
            "<",
            "LX/G4A;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0wL;",
            ">;",
            "Landroid/os/Handler;",
            "LX/FwS;",
            "LX/0yc;",
            "LX/Fsr;",
            "LX/G10;",
            "LX/G49;",
            "LX/G4p;",
            "LX/G35;",
            "LX/0Ot",
            "<",
            "LX/BQT;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/wem/ProfileComposerLauncher;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1EV;",
            ">;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/DJ6;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/facebook/delights/floating/DelightsFireworks;",
            "LX/1Ar;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2293477
    iput-object p1, p0, Lcom/facebook/timeline/TimelineFragment;->d:LX/0Or;

    iput-object p2, p0, Lcom/facebook/timeline/TimelineFragment;->e:LX/0Or;

    iput-object p3, p0, Lcom/facebook/timeline/TimelineFragment;->f:LX/0Or;

    iput-object p4, p0, Lcom/facebook/timeline/TimelineFragment;->g:LX/0Or;

    iput-object p5, p0, Lcom/facebook/timeline/TimelineFragment;->h:LX/0Or;

    iput-object p6, p0, Lcom/facebook/timeline/TimelineFragment;->i:LX/0Or;

    iput-object p7, p0, Lcom/facebook/timeline/TimelineFragment;->j:LX/0Or;

    iput-object p8, p0, Lcom/facebook/timeline/TimelineFragment;->k:LX/0Or;

    iput-object p9, p0, Lcom/facebook/timeline/TimelineFragment;->l:LX/0Or;

    iput-object p10, p0, Lcom/facebook/timeline/TimelineFragment;->az:Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;

    iput-object p11, p0, Lcom/facebook/timeline/TimelineFragment;->aA:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/timeline/TimelineFragment;->m:LX/G4l;

    iput-object p13, p0, Lcom/facebook/timeline/TimelineFragment;->n:LX/Fy5;

    iput-object p14, p0, Lcom/facebook/timeline/TimelineFragment;->o:LX/FwU;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->p:LX/1CF;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->q:LX/Fqd;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->r:LX/FrG;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->s:LX/G0y;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->t:LX/FvP;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->u:LX/G2e;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->v:LX/FyE;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->w:LX/FwB;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->x:LX/FwH;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->y:LX/G4f;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->z:LX/FrM;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->A:LX/BPp;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->B:LX/2do;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->C:LX/0b3;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->D:LX/BPs;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->E:LX/BPC;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->F:LX/G4x;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->G:LX/G2a;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->H:Ljava/lang/String;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->I:LX/BQ9;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->J:LX/2SI;

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->K:LX/0Or;

    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->L:LX/Ft1;

    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->M:LX/1dr;

    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->N:LX/Fq8;

    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->O:LX/0zG;

    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->P:LX/G0o;

    move-object/from16 v0, p42

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->Q:LX/G0p;

    move-object/from16 v0, p43

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->R:LX/0Or;

    move-object/from16 v0, p44

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bl:LX/0Ot;

    move-object/from16 v0, p45

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bm:LX/0Ot;

    move-object/from16 v0, p46

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->S:LX/0Or;

    move-object/from16 v0, p47

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->T:LX/Fw1;

    move-object/from16 v0, p48

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->U:LX/Fv8;

    move-object/from16 v0, p49

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->V:LX/FvA;

    move-object/from16 v0, p50

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->W:LX/1lu;

    move-object/from16 v0, p51

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->X:LX/FrW;

    move-object/from16 v0, p52

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bp:LX/0Ot;

    move-object/from16 v0, p53

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->Y:LX/0Or;

    move-object/from16 v0, p54

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->Z:Lcom/facebook/common/perftest/PerfTestConfig;

    move-object/from16 v0, p55

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aa:LX/FuJ;

    move-object/from16 v0, p56

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ab:LX/FtB;

    move-object/from16 v0, p57

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ac:LX/0Xl;

    move-object/from16 v0, p58

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bt:LX/0Ot;

    move-object/from16 v0, p59

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bu:LX/0Ot;

    move-object/from16 v0, p60

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ad:Landroid/os/Handler;

    move-object/from16 v0, p61

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ae:LX/FwS;

    move-object/from16 v0, p62

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->af:LX/0yc;

    move-object/from16 v0, p63

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ag:LX/Fsr;

    move-object/from16 v0, p64

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ah:LX/G10;

    move-object/from16 v0, p65

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ai:LX/G49;

    move-object/from16 v0, p66

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aj:LX/G4p;

    move-object/from16 v0, p67

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ak:LX/G35;

    move-object/from16 v0, p68

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->by:LX/0Ot;

    move-object/from16 v0, p69

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bz:LX/0Or;

    move-object/from16 v0, p70

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bA:LX/0Ot;

    move-object/from16 v0, p71

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bB:LX/0Ot;

    move-object/from16 v0, p72

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bC:LX/0Ot;

    move-object/from16 v0, p73

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bD:LX/0ad;

    move-object/from16 v0, p74

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->al:LX/0Or;

    move-object/from16 v0, p75

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->am:Ljava/lang/Boolean;

    move-object/from16 v0, p76

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bG:Lcom/facebook/delights/floating/DelightsFireworks;

    move-object/from16 v0, p77

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bH:LX/1Ar;

    return-void
.end method

.method public static a(Lcom/facebook/timeline/TimelineFragment;LX/FsE;)V
    .locals 0

    .prologue
    .line 2293351
    iput-object p1, p0, Lcom/facebook/timeline/TimelineFragment;->aQ:LX/FsE;

    .line 2293352
    invoke-direct {p0}, Lcom/facebook/timeline/TimelineFragment;->ah()V

    .line 2293353
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/TimelineFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 80

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v79

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/timeline/TimelineFragment;

    const/16 v3, 0x3669

    move-object/from16 v0, v79

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x1032

    move-object/from16 v0, v79

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x35fa

    move-object/from16 v0, v79

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x24b

    move-object/from16 v0, v79

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x3895

    move-object/from16 v0, v79

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x388f

    move-object/from16 v0, v79

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x2e3

    move-object/from16 v0, v79

    invoke-static {v0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x36ba

    move-object/from16 v0, v79

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x36a2

    move-object/from16 v0, v79

    invoke-static {v0, v11}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static/range {v79 .. v79}, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;->a(LX/0QB;)Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;

    move-result-object v12

    check-cast v12, Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;

    const/16 v13, 0x3631

    move-object/from16 v0, v79

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {v79 .. v79}, LX/G4l;->a(LX/0QB;)LX/G4l;

    move-result-object v14

    check-cast v14, LX/G4l;

    const-class v15, LX/Fy5;

    move-object/from16 v0, v79

    invoke-interface {v0, v15}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/Fy5;

    const-class v16, LX/FwU;

    move-object/from16 v0, v79

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/FwU;

    invoke-static/range {v79 .. v79}, LX/1CF;->a(LX/0QB;)LX/1CF;

    move-result-object v17

    check-cast v17, LX/1CF;

    const-class v18, LX/Fqd;

    move-object/from16 v0, v79

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v18

    check-cast v18, LX/Fqd;

    const-class v19, LX/FrG;

    move-object/from16 v0, v79

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v19

    check-cast v19, LX/FrG;

    const-class v20, LX/G0y;

    move-object/from16 v0, v79

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v20

    check-cast v20, LX/G0y;

    const-class v21, LX/FvP;

    move-object/from16 v0, v79

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v21

    check-cast v21, LX/FvP;

    const-class v22, LX/G2e;

    move-object/from16 v0, v79

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v22

    check-cast v22, LX/G2e;

    const-class v23, LX/FyE;

    move-object/from16 v0, v79

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v23

    check-cast v23, LX/FyE;

    const-class v24, LX/FwB;

    move-object/from16 v0, v79

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v24

    check-cast v24, LX/FwB;

    const-class v25, LX/FwH;

    move-object/from16 v0, v79

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v25

    check-cast v25, LX/FwH;

    const-class v26, LX/G4f;

    move-object/from16 v0, v79

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v26

    check-cast v26, LX/G4f;

    const-class v27, LX/FrM;

    move-object/from16 v0, v79

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v27

    check-cast v27, LX/FrM;

    invoke-static/range {v79 .. v79}, LX/BPp;->a(LX/0QB;)LX/BPp;

    move-result-object v28

    check-cast v28, LX/BPp;

    invoke-static/range {v79 .. v79}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v29

    check-cast v29, LX/2do;

    invoke-static/range {v79 .. v79}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v30

    check-cast v30, LX/0b3;

    invoke-static/range {v79 .. v79}, LX/BPs;->a(LX/0QB;)LX/BPs;

    move-result-object v31

    check-cast v31, LX/BPs;

    invoke-static/range {v79 .. v79}, LX/BPC;->a(LX/0QB;)LX/BPC;

    move-result-object v32

    check-cast v32, LX/BPC;

    invoke-static/range {v79 .. v79}, LX/G4x;->a(LX/0QB;)LX/G4x;

    move-result-object v33

    check-cast v33, LX/G4x;

    invoke-static/range {v79 .. v79}, LX/G2a;->a(LX/0QB;)LX/G2a;

    move-result-object v34

    check-cast v34, LX/G2a;

    invoke-static/range {v79 .. v79}, LX/0dG;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v35

    check-cast v35, Ljava/lang/String;

    invoke-static/range {v79 .. v79}, LX/BQ9;->a(LX/0QB;)LX/BQ9;

    move-result-object v36

    check-cast v36, LX/BQ9;

    invoke-static/range {v79 .. v79}, LX/2SI;->a(LX/0QB;)LX/2SI;

    move-result-object v37

    check-cast v37, LX/2SI;

    const/16 v38, 0x3642

    move-object/from16 v0, v79

    move/from16 v1, v38

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v38

    const-class v39, LX/Ft1;

    move-object/from16 v0, v79

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v39

    check-cast v39, LX/Ft1;

    invoke-static/range {v79 .. v79}, LX/1dr;->a(LX/0QB;)LX/1dr;

    move-result-object v40

    check-cast v40, LX/1dr;

    const-class v41, LX/Fq8;

    move-object/from16 v0, v79

    move-object/from16 v1, v41

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v41

    check-cast v41, LX/Fq8;

    invoke-static/range {v79 .. v79}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v42

    check-cast v42, LX/0zG;

    invoke-static/range {v79 .. v79}, LX/G0o;->a(LX/0QB;)LX/G0o;

    move-result-object v43

    check-cast v43, LX/G0o;

    invoke-static/range {v79 .. v79}, LX/G0p;->a(LX/0QB;)LX/G0p;

    move-result-object v44

    check-cast v44, LX/G0p;

    const/16 v45, 0x2ca

    move-object/from16 v0, v79

    move/from16 v1, v45

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v45

    const/16 v46, 0xb99

    move-object/from16 v0, v79

    move/from16 v1, v46

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v46

    const/16 v47, 0x259

    move-object/from16 v0, v79

    move/from16 v1, v47

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v47

    const/16 v48, 0x1061

    move-object/from16 v0, v79

    move/from16 v1, v48

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v48

    const-class v49, LX/Fw1;

    move-object/from16 v0, v79

    move-object/from16 v1, v49

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v49

    check-cast v49, LX/Fw1;

    invoke-static/range {v79 .. v79}, LX/Fv8;->a(LX/0QB;)LX/Fv8;

    move-result-object v50

    check-cast v50, LX/Fv8;

    const-class v51, LX/FvA;

    move-object/from16 v0, v79

    move-object/from16 v1, v51

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v51

    check-cast v51, LX/FvA;

    invoke-static/range {v79 .. v79}, LX/1lu;->a(LX/0QB;)LX/1lu;

    move-result-object v52

    check-cast v52, LX/1lu;

    const-class v53, LX/FrW;

    move-object/from16 v0, v79

    move-object/from16 v1, v53

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v53

    check-cast v53, LX/FrW;

    const/16 v54, 0x1a75

    move-object/from16 v0, v79

    move/from16 v1, v54

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v54

    const/16 v55, 0xb0b

    move-object/from16 v0, v79

    move/from16 v1, v55

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v55

    invoke-static/range {v79 .. v79}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v56

    check-cast v56, Lcom/facebook/common/perftest/PerfTestConfig;

    const-class v57, LX/FuJ;

    move-object/from16 v0, v79

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v57

    check-cast v57, LX/FuJ;

    const-class v58, LX/FtB;

    move-object/from16 v0, v79

    move-object/from16 v1, v58

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v58

    check-cast v58, LX/FtB;

    invoke-static/range {v79 .. v79}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v59

    check-cast v59, LX/0Xl;

    const/16 v60, 0x36e4

    move-object/from16 v0, v79

    move/from16 v1, v60

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v60

    const/16 v61, 0x64

    move-object/from16 v0, v79

    move/from16 v1, v61

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v61

    invoke-static/range {v79 .. v79}, LX/0Ss;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v62

    check-cast v62, Landroid/os/Handler;

    const-class v63, LX/FwS;

    move-object/from16 v0, v79

    move-object/from16 v1, v63

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v63

    check-cast v63, LX/FwS;

    invoke-static/range {v79 .. v79}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v64

    check-cast v64, LX/0yc;

    invoke-static/range {v79 .. v79}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v65

    check-cast v65, LX/Fsr;

    invoke-static/range {v79 .. v79}, LX/G10;->a(LX/0QB;)LX/G10;

    move-result-object v66

    check-cast v66, LX/G10;

    const-class v67, LX/G49;

    move-object/from16 v0, v79

    move-object/from16 v1, v67

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v67

    check-cast v67, LX/G49;

    invoke-static/range {v79 .. v79}, LX/G4p;->a(LX/0QB;)LX/G4p;

    move-result-object v68

    check-cast v68, LX/G4p;

    const-class v69, LX/G35;

    move-object/from16 v0, v79

    move-object/from16 v1, v69

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v69

    check-cast v69, LX/G35;

    const/16 v70, 0x36a6

    move-object/from16 v0, v79

    move/from16 v1, v70

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v70

    const/16 v71, 0xf9a

    move-object/from16 v0, v79

    move/from16 v1, v71

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v71

    const/16 v72, 0x388d

    move-object/from16 v0, v79

    move/from16 v1, v72

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v72

    const/16 v73, 0x3be

    move-object/from16 v0, v79

    move/from16 v1, v73

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v73

    const/16 v74, 0xb2a

    move-object/from16 v0, v79

    move/from16 v1, v74

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v74

    invoke-static/range {v79 .. v79}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v75

    check-cast v75, LX/0ad;

    const/16 v76, 0x2367

    move-object/from16 v0, v79

    move/from16 v1, v76

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v76

    invoke-static/range {v79 .. v79}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v77

    check-cast v77, Ljava/lang/Boolean;

    invoke-static/range {v79 .. v79}, Lcom/facebook/delights/floating/DelightsFireworks;->a(LX/0QB;)Lcom/facebook/delights/floating/DelightsFireworks;

    move-result-object v78

    check-cast v78, Lcom/facebook/delights/floating/DelightsFireworks;

    invoke-static/range {v79 .. v79}, LX/1Ar;->a(LX/0QB;)LX/1Ar;

    move-result-object v79

    check-cast v79, LX/1Ar;

    invoke-static/range {v2 .. v79}, Lcom/facebook/timeline/TimelineFragment;->a(Lcom/facebook/timeline/TimelineFragment;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;LX/0Ot;LX/G4l;LX/Fy5;LX/FwU;LX/1CF;LX/Fqd;LX/FrG;LX/G0y;LX/FvP;LX/G2e;LX/FyE;LX/FwB;LX/FwH;LX/G4f;LX/FrM;LX/BPp;LX/2do;LX/0b3;LX/BPs;LX/BPC;LX/G4x;LX/G2a;Ljava/lang/String;LX/BQ9;LX/2SI;LX/0Or;LX/Ft1;LX/1dr;LX/Fq8;LX/0zG;LX/G0o;LX/G0p;LX/0Or;LX/0Ot;LX/0Ot;LX/0Or;LX/Fw1;LX/Fv8;LX/FvA;LX/1lu;LX/FrW;LX/0Ot;LX/0Or;Lcom/facebook/common/perftest/PerfTestConfig;LX/FuJ;LX/FtB;LX/0Xl;LX/0Ot;LX/0Ot;Landroid/os/Handler;LX/FwS;LX/0yc;LX/Fsr;LX/G10;LX/G49;LX/G4p;LX/G35;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/0Or;Ljava/lang/Boolean;Lcom/facebook/delights/floating/DelightsFireworks;LX/1Ar;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/timeline/TimelineFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2293444
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    if-eqz v0, :cond_0

    .line 2293445
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bm:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "native_timeline_profile"

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    .line 2293446
    iget-wide v4, v2, LX/5SB;->b:J

    move-wide v2, v4

    .line 2293447
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2293448
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bm:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {v0, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2293449
    return-void
.end method

.method public static a$redex0(Lcom/facebook/timeline/TimelineFragment;JLcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 7

    .prologue
    .line 2293439
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    .line 2293440
    iget-wide v5, v0, LX/5SB;->b:J

    move-wide v0, v5

    .line 2293441
    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    invoke-static {p3}, LX/9A3;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    .line 2293442
    iget-wide v5, v0, LX/5SB;->b:J

    move-wide v0, v5

    .line 2293443
    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->H:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    invoke-static {p3}, LX/9A3;->b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p3}, LX/9A3;->b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aa()Z
    .locals 2

    .prologue
    .line 2293437
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2293438
    const-string v1, "timeline_has_unseen_section"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private ab()Z
    .locals 1

    .prologue
    .line 2293436
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    invoke-virtual {v0}, LX/5SB;->d()Z

    move-result v0

    return v0
.end method

.method public static ad(Lcom/facebook/timeline/TimelineFragment;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2293434
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ap:LX/G4q;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ap:LX/G4q;

    invoke-virtual {v1}, LX/G4q;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->ap:LX/G4q;

    invoke-virtual {v2}, LX/G4q;->getPaddingBottom()I

    move-result v2

    invoke-virtual {v0, v3, v1, v3, v2}, LX/G4q;->setPadding(IIII)V

    .line 2293435
    return-void
.end method

.method private ae()V
    .locals 2

    .prologue
    .line 2293429
    invoke-virtual {p0}, Lcom/facebook/timeline/TimelineFragment;->k()LX/0g8;

    move-result-object v0

    .line 2293430
    if-nez v0, :cond_0

    .line 2293431
    :goto_0
    return-void

    .line 2293432
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bv:LX/1Jg;

    invoke-interface {v1, v0}, LX/1Jg;->a(LX/0g8;)V

    .line 2293433
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bv:LX/1Jg;

    invoke-interface {v1}, LX/1Jg;->a()LX/0fx;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    goto :goto_0
.end method

.method private af()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2293410
    const/4 v0, 0x0

    .line 2293411
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    if-eqz v1, :cond_0

    .line 2293412
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v0

    .line 2293413
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bD:LX/0ad;

    sget-short v2, LX/100;->N:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->z()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2293414
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08161c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->z()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2293415
    :goto_0
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v2, 0x1

    .line 2293416
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aE:LX/63T;

    if-eqz v0, :cond_4

    move v0, v2

    .line 2293417
    :goto_1
    move v0, v0

    .line 2293418
    if-eqz v0, :cond_1

    .line 2293419
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2293420
    if-eqz v0, :cond_2

    .line 2293421
    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2293422
    :cond_1
    :goto_2
    return-void

    .line 2293423
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aE:LX/63T;

    if-eqz v0, :cond_1

    .line 2293424
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aE:LX/63T;

    invoke-interface {v0, v1}, LX/63T;->setTitle(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    move-object v1, v0

    goto :goto_0

    .line 2293425
    :cond_4
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->O:LX/0zG;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->O:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2293426
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->O:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/63T;

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aE:LX/63T;

    move v0, v2

    .line 2293427
    goto :goto_1

    .line 2293428
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private ah()V
    .locals 6

    .prologue
    .line 2293380
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->K:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fsw;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aQ:LX/FsE;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 2293381
    if-nez v1, :cond_1

    .line 2293382
    :cond_0
    :goto_0
    return-void

    .line 2293383
    :cond_1
    const/4 p0, 0x0

    .line 2293384
    iget-object v3, v1, LX/FsE;->b:LX/1Zp;

    if-eqz v3, :cond_5

    iget-boolean v3, v1, LX/FsE;->f:Z

    if-nez v3, :cond_5

    const/4 v3, 0x1

    :goto_1
    move v3, v3

    .line 2293385
    if-nez v3, :cond_3

    .line 2293386
    iget-object v3, v0, LX/Fsw;->d:[Z

    aput-boolean p0, v3, p0

    .line 2293387
    :goto_2
    iget-object v3, v1, LX/FsE;->a:LX/1Zp;

    if-eqz v3, :cond_7

    iget-boolean v3, v1, LX/FsE;->e:Z

    if-nez v3, :cond_7

    const/4 v3, 0x1

    :goto_3
    move v3, v3

    .line 2293388
    if-nez v3, :cond_6

    .line 2293389
    :goto_4
    iget-object v3, v1, LX/FsE;->c:LX/1Zp;

    if-eqz v3, :cond_8

    iget-boolean v3, v1, LX/FsE;->g:Z

    if-nez v3, :cond_8

    const/4 v3, 0x1

    :goto_5
    move v3, v3

    .line 2293390
    if-eqz v3, :cond_2

    .line 2293391
    new-instance v3, LX/Fsu;

    invoke-direct {v3, v0, v1, v2}, LX/Fsu;-><init>(LX/Fsw;LX/FsE;Ljava/lang/ref/WeakReference;)V

    .line 2293392
    iget-object v4, v1, LX/FsE;->c:LX/1Zp;

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v5

    invoke-static {v4, v3, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2293393
    iget-object v4, v0, LX/Fsw;->a:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2293394
    :cond_2
    iget-object v3, v1, LX/FsE;->d:LX/1Zp;

    if-eqz v3, :cond_9

    iget-boolean v3, v1, LX/FsE;->h:Z

    if-nez v3, :cond_9

    const/4 v3, 0x1

    :goto_6
    move v3, v3

    .line 2293395
    if-eqz v3, :cond_0

    .line 2293396
    new-instance v3, LX/Fsv;

    invoke-direct {v3, v0, v1, v2}, LX/Fsv;-><init>(LX/Fsw;LX/FsE;Ljava/lang/ref/WeakReference;)V

    .line 2293397
    iget-object v4, v1, LX/FsE;->d:LX/1Zp;

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v5

    invoke-static {v4, v3, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2293398
    iget-object v4, v0, LX/Fsw;->a:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2293399
    goto :goto_0

    .line 2293400
    :cond_3
    iget-object v3, v1, LX/FsE;->b:LX/1Zp;

    invoke-virtual {v3}, LX/1Zp;->isDone()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, v1, LX/FsE;->b:LX/1Zp;

    invoke-virtual {v3}, LX/1Zp;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_4

    .line 2293401
    :try_start_0
    iget-object v3, v1, LX/FsE;->b:LX/1Zp;

    invoke-static {v3}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    .line 2293402
    iget-object v3, v0, LX/Fsw;->d:[Z

    const/4 v4, 0x0

    const/4 v5, 0x1

    aput-boolean v5, v3, v4
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2293403
    :cond_4
    :goto_7
    new-instance v4, LX/Fss;

    invoke-direct {v4, v0, v1, v2}, LX/Fss;-><init>(LX/Fsw;LX/FsE;Ljava/lang/ref/WeakReference;)V

    .line 2293404
    iget-object v5, v1, LX/FsE;->b:LX/1Zp;

    iget-object v3, v0, LX/Fsw;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-static {v5, v4, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2293405
    iget-object v3, v0, LX/Fsw;->a:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2293406
    :catch_0
    iget-object v3, v0, LX/Fsw;->d:[Z

    aput-boolean p0, v3, p0

    goto :goto_7

    :cond_5
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 2293407
    :cond_6
    new-instance v4, LX/Fst;

    invoke-direct {v4, v0, v1, v2}, LX/Fst;-><init>(LX/Fsw;LX/FsE;Ljava/lang/ref/WeakReference;)V

    .line 2293408
    iget-object v5, v1, LX/FsE;->a:LX/1Zp;

    iget-object v3, v0, LX/Fsw;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-static {v5, v4, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2293409
    iget-object v3, v0, LX/Fsw;->a:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_3

    :cond_8
    const/4 v3, 0x0

    goto/16 :goto_5

    :cond_9
    const/4 v3, 0x0

    goto :goto_6
.end method

.method private ai()V
    .locals 2

    .prologue
    .line 2293377
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aq:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 2293378
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aq:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2293379
    :cond_0
    return-void
.end method

.method public static ak(Lcom/facebook/timeline/TimelineFragment;)V
    .locals 13

    .prologue
    .line 2293354
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bx:LX/G34;

    if-nez v0, :cond_0

    .line 2293355
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ak:LX/G35;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/timeline/TimelineFragment;->ap:LX/G4q;

    const v5, 0x7f0d2f36

    invoke-static {v4, v5}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    .line 2293356
    new-instance v6, LX/G34;

    const/16 v7, 0xbd2

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v7, 0x1052

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    invoke-direct/range {v6 .. v12}, LX/G34;-><init>(LX/BP0;LX/BQ1;LX/0gc;Landroid/view/View;LX/0Or;LX/0Or;)V

    .line 2293357
    move-object v0, v6

    .line 2293358
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bx:LX/G34;

    .line 2293359
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bx:LX/G34;

    .line 2293360
    const/4 v1, 0x0

    .line 2293361
    iget-object v2, v0, LX/G34;->a:LX/BP0;

    invoke-virtual {v2}, LX/5SB;->i()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2293362
    sget-object v2, LX/77s;->h:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {v0, v2}, LX/G34;->a(LX/G34;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Landroid/content/Intent;

    move-result-object v2

    .line 2293363
    :goto_0
    move-object v2, v2

    .line 2293364
    if-eqz v2, :cond_1

    iget-object v3, v0, LX/G34;->g:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    if-eqz v3, :cond_1

    iget-object v3, v0, LX/G34;->g:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    invoke-virtual {v3, v2}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v0, LX/G34;->g:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2293365
    iget-object v1, v0, LX/G34;->g:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    invoke-virtual {v1}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->r()V

    .line 2293366
    :goto_1
    return-void

    .line 2293367
    :cond_1
    if-eqz v2, :cond_2

    .line 2293368
    iget-object v1, v0, LX/G34;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2hf;

    invoke-virtual {v1, v2}, LX/2hf;->a(Landroid/content/Intent;)Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    move-result-object v1

    .line 2293369
    :cond_2
    instance-of v2, v1, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    if-eqz v2, :cond_3

    .line 2293370
    check-cast v1, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    iput-object v1, v0, LX/G34;->g:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    .line 2293371
    iget-object v1, v0, LX/G34;->c:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d2f36

    iget-object v3, v0, LX/G34;->g:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    invoke-virtual {v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 2293372
    iget-object v1, v0, LX/G34;->d:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 2293373
    :cond_3
    invoke-virtual {v0}, LX/G34;->c()V

    goto :goto_1

    .line 2293374
    :cond_4
    iget-object v2, v0, LX/G34;->b:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v2, v3, :cond_5

    .line 2293375
    sget-object v2, LX/77s;->i:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {v0, v2}, LX/G34;->a(LX/G34;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0

    .line 2293376
    :cond_5
    sget-object v2, LX/77s;->j:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {v0, v2}, LX/G34;->a(LX/G34;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0
.end method

.method public static al(Lcom/facebook/timeline/TimelineFragment;)V
    .locals 3

    .prologue
    .line 2293760
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->X()Z

    move-result v1

    invoke-static {v0, v1}, LX/BQ3;->a(LX/5SB;Z)Z

    move-result v0

    .line 2293761
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->G:LX/G2a;

    .line 2293762
    iget-object v2, v1, LX/G2a;->b:LX/G2f;

    .line 2293763
    iput-boolean v0, v2, LX/G2f;->a:Z

    .line 2293764
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->G:LX/G2a;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    invoke-virtual {v1}, LX/5SB;->i()Z

    move-result v1

    .line 2293765
    iget-object v2, v0, LX/G2a;->b:LX/G2f;

    .line 2293766
    iput-boolean v1, v2, LX/G2f;->b:Z

    .line 2293767
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2293768
    return-void
.end method

.method public static an(Lcom/facebook/timeline/TimelineFragment;)V
    .locals 1

    .prologue
    .line 2293611
    iget-boolean v0, p0, Lcom/facebook/timeline/TimelineFragment;->au:Z

    if-eqz v0, :cond_0

    .line 2293612
    :goto_0
    return-void

    .line 2293613
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->lq_()V

    .line 2293614
    invoke-direct {p0}, Lcom/facebook/timeline/TimelineFragment;->ae()V

    .line 2293615
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/timeline/TimelineFragment;->au:Z

    goto :goto_0
.end method

.method private static ao(Lcom/facebook/timeline/TimelineFragment;)V
    .locals 8

    .prologue
    .line 2293769
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v0

    .line 2293770
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v0}, LX/BPy;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2293771
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->I:LX/BQ9;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    .line 2293772
    iget-wide v6, v1, LX/5SB;->b:J

    move-wide v2, v6

    .line 2293773
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    invoke-virtual {v1}, LX/5SB;->i()Z

    move-result v1

    iget-object v4, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v4}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v5

    invoke-static {v1, v4, v5}, LX/9lQ;->getRelationshipType(ZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/9lQ;

    move-result-object v1

    .line 2293774
    iget-boolean v4, v0, LX/BQ9;->j:Z

    if-eqz v4, :cond_1

    .line 2293775
    :cond_0
    :goto_0
    return-void

    .line 2293776
    :cond_1
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "view"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "timeline"

    .line 2293777
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2293778
    move-object v4, v4

    .line 2293779
    invoke-static {v0, v4, v2, v3, v1}, LX/BQ9;->a(LX/BQ9;Lcom/facebook/analytics/logger/HoneyClientEvent;JLX/9lQ;)V

    .line 2293780
    iget-object v5, v0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2293781
    const/4 v4, 0x1

    iput-boolean v4, v0, LX/BQ9;->j:Z

    goto :goto_0
.end method

.method private ap()V
    .locals 6

    .prologue
    .line 2293782
    iget-boolean v0, p0, Lcom/facebook/timeline/TimelineFragment;->aw:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->y:LX/G4f;

    if-eqz v0, :cond_0

    .line 2293783
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->y:LX/G4f;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->ln_()LX/Frd;

    move-result-object v2

    .line 2293784
    new-instance v5, LX/G4e;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    invoke-direct {v5, v1, v2, v3, v4}, LX/G4e;-><init>(LX/5SB;LX/Frd;Landroid/content/Context;LX/17W;)V

    .line 2293785
    move-object v0, v5

    .line 2293786
    invoke-virtual {p0, v0}, Lcom/facebook/timeline/BaseTimelineFragment;->a(LX/G4e;)V

    .line 2293787
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/timeline/TimelineFragment;->aw:Z

    .line 2293788
    :cond_0
    return-void
.end method

.method private static aq(Lcom/facebook/timeline/TimelineFragment;)V
    .locals 4

    .prologue
    .line 2293789
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aj:LX/G4p;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->as:LX/0zw;

    const v2, 0x7f080067

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/G4p;->a(LX/0zw;IZ)V

    .line 2293790
    return-void
.end method

.method private ar()V
    .locals 4

    .prologue
    .line 2293791
    new-instance v0, LX/FqA;

    invoke-direct {v0, p0}, LX/FqA;-><init>(Lcom/facebook/timeline/TimelineFragment;)V

    .line 2293792
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->p:LX/1CF;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->F:LX/G4x;

    .line 2293793
    iget-object v3, v2, LX/G4x;->e:LX/0qm;

    move-object v2, v3

    .line 2293794
    invoke-virtual {v1, v0, v2}, LX/1CF;->a(LX/1CH;LX/0qm;)V

    .line 2293795
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ac:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.intent.action.COVER_PHOTO_UPDATED"

    new-instance v2, LX/FqE;

    invoke-direct {v2, p0}, LX/FqE;-><init>(Lcom/facebook/timeline/TimelineFragment;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.intent.action.ACTION_OPTIMISTIC_COVER_PHOTO_UPDATED"

    new-instance v2, LX/FqD;

    invoke-direct {v2, p0}, LX/FqD;-><init>(Lcom/facebook/timeline/TimelineFragment;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.intent.action.ACTION_OPTIMISTIC_PROFILE_PIC_UPDATED"

    new-instance v2, LX/FqC;

    invoke-direct {v2, p0}, LX/FqC;-><init>(Lcom/facebook/timeline/TimelineFragment;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.intent.action.ACTION_REFRESH_TIMELINE"

    new-instance v2, LX/FqB;

    invoke-direct {v2, p0}, LX/FqB;-><init>(Lcom/facebook/timeline/TimelineFragment;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bs:LX/0Yb;

    .line 2293796
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bs:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2293797
    return-void
.end method

.method private as()V
    .locals 13

    .prologue
    .line 2293798
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aW:LX/1B1;

    .line 2293799
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aW:LX/1B1;

    new-instance v1, LX/FqF;

    invoke-direct {v1, p0}, LX/FqF;-><init>(Lcom/facebook/timeline/TimelineFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2293800
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aX:LX/1B1;

    .line 2293801
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->v:LX/FyE;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    iget-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->aB:LX/Fy4;

    .line 2293802
    new-instance v4, LX/FyD;

    invoke-static {v0}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v9

    check-cast v9, LX/Fsr;

    invoke-static {v0}, LX/G1I;->a(LX/0QB;)LX/G1I;

    move-result-object v10

    check-cast v10, LX/G1I;

    const/16 v5, 0x3622

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SG;

    move-object v5, v1

    move-object v6, p0

    move-object v7, v2

    move-object v8, v3

    invoke-direct/range {v4 .. v12}, LX/FyD;-><init>(LX/BP0;Lcom/facebook/timeline/TimelineFragment;LX/BQ1;LX/Fy4;LX/Fsr;LX/G1I;LX/0Or;LX/0SG;)V

    .line 2293803
    move-object v0, v4

    .line 2293804
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aX:LX/1B1;

    invoke-virtual {v0, v1}, LX/FyD;->a(LX/1B1;)V

    .line 2293805
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aW:LX/1B1;

    invoke-virtual {v0, v1}, LX/FyD;->a(LX/1B1;)V

    .line 2293806
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->B:LX/2do;

    .line 2293807
    new-instance v2, LX/FyC;

    invoke-direct {v2, v0}, LX/FyC;-><init>(LX/FyD;)V

    .line 2293808
    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2293809
    move-object v0, v2

    .line 2293810
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bJ:LX/2f1;

    .line 2293811
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->z:LX/FrM;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->F:LX/G4x;

    .line 2293812
    new-instance v4, LX/FrL;

    invoke-static {v0}, LX/G1I;->a(LX/0QB;)LX/G1I;

    move-result-object v7

    check-cast v7, LX/G1I;

    invoke-static {v0}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v8

    check-cast v8, LX/Fsr;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    move-object v5, v1

    move-object v6, v2

    invoke-direct/range {v4 .. v9}, LX/FrL;-><init>(LX/5SB;LX/G4x;LX/G1I;LX/Fsr;LX/0SG;)V

    .line 2293813
    move-object v0, v4

    .line 2293814
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->D:LX/BPs;

    .line 2293815
    new-instance v2, LX/FrK;

    iget-object v3, v0, LX/FrL;->a:LX/5SB;

    .line 2293816
    iget-object v4, v3, LX/5SB;->d:Landroid/os/ParcelUuid;

    move-object v3, v4

    .line 2293817
    invoke-direct {v2, v0, v3}, LX/FrK;-><init>(LX/FrL;Landroid/os/ParcelUuid;)V

    .line 2293818
    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2293819
    move-object v0, v2

    .line 2293820
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bK:LX/BPt;

    .line 2293821
    return-void
.end method

.method private av()Z
    .locals 1

    .prologue
    .line 2293822
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    invoke-virtual {v0}, LX/5SB;->i()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final A()LX/FvM;
    .locals 1

    .prologue
    .line 2293823
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aV:LX/FvM;

    return-object v0
.end method

.method public final B()LX/G2b;
    .locals 6

    .prologue
    .line 2293824
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aO:LX/G2b;

    if-nez v0, :cond_0

    .line 2293825
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->u:LX/G2e;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->I:LX/BQ9;

    iget-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    iget-object v4, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->ln_()LX/Frd;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/G2e;->a(Landroid/app/Activity;LX/BQ9;LX/5SB;LX/BQ1;LX/Frd;)LX/G2d;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aO:LX/G2b;

    .line 2293826
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aO:LX/G2b;

    return-object v0
.end method

.method public final E()V
    .locals 0

    .prologue
    .line 2293758
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2293759
    return-void
.end method

.method public final F()V
    .locals 1

    .prologue
    .line 2293827
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2293828
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    .line 2293829
    :cond_0
    return-void
.end method

.method public final G()V
    .locals 2

    .prologue
    .line 2293753
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2293754
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aq:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 2293755
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aq:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2293756
    :cond_0
    invoke-direct {p0}, Lcom/facebook/timeline/TimelineFragment;->af()V

    .line 2293757
    return-void
.end method

.method public final J()V
    .locals 4

    .prologue
    .line 2293729
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bd:LX/FrH;

    if-eqz v0, :cond_0

    .line 2293730
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bd:LX/FrH;

    invoke-virtual {v0}, LX/BPB;->h()V

    .line 2293731
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    if-eqz v0, :cond_1

    .line 2293732
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    const/4 v1, 0x2

    .line 2293733
    iput v1, v0, LX/BPy;->g:I

    .line 2293734
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2293735
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    if-eqz v0, :cond_2

    .line 2293736
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    invoke-virtual {v0}, LX/FrV;->f()V

    .line 2293737
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    .line 2293738
    iget-object v1, v0, LX/BPy;->e:LX/BPx;

    move-object v0, v1

    .line 2293739
    sget-object v1, LX/BPx;->UNINITIALIZED:LX/BPx;

    if-ne v0, v1, :cond_3

    .line 2293740
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->R:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2293741
    const v0, 0x7f08003f

    .line 2293742
    :goto_0
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aj:LX/G4p;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->as:LX/0zw;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, LX/G4p;->a(LX/0zw;IZ)V

    .line 2293743
    :cond_3
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bg:LX/BQB;

    .line 2293744
    iget-object v1, v0, LX/BQB;->c:LX/BQD;

    const-string v2, "TimelineLoadHeader"

    invoke-virtual {v1, v2}, LX/BQD;->d(Ljava/lang/String;)V

    .line 2293745
    iget-boolean v1, v0, LX/BQB;->E:Z

    if-nez v1, :cond_4

    .line 2293746
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/BQB;->E:Z

    .line 2293747
    iget-object v1, v0, LX/BQB;->f:LX/BPC;

    .line 2293748
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/BPC;->b:Z

    .line 2293749
    :cond_4
    invoke-static {p0}, Lcom/facebook/timeline/TimelineFragment;->an(Lcom/facebook/timeline/TimelineFragment;)V

    .line 2293750
    invoke-direct {p0}, Lcom/facebook/timeline/TimelineFragment;->ai()V

    .line 2293751
    return-void

    .line 2293752
    :cond_5
    const v0, 0x7f080062

    goto :goto_0
.end method

.method public final K()V
    .locals 7

    .prologue
    .line 2293704
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->F:LX/G4x;

    if-nez v0, :cond_1

    .line 2293705
    :cond_0
    :goto_0
    return-void

    .line 2293706
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    invoke-interface {v0}, LX/0g8;->q()I

    move-result v0

    .line 2293707
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    invoke-virtual {v1}, LX/G0n;->b()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 2293708
    invoke-virtual {p0, v0}, Lcom/facebook/timeline/TimelineFragment;->a(I)I

    move-result v1

    .line 2293709
    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->F:LX/G4x;

    iget-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    invoke-virtual {v3, v0}, LX/99d;->g(I)I

    move-result v0

    .line 2293710
    const/4 v3, 0x0

    .line 2293711
    iget-object v4, v2, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v3

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/G59;

    .line 2293712
    invoke-virtual {v3}, LX/G59;->b()I

    move-result v5

    add-int/2addr v5, v4

    .line 2293713
    if-lt v5, v1, :cond_2

    .line 2293714
    sub-int v4, v1, v4

    add-int/lit8 v5, v4, -0x1

    .line 2293715
    invoke-virtual {v3, v5}, LX/G59;->b(I)Ljava/lang/String;

    move-result-object v6

    .line 2293716
    new-instance v4, LX/G5B;

    .line 2293717
    iget-object v2, v3, LX/G59;->f:Ljava/lang/String;

    move-object v3, v2

    .line 2293718
    invoke-direct {v4, v3, v5, v6, v0}, LX/G5B;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    move-object v3, v4

    .line 2293719
    :goto_2
    move-object v0, v3

    .line 2293720
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bf:LX/G5B;

    .line 2293721
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bf:LX/G5B;

    if-eqz v0, :cond_0

    .line 2293722
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bf:LX/G5B;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    .line 2293723
    iput v1, v0, LX/G5B;->e:I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2293724
    goto :goto_0

    .line 2293725
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2293726
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bm:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "TimelineFragment"

    const-string v3, "Illegal index in onBeforeDataChanged"

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    move v4, v5

    .line 2293727
    goto :goto_1

    .line 2293728
    :cond_3
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public final L()V
    .locals 11

    .prologue
    .line 2293644
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    if-nez v0, :cond_0

    .line 2293645
    :goto_0
    return-void

    .line 2293646
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bf:LX/G5B;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->F:LX/G4x;

    if-eqz v0, :cond_2

    .line 2293647
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    invoke-virtual {v0}, LX/G0n;->c()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->F:LX/G4x;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->bf:LX/G5B;

    const/4 v5, 0x0

    .line 2293648
    if-nez v2, :cond_4

    .line 2293649
    :cond_1
    :goto_1
    move v1, v5

    .line 2293650
    add-int/2addr v0, v1

    .line 2293651
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    invoke-virtual {v1, v0}, LX/99d;->m_(I)I

    move-result v0

    .line 2293652
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->bf:LX/G5B;

    .line 2293653
    iget v3, v2, LX/G5B;->c:I

    move v2, v3

    .line 2293654
    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->bf:LX/G5B;

    .line 2293655
    iget v3, v2, LX/G5B;->e:I

    move v2, v3

    .line 2293656
    invoke-interface {v1, v0, v2}, LX/0g8;->d(II)V

    .line 2293657
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bf:LX/G5B;

    .line 2293658
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bt:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G4A;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    .line 2293659
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2293660
    const-string v4, "navigation_source"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2293661
    if-eqz v1, :cond_3

    if-eqz v2, :cond_3

    iget-boolean v5, v0, LX/G4A;->b:Z

    if-eqz v5, :cond_c

    .line 2293662
    :cond_3
    :goto_2
    goto :goto_0

    .line 2293663
    :cond_4
    iget-object v3, v1, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v5

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/G59;

    .line 2293664
    iget-object v7, v3, LX/G59;->f:Ljava/lang/String;

    move-object v7, v7

    .line 2293665
    iget-object v8, v2, LX/G5B;->a:Ljava/lang/String;

    move-object v8, v8

    .line 2293666
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2293667
    iget v5, v2, LX/G5B;->b:I

    move v5, v5

    .line 2293668
    invoke-virtual {v3, v5}, LX/G59;->b(I)Ljava/lang/String;

    move-result-object v5

    .line 2293669
    iget-object v6, v2, LX/G5B;->d:Ljava/lang/String;

    move-object v6, v6

    .line 2293670
    if-eqz v6, :cond_5

    .line 2293671
    iget-object v6, v2, LX/G5B;->d:Ljava/lang/String;

    move-object v6, v6

    .line 2293672
    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2293673
    iget v3, v2, LX/G5B;->b:I

    move v3, v3

    .line 2293674
    add-int/2addr v3, v4

    add-int/lit8 v5, v3, 0x1

    goto :goto_1

    .line 2293675
    :cond_5
    iget-object v5, v2, LX/G5B;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2293676
    const/4 v7, -0x1

    .line 2293677
    if-nez v5, :cond_9

    move v6, v7

    .line 2293678
    :cond_6
    :goto_4
    move v5, v6

    .line 2293679
    if-gez v5, :cond_8

    .line 2293680
    const-string v5, "PLACEHOLDER_ID"

    .line 2293681
    iget-object v6, v2, LX/G5B;->d:Ljava/lang/String;

    move-object v6, v6

    .line 2293682
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 2293683
    invoke-virtual {v3}, LX/G59;->b()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    .line 2293684
    :goto_5
    move v3, v5

    .line 2293685
    :goto_6
    add-int/2addr v3, v4

    add-int/lit8 v5, v3, 0x1

    goto/16 :goto_1

    .line 2293686
    :cond_7
    invoke-virtual {v3}, LX/G59;->b()I

    move-result v3

    add-int/2addr v3, v4

    move v4, v3

    .line 2293687
    goto :goto_3

    :cond_8
    move v3, v5

    goto :goto_6

    .line 2293688
    :cond_9
    const/4 v6, 0x0

    :goto_7
    invoke-virtual {v3}, LX/G59;->b()I

    move-result v8

    if-ge v6, v8, :cond_a

    .line 2293689
    invoke-virtual {v3, v6}, LX/G59;->b(I)Ljava/lang/String;

    move-result-object v8

    .line 2293690
    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 2293691
    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    :cond_a
    move v6, v7

    .line 2293692
    goto :goto_4

    .line 2293693
    :cond_b
    iget v5, v2, LX/G5B;->b:I

    move v5, v5

    .line 2293694
    invoke-virtual {v3}, LX/G59;->b()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    goto :goto_5

    .line 2293695
    :cond_c
    invoke-virtual {v1}, LX/5SB;->i()Z

    move-result v5

    invoke-virtual {v2}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v6

    invoke-virtual {v2}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v7

    invoke-static {v5, v6, v7}, LX/9lQ;->getRelationshipType(ZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/9lQ;

    move-result-object v5

    .line 2293696
    sget-object v6, LX/9lQ;->FRIEND:LX/9lQ;

    invoke-virtual {v5, v6}, LX/9lQ;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_d

    sget-object v6, LX/9lQ;->SELF:LX/9lQ;

    invoke-virtual {v5, v6}, LX/9lQ;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 2293697
    const-string v5, "pps"

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    const-string v5, "search"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 2293698
    iget-object v5, v0, LX/G4A;->a:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/8bf;

    .line 2293699
    iget-wide v9, v1, LX/5SB;->b:J

    move-wide v7, v9

    .line 2293700
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 2293701
    iget-object v7, v5, LX/8bf;->e:Ljava/util/Set;

    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v7

    const/16 v8, 0x14

    if-ge v7, v8, :cond_d

    .line 2293702
    iget-object v7, v5, LX/8bf;->e:Ljava/util/Set;

    invoke-interface {v7, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2293703
    :cond_d
    const/4 v5, 0x1

    iput-boolean v5, v0, LX/G4A;->b:Z

    goto/16 :goto_2
.end method

.method public final M()V
    .locals 3

    .prologue
    .line 2293638
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    if-nez v0, :cond_0

    .line 2293639
    :goto_0
    return-void

    .line 2293640
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    invoke-interface {v0}, LX/0g8;->q()I

    move-result v0

    .line 2293641
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    invoke-interface {v1}, LX/0g8;->r()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    invoke-interface {v2}, LX/0g8;->s()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2293642
    sub-int/2addr v1, v0

    add-int/lit8 v1, v1, 0x1

    .line 2293643
    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    invoke-interface {v2}, LX/0g8;->s()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/timeline/BaseTimelineFragment;->a(III)V

    goto :goto_0
.end method

.method public final O()V
    .locals 2

    .prologue
    .line 2293635
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bg:LX/BQB;

    .line 2293636
    iget-object v1, v0, LX/BQB;->c:LX/BQD;

    const-string p0, "TimelineFetchProfilePicUri"

    invoke-virtual {v1, p0}, LX/BQD;->d(Ljava/lang/String;)V

    .line 2293637
    return-void
.end method

.method public final R()V
    .locals 4

    .prologue
    .line 2293626
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    if-nez v0, :cond_0

    .line 2293627
    :goto_0
    return-void

    .line 2293628
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/timeline/TimelineFragment;->ll_()V

    .line 2293629
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    .line 2293630
    sget-object v1, LX/BPx;->UNINITIALIZED:LX/BPx;

    iput-object v1, v0, LX/BPy;->e:LX/BPx;

    .line 2293631
    const/4 v1, 0x1

    .line 2293632
    iput v1, v0, LX/BPy;->g:I

    .line 2293633
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->Y:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1mR;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "CoverImageRequest"

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2293634
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    goto :goto_0
.end method

.method public final a(I)I
    .locals 2

    .prologue
    .line 2293623
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    invoke-virtual {v0}, LX/G0n;->b()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2293624
    const/4 v0, -0x1

    .line 2293625
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    invoke-virtual {v0, p1}, LX/99d;->h_(I)I

    move-result v0

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    invoke-virtual {v1}, LX/G0n;->c()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2293622
    sget-object v0, Lcom/facebook/timeline/TimelineFragment;->an:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0g8;I)V
    .locals 1

    .prologue
    .line 2293619
    invoke-super {p0, p1, p2}, Lcom/facebook/timeline/BaseTimelineFragment;->a(LX/0g8;I)V

    .line 2293620
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bj:LX/Ft0;

    invoke-virtual {v0, p1, p2}, LX/Ft0;->a(LX/0g8;I)V

    .line 2293621
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 1

    .prologue
    .line 2293616
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/timeline/BaseTimelineFragment;->a(LX/0g8;III)V

    .line 2293617
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bj:LX/Ft0;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/Ft0;->a(LX/0g8;III)V

    .line 2293618
    return-void
.end method

.method public final a(LX/0ta;Lcom/facebook/timeline/protocol/ResultSource;J)V
    .locals 9

    .prologue
    .line 2293450
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    if-nez v0, :cond_1

    .line 2293451
    :cond_0
    :goto_0
    return-void

    .line 2293452
    :cond_1
    invoke-direct {p0}, Lcom/facebook/timeline/TimelineFragment;->ap()V

    .line 2293453
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aA:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fry;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Fry;->a(LX/BP0;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2293454
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    invoke-virtual {v0}, LX/5SB;->i()Z

    move-result v0

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/9lQ;->getRelationshipType(ZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/9lQ;

    move-result-object v0

    .line 2293455
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bg:LX/BQB;

    const v8, 0x1a0014

    const v7, 0x1a0013

    const v4, 0x1a0010

    const/4 v6, 0x1

    const/4 v5, 0x2

    .line 2293456
    iget-object v2, v1, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x1a0012

    invoke-interface {v2, v3, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2293457
    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-virtual {v2, p1}, LX/0ta;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2293458
    iget-object v2, v1, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v7, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2293459
    iget-object v2, v1, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 2293460
    iget-object v2, v1, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2293461
    iget-object v2, v1, LX/BQB;->c:LX/BQD;

    const-string v3, "TimelineFirstUnitsNetworkFetch"

    invoke-virtual {v2, v3}, LX/BQD;->b(Ljava/lang/String;)V

    .line 2293462
    :goto_1
    new-instance v2, LX/0P2;

    invoke-direct {v2}, LX/0P2;-><init>()V

    const-string v3, "source_initial_units"

    invoke-virtual {p2}, Lcom/facebook/timeline/protocol/ResultSource;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v3, "relationship_type"

    invoke-virtual {v0}, LX/9lQ;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    .line 2293463
    iget-object v3, v1, LX/BQB;->c:LX/BQD;

    const-string v4, "TimelineInitialFetchUnits"

    invoke-virtual {v3, v4, v2}, LX/BQD;->b(Ljava/lang/String;LX/0P1;)V

    .line 2293464
    iget-object v2, v1, LX/BQB;->i:Lcom/facebook/timeline/protocol/ResultSource;

    sget-object v3, Lcom/facebook/timeline/protocol/ResultSource;->UNDEFINED:Lcom/facebook/timeline/protocol/ResultSource;

    if-ne v2, v3, :cond_2

    .line 2293465
    iput-object p2, v1, LX/BQB;->i:Lcom/facebook/timeline/protocol/ResultSource;

    .line 2293466
    :cond_2
    iget-boolean v2, v1, LX/BQB;->A:Z

    if-eqz v2, :cond_3

    .line 2293467
    iget-object v2, v1, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x1a0002

    invoke-interface {v2, v3, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2293468
    iget-object v2, v1, LX/BQB;->c:LX/BQD;

    const-string v3, "TimelineFetchInitialUnitsWaitTime"

    invoke-virtual {v2, v3}, LX/BQD;->b(Ljava/lang/String;)V

    .line 2293469
    iput-boolean v6, v1, LX/BQB;->z:Z

    .line 2293470
    :cond_3
    iput-boolean v6, v1, LX/BQB;->s:Z

    .line 2293471
    invoke-static {v1}, LX/BQB;->w(LX/BQB;)V

    .line 2293472
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->I:LX/BQ9;

    invoke-virtual {v0, p1, p3, p4}, LX/BQ9;->a(LX/0ta;J)V

    goto/16 :goto_0

    .line 2293473
    :cond_4
    iget-object v2, v1, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v8, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2293474
    iget-object v2, v1, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 2293475
    iget-object v2, v1, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 2293476
    iget-object v2, v1, LX/BQB;->c:LX/BQD;

    const-string v3, "TimelineFirstUnitsNetworkFetch"

    invoke-virtual {v2, v3}, LX/BQD;->c(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(LX/5Mj;)V
    .locals 2

    .prologue
    .line 2293609
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bp:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Mk;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    invoke-virtual {v0, v1, p1}, LX/5Mk;->a(Ljava/lang/Object;LX/5Mj;)V

    .line 2293610
    return-void
.end method

.method public final a(LX/Fso;)V
    .locals 4

    .prologue
    .line 2293015
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bg:LX/BQB;

    iget-boolean v1, p1, LX/Fso;->f:Z

    iget-object v2, p1, LX/Fso;->a:Ljava/lang/String;

    .line 2293016
    if-eqz v1, :cond_0

    .line 2293017
    iget-object v3, v0, LX/BQB;->c:LX/BQD;

    const-string p0, "TimelineInitialFetchUnits"

    invoke-virtual {v3, p0}, LX/BQD;->a(Ljava/lang/String;)V

    .line 2293018
    iget-object v3, v0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const p0, 0x1a0010

    invoke-interface {v3, p0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2293019
    iget-object v3, v0, LX/BQB;->c:LX/BQD;

    const-string p0, "TimelineFirstUnitsNetworkFetch"

    invoke-virtual {v3, p0}, LX/BQD;->a(Ljava/lang/String;)V

    .line 2293020
    iget-object v3, v0, LX/BQB;->g:LX/BQ7;

    const-string p0, "TimelineFirstUnitsNetworkFetch"

    invoke-virtual {v3, p0}, LX/BQ7;->a(Ljava/lang/String;)V

    .line 2293021
    :goto_0
    return-void

    .line 2293022
    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/BQB;->u:Z

    .line 2293023
    iget-object v3, v0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const p0, 0x1a0020

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result p1

    invoke-interface {v3, p0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    goto :goto_0
.end method

.method public final a(LX/Fso;LX/0ta;J)V
    .locals 9

    .prologue
    .line 2292997
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    if-nez v0, :cond_1

    .line 2292998
    :cond_0
    :goto_0
    return-void

    .line 2292999
    :cond_1
    invoke-direct {p0}, Lcom/facebook/timeline/TimelineFragment;->ap()V

    .line 2293000
    iput-object p2, p0, Lcom/facebook/timeline/TimelineFragment;->aH:LX/0ta;

    .line 2293001
    invoke-virtual {p0}, Lcom/facebook/timeline/TimelineFragment;->G()V

    .line 2293002
    iget-boolean v0, p1, LX/Fso;->g:Z

    if-eqz v0, :cond_2

    .line 2293003
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->I:LX/BQ9;

    invoke-virtual {v0, p2, p3, p4}, LX/BQ9;->a(LX/0ta;J)V

    .line 2293004
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bg:LX/BQB;

    iget-object v1, p1, LX/Fso;->a:Ljava/lang/String;

    const v4, 0x1a0015

    const/4 v6, 0x2

    .line 2293005
    iget-boolean v3, v0, LX/BQB;->u:Z

    if-eqz v3, :cond_4

    .line 2293006
    const/4 v3, 0x0

    iput-boolean v3, v0, LX/BQB;->u:Z

    .line 2293007
    iget-object v3, v0, LX/BQB;->e:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v7

    .line 2293008
    iget-object v3, v0, LX/BQB;->C:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2293009
    iget-object v3, v0, LX/BQB;->C:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2293010
    iget-object v3, v0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v5

    invoke-interface {v3, v4, v5, v7, v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerStart(IIJ)V

    .line 2293011
    :cond_3
    iget-object v3, v0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v5

    invoke-interface/range {v3 .. v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerEnd(IISJ)V

    .line 2293012
    iget-object v3, v0, LX/BQB;->C:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2293013
    iget-object v3, v0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x1a0020

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v5

    invoke-interface {v3, v4, v5, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2293014
    :cond_4
    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 17
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2292892
    const-string v2, "TimelineFragment.onFragmentCreate.getPerformanceLogger"

    const v3, 0x7b6a1b29

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2292893
    :try_start_0
    new-instance v2, LX/BQC;

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/BQC;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, LX/BQC;->a()LX/BQB;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->bg:LX/BQB;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2292894
    const v2, 0x52545a89

    invoke-static {v2}, LX/02m;->a(I)V

    .line 2292895
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v16

    .line 2292896
    const-string v2, "TimelineFragment.onFragmentCreate.startInitialLogging"

    const v3, -0x1af24c0d

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2292897
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2292898
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/timeline/TimelineFragment;->bg:LX/BQB;

    const-string v2, "navigation_source"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget v2, Lcom/facebook/timeline/TimelineFragment;->bh:I

    add-int/lit8 v5, v2, 0x1

    sput v5, Lcom/facebook/timeline/TimelineFragment;->bh:I

    if-nez v2, :cond_8

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v3, v4, v2}, LX/BQB;->a(Ljava/lang/String;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2292899
    :cond_0
    const v2, -0x1e7efe51

    invoke-static {v2}, LX/02m;->a(I)V

    .line 2292900
    invoke-super/range {p0 .. p1}, Lcom/facebook/timeline/BaseTimelineFragment;->a(Landroid/os/Bundle;)V

    .line 2292901
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->bg:LX/BQB;

    invoke-virtual {v2}, LX/BQB;->b()V

    .line 2292902
    const-string v2, "TimelineFragment.onFragmentCreate.injectMe"

    const v3, -0x63c476

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2292903
    :try_start_2
    const-class v2, Lcom/facebook/timeline/TimelineFragment;

    move-object/from16 v0, p0

    invoke-static {v2, v0}, Lcom/facebook/timeline/TimelineFragment;->a(Ljava/lang/Class;LX/02k;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2292904
    const v2, 0x61715b51

    invoke-static {v2}, LX/02m;->a(I)V

    .line 2292905
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->ag:LX/Fsr;

    new-instance v3, Ljava/lang/ref/WeakReference;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, LX/Fsr;->a(Ljava/lang/ref/WeakReference;)V

    .line 2292906
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->bg:LX/BQB;

    invoke-virtual {v2}, LX/BQB;->c()V

    .line 2292907
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->ah:LX/G10;

    invoke-virtual {v2}, LX/G10;->a()LX/1Jg;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->bv:LX/1Jg;

    .line 2292908
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->m:LX/G4l;

    const-string v3, "search_typeahead"

    const-string v4, "navigation_source"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v2, v3}, LX/G4l;->a(Z)V

    .line 2292909
    const-string v2, "parent_control_title_bar"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/facebook/timeline/TimelineFragment;->bF:Z

    .line 2292910
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->af:LX/0yc;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, LX/0yc;->a(LX/0yL;)V

    .line 2292911
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->H:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_0

    move-result-wide v2

    .line 2292912
    :goto_1
    const-string v4, "com.facebook.katana.profile.id"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2292913
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-gez v6, :cond_1

    move-wide v4, v2

    .line 2292914
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "session_id"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 2292915
    new-instance v7, LX/FqH;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v4, v5}, LX/FqH;-><init>(Lcom/facebook/timeline/TimelineFragment;J)V

    invoke-static {v7}, LX/0Wf;->memoize(LX/0QR;)LX/0QR;

    move-result-object v7

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/facebook/timeline/TimelineFragment;->bn:LX/0QR;

    .line 2292916
    if-eqz p1, :cond_9

    .line 2292917
    const-string v7, "fragment_uuid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Landroid/os/ParcelUuid;

    .line 2292918
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2292919
    :goto_2
    const-string v8, "timeline_context_item_type"

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v9

    .line 2292920
    const-string v8, "timeline_friend_request_ref"

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v8

    invoke-static {v8}, LX/5P2;->from(Ljava/io/Serializable;)LX/5P2;

    move-result-object v8

    invoke-direct/range {p0 .. p0}, Lcom/facebook/timeline/TimelineFragment;->aa()Z

    move-result v10

    invoke-static/range {v2 .. v10}, LX/BP0;->a(JJLjava/lang/String;Landroid/os/ParcelUuid;LX/5P2;LX/0am;Z)LX/BP0;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    .line 2292921
    new-instance v2, LX/FuR;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    invoke-direct {v2, v3}, LX/FuR;-><init>(LX/5SB;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->bq:LX/FuR;

    .line 2292922
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->M:LX/1dr;

    invoke-direct/range {p0 .. p0}, Lcom/facebook/timeline/TimelineFragment;->ab()Z

    move-result v3

    invoke-virtual {v2, v3}, LX/1dr;->a(Z)V

    .line 2292923
    const-string v2, "TimelineFragment.onFragmentCreate.createTimelineConfig"

    const v3, 0x1676892d

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2292924
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->l:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BQG;

    .line 2292925
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->be:LX/BQG;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 2292926
    const v2, 0x6bc50ae6

    invoke-static {v2}, LX/02m;->a(I)V

    .line 2292927
    const-string v2, "TimelineFragment.onFragmentCreate.createDataObjects"

    const v3, -0x5a17ec09

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2292928
    :try_start_5
    new-instance v2, LX/BQ1;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    invoke-direct {v2, v3}, LX/BQ1;-><init>(LX/5SB;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    .line 2292929
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    invoke-virtual {v2}, LX/5SB;->i()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2292930
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->by:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BQT;

    invoke-virtual {v2}, LX/BQT;->a()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/BQ1;->a(Landroid/net/Uri;)V

    .line 2292931
    :cond_2
    new-instance v2, LX/G4m;

    invoke-direct {v2}, LX/G4m;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->bb:LX/G4m;

    .line 2292932
    new-instance v2, LX/G1N;

    invoke-direct {v2}, LX/G1N;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->bc:LX/G1N;

    .line 2292933
    invoke-static {}, LX/FrI;->a()LX/FrH;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->bd:LX/FrH;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 2292934
    const v2, 0x4abaf72b    # 6126485.5f

    invoke-static {v2}, LX/02m;->a(I)V

    .line 2292935
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->V:LX/FvA;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v2, v6, v3, v7}, LX/FvA;->a(Ljava/lang/String;Ljava/lang/Long;LX/BQ1;)LX/Fv9;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->bo:LX/Fv9;

    .line 2292936
    const-string v2, "TimelineFragment.onFragmentCreate.getDataFetcher"

    const v3, -0x700cb4f9

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2292937
    :try_start_6
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/timeline/TimelineFragment;->X:LX/FrW;

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    sget-object v10, LX/G11;->USER:LX/G11;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/timeline/TimelineFragment;->F:LX/G4x;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/timeline/TimelineFragment;->bb:LX/G4m;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/timeline/TimelineFragment;->bc:LX/G1N;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/timeline/TimelineFragment;->bg:LX/BQB;

    sget-object v15, Lcom/facebook/timeline/TimelineFragment;->an:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v8, p0

    invoke-virtual/range {v6 .. v15}, LX/FrW;->a(Landroid/content/Context;LX/FqO;LX/BP0;LX/G11;LX/G4x;LX/G4m;LX/G1N;LX/BQB;Lcom/facebook/common/callercontext/CallerContext;)LX/FrV;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    .line 2292938
    const v2, 0x4843f937

    invoke-static {v2}, LX/02m;->a(I)V

    .line 2292939
    const-string v2, "graphql_profile"

    move-object/from16 v0, v16

    invoke-static {v0, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/36O;

    .line 2292940
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v2}, Lcom/facebook/timeline/TimelineFragment;->a(JLX/36O;)V

    .line 2292941
    const-string v3, "TimelineFragment.onFragmentCreate.setPreliminaryProfile"

    const v6, 0x45746358

    invoke-static {v3, v6}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2292942
    :try_start_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/timeline/TimelineFragment;->U:LX/Fv8;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v3, v2, v6, v7}, LX/Fv8;->a(LX/36O;LX/5SB;LX/BQ1;)V

    .line 2292943
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->T()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v2

    invoke-static {v2}, LX/BQ3;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2292944
    sget-object v2, LX/BQ5;->PRELIMINARY_DATA:LX/BQ5;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->bi:LX/BQ5;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_6

    .line 2292945
    :cond_3
    const v2, -0x4cec52d3

    invoke-static {v2}, LX/02m;->a(I)V

    .line 2292946
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->am:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_4

    .line 2292947
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->k:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/G1I;

    .line 2292948
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/timeline/TimelineFragment;->j:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, LX/0SG;

    .line 2292949
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct/range {p0 .. p0}, Lcom/facebook/timeline/TimelineFragment;->av()Z

    move-result v5

    invoke-static {v4, v5}, LX/G1B;->a(Landroid/content/Context;Z)Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$TimelineProtilesQueryModel;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    invoke-virtual/range {v2 .. v7}, LX/G1I;->a(Ljava/lang/String;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$TimelineProtilesQueryModel;ZJ)V

    .line 2292950
    invoke-virtual {v2}, LX/G1I;->a()V

    .line 2292951
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->F:LX/G4x;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/G4x;->b(Z)V

    .line 2292952
    :cond_4
    new-instance v2, LX/FqI;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/FqI;-><init>(Lcom/facebook/timeline/TimelineFragment;)V

    invoke-static {v2}, LX/0Wf;->memoize(LX/0QR;)LX/0QR;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->aC:LX/0QR;

    .line 2292953
    new-instance v2, LX/FqJ;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/FqJ;-><init>(Lcom/facebook/timeline/TimelineFragment;)V

    invoke-static {v2}, LX/0Wf;->memoize(LX/0QR;)LX/0QR;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->bk:LX/0QR;

    .line 2292954
    const-string v2, "TimelineFragment.onFragmentCreate.getTimelineFriendingClient"

    const v3, 0x2ca13caf

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2292955
    :try_start_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->n:LX/Fy5;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    move-object/from16 v0, p0

    invoke-virtual {v2, v3, v4, v0}, LX/Fy5;->a(LX/5SB;LX/BQ1;Lcom/facebook/timeline/TimelineFragment;)LX/Fy4;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->aB:LX/Fy4;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_7

    .line 2292956
    const v2, 0x6d404464

    invoke-static {v2}, LX/02m;->a(I)V

    .line 2292957
    const-string v2, "TimelineFragment.onFragmentCreate.setupSubControllers"

    const v3, 0x53fdd9f4

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2292958
    :try_start_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->t:LX/FvP;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/timeline/TimelineFragment;->bo:LX/Fv9;

    invoke-virtual {v2, v3}, LX/FvP;->a(LX/Fv9;)LX/FvO;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->aV:LX/FvM;

    .line 2292959
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->aV:LX/FvM;

    move-object/from16 v0, p0

    invoke-interface {v2, v0}, LX/FvM;->a(Lcom/facebook/timeline/TimelineFragment;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_8

    .line 2292960
    const v2, 0x10ac8f6b

    invoke-static {v2}, LX/02m;->a(I)V

    .line 2292961
    const-string v2, "TimelineFragment.onFragmentCreate.setupEventHandlers"

    const v3, 0x31f1b2a0

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2292962
    :try_start_a
    invoke-direct/range {p0 .. p0}, Lcom/facebook/timeline/TimelineFragment;->as()V

    .line 2292963
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->aX:LX/1B1;

    if-eqz v2, :cond_5

    .line 2292964
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->aX:LX/1B1;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/timeline/TimelineFragment;->B:LX/2do;

    invoke-virtual {v2, v3}, LX/1B1;->a(LX/0b4;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_9

    .line 2292965
    :cond_5
    const v2, -0x39d77c11

    invoke-static {v2}, LX/02m;->a(I)V

    .line 2292966
    invoke-direct/range {p0 .. p0}, Lcom/facebook/timeline/TimelineFragment;->ar()V

    .line 2292967
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->aC:LX/0QR;

    invoke-interface {v2}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FwT;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, LX/FwT;->a(Landroid/os/Bundle;)V

    .line 2292968
    sget-object v2, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 2292969
    const-string v2, "extra_profile_pic_expiration"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 2292970
    const-string v2, "profile_photo_method_extra"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 2292971
    const-string v2, "extra_action_on_fragment_create"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    .line 2292972
    instance-of v3, v2, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    if-eqz v3, :cond_6

    .line 2292973
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/timeline/TimelineFragment;->ae:LX/FwS;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/timeline/TimelineFragment;->aC:LX/0QR;

    invoke-virtual {v3, v4}, LX/FwS;->a(LX/0QR;)LX/FwR;

    move-result-object v3

    .line 2292974
    check-cast v2, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    .line 2292975
    invoke-virtual {v3, v2}, LX/FwR;->a(Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;)V

    .line 2292976
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/facebook/timeline/TimelineFragment;->V()V

    .line 2292977
    invoke-direct/range {p0 .. p0}, Lcom/facebook/timeline/TimelineFragment;->Y()V

    .line 2292978
    invoke-static/range {p0 .. p0}, Lcom/facebook/timeline/TimelineFragment;->U(Lcom/facebook/timeline/TimelineFragment;)V

    .line 2292979
    invoke-direct/range {p0 .. p0}, Lcom/facebook/timeline/TimelineFragment;->X()V

    .line 2292980
    if-nez p1, :cond_7

    .line 2292981
    invoke-direct/range {p0 .. p0}, Lcom/facebook/timeline/TimelineFragment;->S()V

    .line 2292982
    :cond_7
    return-void

    .line 2292983
    :catchall_0
    move-exception v2

    const v3, 0x46c2603d

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2

    .line 2292984
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2292985
    :catchall_1
    move-exception v2

    const v3, -0x7b633992

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2

    .line 2292986
    :catchall_2
    move-exception v2

    const v3, -0x6e1ab60c

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2

    .line 2292987
    :catch_0
    const-wide/16 v2, -0x1

    .line 2292988
    const-string v4, "timeline_invalid_meuser"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "logged in user: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/timeline/TimelineFragment;->H:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Lcom/facebook/timeline/TimelineFragment;->a$redex0(Lcom/facebook/timeline/TimelineFragment;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2292989
    :cond_9
    new-instance v7, Landroid/os/ParcelUuid;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    goto/16 :goto_2

    .line 2292990
    :catchall_3
    move-exception v2

    const v3, -0x7e8e235c

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2

    .line 2292991
    :catchall_4
    move-exception v2

    const v3, -0x2572d2bf

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2

    .line 2292992
    :catchall_5
    move-exception v2

    const v3, -0x396b965d

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2

    .line 2292993
    :catchall_6
    move-exception v2

    const v3, -0x7b03831c

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2

    .line 2292994
    :catchall_7
    move-exception v2

    const v3, -0x3704a27a

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2

    .line 2292995
    :catchall_8
    move-exception v2

    const v3, -0x8d8f9b1

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2

    .line 2292996
    :catchall_9
    move-exception v2

    const v3, -0x25d64815

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2
.end method

.method public final a(Ljava/lang/Object;LX/0ta;Lcom/facebook/timeline/protocol/ResultSource;)V
    .locals 10

    .prologue
    .line 2292800
    const/4 v0, 0x0

    .line 2292801
    instance-of v1, p1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;

    if-eqz v1, :cond_2

    move-object v0, p1

    .line 2292802
    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    move-result-object v0

    .line 2292803
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 2292804
    invoke-static {p0}, Lcom/facebook/timeline/TimelineFragment;->aq(Lcom/facebook/timeline/TimelineFragment;)V

    .line 2292805
    :cond_1
    :goto_1
    return-void

    .line 2292806
    :cond_2
    instance-of v1, p1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 2292807
    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    goto :goto_0

    .line 2292808
    :cond_3
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    if-eqz v1, :cond_1

    .line 2292809
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    .line 2292810
    iget-object v2, v1, LX/BQ1;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    if-eqz v2, :cond_4

    iget-object v2, v1, LX/BQ1;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->B()Z

    move-result v2

    if-nez v2, :cond_e

    :cond_4
    const/4 v2, 0x1

    :goto_2
    move v1, v2

    .line 2292811
    if-nez v1, :cond_5

    .line 2292812
    invoke-static {p0}, Lcom/facebook/timeline/TimelineFragment;->aq(Lcom/facebook/timeline/TimelineFragment;)V

    goto :goto_1

    .line 2292813
    :cond_5
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->as:LX/0zw;

    invoke-static {v1}, LX/G4p;->a(LX/0zw;)V

    .line 2292814
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->U:LX/Fv8;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->u()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/Fv8;->a(LX/BQ1;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;)V

    .line 2292815
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FvX;

    .line 2292816
    iget-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    iget-object v4, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    iget-object v5, p0, Lcom/facebook/timeline/TimelineFragment;->bd:LX/FrH;

    move-object v1, p1

    move-object v2, p2

    .line 2292817
    const/4 v6, 0x0

    .line 2292818
    instance-of v7, v1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;

    if-eqz v7, :cond_f

    move-object v6, v1

    .line 2292819
    check-cast v6, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;

    invoke-virtual {v6}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    move-result-object v6

    .line 2292820
    :cond_6
    :goto_3
    if-eqz v4, :cond_7

    if-eqz v3, :cond_7

    if-nez v6, :cond_10

    .line 2292821
    :cond_7
    :goto_4
    iput-object p2, p0, Lcom/facebook/timeline/TimelineFragment;->aG:LX/0ta;

    .line 2292822
    invoke-virtual {p0}, Lcom/facebook/timeline/TimelineFragment;->G()V

    .line 2292823
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    invoke-virtual {v0}, LX/5SB;->i()Z

    move-result v0

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/9lQ;->getRelationshipType(ZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/9lQ;

    move-result-object v1

    .line 2292824
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bn:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fw0;

    .line 2292825
    iput-object v1, v0, LX/Fw0;->l:LX/9lQ;

    .line 2292826
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bo:LX/Fv9;

    .line 2292827
    iput-object v1, v0, LX/Fv9;->k:LX/9lQ;

    .line 2292828
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bj:LX/Ft0;

    .line 2292829
    iput-object v1, v0, LX/Ft0;->p:LX/9lQ;

    .line 2292830
    iget-object v2, v0, LX/Ft0;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1LV;

    .line 2292831
    invoke-static {v0, v2}, LX/Ft0;->a(LX/Ft0;LX/1LV;)V

    .line 2292832
    const/4 v2, 0x1

    .line 2292833
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    .line 2292834
    iget-object v3, v0, LX/BQ1;->l:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;

    move-object v0, v3

    .line 2292835
    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    .line 2292836
    iget-object v3, v0, LX/BQ1;->l:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;

    move-object v0, v3

    .line 2292837
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->c()Z

    move-result v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bz:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/BQO;->b:LX/0Tn;

    invoke-interface {v0, v3, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-static {}, LX/0i9;->a()Z

    move-result v0

    if-nez v0, :cond_18

    move v0, v2

    :goto_5
    move v0, v0

    .line 2292838
    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/facebook/timeline/TimelineFragment;->aU:Z

    if-nez v0, :cond_8

    .line 2292839
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/timeline/TimelineFragment;->aU:Z

    .line 2292840
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fq4;

    .line 2292841
    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->ad:Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    iget-object v5, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    .line 2292842
    iget-wide v8, v5, LX/5SB;->b:J

    move-wide v6, v8

    .line 2292843
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    .line 2292844
    new-instance v6, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;

    invoke-direct {v6, v0, v3, v4, v5}, Lcom/facebook/timeline/ProfileNuxModalRunnableHelper$1;-><init>(LX/Fq4;Landroid/app/Activity;LX/BQ1;Ljava/lang/String;)V

    move-object v0, v6

    .line 2292845
    const-wide/16 v4, 0x5dc

    const v3, 0x1028ca57

    invoke-static {v2, v0, v4, v5, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2292846
    :cond_8
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aB:LX/Fy4;

    .line 2292847
    iget-object v2, v0, LX/Fy4;->e:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    iput-object v2, v0, LX/Fy4;->l:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2292848
    iget-object v2, v0, LX/Fy4;->e:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->F()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v2

    iput-object v2, v0, LX/Fy4;->m:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2292849
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->z()Ljava/lang/String;

    move-result-object v0

    .line 2292850
    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->bI:LX/FtA;

    .line 2292851
    iget-object v3, v2, LX/FtA;->t:LX/Ft5;

    .line 2292852
    iput-object v0, v3, LX/Ft5;->a:Ljava/lang/String;

    .line 2292853
    invoke-static {p0}, Lcom/facebook/timeline/TimelineFragment;->ao(Lcom/facebook/timeline/TimelineFragment;)V

    .line 2292854
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bi:LX/BQ5;

    if-nez v0, :cond_9

    .line 2292855
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->T()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/BQ3;->b(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;)Z

    move-result v0

    if-eqz v0, :cond_d

    sget-object v0, LX/BQ5;->GRAPHQL_MAIN_REQUEST:LX/BQ5;

    :goto_6
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bi:LX/BQ5;

    .line 2292856
    :cond_9
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bg:LX/BQB;

    iget-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->bi:LX/BQ5;

    .line 2292857
    new-instance v4, LX/0P2;

    invoke-direct {v4}, LX/0P2;-><init>()V

    const-string v5, "source_header"

    invoke-virtual {p3}, Lcom/facebook/timeline/protocol/ResultSource;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v4

    const-string v5, "relationship_type"

    invoke-virtual {v1}, LX/9lQ;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v4

    const-string v5, "cover_photo_source"

    invoke-virtual {v3}, LX/BQ5;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v4

    .line 2292858
    goto :goto_7

    .line 2292859
    :goto_7
    invoke-virtual {v4}, LX/0P2;->b()LX/0P1;

    move-result-object v4

    .line 2292860
    iget-object v5, v0, LX/BQB;->c:LX/BQD;

    const-string v6, "TimelineLoadHeader"

    invoke-virtual {v5, v6, v4}, LX/BQD;->b(Ljava/lang/String;LX/0P1;)V

    .line 2292861
    iget-object v4, v0, LX/BQB;->g:LX/BQ7;

    const-string v5, "TimelineLoadHeader"

    invoke-virtual {v4, v5}, LX/BQ7;->b(Ljava/lang/String;)V

    .line 2292862
    iget-object v4, v0, LX/BQB;->h:Lcom/facebook/timeline/protocol/ResultSource;

    sget-object v5, Lcom/facebook/timeline/protocol/ResultSource;->UNDEFINED:Lcom/facebook/timeline/protocol/ResultSource;

    if-ne v4, v5, :cond_a

    .line 2292863
    iput-object p3, v0, LX/BQB;->h:Lcom/facebook/timeline/protocol/ResultSource;

    .line 2292864
    :cond_a
    iget-object v4, v0, LX/BQB;->j:LX/9lQ;

    sget-object v5, LX/9lQ;->UNDEFINED:LX/9lQ;

    if-ne v4, v5, :cond_b

    .line 2292865
    iput-object v1, v0, LX/BQB;->j:LX/9lQ;

    .line 2292866
    :cond_b
    const/4 v4, 0x1

    iput-boolean v4, v0, LX/BQB;->E:Z

    .line 2292867
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->E:LX/BPC;

    invoke-virtual {v0}, LX/BPC;->i()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2292868
    invoke-static {p0}, Lcom/facebook/timeline/TimelineFragment;->al(Lcom/facebook/timeline/TimelineFragment;)V

    .line 2292869
    :cond_c
    invoke-static {p0}, Lcom/facebook/timeline/TimelineFragment;->U(Lcom/facebook/timeline/TimelineFragment;)V

    goto/16 :goto_1

    .line 2292870
    :cond_d
    sget-object v0, LX/BQ5;->NO_PHOTO:LX/BQ5;

    goto :goto_6

    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 2292871
    :cond_f
    instance-of v7, v1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    if-eqz v7, :cond_6

    move-object v6, v1

    .line 2292872
    check-cast v6, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    goto/16 :goto_3

    .line 2292873
    :cond_10
    iput-object v2, v4, LX/BPy;->f:LX/0ta;

    .line 2292874
    invoke-virtual {v4, v1}, LX/BQ1;->a(Ljava/lang/Object;)V

    .line 2292875
    invoke-static {v3, v4}, LX/FvX;->a(LX/5SB;LX/BQ1;)V

    .line 2292876
    invoke-virtual {v4}, LX/BQ1;->t()LX/0am;

    move-result-object v7

    invoke-virtual {v7}, LX/0am;->isPresent()Z

    move-result v7

    if-eqz v7, :cond_12

    if-eqz v5, :cond_12

    .line 2292877
    invoke-virtual {v4}, LX/BQ1;->t()LX/0am;

    move-result-object v7

    invoke-virtual {v7}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemsConnectionFieldsModel;

    .line 2292878
    if-nez v7, :cond_15

    .line 2292879
    const/4 v8, 0x0

    .line 2292880
    :goto_8
    move-object v7, v8

    .line 2292881
    const/4 v8, 0x2

    .line 2292882
    iget-object p1, v0, LX/FvX;->a:LX/0ad;

    sget-short v1, LX/0wf;->W:S

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, LX/0ad;->a(SZ)Z

    move-result p1

    if-eqz p1, :cond_16

    .line 2292883
    :cond_11
    :goto_9
    move v8, v8

    .line 2292884
    invoke-virtual {v5, v7, v8}, LX/FrH;->a(LX/Fr5;I)V

    .line 2292885
    :cond_12
    if-eqz v5, :cond_13

    .line 2292886
    invoke-virtual {v5}, LX/BPB;->h()V

    .line 2292887
    :cond_13
    invoke-virtual {v6}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->c()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 2292888
    invoke-virtual {v6}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v7

    .line 2292889
    if-eqz v7, :cond_14

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-eq v7, v8, :cond_14

    .line 2292890
    iget-object v7, v0, LX/FvX;->b:LX/BPp;

    new-instance v8, LX/BPk;

    invoke-virtual {v4}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    invoke-virtual {v4}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    invoke-static {v3, p1, v1, v2}, Lcom/facebook/ipc/profile/TimelineFriendParams;->a(LX/5SB;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Lcom/facebook/ipc/profile/TimelineFriendParams;

    move-result-object p1

    invoke-direct {v8, p1}, LX/BPk;-><init>(Lcom/facebook/ipc/profile/TimelineFriendParams;)V

    invoke-virtual {v7, v8}, LX/0b4;->a(LX/0b7;)V

    .line 2292891
    :cond_14
    goto/16 :goto_4

    :cond_15
    new-instance v8, LX/Fr5;

    invoke-virtual {v7}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemsConnectionFieldsModel;->a()LX/0Px;

    move-result-object p1

    invoke-virtual {v7}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemsConnectionFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextItemsPageInfoModel;

    move-result-object v1

    invoke-direct {v8, p1, v1}, LX/Fr5;-><init>(LX/0Px;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextItemsPageInfoModel;)V

    goto :goto_8

    :cond_16
    invoke-virtual {v3}, LX/5SB;->i()Z

    move-result p1

    if-nez p1, :cond_17

    invoke-virtual {v4}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object p1

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne p1, v1, :cond_11

    :cond_17
    const/4 v8, 0x1

    goto :goto_9

    :cond_18
    const/4 v0, 0x0

    goto/16 :goto_5
.end method

.method public final b()Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2292790
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2292791
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2292792
    if-eqz v0, :cond_0

    .line 2292793
    iget-wide v6, v0, LX/5SB;->b:J

    move-wide v2, v6

    .line 2292794
    :goto_0
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gez v4, :cond_1

    .line 2292795
    const/4 v2, 0x0

    .line 2292796
    :goto_1
    move-object v0, v2

    .line 2292797
    return-object v0

    .line 2292798
    :cond_0
    const-string v2, "com.facebook.katana.profile.id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    goto :goto_0

    .line 2292799
    :cond_1
    const-string v4, "profile_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v4, v2}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    goto :goto_1
.end method

.method public final b(LX/Fso;)V
    .locals 5

    .prologue
    .line 2292777
    invoke-direct {p0}, Lcom/facebook/timeline/TimelineFragment;->ap()V

    .line 2292778
    iget-boolean v0, p1, LX/Fso;->f:Z

    if-eqz v0, :cond_0

    .line 2292779
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bg:LX/BQB;

    invoke-virtual {v0}, LX/BQB;->j()V

    .line 2292780
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->F:LX/G4x;

    if-eqz v0, :cond_1

    .line 2292781
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->F:LX/G4x;

    sget-object v1, LX/G5A;->FAILED:LX/G5A;

    invoke-virtual {v0, p1, v1}, LX/G4x;->a(LX/Fso;LX/G5A;)V

    .line 2292782
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bg:LX/BQB;

    iget-object v1, p1, LX/Fso;->a:Ljava/lang/String;

    const/4 p1, 0x3

    .line 2292783
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2292784
    iget-object v2, v0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x1a0020

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v4

    invoke-interface {v2, v3, v4, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2292785
    iget-object v2, v0, LX/BQB;->C:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2292786
    iget-object v2, v0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x1a0015

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v4

    invoke-interface {v2, v3, v4, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2292787
    invoke-direct {p0}, Lcom/facebook/timeline/TimelineFragment;->ai()V

    .line 2292788
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2292789
    return-void
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 2292774
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bx:LX/G34;

    if-eqz v0, :cond_0

    .line 2292775
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bx:LX/G34;

    invoke-virtual {v0}, LX/G34;->c()V

    .line 2292776
    :cond_0
    return-void
.end method

.method public final d(I)V
    .locals 5

    .prologue
    .line 2292771
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    if-eqz v0, :cond_0

    .line 2292772
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ad:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/timeline/TimelineFragment$9;

    invoke-direct {v1, p0, p1}, Lcom/facebook/timeline/TimelineFragment$9;-><init>(Lcom/facebook/timeline/TimelineFragment;I)V

    const-wide/16 v2, 0x0

    const v4, -0x318849e5

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2292773
    :cond_0
    return-void
.end method

.method public final dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2292767
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/timeline/BaseTimelineFragment;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2292768
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    if-eqz v0, :cond_0

    .line 2292769
    new-instance v0, LX/5Mj;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bp:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/5Mk;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/5Mj;-><init>(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;LX/5Mk;)V

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/TimelineFragment;->a(LX/5Mj;)V

    .line 2292770
    :cond_0
    return-void
.end method

.method public final e()LX/Fv9;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2292766
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bo:LX/Fv9;

    return-object v0
.end method

.method public final e_(Z)V
    .locals 0

    .prologue
    .line 2292764
    invoke-virtual {p0}, Lcom/facebook/timeline/TimelineFragment;->E()V

    .line 2292765
    return-void
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 2292763
    invoke-virtual {p0}, Lcom/facebook/timeline/TimelineFragment;->mK_()LX/63R;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/TimelineFragment;->mK_()LX/63R;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ao:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2292761
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    invoke-interface {v0}, LX/0g8;->z()LX/0P1;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    invoke-interface {v2, v1}, LX/0g8;->h(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2292762
    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 2292759
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/timeline/TimelineFragment;->at:Z

    .line 2292760
    return-void
.end method

.method public final ig_()V
    .locals 0

    .prologue
    .line 2292758
    return-void
.end method

.method public final j()Lcom/facebook/search/api/GraphSearchQuery;
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 2292737
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bw:LX/G48;

    if-nez v1, :cond_1

    .line 2292738
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bD:LX/0ad;

    sget-short v2, LX/100;->N:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bD:LX/0ad;

    sget-short v2, LX/100;->P:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 2292739
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ai:LX/G49;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->z()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    .line 2292740
    iget-wide v4, v2, LX/5SB;->b:J

    move-wide v2, v4

    .line 2292741
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 2292742
    new-instance v4, LX/G48;

    invoke-static {v1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {v4, v0, v2, v3}, LX/G48;-><init>(Ljava/lang/String;Ljava/lang/Long;LX/0ad;)V

    .line 2292743
    move-object v0, v4

    .line 2292744
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bw:LX/G48;

    .line 2292745
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bw:LX/G48;

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 2292746
    iget-object v4, v0, LX/G48;->c:LX/0ad;

    sget-short v5, LX/100;->N:S

    invoke-interface {v4, v5, v10}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2292747
    sget-object v4, LX/7BH;->LIGHT:LX/7BH;

    sget-object v5, LX/103;->USER:LX/103;

    iget-object v6, v0, LX/G48;->b:Ljava/lang/String;

    iget-object v7, v0, LX/G48;->a:Ljava/lang/String;

    sget-object v8, LX/7B5;->TAB:LX/7B5;

    invoke-static/range {v4 .. v9}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7BH;LX/103;Ljava/lang/String;Ljava/lang/String;LX/7B5;Z)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v4

    .line 2292748
    sget-object v5, LX/7B4;->SCOPED_TAB:LX/7B4;

    new-instance v6, LX/7BC;

    invoke-direct {v6}, LX/7BC;-><init>()V

    .line 2292749
    iput-boolean v9, v6, LX/7BC;->a:Z

    .line 2292750
    move-object v6, v6

    .line 2292751
    iget-object v7, v0, LX/G48;->c:LX/0ad;

    sget-short v8, LX/100;->O:S

    invoke-interface {v7, v8, v10}, LX/0ad;->a(SZ)Z

    move-result v7

    .line 2292752
    iput-boolean v7, v6, LX/7BC;->b:Z

    .line 2292753
    move-object v6, v6

    .line 2292754
    invoke-virtual {v6}, LX/7BC;->a()Lcom/facebook/search/api/GraphSearchQueryTabModifier;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7B4;Landroid/os/Parcelable;)V

    .line 2292755
    :goto_1
    move-object v0, v4

    .line 2292756
    return-object v0

    .line 2292757
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    sget-object v4, LX/7BH;->LIGHT:LX/7BH;

    sget-object v5, LX/103;->USER:LX/103;

    iget-object v6, v0, LX/G48;->b:Ljava/lang/String;

    iget-object v7, v0, LX/G48;->a:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static/range {v4 .. v9}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7BH;LX/103;Ljava/lang/String;Ljava/lang/String;LX/7B5;Z)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v4

    goto :goto_1
.end method

.method public final k()LX/0g8;
    .locals 1

    .prologue
    .line 2292736
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    return-object v0
.end method

.method public final kR_()LX/0cQ;
    .locals 1

    .prologue
    .line 2292735
    sget-object v0, LX/0cQ;->TIMELINE_COVERPHOTO_FRAGMENT:LX/0cQ;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2292734
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->T()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->T()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->T()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final lk_()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2292728
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    if-eqz v0, :cond_0

    .line 2292729
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    invoke-virtual {v0}, LX/FrV;->a()LX/Fra;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/Fra;->a(I)LX/FsE;

    move-result-object v0

    .line 2292730
    invoke-static {p0, v0}, Lcom/facebook/timeline/TimelineFragment;->a(Lcom/facebook/timeline/TimelineFragment;LX/FsE;)V

    .line 2292731
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    .line 2292732
    iput v1, v0, LX/BPy;->g:I

    .line 2292733
    :cond_0
    return-void
.end method

.method public final ll_()V
    .locals 2

    .prologue
    .line 2292723
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    if-eqz v0, :cond_0

    .line 2292724
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    invoke-virtual {v0}, LX/FrV;->a()LX/Fra;

    move-result-object v0

    .line 2292725
    invoke-static {v0}, LX/Fra;->e(LX/Fra;)LX/Fs2;

    move-result-object v1

    sget-object p0, LX/Fs1;->REFRESH_ON_RESUME:LX/Fs1;

    .line 2292726
    iput-object p0, v1, LX/Fs2;->i:LX/Fs1;

    .line 2292727
    :cond_0
    return-void
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2292722
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->G()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final mJ_()V
    .locals 5

    .prologue
    .line 2292702
    const/4 v0, 0x1

    .line 2292703
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    const/4 v2, 0x1

    .line 2292704
    iput v2, v1, LX/BPy;->g:I

    .line 2292705
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->F:LX/G4x;

    sget-object v2, LX/G5A;->LOADING:LX/G5A;

    invoke-virtual {v1, v2}, LX/G4x;->a(LX/G5A;)V

    .line 2292706
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2292707
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    if-eqz v1, :cond_0

    .line 2292708
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    .line 2292709
    invoke-virtual {v1}, LX/FrV;->a()LX/Fra;

    move-result-object v2

    invoke-virtual {v2}, LX/Fra;->a()V

    .line 2292710
    invoke-virtual {v1}, LX/FrV;->b()LX/Frd;

    move-result-object v2

    invoke-virtual {v2}, LX/Frd;->b()V

    .line 2292711
    const/4 v2, 0x1

    .line 2292712
    invoke-virtual {v1}, LX/FrV;->a()LX/Fra;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/Fra;->a(I)LX/FsE;

    move-result-object v3

    .line 2292713
    invoke-virtual {v1}, LX/FrV;->b()LX/Frd;

    move-result-object v4

    if-ne v0, v2, :cond_1

    :goto_0
    invoke-virtual {v4, v2}, LX/Frd;->a(Z)LX/FsJ;

    move-result-object v2

    .line 2292714
    invoke-virtual {v1}, LX/FrV;->b()LX/Frd;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/Frd;->b(LX/FsJ;)V

    .line 2292715
    move-object v2, v3

    .line 2292716
    move-object v1, v2

    .line 2292717
    invoke-static {p0, v1}, Lcom/facebook/timeline/TimelineFragment;->a(Lcom/facebook/timeline/TimelineFragment;LX/FsE;)V

    .line 2292718
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bu:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wL;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->ap:LX/G4q;

    const v3, 0x7f0805ec

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2292719
    invoke-static {v1, v2, v3}, LX/0wL;->b(LX/0wL;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 2292720
    return-void

    .line 2292721
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final mK_()LX/63R;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2293074
    const/4 v1, 0x0

    .line 2293075
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    invoke-interface {v0, v3}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, LX/63R;

    if-eqz v0, :cond_0

    .line 2293076
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    invoke-interface {v0, v3}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/63R;

    .line 2293077
    :goto_0
    return-object v0

    .line 2293078
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    invoke-interface {v0, v3}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 2293079
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    invoke-interface {v0, v3}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2293080
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v2, v2, LX/63R;

    if-eqz v2, :cond_1

    .line 2293081
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/63R;

    goto :goto_0

    .line 2293082
    :cond_1
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v2, v2, Landroid/view/ViewGroup;

    if-eqz v2, :cond_2

    .line 2293083
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2293084
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v2, v2, LX/63R;

    if-eqz v2, :cond_2

    .line 2293085
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/63R;

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2293350
    const-string v0, "timeline"

    return-object v0
.end method

.method public final o()LX/G4x;
    .locals 1

    .prologue
    .line 2293349
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->F:LX/G4x;

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2293337
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bk:LX/0QR;

    if-eqz v0, :cond_0

    .line 2293338
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bk:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fq7;

    invoke-virtual {v0, p1, p2, p3}, LX/Fq7;->a(IILandroid/content/Intent;)V

    .line 2293339
    :cond_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_2

    .line 2293340
    :cond_1
    :goto_0
    return-void

    .line 2293341
    :cond_2
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 2293342
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bC:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EV;

    .line 2293343
    iget-object v1, v0, LX/1EV;->a:LX/0Uh;

    const/16 v2, 0x30b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 2293344
    if-eqz v0, :cond_1

    .line 2293345
    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 2293346
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bE:LX/DJ6;

    if-nez v1, :cond_3

    .line 2293347
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->al:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/DJ6;

    iput-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bE:LX/DJ6;

    .line 2293348
    :cond_3
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bE:LX/DJ6;

    iget-object v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    sget-object v2, LX/21D;->TIMELINE:LX/21D;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, LX/DJ6;->a(Ljava/lang/String;LX/21D;Landroid/content/Context;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x6dc
        :pswitch_0
    .end packed-switch
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2293334
    invoke-super {p0, p1}, Lcom/facebook/timeline/BaseTimelineFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2293335
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    invoke-virtual {v0, p1}, LX/99d;->a(Landroid/content/res/Configuration;)V

    .line 2293336
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x5c4e0d04

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v11

    .line 2293295
    new-instance v0, LX/G4q;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/G4q;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ap:LX/G4q;

    .line 2293296
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    .line 2293297
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ap:LX/G4q;

    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 2293298
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ap:LX/G4q;

    const v1, 0x7f0d1239

    invoke-virtual {v0, v1}, LX/G4q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aq:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 2293299
    const v0, 0x7f0314d9

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aq:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2293300
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aq:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-instance v1, LX/FqK;

    invoke-direct {v1, p0}, LX/FqK;-><init>(Lcom/facebook/timeline/TimelineFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2293301
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->Q:LX/G0p;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ap:LX/G4q;

    .line 2293302
    invoke-virtual {v1}, Lcom/facebook/widget/CustomRelativeLayout;->asViewGroup()Landroid/view/ViewGroup;

    move-result-object v2

    const v3, 0x102000a

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2293303
    new-instance v3, LX/1Oz;

    invoke-direct {v3, v2}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2293304
    new-instance v3, LX/0g6;

    invoke-direct {v3, v2}, LX/0g6;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    .line 2293305
    const/4 v2, 0x1

    invoke-interface {v3, v2}, LX/0g8;->d(Z)V

    .line 2293306
    invoke-interface {v3}, LX/0g8;->k()V

    .line 2293307
    invoke-interface {v3}, LX/0g8;->b()Landroid/view/ViewGroup;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    .line 2293308
    iget-object v2, v0, LX/G0p;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Db;

    .line 2293309
    invoke-virtual {v2}, LX/1Db;->a()LX/1St;

    move-result-object v2

    invoke-interface {v3, v2}, LX/0g8;->a(LX/1St;)V

    .line 2293310
    move-object v0, v3

    .line 2293311
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    .line 2293312
    new-instance v4, Lcom/facebook/timeline/TimelineFragment$5;

    invoke-direct {v4, p0}, Lcom/facebook/timeline/TimelineFragment$5;-><init>(Lcom/facebook/timeline/TimelineFragment;)V

    .line 2293313
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ab:LX/FtB;

    const-string v1, "native_timeline"

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->bq:LX/FuR;

    iget-object v5, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    invoke-static {v5}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/timeline/TimelineFragment;->bv:LX/1Jg;

    iget-object v7, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    invoke-virtual/range {v0 .. v7}, LX/FtB;->a(Ljava/lang/String;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;LX/1Jg;LX/5SB;)LX/FtA;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bI:LX/FtA;

    .line 2293314
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->P:LX/G0o;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->bd:LX/FrH;

    iget-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    iget-object v4, p0, Lcom/facebook/timeline/TimelineFragment;->bb:LX/G4m;

    iget-object v5, p0, Lcom/facebook/timeline/TimelineFragment;->bo:LX/Fv9;

    iget-boolean v6, p0, Lcom/facebook/timeline/TimelineFragment;->at:Z

    iget-object v7, p0, Lcom/facebook/timeline/TimelineFragment;->bI:LX/FtA;

    iget-object v8, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    invoke-virtual/range {v0 .. v8}, LX/G0o;->a(LX/BP0;LX/FrH;LX/BQ1;LX/G4m;LX/Fv9;ZLX/FtA;LX/0g8;)LX/G0n;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    .line 2293315
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->ap:LX/G4q;

    .line 2293316
    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2293317
    invoke-virtual {v1}, LX/G0n;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2293318
    const v3, 0x7f0d2f38

    invoke-virtual {v2, v3}, LX/G4q;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewStub;

    .line 2293319
    invoke-virtual {v3}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v3

    invoke-interface {v0, v3}, LX/0g8;->f(Landroid/view/View;)V

    .line 2293320
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bv:LX/1Jg;

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    invoke-virtual {v1}, LX/99d;->e()LX/1R4;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Jg;->a(LX/1R4;)V

    .line 2293321
    invoke-direct {p0}, Lcom/facebook/timeline/TimelineFragment;->T()V

    .line 2293322
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ap:LX/G4q;

    new-instance v1, LX/FqL;

    invoke-direct {v1, p0}, LX/FqL;-><init>(Lcom/facebook/timeline/TimelineFragment;)V

    .line 2293323
    iput-object v1, v0, LX/G4q;->a:LX/FqL;

    .line 2293324
    new-instance v0, LX/FqM;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/FqM;-><init>(Lcom/facebook/timeline/TimelineFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aD:LX/1PF;

    .line 2293325
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->L:LX/Ft1;

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    invoke-virtual {v0}, LX/5SB;->g()J

    move-result-wide v2

    iget-object v4, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    iget-object v5, p0, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    iget-object v6, p0, Lcom/facebook/timeline/TimelineFragment;->be:LX/BQG;

    iget-object v7, p0, Lcom/facebook/timeline/TimelineFragment;->bg:LX/BQB;

    iget-object v8, p0, Lcom/facebook/timeline/TimelineFragment;->bn:LX/0QR;

    iget-object v9, p0, Lcom/facebook/timeline/TimelineFragment;->bo:LX/Fv9;

    .line 2293326
    const-string v0, "timeline"

    move-object v10, v0

    .line 2293327
    invoke-virtual/range {v1 .. v10}, LX/Ft1;->a(JLX/0g8;LX/G0n;LX/BQG;LX/BQB;LX/0QR;LX/Fv9;Ljava/lang/String;)LX/Ft0;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bj:LX/Ft0;

    .line 2293328
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ap:LX/G4q;

    .line 2293329
    const v1, 0x7f0d0ab2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 2293330
    const-string v2, "Unable to find error view or error view stub"

    invoke-static {v1, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2293331
    new-instance v2, LX/0zw;

    invoke-direct {v2, v1}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    move-object v0, v2

    .line 2293332
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->as:LX/0zw;

    .line 2293333
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ap:LX/G4q;

    const/4 v1, 0x2

    const/16 v2, 0x2b

    const v3, -0x14747e04

    invoke-static {v1, v2, v3, v11}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroy()V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x2008fb79    # -3.5599E19f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2293248
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->p:LX/1CF;

    if-eqz v1, :cond_0

    .line 2293249
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->p:LX/1CF;

    invoke-virtual {v1}, LX/14l;->b()V

    .line 2293250
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bs:LX/0Yb;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bs:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2293251
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bs:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2293252
    :cond_1
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aX:LX/1B1;

    if-eqz v1, :cond_2

    .line 2293253
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aX:LX/1B1;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->B:LX/2do;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2293254
    :cond_2
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aY:LX/1B1;

    if-eqz v1, :cond_3

    .line 2293255
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aY:LX/1B1;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->C:LX/0b3;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2293256
    :cond_3
    iget-boolean v1, p0, Lcom/facebook/timeline/TimelineFragment;->aS:Z

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->aQ:LX/FsE;

    invoke-static {v1, v2}, LX/Fsw;->a(ZLX/FsE;)V

    .line 2293257
    iput-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->aQ:LX/FsE;

    .line 2293258
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    if-eqz v1, :cond_4

    .line 2293259
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    invoke-virtual {v1}, LX/FrV;->c()V

    .line 2293260
    :cond_4
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->af:LX/0yc;

    invoke-virtual {v1, p0}, LX/0yc;->b(LX/0yL;)V

    .line 2293261
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v1}, LX/BPy;->p()V

    .line 2293262
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bc:LX/G1N;

    .line 2293263
    const/4 v5, 0x0

    .line 2293264
    const/4 v2, 0x0

    iput-object v2, v1, LX/BPB;->b:Ljava/lang/Object;

    .line 2293265
    iput v5, v1, LX/BPB;->d:I

    .line 2293266
    iput v5, v1, LX/BPB;->a:I

    .line 2293267
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bd:LX/FrH;

    .line 2293268
    const/4 v5, 0x0

    .line 2293269
    const/4 v2, 0x0

    iput-object v2, v1, LX/BPB;->b:Ljava/lang/Object;

    .line 2293270
    iput v5, v1, LX/BPB;->d:I

    .line 2293271
    iput v5, v1, LX/BPB;->a:I

    .line 2293272
    iput-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->aC:LX/0QR;

    .line 2293273
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aL:LX/Fqc;

    if-eqz v1, :cond_5

    .line 2293274
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aL:LX/Fqc;

    invoke-virtual {v1}, LX/Fqc;->b()V

    .line 2293275
    iput-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->aL:LX/Fqc;

    .line 2293276
    :cond_5
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aV:LX/FvM;

    if-eqz v1, :cond_6

    .line 2293277
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aV:LX/FvM;

    invoke-interface {v1, v3}, LX/FvM;->a(Lcom/facebook/timeline/TimelineFragment;)V

    .line 2293278
    iput-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->aV:LX/FvM;

    .line 2293279
    :cond_6
    iput-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    .line 2293280
    iput-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    .line 2293281
    iput-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->bc:LX/G1N;

    .line 2293282
    iput-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->bd:LX/FrH;

    .line 2293283
    iput-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->p:LX/1CF;

    .line 2293284
    iput-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->bs:LX/0Yb;

    .line 2293285
    iput-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->q:LX/Fqd;

    .line 2293286
    iput-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->r:LX/FrG;

    .line 2293287
    iput-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->s:LX/G0y;

    .line 2293288
    iput-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->u:LX/G2e;

    .line 2293289
    iput-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->v:LX/FyE;

    .line 2293290
    iput-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->w:LX/FwB;

    .line 2293291
    iput-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->y:LX/G4f;

    .line 2293292
    iput-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->bL:LX/8Kc;

    .line 2293293
    invoke-super {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->onDestroy()V

    .line 2293294
    const/16 v1, 0x2b

    const v2, 0x6698ef62

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x6b875434

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2293203
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    .line 2293204
    iget-object v4, v1, LX/G0n;->c:LX/FvG;

    .line 2293205
    iget-object v5, v4, LX/FvG;->a:LX/FvV;

    .line 2293206
    const/4 v6, 0x1

    iput-boolean v6, v5, LX/Fv4;->h:Z

    .line 2293207
    iget-object v5, v4, LX/FvG;->c:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 2293208
    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2293209
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/BP9;

    .line 2293210
    if-eqz v5, :cond_0

    .line 2293211
    instance-of v6, v5, Landroid/view/View;

    if-eqz v6, :cond_1

    move-object v6, v5

    .line 2293212
    check-cast v6, Landroid/view/View;

    .line 2293213
    invoke-virtual {v6}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    if-nez v6, :cond_0

    .line 2293214
    invoke-interface {v5}, LX/BP9;->c()V

    goto :goto_0

    .line 2293215
    :cond_1
    invoke-interface {v5}, LX/BP9;->c()V

    goto :goto_0

    .line 2293216
    :cond_2
    iget-object v5, v4, LX/FvG;->c:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->clear()V

    .line 2293217
    iget-object v4, v1, LX/G0n;->g:LX/1Qq;

    move-object v4, v4

    .line 2293218
    invoke-interface {v4}, LX/0Vf;->dispose()V

    .line 2293219
    iget-object v4, v1, LX/G0n;->d:LX/1Qq;

    invoke-interface {v4}, LX/0Vf;->dispose()V

    .line 2293220
    iget-object v4, v1, LX/G0n;->e:LX/1Qq;

    if-eqz v4, :cond_3

    .line 2293221
    iget-object v4, v1, LX/G0n;->e:LX/1Qq;

    invoke-interface {v4}, LX/0Vf;->dispose()V

    .line 2293222
    :cond_3
    iget-object v4, v1, LX/G0n;->f:LX/1Qq;

    if-eqz v4, :cond_4

    .line 2293223
    iget-object v4, v1, LX/G0n;->f:LX/1Qq;

    invoke-interface {v4}, LX/0Vf;->dispose()V

    .line 2293224
    :cond_4
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v1}, LX/BPy;->c()V

    .line 2293225
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bc:LX/G1N;

    invoke-virtual {v1}, LX/BPB;->c()V

    .line 2293226
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bd:LX/FrH;

    invoke-virtual {v1}, LX/BPB;->c()V

    .line 2293227
    invoke-super {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->onDestroyView()V

    .line 2293228
    iput-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->bj:LX/Ft0;

    .line 2293229
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->as:LX/0zw;

    invoke-static {v1}, LX/G4p;->a(LX/0zw;)V

    .line 2293230
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    if-eqz v1, :cond_5

    .line 2293231
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    invoke-interface {v1, v2}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2293232
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    invoke-interface {v1}, LX/0g8;->x()V

    .line 2293233
    :cond_5
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aD:LX/1PF;

    if-eqz v1, :cond_6

    .line 2293234
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aD:LX/1PF;

    invoke-virtual {v1}, LX/14l;->b()V

    .line 2293235
    iput-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->aD:LX/1PF;

    .line 2293236
    :cond_6
    iput-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->ap:LX/G4q;

    .line 2293237
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bx:LX/G34;

    if-eqz v1, :cond_7

    .line 2293238
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bx:LX/G34;

    .line 2293239
    iget-object v4, v1, LX/G34;->d:Landroid/view/View;

    if-eqz v4, :cond_7

    .line 2293240
    iget-object v4, v1, LX/G34;->d:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2293241
    :cond_7
    iput-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    .line 2293242
    iput-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    .line 2293243
    iput-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->as:LX/0zw;

    .line 2293244
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aq:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v1, :cond_8

    .line 2293245
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aq:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2293246
    iput-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->aq:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 2293247
    :cond_8
    const/16 v1, 0x2b

    const v2, -0x3b120ab1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4b2a0276

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2293171
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aD:LX/1PF;

    if-eqz v1, :cond_0

    .line 2293172
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aD:LX/1PF;

    invoke-virtual {v1}, LX/14l;->b()V

    .line 2293173
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bg:LX/BQB;

    invoke-virtual {v1}, LX/BQB;->a()V

    .line 2293174
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aW:LX/1B1;

    if-eqz v1, :cond_1

    .line 2293175
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aW:LX/1B1;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->A:LX/BPp;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2293176
    :cond_1
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->br:LX/FuI;

    if-eqz v1, :cond_2

    .line 2293177
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->br:LX/FuI;

    .line 2293178
    iget-object v2, v1, LX/FuI;->j:LX/6Vi;

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :goto_0
    const-string v5, "Trying to stop TimelineScrubberClickEventProcessor that was not started"

    invoke-static {v2, v5}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2293179
    iget-object v2, v1, LX/FuI;->b:LX/1K9;

    iget-object v5, v1, LX/FuI;->j:LX/6Vi;

    invoke-virtual {v2, v5}, LX/1K9;->a(LX/6Vi;)V

    .line 2293180
    const/4 v2, 0x0

    iput-object v2, v1, LX/FuI;->j:LX/6Vi;

    .line 2293181
    iput-object v4, p0, Lcom/facebook/timeline/TimelineFragment;->br:LX/FuI;

    .line 2293182
    :cond_2
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bE:LX/DJ6;

    if-eqz v1, :cond_3

    .line 2293183
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bE:LX/DJ6;

    invoke-virtual {v1}, LX/DJ6;->a()V

    .line 2293184
    :cond_3
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bj:LX/Ft0;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    const/4 v7, 0x0

    .line 2293185
    iget-boolean v5, v1, LX/Ft0;->n:Z

    if-eqz v5, :cond_4

    .line 2293186
    iget-object v5, v1, LX/Ft0;->g:LX/0QR;

    invoke-interface {v5}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/195;

    invoke-virtual {v5}, LX/195;->b()V

    .line 2293187
    :cond_4
    iget-object v5, v1, LX/Ft0;->h:LX/0QR;

    invoke-interface {v5}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/23N;

    invoke-virtual {v5, v2}, LX/1Kt;->c(LX/0g8;)V

    .line 2293188
    iget-object v5, v1, LX/Ft0;->i:LX/Fv9;

    invoke-virtual {v5, v7}, LX/Fv9;->a(Z)V

    .line 2293189
    iget-object v5, v1, LX/Ft0;->i:LX/Fv9;

    invoke-virtual {v5, v7, v7}, LX/Fv9;->a(IZ)V

    .line 2293190
    iget-object v5, v1, LX/Ft0;->i:LX/Fv9;

    const/4 v6, 0x1

    invoke-virtual {v5, v6, v7}, LX/Fv9;->a(IZ)V

    .line 2293191
    iget-object v5, v1, LX/Ft0;->a:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1LV;

    .line 2293192
    const-string v6, "unknown"

    invoke-virtual {v5, v6}, LX/1LV;->a(Ljava/lang/String;)V

    .line 2293193
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/1LV;->a(LX/0P1;)V

    .line 2293194
    iget-boolean v1, p0, Lcom/facebook/timeline/TimelineFragment;->au:Z

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/facebook/timeline/TimelineFragment;->k()LX/0g8;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 2293195
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bv:LX/1Jg;

    invoke-interface {v1}, LX/1Jg;->b()V

    .line 2293196
    invoke-virtual {p0}, Lcom/facebook/timeline/TimelineFragment;->k()LX/0g8;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->bv:LX/1Jg;

    invoke-interface {v2}, LX/1Jg;->a()LX/0fx;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0g8;->c(LX/0fx;)V

    .line 2293197
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/timeline/TimelineFragment;->av:Z

    .line 2293198
    :cond_5
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->J:LX/2SI;

    .line 2293199
    iput-object v4, v1, LX/2SI;->j:LX/23S;

    .line 2293200
    invoke-super {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->onPause()V

    .line 2293201
    const/16 v1, 0x2b

    const v2, -0x3993e07a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2293202
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public final onResume()V
    .locals 8

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, -0x6c323025

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2293118
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aD:LX/1PF;

    if-eqz v1, :cond_0

    .line 2293119
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aD:LX/1PF;

    invoke-virtual {v1}, LX/14l;->a()V

    .line 2293120
    :cond_0
    invoke-super {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->onResume()V

    .line 2293121
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aW:LX/1B1;

    if-eqz v1, :cond_1

    .line 2293122
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aW:LX/1B1;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->A:LX/BPp;

    invoke-virtual {v1, v2}, LX/1B1;->a(LX/0b4;)V

    .line 2293123
    :cond_1
    iget-boolean v1, p0, Lcom/facebook/timeline/TimelineFragment;->av:Z

    if-eqz v1, :cond_2

    .line 2293124
    invoke-direct {p0}, Lcom/facebook/timeline/TimelineFragment;->ae()V

    .line 2293125
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/timeline/TimelineFragment;->av:Z

    .line 2293126
    :cond_2
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aa:LX/FuJ;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    iget-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->bb:LX/G4m;

    iget-object v4, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    iget-object v5, p0, Lcom/facebook/timeline/TimelineFragment;->I:LX/BQ9;

    invoke-virtual {v1, v2, v3, v4, v5}, LX/FuJ;->a(LX/BQ1;LX/G4m;LX/5SB;LX/BQ9;)LX/FuI;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->br:LX/FuI;

    .line 2293127
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->br:LX/FuI;

    .line 2293128
    iget-object v2, v1, LX/FuI;->j:LX/6Vi;

    if-nez v2, :cond_3

    .line 2293129
    iget-object v2, v1, LX/FuI;->b:LX/1K9;

    const-class v3, LX/G1D;

    .line 2293130
    new-instance v4, LX/FuG;

    invoke-direct {v4, v1}, LX/FuG;-><init>(LX/FuI;)V

    move-object v4, v4

    .line 2293131
    invoke-virtual {v2, v3, v4}, LX/1K9;->a(Ljava/lang/Class;LX/6Ve;)LX/6Vi;

    move-result-object v2

    iput-object v2, v1, LX/FuI;->j:LX/6Vi;

    .line 2293132
    :cond_3
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    .line 2293133
    invoke-virtual {v1}, LX/FrV;->a()LX/Fra;

    move-result-object v2

    .line 2293134
    const/4 v3, 0x0

    .line 2293135
    invoke-static {v2}, LX/Fra;->e(LX/Fra;)LX/Fs2;

    move-result-object v4

    .line 2293136
    iget-object v5, v4, LX/Fs2;->i:LX/Fs1;

    move-object v4, v5

    .line 2293137
    sget-object v5, LX/Fs1;->REFRESH_ON_RESUME:LX/Fs1;

    if-ne v4, v5, :cond_4

    .line 2293138
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/Fra;->a(I)LX/FsE;

    move-result-object v3

    .line 2293139
    :cond_4
    invoke-static {v2}, LX/Fra;->e(LX/Fra;)LX/Fs2;

    move-result-object v4

    sget-object v5, LX/Fs1;->VISIBLE:LX/Fs1;

    .line 2293140
    iput-object v5, v4, LX/Fs2;->i:LX/Fs1;

    .line 2293141
    move-object v2, v3

    .line 2293142
    invoke-virtual {v1}, LX/FrV;->b()LX/Frd;

    move-result-object v3

    .line 2293143
    const/4 v4, 0x0

    .line 2293144
    invoke-static {v3}, LX/Frd;->g(LX/Frd;)LX/Fs2;

    move-result-object v5

    .line 2293145
    iget-object v7, v5, LX/Fs2;->i:LX/Fs1;

    move-object v5, v7

    .line 2293146
    sget-object v7, LX/Fs1;->REFRESH_ON_RESUME:LX/Fs1;

    if-ne v5, v7, :cond_8

    .line 2293147
    invoke-virtual {v3}, LX/Frd;->e()LX/FsJ;

    move-result-object v4

    .line 2293148
    :cond_5
    :goto_0
    invoke-static {v3}, LX/Frd;->g(LX/Frd;)LX/Fs2;

    move-result-object v5

    sget-object v7, LX/Fs1;->VISIBLE:LX/Fs1;

    .line 2293149
    iput-object v7, v5, LX/Fs2;->i:LX/Fs1;

    .line 2293150
    move-object v3, v4

    .line 2293151
    if-eqz v3, :cond_6

    .line 2293152
    invoke-virtual {v1}, LX/FrV;->b()LX/Frd;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/Frd;->b(LX/FsJ;)V

    .line 2293153
    :cond_6
    move-object v1, v2

    .line 2293154
    if-eqz v1, :cond_7

    .line 2293155
    invoke-static {p0, v1}, Lcom/facebook/timeline/TimelineFragment;->a(Lcom/facebook/timeline/TimelineFragment;LX/FsE;)V

    .line 2293156
    :cond_7
    invoke-direct {p0}, Lcom/facebook/timeline/TimelineFragment;->af()V

    .line 2293157
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bg:LX/BQB;

    .line 2293158
    iget-object v2, v1, LX/BQB;->c:LX/BQD;

    const-string v3, "TimelineFragmentCreate"

    invoke-virtual {v2, v3}, LX/BQD;->b(Ljava/lang/String;)V

    .line 2293159
    invoke-static {p0}, Lcom/facebook/timeline/TimelineFragment;->ao(Lcom/facebook/timeline/TimelineFragment;)V

    .line 2293160
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->bj:LX/Ft0;

    .line 2293161
    iget-object v2, v1, LX/Ft0;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1LV;

    .line 2293162
    const-string v3, "native_timeline"

    invoke-virtual {v2, v3}, LX/1LV;->a(Ljava/lang/String;)V

    .line 2293163
    invoke-static {v1, v2}, LX/Ft0;->a(LX/Ft0;LX/1LV;)V

    .line 2293164
    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->J:LX/2SI;

    .line 2293165
    iput-object p0, v1, LX/2SI;->j:LX/23S;

    .line 2293166
    const/16 v1, 0x2b

    const v2, -0x5842863c

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2293167
    :cond_8
    invoke-static {v3}, LX/Frd;->g(LX/Frd;)LX/Fs2;

    move-result-object v5

    .line 2293168
    iget-object v7, v5, LX/Fs2;->i:LX/Fs1;

    move-object v5, v7

    .line 2293169
    sget-object v7, LX/Fs1;->CANCELLED:LX/Fs1;

    if-ne v5, v7, :cond_5

    .line 2293170
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/Frd;->a(Z)LX/FsJ;

    move-result-object v4

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2293113
    invoke-super {p0, p1}, Lcom/facebook/timeline/BaseTimelineFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2293114
    const-string v0, "fragment_uuid"

    iget-object v1, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    .line 2293115
    iget-object p0, v1, LX/5SB;->d:Landroid/os/ParcelUuid;

    move-object v1, p0

    .line 2293116
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2293117
    return-void
.end method

.method public final onStart()V
    .locals 14

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/16 v0, 0x2a

    const v1, 0x1b9f9cfa

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2293086
    invoke-super {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->onStart()V

    .line 2293087
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    invoke-interface {v0, p0}, LX/0g8;->a(LX/0fx;)V

    .line 2293088
    iget-boolean v0, p0, Lcom/facebook/timeline/TimelineFragment;->aF:Z

    if-eqz v0, :cond_0

    .line 2293089
    new-instance v0, LX/FqN;

    invoke-direct {v0, p0}, LX/FqN;-><init>(Lcom/facebook/timeline/TimelineFragment;)V

    .line 2293090
    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->W:LX/1lu;

    iget-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->F:LX/G4x;

    .line 2293091
    iget-object v4, v3, LX/G4x;->e:LX/0qm;

    move-object v3, v4

    .line 2293092
    invoke-virtual {v2, v0, v3}, LX/1lu;->a(LX/1gl;LX/0qm;)V

    .line 2293093
    iput-boolean v5, p0, Lcom/facebook/timeline/TimelineFragment;->aF:Z

    .line 2293094
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->O:LX/0zG;

    if-eqz v0, :cond_1

    .line 2293095
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->O:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/63T;

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aE:LX/63T;

    .line 2293096
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aE:LX/63T;

    if-eqz v0, :cond_1

    .line 2293097
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aE:LX/63T;

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->m:LX/G4l;

    iget-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    iget-object v4, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    .line 2293098
    new-instance v7, LX/G4k;

    invoke-direct {v7, v2, v3, v4}, LX/G4k;-><init>(LX/G4l;LX/5SB;LX/BQ1;)V

    move-object v2, v7

    .line 2293099
    invoke-interface {v0, v2}, LX/63T;->a(Landroid/view/View$OnClickListener;)V

    .line 2293100
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aB:LX/Fy4;

    invoke-virtual {v0, v5}, LX/Fy4;->a(Z)V

    .line 2293101
    invoke-direct {p0}, Lcom/facebook/timeline/TimelineFragment;->ah()V

    .line 2293102
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aE:LX/63T;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/facebook/timeline/TimelineFragment;->at:Z

    if-nez v0, :cond_3

    .line 2293103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/timeline/TimelineFragment;->aP:Z

    .line 2293104
    const/4 v12, 0x1

    .line 2293105
    const-class v7, LX/63U;

    invoke-virtual {p0, v7}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LX/63U;

    .line 2293106
    iget-object v7, p0, Lcom/facebook/timeline/TimelineFragment;->aI:LX/63Q;

    if-nez v7, :cond_2

    if-eqz v11, :cond_2

    .line 2293107
    new-instance v7, LX/63Q;

    invoke-direct {v7}, LX/63Q;-><init>()V

    iput-object v7, p0, Lcom/facebook/timeline/TimelineFragment;->aI:LX/63Q;

    .line 2293108
    iget-object v7, p0, Lcom/facebook/timeline/TimelineFragment;->aI:LX/63Q;

    iget-object v9, p0, Lcom/facebook/timeline/TimelineFragment;->aE:LX/63T;

    iget-object v10, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    iget-boolean v8, p0, Lcom/facebook/timeline/TimelineFragment;->bF:Z

    if-nez v8, :cond_4

    move v13, v12

    :goto_0
    move-object v8, p0

    invoke-virtual/range {v7 .. v13}, LX/63Q;->a(LX/63S;LX/63T;LX/0g9;LX/63U;ZZ)V

    .line 2293109
    :cond_2
    invoke-static {p0}, Lcom/facebook/timeline/TimelineFragment;->ad(Lcom/facebook/timeline/TimelineFragment;)V

    .line 2293110
    :cond_3
    invoke-static {p0}, Lcom/facebook/timeline/TimelineFragment;->ad(Lcom/facebook/timeline/TimelineFragment;)V

    .line 2293111
    const/16 v0, 0x2b

    const v2, 0x182ab65e

    invoke-static {v6, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2293112
    :cond_4
    const/4 v13, 0x0

    goto :goto_0
.end method

.method public final onStop()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6b4ffbba

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2293024
    invoke-super {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->onStop()V

    .line 2293025
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    .line 2293026
    invoke-virtual {v0}, LX/FrV;->a()LX/Fra;

    move-result-object v2

    .line 2293027
    invoke-static {v2}, LX/Fra;->e(LX/Fra;)LX/Fs2;

    move-result-object v4

    .line 2293028
    iget-object v5, v4, LX/Fs2;->i:LX/Fs1;

    move-object v4, v5

    .line 2293029
    sget-object v5, LX/Fs1;->VISIBLE:LX/Fs1;

    if-ne v4, v5, :cond_0

    invoke-static {v2}, LX/Fra;->e(LX/Fra;)LX/Fs2;

    move-result-object v4

    invoke-virtual {v4}, LX/Fs2;->b()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2293030
    invoke-static {v2}, LX/Fra;->e(LX/Fra;)LX/Fs2;

    move-result-object v4

    sget-object v5, LX/Fs1;->PAUSED:LX/Fs1;

    .line 2293031
    iput-object v5, v4, LX/Fs2;->i:LX/Fs1;

    .line 2293032
    :cond_0
    invoke-virtual {v0}, LX/FrV;->b()LX/Frd;

    move-result-object v2

    invoke-virtual {v2}, LX/Frd;->c()V

    .line 2293033
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->K:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fsw;

    .line 2293034
    iget-object v2, v0, LX/Fsw;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 2293035
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2293036
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Vd;

    invoke-virtual {v2}, LX/0Vd;->dispose()V

    .line 2293037
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 2293038
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    if-eqz v0, :cond_2

    .line 2293039
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ar:LX/0g8;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, LX/0g8;->a(LX/0fx;)V

    .line 2293040
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bg:LX/BQB;

    invoke-virtual {v0}, LX/BQB;->a()V

    .line 2293041
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aB:LX/Fy4;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/Fy4;->a(Z)V

    .line 2293042
    const/16 v0, 0x2b

    const v2, -0x784fc5a2

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final p()LX/FwE;
    .locals 6

    .prologue
    .line 2293071
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aJ:LX/FwE;

    if-nez v0, :cond_0

    .line 2293072
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->x:LX/FwH;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->I:LX/BQ9;

    iget-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    iget-object v4, p0, Lcom/facebook/timeline/TimelineFragment;->aC:LX/0QR;

    iget-object v5, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual/range {v0 .. v5}, LX/FwH;->a(Landroid/app/Activity;LX/BQ9;LX/5SB;LX/0QR;LX/BQ1;)LX/FwG;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aJ:LX/FwE;

    .line 2293073
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aJ:LX/FwE;

    return-object v0
.end method

.method public final q()LX/FqT;
    .locals 8

    .prologue
    .line 2293067
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aL:LX/Fqc;

    if-nez v0, :cond_0

    .line 2293068
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v6, v0

    .line 2293069
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->q:LX/Fqd;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->I:LX/BQ9;

    iget-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    iget-object v4, p0, Lcom/facebook/timeline/TimelineFragment;->aB:LX/Fy4;

    iget-object v5, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    const-string v7, "group_commerce_sell_options_id"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, LX/Fqd;->a(Landroid/content/Context;LX/BQ9;LX/5SB;LX/Fy4;LX/BQ1;Ljava/lang/String;)LX/Fqc;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aL:LX/Fqc;

    .line 2293070
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aL:LX/Fqc;

    return-object v0
.end method

.method public final r()LX/FrC;
    .locals 7

    .prologue
    .line 2293064
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aM:LX/FrF;

    if-nez v0, :cond_0

    .line 2293065
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->r:LX/FrG;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->I:LX/BQ9;

    iget-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    iget-object v4, p0, Lcom/facebook/timeline/TimelineFragment;->bd:LX/FrH;

    iget-object v5, p0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    iget-object v6, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual/range {v0 .. v6}, LX/FrG;->a(Landroid/content/Context;LX/BQ9;LX/5SB;LX/FrH;LX/FrV;LX/BQ1;)LX/FrF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aM:LX/FrF;

    .line 2293066
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aM:LX/FrF;

    return-object v0
.end method

.method public final s()LX/G0u;
    .locals 15

    .prologue
    .line 2293058
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aN:LX/G0u;

    if-nez v0, :cond_0

    .line 2293059
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->s:LX/G0y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/TimelineFragment;->I:LX/BQ9;

    iget-object v3, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    iget-object v4, p0, Lcom/facebook/timeline/TimelineFragment;->bb:LX/G4m;

    iget-object v5, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    .line 2293060
    new-instance v6, LX/G0x;

    const/16 v7, 0x2eb

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    const/16 v7, 0x97

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v14

    check-cast v14, Ljava/lang/Boolean;

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    move-object v11, v5

    invoke-direct/range {v6 .. v14}, LX/G0x;-><init>(Landroid/content/Context;LX/BQ9;LX/5SB;LX/G4m;LX/BQ1;LX/0Or;LX/0Or;Ljava/lang/Boolean;)V

    .line 2293061
    move-object v0, v6

    .line 2293062
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aN:LX/G0u;

    .line 2293063
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aN:LX/G0u;

    return-object v0
.end method

.method public final t()LX/Frd;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2293057
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ay:LX/FrV;

    invoke-virtual {v0}, LX/FrV;->b()LX/Frd;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final u()LX/Fw9;
    .locals 2

    .prologue
    .line 2293051
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aK:LX/Fw9;

    if-nez v0, :cond_0

    .line 2293052
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aC:LX/0QR;

    .line 2293053
    new-instance v1, LX/FwA;

    invoke-direct {v1, v0}, LX/FwA;-><init>(LX/0QR;)V

    .line 2293054
    move-object v0, v1

    .line 2293055
    iput-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aK:LX/Fw9;

    .line 2293056
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aK:LX/Fw9;

    return-object v0
.end method

.method public final synthetic v()LX/1OP;
    .locals 1

    .prologue
    .line 2293049
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    move-object v0, v0

    .line 2293050
    return-object v0
.end method

.method public final w()LX/1Kt;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2293046
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bj:LX/Ft0;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->bj:LX/Ft0;

    .line 2293047
    iget-object p0, v0, LX/Ft0;->h:LX/0QR;

    invoke-interface {p0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1Kt;

    move-object v0, p0

    .line 2293048
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x()LX/2dj;
    .locals 1

    .prologue
    .line 2293045
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aB:LX/Fy4;

    invoke-virtual {v0}, LX/Fy4;->a()LX/2dj;

    move-result-object v0

    return-object v0
.end method

.method public final y()LX/5SB;
    .locals 1

    .prologue
    .line 2293044
    iget-object v0, p0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    return-object v0
.end method

.method public final z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2293043
    const-string v0, "timeline_ufi"

    return-object v0
.end method
