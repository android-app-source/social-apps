.class public final Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x584d3ec9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2312320
    const-class v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2312319
    const-class v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2312317
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2312318
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2312314
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2312315
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2312316
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2312308
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2312309
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2312310
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2312311
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2312312
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2312313
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2312306
    iget-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;->e:Ljava/util/List;

    .line 2312307
    iget-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2312293
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2312294
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2312295
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2312296
    if-eqz v1, :cond_0

    .line 2312297
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;

    .line 2312298
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;->e:Ljava/util/List;

    .line 2312299
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2312300
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2312303
    new-instance v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;-><init>()V

    .line 2312304
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2312305
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2312302
    const v0, -0x4a8f1399

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2312301
    const v0, -0x407a981d

    return v0
.end method
