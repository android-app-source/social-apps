.class public final Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2312151
    const-class v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel;

    new-instance v1, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2312152
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2312153
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2312138
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2312139
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2312140
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2312141
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2312142
    if-eqz v2, :cond_0

    .line 2312143
    const-string p0, "subtitle"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312144
    invoke-static {v1, v2, p1}, LX/G1q;->a(LX/15i;ILX/0nX;)V

    .line 2312145
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2312146
    if-eqz v2, :cond_1

    .line 2312147
    const-string p0, "title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312148
    invoke-static {v1, v2, p1}, LX/G1r;->a(LX/15i;ILX/0nX;)V

    .line 2312149
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2312150
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2312137
    check-cast p1, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$Serializer;->a(Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
