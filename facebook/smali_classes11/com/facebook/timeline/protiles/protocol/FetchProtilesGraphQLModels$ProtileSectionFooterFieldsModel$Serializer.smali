.class public final Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2312070
    const-class v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel;

    new-instance v1, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2312071
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2312072
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2312073
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2312074
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2312075
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2312076
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2312077
    if-eqz v2, :cond_0

    .line 2312078
    const-string p0, "footer"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312079
    invoke-static {v1, v2, p1}, LX/G1p;->a(LX/15i;ILX/0nX;)V

    .line 2312080
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2312081
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2312082
    check-cast p1, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$Serializer;->a(Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
