.class public final Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/FU5;
.implements LX/FU6;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x582772f1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewsConnectionFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$SubtitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2312000
    const-class v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2311999
    const-class v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2311997
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2311998
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2311994
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2311995
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2311996
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 2311978
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2311979
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->a()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2311980
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->j()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2311981
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->k()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 2311982
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->l()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewsConnectionFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2311983
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->m()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$SubtitleModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2311984
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->n()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2311985
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2311986
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 2311987
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2311988
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2311989
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2311990
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2311991
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2311992
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2311993
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2311950
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2311951
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->a()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2311952
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->a()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    .line 2311953
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->a()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2311954
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    .line 2311955
    iput-object v0, v1, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->e:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    .line 2311956
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->j()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2311957
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->j()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;

    .line 2311958
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->j()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2311959
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    .line 2311960
    iput-object v0, v1, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->f:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;

    .line 2311961
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->l()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewsConnectionFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2311962
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->l()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewsConnectionFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewsConnectionFieldsModel;

    .line 2311963
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->l()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewsConnectionFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2311964
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    .line 2311965
    iput-object v0, v1, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->h:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewsConnectionFieldsModel;

    .line 2311966
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->m()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$SubtitleModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2311967
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->m()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$SubtitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$SubtitleModel;

    .line 2311968
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->m()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$SubtitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2311969
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    .line 2311970
    iput-object v0, v1, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->i:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$SubtitleModel;

    .line 2311971
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->n()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2311972
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->n()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;

    .line 2311973
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->n()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 2311974
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    .line 2311975
    iput-object v0, v1, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->j:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;

    .line 2311976
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2311977
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2311948
    iget-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->e:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->e:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    .line 2311949
    iget-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->e:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2312001
    new-instance v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;-><init>()V

    .line 2312002
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2312003
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2311937
    const v0, -0xd6e4508

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2311936
    const v0, -0x503c94f2

    return v0
.end method

.method public final j()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2311946
    iget-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->f:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;

    iput-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->f:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;

    .line 2311947
    iget-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->f:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2311938
    iget-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->g:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    iput-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->g:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    .line 2311939
    iget-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->g:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    return-object v0
.end method

.method public final l()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewsConnectionFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2311940
    iget-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->h:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewsConnectionFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewsConnectionFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewsConnectionFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->h:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewsConnectionFieldsModel;

    .line 2311941
    iget-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->h:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewsConnectionFieldsModel;

    return-object v0
.end method

.method public final m()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$SubtitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2311942
    iget-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->i:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$SubtitleModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$SubtitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$SubtitleModel;

    iput-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->i:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$SubtitleModel;

    .line 2311943
    iget-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->i:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$SubtitleModel;

    return-object v0
.end method

.method public final n()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2311944
    iget-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->j:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;

    iput-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->j:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;

    .line 2311945
    iget-object v0, p0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->j:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;

    return-object v0
.end method
