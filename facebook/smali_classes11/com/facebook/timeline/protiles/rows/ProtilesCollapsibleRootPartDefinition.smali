.class public Lcom/facebook/timeline/protiles/rows/ProtilesCollapsibleRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/timeline/protiles/model/ProtileModel;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;

.field private final b:Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;

.field private final c:LX/G2X;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;LX/G2X;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2313378
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2313379
    iput-object p1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesCollapsibleRootPartDefinition;->a:Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;

    .line 2313380
    iput-object p2, p0, Lcom/facebook/timeline/protiles/rows/ProtilesCollapsibleRootPartDefinition;->b:Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;

    .line 2313381
    iput-object p3, p0, Lcom/facebook/timeline/protiles/rows/ProtilesCollapsibleRootPartDefinition;->c:LX/G2X;

    .line 2313382
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesCollapsibleRootPartDefinition;
    .locals 6

    .prologue
    .line 2313367
    const-class v1, Lcom/facebook/timeline/protiles/rows/ProtilesCollapsibleRootPartDefinition;

    monitor-enter v1

    .line 2313368
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesCollapsibleRootPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2313369
    sput-object v2, Lcom/facebook/timeline/protiles/rows/ProtilesCollapsibleRootPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2313370
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2313371
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2313372
    new-instance p0, Lcom/facebook/timeline/protiles/rows/ProtilesCollapsibleRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;

    invoke-static {v0}, LX/G2X;->a(LX/0QB;)LX/G2X;

    move-result-object v5

    check-cast v5, LX/G2X;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/timeline/protiles/rows/ProtilesCollapsibleRootPartDefinition;-><init>(Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;LX/G2X;)V

    .line 2313373
    move-object v0, p0

    .line 2313374
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2313375
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/protiles/rows/ProtilesCollapsibleRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2313376
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2313377
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/timeline/protiles/model/ProtileModel;LX/G2X;)Z
    .locals 1

    .prologue
    .line 2313383
    if-eqz p0, :cond_0

    invoke-virtual {p1, p0}, LX/G2X;->c(Lcom/facebook/timeline/protiles/model/ProtileModel;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2313363
    check-cast p2, Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2313364
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesCollapsibleRootPartDefinition;->a:Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2313365
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesCollapsibleRootPartDefinition;->b:Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2313366
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2313361
    check-cast p1, Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2313362
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesCollapsibleRootPartDefinition;->c:LX/G2X;

    invoke-static {p1, v0}, Lcom/facebook/timeline/protiles/rows/ProtilesCollapsibleRootPartDefinition;->a(Lcom/facebook/timeline/protiles/model/ProtileModel;LX/G2X;)Z

    move-result v0

    return v0
.end method
