.class public Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/timeline/protiles/model/ProtileModel;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;

.field public final b:Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/feed/rows/sections/TextOrTranslationSelectorPartDefinition;

.field public final d:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;Lcom/facebook/feed/rows/sections/TextOrTranslationSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2313876
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2313877
    iput-object p1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;->a:Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;

    .line 2313878
    iput-object p2, p0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;->b:Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;

    .line 2313879
    iput-object p3, p0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;->c:Lcom/facebook/feed/rows/sections/TextOrTranslationSelectorPartDefinition;

    .line 2313880
    iput-object p4, p0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;->d:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    .line 2313881
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;
    .locals 7

    .prologue
    .line 2313882
    const-class v1, Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;

    monitor-enter v1

    .line 2313883
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2313884
    sput-object v2, Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2313885
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2313886
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2313887
    new-instance p0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/TextOrTranslationSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/TextOrTranslationSelectorPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/TextOrTranslationSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;-><init>(Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;Lcom/facebook/feed/rows/sections/TextOrTranslationSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;)V

    .line 2313888
    move-object v0, p0

    .line 2313889
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2313890
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2313891
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2313892
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2313893
    check-cast p2, Lcom/facebook/timeline/protiles/model/ProtileModel;

    const/4 v2, 0x0

    .line 2313894
    iget-object v0, p2, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    move-object v4, v0

    .line 2313895
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_1

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    .line 2313896
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2313897
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->s()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2313898
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    .line 2313899
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v1, v2

    :goto_1
    if-ge v1, v8, :cond_0

    invoke-virtual {v7, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2313900
    invoke-virtual {v6, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2313901
    iget-object v9, p0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;->a:Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;

    new-instance v10, LX/33s;

    sget-object p3, LX/04D;->PROFILE_VIDEO_CARD:LX/04D;

    invoke-direct {v10, v0, p3}, LX/33s;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/04D;)V

    invoke-static {p1, v9, v10}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v9

    iget-object v10, p0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;->b:Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;

    invoke-virtual {v9, v10, v0}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2313902
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;->c:Lcom/facebook/feed/rows/sections/TextOrTranslationSelectorPartDefinition;

    invoke-virtual {p1, v0, v6}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2313903
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;->d:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-virtual {p1, v0, v6}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2313904
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2313905
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2313906
    :cond_1
    const/4 v0, 0x0

    move-object v0, v0

    .line 2313907
    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2313908
    check-cast p1, Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2313909
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne v0, v1, :cond_0

    .line 2313910
    iget-object v0, p1, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    move-object v0, v0

    .line 2313911
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
