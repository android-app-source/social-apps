.class public Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/G2F;",
        "LX/G2G;",
        "TE;",
        "Lcom/facebook/widget/mosaic/MosaicGridLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field public final b:LX/G2N;

.field public final c:LX/1K9;

.field private final d:LX/1Ad;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/G2T;

.field public final g:LX/EQL;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2313803
    const-class v0, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1K9;LX/G2N;LX/1Ad;LX/0Or;LX/G2T;LX/EQL;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/common/IsDialtonePhotoFeatureEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1K9;",
            "LX/G2N;",
            "LX/1Ad;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/G2T;",
            "LX/EQL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2313804
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2313805
    iput-object p1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->c:LX/1K9;

    .line 2313806
    iput-object p2, p0, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->b:LX/G2N;

    .line 2313807
    iput-object p3, p0, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->d:LX/1Ad;

    .line 2313808
    iput-object p4, p0, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->e:LX/0Or;

    .line 2313809
    iput-object p5, p0, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->f:LX/G2T;

    .line 2313810
    iput-object p6, p0, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->g:LX/EQL;

    .line 2313811
    return-void
.end method

.method public static a(LX/1Fb;)LX/1bf;
    .locals 2
    .param p0    # LX/1Fb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2313812
    if-nez p0, :cond_0

    .line 2313813
    const/4 v0, 0x0

    .line 2313814
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    sget-object v1, LX/1bY;->DISK_CACHE:LX/1bY;

    .line 2313815
    iput-object v1, v0, LX/1bX;->b:LX/1bY;

    .line 2313816
    move-object v0, v0

    .line 2313817
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;
    .locals 10

    .prologue
    .line 2313792
    const-class v1, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;

    monitor-enter v1

    .line 2313793
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2313794
    sput-object v2, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2313795
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2313796
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2313797
    new-instance v3, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v4

    check-cast v4, LX/1K9;

    invoke-static {v0}, LX/G2N;->a(LX/0QB;)LX/G2N;

    move-result-object v5

    check-cast v5, LX/G2N;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v6

    check-cast v6, LX/1Ad;

    const/16 v7, 0x1488

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/G2T;->a(LX/0QB;)LX/G2T;

    move-result-object v8

    check-cast v8, LX/G2T;

    invoke-static {v0}, LX/EQL;->a(LX/0QB;)LX/EQL;

    move-result-object v9

    check-cast v9, LX/EQL;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;-><init>(LX/1K9;LX/G2N;LX/1Ad;LX/0Or;LX/G2T;LX/EQL;)V

    .line 2313798
    move-object v0, v3

    .line 2313799
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2313800
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2313801
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2313802
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2313725
    check-cast p2, LX/G2F;

    check-cast p3, LX/1Pp;

    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 2313726
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->f:LX/G2T;

    iget-object v2, p2, LX/G2F;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    invoke-virtual {v0, v2}, LX/G2T;->c(Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;)LX/1Fb;

    move-result-object v3

    .line 2313727
    iget-object v0, p2, LX/G2F;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    .line 2313728
    new-instance v2, Ljava/util/ArrayList;

    const/4 v4, 0x3

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 2313729
    if-nez v3, :cond_7

    .line 2313730
    :cond_0
    :goto_0
    move-object v4, v2

    .line 2313731
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [LX/1bf;

    .line 2313732
    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 2313733
    iget-object v2, p2, LX/G2F;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    const/4 v5, 0x0

    .line 2313734
    if-nez v3, :cond_9

    .line 2313735
    :cond_1
    :goto_1
    move-object v5, v5

    .line 2313736
    iget-object v2, p0, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->d:LX/1Ad;

    sget-object v6, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v6}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    move-object v0, v1

    :cond_2
    invoke-virtual {v2, v0}, LX/1Ae;->a([Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v5}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    .line 2313737
    if-eqz v5, :cond_3

    move-object v0, p3

    .line 2313738
    check-cast v0, LX/1Pt;

    sget-object v6, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v0, v5, v6}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2313739
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2313740
    iget-object v0, p2, LX/G2F;->b:Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2313741
    iget-object v5, v0, Lcom/facebook/timeline/protiles/model/ProtileModel;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2313742
    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1bf;

    sget-object v6, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {p3, v2, v5, v0, v6}, LX/1Pp;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2313743
    check-cast p3, LX/1Pt;

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1bf;

    sget-object v4, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {p3, v0, v4}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2313744
    :cond_4
    iget-object v0, p2, LX/G2F;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    const/high16 v9, 0x3f000000    # 0.5f

    .line 2313745
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2313746
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->c()LX/1f8;

    move-result-object v8

    if-nez v8, :cond_b

    .line 2313747
    :cond_5
    new-instance v8, Landroid/graphics/PointF;

    invoke-direct {v8, v9, v9}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2313748
    :goto_2
    move-object v4, v8

    .line 2313749
    iget-object v0, p2, LX/G2F;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    iget-object v5, p2, LX/G2F;->b:Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2313750
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v6

    if-nez v6, :cond_c

    .line 2313751
    const/4 v6, 0x0

    .line 2313752
    :goto_3
    move-object v5, v6

    .line 2313753
    iget-object v0, p2, LX/G2F;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v0

    .line 2313754
    if-eqz v0, :cond_d

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v3

    .line 2313755
    :goto_4
    iget-object v6, p0, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->g:LX/EQL;

    const v7, 0x7f0a069b

    invoke-virtual {v6, v3, v7}, LX/EQL;->a(Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    move-object v3, v3

    .line 2313756
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 2313757
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v1

    move-object v6, v1

    .line 2313758
    :goto_5
    new-instance v0, LX/G2G;

    iget-object v1, p2, LX/G2F;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    invoke-static {v1}, LX/G2T;->d(Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;)LX/G6g;

    move-result-object v1

    invoke-direct/range {v0 .. v6}, LX/G2G;-><init>(LX/G6g;LX/1aZ;Landroid/graphics/drawable/Drawable;Landroid/graphics/PointF;Landroid/view/View$OnClickListener;Ljava/lang/String;)V

    return-object v0

    :cond_6
    move-object v6, v1

    goto :goto_5

    .line 2313759
    :cond_7
    invoke-interface {v3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2313760
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->j()LX/1Fb;

    move-result-object v4

    .line 2313761
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->aj_()LX/1Fb;

    move-result-object v5

    .line 2313762
    if-eq v3, v5, :cond_8

    if-eq v3, v4, :cond_8

    .line 2313763
    invoke-static {v4}, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->a(LX/1Fb;)LX/1bf;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2313764
    :cond_8
    if-eq v3, v5, :cond_0

    .line 2313765
    invoke-static {v5}, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->a(LX/1Fb;)LX/1bf;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2313766
    :cond_9
    invoke-virtual {v2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->ai_()LX/1Fb;

    move-result-object v6

    .line 2313767
    invoke-virtual {v2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->j()LX/1Fb;

    move-result-object v8

    .line 2313768
    invoke-virtual {v2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->aj_()LX/1Fb;

    move-result-object p1

    .line 2313769
    if-ne v3, p1, :cond_a

    .line 2313770
    invoke-static {v8}, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->a(LX/1Fb;)LX/1bf;

    move-result-object v5

    goto/16 :goto_1

    .line 2313771
    :cond_a
    if-ne v3, v8, :cond_1

    .line 2313772
    invoke-static {v6}, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->a(LX/1Fb;)LX/1bf;

    move-result-object v5

    goto/16 :goto_1

    :cond_b
    new-instance v8, Landroid/graphics/PointF;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->c()LX/1f8;

    move-result-object v9

    invoke-interface {v9}, LX/1f8;->a()D

    move-result-wide v10

    double-to-float v9, v10

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->c()LX/1f8;

    move-result-object v10

    invoke-interface {v10}, LX/1f8;->b()D

    move-result-wide v10

    double-to-float v10, v10

    invoke-direct {v8, v9, v10}, Landroid/graphics/PointF;-><init>(FF)V

    goto/16 :goto_2

    :cond_c
    new-instance v6, LX/G2E;

    invoke-direct {v6, p0, v0, v3, v5}, LX/G2E;-><init>(Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;LX/1Fb;Lcom/facebook/timeline/protiles/model/ProtileModel;)V

    goto/16 :goto_3

    .line 2313773
    :cond_d
    const/4 v3, 0x0

    goto/16 :goto_4
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 8

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x19022dbc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2313778
    check-cast p1, LX/G2F;

    check-cast p2, LX/G2G;

    check-cast p4, Lcom/facebook/widget/mosaic/MosaicGridLayout;

    const/4 p3, 0x1

    const/4 v7, 0x0

    .line 2313779
    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2313780
    :cond_0
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x66ca6a2b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2313781
    :cond_1
    iget v1, p1, LX/G2F;->c:I

    invoke-virtual {p4}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2313782
    iget v1, p1, LX/G2F;->c:I

    invoke-virtual {p4, v1}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2313783
    iget-object v2, p2, LX/G2G;->a:LX/G6g;

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2313784
    iget-object v2, p2, LX/G2G;->b:LX/1aZ;

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2313785
    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v2

    check-cast v2, LX/1af;

    iget-object v4, p2, LX/G2G;->d:Landroid/graphics/PointF;

    invoke-virtual {v2, v4}, LX/1af;->a(Landroid/graphics/PointF;)V

    .line 2313786
    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v2

    check-cast v2, LX/1af;

    iget-object v4, p2, LX/G2G;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v4}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2313787
    iget-object v2, p2, LX/G2G;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2313788
    iget-object v2, p2, LX/G2G;->f:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 2313789
    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f081ff4

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p1, LX/G2F;->c:I

    add-int/lit8 v6, v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p4}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->getChildCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, p3

    const/4 v6, 0x2

    iget-object v7, p2, LX/G2G;->f:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2313790
    :goto_1
    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2313791
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f081ff3

    new-array v5, p3, [Ljava/lang/Object;

    iget v6, p1, LX/G2F;->c:I

    add-int/lit8 v6, v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2313774
    check-cast p1, LX/G2F;

    check-cast p4, Lcom/facebook/widget/mosaic/MosaicGridLayout;

    .line 2313775
    iget v0, p1, LX/G2F;->c:I

    invoke-virtual {p4}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 2313776
    :goto_0
    return-void

    .line 2313777
    :cond_0
    iget v0, p1, LX/G2F;->c:I

    invoke-virtual {p4, v0}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
