.class public Lcom/facebook/timeline/protiles/rows/ProtilesRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/timeline/protiles/model/ProtileModel;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;

.field private final b:Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/protiles/rows/ProtilesCollapsibleRootPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/G2X;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;LX/0Ot;LX/G2X;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;",
            "Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/protiles/rows/ProtilesCollapsibleRootPartDefinition;",
            ">;",
            "LX/G2X;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2313835
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2313836
    iput-object p1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesRootPartDefinition;->a:Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;

    .line 2313837
    iput-object p2, p0, Lcom/facebook/timeline/protiles/rows/ProtilesRootPartDefinition;->b:Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;

    .line 2313838
    iput-object p3, p0, Lcom/facebook/timeline/protiles/rows/ProtilesRootPartDefinition;->c:LX/0Ot;

    .line 2313839
    iput-object p4, p0, Lcom/facebook/timeline/protiles/rows/ProtilesRootPartDefinition;->d:LX/G2X;

    .line 2313840
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesRootPartDefinition;
    .locals 7

    .prologue
    .line 2313841
    const-class v1, Lcom/facebook/timeline/protiles/rows/ProtilesRootPartDefinition;

    monitor-enter v1

    .line 2313842
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesRootPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2313843
    sput-object v2, Lcom/facebook/timeline/protiles/rows/ProtilesRootPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2313844
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2313845
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2313846
    new-instance v6, Lcom/facebook/timeline/protiles/rows/ProtilesRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;

    const/16 v5, 0x36be

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/G2X;->a(LX/0QB;)LX/G2X;

    move-result-object v5

    check-cast v5, LX/G2X;

    invoke-direct {v6, v3, v4, p0, v5}, Lcom/facebook/timeline/protiles/rows/ProtilesRootPartDefinition;-><init>(Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;LX/0Ot;LX/G2X;)V

    .line 2313847
    move-object v0, v6

    .line 2313848
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2313849
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/protiles/rows/ProtilesRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2313850
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2313851
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2313852
    check-cast p2, Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2313853
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesRootPartDefinition;->a:Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesRootPartDefinition;->b:Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;

    new-instance v2, LX/G2J;

    invoke-direct {v2, p2}, LX/G2J;-><init>(Lcom/facebook/timeline/protiles/model/ProtileModel;)V

    invoke-virtual {v0, v1, v2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2313854
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesRootPartDefinition;->d:LX/G2X;

    invoke-static {p2, v0}, Lcom/facebook/timeline/protiles/rows/ProtilesCollapsibleRootPartDefinition;->a(Lcom/facebook/timeline/protiles/model/ProtileModel;LX/G2X;)Z

    move-result v0

    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesRootPartDefinition;->c:LX/0Ot;

    invoke-virtual {p1, v0, v1, p2}, LX/1RF;->a(ZLX/0Ot;Ljava/lang/Object;)Z

    .line 2313855
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2313856
    check-cast p1, Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2313857
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
