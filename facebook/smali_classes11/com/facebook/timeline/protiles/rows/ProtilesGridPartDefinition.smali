.class public Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/timeline/protiles/model/ProtileModel;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;

.field private final b:LX/G2X;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;LX/G2X;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2313387
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2313388
    iput-object p1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;->a:Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;

    .line 2313389
    iput-object p2, p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;->b:LX/G2X;

    .line 2313390
    return-void
.end method

.method private a(Lcom/facebook/timeline/protiles/model/ProtileModel;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/protiles/model/ProtileModel;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x3

    .line 2313391
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;->b:LX/G2X;

    const/4 v1, 0x0

    .line 2313392
    iget-boolean v2, v0, LX/G2X;->c:Z

    if-eqz v2, :cond_0

    .line 2313393
    iget-object v2, v0, LX/G2X;->a:LX/0ad;

    sget-short p0, LX/0wf;->at:S

    invoke-interface {v2, p0, v1}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 2313394
    :cond_0
    move v0, v1

    .line 2313395
    if-eqz v0, :cond_1

    .line 2313396
    iget-object v0, p1, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    move-object v0, v0

    .line 2313397
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-gt v0, v3, :cond_2

    .line 2313398
    :cond_1
    iget-object v0, p1, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    move-object v0, v0

    .line 2313399
    :goto_0
    return-object v0

    .line 2313400
    :cond_2
    iget-object v0, p1, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    move-object v0, v0

    .line 2313401
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 2313402
    iget-object v1, p1, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    move-object v1, v1

    .line 2313403
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 2313404
    iget-object v2, p1, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    move-object v2, v2

    .line 2313405
    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;
    .locals 5

    .prologue
    .line 2313406
    const-class v1, Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;

    monitor-enter v1

    .line 2313407
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2313408
    sput-object v2, Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2313409
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2313410
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2313411
    new-instance p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;

    invoke-static {v0}, LX/G2X;->a(LX/0QB;)LX/G2X;

    move-result-object v4

    check-cast v4, LX/G2X;

    invoke-direct {p0, v3, v4}, Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;-><init>(Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;LX/G2X;)V

    .line 2313412
    move-object v0, p0

    .line 2313413
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2313414
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2313415
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2313416
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2313417
    check-cast p2, Lcom/facebook/timeline/protiles/model/ProtileModel;

    check-cast p3, LX/1Pp;

    .line 2313418
    iget-object v0, p2, Lcom/facebook/timeline/protiles/model/ProtileModel;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2313419
    invoke-interface {p3, v0}, LX/1Pp;->a(Ljava/lang/String;)V

    .line 2313420
    invoke-direct {p0, p2}, Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;->a(Lcom/facebook/timeline/protiles/model/ProtileModel;)LX/0Px;

    move-result-object v0

    .line 2313421
    const/4 v1, 0x3

    .line 2313422
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2313423
    if-lez v1, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2313424
    instance-of v2, v0, Ljava/util/RandomAccess;

    if-eqz v2, :cond_2

    new-instance v2, LX/4zF;

    invoke-direct {v2, v0, v1}, LX/4zF;-><init>(Ljava/util/List;I)V

    :goto_1
    move-object v2, v2

    .line 2313425
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2313426
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 2313427
    if-nez v1, :cond_3

    .line 2313428
    sget-object v3, LX/G1J;->TOP_ROW:LX/G1J;

    .line 2313429
    :goto_3
    move-object v3, v3

    .line 2313430
    iget-object v4, p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;->a:Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;

    new-instance v5, LX/G1K;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {v5, p2, v3, v0}, LX/G1K;-><init>(Lcom/facebook/timeline/protiles/model/ProtileModel;LX/G1J;Ljava/util/List;)V

    invoke-virtual {p1, v4, v5}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2313431
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2313432
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 2313433
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 2313434
    :cond_2
    new-instance v2, LX/4zE;

    invoke-direct {v2, v0, v1}, LX/4zE;-><init>(Ljava/util/List;I)V

    goto :goto_1

    .line 2313435
    :cond_3
    add-int/lit8 v3, v0, -0x1

    if-ne v1, v3, :cond_4

    .line 2313436
    sget-object v3, LX/G1J;->BOTTOM_ROW:LX/G1J;

    goto :goto_3

    .line 2313437
    :cond_4
    sget-object v3, LX/G1J;->MIDDLE_ROW:LX/G1J;

    goto :goto_3
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2313438
    check-cast p1, Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2313439
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
