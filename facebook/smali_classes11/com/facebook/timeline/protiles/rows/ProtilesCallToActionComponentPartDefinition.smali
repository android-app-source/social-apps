.class public Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/timeline/protiles/model/ProtileModel;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/1V0;

.field private final e:LX/G20;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/G20;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2313338
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2313339
    iput-object p2, p0, Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;->d:LX/1V0;

    .line 2313340
    iput-object p3, p0, Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;->e:LX/G20;

    .line 2313341
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/timeline/protiles/model/ProtileModel;LX/1Pf;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/timeline/protiles/model/ProtileModel;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2313321
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;->e:LX/G20;

    const/4 v1, 0x0

    .line 2313322
    new-instance v2, LX/G1z;

    invoke-direct {v2, v0}, LX/G1z;-><init>(LX/G20;)V

    .line 2313323
    iget-object v3, v0, LX/G20;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/G1y;

    .line 2313324
    if-nez v3, :cond_0

    .line 2313325
    new-instance v3, LX/G1y;

    invoke-direct {v3, v0}, LX/G1y;-><init>(LX/G20;)V

    .line 2313326
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/G1y;->a$redex0(LX/G1y;LX/1De;IILX/G1z;)V

    .line 2313327
    move-object v2, v3

    .line 2313328
    move-object v1, v2

    .line 2313329
    move-object v0, v1

    .line 2313330
    iget-object v1, v0, LX/G1y;->a:LX/G1z;

    iput-object p2, v1, LX/G1z;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2313331
    iget-object v1, v0, LX/G1y;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2313332
    move-object v0, v0

    .line 2313333
    iget-object v1, v0, LX/G1y;->a:LX/G1z;

    iput-object p3, v1, LX/G1z;->b:LX/1Pp;

    .line 2313334
    iget-object v1, v0, LX/G1y;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2313335
    move-object v0, v0

    .line 2313336
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2313337
    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;->d:LX/1V0;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->d:LX/1Ua;

    invoke-direct {v2, v3}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;
    .locals 6

    .prologue
    .line 2313306
    const-class v1, Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;

    monitor-enter v1

    .line 2313307
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2313308
    sput-object v2, Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2313309
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2313310
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2313311
    new-instance p0, Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/G20;->a(LX/0QB;)LX/G20;

    move-result-object v5

    check-cast v5, LX/G20;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/G20;)V

    .line 2313312
    move-object v0, p0

    .line 2313313
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2313314
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2313315
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2313316
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2313320
    check-cast p2, Lcom/facebook/timeline/protiles/model/ProtileModel;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/timeline/protiles/model/ProtileModel;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2313319
    check-cast p2, Lcom/facebook/timeline/protiles/model/ProtileModel;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/timeline/protiles/model/ProtileModel;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2313317
    check-cast p1, Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2313318
    invoke-static {p1}, LX/G22;->a(Lcom/facebook/timeline/protiles/model/ProtileModel;)Z

    move-result v0

    return v0
.end method
