.class public Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoFooterPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/G24;",
        "LX/G25;",
        "LX/1PW;",
        "Lcom/facebook/widget/CustomLinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/G2O;


# direct methods
.method public constructor <init>(LX/G2O;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2313488
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2313489
    iput-object p1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoFooterPartDefinition;->a:LX/G2O;

    .line 2313490
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoFooterPartDefinition;
    .locals 4

    .prologue
    .line 2313449
    const-class v1, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoFooterPartDefinition;

    monitor-enter v1

    .line 2313450
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoFooterPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2313451
    sput-object v2, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoFooterPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2313452
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2313453
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2313454
    new-instance p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoFooterPartDefinition;

    invoke-static {v0}, LX/G2O;->a(LX/0QB;)LX/G2O;

    move-result-object v3

    check-cast v3, LX/G2O;

    invoke-direct {p0, v3}, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoFooterPartDefinition;-><init>(LX/G2O;)V

    .line 2313455
    move-object v0, p0

    .line 2313456
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2313457
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2313458
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2313459
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2313471
    check-cast p2, LX/G24;

    .line 2313472
    iget-object v0, p2, LX/G24;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->d()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p2, LX/G24;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->d()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2313473
    :goto_0
    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoFooterPartDefinition;->a:LX/G2O;

    iget-object v2, p2, LX/G24;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    .line 2313474
    invoke-static {v2}, LX/G2O;->d(Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2313475
    iget-object v3, v1, LX/G2O;->a:Landroid/content/res/Resources;

    const v4, 0x7f0f00f4

    invoke-virtual {v2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->q()I

    move-result v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 p1, 0x0

    invoke-virtual {v2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object p3

    invoke-virtual {p3}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->q()I

    move-result p3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, v6, p1

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2313476
    iget-object v4, v1, LX/G2O;->b:LX/0W9;

    invoke-virtual {v4}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 2313477
    :goto_1
    move-object v1, v3

    .line 2313478
    iget-object v2, p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoFooterPartDefinition;->a:LX/G2O;

    iget-object v3, p2, LX/G24;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    const/4 p0, 0x0

    .line 2313479
    invoke-static {v3}, LX/G2O;->d(Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2313480
    const v4, 0x7f021537

    .line 2313481
    :goto_2
    move v4, v4

    .line 2313482
    if-lez v4, :cond_3

    .line 2313483
    iget-object v5, v2, LX/G2O;->a:Landroid/content/res/Resources;

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 2313484
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    invoke-virtual {v4, p0, p0, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2313485
    :goto_3
    move-object v2, v4

    .line 2313486
    new-instance v3, LX/G25;

    invoke-direct {v3, v0, v1, v2}, LX/G25;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    return-object v3

    .line 2313487
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->c()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$SubtitleModel;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->c()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$SubtitleModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$SubtitleModel;->a()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x78fa9622

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2313460
    check-cast p1, LX/G24;

    check-cast p2, LX/G25;

    check-cast p4, Lcom/facebook/widget/CustomLinearLayout;

    .line 2313461
    iget v1, p1, LX/G24;->b:I

    invoke-virtual {p4, v1}, Lcom/facebook/widget/CustomLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/G2Z;

    .line 2313462
    iget-object v2, p2, LX/G25;->a:Ljava/lang/String;

    .line 2313463
    iget-object p1, v1, LX/G2Z;->a:Landroid/widget/TextView;

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2313464
    iget-object v2, p2, LX/G25;->b:Ljava/lang/String;

    .line 2313465
    iget-object p4, v1, LX/G2Z;->b:Landroid/widget/TextView;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x4

    :goto_0
    invoke-virtual {p4, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2313466
    iget-object p1, v1, LX/G2Z;->b:Landroid/widget/TextView;

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2313467
    iget-object v2, p2, LX/G25;->c:Landroid/graphics/drawable/Drawable;

    const/4 p2, 0x0

    .line 2313468
    iget-object p1, v1, LX/G2Z;->b:Landroid/widget/TextView;

    invoke-static {p1, v2, p2, p2, p2}, LX/4lM;->b(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2313469
    const/16 v1, 0x1f

    const v2, -0x51e7a2d1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2313470
    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method
