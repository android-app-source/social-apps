.class public Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/G28;",
        "LX/G29;",
        "TE;",
        "Lcom/facebook/widget/CustomLinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field public final b:LX/1K9;

.field public final c:LX/G2N;

.field private final d:LX/G2S;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/EQL;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/1K9;LX/G2N;LX/G2S;LX/0Or;LX/EQL;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/1K9;",
            "LX/G2N;",
            "LX/G2S;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/EQL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2313591
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2313592
    iput-object p1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->a:Landroid/content/res/Resources;

    .line 2313593
    iput-object p2, p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->b:LX/1K9;

    .line 2313594
    iput-object p3, p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->c:LX/G2N;

    .line 2313595
    iput-object p4, p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->d:LX/G2S;

    .line 2313596
    iput-object p5, p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->e:LX/0Or;

    .line 2313597
    iput-object p6, p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->f:LX/EQL;

    .line 2313598
    return-void
.end method

.method private static a(Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;LX/1bf;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)LX/1aZ;
    .locals 1

    .prologue
    .line 2313599
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->p()LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2313600
    return-object v0
.end method

.method private static a(Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;I)LX/1bf;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2313624
    const/4 v0, 0x0

    .line 2313625
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2313626
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2313627
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 2313628
    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    sget-object v2, LX/1bY;->DISK_CACHE:LX/1bY;

    .line 2313629
    iput-object v2, v0, LX/1bX;->b:LX/1bY;

    .line 2313630
    move-object v0, v0

    .line 2313631
    invoke-static {v1}, LX/1ny;->a(Ljava/lang/String;)LX/1ny;

    move-result-object v1

    .line 2313632
    iput-object v1, v0, LX/1bX;->m:LX/1ny;

    .line 2313633
    move-object v0, v0

    .line 2313634
    invoke-static {p1}, LX/1o9;->a(I)LX/1o9;

    move-result-object v1

    .line 2313635
    iput-object v1, v0, LX/1bX;->c:LX/1o9;

    .line 2313636
    move-object v0, v0

    .line 2313637
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 2313638
    :cond_0
    return-object v0
.end method

.method private a(LX/G28;LX/1Pc;)LX/G29;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/G28;",
            "TE;)",
            "LX/G29;"
        }
    .end annotation

    .prologue
    .line 2313601
    move-object/from16 v0, p1

    iget-object v1, v0, LX/G28;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    move-object/from16 v0, p1

    iget v2, v0, LX/G28;->d:I

    invoke-static {v1, v2}, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->a(Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;I)LX/1bf;

    move-result-object v3

    .line 2313602
    move-object/from16 v0, p1

    iget-object v1, v0, LX/G28;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    invoke-static {v1}, LX/G2T;->b(Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v4

    .line 2313603
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1ny;->a(Ljava/lang/String;)LX/1ny;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1bX;->a(LX/1ny;)LX/1bX;

    move-result-object v1

    move-object/from16 v0, p1

    iget v2, v0, LX/G28;->d:I

    invoke-static {v2}, LX/1o9;->a(I)LX/1o9;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1bX;->a(LX/1o9;)LX/1bX;

    move-result-object v1

    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    move-object v2, v1

    .line 2313604
    :goto_0
    move-object/from16 v0, p1

    iget-object v1, v0, LX/G28;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static {p0, v3, v2, v1}, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->a(Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;LX/1bf;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)LX/1aZ;

    move-result-object v14

    .line 2313605
    if-eqz v3, :cond_0

    move-object/from16 v1, p2

    .line 2313606
    check-cast v1, LX/1Pt;

    move-object/from16 v0, p1

    iget-object v5, v0, LX/G28;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v1, v3, v5}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2313607
    :cond_0
    if-eqz v2, :cond_1

    move-object/from16 v1, p2

    .line 2313608
    check-cast v1, LX/1Pp;

    move-object/from16 v0, p1

    iget-object v3, v0, LX/G28;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    invoke-virtual {v3}, Lcom/facebook/timeline/protiles/model/ProtileModel;->k()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v5, v0, LX/G28;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v1, v14, v3, v2, v5}, LX/1Pp;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    move-object/from16 v1, p2

    .line 2313609
    check-cast v1, LX/1Pt;

    move-object/from16 v0, p1

    iget-object v3, v0, LX/G28;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v1, v2, v3}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2313610
    :cond_1
    move-object/from16 v0, p1

    iget-object v1, v0, LX/G28;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->d()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$TitleModel;

    move-result-object v1

    if-eqz v1, :cond_4

    move-object/from16 v0, p1

    iget-object v1, v0, LX/G28;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->d()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$TitleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 2313611
    :goto_1
    move-object/from16 v0, p1

    iget-object v1, v0, LX/G28;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->q()Ljava/lang/String;

    move-result-object v6

    .line 2313612
    move-object/from16 v0, p1

    iget-object v1, v0, LX/G28;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v1

    if-eqz v1, :cond_2

    move-object/from16 v0, p1

    iget-object v1, v0, LX/G28;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    if-nez v1, :cond_5

    :cond_2
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CANNOT_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2313613
    :goto_2
    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->d:LX/G2S;

    invoke-virtual {v1, v7}, LX/G2S;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .line 2313614
    invoke-static {v7}, LX/G2S;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)I

    move-result v9

    .line 2313615
    move-object/from16 v0, p1

    iget-object v1, v0, LX/G28;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v1

    move-object/from16 v0, p1

    iget v2, v0, LX/G28;->c:I

    invoke-static {p0, v1, v2, v5}, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->a(Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2313616
    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->d:LX/G2S;

    invoke-virtual {v1, v7}, LX/G2S;->c(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Ljava/lang/String;

    move-result-object v11

    .line 2313617
    move-object/from16 v0, p1

    iget-object v2, v0, LX/G28;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    move-object/from16 v0, p1

    iget-object v3, v0, LX/G28;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->a(Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;Lcom/facebook/timeline/protiles/model/ProtileModel;LX/1Fb;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v12

    .line 2313618
    const/16 v1, 0x8

    if-ne v9, v1, :cond_6

    const/4 v13, 0x0

    .line 2313619
    :goto_3
    new-instance v3, LX/G29;

    move-object/from16 v0, p1

    iget-object v1, v0, LX/G28;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    invoke-static {p0, v1}, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->a(Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    move-object v4, v14

    invoke-direct/range {v3 .. v13}, LX/G29;-><init>(LX/1aZ;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Landroid/graphics/drawable/Drawable;ILjava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    return-object v3

    .line 2313620
    :cond_3
    const/4 v1, 0x0

    move-object v2, v1

    goto/16 :goto_0

    .line 2313621
    :cond_4
    const/4 v5, 0x0

    goto :goto_1

    .line 2313622
    :cond_5
    move-object/from16 v0, p1

    iget-object v1, v0, LX/G28;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v7

    goto :goto_2

    .line 2313623
    :cond_6
    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->d:LX/G2S;

    move-object/from16 v0, p1

    iget-object v2, v0, LX/G28;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    move-object/from16 v0, p1

    iget-object v3, v0, LX/G28;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    move-object/from16 v0, p2

    invoke-virtual {v1, v2, v3, v0}, LX/G2S;->a(Lcom/facebook/timeline/protiles/model/ProtileModel;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;LX/1Pc;)Landroid/view/View$OnClickListener;

    move-result-object v13

    goto :goto_3
.end method

.method private static a(Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 2313584
    const/4 v0, 0x0

    .line 2313585
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->o()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel$ProfilePhotoModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2313586
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->o()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel$ProfilePhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel$ProfilePhotoModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2313587
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->f:LX/EQL;

    const v2, 0x7f0a069b

    invoke-virtual {v1, v0, v2}, LX/EQL;->a(Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;Lcom/facebook/timeline/protiles/model/ProtileModel;LX/1Fb;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 7
    .param p2    # Lcom/facebook/timeline/protiles/model/ProtileModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2313588
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2313589
    const/4 v0, 0x0

    .line 2313590
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/G26;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/G26;-><init>(Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;Lcom/facebook/timeline/protiles/model/ProtileModel;LX/1Fb;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;
    .locals 10

    .prologue
    .line 2313573
    const-class v1, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;

    monitor-enter v1

    .line 2313574
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2313575
    sput-object v2, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2313576
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2313577
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2313578
    new-instance v3, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v5

    check-cast v5, LX/1K9;

    invoke-static {v0}, LX/G2N;->a(LX/0QB;)LX/G2N;

    move-result-object v6

    check-cast v6, LX/G2N;

    invoke-static {v0}, LX/G2S;->a(LX/0QB;)LX/G2S;

    move-result-object v7

    check-cast v7, LX/G2S;

    const/16 v8, 0x509

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/EQL;->a(LX/0QB;)LX/EQL;

    move-result-object v9

    check-cast v9, LX/EQL;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;-><init>(Landroid/content/res/Resources;LX/1K9;LX/G2N;LX/G2S;LX/0Or;LX/EQL;)V

    .line 2313579
    move-object v0, v3

    .line 2313580
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2313581
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2313582
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2313583
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;ILjava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2313568
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne p1, v0, :cond_0

    .line 2313569
    :goto_0
    return-object p3

    .line 2313570
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne p1, v0, :cond_1

    .line 2313571
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->a:Landroid/content/res/Resources;

    const v1, 0x7f081ff3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    add-int/lit8 v3, p2, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    .line 2313572
    :cond_1
    const-string p3, ""

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2313567
    check-cast p2, LX/G28;

    check-cast p3, LX/1Pc;

    invoke-direct {p0, p2, p3}, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->a(LX/G28;LX/1Pc;)LX/G29;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x106fb1d1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2313546
    check-cast p1, LX/G28;

    check-cast p2, LX/G29;

    check-cast p4, Lcom/facebook/widget/CustomLinearLayout;

    .line 2313547
    iget v1, p1, LX/G28;->c:I

    invoke-virtual {p4, v1}, Lcom/facebook/widget/CustomLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/G2Z;

    .line 2313548
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/G2Z;->setVisibility(I)V

    .line 2313549
    iget v2, p1, LX/G28;->d:I

    .line 2313550
    iget-object p1, v1, LX/G2Z;->e:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    .line 2313551
    iput v2, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2313552
    iput v2, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2313553
    iget-object v2, p2, LX/G29;->a:LX/1aZ;

    invoke-virtual {v1, v2}, LX/G2Z;->setProfilePictureController(LX/1aZ;)V

    .line 2313554
    iget-object v2, p2, LX/G29;->b:Landroid/graphics/drawable/Drawable;

    .line 2313555
    iget-object p1, v1, LX/G2Z;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object p1

    check-cast p1, LX/1af;

    invoke-virtual {p1, v2}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2313556
    iget-object v2, p2, LX/G29;->e:Landroid/graphics/drawable/Drawable;

    .line 2313557
    iget-object p1, v1, LX/G2Z;->d:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2313558
    iget v2, p2, LX/G29;->f:I

    .line 2313559
    iget-object p1, v1, LX/G2Z;->d:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2313560
    iget-object v2, p2, LX/G29;->g:Ljava/lang/String;

    .line 2313561
    iget-object p1, v1, LX/G2Z;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2313562
    iget-object v2, p2, LX/G29;->h:Ljava/lang/String;

    .line 2313563
    iget-object p1, v1, LX/G2Z;->d:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2313564
    iget-object v2, p2, LX/G29;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, LX/G2Z;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2313565
    iget-object v2, p2, LX/G29;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, LX/G2Z;->setOnAddFriendClickListener(Landroid/view/View$OnClickListener;)V

    .line 2313566
    const/16 v1, 0x1f

    const v2, 0x56eb6755

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2313539
    check-cast p1, LX/G28;

    check-cast p4, Lcom/facebook/widget/CustomLinearLayout;

    const/4 v1, 0x0

    .line 2313540
    iget v0, p1, LX/G28;->c:I

    invoke-virtual {p4, v0}, Lcom/facebook/widget/CustomLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/G2Z;

    .line 2313541
    invoke-virtual {v0, v1}, LX/G2Z;->setProfilePictureController(LX/1aZ;)V

    .line 2313542
    invoke-virtual {v0, v1}, LX/G2Z;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2313543
    invoke-virtual {v0, v1}, LX/G2Z;->setOnAddFriendClickListener(Landroid/view/View$OnClickListener;)V

    .line 2313544
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/G2Z;->setVisibility(I)V

    .line 2313545
    return-void
.end method
