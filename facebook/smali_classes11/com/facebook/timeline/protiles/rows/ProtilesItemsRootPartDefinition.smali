.class public Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/timeline/protiles/model/ProtileModel;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;

.field private final b:Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;

.field private final c:Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;

.field private final d:Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2313660
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2313661
    iput-object p2, p0, Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;->a:Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;

    .line 2313662
    iput-object p1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;->b:Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;

    .line 2313663
    iput-object p3, p0, Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;->c:Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;

    .line 2313664
    iput-object p4, p0, Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;->d:Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;

    .line 2313665
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;
    .locals 7

    .prologue
    .line 2313666
    const-class v1, Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;

    monitor-enter v1

    .line 2313667
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2313668
    sput-object v2, Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2313669
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2313670
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2313671
    new-instance p0, Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;-><init>(Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;)V

    .line 2313672
    move-object v0, p0

    .line 2313673
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2313674
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2313675
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2313676
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2313677
    check-cast p2, Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2313678
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;->b:Lcom/facebook/timeline/protiles/rows/ProtilesGridPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;->a:Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;->c:Lcom/facebook/timeline/protiles/rows/ProtilesVideoSelectorPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2313679
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesItemsRootPartDefinition;->d:Lcom/facebook/timeline/protiles/rows/ProtilesCallToActionComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2313680
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2313681
    check-cast p1, Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2313682
    if-eqz p1, :cond_0

    .line 2313683
    iget-object v0, p1, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    move-object v0, v0

    .line 2313684
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
