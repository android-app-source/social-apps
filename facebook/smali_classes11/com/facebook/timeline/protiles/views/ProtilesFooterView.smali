.class public Lcom/facebook/timeline/protiles/views/ProtilesFooterView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/graphics/Paint;

.field private c:I

.field public d:Z

.field private e:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2314219
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2314220
    invoke-direct {p0}, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->a()V

    .line 2314221
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2314246
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2314247
    invoke-direct {p0}, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->a()V

    .line 2314248
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2314249
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2314250
    invoke-direct {p0}, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->a()V

    .line 2314251
    return-void
.end method

.method private a()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 2314231
    const-class v0, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2314232
    const v0, 0x7f03109f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2314233
    const v0, 0x7f021534

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->setBackgroundResource(I)V

    .line 2314234
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->b:Landroid/graphics/Paint;

    .line 2314235
    iget-object v0, p0, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->b:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a00e7

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2314236
    iget-object v0, p0, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->b:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b0033

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2314237
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1480

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->c:I

    .line 2314238
    const v0, 0x7f0d279d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->e:Landroid/widget/TextView;

    .line 2314239
    iget-object v0, p0, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->a:LX/0hL;

    const v1, 0x7f0207ed

    invoke-virtual {v0, v1}, LX/0hL;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2314240
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b148b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2314241
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    neg-int v5, v3

    move v4, v2

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 2314242
    iget-object v1, p0, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->a:LX/0hL;

    invoke-virtual {v1}, LX/0hL;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2314243
    iget-object v1, p0, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2314244
    :goto_0
    return-void

    .line 2314245
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v6, v6, v0, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;

    invoke-static {v0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v0

    check-cast v0, LX/0hL;

    iput-object v0, p0, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->a:LX/0hL;

    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2314226
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2314227
    iget-boolean v0, p0, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->d:Z

    if-eqz v0, :cond_0

    .line 2314228
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b148f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2314229
    iget v1, p0, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->c:I

    int-to-float v1, v1

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->getWidth()I

    move-result v3

    iget v4, p0, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->c:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2314230
    :cond_0
    return-void
.end method

.method public setFooterVisibility(I)V
    .locals 1

    .prologue
    .line 2314224
    iget-object v0, p0, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2314225
    return-void
.end method

.method public setSeparatorIsVisible(Z)V
    .locals 0

    .prologue
    .line 2314222
    iput-boolean p1, p0, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->d:Z

    .line 2314223
    return-void
.end method
