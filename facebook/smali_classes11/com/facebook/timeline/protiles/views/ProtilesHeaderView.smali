.class public Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field public j:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2314306
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 2314307
    invoke-direct {p0}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->d()V

    .line 2314308
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2314303
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2314304
    invoke-direct {p0}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->d()V

    .line 2314305
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2314300
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2314301
    invoke-direct {p0}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->d()V

    .line 2314302
    return-void
.end method

.method private static a(ILandroid/text/Spannable;)V
    .locals 4

    .prologue
    .line 2314298
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v0, p0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v1, 0x0

    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-interface {p1, v0, v1, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2314299
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;

    invoke-static {v0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v0

    check-cast v0, LX/0hL;

    iput-object v0, p0, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->j:LX/0hL;

    return-void
.end method

.method private static b(ILandroid/text/Spannable;)V
    .locals 4

    .prologue
    .line 2314296
    new-instance v0, Landroid/text/style/AbsoluteSizeSpan;

    invoke-direct {v0, p0}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    const/4 v1, 0x0

    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-interface {p1, v0, v1, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2314297
    return-void
.end method

.method private d()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2314265
    const-class v0, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2314266
    const v0, 0x7f0310a1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2314267
    const v0, 0x7f021536

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->setBackgroundResource(I)V

    .line 2314268
    const v0, 0x7f0d27a3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2314269
    const v0, 0x7f0d27a4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->l:Landroid/widget/TextView;

    .line 2314270
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1480

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2314271
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b1488

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2314272
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1489

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2314273
    invoke-virtual {p0, v0, v1, v0, v3}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->setPadding(IIII)V

    .line 2314274
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b148a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 2314275
    iget-object v0, p0, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->j:LX/0hL;

    const v1, 0x7f0207ed

    invoke-virtual {v0, v1}, LX/0hL;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2314276
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b148b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2314277
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    neg-int v5, v3

    move v4, v2

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    iput-object v0, p0, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->m:Landroid/graphics/drawable/Drawable;

    .line 2314278
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2314290
    if-nez p1, :cond_0

    .line 2314291
    iget-object v0, p0, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2314292
    :goto_0
    return-void

    .line 2314293
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->j:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2314294
    iget-object v0, p0, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2314295
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2, v2, v1, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2314281
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2314282
    const/high16 v1, -0x1000000

    invoke-static {v1, v0}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->a(ILandroid/text/Spannable;)V

    .line 2314283
    iget-object v1, p0, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2314284
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2314285
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    const-string v1, " \u00b7 "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 2314286
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {v1, v0}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->a(ILandroid/text/Spannable;)V

    .line 2314287
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b004e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v1, v0}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->b(ILandroid/text/Spannable;)V

    .line 2314288
    iget-object v1, p0, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 2314289
    :cond_0
    return-void
.end method

.method public setDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2314279
    iget-object v0, p0, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2314280
    return-void
.end method
