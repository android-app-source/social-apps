.class public Lcom/facebook/timeline/protiles/model/ProtileModel;
.super Lcom/facebook/graphql/model/BaseFeedUnit;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field private final b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z

.field public e:J

.field public f:Z

.field public g:Z

.field private h:J


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;)V
    .locals 2

    .prologue
    .line 2310930
    invoke-direct {p0}, Lcom/facebook/graphql/model/BaseFeedUnit;-><init>()V

    .line 2310931
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2310932
    iput-object p1, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    .line 2310933
    invoke-direct {p0}, Lcom/facebook/timeline/protiles/model/ProtileModel;->w()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewFieldsModel;

    move-result-object v0

    .line 2310934
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewFieldsModel;->j()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemsConnectionFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2310935
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewFieldsModel;->j()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemsConnectionFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    .line 2310936
    :goto_0
    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->d:Z

    .line 2310937
    return-void

    .line 2310938
    :cond_0
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2310939
    iput-object v1, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    goto :goto_0

    .line 2310940
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/timeline/protiles/model/ProtileModel;Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 7

    .prologue
    .line 2310941
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2310942
    iget-object v0, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    .line 2310943
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2310944
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v4

    .line 2310945
    new-instance v5, LX/G1V;

    invoke-direct {v5}, LX/G1V;-><init>()V

    .line 2310946
    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    iput-object v6, v5, LX/G1V;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2310947
    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/G1V;->b:Ljava/lang/String;

    .line 2310948
    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v6

    iput-object v6, v5, LX/G1V;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 2310949
    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->s()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    iput-object v6, v5, LX/G1V;->d:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2310950
    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/G1V;->e:Ljava/lang/String;

    .line 2310951
    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->t()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v6

    iput-object v6, v5, LX/G1V;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 2310952
    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v6

    iput-object v6, v5, LX/G1V;->g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2310953
    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/G1V;->h:Ljava/lang/String;

    .line 2310954
    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    iput-object v6, v5, LX/G1V;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2310955
    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->v()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    iput-object v6, v5, LX/G1V;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2310956
    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    iput-object v6, v5, LX/G1V;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2310957
    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->x()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    iput-object v6, v5, LX/G1V;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2310958
    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->y()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel$ProfilePhotoModel;

    move-result-object v6

    iput-object v6, v5, LX/G1V;->m:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel$ProfilePhotoModel;

    .line 2310959
    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->z()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v6

    iput-object v6, v5, LX/G1V;->n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 2310960
    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->q()I

    move-result v6

    iput v6, v5, LX/G1V;->o:I

    .line 2310961
    move-object v4, v5

    .line 2310962
    iput p2, v4, LX/G1V;->o:I

    .line 2310963
    move-object v4, v4

    .line 2310964
    iput-object p3, v4, LX/G1V;->g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2310965
    move-object v4, v4

    .line 2310966
    invoke-virtual {v4}, LX/G1V;->a()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v4

    .line 2310967
    new-instance v5, LX/G1U;

    invoke-direct {v5}, LX/G1U;-><init>()V

    .line 2310968
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->j()Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    move-result-object v6

    iput-object v6, v5, LX/G1U;->a:Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    .line 2310969
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v6

    iput-object v6, v5, LX/G1U;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    .line 2310970
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->l()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$SubtitleModel;

    move-result-object v6

    iput-object v6, v5, LX/G1U;->c:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$SubtitleModel;

    .line 2310971
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->m()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$TitleModel;

    move-result-object v6

    iput-object v6, v5, LX/G1U;->d:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$TitleModel;

    .line 2310972
    move-object v0, v5

    .line 2310973
    iput-object v4, v0, LX/G1U;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    .line 2310974
    move-object v0, v0

    .line 2310975
    invoke-virtual {v0}, LX/G1U;->a()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2310976
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 2310977
    :cond_0
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2310978
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    .line 2310979
    return-void
.end method

.method public static b(Lcom/facebook/timeline/protiles/model/ProtileModel;Ljava/lang/String;)Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2310984
    iget-object v0, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    .line 2310985
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2310986
    :goto_1
    return-object v0

    .line 2310987
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2310988
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private w()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewFieldsModel;
    .locals 2

    .prologue
    .line 2310980
    iget-object v0, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->l()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewsConnectionFieldsModel;

    move-result-object v0

    .line 2310981
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2310982
    :cond_0
    const/4 v0, 0x0

    .line 2310983
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewFieldsModel;

    goto :goto_0
.end method


# virtual methods
.method public final F_()J
    .locals 2

    .prologue
    .line 2310989
    iget-wide v0, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->h:J

    return-wide v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 2310921
    iput-wide p1, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->h:J

    .line 2310922
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;J)V
    .locals 1

    .prologue
    .line 2310924
    invoke-static {p0, p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->b(Lcom/facebook/timeline/protiles/model/ProtileModel;Ljava/lang/String;)Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    move-result-object v0

    .line 2310925
    if-nez v0, :cond_0

    .line 2310926
    :goto_0
    return-void

    .line 2310927
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->q()I

    move-result v0

    invoke-static {p0, p1, v0, p2}, Lcom/facebook/timeline/protiles/model/ProtileModel;->a(Lcom/facebook/timeline/protiles/model/ProtileModel;Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2310928
    invoke-virtual {p0, p3, p4}, Lcom/facebook/graphql/model/BaseFeedUnit;->a(J)V

    goto :goto_0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2310929
    iget-object v0, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2310923
    iget-object v0, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->k()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2310918
    iget-object v0, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->n()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2310919
    const/4 v0, 0x0

    .line 2310920
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->n()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final n()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2310915
    iget-object v0, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->m()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$SubtitleModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2310916
    const/4 v0, 0x0

    .line 2310917
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->m()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$SubtitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$SubtitleModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2310912
    iget-object v0, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->j()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2310913
    const/4 v0, 0x0

    .line 2310914
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->j()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final p()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2310911
    iget-object v0, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->a()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2310907
    invoke-direct {p0}, Lcom/facebook/timeline/protiles/model/ProtileModel;->w()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewFieldsModel;

    move-result-object v0

    .line 2310908
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewFieldsModel$ViewMediasetModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2310909
    :cond_0
    const/4 v0, 0x0

    .line 2310910
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewFieldsModel$ViewMediasetModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewFieldsModel$ViewMediasetModel;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
