.class public Lcom/facebook/timeline/actionbar/TimelineActionBar;
.super LX/AhO;
.source ""

# interfaces
.implements LX/BP9;
.implements LX/2ea;


# instance fields
.field public a:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2293899
    invoke-direct {p0, p1}, LX/AhO;-><init>(Landroid/content/Context;)V

    .line 2293900
    invoke-direct {p0}, Lcom/facebook/timeline/actionbar/TimelineActionBar;->e()V

    .line 2293901
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2293896
    invoke-direct {p0, p1, p2}, LX/AhO;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2293897
    invoke-direct {p0}, Lcom/facebook/timeline/actionbar/TimelineActionBar;->e()V

    .line 2293898
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2293893
    invoke-direct {p0, p1, p2, p3}, LX/AhO;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2293894
    invoke-direct {p0}, Lcom/facebook/timeline/actionbar/TimelineActionBar;->e()V

    .line 2293895
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/actionbar/TimelineActionBar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/timeline/actionbar/TimelineActionBar;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v0, p0, Lcom/facebook/timeline/actionbar/TimelineActionBar;->a:LX/0ad;

    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 2293890
    const-class v0, Lcom/facebook/timeline/actionbar/TimelineActionBar;

    invoke-static {v0, p0}, Lcom/facebook/timeline/actionbar/TimelineActionBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2293891
    invoke-direct {p0}, Lcom/facebook/timeline/actionbar/TimelineActionBar;->f()V

    .line 2293892
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2293886
    iget-object v1, p0, Lcom/facebook/timeline/actionbar/TimelineActionBar;->a:LX/0ad;

    sget-short v2, LX/0wf;->W:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 2293887
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/actionbar/TimelineActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0d7d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2293888
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, v0}, LX/AhO;->a(ZZI)V

    .line 2293889
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2293872
    iget-boolean v0, p0, Lcom/facebook/timeline/actionbar/TimelineActionBar;->b:Z

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2293883
    const/4 v0, 0x0

    .line 2293884
    iput-object v0, p0, LX/AhO;->b:LX/AhN;

    .line 2293885
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x79a0e2cf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2293879
    invoke-super {p0}, LX/AhO;->onAttachedToWindow()V

    .line 2293880
    const/4 v1, 0x1

    .line 2293881
    iput-boolean v1, p0, Lcom/facebook/timeline/actionbar/TimelineActionBar;->b:Z

    .line 2293882
    const/16 v1, 0x2d

    const v2, -0x18530fa5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x3343e353    # -9.8624872E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2293875
    invoke-super {p0}, LX/AhO;->onDetachedFromWindow()V

    .line 2293876
    const/4 v1, 0x0

    .line 2293877
    iput-boolean v1, p0, Lcom/facebook/timeline/actionbar/TimelineActionBar;->b:Z

    .line 2293878
    const/16 v1, 0x2d

    const v2, -0x57027b23

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setHasBeenAttached(Z)V
    .locals 0

    .prologue
    .line 2293873
    iput-boolean p1, p0, Lcom/facebook/timeline/actionbar/TimelineActionBar;->b:Z

    .line 2293874
    return-void
.end method
