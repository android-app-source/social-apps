.class public final Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/GgK;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/common/callercontext/CallerContext;

.field public b:I

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic f:LX/GgK;


# direct methods
.method public constructor <init>(LX/GgK;)V
    .locals 1

    .prologue
    .line 2378931
    iput-object p1, p0, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->f:LX/GgK;

    .line 2378932
    move-object v0, p1

    .line 2378933
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2378934
    sget-object v0, Lcom/facebook/fig/components/contextrow/FigContextRowComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2378935
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2378936
    const-string v0, "FigContextRowComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2378909
    if-ne p0, p1, :cond_1

    .line 2378910
    :cond_0
    :goto_0
    return v0

    .line 2378911
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2378912
    goto :goto_0

    .line 2378913
    :cond_3
    check-cast p1, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;

    .line 2378914
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2378915
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2378916
    if-eq v2, v3, :cond_0

    .line 2378917
    iget-object v2, p0, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2378918
    goto :goto_0

    .line 2378919
    :cond_5
    iget-object v2, p1, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_4

    .line 2378920
    :cond_6
    iget v2, p0, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->b:I

    iget v3, p1, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2378921
    goto :goto_0

    .line 2378922
    :cond_7
    iget-object v2, p0, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->c:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 2378923
    goto :goto_0

    .line 2378924
    :cond_9
    iget-object v2, p1, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->c:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 2378925
    :cond_a
    iget-object v2, p0, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->d:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 2378926
    goto :goto_0

    .line 2378927
    :cond_c
    iget-object v2, p1, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->d:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 2378928
    :cond_d
    iget-object v2, p0, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->e:Ljava/util/List;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2378929
    goto :goto_0

    .line 2378930
    :cond_e
    iget-object v2, p1, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->e:Ljava/util/List;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
