.class public final Lcom/facebook/fig/texttabbar/FigTextTabBar;
.super Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2379370
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fig/texttabbar/FigTextTabBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2379371
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2379367
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2379368
    invoke-direct {p0, p1}, Lcom/facebook/fig/texttabbar/FigTextTabBar;->a(Landroid/content/Context;)V

    .line 2379369
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2379323
    const v0, 0x7f0e066a

    .line 2379324
    sget-object v1, LX/03r;->FigTextTabBarAttrs:[I

    .line 2379325
    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2379326
    iget-object v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    move-object v1, v1

    .line 2379327
    const/16 v2, 0x7

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 2379328
    invoke-virtual {v1, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->setTabLayout(I)V

    .line 2379329
    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2379330
    if-eqz v2, :cond_1

    .line 2379331
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 2379332
    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2379333
    const/16 v2, 0x0

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 2379334
    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerPadding(I)V

    .line 2379335
    const/16 v2, 0xc

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 2379336
    if-lez v2, :cond_0

    .line 2379337
    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 2379338
    :cond_0
    :goto_0
    const/16 v2, 0x5

    const/high16 v3, -0x1000000

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 2379339
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setUnderlineColor(I)V

    .line 2379340
    const/16 v2, 0x6

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 2379341
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setUnderlineHeight(I)V

    .line 2379342
    const/16 v2, 0x9

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    .line 2379343
    iput-boolean v2, v1, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->h:Z

    .line 2379344
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 2379345
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setCenterSelectedTab(Z)V

    .line 2379346
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 2379347
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setFillParentWidth(Z)V

    .line 2379348
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    const/16 v2, 0x2

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    const/16 v3, 0x3

    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    const/16 v4, 0x4

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/facebook/fig/texttabbar/FigTextTabBar;->setPadding(IIII)V

    .line 2379349
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2379350
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fig/texttabbar/FigTextTabBar;->a:Z

    .line 2379351
    return-void

    .line 2379352
    :cond_1
    invoke-virtual {v1, v5}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 2379353
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2379354
    invoke-virtual {v1, v5}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->setWillNotDraw(Z)V

    goto :goto_0
.end method


# virtual methods
.method public final setFillParentWidth(Z)V
    .locals 2

    .prologue
    .line 2379363
    iget-boolean v0, p0, Lcom/facebook/fig/texttabbar/FigTextTabBar;->a:Z

    if-eqz v0, :cond_0

    .line 2379364
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setFillParentWidth"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 2379365
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setFillParentWidth(Z)V

    .line 2379366
    return-void
.end method

.method public final setUnderlineColor(I)V
    .locals 2

    .prologue
    .line 2379359
    iget-boolean v0, p0, Lcom/facebook/fig/texttabbar/FigTextTabBar;->a:Z

    if-eqz v0, :cond_0

    .line 2379360
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setUnderlineColor"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 2379361
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setUnderlineColor(I)V

    .line 2379362
    return-void
.end method

.method public final setUnderlineHeight(I)V
    .locals 2

    .prologue
    .line 2379355
    iget-boolean v0, p0, Lcom/facebook/fig/texttabbar/FigTextTabBar;->a:Z

    if-eqz v0, :cond_0

    .line 2379356
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setUnderlineHeight"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 2379357
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setUnderlineHeight(I)V

    .line 2379358
    return-void
.end method
