.class public final Lcom/facebook/fig/starrating/FigStackedRatingView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/fig/starrating/FigStackedRatingView$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2379138
    new-instance v0, LX/GgN;

    invoke-direct {v0}, LX/GgN;-><init>()V

    sput-object v0, Lcom/facebook/fig/starrating/FigStackedRatingView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2379142
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 2379143
    const/4 v0, 0x5

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/facebook/fig/starrating/FigStackedRatingView$SavedState;->a:[I

    .line 2379144
    iget-object v0, p0, Lcom/facebook/fig/starrating/FigStackedRatingView$SavedState;->a:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readIntArray([I)V

    .line 2379145
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 2379146
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 2379147
    const/4 v0, 0x5

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/facebook/fig/starrating/FigStackedRatingView$SavedState;->a:[I

    .line 2379148
    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2379139
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2379140
    iget-object v0, p0, Lcom/facebook/fig/starrating/FigStackedRatingView$SavedState;->a:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 2379141
    return-void
.end method
