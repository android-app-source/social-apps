.class public Lcom/facebook/fig/starrating/FigStarRatingBar;
.super Landroid/widget/RatingBar;
.source ""


# static fields
.field private static a:Landroid/util/SparseIntArray;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2379293
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 2379294
    sput-object v0, Lcom/facebook/fig/starrating/FigStarRatingBar;->a:Landroid/util/SparseIntArray;

    const/4 v1, 0x1

    const v2, 0x7f0105fe

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 2379295
    sget-object v0, Lcom/facebook/fig/starrating/FigStarRatingBar;->a:Landroid/util/SparseIntArray;

    const/4 v1, 0x2

    const v2, 0x7f0105ff

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 2379296
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 2379297
    invoke-static {p1}, Lcom/facebook/fig/starrating/FigStarRatingBar;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/fig/starrating/FigStarRatingBar;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v2, p2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Landroid/widget/RatingBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2379298
    invoke-direct {p0}, Lcom/facebook/fig/starrating/FigStarRatingBar;->a()V

    .line 2379299
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 2379300
    invoke-static {p1}, Lcom/facebook/fig/starrating/FigStarRatingBar;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/facebook/fig/starrating/FigStarRatingBar;->a(Landroid/content/Context;Landroid/util/AttributeSet;)I

    move-result v1

    invoke-direct {p0, v0, p2, v1}, Landroid/widget/RatingBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2379301
    invoke-direct {p0}, Lcom/facebook/fig/starrating/FigStarRatingBar;->a()V

    .line 2379302
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/util/AttributeSet;)I
    .locals 4

    .prologue
    .line 2379303
    sget-object v0, LX/03r;->FigStarRatingBar:[I

    invoke-virtual {p0, p1, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 2379304
    :try_start_0
    const/16 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2379305
    sget-object v0, Lcom/facebook/fig/starrating/FigStarRatingBar;->a:Landroid/util/SparseIntArray;

    const/16 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/util/SparseIntArray;->get(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2379306
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    return v0

    .line 2379307
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "The starRatingBarType attribute needs to be set via XML"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2379308
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private static a(Landroid/content/Context;)Landroid/content/Context;
    .locals 2

    .prologue
    .line 2379309
    new-instance v0, Landroid/content/ContextWrapper;

    invoke-direct {v0, p0}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 2379310
    const v1, 0x7f0e0669

    invoke-virtual {v0, v1}, Landroid/content/Context;->setTheme(I)V

    .line 2379311
    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2379312
    invoke-virtual {p0}, Lcom/facebook/fig/starrating/FigStarRatingBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 2379313
    const v1, 0x102000d

    const v2, 0x7f0a0529

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/fig/starrating/FigStarRatingBar;->a(Landroid/graphics/drawable/LayerDrawable;II)V

    .line 2379314
    const v1, 0x102000f

    const v2, 0x7f0a0528

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/fig/starrating/FigStarRatingBar;->a(Landroid/graphics/drawable/LayerDrawable;II)V

    .line 2379315
    const/high16 v1, 0x1020000

    const v2, 0x7f0a0528

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/fig/starrating/FigStarRatingBar;->a(Landroid/graphics/drawable/LayerDrawable;II)V

    .line 2379316
    return-void
.end method

.method private a(Landroid/graphics/drawable/LayerDrawable;II)V
    .locals 2
    .param p3    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 2379317
    invoke-virtual {p0}, Lcom/facebook/fig/starrating/FigStarRatingBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    .line 2379318
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2379319
    invoke-static {v1}, LX/1d5;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2379320
    invoke-static {v1, v0}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 2379321
    invoke-virtual {p1, p2, v1}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 2379322
    return-void
.end method
