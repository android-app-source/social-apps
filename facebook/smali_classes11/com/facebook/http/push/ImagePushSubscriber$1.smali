.class public final Lcom/facebook/http/push/ImagePushSubscriber$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/http/push/ImagePushSubscriber;


# direct methods
.method public constructor <init>(Lcom/facebook/http/push/ImagePushSubscriber;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2206738
    iput-object p1, p0, Lcom/facebook/http/push/ImagePushSubscriber$1;->c:Lcom/facebook/http/push/ImagePushSubscriber;

    iput-object p2, p0, Lcom/facebook/http/push/ImagePushSubscriber$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/http/push/ImagePushSubscriber$1;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2206722
    iget-object v0, p0, Lcom/facebook/http/push/ImagePushSubscriber$1;->c:Lcom/facebook/http/push/ImagePushSubscriber;

    iget-object v0, v0, Lcom/facebook/http/push/ImagePushSubscriber;->d:LX/FA8;

    iget-object v1, p0, Lcom/facebook/http/push/ImagePushSubscriber$1;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/http/push/ImagePushSubscriber$1;->b:Ljava/lang/String;

    .line 2206723
    iget-object v3, v0, LX/FA8;->a:LX/0Zb;

    const-string v4, "android_image_push_promise"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2206724
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2206725
    :goto_0
    iget-object v0, p0, Lcom/facebook/http/push/ImagePushSubscriber$1;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    sget-object v1, LX/1bc;->HIGH:LX/1bc;

    .line 2206726
    iput-object v1, v0, LX/1bX;->i:LX/1bc;

    .line 2206727
    move-object v0, v0

    .line 2206728
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 2206729
    const-class v1, Lcom/facebook/http/push/ImagePushSubscriber;

    iget-object v2, p0, Lcom/facebook/http/push/ImagePushSubscriber$1;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    .line 2206730
    iget-object v2, p0, Lcom/facebook/http/push/ImagePushSubscriber$1;->c:Lcom/facebook/http/push/ImagePushSubscriber;

    iget-object v2, v2, Lcom/facebook/http/push/ImagePushSubscriber;->b:LX/1HI;

    sget-object v3, LX/1bc;->HIGH:LX/1bc;

    invoke-virtual {v2, v0, v1, v3}, LX/1HI;->a(LX/1bf;Ljava/lang/Object;LX/1bc;)LX/1ca;

    move-result-object v0

    .line 2206731
    new-instance v1, LX/FA9;

    invoke-direct {v1}, LX/FA9;-><init>()V

    .line 2206732
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 2206733
    invoke-interface {v0, v1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 2206734
    return-void

    .line 2206735
    :cond_0
    const-string v4, "tag"

    invoke-virtual {v3, v4, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2206736
    const-string v4, "url"

    invoke-virtual {v3, v4, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2206737
    invoke-virtual {v3}, LX/0oG;->d()V

    goto :goto_0
.end method
