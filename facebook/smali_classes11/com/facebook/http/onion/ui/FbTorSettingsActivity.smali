.class public abstract Lcom/facebook/http/onion/ui/FbTorSettingsActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""


# instance fields
.field public a:LX/0s5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/preference/PreferenceScreen;

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2391914
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/http/onion/ui/FbTorSettingsActivity;

    invoke-static {v1}, LX/0s5;->a(LX/0QB;)LX/0s5;

    move-result-object v0

    check-cast v0, LX/0s5;

    const/16 v2, 0xf9a

    invoke-static {v1, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v0, p0, Lcom/facebook/http/onion/ui/FbTorSettingsActivity;->a:LX/0s5;

    iput-object v1, p0, Lcom/facebook/http/onion/ui/FbTorSettingsActivity;->c:LX/0Ot;

    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(Z)V
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2391915
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 2391916
    invoke-static {p0, p0}, Lcom/facebook/http/onion/ui/FbTorSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2391917
    invoke-virtual {p0}, Lcom/facebook/http/onion/ui/FbTorSettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/http/onion/ui/FbTorSettingsActivity;->b:Landroid/preference/PreferenceScreen;

    .line 2391918
    iget-object v0, p0, Lcom/facebook/http/onion/ui/FbTorSettingsActivity;->b:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0, v0}, Lcom/facebook/http/onion/ui/FbTorSettingsActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 2391919
    iget-object v0, p0, Lcom/facebook/http/onion/ui/FbTorSettingsActivity;->b:Landroid/preference/PreferenceScreen;

    .line 2391920
    new-instance v1, LX/GmN;

    invoke-direct {v1, p0}, LX/GmN;-><init>(Landroid/content/Context;)V

    .line 2391921
    new-instance v2, LX/GmK;

    invoke-direct {v2, p0}, LX/GmK;-><init>(Lcom/facebook/http/onion/ui/FbTorSettingsActivity;)V

    invoke-virtual {v1, v2}, LX/GmN;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2391922
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2391923
    iget-object v2, p0, Lcom/facebook/http/onion/ui/FbTorSettingsActivity;->a:LX/0s5;

    invoke-virtual {v2}, LX/0s5;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2391924
    new-instance v2, LX/GmM;

    invoke-direct {v2, p0}, LX/GmM;-><init>(Landroid/content/Context;)V

    .line 2391925
    new-instance p1, LX/GmL;

    invoke-direct {p1, p0}, LX/GmL;-><init>(Lcom/facebook/http/onion/ui/FbTorSettingsActivity;)V

    invoke-virtual {v2, p1}, LX/GmM;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2391926
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2391927
    invoke-virtual {v1}, LX/GmN;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/GmM;->setDependency(Ljava/lang/String;)V

    .line 2391928
    :cond_0
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x87cf046

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2391929
    invoke-virtual {p0}, Lcom/facebook/http/onion/ui/FbTorSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 2391930
    if-eqz v1, :cond_0

    .line 2391931
    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 2391932
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onDestroy()V

    .line 2391933
    const/16 v1, 0x23

    const v2, 0x37ada4e1    # 2.0699985E-5f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x22

    const v1, 0x3af0c00e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2391934
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onResume()V

    .line 2391935
    iget-object v0, p0, Lcom/facebook/http/onion/ui/FbTorSettingsActivity;->a:LX/0s5;

    invoke-virtual {v0}, LX/0s5;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2391936
    iget-object v0, p0, Lcom/facebook/http/onion/ui/FbTorSettingsActivity;->a:LX/0s5;

    invoke-virtual {v0}, LX/0s5;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2391937
    iget-object v0, p0, Lcom/facebook/http/onion/ui/FbTorSettingsActivity;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, LX/0sF;->c:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    .line 2391938
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/http/onion/ui/FbTorSettingsActivity;->finish()V

    .line 2391939
    :cond_1
    const/16 v0, 0x23

    const v2, 0x2c9b4de

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
