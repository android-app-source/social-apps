.class public Lcom/facebook/fbui/nux/canvas/MobileChromeView;
.super Landroid/view/ViewGroup;
.source ""


# instance fields
.field private a:LX/GdK;

.field private b:Landroid/graphics/Paint;

.field private c:Landroid/graphics/Paint;

.field private d:Landroid/graphics/Paint;

.field private e:Landroid/graphics/Path;

.field private f:Landroid/graphics/Path;

.field private g:Landroid/graphics/Path;

.field private final h:Landroid/graphics/Matrix;

.field private final i:Landroid/graphics/RectF;

.field private final j:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2373706
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/nux/canvas/MobileChromeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2373707
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2373704
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/nux/canvas/MobileChromeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2373705
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 12

    .prologue
    const/high16 v11, -0x3f580000    # -5.25f

    const/4 v6, 0x0

    const/high16 v10, 0x42f50000    # 122.5f

    const v9, 0x42888000    # 68.25f

    const/4 v1, 0x0

    .line 2373659
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2373660
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->h:Landroid/graphics/Matrix;

    .line 2373661
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->i:Landroid/graphics/RectF;

    .line 2373662
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->j:Landroid/graphics/RectF;

    .line 2373663
    sget-object v0, LX/03r;->MobileChromeView:[I

    invoke-virtual {p1, p2, v0, p3, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2373664
    const/16 v2, 0x3

    invoke-virtual {v0, v2, v6}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v2

    .line 2373665
    invoke-static {}, LX/GdK;->values()[LX/GdK;

    move-result-object v3

    aget-object v2, v3, v2

    iput-object v2, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->a:LX/GdK;

    .line 2373666
    const/16 v2, 0x0

    const v3, -0xe9f8df

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 2373667
    const/16 v3, 0x1

    const/4 v4, -0x1

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    .line 2373668
    const/16 v4, 0x2

    const/4 v5, -0x1

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    .line 2373669
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2373670
    invoke-virtual {p0, v6}, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->setWillNotDraw(Z)V

    .line 2373671
    new-instance v0, Landroid/graphics/Paint;

    const/4 v5, 0x1

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->b:Landroid/graphics/Paint;

    .line 2373672
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->b:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2373673
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2373674
    new-instance v0, Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->b:Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->c:Landroid/graphics/Paint;

    .line 2373675
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 2373676
    new-instance v0, Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->b:Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->d:Landroid/graphics/Paint;

    .line 2373677
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 2373678
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    .line 2373679
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    const v2, 0x44073000    # 540.75f

    const v3, 0x430c8000    # 140.5f

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2373680
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    const/high16 v2, -0x3d740000    # -70.0f

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 2373681
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    const v2, -0x3d698000    # -75.25f

    const v3, -0x3d468000    # -92.75f

    const/high16 v4, -0x3d840000    # -63.0f

    const v5, -0x3c7be000    # -264.25f

    const/high16 v6, -0x3d7b0000    # -66.5f

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 2373682
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    const/high16 v2, -0x3fa00000    # -3.5f

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 2373683
    iget-object v2, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    const/high16 v3, -0x3cd10000    # -175.0f

    const/high16 v4, 0x40600000    # 3.5f

    const v5, -0x3c7a2000    # -267.75f

    const/high16 v6, -0x3ef40000    # -8.75f

    const v7, -0x3c7a2000    # -267.75f

    const/high16 v8, 0x42830000    # 65.5f

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 2373684
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    const/high16 v2, 0x430c0000    # 140.0f

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 2373685
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    invoke-virtual {v0, v11, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 2373686
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    const v2, 0x4329c000    # 169.75f

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 2373687
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    const/high16 v2, 0x40a80000    # 5.25f

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 2373688
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    const v2, 0x441b5000    # 621.25f

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 2373689
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    const v3, 0x42c78000    # 99.75f

    move v2, v9

    move v4, v9

    move v5, v10

    move v6, v9

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 2373690
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    const v2, 0x43912000    # 290.25f

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 2373691
    iget-object v2, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    const/high16 v3, 0x41b60000    # 22.75f

    const v8, -0x3d778000    # -68.25f

    move v4, v1

    move v5, v10

    move v6, v1

    move v7, v10

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 2373692
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    const v2, -0x3bb9d000    # -792.75f

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 2373693
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    const/high16 v2, 0x40a80000    # 5.25f

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 2373694
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    const/high16 v2, -0x3d7e0000    # -65.0f

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 2373695
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    invoke-virtual {v0, v11, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 2373696
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 2373697
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->f:Landroid/graphics/Path;

    .line 2373698
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->f:Landroid/graphics/Path;

    const v1, 0x43837000    # 262.875f

    const/high16 v2, 0x42480000    # 50.0f

    const/high16 v3, 0x41200000    # 10.0f

    sget-object v4, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 2373699
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->f:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 2373700
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->g:Landroid/graphics/Path;

    .line 2373701
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->g:Landroid/graphics/Path;

    const/high16 v1, 0x41ee0000    # 29.75f

    const/high16 v2, 0x42b20000    # 89.0f

    const v3, 0x44011000    # 516.25f

    const v4, 0x446d8000    # 950.0f

    sget-object v5, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 2373702
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->g:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 2373703
    return-void
.end method

.method private getOnlyChild()Landroid/view/View;
    .locals 2

    .prologue
    .line 2373656
    invoke-virtual {p0}, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 2373657
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "can have at most one child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2373658
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 2373649
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 2373650
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2373651
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2373652
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->g:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2373653
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->f:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2373654
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2373655
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 2373591
    invoke-direct {p0}, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->getOnlyChild()Landroid/view/View;

    move-result-object v0

    .line 2373592
    if-nez v0, :cond_0

    .line 2373593
    :goto_0
    return-void

    .line 2373594
    :cond_0
    iget-object v1, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->j:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v2, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->j:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->j:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->getMeasuredHeight()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->j:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    float-to-int v5, v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 12

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const/4 v10, 0x1

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v0, 0x0

    .line 2373618
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 2373619
    invoke-virtual {p0}, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->getMeasuredWidth()I

    move-result v1

    .line 2373620
    invoke-virtual {p0}, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->getMeasuredHeight()I

    move-result v2

    .line 2373621
    iget-object v3, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->i:Landroid/graphics/RectF;

    invoke-virtual {v3, v4, v10}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 2373622
    iget-object v3, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->i:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    .line 2373623
    iget-object v4, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->i:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    .line 2373624
    int-to-float v5, v1

    div-float/2addr v5, v3

    .line 2373625
    int-to-float v6, v2

    div-float/2addr v6, v4

    .line 2373626
    iget-object v7, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->a:LX/GdK;

    sget-object v8, LX/GdK;->FIT:LX/GdK;

    if-ne v7, v8, :cond_0

    .line 2373627
    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 2373628
    iget-object v6, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v6, v5, v5, v0, v0}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 2373629
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->h:Landroid/graphics/Matrix;

    int-to-float v6, v1

    mul-float/2addr v3, v5

    sub-float v3, v6, v3

    div-float/2addr v3, v9

    int-to-float v6, v2

    mul-float/2addr v4, v5

    sub-float v4, v6, v4

    div-float/2addr v4, v9

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 2373630
    :goto_0
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->e:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0, v3}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 2373631
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->g:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0, v3}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 2373632
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->f:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0, v3}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 2373633
    invoke-direct {p0}, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->getOnlyChild()Landroid/view/View;

    move-result-object v0

    .line 2373634
    if-nez v0, :cond_3

    .line 2373635
    :goto_1
    return-void

    .line 2373636
    :cond_0
    iget-object v6, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v6, v5, v5, v0, v0}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 2373637
    int-to-float v6, v1

    mul-float/2addr v3, v5

    sub-float v3, v6, v3

    div-float/2addr v3, v9

    .line 2373638
    iget-object v6, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->a:LX/GdK;

    sget-object v7, LX/GdK;->TOP:LX/GdK;

    if-ne v6, v7, :cond_1

    .line 2373639
    :goto_2
    iget-object v4, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v4, v3, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    .line 2373640
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->a:LX/GdK;

    sget-object v6, LX/GdK;->BOTTOM:LX/GdK;

    if-ne v0, v6, :cond_2

    .line 2373641
    int-to-float v0, v2

    mul-float/2addr v4, v5

    sub-float/2addr v0, v4

    goto :goto_2

    .line 2373642
    :cond_2
    int-to-float v0, v2

    mul-float/2addr v4, v5

    sub-float/2addr v0, v4

    div-float/2addr v0, v9

    goto :goto_2

    .line 2373643
    :cond_3
    iget-object v3, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->g:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->j:Landroid/graphics/RectF;

    invoke-virtual {v3, v4, v10}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 2373644
    iget-object v3, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->j:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    .line 2373645
    iget-object v4, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->j:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    float-to-int v4, v4

    .line 2373646
    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2373647
    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 2373648
    invoke-static {v1, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v2, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    goto :goto_1
.end method

.method public setCameraColor(I)V
    .locals 1

    .prologue
    .line 2373615
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2373616
    invoke-virtual {p0}, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->invalidate()V

    .line 2373617
    return-void
.end method

.method public setCameraShader(Landroid/graphics/Shader;)V
    .locals 1

    .prologue
    .line 2373612
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 2373613
    invoke-virtual {p0}, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->invalidate()V

    .line 2373614
    return-void
.end method

.method public setChromeColor(I)V
    .locals 1

    .prologue
    .line 2373609
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2373610
    invoke-virtual {p0}, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->invalidate()V

    .line 2373611
    return-void
.end method

.method public setChromeShader(Landroid/graphics/Shader;)V
    .locals 1

    .prologue
    .line 2373606
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 2373607
    invoke-virtual {p0}, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->invalidate()V

    .line 2373608
    return-void
.end method

.method public setScale(LX/GdK;)V
    .locals 1

    .prologue
    .line 2373601
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->a:LX/GdK;

    if-eq v0, p1, :cond_0

    .line 2373602
    iput-object p1, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->a:LX/GdK;

    .line 2373603
    invoke-virtual {p0}, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->invalidate()V

    .line 2373604
    invoke-virtual {p0}, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->requestLayout()V

    .line 2373605
    :cond_0
    return-void
.end method

.method public setScreenColor(I)V
    .locals 1

    .prologue
    .line 2373598
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2373599
    invoke-virtual {p0}, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->invalidate()V

    .line 2373600
    return-void
.end method

.method public setScreenShader(Landroid/graphics/Shader;)V
    .locals 1

    .prologue
    .line 2373595
    iget-object v0, p0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 2373596
    invoke-virtual {p0}, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->invalidate()V

    .line 2373597
    return-void
.end method
