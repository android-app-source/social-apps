.class public Lcom/facebook/adspayments/view/SecurityFooterView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2349855
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2349856
    const-class v0, Lcom/facebook/adspayments/view/SecurityFooterView;

    invoke-static {v0, p0}, Lcom/facebook/adspayments/view/SecurityFooterView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2349857
    const v0, 0x7f0312de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2349858
    const v0, 0x7f0d2bfa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/adspayments/view/SecurityFooterView;->b:Landroid/widget/TextView;

    .line 2349859
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/adspayments/view/SecurityFooterView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/adspayments/view/SecurityFooterView;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, Lcom/facebook/adspayments/view/SecurityFooterView;->a:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static b(Lcom/facebook/adspayments/view/SecurityFooterView;)V
    .locals 3

    .prologue
    .line 2349851
    const-string v0, "https://m.facebook.com/payer_protection"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 2349852
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 2349853
    iget-object v1, p0, Lcom/facebook/adspayments/view/SecurityFooterView;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Lcom/facebook/adspayments/view/SecurityFooterView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2349854
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2349844
    new-instance v0, LX/GQG;

    invoke-direct {v0, p0}, LX/GQG;-><init>(Lcom/facebook/adspayments/view/SecurityFooterView;)V

    .line 2349845
    new-instance v1, LX/47x;

    invoke-virtual {p0}, Lcom/facebook/adspayments/view/SecurityFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2349846
    const v2, 0x7f0827eb

    invoke-virtual {v1, v2}, LX/47x;->a(I)LX/47x;

    .line 2349847
    const-string v2, "[[learn_more_link]]"

    invoke-virtual {p0}, Lcom/facebook/adspayments/view/SecurityFooterView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f08002c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x21

    invoke-virtual {v1, v2, v3, v0, v4}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2349848
    iget-object v0, p0, Lcom/facebook/adspayments/view/SecurityFooterView;->b:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2349849
    iget-object v0, p0, Lcom/facebook/adspayments/view/SecurityFooterView;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2349850
    return-void
.end method
