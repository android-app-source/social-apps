.class public Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/7Tk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/ADW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/widget/TextView;

.field public e:LX/7Tj;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2349785
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2349786
    invoke-direct {p0}, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->a()V

    .line 2349787
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2349805
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2349806
    invoke-direct {p0}, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->a()V

    .line 2349807
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2349802
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2349803
    invoke-direct {p0}, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->a()V

    .line 2349804
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2349796
    const-class v0, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;

    invoke-static {v0, p0}, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2349797
    const v0, 0x7f0300c2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2349798
    invoke-virtual {p0, v2}, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->setOrientation(I)V

    .line 2349799
    const v0, 0x7f0d04fe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->d:Landroid/widget/TextView;

    .line 2349800
    iget-object v0, p0, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->a:LX/7Tk;

    invoke-virtual {p0}, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, LX/7Tk;->a(Landroid/content/Context;Z)LX/7Tj;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->e:LX/7Tj;

    .line 2349801
    return-void
.end method

.method private static a(Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;LX/7Tk;LX/ADW;LX/0W9;)V
    .locals 0

    .prologue
    .line 2349795
    iput-object p1, p0, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->a:LX/7Tk;

    iput-object p2, p0, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->b:LX/ADW;

    iput-object p3, p0, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->c:LX/0W9;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;

    const-class v0, LX/7Tk;

    invoke-interface {v2, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/7Tk;

    invoke-static {v2}, LX/ADW;->a(LX/0QB;)LX/ADW;

    move-result-object v1

    check-cast v1, LX/ADW;

    invoke-static {v2}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v2

    check-cast v2, LX/0W9;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->a(Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;LX/7Tk;LX/ADW;LX/0W9;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/common/locale/Country;)V
    .locals 2

    .prologue
    .line 2349793
    iget-object v0, p0, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->c:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/common/locale/LocaleMember;->b(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2349794
    return-void
.end method

.method public final a(Lcom/facebook/common/locale/Country;LX/6zn;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V
    .locals 2

    .prologue
    .line 2349788
    invoke-virtual {p0, p1}, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->a(Lcom/facebook/common/locale/Country;)V

    .line 2349789
    iget-object v0, p0, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->e:LX/7Tj;

    new-instance v1, LX/GQD;

    invoke-direct {v1, p0, p2}, LX/GQD;-><init>(Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;LX/6zn;)V

    .line 2349790
    iput-object v1, v0, LX/7Tj;->u:LX/6u6;

    .line 2349791
    new-instance v0, LX/GQE;

    invoke-direct {v0, p0, p3}, LX/GQE;-><init>(Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2349792
    return-void
.end method
