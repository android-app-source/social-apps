.class public Lcom/facebook/adspayments/view/ChangePaymentOptionView;
.super Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2349829
    invoke-direct {p0, p1}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;-><init>(Landroid/content/Context;)V

    .line 2349830
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2349813
    invoke-direct {p0, p1, p2}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2349814
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2349815
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2349816
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2349817
    invoke-virtual {p0, p1}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->setTitle(Ljava/lang/String;)V

    .line 2349818
    invoke-virtual {p0, v0}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 2349819
    invoke-virtual {p0, v0}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->setSubtitle(Ljava/lang/String;)V

    .line 2349820
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)V
    .locals 2
    .param p1    # Lcom/facebook/payments/paymentmethods/model/PaymentOption;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2349821
    instance-of v0, p1, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    if-eqz v0, :cond_0

    .line 2349822
    check-cast p1, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    invoke-virtual {p0, p1}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->setPaymentMethod(Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V

    .line 2349823
    :goto_0
    return-void

    .line 2349824
    :cond_0
    instance-of v0, p1, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;

    if-eqz v0, :cond_1

    .line 2349825
    check-cast p1, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;

    .line 2349826
    iget-object v0, p1, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2349827
    invoke-direct {p0, v0}, Lcom/facebook/adspayments/view/ChangePaymentOptionView;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2349828
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/adspayments/view/ChangePaymentOptionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080c79

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/adspayments/view/ChangePaymentOptionView;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
