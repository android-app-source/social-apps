.class public Lcom/facebook/adspayments/model/Payment;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adspayments/model/Payment;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/payments/currency/CurrencyAmount;

.field public final e:J

.field public final f:LX/6zP;

.field public final g:Ljava/lang/String;

.field public final h:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:LX/GPV;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2348772
    new-instance v0, LX/GPU;

    invoke-direct {v0}, LX/GPU;-><init>()V

    sput-object v0, Lcom/facebook/adspayments/model/Payment;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 10

    .prologue
    .line 2348748
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    const-class v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YH;->a(Ljava/lang/String;)LX/6zP;

    move-result-object v6

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1}, LX/46R;->p(Landroid/os/Parcel;)Landroid/net/Uri;

    move-result-object v8

    const-class v0, LX/GPV;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v9

    check-cast v9, LX/GPV;

    move-object v1, p0

    invoke-direct/range {v1 .. v9}, Lcom/facebook/adspayments/model/Payment;-><init>(Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;JLX/6zP;Ljava/lang/String;Landroid/net/Uri;LX/GPV;)V

    .line 2348749
    return-void
.end method

.method public constructor <init>(Lcom/facebook/adspayments/model/Payment;)V
    .locals 10

    .prologue
    .line 2348770
    iget-object v2, p1, Lcom/facebook/adspayments/model/Payment;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/adspayments/model/Payment;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    iget-wide v4, p1, Lcom/facebook/adspayments/model/Payment;->e:J

    iget-object v6, p1, Lcom/facebook/adspayments/model/Payment;->f:LX/6zP;

    iget-object v7, p1, Lcom/facebook/adspayments/model/Payment;->g:Ljava/lang/String;

    iget-object v8, p1, Lcom/facebook/adspayments/model/Payment;->h:Landroid/net/Uri;

    iget-object v9, p1, Lcom/facebook/adspayments/model/Payment;->i:LX/GPV;

    move-object v1, p0

    invoke-direct/range {v1 .. v9}, Lcom/facebook/adspayments/model/Payment;-><init>(Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;JLX/6zP;Ljava/lang/String;Landroid/net/Uri;LX/GPV;)V

    .line 2348771
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;JLX/6zP;Ljava/lang/String;Landroid/net/Uri;LX/GPV;)V
    .locals 1
    .param p7    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2348761
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2348762
    iput-object p1, p0, Lcom/facebook/adspayments/model/Payment;->c:Ljava/lang/String;

    .line 2348763
    iput-object p2, p0, Lcom/facebook/adspayments/model/Payment;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2348764
    iput-wide p3, p0, Lcom/facebook/adspayments/model/Payment;->e:J

    .line 2348765
    iput-object p5, p0, Lcom/facebook/adspayments/model/Payment;->f:LX/6zP;

    .line 2348766
    iput-object p6, p0, Lcom/facebook/adspayments/model/Payment;->g:Ljava/lang/String;

    .line 2348767
    iput-object p7, p0, Lcom/facebook/adspayments/model/Payment;->h:Landroid/net/Uri;

    .line 2348768
    iput-object p8, p0, Lcom/facebook/adspayments/model/Payment;->i:LX/GPV;

    .line 2348769
    return-void
.end method


# virtual methods
.method public a()LX/0zA;
    .locals 6

    .prologue
    .line 2348760
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "id"

    iget-object v2, p0, Lcom/facebook/adspayments/model/Payment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "amount"

    iget-object v2, p0, Lcom/facebook/adspayments/model/Payment;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "createDate"

    new-instance v2, Ljava/util/Date;

    iget-wide v4, p0, Lcom/facebook/adspayments/model/Payment;->e:J

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "paymentOptionType"

    iget-object v2, p0, Lcom/facebook/adspayments/model/Payment;->f:LX/6zP;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "credentialId"

    iget-object v2, p0, Lcom/facebook/adspayments/model/Payment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "webRef"

    iget-object v2, p0, Lcom/facebook/adspayments/model/Payment;->h:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "status"

    iget-object v2, p0, Lcom/facebook/adspayments/model/Payment;->i:LX/GPV;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 2348759
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2348758
    invoke-virtual {p0}, Lcom/facebook/adspayments/model/Payment;->a()LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2348750
    iget-object v0, p0, Lcom/facebook/adspayments/model/Payment;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2348751
    iget-object v0, p0, Lcom/facebook/adspayments/model/Payment;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2348752
    iget-wide v0, p0, Lcom/facebook/adspayments/model/Payment;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2348753
    iget-object v0, p0, Lcom/facebook/adspayments/model/Payment;->f:LX/6zP;

    invoke-interface {v0}, LX/6LU;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2348754
    iget-object v0, p0, Lcom/facebook/adspayments/model/Payment;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2348755
    iget-object v0, p0, Lcom/facebook/adspayments/model/Payment;->h:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2348756
    iget-object v0, p0, Lcom/facebook/adspayments/model/Payment;->i:LX/GPV;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2348757
    return-void
.end method
