.class public Lcom/facebook/adspayments/model/CvvPrepayCreditCard;
.super Lcom/facebook/payments/paymentmethods/model/CreditCard;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adspayments/model/CvvPrepayCreditCard;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/50M;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/50M",
            "<",
            "Lcom/facebook/payments/currency/CurrencyAmount;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/payments/currency/CurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2348934
    new-instance v0, LX/GPT;

    invoke-direct {v0}, LX/GPT;-><init>()V

    sput-object v0, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2348935
    invoke-direct {p0, p1}, Lcom/facebook/payments/paymentmethods/model/CreditCard;-><init>(Landroid/os/Parcel;)V

    .line 2348936
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->a:Ljava/lang/String;

    .line 2348937
    invoke-static {p1}, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->a(Landroid/os/Parcel;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    invoke-static {p1}, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->a(Landroid/os/Parcel;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->a(Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;)LX/50M;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->b:LX/50M;

    .line 2348938
    invoke-static {p1}, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->a(Landroid/os/Parcel;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2348939
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/paymentmethods/model/CreditCard;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 2348932
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;-><init>(Lcom/facebook/payments/paymentmethods/model/CreditCard;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;)V

    .line 2348933
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/paymentmethods/model/CreditCard;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;)V
    .locals 1
    .param p3    # Lcom/facebook/payments/currency/CurrencyAmount;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/payments/currency/CurrencyAmount;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/payments/currency/CurrencyAmount;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2348910
    invoke-direct {p0, p1}, Lcom/facebook/payments/paymentmethods/model/CreditCard;-><init>(Lcom/facebook/payments/paymentmethods/model/CreditCard;)V

    .line 2348911
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->a:Ljava/lang/String;

    .line 2348912
    iput-object p5, p0, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2348913
    invoke-static {p3, p4}, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->a(Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;)LX/50M;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->b:LX/50M;

    .line 2348914
    return-void
.end method

.method private static a(Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;)LX/50M;
    .locals 1
    .param p0    # Lcom/facebook/payments/currency/CurrencyAmount;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/payments/currency/CurrencyAmount;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/currency/CurrencyAmount;",
            "Lcom/facebook/payments/currency/CurrencyAmount;",
            ")",
            "LX/50M",
            "<",
            "Lcom/facebook/payments/currency/CurrencyAmount;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2348931
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-static {p0, p1}, LX/50M;->a(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/50M;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/os/Parcel;)Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 1

    .prologue
    .line 2348930
    const-class v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-static {p0, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    return-object v0
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2348915
    invoke-super {p0, p1, p2}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2348916
    iget-object v0, p0, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2348917
    iget-object v0, p0, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->b:LX/50M;

    move-object v0, v0

    .line 2348918
    if-eqz v0, :cond_0

    .line 2348919
    iget-object v0, p0, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->b:LX/50M;

    move-object v0, v0

    .line 2348920
    invoke-virtual {v0}, LX/50M;->d()Ljava/lang/Comparable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    :goto_0
    move-object v0, v0

    .line 2348921
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2348922
    iget-object v0, p0, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->b:LX/50M;

    move-object v0, v0

    .line 2348923
    if-eqz v0, :cond_1

    .line 2348924
    iget-object v0, p0, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->b:LX/50M;

    move-object v0, v0

    .line 2348925
    invoke-virtual {v0}, LX/50M;->g()Ljava/lang/Comparable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    :goto_1
    move-object v0, v0

    .line 2348926
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2348927
    iget-object v0, p0, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v0, v0

    .line 2348928
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2348929
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
