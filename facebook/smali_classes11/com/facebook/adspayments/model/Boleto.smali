.class public Lcom/facebook/adspayments/model/Boleto;
.super Lcom/facebook/adspayments/model/Payment;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adspayments/model/Boleto;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2348773
    new-instance v0, LX/GPQ;

    invoke-direct {v0}, LX/GPQ;-><init>()V

    sput-object v0, Lcom/facebook/adspayments/model/Boleto;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 2348785
    sget-object v0, Lcom/facebook/adspayments/model/Payment;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adspayments/model/Payment;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, LX/46R;->p(Landroid/os/Parcel;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/adspayments/model/Boleto;-><init>(Lcom/facebook/adspayments/model/Payment;Ljava/lang/String;Landroid/net/Uri;)V

    .line 2348786
    return-void
.end method

.method public constructor <init>(Lcom/facebook/adspayments/model/Payment;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 2348780
    invoke-direct {p0, p1}, Lcom/facebook/adspayments/model/Payment;-><init>(Lcom/facebook/adspayments/model/Payment;)V

    .line 2348781
    invoke-static {p1}, Lcom/facebook/adspayments/model/Boleto;->a(Lcom/facebook/adspayments/model/Payment;)Z

    move-result v0

    const-string v1, "Invalid credential id: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2348782
    iput-object p2, p0, Lcom/facebook/adspayments/model/Boleto;->a:Ljava/lang/String;

    .line 2348783
    iput-object p3, p0, Lcom/facebook/adspayments/model/Boleto;->b:Landroid/net/Uri;

    .line 2348784
    return-void
.end method

.method public static a(Lcom/facebook/adspayments/model/Payment;)Z
    .locals 2
    .param p0    # Lcom/facebook/adspayments/model/Payment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2348787
    if-eqz p0, :cond_0

    const-string v0, "boletobancario_santander_BR"

    iget-object v1, p0, Lcom/facebook/adspayments/model/Payment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0zA;
    .locals 3

    .prologue
    .line 2348779
    invoke-super {p0}, Lcom/facebook/adspayments/model/Payment;->a()LX/0zA;

    move-result-object v0

    const-string v1, "number"

    iget-object v2, p0, Lcom/facebook/adspayments/model/Boleto;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "downloadLink"

    iget-object v2, p0, Lcom/facebook/adspayments/model/Boleto;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2348778
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2348774
    invoke-super {p0, p1, p2}, Lcom/facebook/adspayments/model/Payment;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2348775
    iget-object v0, p0, Lcom/facebook/adspayments/model/Boleto;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2348776
    iget-object v0, p0, Lcom/facebook/adspayments/model/Boleto;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2348777
    return-void
.end method
