.class public Lcom/facebook/adspayments/model/BusinessAddressDetails;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adspayments/model/BusinessAddressDetailsDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adspayments/model/BusinessAddressDetails;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCity:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "city"
    .end annotation
.end field

.field private final mCountryCode:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "country_code"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mNeighborhood:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "neighborhood"
    .end annotation
.end field

.field private final mPostalCode:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "zip"
    .end annotation
.end field

.field private final mState:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "state"
    .end annotation
.end field

.field private final mStreet1:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "street1"
    .end annotation
.end field

.field private final mStreet2:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "street2"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mStreetNumber:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "street_number"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2348877
    const-class v0, Lcom/facebook/adspayments/model/BusinessAddressDetailsDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2348876
    new-instance v0, LX/GPS;

    invoke-direct {v0}, LX/GPS;-><init>()V

    sput-object v0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2348874
    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    move-object v7, v1

    move-object v8, v1

    invoke-direct/range {v0 .. v8}, Lcom/facebook/adspayments/model/BusinessAddressDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/common/locale/Country;)V

    .line 2348875
    return-void
.end method

.method public constructor <init>(LX/0lF;)V
    .locals 9

    .prologue
    .line 2348871
    const-string v0, "street1"

    invoke-static {p1, v0}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "street_number"

    invoke-static {p1, v0}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "street2"

    invoke-static {p1, v0}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "neighborhood"

    invoke-static {p1, v0}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "city"

    invoke-static {p1, v0}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->b(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "state"

    invoke-static {p1, v0}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->b(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v0, "zip"

    invoke-static {p1, v0}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->b(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v0, "country_code"

    invoke-virtual {p1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "country_code"

    invoke-static {p1, v0}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->b(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/locale/Country;->a(Ljava/lang/String;)Lcom/facebook/common/locale/Country;

    move-result-object v8

    :goto_0
    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/facebook/adspayments/model/BusinessAddressDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/common/locale/Country;)V

    .line 2348872
    return-void

    .line 2348873
    :cond_0
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2348861
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2348862
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mStreet1:Ljava/lang/String;

    .line 2348863
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mStreetNumber:Ljava/lang/String;

    .line 2348864
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mStreet2:Ljava/lang/String;

    .line 2348865
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mNeighborhood:Ljava/lang/String;

    .line 2348866
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mCity:Ljava/lang/String;

    .line 2348867
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mState:Ljava/lang/String;

    .line 2348868
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mPostalCode:Ljava/lang/String;

    .line 2348869
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mCountryCode:Ljava/lang/String;

    .line 2348870
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2348851
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2348852
    iput-object p1, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mStreet1:Ljava/lang/String;

    .line 2348853
    iput-object p2, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mStreet2:Ljava/lang/String;

    .line 2348854
    iput-object p3, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mCity:Ljava/lang/String;

    .line 2348855
    iput-object p4, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mState:Ljava/lang/String;

    .line 2348856
    iput-object p5, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mPostalCode:Ljava/lang/String;

    .line 2348857
    iput-object p6, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mCountryCode:Ljava/lang/String;

    .line 2348858
    iput-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mStreetNumber:Ljava/lang/String;

    .line 2348859
    iput-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mNeighborhood:Ljava/lang/String;

    .line 2348860
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/common/locale/Country;)V
    .locals 1

    .prologue
    .line 2348840
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2348841
    iput-object p1, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mStreet1:Ljava/lang/String;

    .line 2348842
    iput-object p2, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mStreetNumber:Ljava/lang/String;

    .line 2348843
    iput-object p3, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mStreet2:Ljava/lang/String;

    .line 2348844
    iput-object p4, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mNeighborhood:Ljava/lang/String;

    .line 2348845
    iput-object p5, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mCity:Ljava/lang/String;

    .line 2348846
    iput-object p6, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mState:Ljava/lang/String;

    .line 2348847
    iput-object p7, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mPostalCode:Ljava/lang/String;

    .line 2348848
    if-nez p8, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mCountryCode:Ljava/lang/String;

    .line 2348849
    return-void

    .line 2348850
    :cond_0
    invoke-virtual {p8}, Lcom/facebook/common/locale/LocaleMember;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2348837
    invoke-virtual {p0, p1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2348838
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    .line 2348839
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0lF;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2348835
    invoke-static {p0, p1}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2348836
    invoke-static {}, LX/0Rj;->notNull()LX/0Rl;

    move-result-object v1

    invoke-static {v0, v1}, LX/GPq;->a(Ljava/lang/Object;LX/0Rl;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private g()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2348805
    iget-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mStreetNumber:Ljava/lang/String;

    return-object v0
.end method

.method private h()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2348878
    iget-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mStreet2:Ljava/lang/String;

    return-object v0
.end method

.method private i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2348834
    iget-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mPostalCode:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2348833
    iget-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mStreet1:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2348832
    iget-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mCity:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2348831
    iget-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mNeighborhood:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2348830
    iget-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mState:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2348829
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/facebook/common/locale/Country;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2348828
    iget-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mCountryCode:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mCountryCode:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/common/locale/Country;->a(Ljava/lang/String;)Lcom/facebook/common/locale/Country;

    move-result-object v0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2348816
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 2348817
    const-string v1, "street1"

    invoke-virtual {p0}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2348818
    const-string v1, "street_number"

    invoke-direct {p0}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2348819
    const-string v1, "street2"

    invoke-direct {p0}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2348820
    const-string v1, "neighborhood"

    invoke-virtual {p0}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2348821
    const-string v1, "city"

    invoke-virtual {p0}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2348822
    const-string v1, "state"

    invoke-virtual {p0}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2348823
    const-string v1, "zip"

    invoke-direct {p0}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2348824
    const-string v1, "country_code"

    iget-object v2, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mCountryCode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2348825
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2348826
    :catch_0
    move-exception v0

    .line 2348827
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2348815
    invoke-virtual {p0}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2348806
    iget-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mStreet1:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2348807
    iget-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mStreetNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2348808
    iget-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mStreet2:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2348809
    iget-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mNeighborhood:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2348810
    iget-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mCity:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2348811
    iget-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mState:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2348812
    iget-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mPostalCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2348813
    iget-object v0, p0, Lcom/facebook/adspayments/model/BusinessAddressDetails;->mCountryCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2348814
    return-void
.end method
