.class public Lcom/facebook/adspayments/protocol/AddPaymentCardResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adspayments/protocol/AddPaymentCardResultDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adspayments/protocol/AddPaymentCardResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCredentialId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field private final mCvvToken:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "cached_csc_token"
    .end annotation
.end field

.field private final mIsPrepayEligible:Ljava/lang/Boolean;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_prepay_eligible"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2349097
    const-class v0, Lcom/facebook/adspayments/protocol/AddPaymentCardResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2349096
    new-instance v0, LX/GPY;

    invoke-direct {v0}, LX/GPY;-><init>()V

    sput-object v0, Lcom/facebook/adspayments/protocol/AddPaymentCardResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2349094
    invoke-direct {p0, v0, v0, v0}, Lcom/facebook/adspayments/protocol/AddPaymentCardResult;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 2349095
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 2349092
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/facebook/adspayments/protocol/AddPaymentCardResult;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 2349093
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2349079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2349080
    iput-object p1, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardResult;->mCredentialId:Ljava/lang/String;

    .line 2349081
    iput-object p2, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardResult;->mIsPrepayEligible:Ljava/lang/Boolean;

    .line 2349082
    iput-object p3, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardResult;->mCvvToken:Ljava/lang/String;

    .line 2349083
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2349091
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardResult;->mCredentialId:Ljava/lang/String;

    return-object v0
.end method

.method public final b()LX/03R;
    .locals 1

    .prologue
    .line 2349090
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardResult;->mIsPrepayEligible:Ljava/lang/Boolean;

    invoke-static {v0}, LX/03R;->valueOf(Ljava/lang/Boolean;)LX/03R;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2349089
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardResult;->mCvvToken:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2349088
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2349084
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardResult;->mCredentialId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2349085
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardResult;->mIsPrepayEligible:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2349086
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardResult;->mCvvToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2349087
    return-void
.end method
