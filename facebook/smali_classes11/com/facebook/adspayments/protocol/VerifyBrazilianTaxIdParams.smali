.class public Lcom/facebook/adspayments/protocol/VerifyBrazilianTaxIdParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2349591
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2349592
    iput-object p1, p0, Lcom/facebook/adspayments/protocol/VerifyBrazilianTaxIdParams;->a:Ljava/lang/String;

    .line 2349593
    iput-object p2, p0, Lcom/facebook/adspayments/protocol/VerifyBrazilianTaxIdParams;->b:Ljava/lang/String;

    .line 2349594
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2349595
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2349596
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/VerifyBrazilianTaxIdParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2349597
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/VerifyBrazilianTaxIdParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2349598
    return-void
.end method
