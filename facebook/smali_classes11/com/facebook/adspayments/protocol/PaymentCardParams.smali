.class public abstract Lcom/facebook/adspayments/protocol/PaymentCardParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field public final a:LX/6xg;

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:I

.field public final e:Ljava/lang/String;

.field public final f:Lcom/facebook/common/locale/Country;


# direct methods
.method public constructor <init>(LX/6xg;Ljava/lang/String;IILjava/lang/String;Lcom/facebook/common/locale/Country;)V
    .locals 0

    .prologue
    .line 2349009
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2349010
    iput-object p1, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->a:LX/6xg;

    .line 2349011
    iput-object p2, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->b:Ljava/lang/String;

    .line 2349012
    iput p3, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->c:I

    .line 2349013
    iput p4, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->d:I

    .line 2349014
    iput-object p5, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->e:Ljava/lang/String;

    .line 2349015
    iput-object p6, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->f:Lcom/facebook/common/locale/Country;

    .line 2349016
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2349017
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2349018
    const-class v0, LX/6xg;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xg;

    iput-object v0, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->a:LX/6xg;

    .line 2349019
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->b:Ljava/lang/String;

    .line 2349020
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->c:I

    .line 2349021
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->d:I

    .line 2349022
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->e:Ljava/lang/String;

    .line 2349023
    const-class v0, Lcom/facebook/common/locale/Country;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    iput-object v0, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->f:Lcom/facebook/common/locale/Country;

    .line 2349024
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 2349025
    const/4 v0, 0x0

    return v0
.end method

.method public final f()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2349026
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2349027
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "payment_type"

    .line 2349028
    iget-object v3, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->a:LX/6xg;

    move-object v3, v3

    .line 2349029
    invoke-virtual {v3}, LX/6xg;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2349030
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "csc"

    .line 2349031
    iget-object v3, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2349032
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2349033
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "expiry_month"

    .line 2349034
    iget v3, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->c:I

    move v3, v3

    .line 2349035
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2349036
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "expiry_year"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "20"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2349037
    iget v4, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->d:I

    move v4, v4

    .line 2349038
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2349039
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "country_code"

    .line 2349040
    iget-object v3, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->f:Lcom/facebook/common/locale/Country;

    move-object v3, v3

    .line 2349041
    invoke-virtual {v3}, Lcom/facebook/common/locale/LocaleMember;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    .line 2349042
    iget-object v2, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->e:Ljava/lang/String;

    move-object v2, v2

    .line 2349043
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2349044
    const-string v2, "zip"

    .line 2349045
    iget-object v3, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->e:Ljava/lang/String;

    move-object v3, v3

    .line 2349046
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2349047
    :cond_0
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "billing_address"

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2349048
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2349049
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->a:LX/6xg;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2349050
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2349051
    iget v0, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2349052
    iget v0, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2349053
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2349054
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/PaymentCardParams;->f:Lcom/facebook/common/locale/Country;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2349055
    return-void
.end method
