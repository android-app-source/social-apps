.class public Lcom/facebook/adspayments/protocol/CvvPrepayData;
.super Lcom/facebook/common/util/Quartet;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/common/util/Quartet",
        "<",
        "Ljava/lang/Boolean;",
        "Lcom/facebook/payments/currency/CurrencyAmount;",
        "Lcom/facebook/payments/currency/CurrencyAmount;",
        "Lcom/facebook/payments/currency/CurrencyAmount;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adspayments/protocol/CvvPrepayData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2349126
    new-instance v0, LX/GPZ;

    invoke-direct {v0}, LX/GPZ;-><init>()V

    sput-object v0, Lcom/facebook/adspayments/protocol/CvvPrepayData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 2349124
    invoke-direct {p0, p1}, Lcom/facebook/common/util/Quartet;-><init>(Landroid/os/Parcel;)V

    .line 2349125
    return-void
.end method

.method public constructor <init>(ZLcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;)V
    .locals 1

    .prologue
    .line 2349128
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/facebook/common/util/Quartet;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2349129
    return-void
.end method


# virtual methods
.method public final c()LX/50M;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/50M",
            "<",
            "Lcom/facebook/payments/currency/CurrencyAmount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2349127
    iget-object v0, p0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Comparable;

    iget-object v1, p0, Lcom/facebook/common/util/Triplet;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Comparable;

    invoke-static {v0, v1}, LX/50M;->a(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/50M;

    move-result-object v0

    return-object v0
.end method
