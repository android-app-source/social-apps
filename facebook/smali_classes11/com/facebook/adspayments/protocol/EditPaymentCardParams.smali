.class public Lcom/facebook/adspayments/protocol/EditPaymentCardParams;
.super Lcom/facebook/adspayments/protocol/PaymentCardParams;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adspayments/protocol/EditPaymentCardParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2349155
    new-instance v0, LX/GPb;

    invoke-direct {v0}, LX/GPb;-><init>()V

    sput-object v0, Lcom/facebook/adspayments/protocol/EditPaymentCardParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2349162
    invoke-direct {p0, p1}, Lcom/facebook/adspayments/protocol/PaymentCardParams;-><init>(Landroid/os/Parcel;)V

    .line 2349163
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/protocol/EditPaymentCardParams;->a:Ljava/lang/String;

    .line 2349164
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/6xg;Ljava/lang/String;IILjava/lang/String;Lcom/facebook/common/locale/Country;)V
    .locals 7

    .prologue
    .line 2349159
    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/facebook/adspayments/protocol/PaymentCardParams;-><init>(LX/6xg;Ljava/lang/String;IILjava/lang/String;Lcom/facebook/common/locale/Country;)V

    .line 2349160
    iput-object p1, p0, Lcom/facebook/adspayments/protocol/EditPaymentCardParams;->a:Ljava/lang/String;

    .line 2349161
    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2349156
    invoke-super {p0, p1, p2}, Lcom/facebook/adspayments/protocol/PaymentCardParams;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2349157
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/EditPaymentCardParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2349158
    return-void
.end method
