.class public Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/adspayments/model/BusinessAddressDetails;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Lcom/facebook/adspayments/model/BusinessAddressDetails;

.field public final g:Ljava/lang/String;

.field public final h:Z

.field public final i:Z

.field public final j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2349533
    new-instance v0, LX/GPn;

    invoke-direct {v0}, LX/GPn;-><init>()V

    sput-object v0, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    .line 2349531
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    const-class v0, Lcom/facebook/adspayments/model/BusinessAddressDetails;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adspayments/model/BusinessAddressDetails;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adspayments/model/BusinessAddressDetails;Ljava/lang/String;)V

    .line 2349532
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/common/locale/Country;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2349529
    new-instance v0, Lcom/facebook/adspayments/model/BusinessAddressDetails;

    invoke-virtual {p2}, Lcom/facebook/common/locale/LocaleMember;->b()Ljava/lang/String;

    move-result-object v6

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    invoke-direct/range {v0 .. v6}, Lcom/facebook/adspayments/model/BusinessAddressDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1, v1, v0, v1}, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adspayments/model/BusinessAddressDetails;Ljava/lang/String;)V

    .line 2349530
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adspayments/model/BusinessAddressDetails;Ljava/lang/String;)V
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2349527
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v6, v5

    move-object v8, v7

    move-object v9, v7

    move v10, v5

    invoke-direct/range {v0 .. v10}, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adspayments/model/BusinessAddressDetails;Ljava/lang/String;ZZLjava/lang/String;Lcom/facebook/adspayments/model/BusinessAddressDetails;Ljava/lang/String;Z)V

    .line 2349528
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adspayments/model/BusinessAddressDetails;Ljava/lang/String;ZZ)V
    .locals 11

    .prologue
    .line 2349534
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    invoke-direct/range {v0 .. v10}, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adspayments/model/BusinessAddressDetails;Ljava/lang/String;ZZLjava/lang/String;Lcom/facebook/adspayments/model/BusinessAddressDetails;Ljava/lang/String;Z)V

    .line 2349535
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adspayments/model/BusinessAddressDetails;Ljava/lang/String;ZZLjava/lang/String;Lcom/facebook/adspayments/model/BusinessAddressDetails;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 2349515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2349516
    iput-object p1, p0, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->a:Ljava/lang/String;

    .line 2349517
    iput-object p2, p0, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->b:Ljava/lang/String;

    .line 2349518
    iput-object p3, p0, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->c:Lcom/facebook/adspayments/model/BusinessAddressDetails;

    .line 2349519
    iput-object p4, p0, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->d:Ljava/lang/String;

    .line 2349520
    iput-object p7, p0, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->e:Ljava/lang/String;

    .line 2349521
    iput-object p8, p0, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->f:Lcom/facebook/adspayments/model/BusinessAddressDetails;

    .line 2349522
    iput-object p9, p0, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->g:Ljava/lang/String;

    .line 2349523
    iput-boolean p5, p0, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->h:Z

    .line 2349524
    iput-boolean p6, p0, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->i:Z

    .line 2349525
    iput-boolean p10, p0, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->j:Z

    .line 2349526
    return-void
.end method

.method public static a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2349512
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2349513
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    invoke-direct {v0, p1, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2349514
    :cond_0
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2349511
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2349502
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2349503
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2349504
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2349505
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2349506
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->c:Lcom/facebook/adspayments/model/BusinessAddressDetails;

    move-object v0, v0

    .line 2349507
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2349508
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2349509
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2349510
    return-void
.end method
