.class public Lcom/facebook/adspayments/protocol/AddPaymentCardParams;
.super Lcom/facebook/adspayments/protocol/PaymentCardParams;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adspayments/protocol/AddPaymentCardParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2349069
    new-instance v0, LX/GPX;

    invoke-direct {v0}, LX/GPX;-><init>()V

    sput-object v0, Lcom/facebook/adspayments/protocol/AddPaymentCardParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2349063
    invoke-direct {p0, p1}, Lcom/facebook/adspayments/protocol/PaymentCardParams;-><init>(Landroid/os/Parcel;)V

    .line 2349064
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardParams;->a:Ljava/lang/String;

    .line 2349065
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardParams;->b:Ljava/lang/String;

    .line 2349066
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardParams;->c:Ljava/lang/String;

    .line 2349067
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardParams;->d:Z

    .line 2349068
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Lcom/facebook/common/locale/Country;Ljava/lang/String;Ljava/lang/String;LX/6xg;Z)V
    .locals 8

    .prologue
    .line 2349070
    move-object v1, p0

    move-object/from16 v2, p9

    move-object v3, p4

    move v4, p2

    move v5, p3

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/facebook/adspayments/protocol/PaymentCardParams;-><init>(LX/6xg;Ljava/lang/String;IILjava/lang/String;Lcom/facebook/common/locale/Country;)V

    .line 2349071
    iput-object p1, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardParams;->a:Ljava/lang/String;

    .line 2349072
    iput-object p7, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardParams;->b:Ljava/lang/String;

    .line 2349073
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardParams;->c:Ljava/lang/String;

    .line 2349074
    move/from16 v0, p10

    iput-boolean v0, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardParams;->d:Z

    .line 2349075
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2349062
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2349056
    invoke-super {p0, p1, p2}, Lcom/facebook/adspayments/protocol/PaymentCardParams;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2349057
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2349058
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2349059
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2349060
    iget-boolean v0, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardParams;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2349061
    return-void
.end method
