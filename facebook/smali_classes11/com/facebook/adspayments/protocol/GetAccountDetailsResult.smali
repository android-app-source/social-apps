.class public Lcom/facebook/adspayments/protocol/GetAccountDetailsResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adspayments/protocol/GetAccountDetailsResultDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adspayments/protocol/GetAccountDetailsResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mBusinessCountry:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "business_country_code"
    .end annotation
.end field

.field private final mCurrency:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "currency"
    .end annotation
.end field

.field private final mStoredBalanceStatus:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "stored_balance_status"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2349200
    const-class v0, Lcom/facebook/adspayments/protocol/GetAccountDetailsResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2349201
    new-instance v0, LX/GPd;

    invoke-direct {v0}, LX/GPd;-><init>()V

    sput-object v0, Lcom/facebook/adspayments/protocol/GetAccountDetailsResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2349202
    invoke-direct {p0, v0, v0, v0}, Lcom/facebook/adspayments/protocol/GetAccountDetailsResult;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2349203
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2349204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2349205
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/protocol/GetAccountDetailsResult;->mStoredBalanceStatus:Ljava/lang/String;

    .line 2349206
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/protocol/GetAccountDetailsResult;->mBusinessCountry:Ljava/lang/String;

    .line 2349207
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/protocol/GetAccountDetailsResult;->mCurrency:Ljava/lang/String;

    .line 2349208
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2349209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2349210
    iput-object p1, p0, Lcom/facebook/adspayments/protocol/GetAccountDetailsResult;->mStoredBalanceStatus:Ljava/lang/String;

    .line 2349211
    iput-object p2, p0, Lcom/facebook/adspayments/protocol/GetAccountDetailsResult;->mBusinessCountry:Ljava/lang/String;

    .line 2349212
    iput-object p3, p0, Lcom/facebook/adspayments/protocol/GetAccountDetailsResult;->mCurrency:Ljava/lang/String;

    .line 2349213
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2349214
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2349215
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/GetAccountDetailsResult;->mStoredBalanceStatus:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2349216
    return-void
.end method
