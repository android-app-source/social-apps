.class public Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/ADW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2347956
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2347957
    return-void
.end method

.method private a()Lcom/facebook/adspayments/analytics/PaymentsFlowContext;
    .locals 2

    .prologue
    .line 2347954
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2347955
    const-string v1, "payments_flow_context_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    .line 2347958
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 2347959
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;->a()Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    move-result-object v3

    .line 2347960
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2347961
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2347962
    const-string v4, "payment_options"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 2347963
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    .line 2347964
    sget-object v5, LX/GOh;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2347965
    :pswitch_0
    const v0, 0x7f0827c3

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2347966
    :pswitch_1
    const v0, 0x7f0827c4

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2347967
    :cond_0
    const-string v0, "country"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/facebook/common/locale/Country;

    .line 2347968
    const-string v0, "request_code"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 2347969
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/CharSequence;

    .line 2347970
    new-instance v6, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v6, v0}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 2347971
    const v0, 0x7f0827c2

    invoke-virtual {v6, v0}, LX/0ju;->a(I)LX/0ju;

    .line 2347972
    new-instance v0, LX/GOg;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/GOg;-><init>(Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;[Ljava/lang/CharSequence;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;I)V

    invoke-virtual {v6, v2, v0}, LX/0ju;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2347973
    invoke-virtual {v6}, LX/0ju;->b()LX/2EJ;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1e85fa81

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2347948
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2347949
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p1

    check-cast p1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v1}, LX/ADW;->a(LX/0QB;)LX/ADW;

    move-result-object v1

    check-cast v1, LX/ADW;

    iput-object p1, p0, Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;->m:Lcom/facebook/content/SecureContextHelper;

    iput-object v1, p0, Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;->n:LX/ADW;

    .line 2347950
    const/16 v1, 0x2b

    const v2, 0xabe9aa6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x264d90d3

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2347951
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onResume()V

    .line 2347952
    iget-object v1, p0, Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;->n:LX/ADW;

    sget-object v2, LX/ADV;->SELECT_PAYMENT_METHOD_STATE:LX/ADV;

    invoke-direct {p0}, Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;->a()Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/ADW;->a(LX/ADV;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)Lcom/facebook/adspayments/analytics/PaymentsReliabilityTransitionLogEvent;

    .line 2347953
    const/16 v1, 0x2b

    const v2, 0x2f223bef

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
