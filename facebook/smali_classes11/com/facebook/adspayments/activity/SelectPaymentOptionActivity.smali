.class public Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;
.super Lcom/facebook/adspayments/activity/AdsPaymentsActivity;
.source ""


# static fields
.field public static final K:LX/0Rl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rl",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final p:I

.field public static final q:I

.field public static final r:I

.field public static final s:I


# instance fields
.field public L:LX/GOy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:LX/70D;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:LX/GPm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private P:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/GOw;",
            ">;"
        }
    .end annotation
.end field

.field private Q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public R:Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;

.field private S:Landroid/view/View;

.field private T:Landroid/widget/ListView;

.field private U:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/6zQ;",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private V:Landroid/widget/TextView;

.field private W:Landroid/widget/ListView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2348168
    sget-object v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    sput v0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->p:I

    .line 2348169
    sget-object v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    sput v0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->q:I

    .line 2348170
    sget-object v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    sput v0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->r:I

    .line 2348171
    sget-object v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    sput v0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->s:I

    .line 2348172
    const-class v0, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;

    invoke-static {v0}, LX/0Rj;->instanceOf(Ljava/lang/Class;)LX/0Rl;

    move-result-object v0

    sput-object v0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->K:LX/0Rl;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2348173
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;-><init>()V

    .line 2348174
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2348175
    iput-object v0, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->Q:LX/0Ot;

    .line 2348176
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2348177
    const-class v0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;

    invoke-static {v0, p0, p1, p2}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Ljava/lang/Class;Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;)Landroid/content/Intent;

    move-result-object v0

    .line 2348178
    const-string v1, "show_checkout"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2348179
    return-object v0
.end method

.method private a(ILjava/lang/String;)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param

    .prologue
    .line 2348180
    invoke-virtual {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/GOo;

    invoke-direct {v1, p0, p2}, LX/GOo;-><init>(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2348181
    return-void
.end method

.method private static a(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;LX/GOy;LX/1Ck;LX/70D;LX/GPm;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;",
            "LX/GOy;",
            "LX/1Ck;",
            "LX/70D;",
            "LX/GPm;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2348182
    iput-object p1, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->L:LX/GOy;

    iput-object p2, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->M:LX/1Ck;

    iput-object p3, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->N:LX/70D;

    iput-object p4, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->O:LX/GPm;

    iput-object p5, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->Q:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;

    invoke-static {v5}, LX/GOy;->b(LX/0QB;)LX/GOy;

    move-result-object v1

    check-cast v1, LX/GOy;

    invoke-static {v5}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static {v5}, LX/70D;->a(LX/0QB;)LX/70D;

    move-result-object v3

    check-cast v3, LX/70D;

    invoke-static {v5}, LX/GPm;->b(LX/0QB;)LX/GPm;

    move-result-object v4

    check-cast v4, LX/GPm;

    const/16 v6, 0x2eb

    invoke-static {v5, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->a(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;LX/GOy;LX/1Ck;LX/70D;LX/GPm;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;Lcom/facebook/common/locale/Country;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2348183
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->H:Lcom/facebook/common/locale/Country;

    invoke-virtual {p1, v0}, Lcom/facebook/common/locale/Country;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2348184
    :goto_0
    return-void

    .line 2348185
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->v()Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->b()Ljava/lang/String;

    move-result-object v0

    .line 2348186
    sget-object v1, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;->s:Lcom/facebook/common/locale/Country;

    invoke-virtual {p1, v1}, Lcom/facebook/common/locale/Country;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "BRL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2348187
    const v0, 0x7f0827cd

    const v1, 0x7f0827ce

    const v2, 0x7f080016

    new-instance v3, LX/GOn;

    invoke-direct {v3, p0}, LX/GOn;-><init>(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;)V

    invoke-static {p0, v0, v1, v2, v3}, LX/Gza;->a(Landroid/content/Context;IIILandroid/content/DialogInterface$OnClickListener;)LX/2EJ;

    move-result-object v0

    .line 2348188
    invoke-virtual {v0, v4}, LX/2EJ;->setCanceledOnTouchOutside(Z)V

    .line 2348189
    invoke-virtual {v0, v4}, LX/2EJ;->setCancelable(Z)V

    .line 2348190
    invoke-virtual {v0}, LX/2EJ;->show()V

    goto :goto_0

    .line 2348191
    :cond_1
    invoke-static {p1, v0}, LX/GOy;->a(Lcom/facebook/common/locale/Country;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2348192
    iget-object v0, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->R:Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->setVisibility(I)V

    .line 2348193
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->F:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-static {p0, v1}, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)Landroid/content/Intent;

    move-result-object v1

    sget v2, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->r:I

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0

    .line 2348194
    :cond_2
    iput-object p1, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->H:Lcom/facebook/common/locale/Country;

    .line 2348195
    iget-object v0, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->O:LX/GPm;

    new-instance v1, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2348196
    iget-object v3, v2, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2348197
    invoke-direct {v1, v2, p1}, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;-><init>(Ljava/lang/String;Lcom/facebook/common/locale/Country;)V

    invoke-virtual {v0, v1}, LX/6u5;->c(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/GO9;

    invoke-direct {v1, p0}, LX/GO9;-><init>(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;)V

    .line 2348198
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 2348199
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2348200
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->q()V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;)V
    .locals 6

    .prologue
    .line 2348258
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->v()Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    move-result-object v0

    .line 2348259
    iget-object v2, v0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v2, v2

    .line 2348260
    new-instance v0, Lcom/facebook/common/util/Either;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v0, v3, p1, v4}, Lcom/facebook/common/util/Either;-><init>(Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object v3, v0

    .line 2348261
    iget-object v4, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->H:Lcom/facebook/common/locale/Country;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/common/util/Either;Lcom/facebook/common/locale/Country;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->c(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Landroid/content/Intent;Landroid/content/Intent;)V

    .line 2348262
    return-void
.end method

.method public static a$redex0(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2348201
    iget-object v0, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->T:Landroid/widget/ListView;

    new-instance v3, LX/GOs;

    .line 2348202
    iget-object v4, p1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->e:LX/0Px;

    move-object v4, v4

    .line 2348203
    invoke-direct {v3, p0, p0, v4}, LX/GOs;-><init>(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2348204
    iget-object v0, p1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->e:LX/0Px;

    move-object v0, v0

    .line 2348205
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2348206
    :goto_0
    iget-object v3, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->S:Landroid/view/View;

    invoke-static {v3, v0}, LX/GQ1;->a(Landroid/view/View;Z)V

    .line 2348207
    iget-object v0, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->R:Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;

    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->v()Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    move-result-object v3

    .line 2348208
    iget-object v4, v3, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->c:LX/ADX;

    move-object v3, v4

    .line 2348209
    invoke-virtual {v3}, LX/ADX;->isNUX()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->w()Z

    move-result v3

    if-nez v3, :cond_1

    :goto_1
    invoke-static {v0, v1}, LX/GQ1;->a(Landroid/view/View;Z)V

    .line 2348210
    return-void

    :cond_0
    move v0, v2

    .line 2348211
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2348212
    goto :goto_1
.end method

.method private static b(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 2

    .prologue
    .line 2348213
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "currency"

    invoke-virtual {v0, v1, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2348214
    :catch_0
    move-exception v0

    .line 2348215
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public static b(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;Lcom/facebook/payments/paymentmethods/model/PaymentOption;)V
    .locals 2

    .prologue
    .line 2348216
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->D:LX/ADW;

    const-string v1, "payments_payment_method_selected"

    invoke-virtual {p0, v1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->h(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;->a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/ADW;->a(Lcom/facebook/adspayments/analytics/PaymentsLogEvent;)V

    .line 2348217
    invoke-virtual {p0, p1}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)V

    .line 2348218
    return-void
.end method

.method public static b$redex0(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;)V
    .locals 12

    .prologue
    .line 2348219
    iget-object v0, p1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->f:LX/0Px;

    move-object v0, v0

    .line 2348220
    sget-object v1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->a:LX/0QK;

    invoke-static {v0, v1}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v0

    move-object v2, v0

    .line 2348221
    iget-object v0, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->U:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2348222
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v1, v0}, LX/GQ1;->a(Landroid/view/View;Z)V

    goto :goto_0

    .line 2348223
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->f()Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;

    move-result-object v0

    .line 2348224
    iget-object v1, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->V:Landroid/widget/TextView;

    .line 2348225
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_5

    const/4 v2, 0x1

    :goto_1
    move v1, v2

    .line 2348226
    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    .line 2348227
    iget-object v1, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->V:Landroid/widget/TextView;

    .line 2348228
    iget-object v2, v0, Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;->d:LX/0Rf;

    move-object v0, v2

    .line 2348229
    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v4

    const/4 v8, 0x0

    .line 2348230
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2348231
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    .line 2348232
    sget-object v9, LX/GPr;->a:[I

    invoke-virtual {v5}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 2348233
    const/4 v9, 0x0

    :goto_3
    move-object v5, v9

    .line 2348234
    if-eqz v5, :cond_1

    .line 2348235
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2348236
    :cond_2
    new-instance v5, Landroid/graphics/drawable/LayerDrawable;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    new-array v7, v7, [Landroid/graphics/drawable/Drawable;

    invoke-interface {v6, v7}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Landroid/graphics/drawable/Drawable;

    invoke-direct {v5, v6}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 2348237
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b1ae1

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v11, v6

    move v6, v8

    move v9, v8

    .line 2348238
    :goto_4
    invoke-virtual {v5}, Landroid/graphics/drawable/LayerDrawable;->getNumberOfLayers()I

    move-result v7

    if-ge v6, v7, :cond_3

    .line 2348239
    neg-int v7, v9

    move v10, v8

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/drawable/LayerDrawable;->setLayerInset(IIIII)V

    .line 2348240
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    add-int/2addr v7, v11

    add-int/2addr v9, v7

    .line 2348241
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 2348242
    :cond_3
    move-object v4, v5

    .line 2348243
    invoke-static {v1, v4}, LX/GPs;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;)V

    .line 2348244
    :cond_4
    iget-object v0, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->W:Landroid/widget/ListView;

    new-instance v1, LX/GOu;

    .line 2348245
    iget-object v2, p1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->f:LX/0Px;

    move-object v2, v2

    .line 2348246
    invoke-static {v2}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v2

    const-class v3, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;

    invoke-virtual {v2, v3}, LX/0wv;->a(Ljava/lang/Class;)LX/0wv;

    move-result-object v2

    invoke-virtual {v2}, LX/0wv;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v1, p0, p0, v2}, LX/GOu;-><init>(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2348247
    return-void

    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 2348248
    :pswitch_0
    const v9, 0x7f0213fe

    invoke-static {v4, v9}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    goto :goto_3

    .line 2348249
    :pswitch_1
    const v9, 0x7f021401

    invoke-static {v4, v9}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    goto :goto_3

    .line 2348250
    :pswitch_2
    const v9, 0x7f021404

    invoke-static {v4, v9}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    goto :goto_3

    .line 2348251
    :pswitch_3
    const v9, 0x7f021405

    invoke-static {v4, v9}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    goto :goto_3

    .line 2348252
    :pswitch_4
    const v9, 0x7f021417

    invoke-static {v4, v9}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static c(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)Landroid/content/Intent;
    .locals 2
    .param p0    # Lcom/facebook/payments/paymentmethods/model/PaymentOption;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2348253
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "selected_payment_method"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static d(Landroid/content/Intent;)Lcom/facebook/payments/paymentmethods/model/PaymentOption;
    .locals 1

    .prologue
    .line 2348167
    const-string v0, "selected_payment_method"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentOption;

    return-object v0
.end method

.method public static n(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;)V
    .locals 3

    .prologue
    .line 2348254
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "show_checkout"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2348255
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->o()V

    .line 2348256
    :goto_0
    return-void

    .line 2348257
    :cond_0
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->F:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->H:Lcom/facebook/common/locale/Country;

    invoke-static {p0, v1, v2}, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;)Landroid/content/Intent;

    move-result-object v1

    sget v2, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->p:I

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method

.method private o()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 2348064
    sget-object v0, LX/0ax;->u:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->v()Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    move-result-object v1

    .line 2348065
    iget-object v2, v1, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2348066
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->v()Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    move-result-object v5

    .line 2348067
    iget-wide v8, v5, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->c:J

    move-wide v6, v8

    .line 2348068
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v0, v1, v2, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2348069
    iget-object v0, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->Q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    sget v5, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->p:I

    move-object v1, p0

    move-object v4, v3

    move-object v6, p0

    invoke-virtual/range {v0 .. v6}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;ILandroid/app/Activity;)Z

    .line 2348070
    return-void
.end method

.method public static p(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;)V
    .locals 4

    .prologue
    .line 2348086
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t()V

    .line 2348087
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->D:LX/ADW;

    sget-object v1, LX/ADV;->ADD_PAYPAL_STATE:LX/ADV;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {v0, v1, v2}, LX/ADW;->a(LX/ADV;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)Lcom/facebook/adspayments/analytics/PaymentsReliabilityTransitionLogEvent;

    .line 2348088
    const-string v0, "payments_new_paypal_selected"

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->g(Ljava/lang/String;)V

    .line 2348089
    const-string v0, "add_paypal"

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->e(Ljava/lang/String;)V

    .line 2348090
    iget-object v0, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->N:LX/70D;

    invoke-virtual {v0}, LX/0QG;->a()V

    .line 2348091
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->F:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2348092
    iget-object v2, v1, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->b:LX/6xg;

    move-object v1, v2

    .line 2348093
    iget-object v2, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2348094
    iget-object v3, v2, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2348095
    invoke-static {v1, v2}, LX/GPx;->a(LX/6xg;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    sget v2, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->q:I

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2348096
    return-void
.end method

.method private q()V
    .locals 4

    .prologue
    .line 2348097
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t()V

    .line 2348098
    iget-object v0, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->P:LX/1Ck;

    sget-object v1, LX/GOw;->GET_PAYMENT_METHODS:LX/GOw;

    invoke-direct {p0}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->y()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/GOp;

    invoke-direct {v3, p0}, LX/GOp;-><init>(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2348099
    return-void
.end method

.method private w()Z
    .locals 2

    .prologue
    .line 2348100
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->H:Lcom/facebook/common/locale/Country;

    sget-object v1, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;->s:Lcom/facebook/common/locale/Country;

    invoke-virtual {v0, v1}, Lcom/facebook/common/locale/Country;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private x()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2348101
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->y()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/GOi;

    invoke-direct {v1, p0}, LX/GOi;-><init>(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;)V

    .line 2348102
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 2348103
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private y()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2348071
    iget-object v0, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->N:LX/70D;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2348072
    iget-object v2, v1, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->b:LX/6xg;

    move-object v1, v2

    .line 2348073
    invoke-static {v1}, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a(LX/6xg;)LX/70C;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2348074
    iget-object v3, v2, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2348075
    iput-object v2, v1, LX/70C;->b:Ljava/lang/String;

    .line 2348076
    move-object v1, v1

    .line 2348077
    iget-object v2, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->H:Lcom/facebook/common/locale/Country;

    .line 2348078
    iput-object v2, v1, LX/70C;->e:Lcom/facebook/common/locale/Country;

    .line 2348079
    move-object v1, v1

    .line 2348080
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->v()Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->b(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 2348081
    iput-object v2, v1, LX/70C;->d:Lorg/json/JSONObject;

    .line 2348082
    move-object v1, v1

    .line 2348083
    invoke-virtual {v1}, LX/70C;->a()Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6u3;->c(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/GOj;

    invoke-direct {v1, p0}, LX/GOj;-><init>(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;)V

    .line 2348084
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 2348085
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2348104
    const-string v0, "ads_select_payment_method"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2348105
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Landroid/os/Bundle;)V

    .line 2348106
    invoke-static {p0, p0}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2348107
    iget-object v0, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->M:LX/1Ck;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->P:LX/1Ck;

    .line 2348108
    const/4 v0, 0x0

    .line 2348109
    invoke-static {p0, v0, v0}, LX/4AN;->a(Landroid/content/Context;LX/1H6;LX/4AM;)V

    .line 2348110
    return-void
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)V
    .locals 1

    .prologue
    .line 2348111
    invoke-static {p1}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->c(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->e(Landroid/content/Intent;)V

    .line 2348112
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2348113
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->b(Landroid/os/Bundle;)V

    .line 2348114
    const v0, 0x7f0312ef

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->setContentView(I)V

    .line 2348115
    const v0, 0x7f0d0797

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->R:Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;

    .line 2348116
    const v0, 0x7f0d2c14

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->T:Landroid/widget/ListView;

    .line 2348117
    const v0, 0x7f0d2c13    # 1.8765E38f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->S:Landroid/view/View;

    .line 2348118
    const v0, 0x7f0d2c15

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->W:Landroid/widget/ListView;

    .line 2348119
    const v0, 0x7f0d04a9

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2348120
    const v1, 0x7f0d04a2

    invoke-virtual {p0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->V:Landroid/widget/TextView;

    .line 2348121
    sget-object v1, LX/6zQ;->NEW_CREDIT_CARD:LX/6zQ;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->V:Landroid/widget/TextView;

    sget-object v3, LX/6zQ;->NEW_PAYPAL:LX/6zQ;

    invoke-static {v1, v2, v3, v0}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->U:LX/0P1;

    .line 2348122
    iget-object v1, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->V:Landroid/widget/TextView;

    new-instance v2, LX/GOk;

    invoke-direct {v2, p0}, LX/GOk;-><init>(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2348123
    new-instance v1, LX/GOl;

    invoke-direct {v1, p0}, LX/GOl;-><init>(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2348124
    const v1, 0x7f021408

    .line 2348125
    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v0, v2}, LX/GPs;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;)V

    .line 2348126
    iget-object v1, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->R:Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;

    invoke-direct {p0}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->w()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, LX/GQ1;->a(Landroid/view/View;Z)V

    .line 2348127
    iget-object v0, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->R:Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->H:Lcom/facebook/common/locale/Country;

    new-instance v2, LX/GOm;

    invoke-direct {v2, p0}, LX/GOm;-><init>(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;)V

    iget-object v3, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->a(Lcom/facebook/common/locale/Country;LX/6zn;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 2348128
    const v0, 0x7f0d247b

    const-string v1, "https://m.facebook.com/payer_protection"

    invoke-direct {p0, v0, v1}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->a(ILjava/lang/String;)V

    .line 2348129
    const v0, 0x7f0d24b7

    const-string v1, "https://m.facebook.com/payments_terms"

    invoke-direct {p0, v0, v1}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->a(ILjava/lang/String;)V

    .line 2348130
    return-void

    .line 2348131
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2348132
    invoke-static {p1}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->d(Landroid/content/Intent;)Lcom/facebook/payments/paymentmethods/model/PaymentOption;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;Ljava/lang/String;)V

    .line 2348133
    return-void
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 2348134
    const v0, 0x7f0827e5

    return v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 2348135
    sget v0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->p:I

    if-ne p1, v0, :cond_2

    .line 2348136
    if-ne p2, v2, :cond_0

    .line 2348137
    const-string v0, "credential_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2348138
    invoke-virtual {p0, v2, p3}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->setResult(ILandroid/content/Intent;)V

    .line 2348139
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->finish()V

    .line 2348140
    :cond_0
    :goto_0
    return-void

    .line 2348141
    :cond_1
    invoke-static {p3}, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->d(Landroid/content/Intent;)Lcom/facebook/payments/paymentmethods/model/CreditCard;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)V

    goto :goto_0

    .line 2348142
    :cond_2
    sget v0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->s:I

    if-ne p1, v0, :cond_3

    .line 2348143
    if-ne p2, v2, :cond_0

    .line 2348144
    invoke-virtual {p0, v2, p3}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->setResult(ILandroid/content/Intent;)V

    .line 2348145
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->finish()V

    goto :goto_0

    .line 2348146
    :cond_3
    sget v0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->q:I

    if-ne p1, v0, :cond_4

    .line 2348147
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t()V

    .line 2348148
    const-string v0, "add_paypal"

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->f(Ljava/lang/String;)V

    .line 2348149
    iget-object v0, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->P:LX/1Ck;

    sget-object v1, LX/GOw;->GET_ADDED_PAYPAL:LX/GOw;

    invoke-direct {p0}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->x()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/GOv;

    invoke-direct {v3, p0}, LX/GOv;-><init>(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    .line 2348150
    :cond_4
    sget v0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->r:I

    if-ne p1, v0, :cond_9

    .line 2348151
    if-ne p2, v2, :cond_7

    .line 2348152
    const-string v0, "country"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    move-object v0, v0

    .line 2348153
    iput-object v0, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->H:Lcom/facebook/common/locale/Country;

    .line 2348154
    iget-object v1, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->R:Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;

    invoke-virtual {v1, v0}, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->a(Lcom/facebook/common/locale/Country;)V

    .line 2348155
    :cond_5
    :goto_1
    iget-object v1, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->R:Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;

    if-ne p2, v2, :cond_6

    invoke-direct {p0}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->w()Z

    move-result v0

    if-nez v0, :cond_8

    :cond_6
    const/4 v0, 0x1

    :goto_2
    invoke-static {v1, v0}, LX/GQ1;->a(Landroid/view/View;Z)V

    goto :goto_0

    .line 2348156
    :cond_7
    if-nez p2, :cond_5

    .line 2348157
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->onBackPressed()V

    goto :goto_1

    .line 2348158
    :cond_8
    const/4 v0, 0x0

    goto :goto_2

    .line 2348159
    :cond_9
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x2817c9b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2348160
    invoke-super {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->onDestroy()V

    .line 2348161
    iget-object v1, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->P:LX/1Ck;

    if-eqz v1, :cond_0

    .line 2348162
    iget-object v1, p0, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->P:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2348163
    :cond_0
    const/16 v1, 0x23

    const v2, -0x1759f80e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x48d65d54

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2348164
    invoke-super {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->onResume()V

    .line 2348165
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->q()V

    .line 2348166
    const/16 v1, 0x23

    const v2, -0x2a82f3be

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
