.class public Lcom/facebook/adspayments/activity/PaymentStatusActivity;
.super Lcom/facebook/adspayments/activity/AdsPaymentsActivity;
.source ""


# static fields
.field public static final p:LX/0Rf;


# instance fields
.field public q:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/GPi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2347639
    sget-object v0, LX/GPV;->INITED:LX/GPV;

    const/4 v1, 0x1

    new-array v1, v1, [LX/GPV;

    const/4 v2, 0x0

    sget-object v3, LX/GPV;->COMPLETED:LX/GPV;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/0RA;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)LX/0Rf;

    move-result-object v0

    sput-object v0, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->p:LX/0Rf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2347606
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;-><init>()V

    .line 2347607
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->s:Z

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;Ljava/lang/String;Lcom/facebook/payments/paymentmethods/model/PaymentOption;)Landroid/content/Intent;
    .locals 2
    .param p4    # Lcom/facebook/payments/paymentmethods/model/PaymentOption;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2347638
    const-class v0, Lcom/facebook/adspayments/activity/PaymentStatusActivity;

    invoke-static {v0, p0, p1, p2}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Ljava/lang/Class;Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "payment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "payment_option"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/adspayments/activity/PaymentStatusActivity;LX/1Ck;LX/GPi;)V
    .locals 0

    .prologue
    .line 2347637
    iput-object p1, p0, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->q:LX/1Ck;

    iput-object p2, p0, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->r:LX/GPi;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/adspayments/activity/PaymentStatusActivity;

    invoke-static {v1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-static {v1}, LX/GPi;->a(LX/0QB;)LX/GPi;

    move-result-object v1

    check-cast v1, LX/GPi;

    invoke-static {p0, v0, v1}, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->a(Lcom/facebook/adspayments/activity/PaymentStatusActivity;LX/1Ck;LX/GPi;)V

    return-void
.end method

.method private b(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/adspayments/model/Payment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2347634
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->r:LX/GPi;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2347635
    iget-object p0, v1, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    move-object v1, p0

    .line 2347636
    invoke-static {v1, p1}, Lcom/facebook/common/util/ParcelablePair;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/facebook/common/util/ParcelablePair;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6sU;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2347633
    const-string v0, "payment_status"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2347630
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Landroid/os/Bundle;)V

    .line 2347631
    invoke-static {p0, p0}, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2347632
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2347613
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->b(Landroid/os/Bundle;)V

    .line 2347614
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2347615
    const-string v1, "payment_option"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentOption;

    .line 2347616
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->s:Z

    .line 2347617
    const v0, 0x7f030f13

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->setContentView(I)V

    .line 2347618
    const v0, 0x7f0d0340

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2347619
    const v1, 0x7f0d24a9

    invoke-virtual {p0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2347620
    const v2, 0x7f0d0626

    invoke-virtual {p0, v2}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 2347621
    const v3, 0x7f0d0372

    invoke-virtual {p0, v3}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 2347622
    iget-boolean v4, p0, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->s:Z

    if-eqz v4, :cond_1

    const v4, 0x7f08280b

    :goto_1
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 2347623
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t()V

    .line 2347624
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "payment_id"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->b(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2347625
    new-instance v5, LX/GOT;

    invoke-direct {v5, p0}, LX/GOT;-><init>(Lcom/facebook/adspayments/activity/PaymentStatusActivity;)V

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2347626
    iget-object v3, p0, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->q:LX/1Ck;

    new-instance v5, LX/GOU;

    invoke-direct {v5, p0, v0, v1, v2}, LX/GOU;-><init>(Lcom/facebook/adspayments/activity/PaymentStatusActivity;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;)V

    invoke-virtual {v3, p0, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2347627
    return-void

    .line 2347628
    :cond_0
    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "boletobancario_santander_BR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 2347629
    :cond_1
    const v4, 0x7f08280a

    goto :goto_1
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 2347612
    iget-boolean v0, p0, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->s:Z

    if-eqz v0, :cond_0

    const v0, 0x7f082806

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f082805

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x7e022e01

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2347608
    invoke-super {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->onDestroy()V

    .line 2347609
    iget-object v1, p0, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->q:LX/1Ck;

    if-eqz v1, :cond_0

    .line 2347610
    iget-object v1, p0, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->q:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2347611
    :cond_0
    const/16 v1, 0x23

    const v2, 0x70485421

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
