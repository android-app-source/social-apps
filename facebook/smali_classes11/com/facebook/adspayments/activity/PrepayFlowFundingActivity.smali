.class public Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;
.super Lcom/facebook/adspayments/activity/AdsPaymentsActivity;
.source ""


# static fields
.field private static final Q:I

.field private static final R:I

.field public static final p:LX/0Rl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rl",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final q:LX/50M;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/50M",
            "<",
            "Lcom/facebook/payments/currency/CurrencyAmount;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public K:LX/70D;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/GPg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:LX/GPo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:LX/5fv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:LX/GPk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public P:LX/1Ck;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/GOf;",
            ">;"
        }
    .end annotation
.end field

.field public S:Lcom/facebook/payments/currency/CurrencyAmount;

.field private T:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/PaymentOption;",
            ">;>;"
        }
    .end annotation
.end field

.field public U:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/50M",
            "<",
            "Lcom/facebook/payments/currency/CurrencyAmount;",
            ">;>;"
        }
    .end annotation
.end field

.field private V:Z

.field private W:Z

.field private X:LX/GPK;

.field private Y:Landroid/view/View;

.field private Z:Lcom/facebook/adspayments/view/ChangePaymentOptionView;

.field private aa:Lcom/facebook/common/util/Either;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/util/Either",
            "<",
            "Lcom/facebook/adspayments/model/CvvPrepayCreditCard;",
            "Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ab:Lcom/facebook/common/locale/Country;

.field private final ac:Ljava/lang/Runnable;

.field private ad:Ljava/lang/String;

.field public r:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/GPL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2347831
    sget-object v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    sput v0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->Q:I

    .line 2347832
    sget-object v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    sput v0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->R:I

    .line 2347833
    const-class v0, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;

    invoke-static {v0}, LX/0Rj;->instanceOf(Ljava/lang/Class;)LX/0Rl;

    move-result-object v0

    sput-object v0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->p:LX/0Rl;

    .line 2347834
    sget-object v0, LX/50M;->d:LX/50M;

    move-object v0, v0

    .line 2347835
    sput-object v0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->q:LX/50M;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2347771
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;-><init>()V

    .line 2347772
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->V:Z

    .line 2347773
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->W:Z

    .line 2347774
    new-instance v0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity$1;

    invoke-direct {v0, p0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity$1;-><init>(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;)V

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->ac:Ljava/lang/Runnable;

    .line 2347775
    return-void
.end method

.method public static A(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2347836
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->T:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2347837
    :goto_0
    iget-boolean v3, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->V:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->X:LX/GPK;

    invoke-virtual {v3}, LX/GP4;->a()Z

    move-result v3

    if-nez v3, :cond_1

    move v3, v1

    .line 2347838
    :goto_1
    iget-boolean v4, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->W:Z

    if-eqz v4, :cond_2

    if-nez v3, :cond_2

    if-nez v0, :cond_2

    .line 2347839
    :goto_2
    const v0, 0x7f0827e8

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->ac:Ljava/lang/Runnable;

    :goto_3
    invoke-virtual {p0, v2, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 2347840
    return-void

    :cond_0
    move v0, v2

    .line 2347841
    goto :goto_0

    :cond_1
    move v3, v2

    .line 2347842
    goto :goto_1

    :cond_2
    move v1, v2

    .line 2347843
    goto :goto_2

    .line 2347844
    :cond_3
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/adspayments/model/CvvPrepayCreditCard;Lcom/facebook/common/locale/Country;Z)Landroid/content/Intent;
    .locals 6
    .param p3    # Lcom/facebook/adspayments/model/CvvPrepayCreditCard;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2347845
    if-nez p3, :cond_0

    const/4 v3, 0x0

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/common/util/Either;Lcom/facebook/common/locale/Country;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 2347846
    :cond_0
    new-instance v0, Lcom/facebook/common/util/Either;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, p3, v1, v2}, Lcom/facebook/common/util/Either;-><init>(Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object v3, v0

    .line 2347847
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/common/util/Either;Lcom/facebook/common/locale/Country;Z)Landroid/content/Intent;
    .locals 2
    .param p3    # Lcom/facebook/common/util/Either;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/adspayments/analytics/PaymentsFlowContext;",
            "Lcom/facebook/payments/currency/CurrencyAmount;",
            "Lcom/facebook/common/util/Either",
            "<",
            "Lcom/facebook/adspayments/model/CvvPrepayCreditCard;",
            "Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;",
            ">;",
            "Lcom/facebook/common/locale/Country;",
            "Z)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 2347848
    const-class v0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    invoke-static {v0, p0, p1, p4}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Ljava/lang/Class;Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "amount"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "payment_option"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ask_cvv"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 2347849
    invoke-static {p1}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->b(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/paymentmethods/model/PaymentOption;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/PaymentOption;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2347850
    invoke-static {p0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/common/util/concurrent/ListenableFuture;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0am",
            "<TT;>;>;)TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2347851
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    invoke-static {p0, v0}, LX/2Ck;->a(Ljava/util/concurrent/Future;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;LX/1Ck;LX/GPL;LX/70D;LX/GPg;LX/GPo;LX/5fv;LX/GPk;)V
    .locals 0

    .prologue
    .line 2347852
    iput-object p1, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->r:LX/1Ck;

    iput-object p2, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->s:LX/GPL;

    iput-object p3, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->K:LX/70D;

    iput-object p4, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->L:LX/GPg;

    iput-object p5, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->M:LX/GPo;

    iput-object p6, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->N:LX/5fv;

    iput-object p7, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->O:LX/GPk;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    invoke-static {v7}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    const-class v2, LX/GPL;

    invoke-interface {v7, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/GPL;

    invoke-static {v7}, LX/70D;->a(LX/0QB;)LX/70D;

    move-result-object v3

    check-cast v3, LX/70D;

    invoke-static {v7}, LX/GPg;->b(LX/0QB;)LX/GPg;

    move-result-object v4

    check-cast v4, LX/GPg;

    invoke-static {v7}, LX/GPo;->b(LX/0QB;)LX/GPo;

    move-result-object v5

    check-cast v5, LX/GPo;

    invoke-static {v7}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object v6

    check-cast v6, LX/5fv;

    invoke-static {v7}, LX/GPk;->a(LX/0QB;)LX/GPk;

    move-result-object v7

    check-cast v7, LX/GPk;

    invoke-static/range {v0 .. v7}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->a(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;LX/1Ck;LX/GPL;LX/70D;LX/GPg;LX/GPo;LX/5fv;LX/GPk;)V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 2347853
    invoke-static {p1}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->c(Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method

.method public static a$redex0(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2347854
    iput-object p2, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->ad:Ljava/lang/String;

    .line 2347855
    const-string v0, "altpay_flow"

    const-string v1, "payment_id"

    invoke-static {v1, p2}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->c(Ljava/lang/String;Ljava/util/Map;)V

    .line 2347856
    invoke-static {p1}, LX/GQ1;->a(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 2347857
    const-string v1, "force_in_app_browser"

    const/4 p1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    move-object v0, v1

    .line 2347858
    sget v1, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->R:I

    invoke-virtual {p0, v0, v1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Landroid/content/Intent;I)V

    .line 2347859
    return-void
.end method

.method public static a$redex0(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;Lcom/facebook/payments/currency/CurrencyAmount;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2347860
    if-eqz p1, :cond_1

    move v1, v2

    .line 2347861
    :goto_0
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->U:Lcom/google/common/util/concurrent/ListenableFuture;

    sget-object v4, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->q:LX/50M;

    invoke-static {v0, v4}, LX/2Ck;->a(Ljava/util/concurrent/Future;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/50M;

    .line 2347862
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, LX/50M;->a(Ljava/lang/Comparable;)Z

    move-result v4

    if-nez v4, :cond_2

    move v4, v2

    .line 2347863
    :goto_1
    if-eqz v1, :cond_3

    if-nez v4, :cond_3

    move v1, v2

    :goto_2
    iput-boolean v1, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->W:Z

    .line 2347864
    iget-boolean v1, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->W:Z

    if-eqz v1, :cond_0

    .line 2347865
    iput-object p1, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->S:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2347866
    :cond_0
    invoke-static {p0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->A(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;)V

    .line 2347867
    const v1, 0x7f0d2494

    invoke-virtual {p0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347868
    if-eqz v4, :cond_4

    .line 2347869
    const v4, 0x7f082802

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, LX/50M;->d()Ljava/lang/Comparable;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-virtual {v0}, LX/50M;->g()Ljava/lang/Comparable;

    move-result-object v0

    aput-object v0, v5, v2

    invoke-virtual {p0, v4, v5}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a(Ljava/lang/String;)V

    .line 2347870
    :goto_3
    return-void

    :cond_1
    move v1, v3

    .line 2347871
    goto :goto_0

    :cond_2
    move v4, v3

    .line 2347872
    goto :goto_1

    :cond_3
    move v1, v3

    .line 2347873
    goto :goto_2

    .line 2347874
    :cond_4
    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->d()V

    goto :goto_3
.end method

.method private static b(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2347875
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "payment_id"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;Lcom/facebook/payments/paymentmethods/model/PaymentOption;)V
    .locals 4
    .param p0    # Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2347876
    invoke-direct {p0, p1}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->c(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)V

    .line 2347877
    iget-object v2, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->Z:Lcom/facebook/adspayments/view/ChangePaymentOptionView;

    invoke-virtual {v2, p1}, Lcom/facebook/adspayments/view/ChangePaymentOptionView;->a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)V

    .line 2347878
    instance-of v2, p1, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    if-eqz v2, :cond_1

    check-cast p1, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    .line 2347879
    :goto_0
    if-eqz p1, :cond_2

    move v2, v0

    .line 2347880
    :goto_1
    if-eqz v2, :cond_3

    instance-of v3, p1, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;

    if-nez v3, :cond_3

    :goto_2
    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->b(Z)V

    .line 2347881
    if-eqz v2, :cond_0

    .line 2347882
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->X:LX/GPK;

    invoke-virtual {p1}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->g()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v1

    .line 2347883
    iput-object v1, v0, LX/GPK;->c:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    .line 2347884
    :cond_0
    return-void

    .line 2347885
    :cond_1
    const/4 p1, 0x0

    goto :goto_0

    :cond_2
    move v2, v1

    .line 2347886
    goto :goto_1

    :cond_3
    move v0, v1

    .line 2347887
    goto :goto_2
.end method

.method private c(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)V
    .locals 5
    .param p1    # Lcom/facebook/payments/paymentmethods/model/PaymentOption;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2347888
    if-nez p1, :cond_0

    .line 2347889
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->u()V

    .line 2347890
    sget-object v0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->q:LX/50M;

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->U:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2347891
    :goto_0
    return-void

    .line 2347892
    :cond_0
    instance-of v0, p1, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;

    .line 2347893
    iget-object v1, v0, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->b:LX/50M;

    move-object v0, v1

    .line 2347894
    if-eqz v0, :cond_1

    .line 2347895
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->u()V

    .line 2347896
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->U:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2347897
    :cond_1
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->O:LX/GPk;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2347898
    iget-object v2, v1, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2347899
    iget-object v2, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->S:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->v()Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    move-result-object v3

    .line 2347900
    iget-boolean v4, v3, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->b:Z

    move v3, v4

    .line 2347901
    invoke-virtual {v0, v1, p1, v2, v3}, LX/GPk;->a(Ljava/lang/String;Lcom/facebook/payments/paymentmethods/model/PaymentOption;Lcom/facebook/payments/currency/CurrencyAmount;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2347902
    iget-object v1, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->P:LX/1Ck;

    const/4 v2, 0x0

    new-instance v3, LX/GOa;

    invoke-direct {v3, p0}, LX/GOa;-><init>(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 2347903
    const v0, 0x7f0d2682

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adspayments/view/ChangePaymentOptionView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->Z:Lcom/facebook/adspayments/view/ChangePaymentOptionView;

    .line 2347904
    if-eqz p1, :cond_0

    .line 2347905
    const v0, 0x7f0d2683

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2347906
    const v0, 0x7f0d2681

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/GOZ;

    invoke-direct {v1, p0}, LX/GOZ;-><init>(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2347907
    :goto_0
    return-void

    .line 2347908
    :cond_0
    const v0, 0x7f0d2683

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2347909
    const v0, 0x7f0d2681

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private static c(Ljava/lang/Throwable;)Z
    .locals 2

    .prologue
    .line 2347910
    const-class v0, LX/2Oo;

    invoke-static {p0, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/2Oo;

    .line 2347911
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    const/16 v1, 0x904

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/paymentmethods/model/PaymentOption;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2347912
    iget-boolean v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->V:Z

    if-eqz v0, :cond_0

    .line 2347913
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->L:LX/GPg;

    check-cast p1, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->X:LX/GPK;

    invoke-virtual {v1}, LX/GP4;->c()Landroid/widget/EditText;

    move-result-object v1

    .line 2347914
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    move-object v1, p0

    .line 2347915
    invoke-static {p1, v1}, Lcom/facebook/common/util/ParcelablePair;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/facebook/common/util/ParcelablePair;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6sU;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2347916
    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p1, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;

    .line 2347917
    iget-object v0, p1, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2347918
    :goto_1
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static d(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2347830
    const-string v0, "payment_id"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d(Z)V
    .locals 12

    .prologue
    .line 2347919
    const v0, 0x7f0d2684

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->Y:Landroid/view/View;

    .line 2347920
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->s:LX/GPL;

    invoke-direct {p0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->z()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v1

    .line 2347921
    new-instance v4, LX/GPK;

    invoke-static {v0}, LX/6yq;->a(LX/0QB;)LX/6yq;

    move-result-object v6

    check-cast v6, LX/6yq;

    invoke-static {v0}, LX/GQC;->a(LX/0QB;)LX/GQC;

    move-result-object v7

    check-cast v7, LX/GQC;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/GOy;->b(LX/0QB;)LX/GOy;

    move-result-object v10

    check-cast v10, LX/GOy;

    invoke-static {v0}, LX/GNs;->a(LX/0QB;)LX/GNs;

    move-result-object v11

    check-cast v11, LX/GNs;

    move-object v5, v1

    invoke-direct/range {v4 .. v11}, LX/GPK;-><init>(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;LX/6yq;LX/GQC;Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;LX/GOy;LX/GNs;)V

    .line 2347922
    move-object v0, v4

    .line 2347923
    iput-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->X:LX/GPK;

    .line 2347924
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->X:LX/GPK;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->Y:Landroid/view/View;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {v0, v1, v2, v3}, LX/GP4;->a(Landroid/view/View;Landroid/view/View;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 2347925
    invoke-virtual {p0, p1}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->b(Z)V

    .line 2347926
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->X:LX/GPK;

    invoke-virtual {v0}, LX/GP4;->c()Landroid/widget/EditText;

    move-result-object v0

    new-instance v1, LX/GOV;

    invoke-direct {v1, p0}, LX/GOV;-><init>(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2347927
    return-void
.end method

.method private e(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)V
    .locals 2

    .prologue
    .line 2347721
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->P:LX/1Ck;

    sget-object v1, LX/GOf;->GET_DEFAULT_PAYMENT_METHOD:LX/GOf;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2347722
    invoke-static {p1}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->T:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2347723
    invoke-static {p0, p1}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->b(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;Lcom/facebook/payments/paymentmethods/model/PaymentOption;)V

    .line 2347724
    return-void
.end method

.method private n()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/PaymentOption;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2347725
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->o()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/GOX;

    invoke-direct {v1, p0}, LX/GOX;-><init>(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;)V

    .line 2347726
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 2347727
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private o()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2347728
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t()V

    .line 2347729
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->K:LX/70D;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2347730
    iget-object v2, v1, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->b:LX/6xg;

    move-object v1, v2

    .line 2347731
    invoke-static {v1}, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a(LX/6xg;)LX/70C;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2347732
    iget-object v3, v2, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2347733
    iput-object v2, v1, LX/70C;->b:Ljava/lang/String;

    .line 2347734
    move-object v1, v1

    .line 2347735
    iget-object v2, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->ab:Lcom/facebook/common/locale/Country;

    .line 2347736
    iput-object v2, v1, LX/70C;->e:Lcom/facebook/common/locale/Country;

    .line 2347737
    move-object v1, v1

    .line 2347738
    invoke-virtual {v1}, LX/70C;->a()Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6u3;->c(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/GOY;

    invoke-direct {v1, p0}, LX/GOY;-><init>(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;)V

    .line 2347739
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 2347740
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static p(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;)V
    .locals 4

    .prologue
    .line 2347741
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->F:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->ab:Lcom/facebook/common/locale/Country;

    const/4 v3, 0x0

    invoke-static {p0, v1, v2, v3}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;Z)Landroid/content/Intent;

    move-result-object v1

    sget v2, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->Q:I

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2347742
    return-void
.end method

.method private q()V
    .locals 3

    .prologue
    .line 2347743
    const v0, 0x7f0d267f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2347744
    iget-object v1, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->S:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v1}, Lcom/facebook/payments/currency/CurrencyAmount;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2347745
    const v0, 0x7f0d2494

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347746
    iget-object v1, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->S:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2347747
    iget-object v2, v1, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v1, v2

    .line 2347748
    sget-object v2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v1, v2}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 2347749
    iget-object v1, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->S:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2347750
    iget-object v2, v1, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v1, v2

    .line 2347751
    invoke-virtual {v1}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 2347752
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->c()V

    .line 2347753
    const/16 v1, 0x2002

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputType(I)V

    .line 2347754
    invoke-static {p0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->A(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;)V

    .line 2347755
    new-instance v1, LX/GOb;

    invoke-direct {v1, p0}, LX/GOb;-><init>(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a(Landroid/text/TextWatcher;)V

    .line 2347756
    return-void
.end method

.method public static x(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;)V
    .locals 6

    .prologue
    .line 2347757
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->S:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-static {p0, v0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->a$redex0(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;Lcom/facebook/payments/currency/CurrencyAmount;)V

    .line 2347758
    iget-boolean v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->W:Z

    if-nez v0, :cond_0

    .line 2347759
    :goto_0
    return-void

    .line 2347760
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t()V

    .line 2347761
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->y()Lcom/facebook/payments/paymentmethods/model/PaymentOption;

    move-result-object v0

    .line 2347762
    iget-object v1, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->S:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2347763
    iget-object v2, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->P:LX/1Ck;

    sget-object v3, LX/GOf;->GET_CVV_TOKEN:LX/GOf;

    invoke-direct {p0, v0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->d(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, LX/GOc;

    invoke-direct {v5, p0, v0, v1}, LX/GOc;-><init>(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;Lcom/facebook/payments/paymentmethods/model/PaymentOption;Lcom/facebook/payments/currency/CurrencyAmount;)V

    invoke-virtual {v2, v3, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method

.method private y()Lcom/facebook/payments/paymentmethods/model/PaymentOption;
    .locals 1

    .prologue
    .line 2347764
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->T:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2347765
    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result p0

    invoke-static {p0}, LX/0PB;->checkState(Z)V

    .line 2347766
    invoke-static {v0}, LX/0Vg;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object p0

    move-object v0, p0

    .line 2347767
    check-cast v0, LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentOption;

    return-object v0
.end method

.method private z()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;
    .locals 2

    .prologue
    .line 2347768
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->T:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentOption;

    .line 2347769
    instance-of v1, v0, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->g()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->UNKNOWN:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2347770
    iget-boolean v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->V:Z

    if-eqz v0, :cond_0

    const-string v0, "funding_cvv"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "funding"

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2347717
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Landroid/os/Bundle;)V

    .line 2347718
    invoke-static {p0, p0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2347719
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->r:LX/1Ck;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->P:LX/1Ck;

    .line 2347720
    return-void
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)V
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2347776
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->P:LX/1Ck;

    sget-object v1, LX/GOf;->CHARGE:LX/GOf;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->M:LX/GPo;

    iget-object v3, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2347777
    iget-object v4, v3, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2347778
    invoke-static {v3, p1, p3, p2}, Lcom/facebook/common/util/Quartet;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/facebook/common/util/Quartet;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/6sU;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/GOe;

    invoke-direct {v3, p0, p1}, LX/GOe;-><init>(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;Lcom/facebook/payments/paymentmethods/model/PaymentOption;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2347779
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2347780
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->b(Landroid/os/Bundle;)V

    .line 2347781
    if-eqz p1, :cond_0

    .line 2347782
    const-string v0, "payment_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->ad:Ljava/lang/String;

    .line 2347783
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 2347784
    const-string v0, "amount"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->S:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2347785
    const-string v0, "payment_option"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/util/Either;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->aa:Lcom/facebook/common/util/Either;

    .line 2347786
    const-string v0, "country"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->ab:Lcom/facebook/common/locale/Country;

    .line 2347787
    const-string v0, "ask_cvv"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 2347788
    const v0, 0x7f031006

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->setContentView(I)V

    .line 2347789
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->aa:Lcom/facebook/common/util/Either;

    if-eqz v0, :cond_1

    move v3, v1

    .line 2347790
    :goto_0
    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->aa:Lcom/facebook/common/util/Either;

    .line 2347791
    invoke-virtual {v0}, Lcom/facebook/common/util/Either;->get()Ljava/lang/Object;

    move-result-object p1

    move-object v0, p1

    .line 2347792
    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentOption;

    invoke-static {v0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->T:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2347793
    invoke-static {p0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->A(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;)V

    .line 2347794
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->q()V

    .line 2347795
    if-nez v3, :cond_3

    move v0, v1

    :goto_2
    invoke-direct {p0, v0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->c(Z)V

    .line 2347796
    invoke-direct {p0, v4}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->d(Z)V

    .line 2347797
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->P:LX/1Ck;

    sget-object v1, LX/GOf;->GET_DEFAULT_PAYMENT_METHOD:LX/GOf;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->T:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/GOW;

    invoke-direct {v3, p0}, LX/GOW;-><init>(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2347798
    return-void

    :cond_1
    move v3, v2

    .line 2347799
    goto :goto_0

    .line 2347800
    :cond_2
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->n()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1

    :cond_3
    move v0, v2

    .line 2347801
    goto :goto_2
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2347802
    iput-boolean p1, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->V:Z

    .line 2347803
    invoke-static {p0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->A(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;)V

    .line 2347804
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->Y:Landroid/view/View;

    invoke-static {v0, p1}, LX/GQ1;->a(Landroid/view/View;Z)V

    .line 2347805
    return-void
.end method

.method public final c(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2347806
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->D:LX/ADW;

    const-string v0, "payments_state_finish_successfully"

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->h(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v0

    invoke-static {p1}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->d(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;->q(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->T:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentOption;

    invoke-virtual {v2, v0}, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;->a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/ADW;->a(Lcom/facebook/adspayments/analytics/PaymentsLogEvent;)V

    .line 2347807
    return-void
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 2347808
    const v0, 0x7f0827e9

    return v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 2347809
    sget v0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->Q:I

    if-ne p1, v0, :cond_0

    if-ne p2, v1, :cond_0

    .line 2347810
    invoke-static {p3}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->d(Landroid/content/Intent;)Lcom/facebook/payments/paymentmethods/model/PaymentOption;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->e(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)V

    .line 2347811
    :goto_0
    return-void

    .line 2347812
    :cond_0
    sget v0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->R:I

    if-ne p1, v0, :cond_3

    .line 2347813
    const-string v0, "altpay_flow"

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->f(Ljava/lang/String;)V

    .line 2347814
    if-ne p2, v1, :cond_2

    .line 2347815
    const-string v0, "altpay_flow"

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->i(Ljava/lang/String;)V

    .line 2347816
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->ab:Lcom/facebook/common/locale/Country;

    iget-object v3, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->ad:Ljava/lang/String;

    iget-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->T:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentOption;

    invoke-static {p0, v1, v2, v3, v0}, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;Ljava/lang/String;Lcom/facebook/payments/paymentmethods/model/PaymentOption;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->ad:Ljava/lang/String;

    invoke-static {v1}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->b(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Landroid/content/Intent;Landroid/content/Intent;)V

    goto :goto_0

    .line 2347817
    :cond_2
    if-nez p2, :cond_1

    .line 2347818
    const-string v0, "altpay_flow"

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->c(Ljava/lang/String;)V

    goto :goto_1

    .line 2347819
    :cond_3
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x5ac540d3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2347820
    invoke-super {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->onDestroy()V

    .line 2347821
    iget-object v1, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->P:LX/1Ck;

    if-eqz v1, :cond_0

    .line 2347822
    iget-object v1, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->P:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2347823
    :cond_0
    const/16 v1, 0x23

    const v2, 0x105fc93b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2347824
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 2347825
    const-string v0, "payment_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->ad:Ljava/lang/String;

    .line 2347826
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2347827
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2347828
    const-string v0, "payment_id"

    iget-object v1, p0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->ad:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2347829
    return-void
.end method
