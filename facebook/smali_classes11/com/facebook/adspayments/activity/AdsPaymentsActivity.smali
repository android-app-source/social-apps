.class public abstract Lcom/facebook/adspayments/activity/AdsPaymentsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/67U;


# static fields
.field public static t:Ljava/util/concurrent/atomic/AtomicInteger;

.field public static final u:I

.field public static final v:I

.field public static final w:I

.field public static final x:I

.field public static final y:I


# instance fields
.field public A:LX/63V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/67X;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/ADW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

.field public H:Lcom/facebook/common/locale/Country;

.field public I:Z

.field public J:[Lcom/facebook/widget/titlebar/TitleBarButtonSpec;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:LX/0h5;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Landroid/content/Intent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final r:Ljava/lang/Object;

.field private s:LX/4BY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "loadingIndicatorLock"
    .end annotation
.end field

.field public z:LX/GNs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2346779
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 2346780
    sput-object v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    sput v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->u:I

    .line 2346781
    sget-object v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    sput v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->v:I

    .line 2346782
    sget-object v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    sput v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->w:I

    .line 2346783
    sget-object v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    sput v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->x:I

    .line 2346784
    sget-object v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    sput v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->y:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2346785
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2346786
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->r:Ljava/lang/Object;

    .line 2346787
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;)Landroid/content/Intent;
    .locals 2
    .param p3    # Lcom/facebook/common/locale/Country;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/adspayments/activity/AdsPaymentsActivity;",
            ">;",
            "Landroid/content/Context;",
            "Lcom/facebook/adspayments/analytics/PaymentsFlowContext;",
            "Lcom/facebook/common/locale/Country;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 2346788
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1, p0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2346789
    const-string v1, "payments_flow_context_key"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2346790
    const-string v1, "country"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2346791
    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2346792
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->D:LX/ADW;

    invoke-static {p0, p1, p2}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->b(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/ADW;->a(Lcom/facebook/adspayments/analytics/PaymentsLogEvent;)V

    .line 2346793
    return-void
.end method

.method public static b(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;
    .locals 2

    .prologue
    .line 2346794
    new-instance v0, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-direct {v0, p1, v1}, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;-><init>(Ljava/lang/String;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 2346795
    const-string v1, "ui_state"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2346796
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->H:Lcom/facebook/common/locale/Country;

    .line 2346797
    const-string p1, "billing_country"

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/common/locale/LocaleMember;->b()Ljava/lang/String;

    move-result-object p0

    :goto_0
    invoke-virtual {v0, p1, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2346798
    return-object v0

    .line 2346799
    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method private n()V
    .locals 7

    .prologue
    .line 2346800
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "payments_flow_context_key"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2346801
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "payments_flow_context_key"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2346802
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "country"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2346803
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "country"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->H:Lcom/facebook/common/locale/Country;

    .line 2346804
    :cond_0
    return-void

    .line 2346805
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ad_account_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "flow_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2346806
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ad_account_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2346807
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "flow_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2346808
    new-instance v0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    sget-object v3, LX/6xj;->PICKER_SCREEN:LX/6xj;

    new-instance v4, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v5, "USD"

    sget-object v6, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-direct {v4, v5, v6}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    const/4 v5, 0x0

    sget-object v6, LX/ADX;->NEW_USER:LX/ADX;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;-><init>(Ljava/lang/String;Ljava/lang/String;LX/6xj;Lcom/facebook/payments/currency/CurrencyAmount;ZLX/ADX;)V

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    goto :goto_0

    .line 2346809
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Need either \'%s\' or (\'%s\' and \'%s\')"

    const-string v2, "payments_flow_context_key"

    const-string v3, "ad_account_id"

    const-string v4, "flow_name"

    invoke-static {v1, v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private o()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2346810
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->e(Ljava/lang/String;)V

    .line 2346811
    return-void
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public final a(IZ)V
    .locals 2

    .prologue
    .line 2346812
    iget-boolean v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->I:Z

    if-nez v0, :cond_0

    .line 2346813
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->J:[Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2346814
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->J:[Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    array-length v0, v0

    invoke-static {p1, v0}, LX/0PB;->checkElementIndex(II)I

    .line 2346815
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->p:LX/0h5;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2346816
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->J:[Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    aget-object v0, v0, p1

    .line 2346817
    iput-boolean p2, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    .line 2346818
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->p:LX/0h5;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->J:[Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-static {v1}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2346819
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 2346820
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "http"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2346821
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->F:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, p1, p2, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2346822
    :goto_0
    return-void

    .line 2346823
    :cond_0
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->F:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, p1, p2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2346824
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->q:Landroid/content/Intent;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Another result is already pending: %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->q:Landroid/content/Intent;

    invoke-static {v4}, LX/GQ1;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2346825
    iput-object p2, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->q:Landroid/content/Intent;

    .line 2346826
    sget v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->v:I

    invoke-virtual {p0, p1, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Landroid/content/Intent;I)V

    .line 2346827
    return-void

    :cond_0
    move v0, v2

    .line 2346828
    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2346829
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 2346830
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v1, p0

    check-cast v1, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;

    invoke-static {v0}, LX/GNs;->a(LX/0QB;)LX/GNs;

    move-result-object v2

    check-cast v2, LX/GNs;

    invoke-static {v0}, LX/63V;->a(LX/0QB;)LX/63V;

    move-result-object v3

    check-cast v3, LX/63V;

    const/16 v4, 0x1677

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/ADW;->a(LX/0QB;)LX/ADW;

    move-result-object v6

    check-cast v6, LX/ADW;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v2, v1, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->z:LX/GNs;

    iput-object v3, v1, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->A:LX/63V;

    iput-object v4, v1, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->B:LX/0Ot;

    iput-object v5, v1, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->C:LX/03V;

    iput-object v6, v1, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->D:LX/ADW;

    iput-object p1, v1, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->E:Ljava/util/concurrent/ExecutorService;

    iput-object v0, v1, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->F:Lcom/facebook/content/SecureContextHelper;

    .line 2346831
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->A:LX/63V;

    invoke-virtual {v0}, LX/63V;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->I:Z

    .line 2346832
    iget-boolean v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->I:Z

    if-eqz v0, :cond_0

    .line 2346833
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T2;

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    .line 2346834
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2346835
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->D:LX/ADW;

    const-string v1, "payments_state_finish_successfully"

    invoke-static {p0, v1, p2}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->b(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;->a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/ADW;->a(Lcom/facebook/adspayments/analytics/PaymentsLogEvent;)V

    .line 2346836
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2346837
    const v0, 0x7f08002a

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 2346838
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 5
    .param p2    # Ljava/lang/Runnable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2346839
    if-nez p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2346840
    new-instance v3, LX/GO8;

    invoke-direct {v3, p0, v0}, LX/GO8;-><init>(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;Ljava/lang/Runnable;)V

    new-array v4, v1, [Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object p2

    .line 2346841
    iput v2, p2, LX/108;->a:I

    .line 2346842
    move-object p2, p2

    .line 2346843
    iput-boolean v1, p2, LX/108;->q:Z

    .line 2346844
    move-object p2, p2

    .line 2346845
    iput-object p1, p2, LX/108;->g:Ljava/lang/String;

    .line 2346846
    move-object p2, p2

    .line 2346847
    if-eqz v0, :cond_2

    .line 2346848
    :goto_1
    iput-boolean v1, p2, LX/108;->d:Z

    .line 2346849
    move-object v1, p2

    .line 2346850
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    aput-object v1, v4, v2

    .line 2346851
    iget-boolean v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->I:Z

    if-nez v1, :cond_0

    .line 2346852
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->p:LX/0h5;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2346853
    iput-object v4, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->J:[Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2346854
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->p:LX/0h5;

    invoke-static {v4}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2346855
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->p:LX/0h5;

    invoke-interface {v1, v3}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2346856
    :cond_0
    return-void

    .line 2346857
    :cond_1
    new-instance v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity$1;

    invoke-direct {v0, p0, p2}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity$1;-><init>(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_2
    move v1, v2

    .line 2346858
    goto :goto_1
.end method

.method public final a(S)Z
    .locals 1

    .prologue
    .line 2346859
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->z:LX/GNs;

    invoke-virtual {v0, p1}, LX/GNs;->a(S)Z

    move-result v0

    return v0
.end method

.method public final b()LX/3u1;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2346860
    iget-boolean v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->I:Z

    if-eqz v0, :cond_0

    .line 2346861
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67X;

    invoke-virtual {v0}, LX/67X;->g()LX/3u1;

    move-result-object v0

    .line 2346862
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2346863
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->D:LX/ADW;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {v0, p1, v1}, LX/ADW;->a(Ljava/lang/Throwable;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)Lcom/facebook/adspayments/analytics/PaymentsReliabilityErrorLogEvent;

    .line 2346864
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->D:LX/ADW;

    const-string v0, "payments_state_error"

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->h(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;->a(Ljava/lang/Throwable;)Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;

    move-result-object v0

    check-cast v0, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    invoke-virtual {v1, v0}, LX/ADW;->a(Lcom/facebook/adspayments/analytics/PaymentsLogEvent;)V

    .line 2346865
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 2346866
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->C:LX/03V;

    invoke-virtual {v1, v0, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2346867
    const-string v1, "Error"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2346868
    return-void
.end method

.method public c(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2346869
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->i(Ljava/lang/String;)V

    .line 2346870
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2346871
    const-string v0, "payments_action_back"

    invoke-direct {p0, v0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2346872
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 2346873
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->D:LX/ADW;

    const-string v2, "payments_state_appear"

    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v2, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->b(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;->b(Ljava/util/Map;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/ADW;->a(Lcom/facebook/adspayments/analytics/PaymentsLogEvent;)V

    .line 2346874
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->D:LX/ADW;

    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {v1, v0, v2}, LX/ADW;->b(Ljava/lang/String;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)Lcom/facebook/adspayments/analytics/PaymentsReliabilityTransitionLogEvent;

    .line 2346875
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2346777
    const-string v0, "payments_action_continue"

    invoke-direct {p0, v0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2346778
    return-void
.end method

.method public final e(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2346876
    invoke-virtual {p0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->c(Landroid/content/Intent;)V

    .line 2346877
    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->setResult(ILandroid/content/Intent;)V

    .line 2346878
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 2346879
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2346697
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 2346698
    invoke-virtual {p0, p1, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->c(Ljava/lang/String;Ljava/util/Map;)V

    .line 2346699
    return-void
.end method

.method public final f(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2346700
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->F:Lcom/facebook/content/SecureContextHelper;

    sget v1, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->u:I

    invoke-interface {v0, p1, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2346701
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2346702
    const-string v0, "payments_state_disappear"

    invoke-direct {p0, v0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2346703
    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2346704
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2346705
    return-void
.end method

.method public h(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;
    .locals 1

    .prologue
    .line 2346706
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->b(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v0

    return-object v0
.end method

.method public final i(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2346707
    const-string v0, "payments_state_finish_successfully"

    invoke-direct {p0, v0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2346708
    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2346709
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract m()I
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 2346710
    sget v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->u:I

    if-eq p1, v0, :cond_0

    const/16 v0, 0x12d

    if-ne p1, v0, :cond_2

    .line 2346711
    :cond_0
    if-ne p2, v1, :cond_1

    .line 2346712
    invoke-virtual {p0, p3}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->e(Landroid/content/Intent;)V

    .line 2346713
    :cond_1
    :goto_0
    return-void

    .line 2346714
    :cond_2
    sget v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->v:I

    if-ne p1, v0, :cond_4

    .line 2346715
    if-ne p2, v1, :cond_3

    .line 2346716
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->q:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->e(Landroid/content/Intent;)V

    goto :goto_0

    .line 2346717
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->q:Landroid/content/Intent;

    goto :goto_0

    .line 2346718
    :cond_4
    sget v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->x:I

    if-ne p1, v0, :cond_5

    .line 2346719
    const-string v0, "result code: %s\nresult: %s"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p3}, LX/GQ1;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2346720
    new-instance v1, Lcom/facebook/adspayments/utils/PaymentUiUtil$4;

    invoke-direct {v1, p0, v0}, Lcom/facebook/adspayments/utils/PaymentUiUtil$4;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2346721
    goto :goto_0

    .line 2346722
    :cond_5
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 2346723
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->r()V

    .line 2346724
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2346725
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 2346726
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_2

    .line 2346727
    invoke-virtual {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 2346728
    const/4 v0, 0x1

    .line 2346729
    :goto_0
    move v0, v0

    .line 2346730
    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x2b9ab6eb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2346731
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->f(Ljava/lang/String;)V

    .line 2346732
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2346733
    const/16 v1, 0x23

    const v2, -0x34125e0d    # -3.1146982E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2346734
    const-string v0, "activity_result"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->q:Landroid/content/Intent;

    .line 2346735
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 2346736
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0xcb55870

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2346737
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2346738
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->o()V

    .line 2346739
    const/16 v1, 0x23

    const v2, 0x484cd618    # 209752.38f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2346740
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->u()V

    .line 2346741
    const-string v0, "activity_result"

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->q:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2346742
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2346743
    return-void
.end method

.method public r()V
    .locals 1

    .prologue
    .line 2346744
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->c(Ljava/lang/String;)V

    .line 2346745
    return-void
.end method

.method public final s()V
    .locals 1

    .prologue
    .line 2346746
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->d(Ljava/lang/String;)V

    .line 2346747
    return-void
.end method

.method public final setContentView(I)V
    .locals 4

    .prologue
    .line 2346748
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->setContentView(I)V

    .line 2346749
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->n()V

    .line 2346750
    iget-boolean v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->I:Z

    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67X;

    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->m()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2346751
    if-eqz v1, :cond_2

    .line 2346752
    invoke-virtual {v0}, LX/67X;->g()LX/3u1;

    move-result-object v3

    .line 2346753
    new-instance p1, LX/63L;

    invoke-direct {p1, p0, v3}, LX/63L;-><init>(LX/0ew;LX/3u1;)V

    .line 2346754
    invoke-virtual {p1, v2}, LX/63L;->setTitle(Ljava/lang/String;)V

    .line 2346755
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, LX/63L;->setHasBackButton(Z)V

    .line 2346756
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->I:Z

    if-nez v0, :cond_1

    .line 2346757
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->p:LX/0h5;

    .line 2346758
    :cond_1
    return-void

    .line 2346759
    :cond_2
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2346760
    const v3, 0x7f0d00bc

    invoke-virtual {p0, v3}, Lcom/facebook/base/activity/FbFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, LX/0h5;

    .line 2346761
    invoke-interface {v3, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2346762
    new-instance p1, LX/GPz;

    invoke-direct {p1, p0}, LX/GPz;-><init>(Lcom/facebook/base/activity/FbFragmentActivity;)V

    invoke-interface {v3, p1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final t()V
    .locals 4

    .prologue
    .line 2346763
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 2346764
    :try_start_0
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->s:LX/4BY;

    if-eqz v0, :cond_0

    .line 2346765
    monitor-exit v1

    .line 2346766
    :goto_0
    return-void

    .line 2346767
    :cond_0
    const/4 v0, 0x0

    const v2, 0x7f080024

    invoke-virtual {p0, v2}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {p0, v0, v2, v3}, LX/4BY;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)LX/4BY;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->s:LX/4BY;

    .line 2346768
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->setRequestedOrientation(I)V

    .line 2346769
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final u()V
    .locals 2

    .prologue
    .line 2346770
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 2346771
    :try_start_0
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->s:LX/4BY;

    if-eqz v0, :cond_0

    .line 2346772
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->s:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2346773
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->s:LX/4BY;

    .line 2346774
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->setRequestedOrientation(I)V

    .line 2346775
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final v()Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;
    .locals 2

    .prologue
    .line 2346776
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    const-class v1, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    invoke-static {v0, v1}, LX/GPq;->a(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    return-object v0
.end method
