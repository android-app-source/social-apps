.class public Lcom/facebook/adspayments/activity/AddressActivity;
.super Lcom/facebook/adspayments/activity/AdsPaymentsActivity;
.source ""


# static fields
.field private static final am:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final r:I


# instance fields
.field public K:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public L:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field private M:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public N:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field private O:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field private P:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field private Q:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field private R:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field private S:Landroid/widget/Spinner;

.field private T:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field private U:Lcom/facebook/resources/ui/FbTextView;

.field private V:Landroid/widget/RadioGroup;

.field private W:Lcom/facebook/resources/ui/FbTextView;

.field private X:Landroid/widget/RadioGroup;

.field private Y:LX/GO6;

.field private Z:Lcom/facebook/common/locale/Country;

.field private aa:Lcom/facebook/payments/currency/CurrencyAmount;

.field private ab:Lcom/facebook/adspayments/model/CvvPrepayCreditCard;

.field private ac:Z

.field private ad:Ljava/lang/String;

.field private ae:Ljava/lang/String;

.field private af:Ljava/lang/String;

.field private ag:Ljava/lang/String;

.field private ah:Ljava/lang/String;

.field private ai:Ljava/lang/String;

.field private aj:Ljava/lang/String;

.field private ak:Ljava/lang/String;

.field private al:Ljava/lang/Boolean;

.field private final an:Ljava/lang/Runnable;

.field public p:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/GPk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 15

    .prologue
    .line 2347269
    sget-object v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    sput v0, Lcom/facebook/adspayments/activity/AddressActivity;->r:I

    .line 2347270
    const-string v0, "AT"

    const-string v1, "BE"

    const-string v2, "BU"

    const-string v3, "CY"

    const-string v4, "CZ"

    const-string v5, "DE"

    const-string v6, "DK"

    const-string v7, "EE"

    const-string v8, "ES"

    const-string v9, "FI"

    const-string v10, "FR"

    const-string v11, "GB"

    const/16 v12, 0x10

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "EL"

    aput-object v14, v12, v13

    const/4 v13, 0x1

    const-string v14, "HR"

    aput-object v14, v12, v13

    const/4 v13, 0x2

    const-string v14, "HU"

    aput-object v14, v12, v13

    const/4 v13, 0x3

    const-string v14, "IR"

    aput-object v14, v12, v13

    const/4 v13, 0x4

    const-string v14, "IT"

    aput-object v14, v12, v13

    const/4 v13, 0x5

    const-string v14, "LT"

    aput-object v14, v12, v13

    const/4 v13, 0x6

    const-string v14, "LU"

    aput-object v14, v12, v13

    const/4 v13, 0x7

    const-string v14, "LV"

    aput-object v14, v12, v13

    const/16 v13, 0x8

    const-string v14, "MT"

    aput-object v14, v12, v13

    const/16 v13, 0x9

    const-string v14, "NL"

    aput-object v14, v12, v13

    const/16 v13, 0xa

    const-string v14, "PL"

    aput-object v14, v12, v13

    const/16 v13, 0xb

    const-string v14, "PT"

    aput-object v14, v12, v13

    const/16 v13, 0xc

    const-string v14, "RO"

    aput-object v14, v12, v13

    const/16 v13, 0xd

    const-string v14, "SE"

    aput-object v14, v12, v13

    const/16 v13, 0xe

    const-string v14, "SI"

    aput-object v14, v12, v13

    const/16 v13, 0xf

    const-string v14, "SK"

    aput-object v14, v12, v13

    invoke-static/range {v0 .. v12}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/adspayments/activity/AddressActivity;->am:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2347271
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;-><init>()V

    .line 2347272
    new-instance v0, Lcom/facebook/adspayments/activity/AddressActivity$1;

    invoke-direct {v0, p0}, Lcom/facebook/adspayments/activity/AddressActivity$1;-><init>(Lcom/facebook/adspayments/activity/AddressActivity;)V

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->an:Ljava/lang/Runnable;

    .line 2347273
    return-void
.end method

.method private A()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2347274
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AddressActivity;->S:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AddressActivity;->T:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static B(Lcom/facebook/adspayments/activity/AddressActivity;)Z
    .locals 2

    .prologue
    .line 2347275
    sget-object v0, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AddressActivity;->L:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

.method public static C(Lcom/facebook/adspayments/activity/AddressActivity;)Z
    .locals 1

    .prologue
    .line 2347276
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->K:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static D(Lcom/facebook/adspayments/activity/AddressActivity;)V
    .locals 2

    .prologue
    .line 2347277
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->Y:LX/GO6;

    sget-object v1, LX/GO6;->BUSINESS_ADDRESS:LX/GO6;

    invoke-virtual {v0, v1}, LX/GO6;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/facebook/adspayments/activity/AddressActivity;->C(Lcom/facebook/adspayments/activity/AddressActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/facebook/adspayments/activity/AddressActivity;->B(Lcom/facebook/adspayments/activity/AddressActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2347278
    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->an:Ljava/lang/Runnable;

    :goto_1
    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Ljava/lang/Runnable;)V

    .line 2347279
    return-void

    .line 2347280
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2347281
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;LX/GO6;Lcom/facebook/common/locale/Country;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2347282
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/adspayments/activity/AddressActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2347283
    const-string v1, "payments_flow_context_key"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2347284
    const-string v1, "address_activity_state"

    invoke-virtual {p2}, LX/GO6;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2347285
    const-string v1, "address_country"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2347286
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;LX/GO6;Lcom/facebook/common/locale/Country;Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/adspayments/model/CvvPrepayCreditCard;Z)Landroid/content/Intent;
    .locals 2
    .param p5    # Lcom/facebook/adspayments/model/CvvPrepayCreditCard;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2347287
    invoke-static {p0, p1, p2, p3}, Lcom/facebook/adspayments/activity/AddressActivity;->a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;LX/GO6;Lcom/facebook/common/locale/Country;)Landroid/content/Intent;

    move-result-object v0

    .line 2347288
    const-string v1, "amount"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2347289
    const-string v1, "card"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2347290
    const-string v1, "ask_cvv"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2347291
    return-object v0
.end method

.method private static a(Lcom/facebook/adspayments/activity/AddressActivity;LX/1Ck;LX/GPk;)V
    .locals 0

    .prologue
    .line 2347292
    iput-object p1, p0, Lcom/facebook/adspayments/activity/AddressActivity;->p:LX/1Ck;

    iput-object p2, p0, Lcom/facebook/adspayments/activity/AddressActivity;->q:LX/GPk;

    return-void
.end method

.method private a(Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2347293
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t()V

    .line 2347294
    new-instance v0, LX/GO5;

    invoke-direct {v0, p0}, LX/GO5;-><init>(Lcom/facebook/adspayments/activity/AddressActivity;)V

    .line 2347295
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AddressActivity;->p:LX/1Ck;

    sget-object v2, LX/GO7;->SEND_BUSINESS_ADDRESS:LX/GO7;

    invoke-virtual {v1, v2, p1, v0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2347296
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/adspayments/activity/AddressActivity;

    invoke-static {v1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-static {v1}, LX/GPk;->a(LX/0QB;)LX/GPk;

    move-result-object v1

    check-cast v1, LX/GPk;

    invoke-static {p0, v0, v1}, Lcom/facebook/adspayments/activity/AddressActivity;->a(Lcom/facebook/adspayments/activity/AddressActivity;LX/1Ck;LX/GPk;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2347297
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "Need %s to start the activity"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2347298
    return-void
.end method

.method private n()Lcom/facebook/common/locale/Country;
    .locals 2

    .prologue
    .line 2347143
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "address_country"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    .line 2347144
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "address_country"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/locale/Country;->a(Ljava/lang/String;)Lcom/facebook/common/locale/Country;

    move-result-object v0

    goto :goto_0
.end method

.method private o()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 2347145
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->Y:LX/GO6;

    sget-object v1, LX/GO6;->BUSINESS_ADDRESS:LX/GO6;

    invoke-virtual {v0, v1}, LX/GO6;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2347146
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->s:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0827ef

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2347147
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->K:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v1, 0x7f0827f1

    invoke-virtual {p0, v1}, Lcom/facebook/adspayments/activity/AddressActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 2347148
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->L:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setVisibility(I)V

    .line 2347149
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->S:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 2347150
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->T:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setVisibility(I)V

    .line 2347151
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->U:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2347152
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->V:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 2347153
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->W:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2347154
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->X:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v3}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 2347155
    :goto_0
    return-void

    .line 2347156
    :cond_0
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->s:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0827f0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2347157
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->K:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v1, 0x7f0827f2

    invoke-virtual {p0, v1}, Lcom/facebook/adspayments/activity/AddressActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 2347158
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->L:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setVisibility(I)V

    .line 2347159
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->S:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 2347160
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->T:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setVisibility(I)V

    .line 2347161
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->U:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2347162
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->V:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v3}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 2347163
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->W:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2347164
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->X:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public static p(Lcom/facebook/adspayments/activity/AddressActivity;)V
    .locals 3

    .prologue
    .line 2347165
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    sget-object v1, LX/GO6;->CLIENT_ADDRESS:LX/GO6;

    const-string v2, "FR"

    invoke-static {v2}, Lcom/facebook/common/locale/Country;->a(Ljava/lang/String;)Lcom/facebook/common/locale/Country;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/adspayments/activity/AddressActivity;->a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;LX/GO6;Lcom/facebook/common/locale/Country;)Landroid/content/Intent;

    move-result-object v0

    .line 2347166
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->F:Lcom/facebook/content/SecureContextHelper;

    sget v2, Lcom/facebook/adspayments/activity/AddressActivity;->r:I

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2347167
    return-void
.end method

.method public static q(Lcom/facebook/adspayments/activity/AddressActivity;)V
    .locals 6

    .prologue
    .line 2347168
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AddressActivity;->aa:Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v3, p0, Lcom/facebook/adspayments/activity/AddressActivity;->ab:Lcom/facebook/adspayments/model/CvvPrepayCreditCard;

    iget-object v4, p0, Lcom/facebook/adspayments/activity/AddressActivity;->Z:Lcom/facebook/common/locale/Country;

    iget-boolean v5, p0, Lcom/facebook/adspayments/activity/AddressActivity;->ac:Z

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/adspayments/model/CvvPrepayCreditCard;Lcom/facebook/common/locale/Country;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->f(Landroid/content/Intent;)V

    .line 2347169
    return-void
.end method

.method private w()V
    .locals 4

    .prologue
    .line 2347170
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2347171
    const-string v0, "client_name"

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AddressActivity;->K:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v2

    .line 2347172
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2347173
    const-string v0, "client_email"

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AddressActivity;->L:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v2

    .line 2347174
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2347175
    const-string v0, "client_address"

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AddressActivity;->M:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v2

    .line 2347176
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2347177
    const-string v0, "client_apt"

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AddressActivity;->N:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v2

    .line 2347178
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2347179
    const-string v0, "client_city"

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AddressActivity;->O:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v2

    .line 2347180
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2347181
    const-string v0, "client_postal_code"

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AddressActivity;->P:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v2

    .line 2347182
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2347183
    const-string v0, "client_state"

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AddressActivity;->Q:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v2

    .line 2347184
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2347185
    const-string v0, "client_country"

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AddressActivity;->Z:Lcom/facebook/common/locale/Country;

    invoke-virtual {v2}, Lcom/facebook/common/locale/LocaleMember;->b()Ljava/lang/String;

    move-result-object v2

    .line 2347186
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2347187
    const-string v2, "is_client_paying_for_invoices"

    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->X:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const v3, 0x7f0d04cb

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2347188
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2347189
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/adspayments/activity/AddressActivity;->setResult(ILandroid/content/Intent;)V

    .line 2347190
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AddressActivity;->finish()V

    .line 2347191
    return-void

    .line 2347192
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static x(Lcom/facebook/adspayments/activity/AddressActivity;)V
    .locals 13

    .prologue
    .line 2347193
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->Y:LX/GO6;

    sget-object v1, LX/GO6;->BUSINESS_ADDRESS:LX/GO6;

    invoke-virtual {v0, v1}, LX/GO6;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2347194
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->V:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    .line 2347195
    const v1, 0x7f0d04c6

    if-ne v0, v1, :cond_0

    .line 2347196
    invoke-static {p0}, Lcom/facebook/adspayments/activity/AddressActivity;->y(Lcom/facebook/adspayments/activity/AddressActivity;)V

    .line 2347197
    :goto_0
    return-void

    .line 2347198
    :cond_0
    const v1, 0x7f0827fe

    const v2, 0x7f080021

    new-instance v3, LX/GO3;

    invoke-direct {v3, p0}, LX/GO3;-><init>(Lcom/facebook/adspayments/activity/AddressActivity;)V

    const v4, 0x7f080020

    new-instance v5, LX/GO4;

    invoke-direct {v5, p0}, LX/GO4;-><init>(Lcom/facebook/adspayments/activity/AddressActivity;)V

    move-object v0, p0

    .line 2347199
    const/4 v7, 0x0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object v6, v0

    move-object v10, v3

    move-object v12, v5

    invoke-static/range {v6 .. v12}, LX/Gza;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/2EJ;

    move-result-object v6

    move-object v0, v6

    .line 2347200
    invoke-virtual {v0}, LX/2EJ;->show()V

    goto :goto_0

    .line 2347201
    :cond_1
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/AddressActivity;->w()V

    goto :goto_0
.end method

.method public static y(Lcom/facebook/adspayments/activity/AddressActivity;)V
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 2347202
    iget-object v8, p0, Lcom/facebook/adspayments/activity/AddressActivity;->q:LX/GPk;

    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2347203
    iget-object v1, v0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    move-object v9, v1

    .line 2347204
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->K:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v10

    new-instance v0, Lcom/facebook/adspayments/model/BusinessAddressDetails;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AddressActivity;->M:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AddressActivity;->N:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/adspayments/activity/AddressActivity;->O:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v3}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/adspayments/activity/AddressActivity;->Q:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v4}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/adspayments/activity/AddressActivity;->P:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v5}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/adspayments/activity/AddressActivity;->Z:Lcom/facebook/common/locale/Country;

    invoke-virtual {v6}, Lcom/facebook/common/locale/LocaleMember;->b()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/facebook/adspayments/model/BusinessAddressDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/facebook/adspayments/activity/AddressActivity;->A()Ljava/lang/String;

    move-result-object v5

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AddressActivity;->V:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v1

    const v2, 0x7f0d04c7

    if-ne v1, v2, :cond_0

    const/4 v6, 0x1

    :goto_0
    move-object v1, v8

    move-object v2, v9

    move-object v3, v10

    move-object v4, v0

    invoke-virtual/range {v1 .. v7}, LX/GPk;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adspayments/model/BusinessAddressDetails;Ljava/lang/String;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2347205
    invoke-direct {p0, v0}, Lcom/facebook/adspayments/activity/AddressActivity;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 2347206
    return-void

    :cond_0
    move v6, v7

    .line 2347207
    goto :goto_0
.end method

.method private z()V
    .locals 18

    .prologue
    .line 2347208
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/adspayments/activity/AddressActivity;->q:LX/GPk;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {v1}, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->f()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/adspayments/activity/AddressActivity;->K:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v16

    new-instance v1, Lcom/facebook/adspayments/model/BusinessAddressDetails;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/adspayments/activity/AddressActivity;->M:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/adspayments/activity/AddressActivity;->N:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v3}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/adspayments/activity/AddressActivity;->O:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v4}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/adspayments/activity/AddressActivity;->Q:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v5}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/adspayments/activity/AddressActivity;->P:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v6}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/adspayments/activity/AddressActivity;->Z:Lcom/facebook/common/locale/Country;

    invoke-virtual {v7}, Lcom/facebook/common/locale/LocaleMember;->b()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Lcom/facebook/adspayments/model/BusinessAddressDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/facebook/adspayments/activity/AddressActivity;->A()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/adspayments/activity/AddressActivity;->V:Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v2

    const v3, 0x7f0d04c7

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    move v11, v2

    :goto_0
    const/4 v9, 0x1

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/adspayments/activity/AddressActivity;->ad:Ljava/lang/String;

    new-instance v2, Lcom/facebook/adspayments/model/BusinessAddressDetails;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/adspayments/activity/AddressActivity;->af:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/adspayments/activity/AddressActivity;->ag:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/adspayments/activity/AddressActivity;->ah:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/adspayments/activity/AddressActivity;->aj:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/adspayments/activity/AddressActivity;->ai:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/adspayments/activity/AddressActivity;->ak:Ljava/lang/String;

    invoke-direct/range {v2 .. v8}, Lcom/facebook/adspayments/model/BusinessAddressDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/adspayments/activity/AddressActivity;->ae:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/adspayments/activity/AddressActivity;->al:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    move-object v3, v14

    move-object v4, v15

    move-object/from16 v5, v16

    move-object v6, v1

    move-object/from16 v7, v17

    move v8, v11

    move-object v11, v2

    invoke-virtual/range {v3 .. v13}, LX/GPk;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adspayments/model/BusinessAddressDetails;Ljava/lang/String;ZZLjava/lang/String;Lcom/facebook/adspayments/model/BusinessAddressDetails;Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2347209
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/facebook/adspayments/activity/AddressActivity;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 2347210
    return-void

    .line 2347211
    :cond_0
    const/4 v2, 0x0

    move v11, v2

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2347212
    const-string v0, "business_info"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2347213
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Landroid/os/Bundle;)V

    .line 2347214
    invoke-static {p0, p0}, Lcom/facebook/adspayments/activity/AddressActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2347215
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2347216
    const-string v0, "address_activity_state"

    invoke-direct {p0, v0}, Lcom/facebook/adspayments/activity/AddressActivity;->b(Ljava/lang/String;)V

    .line 2347217
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "address_activity_state"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/GO6;->find(Ljava/lang/String;)LX/GO6;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->Y:LX/GO6;

    .line 2347218
    const-string v0, "address_country"

    invoke-direct {p0, v0}, Lcom/facebook/adspayments/activity/AddressActivity;->b(Ljava/lang/String;)V

    .line 2347219
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/AddressActivity;->n()Lcom/facebook/common/locale/Country;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->Z:Lcom/facebook/common/locale/Country;

    .line 2347220
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "amount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->aa:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2347221
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "card"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->ab:Lcom/facebook/adspayments/model/CvvPrepayCreditCard;

    .line 2347222
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ask_cvv"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->ac:Z

    .line 2347223
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->b(Landroid/os/Bundle;)V

    .line 2347224
    const v0, 0x7f0300ae

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AddressActivity;->setContentView(I)V

    .line 2347225
    const v0, 0x7f0d04b9

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->s:Lcom/facebook/resources/ui/FbTextView;

    .line 2347226
    const v0, 0x7f0d04ba

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->K:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347227
    const v0, 0x7f0d04bb

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->L:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347228
    const v0, 0x7f0d04bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->M:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347229
    const v0, 0x7f0d04bd

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->N:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347230
    const v0, 0x7f0d04be

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->O:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347231
    const v0, 0x7f0d04bf

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->P:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347232
    const v0, 0x7f0d04c0

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->Q:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347233
    const v0, 0x7f0d04c1

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->R:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347234
    const v0, 0x7f0d04c2

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->S:Landroid/widget/Spinner;

    .line 2347235
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x1090008

    sget-object v2, Lcom/facebook/adspayments/activity/AddressActivity;->am:LX/0Px;

    invoke-direct {v0, p0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 2347236
    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 2347237
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AddressActivity;->S:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 2347238
    const v0, 0x7f0d04c3

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->T:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347239
    const v0, 0x7f0d04c4

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->U:Lcom/facebook/resources/ui/FbTextView;

    .line 2347240
    const v0, 0x7f0d04c5

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->V:Landroid/widget/RadioGroup;

    .line 2347241
    const v0, 0x7f0d04c8

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->W:Lcom/facebook/resources/ui/FbTextView;

    .line 2347242
    const v0, 0x7f0d04c9

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->X:Landroid/widget/RadioGroup;

    .line 2347243
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->N:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v1, 0x7f0827f7

    invoke-virtual {p0, v1}, Lcom/facebook/adspayments/activity/AddressActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/GPt;->OPTIONAL:LX/GPt;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, p0, v3}, LX/GQ1;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;Ljava/lang/String;LX/GPt;Landroid/content/Context;Z)V

    .line 2347244
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->R:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AddressActivity;->Z:Lcom/facebook/common/locale/Country;

    invoke-virtual {v1}, Lcom/facebook/common/locale/Country;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/GQ1;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;Ljava/lang/String;)V

    .line 2347245
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/AddressActivity;->o()V

    .line 2347246
    invoke-static {p0}, Lcom/facebook/adspayments/activity/AddressActivity;->D(Lcom/facebook/adspayments/activity/AddressActivity;)V

    .line 2347247
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->Y:LX/GO6;

    sget-object v1, LX/GO6;->CLIENT_ADDRESS:LX/GO6;

    invoke-virtual {v0, v1}, LX/GO6;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2347248
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->K:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    new-instance v1, LX/GO0;

    invoke-direct {v1, p0}, LX/GO0;-><init>(Lcom/facebook/adspayments/activity/AddressActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2347249
    :cond_0
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->L:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    new-instance v1, LX/GO1;

    invoke-direct {v1, p0}, LX/GO1;-><init>(Lcom/facebook/adspayments/activity/AddressActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2347250
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->N:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    new-instance v1, LX/GO2;

    invoke-direct {v1, p0}, LX/GO2;-><init>(Lcom/facebook/adspayments/activity/AddressActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2347251
    return-void
.end method

.method public final m()I
    .locals 2

    .prologue
    .line 2347252
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->Y:LX/GO6;

    sget-object v1, LX/GO6;->BUSINESS_ADDRESS:LX/GO6;

    invoke-virtual {v0, v1}, LX/GO6;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2347253
    const v0, 0x7f0827ed

    .line 2347254
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0827ee

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2347255
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->Y:LX/GO6;

    sget-object v1, LX/GO6;->BUSINESS_ADDRESS:LX/GO6;

    invoke-virtual {v0, v1}, LX/GO6;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2347256
    sget v0, Lcom/facebook/adspayments/activity/AddressActivity;->r:I

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2347257
    const-string v0, "client_name"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->ad:Ljava/lang/String;

    .line 2347258
    const-string v0, "client_email"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->ae:Ljava/lang/String;

    .line 2347259
    const-string v0, "client_address"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->af:Ljava/lang/String;

    .line 2347260
    const-string v0, "client_apt"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->ag:Ljava/lang/String;

    .line 2347261
    const-string v0, "client_city"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->ah:Ljava/lang/String;

    .line 2347262
    const-string v0, "client_postal_code"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->ai:Ljava/lang/String;

    .line 2347263
    const-string v0, "client_state"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->aj:Ljava/lang/String;

    .line 2347264
    const-string v0, "client_country"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->ak:Ljava/lang/String;

    .line 2347265
    const-string v0, "is_client_paying_for_invoices"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/AddressActivity;->al:Ljava/lang/Boolean;

    .line 2347266
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/AddressActivity;->z()V

    .line 2347267
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2347268
    return-void
.end method
