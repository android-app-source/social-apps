.class public Lcom/facebook/adspayments/activity/AddPaymentCardActivity;
.super Lcom/facebook/adspayments/activity/PaymentCardActivity;
.source ""


# instance fields
.field public p:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/GPk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/GOy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2346978
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/PaymentCardActivity;-><init>()V

    .line 2346979
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 2347094
    const-class v0, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;

    invoke-static {v0, p0, p1, p2}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Ljava/lang/Class;Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/6y8;LX/2h0;)V
    .locals 3

    .prologue
    .line 2347092
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->s:LX/1Ck;

    sget-object v1, LX/GNz;->SAVE_CARD:LX/GNz;

    new-instance v2, LX/GNy;

    invoke-direct {v2, p0, p1}, LX/GNy;-><init>(Lcom/facebook/adspayments/activity/AddPaymentCardActivity;LX/6y8;)V

    invoke-virtual {v0, v1, v2, p2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2347093
    return-void
.end method

.method public static synthetic a(Lcom/facebook/adspayments/activity/AddPaymentCardActivity;)V
    .locals 0

    .prologue
    .line 2347091
    invoke-super {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->onBackPressed()V

    return-void
.end method

.method private static a(Lcom/facebook/adspayments/activity/AddPaymentCardActivity;LX/0Or;LX/GPk;LX/GOy;LX/1Ck;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/adspayments/activity/AddPaymentCardActivity;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/GPk;",
            "LX/GOy;",
            "LX/1Ck;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2347090
    iput-object p1, p0, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->p:LX/0Or;

    iput-object p2, p0, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->q:LX/GPk;

    iput-object p3, p0, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->r:LX/GOy;

    iput-object p4, p0, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->s:LX/1Ck;

    return-void
.end method

.method private a(Lcom/facebook/payments/paymentmethods/model/CreditCard;)V
    .locals 1

    .prologue
    .line 2347088
    invoke-static {p1}, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->b(Lcom/facebook/payments/paymentmethods/model/CreditCard;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->e(Landroid/content/Intent;)V

    .line 2347089
    return-void
.end method

.method private a(Lcom/facebook/payments/paymentmethods/model/CreditCard;Ljava/lang/String;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/paymentmethods/model/CreditCard;",
            "Ljava/lang/String;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/adspayments/protocol/CvvPrepayData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2347086
    new-instance v0, Lcom/facebook/adspayments/activity/AddPaymentCardActivity$4;

    invoke-direct {v0, p0, p3, p2, p1}, Lcom/facebook/adspayments/activity/AddPaymentCardActivity$4;-><init>(Lcom/facebook/adspayments/activity/AddPaymentCardActivity;Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/String;Lcom/facebook/payments/paymentmethods/model/CreditCard;)V

    invoke-static {p0, v0, p1}, LX/GQ5;->a(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;Ljava/lang/Runnable;Lcom/facebook/payments/paymentmethods/model/PaymentOption;)V

    .line 2347087
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;

    const/16 v0, 0x15e7

    invoke-static {v2, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {v2}, LX/GPk;->a(LX/0QB;)LX/GPk;

    move-result-object v0

    check-cast v0, LX/GPk;

    invoke-static {v2}, LX/GOy;->b(LX/0QB;)LX/GOy;

    move-result-object v1

    check-cast v1, LX/GOy;

    invoke-static {v2}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static {p0, v3, v0, v1, v2}, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->a(Lcom/facebook/adspayments/activity/AddPaymentCardActivity;LX/0Or;LX/GPk;LX/GOy;LX/1Ck;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/adspayments/activity/AddPaymentCardActivity;Lcom/facebook/adspayments/model/CvvPrepayCreditCard;)V
    .locals 7

    .prologue
    .line 2347082
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    sget-object v2, LX/GO6;->BUSINESS_ADDRESS:LX/GO6;

    iget-object v3, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->X:Lcom/facebook/common/locale/Country;

    .line 2347083
    iget-object v0, p1, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v4, v0

    .line 2347084
    const/4 v6, 0x0

    move-object v0, p0

    move-object v5, p1

    invoke-static/range {v0 .. v6}, Lcom/facebook/adspayments/activity/AddressActivity;->a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;LX/GO6;Lcom/facebook/common/locale/Country;Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/adspayments/model/CvvPrepayCreditCard;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1}, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->b(Lcom/facebook/payments/paymentmethods/model/CreditCard;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Landroid/content/Intent;Landroid/content/Intent;)V

    .line 2347085
    return-void
.end method

.method public static a$redex0(Lcom/facebook/adspayments/activity/AddPaymentCardActivity;Lcom/facebook/adspayments/protocol/AddPaymentCardResult;LX/6y8;)V
    .locals 12
    .param p0    # Lcom/facebook/adspayments/activity/AddPaymentCardActivity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2347044
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->w()V

    .line 2347045
    if-nez p1, :cond_1

    .line 2347046
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->D:LX/ADW;

    const-string v1, "Add card result was null"

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2347047
    iget-wide v6, v0, LX/ADW;->d:J

    .line 2347048
    iget-wide v10, v2, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->c:J

    move-wide v8, v10

    .line 2347049
    cmp-long v6, v6, v8

    if-eqz v6, :cond_0

    .line 2347050
    invoke-static {v0, v2}, LX/ADW;->a(LX/ADW;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 2347051
    :cond_0
    new-instance v6, Lcom/facebook/adspayments/analytics/PaymentsReliabilityErrorLogEvent;

    invoke-direct {v6, v1, v2}, Lcom/facebook/adspayments/analytics/PaymentsReliabilityErrorLogEvent;-><init>(Ljava/lang/String;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 2347052
    iget-object v7, v0, LX/ADW;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/facebook/adspayments/analytics/PaymentsReliabilityLogEvent;->n(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsReliabilityLogEvent;

    .line 2347053
    invoke-static {v0, v6}, LX/ADW;->a(LX/ADW;Lcom/facebook/adspayments/analytics/PaymentsReliabilityLogEvent;)V

    .line 2347054
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->D:LX/ADW;

    const-string v0, "payments_add_card_fail"

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->h(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v0

    invoke-virtual {p2}, LX/6y8;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;->n(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v0

    const-string v2, "Add card result was null"

    invoke-virtual {v0, v2}, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;->m(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;

    move-result-object v0

    check-cast v0, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    invoke-virtual {v1, v0}, LX/ADW;->a(Lcom/facebook/adspayments/analytics/PaymentsLogEvent;)V

    .line 2347055
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->u()V

    .line 2347056
    const v0, 0x7f0827b5

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2347057
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->C:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Null result received when card is added successfully."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2347058
    :goto_0
    return-void

    .line 2347059
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/adspayments/protocol/AddPaymentCardResult;->a()Ljava/lang/String;

    move-result-object v0

    .line 2347060
    invoke-virtual {p1}, Lcom/facebook/adspayments/protocol/AddPaymentCardResult;->b()LX/03R;

    move-result-object v2

    .line 2347061
    invoke-virtual {v2}, LX/03R;->asBooleanObject()Ljava/lang/Boolean;

    move-result-object v3

    .line 2347062
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v4, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->X:Lcom/facebook/common/locale/Country;

    invoke-virtual {p2, v0, v1, v4}, LX/6y8;->a(Ljava/lang/String;ZLcom/facebook/common/locale/Country;)Lcom/facebook/payments/paymentmethods/model/CreditCard;

    move-result-object v1

    .line 2347063
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->D:LX/ADW;

    const-string v4, "payments_add_card_success"

    invoke-virtual {p0, v4}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->h(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;->a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v4

    .line 2347064
    const-string v5, "is_tricky_bin"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2347065
    move-object v3, v4

    .line 2347066
    invoke-virtual {v0, v3}, LX/ADW;->a(Lcom/facebook/adspayments/analytics/PaymentsLogEvent;)V

    .line 2347067
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, LX/03R;->asBoolean(Z)Z

    move-result v2

    .line 2347068
    invoke-virtual {p1}, Lcom/facebook/adspayments/protocol/AddPaymentCardResult;->c()Ljava/lang/String;

    move-result-object v3

    .line 2347069
    if-eqz v2, :cond_3

    .line 2347070
    new-instance v0, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;

    invoke-direct {v0, v1, v3}, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;-><init>(Lcom/facebook/payments/paymentmethods/model/CreditCard;Ljava/lang/String;)V

    .line 2347071
    :goto_1
    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->v()Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    move-result-object v1

    .line 2347072
    iget-object v2, v1, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->c:LX/ADX;

    move-object v1, v2

    .line 2347073
    invoke-virtual {v1}, LX/ADX;->isNUX()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2347074
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->q:LX/GPk;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2347075
    iget-object v4, v2, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    move-object v2, v4

    .line 2347076
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->x()Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->v()Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    move-result-object v5

    .line 2347077
    iget-boolean v6, v5, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->b:Z

    move v5, v6

    .line 2347078
    invoke-virtual {v1, v2, v0, v4, v5}, LX/GPk;->a(Ljava/lang/String;Lcom/facebook/payments/paymentmethods/model/PaymentOption;Lcom/facebook/payments/currency/CurrencyAmount;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2347079
    invoke-direct {p0, v0, v3, v1}, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->a(Lcom/facebook/payments/paymentmethods/model/CreditCard;Ljava/lang/String;Lcom/google/common/util/concurrent/ListenableFuture;)V

    goto :goto_0

    .line 2347080
    :cond_2
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->D:LX/ADW;

    sget-object v2, LX/ADV;->DONE_STATE:LX/ADV;

    iget-object v3, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {v1, v2, v3}, LX/ADW;->a(LX/ADV;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)Lcom/facebook/adspayments/analytics/PaymentsReliabilityTransitionLogEvent;

    .line 2347081
    invoke-direct {p0, v0}, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->a(Lcom/facebook/payments/paymentmethods/model/CreditCard;)V

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/adspayments/activity/AddPaymentCardActivity;Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/String;Lcom/facebook/payments/paymentmethods/model/CreditCard;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/adspayments/protocol/CvvPrepayData;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/payments/paymentmethods/model/CreditCard;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2347042
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->s:LX/1Ck;

    sget-object v1, LX/GNz;->FETCH_PREPAY_DATA:LX/GNz;

    new-instance v2, LX/GNx;

    invoke-direct {v2, p0, p3, p2}, LX/GNx;-><init>(Lcom/facebook/adspayments/activity/AddPaymentCardActivity;Lcom/facebook/payments/paymentmethods/model/CreditCard;Ljava/lang/String;)V

    invoke-virtual {v0, v1, p1, v2}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2347043
    return-void
.end method

.method private static b(Lcom/facebook/payments/paymentmethods/model/CreditCard;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2347038
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2347039
    const-string v1, "encoded_credential_id"

    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2347040
    const-string v1, "credit_card"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2347041
    return-object v0
.end method

.method public static b$redex0(Lcom/facebook/adspayments/activity/AddPaymentCardActivity;Lcom/facebook/adspayments/model/CvvPrepayCreditCard;)V
    .locals 6

    .prologue
    .line 2347034
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2347035
    iget-object v0, p1, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v2, v0

    .line 2347036
    iget-object v4, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->X:Lcom/facebook/common/locale/Country;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/adspayments/model/CvvPrepayCreditCard;Lcom/facebook/common/locale/Country;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1}, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->b(Lcom/facebook/payments/paymentmethods/model/CreditCard;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Landroid/content/Intent;Landroid/content/Intent;)V

    .line 2347037
    return-void
.end method

.method public static d(Landroid/content/Intent;)Lcom/facebook/payments/paymentmethods/model/CreditCard;
    .locals 1

    .prologue
    .line 2347033
    const-string v0, "credit_card"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    return-object v0
.end method

.method private x()Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 1

    .prologue
    .line 2347095
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->v()Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    move-result-object v0

    .line 2347096
    iget-object p0, v0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v0, p0

    .line 2347097
    return-object v0
.end method


# virtual methods
.method public final a(LX/6y8;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6y8;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2347026
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->q:LX/GPk;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->X:Lcom/facebook/common/locale/Country;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->p:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2347027
    iget-object v4, v1, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    move-object v4, v4

    .line 2347028
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2347029
    iget-object v5, v1, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->b:LX/6xg;

    move-object v5, v5

    .line 2347030
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->r:LX/GOy;

    iget-object v6, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2347031
    iget-object p0, v6, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    move-object v6, p0

    .line 2347032
    invoke-virtual {v1, v6}, LX/GOy;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v6, 0x1

    :goto_0
    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, LX/GPk;->a(LX/6y8;Lcom/facebook/common/locale/Country;Ljava/lang/String;Ljava/lang/String;LX/6xg;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2347025
    const-string v0, "add_card"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2347022
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->a(Landroid/os/Bundle;)V

    .line 2347023
    invoke-static {p0, p0}, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2347024
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2347020
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->T:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2347021
    return-void
.end method

.method public final c(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2347018
    invoke-static {p1}, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->d(Landroid/content/Intent;)Lcom/facebook/payments/paymentmethods/model/CreditCard;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;Ljava/lang/String;)V

    .line 2347019
    return-void
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2347017
    sget-object v0, LX/ADV;->ADD_CC_STATE:LX/ADV;

    invoke-virtual {v0}, LX/ADV;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 2347016
    const v0, 0x7f0827ae

    return v0
.end method

.method public final n()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 2347015
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->T:Landroid/widget/EditText;

    return-object v0
.end method

.method public final o()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;
    .locals 1

    .prologue
    .line 2347014
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->T:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6yU;->a(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v0

    return-object v0
.end method

.method public final onBackPressed()V
    .locals 14

    .prologue
    .line 2347010
    const v1, 0x7f0827d1

    const v2, 0x7f0827d2

    const v3, 0x7f08002a

    new-instance v4, LX/GNt;

    invoke-direct {v4, p0}, LX/GNt;-><init>(Lcom/facebook/adspayments/activity/AddPaymentCardActivity;)V

    const v5, 0x7f080027

    new-instance v6, LX/GNu;

    invoke-direct {v6, p0}, LX/GNu;-><init>(Lcom/facebook/adspayments/activity/AddPaymentCardActivity;)V

    move-object v0, p0

    .line 2347011
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    move-object v7, v0

    move-object v11, v4

    move-object v13, v6

    invoke-static/range {v7 .. v13}, LX/Gza;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/2EJ;

    move-result-object v7

    move-object v0, v7

    .line 2347012
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2347013
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x739b957

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2347006
    invoke-super {p0}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->onDestroy()V

    .line 2347007
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->s:LX/1Ck;

    if-eqz v1, :cond_0

    .line 2347008
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->s:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2347009
    :cond_0
    const/16 v1, 0x23

    const v2, -0x42995253

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final p()V
    .locals 3

    .prologue
    .line 2346987
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t()V

    .line 2346988
    const-string v0, "payments_confirm_card_details"

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->g(Ljava/lang/String;)V

    .line 2346989
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->o()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v0

    .line 2346990
    new-instance v1, LX/6y9;

    invoke-direct {v1}, LX/6y9;-><init>()V

    iget-object v2, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->T:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2346991
    iput-object v2, v1, LX/6y9;->a:Ljava/lang/String;

    .line 2346992
    move-object v1, v1

    .line 2346993
    iget-object v2, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->U:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2346994
    iput-object v2, v1, LX/6y9;->c:Ljava/lang/String;

    .line 2346995
    move-object v1, v1

    .line 2346996
    iget-object v2, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->W:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2346997
    iput-object v2, v1, LX/6y9;->f:Ljava/lang/String;

    .line 2346998
    move-object v1, v1

    .line 2346999
    iget-object v2, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->V:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2347000
    iput-object v2, v1, LX/6y9;->g:Ljava/lang/String;

    .line 2347001
    move-object v1, v1

    .line 2347002
    invoke-virtual {v1}, LX/6y9;->i()LX/6y8;

    move-result-object v1

    .line 2347003
    new-instance v2, LX/GNv;

    invoke-direct {v2, p0, v0, v1}, LX/GNv;-><init>(Lcom/facebook/adspayments/activity/AddPaymentCardActivity;Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;LX/6y8;)V

    .line 2347004
    invoke-direct {p0, v1, v2}, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->a(LX/6y8;LX/2h0;)V

    .line 2347005
    return-void
.end method

.method public final q()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2346980
    const/4 v2, 0x1

    .line 2346981
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->Y:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v1

    :goto_0
    if-ge v3, v4, :cond_0

    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->Y:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GP4;

    .line 2346982
    invoke-virtual {v0}, LX/GP4;->i()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2346983
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    .line 2346984
    :cond_0
    if-eqz v2, :cond_1

    .line 2346985
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->p()V

    .line 2346986
    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_1
.end method
