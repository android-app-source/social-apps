.class public final Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/common/locale/Country;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;Lcom/facebook/common/locale/Country;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2347444
    iput-object p1, p0, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity$1;->c:Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;

    iput-object p2, p0, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity$1;->a:Lcom/facebook/common/locale/Country;

    iput-object p3, p0, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity$1;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 2347445
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2347446
    const-string v1, "country"

    iget-object v2, p0, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity$1;->a:Lcom/facebook/common/locale/Country;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2347447
    const-string v1, "brazilian_tax_id"

    iget-object v2, p0, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity$1;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2347448
    const-string v1, "payments_flow_context_key"

    iget-object v2, p0, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity$1;->c:Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;

    iget-object v2, v2, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2347449
    iget-object v1, p0, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity$1;->c:Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;

    invoke-virtual {v1, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->e(Landroid/content/Intent;)V

    .line 2347450
    return-void
.end method
