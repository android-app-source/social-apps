.class public Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;
.super Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;
.source ""


# instance fields
.field public K:LX/GPm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private M:Lcom/facebook/resources/ui/FbEditText;

.field public p:Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public q:Lcom/facebook/common/locale/Country;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public r:LX/GPk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2347511
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;-><init>()V

    .line 2347512
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2347510
    const-class v0, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;

    const/4 v1, 0x0

    invoke-static {v0, p0, p1, v1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Ljava/lang/Class;Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;LX/GPk;LX/GPm;LX/1Ck;)V
    .locals 0

    .prologue
    .line 2347509
    iput-object p1, p0, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->r:LX/GPk;

    iput-object p2, p0, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->K:LX/GPm;

    iput-object p3, p0, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->L:LX/1Ck;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;

    invoke-static {v2}, LX/GPk;->a(LX/0QB;)LX/GPk;

    move-result-object v0

    check-cast v0, LX/GPk;

    invoke-static {v2}, LX/GPm;->b(LX/0QB;)LX/GPm;

    move-result-object v1

    check-cast v1, LX/GPm;

    invoke-static {v2}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->a(Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;LX/GPk;LX/GPm;LX/1Ck;)V

    return-void
.end method

.method public static n(Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;)V
    .locals 3

    .prologue
    .line 2347507
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->q:Lcom/facebook/common/locale/Country;

    invoke-static {p0}, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->q(Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->f(Landroid/content/Intent;)V

    .line 2347508
    return-void
.end method

.method private o()LX/6zn;
    .locals 1

    .prologue
    .line 2347506
    new-instance v0, LX/GOK;

    invoke-direct {v0, p0}, LX/GOK;-><init>(Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;)V

    return-object v0
.end method

.method public static p(Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;)V
    .locals 6

    .prologue
    .line 2347496
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t()V

    .line 2347497
    invoke-static {p0}, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->q(Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/GPR;->ofTaxId(Ljava/lang/String;)LX/GPR;

    move-result-object v0

    .line 2347498
    iget-object v1, p0, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->r:LX/GPk;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2347499
    iget-object v3, v2, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2347500
    invoke-static {p0}, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->q(Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;)Ljava/lang/String;

    move-result-object v3

    .line 2347501
    new-instance v4, Lcom/facebook/adspayments/protocol/VerifyBrazilianTaxIdParams;

    invoke-direct {v4, v2, v3}, Lcom/facebook/adspayments/protocol/VerifyBrazilianTaxIdParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2347502
    iget-object v5, v1, LX/GPk;->d:LX/GPp;

    invoke-virtual {v5, v4}, LX/6u5;->c(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v1, v4

    .line 2347503
    new-instance v2, LX/GOL;

    invoke-direct {v2, p0, v0}, LX/GOL;-><init>(Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;LX/GPR;)V

    .line 2347504
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->L:LX/1Ck;

    sget-object v3, LX/GOM;->SEND_BRAZILIAN_TAX_ID:LX/GOM;

    invoke-virtual {v0, v3, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2347505
    return-void
.end method

.method public static q(Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2347495
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->M:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2347494
    const-string v0, "brazilian_tax_id"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2347491
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;->a(Landroid/os/Bundle;)V

    .line 2347492
    invoke-static {p0, p0}, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2347493
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2347481
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;->b(Landroid/os/Bundle;)V

    .line 2347482
    const v0, 0x7f0301dc

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->setContentView(I)V

    .line 2347483
    new-instance v0, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity$1;

    invoke-direct {v0, p0}, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity$1;-><init>(Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;)V

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Ljava/lang/Runnable;)V

    .line 2347484
    invoke-virtual {p0, v1, v1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(IZ)V

    .line 2347485
    sget-object v0, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;->s:Lcom/facebook/common/locale/Country;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->q:Lcom/facebook/common/locale/Country;

    .line 2347486
    const v0, 0x7f0d0797

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->p:Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;

    .line 2347487
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->p:Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->q:Lcom/facebook/common/locale/Country;

    invoke-direct {p0}, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->o()LX/6zn;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->a(Lcom/facebook/common/locale/Country;LX/6zn;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 2347488
    const v0, 0x7f0d0798

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->M:Lcom/facebook/resources/ui/FbEditText;

    .line 2347489
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->M:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/GOI;

    invoke-direct {v1, p0}, LX/GOI;-><init>(Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2347490
    return-void
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 2347480
    const v0, 0x7f0827db

    return v0
.end method
