.class public abstract Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;
.super Lcom/facebook/adspayments/activity/AdsPaymentsActivity;
.source ""


# static fields
.field public static final s:Lcom/facebook/common/locale/Country;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2347353
    const-string v0, "BR"

    invoke-static {v0}, Lcom/facebook/common/locale/Country;->a(Ljava/lang/String;)Lcom/facebook/common/locale/Country;

    move-result-object v0

    sput-object v0, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;->s:Lcom/facebook/common/locale/Country;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2347354
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/common/locale/Country;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2347355
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->E:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity$1;-><init>(Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;Lcom/facebook/common/locale/Country;Ljava/lang/String;)V

    const v2, 0x67a8d594

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2347356
    return-void
.end method
