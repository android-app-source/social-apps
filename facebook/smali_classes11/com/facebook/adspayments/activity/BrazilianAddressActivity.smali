.class public Lcom/facebook/adspayments/activity/BrazilianAddressActivity;
.super Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;
.source ""


# instance fields
.field private K:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public L:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public M:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field private N:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field private O:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public P:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public Q:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public R:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field private S:Landroid/widget/LinearLayout;

.field public T:Ljava/lang/String;

.field public U:Lcom/facebook/common/locale/Country;

.field private V:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/payments/ui/PaymentFormEditTextView;",
            ">;"
        }
    .end annotation
.end field

.field private W:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/ui/PaymentFormEditTextView;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/GPk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2347442
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;-><init>()V

    .line 2347443
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2347357
    const-class v0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;

    invoke-static {v0, p0, p1, p2}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Ljava/lang/Class;Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "tax_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;LX/1Ck;LX/GPk;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/adspayments/activity/BrazilianAddressActivity;",
            "LX/1Ck;",
            "LX/GPk;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2347441
    iput-object p1, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->p:LX/1Ck;

    iput-object p2, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->q:LX/GPk;

    iput-object p3, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->r:LX/0Or;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;

    invoke-static {v2}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-static {v2}, LX/GPk;->a(LX/0QB;)LX/GPk;

    move-result-object v1

    check-cast v1, LX/GPk;

    const/16 v3, 0x12cb

    invoke-static {v2, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->a(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;LX/1Ck;LX/GPk;LX/0Or;)V

    return-void
.end method

.method public static b(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2347435
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t()V

    .line 2347436
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->q:LX/GPk;

    .line 2347437
    iget-object v1, v0, LX/GPk;->e:LX/GPe;

    new-instance v2, Lcom/facebook/adspayments/protocol/GetBrazilianAddressDetailsParams;

    invoke-direct {v2, p1}, Lcom/facebook/adspayments/protocol/GetBrazilianAddressDetailsParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/6sU;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2347438
    new-instance v1, LX/GOF;

    invoke-direct {v1, p0}, LX/GOF;-><init>(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;)V

    .line 2347439
    iget-object v2, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->p:LX/1Ck;

    sget-object v3, LX/GOH;->SEND_POSTAL_CODE:LX/GOH;

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2347440
    return-void
.end method

.method public static b$redex0(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;Z)V
    .locals 3

    .prologue
    .line 2347433
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->N:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v1, 0x7f0827de

    invoke-virtual {p0, v1}, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/GPt;->REQUIRED:LX/GPt;

    invoke-static {v0, v1, v2, p0, p1}, LX/GQ1;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;Ljava/lang/String;LX/GPt;Landroid/content/Context;Z)V

    .line 2347434
    return-void
.end method

.method public static c(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;Z)V
    .locals 3

    .prologue
    .line 2347431
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->O:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v1, 0x7f0827df

    invoke-virtual {p0, v1}, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/GPt;->OPTIONAL:LX/GPt;

    invoke-static {v0, v1, v2, p0, p1}, LX/GQ1;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;Ljava/lang/String;LX/GPt;Landroid/content/Context;Z)V

    .line 2347432
    return-void
.end method

.method private d(Z)V
    .locals 1

    .prologue
    .line 2347425
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->M:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-static {v0, p1}, LX/GQ1;->a(Landroid/view/View;Z)V

    .line 2347426
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->N:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-static {v0, p1}, LX/GQ1;->a(Landroid/view/View;Z)V

    .line 2347427
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->O:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-static {v0, p1}, LX/GQ1;->a(Landroid/view/View;Z)V

    .line 2347428
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->P:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-static {v0, p1}, LX/GQ1;->a(Landroid/view/View;Z)V

    .line 2347429
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->S:Landroid/widget/LinearLayout;

    invoke-static {v0, p1}, LX/GQ1;->a(Landroid/view/View;Z)V

    .line 2347430
    return-void
.end method

.method public static n(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2347419
    const/4 v0, 0x1

    .line 2347420
    iget-object v1, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->V:LX/0Rf;

    invoke-virtual {v1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347421
    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    move v1, v0

    .line 2347422
    goto :goto_0

    .line 2347423
    :cond_0
    invoke-virtual {p0, v2, v1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(IZ)V

    .line 2347424
    return-void

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static o(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;)V
    .locals 12

    .prologue
    .line 2347410
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t()V

    .line 2347411
    iget-object v9, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->q:LX/GPk;

    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2347412
    iget-object v1, v0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    move-object v10, v1

    .line 2347413
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->K:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v11

    new-instance v0, Lcom/facebook/adspayments/model/BusinessAddressDetails;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->M:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->N:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->O:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v3}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->P:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v4}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->Q:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v5}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->R:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v6}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->L:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v7}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->U:Lcom/facebook/common/locale/Country;

    invoke-direct/range {v0 .. v8}, Lcom/facebook/adspayments/model/BusinessAddressDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/common/locale/Country;)V

    iget-object v1, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->T:Ljava/lang/String;

    .line 2347414
    new-instance v2, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;

    invoke-direct {v2, v10, v11, v0, v1}, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adspayments/model/BusinessAddressDetails;Ljava/lang/String;)V

    .line 2347415
    iget-object v3, v9, LX/GPk;->f:LX/GPm;

    invoke-virtual {v3, v2}, LX/6u5;->c(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2347416
    new-instance v1, LX/GOG;

    invoke-direct {v1, p0}, LX/GOG;-><init>(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;)V

    .line 2347417
    iget-object v2, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->p:LX/1Ck;

    sget-object v3, LX/GOH;->SEND_BUSINESS_ADDRESS:LX/GOH;

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2347418
    return-void
.end method

.method public static p(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;)V
    .locals 1

    .prologue
    .line 2347407
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->d(Z)V

    .line 2347408
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->w()V

    .line 2347409
    return-void
.end method

.method public static q(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;)V
    .locals 2

    .prologue
    .line 2347401
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->d(Z)V

    .line 2347402
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->M:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const-string v1, ""

    invoke-static {v0, v1}, LX/GQ1;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;Ljava/lang/String;)V

    .line 2347403
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->N:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const-string v1, ""

    invoke-static {v0, v1}, LX/GQ1;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;Ljava/lang/String;)V

    .line 2347404
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->O:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const-string v1, ""

    invoke-static {v0, v1}, LX/GQ1;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;Ljava/lang/String;)V

    .line 2347405
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->P:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const-string v1, ""

    invoke-static {v0, v1}, LX/GQ1;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;Ljava/lang/String;)V

    .line 2347406
    return-void
.end method

.method private w()V
    .locals 4

    .prologue
    .line 2347396
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->W:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->W:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347397
    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2347398
    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->requestFocus()Z

    .line 2347399
    :cond_0
    return-void

    .line 2347400
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2347395
    const-string v0, "brazilian_address"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2347392
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;->a(Landroid/os/Bundle;)V

    .line 2347393
    invoke-static {p0, p0}, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2347394
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2347359
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;->b(Landroid/os/Bundle;)V

    .line 2347360
    const v0, 0x7f0301db

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->setContentView(I)V

    .line 2347361
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "country"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->U:Lcom/facebook/common/locale/Country;

    .line 2347362
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "tax_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->T:Ljava/lang/String;

    .line 2347363
    new-instance v0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity$1;

    invoke-direct {v0, p0}, Lcom/facebook/adspayments/activity/BrazilianAddressActivity$1;-><init>(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;)V

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Ljava/lang/Runnable;)V

    .line 2347364
    invoke-virtual {p0, v2, v2}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(IZ)V

    .line 2347365
    const v0, 0x7f0d078e

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->K:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347366
    const v0, 0x7f0d078f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->L:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347367
    const v0, 0x7f0d0790

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->M:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347368
    const v0, 0x7f0d0791

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->N:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347369
    invoke-static {p0, v2}, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->b$redex0(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;Z)V

    .line 2347370
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->N:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    new-instance v1, LX/GOA;

    invoke-direct {v1, p0}, LX/GOA;-><init>(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2347371
    const v0, 0x7f0d0792

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->O:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347372
    invoke-static {p0, v2}, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->c(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;Z)V

    .line 2347373
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->O:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    new-instance v1, LX/GOB;

    invoke-direct {v1, p0}, LX/GOB;-><init>(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2347374
    const v0, 0x7f0d0793

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->P:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347375
    const v0, 0x7f0d0795

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->Q:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347376
    const v0, 0x7f0d0796

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->R:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347377
    const v0, 0x7f0d0794

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->S:Landroid/widget/LinearLayout;

    .line 2347378
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->K:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->M:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->N:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->V:LX/0Rf;

    .line 2347379
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->K:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->L:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->M:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v3, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->N:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v4, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->O:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v5, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->P:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-static/range {v0 .. v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->W:LX/0Px;

    .line 2347380
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->L:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347381
    iget-object v1, v0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    move-object v0, v1

    .line 2347382
    new-instance v1, LX/GOD;

    invoke-direct {v1, p0}, LX/GOD;-><init>(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2347383
    new-instance v1, LX/GOE;

    invoke-direct {v1, p0}, LX/GOE;-><init>(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;)V

    .line 2347384
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->V:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347385
    iget-object v3, v0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    move-object v0, v3

    .line 2347386
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    goto :goto_0

    .line 2347387
    :cond_0
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->K:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2347388
    iget-object v1, v0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    move-object v1, v1

    .line 2347389
    iget-object v0, p0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->r:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2347390
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->w()V

    .line 2347391
    return-void
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 2347358
    const v0, 0x7f0827ea

    return v0
.end method
