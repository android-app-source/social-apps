.class public abstract Lcom/facebook/adspayments/activity/PaymentCardActivity;
.super Lcom/facebook/adspayments/activity/AdsPaymentsActivity;
.source ""


# static fields
.field public static final K:I


# instance fields
.field public L:LX/GQ2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:LX/GPC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:LX/GP7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:LX/GPG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public P:LX/GP5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Q:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public R:LX/HXt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public S:LX/70D;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public T:Landroid/widget/EditText;

.field public U:Landroid/widget/EditText;

.field public V:Landroid/widget/EditText;

.field public W:Landroid/widget/EditText;

.field public X:Lcom/facebook/common/locale/Country;

.field public Y:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GP4",
            "<*>;>;"
        }
    .end annotation
.end field

.field public Z:Landroid/widget/TextView;

.field private aa:Landroid/widget/ImageView;

.field private p:Landroid/widget/LinearLayout;

.field private q:Landroid/widget/ProgressBar;

.field public r:LX/6ww;

.field public s:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2346916
    sget-object v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    sput v0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->K:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2346917
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;-><init>()V

    .line 2346918
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->s:Z

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/adspayments/activity/PaymentCardActivity;

    new-instance v2, LX/GQ2;

    invoke-static {v8}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    invoke-direct {v2, v1}, LX/GQ2;-><init>(Landroid/view/inputmethod/InputMethodManager;)V

    move-object v1, v2

    check-cast v1, LX/GQ2;

    invoke-static {v8}, LX/GPC;->b(LX/0QB;)LX/GPC;

    move-result-object v2

    check-cast v2, LX/GPC;

    invoke-static {v8}, LX/GP7;->b(LX/0QB;)LX/GP7;

    move-result-object v3

    check-cast v3, LX/GP7;

    invoke-static {v8}, LX/GPG;->b(LX/0QB;)LX/GPG;

    move-result-object v4

    check-cast v4, LX/GPG;

    new-instance p1, LX/GP5;

    new-instance v5, LX/GQ8;

    invoke-direct {v5}, LX/GQ8;-><init>()V

    move-object v5, v5

    move-object v5, v5

    check-cast v5, LX/GQ8;

    invoke-static {v8}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v8}, LX/GOy;->b(LX/0QB;)LX/GOy;

    move-result-object v7

    check-cast v7, LX/GOy;

    invoke-static {v8}, LX/GNs;->a(LX/0QB;)LX/GNs;

    move-result-object p0

    check-cast p0, LX/GNs;

    invoke-direct {p1, v5, v6, v7, p0}, LX/GP5;-><init>(LX/GQ8;Landroid/content/res/Resources;LX/GOy;LX/GNs;)V

    move-object v5, p1

    check-cast v5, LX/GP5;

    invoke-static {v8}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v8}, LX/HXt;->a(LX/0QB;)LX/HXt;

    move-result-object v7

    check-cast v7, LX/HXt;

    invoke-static {v8}, LX/70D;->a(LX/0QB;)LX/70D;

    move-result-object v8

    check-cast v8, LX/70D;

    iput-object v1, v0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->L:LX/GQ2;

    iput-object v2, v0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->M:LX/GPC;

    iput-object v3, v0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->N:LX/GP7;

    iput-object v4, v0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->O:LX/GPG;

    iput-object v5, v0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->P:LX/GP5;

    iput-object v6, v0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->Q:Landroid/content/res/Resources;

    iput-object v7, v0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->R:LX/HXt;

    iput-object v8, v0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->S:LX/70D;

    return-void
.end method

.method public static x(Lcom/facebook/adspayments/activity/PaymentCardActivity;)V
    .locals 2

    .prologue
    .line 2346923
    iget-object v1, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->V:Landroid/widget/EditText;

    iget-boolean v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->s:Z

    if-eqz v0, :cond_0

    const v0, 0x81001

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setInputType(I)V

    .line 2346924
    iget-object v1, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->aa:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->s:Z

    if-eqz v0, :cond_1

    const v0, 0x7f021142

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2346925
    return-void

    .line 2346926
    :cond_0
    const/16 v0, 0x14

    goto :goto_0

    .line 2346927
    :cond_1
    const v0, 0x7f020001

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2346919
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Landroid/os/Bundle;)V

    .line 2346920
    invoke-static {p0, p0}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2346921
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->M:LX/GPC;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->O:LX/GPG;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->N:LX/GP7;

    iget-object v3, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->P:LX/GP5;

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->Y:LX/0Px;

    .line 2346922
    return-void
.end method

.method public final a(Lcom/facebook/fbservice/service/ServiceException;Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;)V
    .locals 3

    .prologue
    .line 2346972
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->u()V

    .line 2346973
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->D:LX/ADW;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {v0, p1, v1}, LX/ADW;->a(Ljava/lang/Throwable;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)Lcom/facebook/adspayments/analytics/PaymentsReliabilityErrorLogEvent;

    .line 2346974
    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->D:LX/ADW;

    const-string v0, "payments_add_card_fail"

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->h(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->getHumanReadableName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;->n(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;->a(Ljava/lang/Throwable;)Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;

    move-result-object v0

    check-cast v0, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    invoke-virtual {v1, v0}, LX/ADW;->a(Lcom/facebook/adspayments/analytics/PaymentsLogEvent;)V

    .line 2346975
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->C:LX/03V;

    const-string v1, "PAYMENT_CARD_ADD_FAILED"

    const-string v2, "Attempted to add a PaymentCard, but received a response with an error"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2346976
    const v0, 0x7f0827bd

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0827be

    invoke-virtual {p0, v1}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p1, v0, v1}, LX/Gza;->a(Landroid/content/Context;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V

    .line 2346977
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2346928
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->b(Landroid/os/Bundle;)V

    .line 2346929
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 2346930
    const v0, 0x7f030efb

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->setContentView(I)V

    .line 2346931
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "payments_flow_context_key"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2346932
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->D:LX/ADW;

    const-string v1, "payments_initiate_add_card"

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {v0, v1, v2}, LX/ADW;->a(Ljava/lang/String;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 2346933
    new-instance v0, LX/GOO;

    invoke-direct {v0, p0}, LX/GOO;-><init>(Lcom/facebook/adspayments/activity/PaymentCardActivity;)V

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->r:LX/6ww;

    .line 2346934
    const v0, 0x7f0d247f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->p:Landroid/widget/LinearLayout;

    .line 2346935
    const v0, 0x7f0d2480

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->T:Landroid/widget/EditText;

    .line 2346936
    const v0, 0x7f0d0188

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->U:Landroid/widget/EditText;

    .line 2346937
    const v0, 0x7f0d2488

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->W:Landroid/widget/EditText;

    .line 2346938
    const v0, 0x7f0d018a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->V:Landroid/widget/EditText;

    .line 2346939
    const v0, 0x7f0d2484

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->aa:Landroid/widget/ImageView;

    .line 2346940
    const v0, 0x7f0d2486

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->q:Landroid/widget/ProgressBar;

    .line 2346941
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "country"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->X:Lcom/facebook/common/locale/Country;

    .line 2346942
    const v0, 0x7f0d2485

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adspayments/view/SecurityFooterView;

    .line 2346943
    invoke-virtual {v0}, Lcom/facebook/adspayments/view/SecurityFooterView;->a()V

    .line 2346944
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2346945
    iget-object v1, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->M:LX/GPC;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->U:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {v1, v0, v2, v3}, LX/GP4;->a(Landroid/view/View;Landroid/view/View;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 2346946
    iget-object v1, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->O:LX/GPG;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->W:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {v1, v0, v2, v3}, LX/GP4;->a(Landroid/view/View;Landroid/view/View;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 2346947
    iget-object v1, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->N:LX/GP7;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->V:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {v1, v0, v2, v3}, LX/GP4;->a(Landroid/view/View;Landroid/view/View;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 2346948
    iget-object v1, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->P:LX/GP5;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    iget-object v4, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->X:Lcom/facebook/common/locale/Country;

    invoke-virtual {v1, v0, v2, v3, v4}, LX/GP5;->a(Landroid/view/View;Landroid/view/View;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;)V

    .line 2346949
    const v0, 0x7f0d24ac

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->Z:Landroid/widget/TextView;

    .line 2346950
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->Z:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->Q:Landroid/content/res/Resources;

    const v2, 0x7f0a03a6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2346951
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->Z:Landroid/widget/TextView;

    new-instance v1, LX/GOP;

    invoke-direct {v1, p0}, LX/GOP;-><init>(Lcom/facebook/adspayments/activity/PaymentCardActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2346952
    new-instance v2, LX/GOQ;

    invoke-direct {v2, p0}, LX/GOQ;-><init>(Lcom/facebook/adspayments/activity/PaymentCardActivity;)V

    .line 2346953
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->X:Lcom/facebook/common/locale/Country;

    invoke-static {v0}, LX/46H;->a(Lcom/facebook/common/locale/Country;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->s:Z

    .line 2346954
    invoke-static {p0}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->x(Lcom/facebook/adspayments/activity/PaymentCardActivity;)V

    .line 2346955
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->aa:Landroid/widget/ImageView;

    new-instance v1, LX/GOR;

    invoke-direct {v1, p0}, LX/GOR;-><init>(Lcom/facebook/adspayments/activity/PaymentCardActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2346956
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->Y:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->Y:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GP4;

    .line 2346957
    invoke-virtual {v0}, LX/GP4;->c()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2346958
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2346959
    :cond_0
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->L:LX/GQ2;

    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->n()Landroid/widget/EditText;

    move-result-object v1

    .line 2346960
    if-nez v1, :cond_1

    .line 2346961
    invoke-virtual {p0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 2346962
    :cond_1
    if-eqz v1, :cond_3

    .line 2346963
    invoke-virtual {v1}, Landroid/view/View;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2346964
    invoke-virtual {v1}, Landroid/view/View;->clearFocus()V

    .line 2346965
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 2346966
    iget-object v2, v0, LX/GQ2;->a:Landroid/view/inputmethod/InputMethodManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 2346967
    :cond_3
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "add_card_retry_dialog"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    .line 2346968
    if-eqz v0, :cond_4

    .line 2346969
    iget-object v1, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->r:LX/6ww;

    .line 2346970
    iput-object v1, v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    .line 2346971
    :cond_4
    return-void
.end method

.method public abstract b(Ljava/lang/String;)V
.end method

.method public abstract n()Landroid/widget/EditText;
.end method

.method public abstract o()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2346880
    sget v0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->K:I

    if-ne p1, v0, :cond_0

    .line 2346881
    sget-object v0, LX/GOS;->a:[I

    iget-object v1, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->R:LX/HXt;

    invoke-virtual {v1}, LX/HXt;->d()LX/HXu;

    move-result-object v1

    invoke-virtual {v1}, LX/HXu;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2346882
    :goto_0
    return-void

    .line 2346883
    :pswitch_0
    const v0, 0x7f0827ca

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2346884
    :pswitch_1
    const-string v0, "payments_card_scanner_fail"

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->g(Ljava/lang/String;)V

    goto :goto_0

    .line 2346885
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->R:LX/HXt;

    invoke-virtual {v0}, LX/HXt;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->b(Ljava/lang/String;)V

    .line 2346886
    const-string v0, "payments_card_scanner_success"

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->g(Ljava/lang/String;)V

    goto :goto_0

    .line 2346887
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final onPause()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x7d66a987

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2346888
    invoke-super {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->onPause()V

    .line 2346889
    iget-object v1, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->L:LX/GQ2;

    .line 2346890
    invoke-virtual {p0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    .line 2346891
    if-eqz v2, :cond_0

    .line 2346892
    iget-object v4, v1, LX/GQ2;->a:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2346893
    :cond_0
    const/16 v1, 0x23

    const v2, -0x648aa8c9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2346894
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 2346895
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->Y:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->Y:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GP4;

    .line 2346896
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, LX/GP4;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_input_has_error"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v0, v4}, LX/GP4;->a(Z)V

    .line 2346897
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2346898
    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2346899
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->Y:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->Y:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GP4;

    .line 2346900
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, LX/GP4;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_input_has_error"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2346901
    iget-boolean v4, v0, LX/GP4;->k:Z

    move v0, v4

    .line 2346902
    invoke-virtual {p1, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2346903
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2346904
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2346905
    return-void
.end method

.method public abstract p()V
.end method

.method public abstract q()V
.end method

.method public final r()V
    .locals 6

    .prologue
    .line 2346906
    invoke-super {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->r()V

    .line 2346907
    const-string v0, "payments_cancel_add_card"

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->h(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->o()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->getHumanReadableName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;->n(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v2

    .line 2346908
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->Y:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->Y:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GP4;

    .line 2346909
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, LX/GP4;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_is_empty"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, LX/GP4;->c()Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    invoke-virtual {v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2346910
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, LX/GP4;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_is_valid"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, LX/GP4;->a()Z

    move-result v0

    invoke-virtual {v2, v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2346911
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2346912
    :cond_0
    iget-object v0, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->D:LX/ADW;

    invoke-virtual {v0, v2}, LX/ADW;->a(Lcom/facebook/adspayments/analytics/PaymentsLogEvent;)V

    .line 2346913
    return-void
.end method

.method public final w()V
    .locals 1

    .prologue
    .line 2346914
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->S:LX/70D;

    invoke-virtual {v0}, LX/0QG;->a()V

    .line 2346915
    return-void
.end method
