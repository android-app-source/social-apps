.class public Lcom/facebook/adspayments/activity/EditPaymentCardActivity;
.super Lcom/facebook/adspayments/activity/PaymentCardActivity;
.source ""


# instance fields
.field private aa:I

.field private ab:I

.field private ac:Ljava/lang/String;

.field private ad:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/GPk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2347564
    invoke-direct {p0}, Lcom/facebook/adspayments/activity/PaymentCardActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;

    invoke-static {v0}, LX/GPk;->a(LX/0QB;)LX/GPk;

    move-result-object v0

    check-cast v0, LX/GPk;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->p:LX/GPk;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2347563
    const-string v0, "edit_card"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2347560
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->a(Landroid/os/Bundle;)V

    .line 2347561
    invoke-static {p0, p0}, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2347562
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2347545
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->b(Landroid/os/Bundle;)V

    .line 2347546
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2347547
    const-string v0, "encoded_credential_id"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->q:Ljava/lang/String;

    .line 2347548
    const-string v0, "payment_last_four"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->r:Ljava/lang/String;

    .line 2347549
    const-string v0, "payment_card_type"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    iput-object v0, p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->s:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    .line 2347550
    const-string v0, "payment_expiry_month"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->aa:I

    .line 2347551
    const-string v0, "payment_expiry_year"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->ab:I

    .line 2347552
    const-string v0, "payment_zip_code"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->ac:Ljava/lang/String;

    .line 2347553
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->T:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->s:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->r:Ljava/lang/String;

    invoke-static {v1, v2}, LX/6yU;->a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2347554
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->T:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 2347555
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->M:LX/GPC;

    invoke-virtual {v0, v3}, LX/GP4;->a(Z)V

    .line 2347556
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->M:LX/GPC;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->s:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    invoke-virtual {v0, v1}, LX/GPC;->a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;)V

    .line 2347557
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->U:Landroid/widget/EditText;

    const-string v1, "%02d/%02d"

    iget v2, p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->aa:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->ab:I

    rem-int/lit8 v3, v3, 0x64

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2347558
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->V:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->ac:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2347559
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2347544
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Card number can not be changed for edit card operation."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final h(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;
    .locals 2

    .prologue
    .line 2347543
    invoke-super {p0, p1}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->h(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;->o(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v0

    return-object v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 2347542
    const v0, 0x7f0827c9

    return v0
.end method

.method public final n()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 2347541
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->W:Landroid/widget/EditText;

    return-object v0
.end method

.method public final o()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;
    .locals 1

    .prologue
    .line 2347540
    iget-object v0, p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->s:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    return-object v0
.end method

.method public final p()V
    .locals 9

    .prologue
    .line 2347523
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->t()V

    .line 2347524
    const-string v0, "payments_confirm_card_details"

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->g(Ljava/lang/String;)V

    .line 2347525
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->U:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 2347526
    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 2347527
    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 2347528
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->W:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2347529
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->V:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2347530
    new-instance v8, LX/GON;

    invoke-direct {v8, p0}, LX/GON;-><init>(Lcom/facebook/adspayments/activity/EditPaymentCardActivity;)V

    .line 2347531
    iget-object v0, p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->ad:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2347532
    :goto_0
    return-void

    .line 2347533
    :cond_0
    iget-object v0, p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->p:LX/GPk;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->q:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->G:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2347534
    iget-object v7, v2, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->b:LX/6xg;

    move-object v2, v7

    .line 2347535
    iget-object v7, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->X:Lcom/facebook/common/locale/Country;

    invoke-virtual/range {v0 .. v7}, LX/GPk;->a(Ljava/lang/String;LX/6xg;Ljava/lang/String;IILjava/lang/String;Lcom/facebook/common/locale/Country;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->ad:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2347536
    iget-object v0, p0, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->ad:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v1, p0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->E:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v8, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 2347537
    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->O:LX/GPG;

    invoke-virtual {v0}, LX/GP4;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->N:LX/GP7;

    invoke-virtual {v0}, LX/GP4;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/adspayments/activity/PaymentCardActivity;->P:LX/GP5;

    invoke-virtual {v0}, LX/GP4;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2347538
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->p()V

    .line 2347539
    :cond_0
    return-void
.end method
