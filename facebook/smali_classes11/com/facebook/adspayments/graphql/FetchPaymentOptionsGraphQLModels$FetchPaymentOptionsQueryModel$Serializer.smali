.class public final Lcom/facebook/adspayments/graphql/FetchPaymentOptionsGraphQLModels$FetchPaymentOptionsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adspayments/graphql/FetchPaymentOptionsGraphQLModels$FetchPaymentOptionsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2348692
    const-class v0, Lcom/facebook/adspayments/graphql/FetchPaymentOptionsGraphQLModels$FetchPaymentOptionsQueryModel;

    new-instance v1, Lcom/facebook/adspayments/graphql/FetchPaymentOptionsGraphQLModels$FetchPaymentOptionsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adspayments/graphql/FetchPaymentOptionsGraphQLModels$FetchPaymentOptionsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2348693
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2348694
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adspayments/graphql/FetchPaymentOptionsGraphQLModels$FetchPaymentOptionsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2348695
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2348696
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2348697
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2348698
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2348699
    if-eqz v2, :cond_1

    .line 2348700
    const-string p0, "supported_payment_options"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2348701
    const/4 v0, 0x0

    .line 2348702
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2348703
    invoke-virtual {v1, v2, v0}, LX/15i;->g(II)I

    move-result p0

    .line 2348704
    if-eqz p0, :cond_0

    .line 2348705
    const-string p0, "nodes"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2348706
    invoke-virtual {v1, v2, v0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2348707
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2348708
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2348709
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2348710
    check-cast p1, Lcom/facebook/adspayments/graphql/FetchPaymentOptionsGraphQLModels$FetchPaymentOptionsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adspayments/graphql/FetchPaymentOptionsGraphQLModels$FetchPaymentOptionsQueryModel$Serializer;->a(Lcom/facebook/adspayments/graphql/FetchPaymentOptionsGraphQLModels$FetchPaymentOptionsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
