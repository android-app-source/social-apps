.class public Lcom/facebook/login/GetTokenLoginMethodHandler;
.super Lcom/facebook/login/LoginMethodHandler;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/login/GetTokenLoginMethodHandler;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public c:LX/Gz1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2410950
    new-instance v0, LX/Gz4;

    invoke-direct {v0}, LX/Gz4;-><init>()V

    sput-object v0, Lcom/facebook/login/GetTokenLoginMethodHandler;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 2410977
    invoke-direct {p0, p1}, Lcom/facebook/login/LoginMethodHandler;-><init>(Landroid/os/Parcel;)V

    .line 2410978
    return-void
.end method

.method public constructor <init>(Lcom/facebook/login/LoginClient;)V
    .locals 0

    .prologue
    .line 2410975
    invoke-direct {p0, p1}, Lcom/facebook/login/LoginMethodHandler;-><init>(Lcom/facebook/login/LoginClient;)V

    .line 2410976
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2410968
    iget-object v0, p0, Lcom/facebook/login/GetTokenLoginMethodHandler;->c:LX/Gz1;

    if-eqz v0, :cond_0

    .line 2410969
    iget-object v0, p0, Lcom/facebook/login/GetTokenLoginMethodHandler;->c:LX/Gz1;

    .line 2410970
    const/4 v2, 0x0

    iput-boolean v2, v0, LX/GsU;->d:Z

    .line 2410971
    iget-object v0, p0, Lcom/facebook/login/GetTokenLoginMethodHandler;->c:LX/Gz1;

    .line 2410972
    iput-object v1, v0, LX/GsU;->c:LX/Gz2;

    .line 2410973
    iput-object v1, p0, Lcom/facebook/login/GetTokenLoginMethodHandler;->c:LX/Gz1;

    .line 2410974
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/login/LoginClient$Request;)Z
    .locals 14

    .prologue
    .line 2410979
    new-instance v0, LX/Gz1;

    iget-object v1, p0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    invoke-virtual {v1}, Lcom/facebook/login/LoginClient;->b()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2410980
    iget-object v2, p1, Lcom/facebook/login/LoginClient$Request;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2410981
    invoke-direct {v0, v1, v2}, LX/Gz1;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/login/GetTokenLoginMethodHandler;->c:LX/Gz1;

    .line 2410982
    iget-object v0, p0, Lcom/facebook/login/GetTokenLoginMethodHandler;->c:LX/Gz1;

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2410983
    iget-boolean v3, v0, LX/GsU;->d:Z

    if-eqz v3, :cond_2

    .line 2410984
    :cond_0
    :goto_0
    move v0, v1

    .line 2410985
    if-nez v0, :cond_1

    .line 2410986
    const/4 v0, 0x0

    .line 2410987
    :goto_1
    return v0

    .line 2410988
    :cond_1
    iget-object v0, p0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    invoke-virtual {v0}, Lcom/facebook/login/LoginClient;->h()V

    .line 2410989
    new-instance v0, LX/Gz2;

    invoke-direct {v0, p0, p1}, LX/Gz2;-><init>(Lcom/facebook/login/GetTokenLoginMethodHandler;Lcom/facebook/login/LoginClient$Request;)V

    .line 2410990
    iget-object v1, p0, Lcom/facebook/login/GetTokenLoginMethodHandler;->c:LX/Gz1;

    .line 2410991
    iput-object v0, v1, LX/GsU;->c:LX/Gz2;

    .line 2410992
    const/4 v0, 0x1

    goto :goto_1

    .line 2410993
    :cond_2
    iget v3, v0, LX/GsU;->i:I

    .line 2410994
    sget-object v4, LX/GsS;->b:Ljava/util/List;

    const/4 v5, 0x1

    new-array v5, v5, [I

    const/4 v6, 0x0

    aput v3, v5, v6

    const/4 v7, -0x1

    .line 2410995
    invoke-static {}, LX/GsS;->b()V

    .line 2410996
    if-nez v4, :cond_5

    move v6, v7

    .line 2410997
    :goto_2
    move v4, v6

    .line 2410998
    move v3, v4

    .line 2410999
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 2411000
    iget-object v3, v0, LX/GsU;->a:Landroid/content/Context;

    .line 2411001
    sget-object v4, LX/GsS;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/GsO;

    .line 2411002
    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.facebook.platform.PLATFORM_SERVICE"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, LX/GsO;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "android.intent.category.DEFAULT"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 2411003
    const/4 v7, 0x0

    .line 2411004
    if-nez v6, :cond_e

    move-object v6, v7

    .line 2411005
    :cond_4
    :goto_3
    move-object v4, v6

    .line 2411006
    if-eqz v4, :cond_3

    .line 2411007
    :goto_4
    move-object v3, v4

    .line 2411008
    if-eqz v3, :cond_0

    .line 2411009
    iput-boolean v2, v0, LX/GsU;->d:Z

    .line 2411010
    iget-object v1, v0, LX/GsU;->a:Landroid/content/Context;

    const v4, -0x64bf9e81

    invoke-static {v1, v3, v0, v2, v4}, LX/04O;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    move v1, v2

    .line 2411011
    goto :goto_0

    .line 2411012
    :cond_5
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/GsO;

    .line 2411013
    iget-object v9, v6, LX/GsO;->b:Ljava/util/TreeSet;

    if-nez v9, :cond_7

    .line 2411014
    const/4 v9, 0x0

    invoke-static {v6, v9}, LX/GsO;->a$redex0(LX/GsO;Z)V

    .line 2411015
    :cond_7
    iget-object v9, v6, LX/GsO;->b:Ljava/util/TreeSet;

    move-object v6, v9

    .line 2411016
    invoke-static {}, LX/GsS;->a()I

    move-result v9

    const/4 v12, -0x1

    .line 2411017
    array-length v10, v5

    add-int/lit8 v10, v10, -0x1

    .line 2411018
    invoke-virtual {v6}, Ljava/util/TreeSet;->descendingIterator()Ljava/util/Iterator;

    move-result-object v3

    move v11, v12

    move v13, v10

    .line 2411019
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_9

    .line 2411020
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 2411021
    invoke-static {v11, v4}, Ljava/lang/Math;->max(II)I

    move-result v10

    move v11, v13

    .line 2411022
    :goto_6
    if-ltz v11, :cond_8

    aget v13, v5, v11

    if-le v13, v4, :cond_8

    .line 2411023
    add-int/lit8 v11, v11, -0x1

    goto :goto_6

    .line 2411024
    :cond_8
    if-gez v11, :cond_b

    .line 2411025
    :cond_9
    :goto_7
    move v6, v12

    .line 2411026
    if-eq v6, v7, :cond_6

    goto/16 :goto_2

    :cond_a
    move v6, v7

    .line 2411027
    goto/16 :goto_2

    .line 2411028
    :cond_b
    aget v13, v5, v11

    if-ne v13, v4, :cond_c

    .line 2411029
    rem-int/lit8 v11, v11, 0x2

    if-nez v11, :cond_9

    invoke-static {v10, v9}, Ljava/lang/Math;->min(II)I

    move-result v12

    goto :goto_7

    :cond_c
    move v13, v11

    move v11, v10

    .line 2411030
    goto :goto_5

    :cond_d
    const/4 v4, 0x0

    goto :goto_4

    .line 2411031
    :cond_e
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v6, v9}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v8

    .line 2411032
    if-nez v8, :cond_f

    move-object v6, v7

    .line 2411033
    goto/16 :goto_3

    .line 2411034
    :cond_f
    iget-object v8, v8, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v8, v8, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-static {v3, v8}, LX/GsO;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_4

    move-object v6, v7

    .line 2411035
    goto/16 :goto_3
.end method

.method public final b(Lcom/facebook/login/LoginClient$Request;Landroid/os/Bundle;)V
    .locals 11

    .prologue
    .line 2410953
    sget-object v0, LX/GA9;->FACEBOOK_APPLICATION_SERVICE:LX/GA9;

    .line 2410954
    iget-object v1, p1, Lcom/facebook/login/LoginClient$Request;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2410955
    const/4 v7, 0x0

    .line 2410956
    const-string v2, "com.facebook.platform.extra.EXPIRES_SECONDS_SINCE_EPOCH"

    new-instance v3, Ljava/util/Date;

    const-wide/16 v4, 0x0

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-static {p2, v2, v3}, LX/Gsc;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/Date;)Ljava/util/Date;

    move-result-object v9

    .line 2410957
    const-string v2, "com.facebook.platform.extra.PERMISSIONS"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 2410958
    const-string v2, "com.facebook.platform.extra.ACCESS_TOKEN"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2410959
    invoke-static {v3}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2410960
    :goto_0
    move-object v0, v7

    .line 2410961
    iget-object v1, p0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    .line 2410962
    iget-object v2, v1, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    move-object v1, v2

    .line 2410963
    invoke-static {v1, v0}, Lcom/facebook/login/LoginClient$Result;->a(Lcom/facebook/login/LoginClient$Request;Lcom/facebook/AccessToken;)Lcom/facebook/login/LoginClient$Result;

    move-result-object v0

    .line 2410964
    iget-object v1, p0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    invoke-virtual {v1, v0}, Lcom/facebook/login/LoginClient;->a(Lcom/facebook/login/LoginClient$Result;)V

    .line 2410965
    return-void

    .line 2410966
    :cond_0
    const-string v2, "com.facebook.platform.extra.USER_ID"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2410967
    new-instance v2, Lcom/facebook/AccessToken;

    new-instance v10, Ljava/util/Date;

    invoke-direct {v10}, Ljava/util/Date;-><init>()V

    move-object v4, v1

    move-object v8, v0

    invoke-direct/range {v2 .. v10}, Lcom/facebook/AccessToken;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Ljava/util/Collection;LX/GA9;Ljava/util/Date;Ljava/util/Date;)V

    move-object v7, v2

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2410952
    const-string v0, "get_token"

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2410951
    const/4 v0, 0x0

    return v0
.end method
