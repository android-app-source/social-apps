.class public Lcom/facebook/login/LoginClient;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/login/LoginClient;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:[Lcom/facebook/login/LoginMethodHandler;

.field public b:I

.field public c:Landroid/support/v4/app/Fragment;

.field public d:LX/GzB;

.field public e:LX/GzC;

.field public f:Z

.field public g:Lcom/facebook/login/LoginClient$Request;

.field public h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/GzD;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2411318
    new-instance v0, LX/Gz7;

    invoke-direct {v0}, LX/Gz7;-><init>()V

    sput-object v0, Lcom/facebook/login/LoginClient;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    .line 2411319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2411320
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/login/LoginClient;->b:I

    .line 2411321
    const-class v0, Lcom/facebook/login/LoginMethodHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v2

    .line 2411322
    array-length v0, v2

    new-array v0, v0, [Lcom/facebook/login/LoginMethodHandler;

    iput-object v0, p0, Lcom/facebook/login/LoginClient;->a:[Lcom/facebook/login/LoginMethodHandler;

    .line 2411323
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_0

    .line 2411324
    iget-object v3, p0, Lcom/facebook/login/LoginClient;->a:[Lcom/facebook/login/LoginMethodHandler;

    aget-object v0, v2, v1

    check-cast v0, Lcom/facebook/login/LoginMethodHandler;

    aput-object v0, v3, v1

    .line 2411325
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->a:[Lcom/facebook/login/LoginMethodHandler;

    aget-object v0, v0, v1

    invoke-virtual {v0, p0}, Lcom/facebook/login/LoginMethodHandler;->a(Lcom/facebook/login/LoginClient;)V

    .line 2411326
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2411327
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/login/LoginClient;->b:I

    .line 2411328
    const-class v0, Lcom/facebook/login/LoginClient$Request;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/login/LoginClient$Request;

    iput-object v0, p0, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    .line 2411329
    invoke-static {p1}, LX/Gsc;->a(Landroid/os/Parcel;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/login/LoginClient;->h:Ljava/util/Map;

    .line 2411330
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 2411331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2411332
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/login/LoginClient;->b:I

    .line 2411333
    iput-object p1, p0, Lcom/facebook/login/LoginClient;->c:Landroid/support/v4/app/Fragment;

    .line 2411334
    return-void
.end method

.method public static a(Lcom/facebook/login/LoginClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2411335
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    if-nez v0, :cond_0

    .line 2411336
    invoke-static {p0}, Lcom/facebook/login/LoginClient;->o(Lcom/facebook/login/LoginClient;)LX/GzD;

    move-result-object v0

    const-string v1, "fb_mobile_login_method_complete"

    const-string v2, "Unexpected call to logCompleteLogin with null pendingAuthorizationRequest."

    .line 2411337
    const-string v3, ""

    invoke-static {v3}, LX/GzD;->a(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 2411338
    const-string v4, "2_result"

    sget-object v5, LX/GzA;->ERROR:LX/GzA;

    invoke-virtual {v5}, LX/GzA;->getLoggingValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411339
    const-string v4, "5_error_message"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411340
    const-string v4, "3_method"

    invoke-virtual {v3, v4, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411341
    iget-object v4, v0, LX/GzD;->a:LX/GR4;

    const/4 v5, 0x0

    invoke-virtual {v4, v1, v5, v3}, LX/GR4;->a(Ljava/lang/String;Ljava/lang/Double;Landroid/os/Bundle;)V

    .line 2411342
    :goto_0
    return-void

    .line 2411343
    :cond_0
    invoke-static {p0}, Lcom/facebook/login/LoginClient;->o(Lcom/facebook/login/LoginClient;)LX/GzD;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    .line 2411344
    iget-object v2, v1, Lcom/facebook/login/LoginClient$Request;->e:Ljava/lang/String;

    move-object v1, v2

    .line 2411345
    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 2411346
    invoke-static {v1}, LX/GzD;->a(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p0

    .line 2411347
    if-eqz v3, :cond_1

    .line 2411348
    const-string p1, "2_result"

    invoke-virtual {p0, p1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411349
    :cond_1
    if-eqz v4, :cond_2

    .line 2411350
    const-string p1, "5_error_message"

    invoke-virtual {p0, p1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411351
    :cond_2
    if-eqz v5, :cond_3

    .line 2411352
    const-string p1, "4_error_code"

    invoke-virtual {p0, p1, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411353
    :cond_3
    if-eqz v6, :cond_4

    invoke-interface {v6}, Ljava/util/Map;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_4

    .line 2411354
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1, v6}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 2411355
    const-string p2, "6_extras"

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411356
    :cond_4
    const-string p1, "3_method"

    invoke-virtual {p0, p1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411357
    iget-object p1, v0, LX/GzD;->a:LX/GR4;

    const-string p2, "fb_mobile_login_method_complete"

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3, p0}, LX/GR4;->a(Ljava/lang/String;Ljava/lang/Double;Landroid/os/Bundle;)V

    .line 2411358
    goto :goto_0
.end method

.method private static a(Lcom/facebook/login/LoginClient;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 2411379
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->h:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 2411380
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/login/LoginClient;->h:Ljava/util/Map;

    .line 2411381
    :cond_0
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p3, :cond_1

    .line 2411382
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/facebook/login/LoginClient;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 2411383
    :cond_1
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->h:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2411384
    return-void
.end method

.method public static b(Lcom/facebook/login/LoginClient;Lcom/facebook/login/LoginClient$Request;)V
    .locals 3

    .prologue
    .line 2411359
    if-nez p1, :cond_1

    .line 2411360
    :cond_0
    :goto_0
    return-void

    .line 2411361
    :cond_1
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    if-eqz v0, :cond_2

    .line 2411362
    new-instance v0, LX/GAA;

    const-string v1, "Attempted to authorize while a request is pending."

    invoke-direct {v0, v1}, LX/GAA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2411363
    :cond_2
    invoke-static {}, Lcom/facebook/AccessToken;->a()Lcom/facebook/AccessToken;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/facebook/login/LoginClient;->l(Lcom/facebook/login/LoginClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2411364
    :cond_3
    iput-object p1, p0, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    .line 2411365
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2411366
    iget-object v1, p1, Lcom/facebook/login/LoginClient$Request;->a:LX/Gz6;

    move-object v1, v1

    .line 2411367
    invoke-virtual {v1}, LX/Gz6;->allowsKatanaAuth()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2411368
    new-instance v2, Lcom/facebook/login/GetTokenLoginMethodHandler;

    invoke-direct {v2, p0}, Lcom/facebook/login/GetTokenLoginMethodHandler;-><init>(Lcom/facebook/login/LoginClient;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2411369
    new-instance v2, Lcom/facebook/login/KatanaProxyLoginMethodHandler;

    invoke-direct {v2, p0}, Lcom/facebook/login/KatanaProxyLoginMethodHandler;-><init>(Lcom/facebook/login/LoginClient;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2411370
    :cond_4
    invoke-virtual {v1}, LX/Gz6;->allowsWebViewAuth()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2411371
    new-instance v2, Lcom/facebook/login/WebViewLoginMethodHandler;

    invoke-direct {v2, p0}, Lcom/facebook/login/WebViewLoginMethodHandler;-><init>(Lcom/facebook/login/LoginClient;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2411372
    :cond_5
    invoke-virtual {v1}, LX/Gz6;->allowsDeviceAuth()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2411373
    new-instance v1, Lcom/facebook/login/DeviceAuthMethodHandler;

    invoke-direct {v1, p0}, Lcom/facebook/login/DeviceAuthMethodHandler;-><init>(Lcom/facebook/login/LoginClient;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2411374
    :cond_6
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/facebook/login/LoginMethodHandler;

    .line 2411375
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 2411376
    move-object v0, v1

    .line 2411377
    iput-object v0, p0, Lcom/facebook/login/LoginClient;->a:[Lcom/facebook/login/LoginMethodHandler;

    .line 2411378
    invoke-virtual {p0}, Lcom/facebook/login/LoginClient;->g()V

    goto :goto_0
.end method

.method private c(Lcom/facebook/login/LoginClient$Result;)V
    .locals 3

    .prologue
    .line 2411396
    iget-object v0, p1, Lcom/facebook/login/LoginClient$Result;->b:Lcom/facebook/AccessToken;

    if-nez v0, :cond_0

    .line 2411397
    new-instance v0, LX/GAA;

    const-string v1, "Can\'t validate without a token"

    invoke-direct {v0, v1}, LX/GAA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2411398
    :cond_0
    invoke-static {}, Lcom/facebook/AccessToken;->a()Lcom/facebook/AccessToken;

    move-result-object v0

    .line 2411399
    iget-object v1, p1, Lcom/facebook/login/LoginClient$Result;->b:Lcom/facebook/AccessToken;

    .line 2411400
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 2411401
    :try_start_0
    iget-object v2, v0, Lcom/facebook/AccessToken;->l:Ljava/lang/String;

    move-object v0, v2

    .line 2411402
    iget-object v2, v1, Lcom/facebook/AccessToken;->l:Ljava/lang/String;

    move-object v1, v2

    .line 2411403
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2411404
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    iget-object v1, p1, Lcom/facebook/login/LoginClient$Result;->b:Lcom/facebook/AccessToken;

    invoke-static {v0, v1}, Lcom/facebook/login/LoginClient$Result;->a(Lcom/facebook/login/LoginClient$Request;Lcom/facebook/AccessToken;)Lcom/facebook/login/LoginClient$Result;

    move-result-object v0

    .line 2411405
    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/login/LoginClient;->b(Lcom/facebook/login/LoginClient$Result;)V

    .line 2411406
    :goto_1
    return-void

    .line 2411407
    :cond_1
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    const-string v1, "User logged in as different Facebook user."

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/facebook/login/LoginClient$Result;->a(Lcom/facebook/login/LoginClient$Request;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/login/LoginClient$Result;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 2411408
    :catch_0
    move-exception v0

    .line 2411409
    iget-object v1, p0, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    const-string v2, "Caught exception"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/facebook/login/LoginClient$Result;->a(Lcom/facebook/login/LoginClient$Request;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/login/LoginClient$Result;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/login/LoginClient;->b(Lcom/facebook/login/LoginClient$Result;)V

    goto :goto_1
.end method

.method public static j()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2411410
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 2411411
    :try_start_0
    const-string v1, "init"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2411412
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catch_0
    goto :goto_0
.end method

.method private static l(Lcom/facebook/login/LoginClient;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2411385
    iget-boolean v1, p0, Lcom/facebook/login/LoginClient;->f:Z

    if-eqz v1, :cond_0

    .line 2411386
    :goto_0
    return v0

    .line 2411387
    :cond_0
    const-string v1, "android.permission.INTERNET"

    .line 2411388
    invoke-virtual {p0}, Lcom/facebook/login/LoginClient;->b()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/FragmentActivity;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v2

    move v1, v2

    .line 2411389
    if-eqz v1, :cond_1

    .line 2411390
    invoke-virtual {p0}, Lcom/facebook/login/LoginClient;->b()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2411391
    const v1, 0x7f082893

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2411392
    const v2, 0x7f082894

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2411393
    iget-object v2, p0, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    invoke-static {v2, v1, v0}, Lcom/facebook/login/LoginClient$Result;->a(Lcom/facebook/login/LoginClient$Request;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/login/LoginClient$Result;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/login/LoginClient;->b(Lcom/facebook/login/LoginClient$Result;)V

    .line 2411394
    const/4 v0, 0x0

    goto :goto_0

    .line 2411395
    :cond_1
    iput-boolean v0, p0, Lcom/facebook/login/LoginClient;->f:Z

    goto :goto_0
.end method

.method private n()Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 2411295
    invoke-virtual {p0}, Lcom/facebook/login/LoginClient;->f()Lcom/facebook/login/LoginMethodHandler;

    move-result-object v1

    .line 2411296
    invoke-virtual {v1}, Lcom/facebook/login/LoginMethodHandler;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/facebook/login/LoginClient;->l(Lcom/facebook/login/LoginClient;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2411297
    const-string v1, "no_internet_permission"

    const-string v2, "1"

    invoke-static {p0, v1, v2, v0}, Lcom/facebook/login/LoginClient;->a(Lcom/facebook/login/LoginClient;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2411298
    :goto_0
    return v0

    .line 2411299
    :cond_0
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    invoke-virtual {v1, v0}, Lcom/facebook/login/LoginMethodHandler;->a(Lcom/facebook/login/LoginClient$Request;)Z

    move-result v0

    .line 2411300
    if-eqz v0, :cond_1

    .line 2411301
    invoke-static {p0}, Lcom/facebook/login/LoginClient;->o(Lcom/facebook/login/LoginClient;)LX/GzD;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    .line 2411302
    iget-object p0, v3, Lcom/facebook/login/LoginClient$Request;->e:Ljava/lang/String;

    move-object v3, p0

    .line 2411303
    invoke-virtual {v1}, Lcom/facebook/login/LoginMethodHandler;->c()Ljava/lang/String;

    move-result-object v1

    .line 2411304
    invoke-static {v3}, LX/GzD;->a(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    .line 2411305
    const-string v5, "3_method"

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411306
    iget-object v5, v2, LX/GzD;->a:LX/GR4;

    const-string v6, "fb_mobile_login_method_start"

    const/4 p0, 0x0

    invoke-virtual {v5, v6, p0, v4}, LX/GR4;->a(Ljava/lang/String;Ljava/lang/Double;Landroid/os/Bundle;)V

    .line 2411307
    goto :goto_0

    .line 2411308
    :cond_1
    const-string v2, "not_tried"

    invoke-virtual {v1}, Lcom/facebook/login/LoginMethodHandler;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-static {p0, v2, v1, v3}, Lcom/facebook/login/LoginClient;->a(Lcom/facebook/login/LoginClient;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private static o(Lcom/facebook/login/LoginClient;)LX/GzD;
    .locals 4

    .prologue
    .line 2411309
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->i:LX/GzD;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/login/LoginClient;->i:LX/GzD;

    .line 2411310
    iget-object v1, v0, LX/GzD;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2411311
    iget-object v1, p0, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    .line 2411312
    iget-object v2, v1, Lcom/facebook/login/LoginClient$Request;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2411313
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2411314
    :cond_0
    new-instance v0, LX/GzD;

    invoke-virtual {p0}, Lcom/facebook/login/LoginClient;->b()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    .line 2411315
    iget-object v3, v2, Lcom/facebook/login/LoginClient$Request;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2411316
    invoke-direct {v0, v1, v2}, LX/GzD;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/login/LoginClient;->i:LX/GzD;

    .line 2411317
    :cond_1
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->i:LX/GzD;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 2411291
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->c:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    .line 2411292
    new-instance v0, LX/GAA;

    const-string v1, "Can\'t set fragment once it is already set."

    invoke-direct {v0, v1}, LX/GAA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2411293
    :cond_0
    iput-object p1, p0, Lcom/facebook/login/LoginClient;->c:Landroid/support/v4/app/Fragment;

    .line 2411294
    return-void
.end method

.method public final a(Lcom/facebook/login/LoginClient$Result;)V
    .locals 1

    .prologue
    .line 2411287
    iget-object v0, p1, Lcom/facebook/login/LoginClient$Result;->b:Lcom/facebook/AccessToken;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/facebook/AccessToken;->a()Lcom/facebook/AccessToken;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2411288
    invoke-direct {p0, p1}, Lcom/facebook/login/LoginClient;->c(Lcom/facebook/login/LoginClient$Result;)V

    .line 2411289
    :goto_0
    return-void

    .line 2411290
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/login/LoginClient;->b(Lcom/facebook/login/LoginClient$Result;)V

    goto :goto_0
.end method

.method public final b()Landroid/support/v4/app/FragmentActivity;
    .locals 1

    .prologue
    .line 2411286
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->c:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/login/LoginClient$Result;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 2411262
    invoke-virtual {p0}, Lcom/facebook/login/LoginClient;->f()Lcom/facebook/login/LoginMethodHandler;

    move-result-object v0

    .line 2411263
    if-eqz v0, :cond_0

    .line 2411264
    invoke-virtual {v0}, Lcom/facebook/login/LoginMethodHandler;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Lcom/facebook/login/LoginMethodHandler;->a:Ljava/util/Map;

    .line 2411265
    iget-object v3, p1, Lcom/facebook/login/LoginClient$Result;->a:LX/GzA;

    invoke-virtual {v3}, LX/GzA;->getLoggingValue()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p1, Lcom/facebook/login/LoginClient$Result;->c:Ljava/lang/String;

    iget-object v7, p1, Lcom/facebook/login/LoginClient$Result;->d:Ljava/lang/String;

    move-object v3, p0

    move-object v4, v1

    move-object v8, v0

    invoke-static/range {v3 .. v8}, Lcom/facebook/login/LoginClient;->a(Lcom/facebook/login/LoginClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 2411266
    :cond_0
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->h:Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 2411267
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->h:Ljava/util/Map;

    iput-object v0, p1, Lcom/facebook/login/LoginClient$Result;->f:Ljava/util/Map;

    .line 2411268
    :cond_1
    iput-object v2, p0, Lcom/facebook/login/LoginClient;->a:[Lcom/facebook/login/LoginMethodHandler;

    .line 2411269
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/login/LoginClient;->b:I

    .line 2411270
    iput-object v2, p0, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    .line 2411271
    iput-object v2, p0, Lcom/facebook/login/LoginClient;->h:Ljava/util/Map;

    .line 2411272
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->d:LX/GzB;

    if-eqz v0, :cond_2

    .line 2411273
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->d:LX/GzB;

    .line 2411274
    iget-object v1, v0, LX/GzB;->a:Lcom/facebook/login/LoginFragment;

    .line 2411275
    const/4 v2, 0x0

    iput-object v2, v1, Lcom/facebook/login/LoginFragment;->c:Lcom/facebook/login/LoginClient$Request;

    .line 2411276
    iget-object v2, p1, Lcom/facebook/login/LoginClient$Result;->a:LX/GzA;

    sget-object p0, LX/GzA;->CANCEL:LX/GzA;

    if-ne v2, p0, :cond_3

    const/4 v2, 0x0

    .line 2411277
    :goto_0
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2411278
    const-string v0, "com.facebook.LoginFragment:Result"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2411279
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2411280
    invoke-virtual {v0, p0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2411281
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result p0

    if-eqz p0, :cond_2

    .line 2411282
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p0

    invoke-virtual {p0, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2411283
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2411284
    :cond_2
    return-void

    .line 2411285
    :cond_3
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public final c()Lcom/facebook/login/LoginClient$Request;
    .locals 1

    .prologue
    .line 2411261
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2411260
    const/4 v0, 0x0

    return v0
.end method

.method public final f()Lcom/facebook/login/LoginMethodHandler;
    .locals 2

    .prologue
    .line 2411257
    iget v0, p0, Lcom/facebook/login/LoginClient;->b:I

    if-ltz v0, :cond_0

    .line 2411258
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->a:[Lcom/facebook/login/LoginMethodHandler;

    iget v1, p0, Lcom/facebook/login/LoginClient;->b:I

    aget-object v0, v0, v1

    .line 2411259
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 2411247
    iget v0, p0, Lcom/facebook/login/LoginClient;->b:I

    if-ltz v0, :cond_0

    .line 2411248
    invoke-virtual {p0}, Lcom/facebook/login/LoginClient;->f()Lcom/facebook/login/LoginMethodHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/login/LoginMethodHandler;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "skipped"

    invoke-virtual {p0}, Lcom/facebook/login/LoginClient;->f()Lcom/facebook/login/LoginMethodHandler;

    move-result-object v0

    iget-object v5, v0, Lcom/facebook/login/LoginMethodHandler;->a:Ljava/util/Map;

    move-object v0, p0

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/facebook/login/LoginClient;->a(Lcom/facebook/login/LoginClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 2411249
    :cond_0
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->a:[Lcom/facebook/login/LoginMethodHandler;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/facebook/login/LoginClient;->b:I

    iget-object v1, p0, Lcom/facebook/login/LoginClient;->a:[Lcom/facebook/login/LoginMethodHandler;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 2411250
    iget v0, p0, Lcom/facebook/login/LoginClient;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/login/LoginClient;->b:I

    .line 2411251
    invoke-direct {p0}, Lcom/facebook/login/LoginClient;->n()Z

    move-result v0

    .line 2411252
    if-eqz v0, :cond_0

    .line 2411253
    :cond_1
    :goto_0
    return-void

    .line 2411254
    :cond_2
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    if-eqz v0, :cond_1

    .line 2411255
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    const-string v1, "Login attempt failed."

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/facebook/login/LoginClient$Result;->a(Lcom/facebook/login/LoginClient$Request;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/login/LoginClient$Result;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/login/LoginClient;->b(Lcom/facebook/login/LoginClient$Result;)V

    .line 2411256
    goto :goto_0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 2411243
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->e:LX/GzC;

    if-eqz v0, :cond_0

    .line 2411244
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->e:LX/GzC;

    .line 2411245
    iget-object v1, v0, LX/GzC;->a:Landroid/view/View;

    const p0, 0x7f0d09aa

    invoke-virtual {v1, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    .line 2411246
    :cond_0
    return-void
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2411238
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->a:[Lcom/facebook/login/LoginMethodHandler;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 2411239
    iget v0, p0, Lcom/facebook/login/LoginClient;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2411240
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2411241
    iget-object v0, p0, Lcom/facebook/login/LoginClient;->h:Ljava/util/Map;

    invoke-static {p1, v0}, LX/Gsc;->a(Landroid/os/Parcel;Ljava/util/Map;)V

    .line 2411242
    return-void
.end method
