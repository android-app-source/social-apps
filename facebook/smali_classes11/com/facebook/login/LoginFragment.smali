.class public Lcom/facebook/login/LoginFragment;
.super Landroid/support/v4/app/Fragment;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/facebook/login/LoginClient;

.field public c:Lcom/facebook/login/LoginClient$Request;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2411465
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2411460
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2411461
    iget-object v0, p0, Lcom/facebook/login/LoginFragment;->b:Lcom/facebook/login/LoginClient;

    .line 2411462
    iget-object p0, v0, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    if-eqz p0, :cond_0

    .line 2411463
    invoke-virtual {v0}, Lcom/facebook/login/LoginClient;->f()Lcom/facebook/login/LoginMethodHandler;

    move-result-object p0

    invoke-virtual {p0, p2, p3}, Lcom/facebook/login/LoginMethodHandler;->a(ILandroid/content/Intent;)Z

    .line 2411464
    :goto_0
    return-void

    :cond_0
    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x6a3cabfb

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2411441
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 2411442
    if-eqz p1, :cond_0

    .line 2411443
    const-string v0, "loginClient"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/login/LoginClient;

    iput-object v0, p0, Lcom/facebook/login/LoginFragment;->b:Lcom/facebook/login/LoginClient;

    .line 2411444
    iget-object v0, p0, Lcom/facebook/login/LoginFragment;->b:Lcom/facebook/login/LoginClient;

    invoke-virtual {v0, p0}, Lcom/facebook/login/LoginClient;->a(Landroid/support/v4/app/Fragment;)V

    .line 2411445
    :goto_0
    iget-object v0, p0, Lcom/facebook/login/LoginFragment;->b:Lcom/facebook/login/LoginClient;

    new-instance v2, LX/GzB;

    invoke-direct {v2, p0}, LX/GzB;-><init>(Lcom/facebook/login/LoginFragment;)V

    .line 2411446
    iput-object v2, v0, Lcom/facebook/login/LoginClient;->d:LX/GzB;

    .line 2411447
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2411448
    if-nez v0, :cond_1

    .line 2411449
    const v0, 0x8d03163

    invoke-static {v0, v1}, LX/02F;->f(II)V

    .line 2411450
    :goto_1
    return-void

    .line 2411451
    :cond_0
    new-instance v0, Lcom/facebook/login/LoginClient;

    invoke-direct {v0, p0}, Lcom/facebook/login/LoginClient;-><init>(Landroid/support/v4/app/Fragment;)V

    iput-object v0, p0, Lcom/facebook/login/LoginFragment;->b:Lcom/facebook/login/LoginClient;

    goto :goto_0

    .line 2411452
    :cond_1
    invoke-virtual {v0}, Landroid/app/Activity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v2

    .line 2411453
    if-nez v2, :cond_3

    .line 2411454
    :goto_2
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2411455
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2411456
    const-class v2, Lcom/facebook/login/LoginClient$Request;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    .line 2411457
    const-string v2, "request"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/login/LoginClient$Request;

    iput-object v0, p0, Lcom/facebook/login/LoginFragment;->c:Lcom/facebook/login/LoginClient$Request;

    .line 2411458
    :cond_2
    const v0, -0x57e59b34

    invoke-static {v0, v1}, LX/02F;->f(II)V

    goto :goto_1

    .line 2411459
    :cond_3
    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/login/LoginFragment;->a:Ljava/lang/String;

    goto :goto_2
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x601fc0b0

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2411437
    const v1, 0x7f0302bc

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2411438
    iget-object v2, p0, Lcom/facebook/login/LoginFragment;->b:Lcom/facebook/login/LoginClient;

    new-instance v3, LX/GzC;

    invoke-direct {v3, p0, v1}, LX/GzC;-><init>(Lcom/facebook/login/LoginFragment;Landroid/view/View;)V

    .line 2411439
    iput-object v3, v2, Lcom/facebook/login/LoginClient;->e:LX/GzC;

    .line 2411440
    const/16 v2, 0x2b

    const v3, 0x12ef1cac

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x45d2e1ab

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2411432
    iget-object v1, p0, Lcom/facebook/login/LoginFragment;->b:Lcom/facebook/login/LoginClient;

    .line 2411433
    iget v2, v1, Lcom/facebook/login/LoginClient;->b:I

    if-ltz v2, :cond_0

    .line 2411434
    invoke-virtual {v1}, Lcom/facebook/login/LoginClient;->f()Lcom/facebook/login/LoginMethodHandler;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/login/LoginMethodHandler;->a()V

    .line 2411435
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 2411436
    const/16 v1, 0x2b

    const v2, -0x118e93ad

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x22cd19e0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2411415
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 2411416
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0d09aa

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2411417
    const/16 v1, 0x2b

    const v2, 0x6782818a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x319ea92b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2411421
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 2411422
    iget-object v1, p0, Lcom/facebook/login/LoginFragment;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2411423
    const-string v1, "LoginFragment"

    const-string v2, "Cannot call LoginFragment with a null calling package. This can occur if the launchMode of the caller is singleInstance."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2411424
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2411425
    const/16 v1, 0x2b

    const v2, -0x54f847b9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2411426
    :goto_0
    return-void

    .line 2411427
    :cond_0
    iget-object v1, p0, Lcom/facebook/login/LoginFragment;->b:Lcom/facebook/login/LoginClient;

    iget-object v2, p0, Lcom/facebook/login/LoginFragment;->c:Lcom/facebook/login/LoginClient$Request;

    .line 2411428
    iget-object v3, v1, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    if-eqz v3, :cond_2

    iget v3, v1, Lcom/facebook/login/LoginClient;->b:I

    if-ltz v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    move v3, v3

    .line 2411429
    if-nez v3, :cond_1

    .line 2411430
    invoke-static {v1, v2}, Lcom/facebook/login/LoginClient;->b(Lcom/facebook/login/LoginClient;Lcom/facebook/login/LoginClient$Request;)V

    .line 2411431
    :cond_1
    const v1, -0x4d56ffa6

    invoke-static {v1, v0}, LX/02F;->f(II)V

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2411418
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2411419
    const-string v0, "loginClient"

    iget-object v1, p0, Lcom/facebook/login/LoginFragment;->b:Lcom/facebook/login/LoginClient;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2411420
    return-void
.end method
