.class public Lcom/facebook/login/DeviceAuthDialog;
.super Landroid/support/v4/app/DialogFragment;
.source ""


# instance fields
.field private j:Landroid/widget/ProgressBar;

.field private k:Landroid/widget/TextView;

.field public l:Lcom/facebook/login/DeviceAuthMethodHandler;

.field public m:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private volatile n:LX/GAV;

.field private volatile o:Ljava/util/concurrent/ScheduledFuture;

.field public volatile p:Lcom/facebook/login/DeviceAuthDialog$RequestState;

.field public q:Landroid/app/Dialog;

.field private r:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2410760
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 2410761
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2410762
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/login/DeviceAuthDialog;->r:Z

    .line 2410763
    return-void
.end method

.method public static a$redex0(Lcom/facebook/login/DeviceAuthDialog;)V
    .locals 10

    .prologue
    .line 2410838
    iget-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->p:Lcom/facebook/login/DeviceAuthDialog$RequestState;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 2410839
    iput-wide v2, v0, Lcom/facebook/login/DeviceAuthDialog$RequestState;->d:J

    .line 2410840
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 2410841
    const-string v4, "type"

    const-string v5, "device_token"

    invoke-virtual {v7, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2410842
    const-string v4, "client_id"

    invoke-static {}, LX/GAK;->i()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2410843
    const-string v4, "code"

    iget-object v5, p0, Lcom/facebook/login/DeviceAuthDialog;->p:Lcom/facebook/login/DeviceAuthDialog$RequestState;

    .line 2410844
    iget-object v6, v5, Lcom/facebook/login/DeviceAuthDialog$RequestState;->b:Ljava/lang/String;

    move-object v5, v6

    .line 2410845
    invoke-virtual {v7, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2410846
    new-instance v4, LX/GAU;

    const/4 v5, 0x0

    const-string v6, "oauth/device"

    sget-object v8, LX/GAZ;->POST:LX/GAZ;

    new-instance v9, LX/Gyx;

    invoke-direct {v9, p0}, LX/Gyx;-><init>(Lcom/facebook/login/DeviceAuthDialog;)V

    invoke-direct/range {v4 .. v9}, LX/GAU;-><init>(Lcom/facebook/AccessToken;Ljava/lang/String;Landroid/os/Bundle;LX/GAZ;LX/GA2;)V

    move-object v0, v4

    .line 2410847
    invoke-virtual {v0}, LX/GAU;->g()LX/GAV;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->n:LX/GAV;

    .line 2410848
    return-void
.end method

.method public static a$redex0(Lcom/facebook/login/DeviceAuthDialog;LX/GAA;)V
    .locals 4

    .prologue
    .line 2410830
    iget-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2410831
    :goto_0
    return-void

    .line 2410832
    :cond_0
    iget-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->l:Lcom/facebook/login/DeviceAuthMethodHandler;

    .line 2410833
    iget-object v1, v0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    .line 2410834
    iget-object v2, v1, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    move-object v1, v2

    .line 2410835
    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/login/LoginClient$Result;->a(Lcom/facebook/login/LoginClient$Request;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/login/LoginClient$Result;

    move-result-object v1

    .line 2410836
    iget-object v2, v0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    invoke-virtual {v2, v1}, Lcom/facebook/login/LoginClient;->a(Lcom/facebook/login/LoginClient$Result;)V

    .line 2410837
    iget-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->q:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/login/DeviceAuthDialog;Lcom/facebook/login/DeviceAuthDialog$RequestState;)V
    .locals 12

    .prologue
    .line 2410815
    iput-object p1, p0, Lcom/facebook/login/DeviceAuthDialog;->p:Lcom/facebook/login/DeviceAuthDialog$RequestState;

    .line 2410816
    iget-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->k:Landroid/widget/TextView;

    .line 2410817
    iget-object v1, p1, Lcom/facebook/login/DeviceAuthDialog$RequestState;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2410818
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2410819
    iget-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->k:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2410820
    iget-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->j:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2410821
    const-wide/16 v10, 0x0

    const/4 v2, 0x0

    .line 2410822
    iget-wide v4, p1, Lcom/facebook/login/DeviceAuthDialog$RequestState;->d:J

    cmp-long v3, v4, v10

    if-nez v3, :cond_2

    .line 2410823
    :cond_0
    :goto_0
    move v0, v2

    .line 2410824
    if-eqz v0, :cond_1

    .line 2410825
    invoke-static {p0}, Lcom/facebook/login/DeviceAuthDialog;->b$redex0(Lcom/facebook/login/DeviceAuthDialog;)V

    .line 2410826
    :goto_1
    return-void

    .line 2410827
    :cond_1
    invoke-static {p0}, Lcom/facebook/login/DeviceAuthDialog;->a$redex0(Lcom/facebook/login/DeviceAuthDialog;)V

    goto :goto_1

    .line 2410828
    :cond_2
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    iget-wide v6, p1, Lcom/facebook/login/DeviceAuthDialog$RequestState;->d:J

    sub-long/2addr v4, v6

    iget-wide v6, p1, Lcom/facebook/login/DeviceAuthDialog$RequestState;->c:J

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    sub-long/2addr v4, v6

    .line 2410829
    cmp-long v3, v4, v10

    if-gez v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static b$redex0(Lcom/facebook/login/DeviceAuthDialog;)V
    .locals 7

    .prologue
    .line 2410811
    invoke-static {}, Lcom/facebook/login/DeviceAuthMethodHandler;->b()Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    move-result-object v0

    new-instance v1, Lcom/facebook/login/DeviceAuthDialog$3;

    invoke-direct {v1, p0}, Lcom/facebook/login/DeviceAuthDialog$3;-><init>(Lcom/facebook/login/DeviceAuthDialog;)V

    iget-object v2, p0, Lcom/facebook/login/DeviceAuthDialog;->p:Lcom/facebook/login/DeviceAuthDialog$RequestState;

    .line 2410812
    iget-wide v5, v2, Lcom/facebook/login/DeviceAuthDialog$RequestState;->c:J

    move-wide v2, v5

    .line 2410813
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->o:Ljava/util/concurrent/ScheduledFuture;

    .line 2410814
    return-void
.end method

.method public static i(Lcom/facebook/login/DeviceAuthDialog;)V
    .locals 3

    .prologue
    .line 2410802
    iget-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2410803
    :goto_0
    return-void

    .line 2410804
    :cond_0
    iget-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->l:Lcom/facebook/login/DeviceAuthMethodHandler;

    if-eqz v0, :cond_1

    .line 2410805
    iget-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->l:Lcom/facebook/login/DeviceAuthMethodHandler;

    .line 2410806
    iget-object v1, v0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    .line 2410807
    iget-object v2, v1, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    move-object v1, v2

    .line 2410808
    const-string v2, "User canceled log in."

    invoke-static {v1, v2}, Lcom/facebook/login/LoginClient$Result;->a(Lcom/facebook/login/LoginClient$Request;Ljava/lang/String;)Lcom/facebook/login/LoginClient$Result;

    move-result-object v1

    .line 2410809
    iget-object v2, v0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    invoke-virtual {v2, v1}, Lcom/facebook/login/LoginClient;->a(Lcom/facebook/login/LoginClient$Result;)V

    .line 2410810
    :cond_1
    iget-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->q:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 2410791
    new-instance v0, Landroid/app/Dialog;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0e09fe

    invoke-direct {v0, v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->q:Landroid/app/Dialog;

    .line 2410792
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2410793
    const v1, 0x7f0302bb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2410794
    const v0, 0x7f0d04de

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->j:Landroid/widget/ProgressBar;

    .line 2410795
    const v0, 0x7f0d09a8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->k:Landroid/widget/TextView;

    .line 2410796
    const v0, 0x7f0d09a9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 2410797
    new-instance v2, LX/Gyv;

    invoke-direct {v2, p0}, LX/Gyv;-><init>(Lcom/facebook/login/DeviceAuthDialog;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2410798
    const v0, 0x7f0d09a7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2410799
    const v2, 0x7f082899

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2410800
    iget-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->q:Landroid/app/Dialog;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 2410801
    iget-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->q:Landroid/app/Dialog;

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x40c03850

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2410780
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/DialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 2410781
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/facebook/FacebookActivity;

    .line 2410782
    iget-object v3, v0, Lcom/facebook/FacebookActivity;->r:Landroid/support/v4/app/Fragment;

    move-object v0, v3

    .line 2410783
    check-cast v0, Lcom/facebook/login/LoginFragment;

    .line 2410784
    iget-object v3, v0, Lcom/facebook/login/LoginFragment;->b:Lcom/facebook/login/LoginClient;

    move-object v0, v3

    .line 2410785
    invoke-virtual {v0}, Lcom/facebook/login/LoginClient;->f()Lcom/facebook/login/LoginMethodHandler;

    move-result-object v0

    check-cast v0, Lcom/facebook/login/DeviceAuthMethodHandler;

    iput-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->l:Lcom/facebook/login/DeviceAuthMethodHandler;

    .line 2410786
    if-eqz p3, :cond_0

    .line 2410787
    const-string v0, "request_state"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/login/DeviceAuthDialog$RequestState;

    .line 2410788
    if-eqz v0, :cond_0

    .line 2410789
    invoke-static {p0, v0}, Lcom/facebook/login/DeviceAuthDialog;->a$redex0(Lcom/facebook/login/DeviceAuthDialog;Lcom/facebook/login/DeviceAuthDialog$RequestState;)V

    .line 2410790
    :cond_0
    const/16 v0, 0x2b

    const v3, 0x3ce2cb99

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/16 v0, 0x2a

    const v1, -0x17cf02a6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2410772
    iput-boolean v2, p0, Lcom/facebook/login/DeviceAuthDialog;->r:Z

    .line 2410773
    iget-object v1, p0, Lcom/facebook/login/DeviceAuthDialog;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2410774
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDestroy()V

    .line 2410775
    iget-object v1, p0, Lcom/facebook/login/DeviceAuthDialog;->n:LX/GAV;

    if-eqz v1, :cond_0

    .line 2410776
    iget-object v1, p0, Lcom/facebook/login/DeviceAuthDialog;->n:LX/GAV;

    invoke-virtual {v1, v2}, LX/GAV;->cancel(Z)Z

    .line 2410777
    :cond_0
    iget-object v1, p0, Lcom/facebook/login/DeviceAuthDialog;->o:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v1, :cond_1

    .line 2410778
    iget-object v1, p0, Lcom/facebook/login/DeviceAuthDialog;->o:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v1, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2410779
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x7c75916f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 2410768
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 2410769
    iget-boolean v0, p0, Lcom/facebook/login/DeviceAuthDialog;->r:Z

    if-nez v0, :cond_0

    .line 2410770
    invoke-static {p0}, Lcom/facebook/login/DeviceAuthDialog;->i(Lcom/facebook/login/DeviceAuthDialog;)V

    .line 2410771
    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2410764
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2410765
    iget-object v0, p0, Lcom/facebook/login/DeviceAuthDialog;->p:Lcom/facebook/login/DeviceAuthDialog$RequestState;

    if-eqz v0, :cond_0

    .line 2410766
    const-string v0, "request_state"

    iget-object v1, p0, Lcom/facebook/login/DeviceAuthDialog;->p:Lcom/facebook/login/DeviceAuthDialog$RequestState;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2410767
    :cond_0
    return-void
.end method
