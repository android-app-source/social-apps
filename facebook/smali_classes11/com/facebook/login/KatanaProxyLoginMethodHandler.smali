.class public Lcom/facebook/login/KatanaProxyLoginMethodHandler;
.super Lcom/facebook/login/LoginMethodHandler;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/login/KatanaProxyLoginMethodHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2411135
    new-instance v0, LX/Gz5;

    invoke-direct {v0}, LX/Gz5;-><init>()V

    sput-object v0, Lcom/facebook/login/KatanaProxyLoginMethodHandler;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 2411133
    invoke-direct {p0, p1}, Lcom/facebook/login/LoginMethodHandler;-><init>(Landroid/os/Parcel;)V

    .line 2411134
    return-void
.end method

.method public constructor <init>(Lcom/facebook/login/LoginClient;)V
    .locals 0

    .prologue
    .line 2411131
    invoke-direct {p0, p1}, Lcom/facebook/login/LoginMethodHandler;-><init>(Lcom/facebook/login/LoginClient;)V

    .line 2411132
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2411039
    const-string v0, "error"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2411040
    if-nez v0, :cond_0

    .line 2411041
    const-string v0, "error_type"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2411042
    :cond_0
    return-object v0
.end method

.method public static b(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2411127
    const-string v0, "error_message"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2411128
    if-nez v0, :cond_0

    .line 2411129
    const-string v0, "error_description"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2411130
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(ILandroid/content/Intent;)Z
    .locals 7

    .prologue
    .line 2411086
    iget-object v0, p0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    .line 2411087
    iget-object v1, v0, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    move-object v0, v1

    .line 2411088
    if-nez p2, :cond_0

    .line 2411089
    const-string v1, "Operation canceled"

    invoke-static {v0, v1}, Lcom/facebook/login/LoginClient$Result;->a(Lcom/facebook/login/LoginClient$Request;Ljava/lang/String;)Lcom/facebook/login/LoginClient$Result;

    move-result-object v0

    .line 2411090
    :goto_0
    if-eqz v0, :cond_5

    .line 2411091
    iget-object v1, p0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    invoke-virtual {v1, v0}, Lcom/facebook/login/LoginClient;->a(Lcom/facebook/login/LoginClient$Result;)V

    .line 2411092
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 2411093
    :cond_0
    if-nez p1, :cond_1

    .line 2411094
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 2411095
    invoke-static {v1}, Lcom/facebook/login/KatanaProxyLoginMethodHandler;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    .line 2411096
    const-string v3, "error_code"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2411097
    const-string p1, "CONNECTION_FAILURE"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 2411098
    invoke-static {v1}, Lcom/facebook/login/KatanaProxyLoginMethodHandler;->b(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    .line 2411099
    invoke-static {v0, v2, v1, v3}, Lcom/facebook/login/LoginClient$Result;->a(Lcom/facebook/login/LoginClient$Request;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/login/LoginClient$Result;

    move-result-object v1

    .line 2411100
    :goto_2
    move-object v0, v1

    .line 2411101
    goto :goto_0

    .line 2411102
    :cond_1
    const/4 v1, -0x1

    if-eq p1, v1, :cond_2

    .line 2411103
    const-string v1, "Unexpected resultCode from authorization."

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/facebook/login/LoginClient$Result;->a(Lcom/facebook/login/LoginClient$Request;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/login/LoginClient$Result;

    move-result-object v0

    goto :goto_0

    .line 2411104
    :cond_2
    const/4 v1, 0x0

    .line 2411105
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 2411106
    invoke-static {v2}, Lcom/facebook/login/KatanaProxyLoginMethodHandler;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    .line 2411107
    const-string v4, "error_code"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2411108
    invoke-static {v2}, Lcom/facebook/login/KatanaProxyLoginMethodHandler;->b(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v5

    .line 2411109
    const-string v6, "e2e"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2411110
    invoke-static {v6}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 2411111
    invoke-virtual {p0, v6}, Lcom/facebook/login/LoginMethodHandler;->a(Ljava/lang/String;)V

    .line 2411112
    :cond_3
    if-nez v3, :cond_7

    if-nez v4, :cond_7

    if-nez v5, :cond_7

    .line 2411113
    :try_start_0
    iget-object v3, v0, Lcom/facebook/login/LoginClient$Request;->b:Ljava/util/Set;

    move-object v3, v3

    .line 2411114
    sget-object v4, LX/GA9;->FACEBOOK_APPLICATION_WEB:LX/GA9;

    .line 2411115
    iget-object v5, v0, Lcom/facebook/login/LoginClient$Request;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2411116
    invoke-static {v3, v2, v4, v5}, Lcom/facebook/login/LoginMethodHandler;->a(Ljava/util/Collection;Landroid/os/Bundle;LX/GA9;Ljava/lang/String;)Lcom/facebook/AccessToken;

    move-result-object v2

    .line 2411117
    invoke-static {v0, v2}, Lcom/facebook/login/LoginClient$Result;->a(Lcom/facebook/login/LoginClient$Request;Lcom/facebook/AccessToken;)Lcom/facebook/login/LoginClient$Result;
    :try_end_0
    .catch LX/GAA; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2411118
    :cond_4
    :goto_3
    move-object v0, v1

    .line 2411119
    goto :goto_0

    .line 2411120
    :cond_5
    iget-object v0, p0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    invoke-virtual {v0}, Lcom/facebook/login/LoginClient;->g()V

    goto :goto_1

    :cond_6
    invoke-static {v0, v2}, Lcom/facebook/login/LoginClient$Result;->a(Lcom/facebook/login/LoginClient$Request;Ljava/lang/String;)Lcom/facebook/login/LoginClient$Result;

    move-result-object v1

    goto :goto_2

    .line 2411121
    :catch_0
    move-exception v2

    .line 2411122
    invoke-virtual {v2}, LX/GAA;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/login/LoginClient$Result;->a(Lcom/facebook/login/LoginClient$Request;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/login/LoginClient$Result;

    move-result-object v1

    goto :goto_3

    .line 2411123
    :cond_7
    sget-object v2, LX/GsW;->a:Ljava/util/Collection;

    invoke-interface {v2, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2411124
    sget-object v2, LX/GsW;->b:Ljava/util/Collection;

    invoke-interface {v2, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2411125
    invoke-static {v0, v1}, Lcom/facebook/login/LoginClient$Result;->a(Lcom/facebook/login/LoginClient$Request;Ljava/lang/String;)Lcom/facebook/login/LoginClient$Result;

    move-result-object v1

    goto :goto_3

    .line 2411126
    :cond_8
    invoke-static {v0, v3, v5, v4}, Lcom/facebook/login/LoginClient$Result;->a(Lcom/facebook/login/LoginClient$Request;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/login/LoginClient$Result;

    move-result-object v1

    goto :goto_3
.end method

.method public final a(Lcom/facebook/login/LoginClient$Request;)Z
    .locals 12

    .prologue
    .line 2411045
    invoke-static {}, Lcom/facebook/login/LoginClient;->j()Ljava/lang/String;

    move-result-object v3

    .line 2411046
    iget-object v0, p0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    invoke-virtual {v0}, Lcom/facebook/login/LoginClient;->b()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2411047
    iget-object v1, p1, Lcom/facebook/login/LoginClient$Request;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2411048
    iget-object v2, p1, Lcom/facebook/login/LoginClient$Request;->b:Ljava/util/Set;

    move-object v2, v2

    .line 2411049
    iget-boolean v4, p1, Lcom/facebook/login/LoginClient$Request;->f:Z

    move v4, v4

    .line 2411050
    invoke-virtual {p1}, Lcom/facebook/login/LoginClient$Request;->g()Z

    move-result v5

    .line 2411051
    iget-object v6, p1, Lcom/facebook/login/LoginClient$Request;->c:LX/Gyu;

    move-object v6, v6

    .line 2411052
    sget-object v7, LX/GsS;->b:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/GsO;

    .line 2411053
    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v7}, LX/GsO;->a()Ljava/lang/String;

    move-result-object v10

    const-string v11, "com.facebook.katana.ProxyAuth"

    invoke-virtual {v9, v10, v11}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    const-string v10, "client_id"

    invoke-virtual {v9, v10, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    .line 2411054
    invoke-static {v2}, LX/Gsc;->a(Ljava/util/Collection;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 2411055
    const-string v10, "scope"

    const-string v11, ","

    invoke-static {v11, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2411056
    :cond_1
    invoke-static {v3}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 2411057
    const-string v10, "e2e"

    invoke-virtual {v9, v10, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2411058
    :cond_2
    const-string v10, "response_type"

    const-string v11, "token,signed_request"

    invoke-virtual {v9, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2411059
    const-string v10, "return_scopes"

    const-string v11, "true"

    invoke-virtual {v9, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2411060
    if-eqz v5, :cond_3

    .line 2411061
    const-string v10, "default_audience"

    invoke-virtual {v6}, LX/Gyu;->getNativeProtocolAudience()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2411062
    :cond_3
    const-string v10, "legacy_override"

    const-string v11, "v2.5"

    invoke-virtual {v9, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2411063
    if-eqz v4, :cond_4

    .line 2411064
    const-string v10, "auth_type"

    const-string v11, "rerequest"

    invoke-virtual {v9, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2411065
    :cond_4
    const/4 v10, 0x0

    .line 2411066
    if-nez v9, :cond_7

    move-object v9, v10

    .line 2411067
    :cond_5
    :goto_0
    move-object v7, v9

    .line 2411068
    if-eqz v7, :cond_0

    .line 2411069
    :goto_1
    move-object v0, v7

    .line 2411070
    const-string v1, "e2e"

    invoke-virtual {p0, v1, v3}, Lcom/facebook/login/LoginMethodHandler;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2411071
    sget-object v1, LX/Gs9;->Login:LX/Gs9;

    invoke-virtual {v1}, LX/Gs9;->toRequestCode()I

    move-result v1

    move v1, v1

    .line 2411072
    const/4 v2, 0x0

    .line 2411073
    if-nez v0, :cond_9

    .line 2411074
    :goto_2
    move v0, v2

    .line 2411075
    return v0

    :cond_6
    const/4 v7, 0x0

    goto :goto_1

    .line 2411076
    :cond_7
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    const/4 p1, 0x0

    invoke-virtual {v11, v9, p1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v11

    .line 2411077
    if-nez v11, :cond_8

    move-object v9, v10

    .line 2411078
    goto :goto_0

    .line 2411079
    :cond_8
    iget-object v11, v11, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-static {v0, v11}, LX/GsO;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_5

    move-object v9, v10

    .line 2411080
    goto :goto_0

    .line 2411081
    :cond_9
    :try_start_0
    iget-object v3, p0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    .line 2411082
    iget-object p0, v3, Lcom/facebook/login/LoginClient;->c:Landroid/support/v4/app/Fragment;

    move-object v3, p0

    .line 2411083
    invoke-virtual {v3, v0, v1}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2411084
    const/4 v2, 0x1

    goto :goto_2

    .line 2411085
    :catch_0
    goto :goto_2
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2411044
    const-string v0, "katana_proxy_auth"

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2411043
    const/4 v0, 0x0

    return v0
.end method
