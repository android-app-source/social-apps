.class public Lcom/facebook/login/WebViewLoginMethodHandler;
.super Lcom/facebook/login/LoginMethodHandler;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/login/WebViewLoginMethodHandler;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/GsI;

.field public d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2411618
    new-instance v0, LX/GzH;

    invoke-direct {v0}, LX/GzH;-><init>()V

    sput-object v0, Lcom/facebook/login/WebViewLoginMethodHandler;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2411615
    invoke-direct {p0, p1}, Lcom/facebook/login/LoginMethodHandler;-><init>(Landroid/os/Parcel;)V

    .line 2411616
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/login/WebViewLoginMethodHandler;->d:Ljava/lang/String;

    .line 2411617
    return-void
.end method

.method public constructor <init>(Lcom/facebook/login/LoginClient;)V
    .locals 0

    .prologue
    .line 2411613
    invoke-direct {p0, p1}, Lcom/facebook/login/LoginMethodHandler;-><init>(Lcom/facebook/login/LoginClient;)V

    .line 2411614
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2411559
    iget-object v0, p0, Lcom/facebook/login/WebViewLoginMethodHandler;->c:LX/GsI;

    if-eqz v0, :cond_0

    .line 2411560
    iget-object v0, p0, Lcom/facebook/login/WebViewLoginMethodHandler;->c:LX/GsI;

    invoke-virtual {v0}, LX/GsI;->cancel()V

    .line 2411561
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/login/WebViewLoginMethodHandler;->c:LX/GsI;

    .line 2411562
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/login/LoginClient$Request;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2411569
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2411570
    iget-object v0, p1, Lcom/facebook/login/LoginClient$Request;->b:Ljava/util/Set;

    move-object v0, v0

    .line 2411571
    invoke-static {v0}, LX/Gsc;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2411572
    const-string v0, ","

    .line 2411573
    iget-object v2, p1, Lcom/facebook/login/LoginClient$Request;->b:Ljava/util/Set;

    move-object v2, v2

    .line 2411574
    invoke-static {v0, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 2411575
    const-string v2, "scope"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411576
    const-string v2, "scope"

    invoke-virtual {p0, v2, v0}, Lcom/facebook/login/LoginMethodHandler;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2411577
    :cond_0
    iget-object v0, p1, Lcom/facebook/login/LoginClient$Request;->c:LX/Gyu;

    move-object v0, v0

    .line 2411578
    const-string v2, "default_audience"

    invoke-virtual {v0}, LX/Gyu;->getNativeProtocolAudience()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411579
    invoke-static {}, Lcom/facebook/AccessToken;->a()Lcom/facebook/AccessToken;

    move-result-object v0

    .line 2411580
    if-eqz v0, :cond_1

    .line 2411581
    iget-object v2, v0, Lcom/facebook/AccessToken;->h:Ljava/lang/String;

    move-object v0, v2

    .line 2411582
    :goto_0
    if-eqz v0, :cond_2

    .line 2411583
    iget-object v2, p0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    invoke-virtual {v2}, Lcom/facebook/login/LoginClient;->b()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 2411584
    const-string v3, "com.facebook.login.AuthorizationClient.WebViewAuthHandler.TOKEN_STORE_KEY"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 2411585
    const-string v3, "TOKEN"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 2411586
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2411587
    const-string v2, "access_token"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411588
    const-string v0, "access_token"

    const-string v2, "1"

    invoke-virtual {p0, v0, v2}, Lcom/facebook/login/LoginMethodHandler;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2411589
    :goto_1
    new-instance v0, LX/GzG;

    invoke-direct {v0, p0, p1}, LX/GzG;-><init>(Lcom/facebook/login/WebViewLoginMethodHandler;Lcom/facebook/login/LoginClient$Request;)V

    .line 2411590
    invoke-static {}, Lcom/facebook/login/LoginClient;->j()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/login/WebViewLoginMethodHandler;->d:Ljava/lang/String;

    .line 2411591
    const-string v2, "e2e"

    iget-object v3, p0, Lcom/facebook/login/WebViewLoginMethodHandler;->d:Ljava/lang/String;

    invoke-virtual {p0, v2, v3}, Lcom/facebook/login/LoginMethodHandler;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2411592
    iget-object v2, p0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    invoke-virtual {v2}, Lcom/facebook/login/LoginClient;->b()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 2411593
    new-instance v3, LX/GzI;

    .line 2411594
    iget-object v4, p1, Lcom/facebook/login/LoginClient$Request;->d:Ljava/lang/String;

    move-object v4, v4

    .line 2411595
    invoke-direct {v3, v2, v4, v1}, LX/GzI;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/facebook/login/WebViewLoginMethodHandler;->d:Ljava/lang/String;

    .line 2411596
    iput-object v1, v3, LX/GzI;->a:Ljava/lang/String;

    .line 2411597
    move-object v1, v3

    .line 2411598
    iget-boolean v3, p1, Lcom/facebook/login/LoginClient$Request;->f:Z

    move v3, v3

    .line 2411599
    iput-boolean v3, v1, LX/GzI;->b:Z

    .line 2411600
    move-object v1, v1

    .line 2411601
    iput-object v0, v1, LX/Gsi;->e:LX/GsB;

    .line 2411602
    move-object v0, v1

    .line 2411603
    invoke-virtual {v0}, LX/Gsi;->a()LX/GsI;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/login/WebViewLoginMethodHandler;->c:LX/GsI;

    .line 2411604
    new-instance v0, Lcom/facebook/internal/FacebookDialogFragment;

    invoke-direct {v0}, Lcom/facebook/internal/FacebookDialogFragment;-><init>()V

    .line 2411605
    invoke-virtual {v0, v5}, Landroid/support/v4/app/Fragment;->setRetainInstance(Z)V

    .line 2411606
    iget-object v1, p0, Lcom/facebook/login/WebViewLoginMethodHandler;->c:LX/GsI;

    .line 2411607
    iput-object v1, v0, Lcom/facebook/internal/FacebookDialogFragment;->j:Landroid/app/Dialog;

    .line 2411608
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    const-string v2, "FacebookDialogFragment"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2411609
    return v5

    .line 2411610
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2411611
    :cond_2
    iget-object v0, p0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    invoke-virtual {v0}, Lcom/facebook/login/LoginClient;->b()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LX/Gsc;->b(Landroid/content/Context;)V

    .line 2411612
    const-string v0, "access_token"

    const-string v2, "0"

    invoke-virtual {p0, v0, v2}, Lcom/facebook/login/LoginMethodHandler;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2411568
    const-string v0, "web_view"

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2411567
    const/4 v0, 0x1

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2411566
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2411563
    invoke-super {p0, p1, p2}, Lcom/facebook/login/LoginMethodHandler;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2411564
    iget-object v0, p0, Lcom/facebook/login/WebViewLoginMethodHandler;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2411565
    return-void
.end method
