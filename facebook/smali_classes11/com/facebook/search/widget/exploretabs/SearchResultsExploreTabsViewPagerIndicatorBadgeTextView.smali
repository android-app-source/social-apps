.class public Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/6Ud;


# instance fields
.field private a:LX/0wM;

.field public b:LX/FeQ;

.field public c:Lcom/facebook/widget/text/BetterTextView;

.field private d:F

.field public e:I

.field public f:I

.field public g:I

.field public h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2276947
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2276948
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2276945
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2276946
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2276942
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2276943
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2276944
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2276939
    invoke-virtual {p0}, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03059a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2276940
    iget-object v0, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, v0}, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->addView(Landroid/view/View;)V

    .line 2276941
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 2276892
    invoke-virtual {p0}, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2276893
    invoke-direct {p0}, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->a()V

    .line 2276894
    new-instance v1, LX/0wM;

    invoke-direct {v1, v0}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    iput-object v1, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->a:LX/0wM;

    .line 2276895
    sget-object v1, LX/03r;->ColoredTabProgressListenerBadgeTextView:[I

    const/4 v2, 0x0

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 2276896
    const/16 v2, 0x0

    const v3, 0x7f0a0097

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 2276897
    iput v2, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->e:I

    .line 2276898
    const/16 v2, 0x2

    const v3, 0x7f0a00d2

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 2276899
    iput v2, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->g:I

    .line 2276900
    const/16 v2, 0x1

    const v3, 0x7f0a00a8

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 2276901
    iput v2, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->f:I

    .line 2276902
    const/16 v2, 0x3

    const v3, 0x7f0a0097

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 2276903
    iput v0, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->h:I

    .line 2276904
    iget-object v0, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    iget v2, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->f:I

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2276905
    iget v0, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->h:I

    invoke-virtual {p0, v0}, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->setBackgroundColor(I)V

    .line 2276906
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 2276907
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 2276919
    invoke-static {p1}, LX/6Uc;->a(F)F

    move-result v3

    .line 2276920
    iget v0, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->d:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_0

    .line 2276921
    :goto_0
    return-void

    .line 2276922
    :cond_0
    iget v0, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->f:I

    iget v1, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->e:I

    invoke-static {p1, v0, v1}, LX/6Uc;->a(FII)I

    move-result v4

    .line 2276923
    iget-object v0, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2276924
    invoke-virtual {p0}, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0hL;->a(Landroid/content/Context;)Z

    move-result v5

    .line 2276925
    if-eqz v5, :cond_1

    .line 2276926
    iget-object v0, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getCompoundDrawablesRelative()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_1
    move v1, v2

    .line 2276927
    :goto_2
    array-length v6, v0

    if-ge v1, v6, :cond_2

    .line 2276928
    iget-object v6, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->a:LX/0wM;

    aget-object v7, v0, v1

    invoke-virtual {v6, v7, v4}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    aput-object v6, v0, v1

    .line 2276929
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2276930
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    .line 2276931
    :cond_2
    if-eqz v5, :cond_4

    .line 2276932
    iget-object v1, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    aget-object v2, v0, v2

    aget-object v4, v0, v8

    aget-object v5, v0, v9

    aget-object v0, v0, v10

    invoke-virtual {v1, v2, v4, v5, v0}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2276933
    :goto_3
    iget v0, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->h:I

    iget v1, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->g:I

    invoke-static {p1, v0, v1}, LX/6Uc;->a(FII)I

    move-result v0

    .line 2276934
    invoke-virtual {p0, v0}, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->setBackgroundColor(I)V

    .line 2276935
    iget-object v0, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->b:LX/FeQ;

    if-eqz v0, :cond_3

    .line 2276936
    iget-object v0, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->b:LX/FeQ;

    invoke-interface {v0, p1}, LX/FeQ;->a(F)V

    .line 2276937
    :cond_3
    iput v3, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->d:F

    goto :goto_0

    .line 2276938
    :cond_4
    iget-object v1, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    aget-object v2, v0, v2

    aget-object v4, v0, v8

    aget-object v5, v0, v9

    aget-object v0, v0, v10

    invoke-virtual {v1, v2, v4, v5, v0}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_3
.end method

.method public getTextView()Lcom/facebook/widget/text/BetterTextView;
    .locals 1

    .prologue
    .line 2276918
    iget-object v0, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public setSelectedBackgroundColor(I)V
    .locals 0

    .prologue
    .line 2276916
    iput p1, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->g:I

    .line 2276917
    return-void
.end method

.method public setSelectedTextColor(I)V
    .locals 0

    .prologue
    .line 2276914
    iput p1, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->e:I

    .line 2276915
    return-void
.end method

.method public setUnselectedBackgroundColor(I)V
    .locals 0

    .prologue
    .line 2276912
    iput p1, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->h:I

    .line 2276913
    return-void
.end method

.method public setUnselectedTextColor(I)V
    .locals 0

    .prologue
    .line 2276910
    iput p1, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->f:I

    .line 2276911
    return-void
.end method

.method public setUpdateListener(LX/FeQ;)V
    .locals 0

    .prologue
    .line 2276908
    iput-object p1, p0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->b:LX/FeQ;

    .line 2276909
    return-void
.end method
