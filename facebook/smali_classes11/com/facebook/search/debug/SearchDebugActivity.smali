.class public Lcom/facebook/search/debug/SearchDebugActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/2SY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Fho;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1mR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1nD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/3Qc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/3Qn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:Landroid/preference/PreferenceScreen;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2257158
    const-class v0, Lcom/facebook/search/debug/SearchDebugActivity;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/debug/SearchDebugActivity;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2257159
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/search/debug/SearchDebugActivity;LX/2SY;LX/Fho;LX/1mR;Ljava/util/concurrent/Executor;LX/1nD;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/3Qc;LX/3Qn;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 2257160
    iput-object p1, p0, Lcom/facebook/search/debug/SearchDebugActivity;->a:LX/2SY;

    iput-object p2, p0, Lcom/facebook/search/debug/SearchDebugActivity;->b:LX/Fho;

    iput-object p3, p0, Lcom/facebook/search/debug/SearchDebugActivity;->c:LX/1mR;

    iput-object p4, p0, Lcom/facebook/search/debug/SearchDebugActivity;->d:Ljava/util/concurrent/Executor;

    iput-object p5, p0, Lcom/facebook/search/debug/SearchDebugActivity;->e:LX/1nD;

    iput-object p6, p0, Lcom/facebook/search/debug/SearchDebugActivity;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p7, p0, Lcom/facebook/search/debug/SearchDebugActivity;->g:LX/3Qc;

    iput-object p8, p0, Lcom/facebook/search/debug/SearchDebugActivity;->h:LX/3Qn;

    iput-object p9, p0, Lcom/facebook/search/debug/SearchDebugActivity;->i:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/search/debug/SearchDebugActivity;

    invoke-static {v9}, LX/2SY;->a(LX/0QB;)LX/2SY;

    move-result-object v1

    check-cast v1, LX/2SY;

    invoke-static {v9}, LX/Fho;->a(LX/0QB;)LX/Fho;

    move-result-object v2

    check-cast v2, LX/Fho;

    invoke-static {v9}, LX/1mR;->a(LX/0QB;)LX/1mR;

    move-result-object v3

    check-cast v3, LX/1mR;

    invoke-static {v9}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-static {v9}, LX/1nD;->a(LX/0QB;)LX/1nD;

    move-result-object v5

    check-cast v5, LX/1nD;

    invoke-static {v9}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v9}, LX/3Qc;->a(LX/0QB;)LX/3Qc;

    move-result-object v7

    check-cast v7, LX/3Qc;

    invoke-static {v9}, LX/3Qn;->a(LX/0QB;)LX/3Qn;

    move-result-object v8

    check-cast v8, LX/3Qn;

    invoke-static {v9}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v0 .. v9}, Lcom/facebook/search/debug/SearchDebugActivity;->a(Lcom/facebook/search/debug/SearchDebugActivity;LX/2SY;LX/Fho;LX/1mR;Ljava/util/concurrent/Executor;LX/1nD;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/3Qc;LX/3Qn;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method

.method private b()Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 2257161
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2257162
    const-string v1, "Fetch Recent Searches"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257163
    const-string v1, "Force fetches the list of recent searches in null state"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2257164
    new-instance v1, LX/FZG;

    invoke-direct {v1, p0}, LX/FZG;-><init>(Lcom/facebook/search/debug/SearchDebugActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2257165
    return-object v0
.end method

.method public static b(Landroid/preference/Preference;Z)V
    .locals 1

    .prologue
    .line 2257166
    if-eqz p1, :cond_0

    const-string v0, "Disable Tutorial Nux Debug Mode"

    :goto_0
    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257167
    if-eqz p1, :cond_1

    const-string v0, "Use prod rate-limiting and caching policies\nDebug Mode currently ENABLED"

    :goto_1
    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2257168
    return-void

    .line 2257169
    :cond_0
    const-string v0, "Enable Tutorial Nux Debug Mode"

    goto :goto_0

    .line 2257170
    :cond_1
    const-string v0, "Use debug rate-limiting and caching policies\nDebug mode currently DISABLED"

    goto :goto_1
.end method

.method private d()Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 2257171
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2257172
    const-string v1, "Clear Recent Searches Cache"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257173
    const-string v1, "Clears all memory and disk caches holding recent searches"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2257174
    new-instance v1, LX/FZI;

    invoke-direct {v1, p0}, LX/FZI;-><init>(Lcom/facebook/search/debug/SearchDebugActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2257175
    return-object v0
.end method

.method private e()Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 2257176
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2257177
    const-string v1, "Clear Null State Searches Cache"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257178
    const-string v1, "Clears all memory and disk caches holding Null State searches"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2257179
    new-instance v1, LX/FZK;

    invoke-direct {v1, p0}, LX/FZK;-><init>(Lcom/facebook/search/debug/SearchDebugActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2257180
    return-object v0
.end method

.method public static f(Lcom/facebook/search/debug/SearchDebugActivity;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2257181
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->b:LX/Fho;

    invoke-virtual {v0}, LX/2SP;->c()V

    .line 2257182
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->c:LX/1mR;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "null_state_module_cache_tag"

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private g()Landroid/preference/Preference;
    .locals 4

    .prologue
    .line 2257183
    new-instance v0, LX/4om;

    invoke-direct {v0, p0}, LX/4om;-><init>(Landroid/content/Context;)V

    .line 2257184
    const-string v1, "Load Trending Topic ID"

    invoke-virtual {v0, v1}, LX/4om;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257185
    const-string v1, "Load news search for a specified ID"

    invoke-virtual {v0, v1}, LX/4om;->setSummary(Ljava/lang/CharSequence;)V

    .line 2257186
    sget-object v1, LX/3Qm;->e:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4om;->a(LX/0Tn;)V

    .line 2257187
    iget-object v1, p0, Lcom/facebook/search/debug/SearchDebugActivity;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/3Qm;->e:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4om;->setDefaultValue(Ljava/lang/Object;)V

    .line 2257188
    new-instance v1, LX/FZL;

    invoke-direct {v1, p0}, LX/FZL;-><init>(Lcom/facebook/search/debug/SearchDebugActivity;)V

    invoke-virtual {v0, v1}, LX/4om;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2257189
    return-object v0
.end method

.method private h()Landroid/preference/Preference;
    .locals 4

    .prologue
    .line 2257190
    new-instance v0, LX/4om;

    invoke-direct {v0, p0}, LX/4om;-><init>(Landroid/content/Context;)V

    .line 2257191
    const-string v1, "Load Pulse URL"

    invoke-virtual {v0, v1}, LX/4om;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257192
    const-string v1, "Load Pulse content modules for a specified link/article"

    invoke-virtual {v0, v1}, LX/4om;->setSummary(Ljava/lang/CharSequence;)V

    .line 2257193
    sget-object v1, LX/3Qm;->f:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4om;->a(LX/0Tn;)V

    .line 2257194
    iget-object v1, p0, Lcom/facebook/search/debug/SearchDebugActivity;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/3Qm;->f:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4om;->setDefaultValue(Ljava/lang/Object;)V

    .line 2257195
    new-instance v1, LX/FZM;

    invoke-direct {v1, p0}, LX/FZM;-><init>(Lcom/facebook/search/debug/SearchDebugActivity;)V

    invoke-virtual {v0, v1}, LX/4om;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2257196
    return-object v0
.end method

.method private i()Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 2257153
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2257154
    const-string v1, "Refresh Bootstrap Entities"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257155
    const-string v1, "Force fetches bootstrap entities"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2257156
    new-instance v1, LX/FZN;

    invoke-direct {v1, p0}, LX/FZN;-><init>(Lcom/facebook/search/debug/SearchDebugActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2257157
    return-object v0
.end method

.method private j()Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 2257197
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2257198
    const-string v1, "Refresh Bootstrap Keywords"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257199
    const-string v1, "Force fetches bootstrap keywords"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2257200
    new-instance v1, LX/FZO;

    invoke-direct {v1, p0}, LX/FZO;-><init>(Lcom/facebook/search/debug/SearchDebugActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2257201
    return-object v0
.end method

.method private k()Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 2257052
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2257053
    const-string v1, "Reset Tutorial Nux"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257054
    const-string v1, "Clears the preferences flag which disables tutorial nux"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2257055
    new-instance v1, LX/FZP;

    invoke-direct {v1, p0}, LX/FZP;-><init>(Lcom/facebook/search/debug/SearchDebugActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2257056
    return-object v0
.end method

.method private l()Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 2257057
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2257058
    invoke-static {p0}, Lcom/facebook/search/debug/SearchDebugActivity;->m(Lcom/facebook/search/debug/SearchDebugActivity;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/facebook/search/debug/SearchDebugActivity;->b(Landroid/preference/Preference;Z)V

    .line 2257059
    new-instance v1, LX/FZQ;

    invoke-direct {v1, p0, v0}, LX/FZQ;-><init>(Lcom/facebook/search/debug/SearchDebugActivity;Landroid/preference/Preference;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2257060
    return-object v0
.end method

.method public static m(Lcom/facebook/search/debug/SearchDebugActivity;)Z
    .locals 3

    .prologue
    .line 2257061
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/7CP;->g:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method private n()Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 2257062
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2257063
    const-string v1, "Test Tutorial Nux Configuration"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257064
    const-string v1, "Allows user to test a configuration of Tutorial Nux"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2257065
    new-instance v1, LX/FZ9;

    invoke-direct {v1, p0}, LX/FZ9;-><init>(Lcom/facebook/search/debug/SearchDebugActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2257066
    return-object v0
.end method

.method private o()Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 2257067
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2257068
    const-string v1, "Reset Awareness Opt-Outs"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257069
    const-string v1, "Clears device flags for opting out of each type of awareness unit"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2257070
    new-instance v1, LX/FZA;

    invoke-direct {v1, p0}, LX/FZA;-><init>(Lcom/facebook/search/debug/SearchDebugActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2257071
    return-object v0
.end method

.method private p()Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 2257072
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2257073
    const-string v1, "Reset Learning Nux State"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257074
    const-string v1, "Clears stored client data for Learning Nux.  Does NOT reset server state."

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2257075
    new-instance v1, LX/FZB;

    invoke-direct {v1, p0}, LX/FZB;-><init>(Lcom/facebook/search/debug/SearchDebugActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2257076
    return-object v0
.end method

.method private q()Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 2257077
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2257078
    const-string v1, "Reset Search Spotlight State"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257079
    const-string v1, "Clears stored client data for Search Spotlight.  Does NOT reset server state."

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2257080
    new-instance v1, LX/FZC;

    invoke-direct {v1, p0}, LX/FZC;-><init>(Lcom/facebook/search/debug/SearchDebugActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2257081
    return-object v0
.end method

.method private r()Landroid/preference/Preference;
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2257082
    sget-object v0, LX/3Qm;->j:LX/0Tn;

    .line 2257083
    new-instance v1, LX/FZD;

    invoke-direct {v1, p0, p0, v0}, LX/FZD;-><init>(Lcom/facebook/search/debug/SearchDebugActivity;Landroid/content/Context;LX/0Tn;)V

    .line 2257084
    const-string v2, "Enable/Disable Post Search"

    invoke-virtual {v1, v2}, LX/4or;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257085
    new-array v2, v7, [Ljava/lang/CharSequence;

    const-string v3, "Force Post Search"

    aput-object v3, v2, v4

    const-string v3, "Force Simple Search"

    aput-object v3, v2, v5

    const-string v3, "Unset Override"

    aput-object v3, v2, v6

    invoke-virtual {v1, v2}, LX/4or;->setEntries([Ljava/lang/CharSequence;)V

    .line 2257086
    new-array v2, v7, [Ljava/lang/CharSequence;

    const-string v3, "post search"

    aput-object v3, v2, v4

    const-string v3, "simple search"

    aput-object v3, v2, v5

    const-string v3, "unset"

    aput-object v3, v2, v6

    invoke-virtual {v1, v2}, LX/4or;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 2257087
    const-string v2, "Override Post Search GK"

    invoke-virtual {v1, v2}, LX/4or;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 2257088
    const-string v2, ""

    invoke-virtual {v1, v2}, LX/4or;->setSummary(Ljava/lang/CharSequence;)V

    .line 2257089
    invoke-virtual {v1, v0}, LX/4or;->a(LX/0Tn;)V

    .line 2257090
    new-instance v0, LX/FZE;

    invoke-direct {v0, p0, v1}, LX/FZE;-><init>(Lcom/facebook/search/debug/SearchDebugActivity;LX/4or;)V

    invoke-virtual {v1, v0}, LX/4or;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2257091
    return-object v1
.end method

.method private s()Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 2257092
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2257093
    const-string v1, "Enable Search Native Template"

    invoke-virtual {v0, v1}, LX/4ok;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257094
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 2257095
    sget-object v1, LX/3Qm;->k:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 2257096
    return-object v0
.end method

.method private t()Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 2257097
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2257098
    const-string v1, "Enable Search Result Debug Info"

    invoke-virtual {v0, v1}, LX/4ok;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257099
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 2257100
    sget-object v1, LX/3Qm;->n:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 2257101
    return-object v0
.end method

.method private u()Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 2257102
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2257103
    const-string v1, "Enable Search Nux Triggering Dev Mode"

    invoke-virtual {v0, v1}, LX/4ok;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257104
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 2257105
    sget-object v1, LX/3Qm;->l:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 2257106
    return-object v0
.end method

.method private v()Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 2257107
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2257108
    const-string v1, "Quick Experiment"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257109
    const-string v1, "Shortcut to QE Page"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2257110
    new-instance v1, LX/FZF;

    invoke-direct {v1, p0}, LX/FZF;-><init>(Lcom/facebook/search/debug/SearchDebugActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2257111
    return-object v0
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2257112
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 2257113
    invoke-static {p0, p0}, Lcom/facebook/search/debug/SearchDebugActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2257114
    invoke-virtual {p0}, Lcom/facebook/search/debug/SearchDebugActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    .line 2257115
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2257116
    const-string v1, "Trending Topics"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257117
    iget-object v1, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257118
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-direct {p0}, Lcom/facebook/search/debug/SearchDebugActivity;->g()Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257119
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-direct {p0}, Lcom/facebook/search/debug/SearchDebugActivity;->h()Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257120
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-direct {p0}, Lcom/facebook/search/debug/SearchDebugActivity;->e()Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257121
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2257122
    const-string v1, "Recent Searches"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257123
    iget-object v1, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257124
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-direct {p0}, Lcom/facebook/search/debug/SearchDebugActivity;->b()Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257125
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-direct {p0}, Lcom/facebook/search/debug/SearchDebugActivity;->d()Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257126
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2257127
    const-string v1, "Bootstrap"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257128
    iget-object v1, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257129
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-direct {p0}, Lcom/facebook/search/debug/SearchDebugActivity;->i()Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257130
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-direct {p0}, Lcom/facebook/search/debug/SearchDebugActivity;->j()Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257131
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2257132
    const-string v1, "Search Awareness and Education"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257133
    iget-object v1, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257134
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-direct {p0}, Lcom/facebook/search/debug/SearchDebugActivity;->k()Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257135
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-direct {p0}, Lcom/facebook/search/debug/SearchDebugActivity;->l()Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257136
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-direct {p0}, Lcom/facebook/search/debug/SearchDebugActivity;->n()Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257137
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-direct {p0}, Lcom/facebook/search/debug/SearchDebugActivity;->o()Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257138
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-direct {p0}, Lcom/facebook/search/debug/SearchDebugActivity;->p()Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257139
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-direct {p0}, Lcom/facebook/search/debug/SearchDebugActivity;->q()Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257140
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2257141
    const-string v1, "Search Native Template"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257142
    iget-object v1, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257143
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-direct {p0}, Lcom/facebook/search/debug/SearchDebugActivity;->s()Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257144
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-direct {p0}, Lcom/facebook/search/debug/SearchDebugActivity;->t()Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257145
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2257146
    const-string v1, "Misc"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2257147
    iget-object v1, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257148
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-direct {p0}, Lcom/facebook/search/debug/SearchDebugActivity;->u()Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257149
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-direct {p0}, Lcom/facebook/search/debug/SearchDebugActivity;->r()Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257150
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-direct {p0}, Lcom/facebook/search/debug/SearchDebugActivity;->v()Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2257151
    iget-object v0, p0, Lcom/facebook/search/debug/SearchDebugActivity;->k:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0, v0}, Lcom/facebook/search/debug/SearchDebugActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 2257152
    return-void
.end method
