.class public Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field private static final r:[Ljava/lang/String;


# instance fields
.field public A:Lcom/facebook/widget/text/BetterEditTextView;

.field public B:Lcom/facebook/widget/text/BetterEditTextView;

.field private C:Lcom/facebook/resources/ui/FbButton;

.field private D:Lcom/facebook/resources/ui/FbButton;

.field public p:LX/Fas;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/widget/text/BetterEditTextView;

.field public t:Lcom/facebook/widget/text/BetterEditTextView;

.field public u:Lcom/facebook/widget/text/BetterEditTextView;

.field public v:Lcom/facebook/widget/text/BetterEditTextView;

.field public w:Lcom/facebook/widget/text/BetterEditTextView;

.field public x:Lcom/facebook/widget/text/BetterEditTextView;

.field public y:Lcom/facebook/widget/text/BetterEditTextView;

.field public z:Lcom/facebook/widget/text/BetterEditTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2256945
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "FF0991FF"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "FF14C7A6"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "FFF5156F"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->r:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2256946
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2256948
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static synthetic a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;
    .locals 1

    .prologue
    .line 2256947
    invoke-static/range {p0 .. p9}, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;LX/Fas;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 2256949
    iput-object p1, p0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->p:LX/Fas;

    iput-object p2, p0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->q:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;

    invoke-static {v1}, LX/Fas;->a(LX/0QB;)LX/Fas;

    move-result-object v0

    check-cast v0, LX/Fas;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0, v0, v1}, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->a(Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;LX/Fas;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;
    .locals 11

    .prologue
    .line 2256935
    invoke-static/range {p5 .. p5}, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static/range {p6 .. p6}, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static/range {p7 .. p7}, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static/range {p8 .. p8}, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static/range {p9 .. p9}, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->BASIC:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    new-instance v6, LX/186;

    const/16 v7, 0x400

    invoke-direct {v6, v7}, LX/186;-><init>(I)V

    invoke-virtual {v6, p2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v6, p0}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v6, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v6, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v6, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v6, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v6, p1}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v6, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    const/16 v10, 0x9

    invoke-virtual {v6, v10}, LX/186;->c(I)V

    const/4 v10, 0x0

    invoke-virtual {v6, v10, v7}, LX/186;->b(II)V

    const/4 v7, 0x1

    invoke-virtual {v6, v7, v0}, LX/186;->b(II)V

    const/4 v0, 0x2

    invoke-virtual {v6, v0, v8}, LX/186;->b(II)V

    const/4 v0, 0x3

    invoke-virtual {v6, v0, v2}, LX/186;->b(II)V

    const/4 v0, 0x4

    invoke-virtual {v6, v0, v1}, LX/186;->b(II)V

    const/4 v0, 0x5

    invoke-virtual {v6, v0, v3}, LX/186;->b(II)V

    const/4 v0, 0x6

    invoke-virtual {v6, v0, v4}, LX/186;->b(II)V

    const/4 v0, 0x7

    invoke-virtual {v6, v0, v9}, LX/186;->b(II)V

    const/16 v0, 0x8

    invoke-virtual {v6, v0, v5}, LX/186;->b(II)V

    const v0, 0x2d4f589d

    invoke-static {v6, v0}, LX/1vs;->a(LX/186;I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2256936
    new-instance v2, LX/A0b;

    invoke-direct {v2}, LX/A0b;-><init>()V

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;->DEFAULT:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    invoke-virtual {v2, v3}, LX/A0b;->a(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;)LX/A0b;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/A0b;->e(Ljava/lang/String;)LX/A0b;

    move-result-object v2

    invoke-virtual {v2, p4}, LX/A0b;->f(Ljava/lang/String;)LX/A0b;

    move-result-object v2

    const-string v3, "https://lookaside.facebook.com/assets/1547222318925572/"

    invoke-virtual {v2, v3}, LX/A0b;->b(Ljava/lang/String;)LX/A0b;

    move-result-object v2

    const-string v3, "https://lookaside.facebook.com/assets/1504507246518835/"

    invoke-virtual {v2, v3}, LX/A0b;->a(Ljava/lang/String;)LX/A0b;

    move-result-object v2

    const-string v3, "https://lookaside.facebook.com/assets/1505070809795626/"

    invoke-virtual {v2, v3}, LX/A0b;->c(Ljava/lang/String;)LX/A0b;

    move-result-object v2

    const-string v3, "https://lookaside.facebook.com/assets/1502643223372321/"

    invoke-virtual {v2, v3}, LX/A0b;->d(Ljava/lang/String;)LX/A0b;

    move-result-object v2

    const v3, 0x2d4f589d

    invoke-static {v1, v0, v3}, LX/2uF;->b(LX/15i;II)LX/2uF;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/A0b;->a(LX/2uF;)LX/A0b;

    move-result-object v0

    invoke-virtual {v0}, LX/A0b;->a()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;

    move-result-object v0

    .line 2256937
    new-instance v1, LX/A0c;

    invoke-direct {v1}, LX/A0c;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/A0c;->a(Z)LX/A0c;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/A0c;->a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;)LX/A0c;

    move-result-object v0

    invoke-virtual {v0}, LX/A0c;->a()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    move-result-object v0

    .line 2256938
    return-object v0

    .line 2256939
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2256940
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2256941
    sget-object v0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->r:[Ljava/lang/String;

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    sget-object v1, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->r:[Ljava/lang/String;

    array-length v1, v1

    int-to-double v4, v1

    mul-double/2addr v2, v4

    double-to-int v1, v2

    aget-object p0, v0, v1

    .line 2256942
    :cond_0
    :goto_0
    return-object p0

    .line 2256943
    :cond_1
    const-string v0, "#"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2256944
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2256909
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const-string p0, "https://lookaside.facebook.com/assets/442678635929912/"

    goto :goto_0
.end method

.method private static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2256910
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const-string p0, "https://lookaside.facebook.com/assets/1544405985882709/"

    goto :goto_0
.end method

.method private static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2256911
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const-string p0, "https://lookaside.facebook.com/assets/958872200860045/"

    goto :goto_0
.end method

.method private static f(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2256912
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const-string p0, "https://lookaside.facebook.com/assets/991821417567505/"

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2256913
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2256914
    const v0, 0x7f03014f

    invoke-virtual {p0, v0}, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->setContentView(I)V

    .line 2256915
    invoke-static {p0, p0}, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2256916
    const v0, 0x7f0d04f9

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->s:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2256917
    const v0, 0x7f0d0625

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->t:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2256918
    const v0, 0x7f0d0626

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->u:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2256919
    const v0, 0x7f0d0627

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->v:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2256920
    const v0, 0x7f0d0628

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->w:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2256921
    const v0, 0x7f0d0629

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->x:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2256922
    const v0, 0x7f0d062a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->y:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2256923
    const v0, 0x7f0d062b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->z:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2256924
    const v0, 0x7f0d062c

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->A:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2256925
    const v0, 0x7f0d062d

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->B:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2256926
    const v0, 0x7f0d062e

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->C:Lcom/facebook/resources/ui/FbButton;

    .line 2256927
    const v0, 0x7f0d062f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->D:Lcom/facebook/resources/ui/FbButton;

    .line 2256928
    iget-object v0, p0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->C:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/FZ6;

    invoke-direct {v1, p0}, LX/FZ6;-><init>(Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2256929
    iget-object v0, p0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->D:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/FZ7;

    invoke-direct {v1, p0}, LX/FZ7;-><init>(Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2256930
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x509a1e44

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2256931
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2256932
    iget-object v1, p0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->C:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2256933
    iget-object v1, p0, Lcom/facebook/search/debug/SearchAwarenessTutorialNuxDebugActivity;->D:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2256934
    const/16 v1, 0x23

    const v2, 0x31bde50b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
