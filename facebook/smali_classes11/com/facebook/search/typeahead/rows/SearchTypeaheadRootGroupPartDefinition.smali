.class public Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        "Ljava/lang/Void;",
        "LX/FhE;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/1T5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1T5",
            "<",
            "LX/FhE;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "LX/1RB",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/UnsupportedSearchTypeaheadPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Z


# direct methods
.method public constructor <init>(LX/0Uh;LX/0ad;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/UnsupportedSearchTypeaheadPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleEntityPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadNeueTrendingEntityPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadDividerPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadQRCodePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadHeaderPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/nullstate/SearchTypeaheadHeaderComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/nullstate/NullStateSeeMorePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/nullstate/NullStateFigSeeMorePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadNullStateSuggestionPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadPlaceTipsPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadKeywordPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleKeywordComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadShortcutPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadGapPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentPartDefinition;",
            ">;",
            "Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2275959
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2275960
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;->a:LX/0Ot;

    .line 2275961
    invoke-static/range {p12 .. p12}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v1

    .line 2275962
    invoke-static/range {p11 .. p11}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v2

    .line 2275963
    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v4

    .line 2275964
    invoke-static/range {p14 .. p14}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    .line 2275965
    invoke-static/range {p22 .. p22}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v7

    .line 2275966
    invoke-static/range {p21 .. p21}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v8

    .line 2275967
    invoke-static/range {p5 .. p5}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v9

    .line 2275968
    invoke-static/range {p24 .. p24}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v5

    .line 2275969
    invoke-static/range {p19 .. p19}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v6

    .line 2275970
    invoke-static {}, LX/1T5;->a()LX/1T5;

    move-result-object v10

    const-class v11, Lcom/facebook/search/model/TrendingTypeaheadUnit;

    move-object/from16 v0, p8

    invoke-virtual {v10, v11, v0}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v10

    const-class v11, Lcom/facebook/search/model/DividerTypeaheadUnit;

    move-object/from16 v0, p9

    invoke-virtual {v10, v11, v0}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v10

    const-class v11, Lcom/facebook/search/model/QRCodePromoUnit;

    move-object/from16 v0, p10

    invoke-virtual {v10, v11, v0}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v10

    const-class v11, Lcom/facebook/search/model/HeaderRowTypeaheadUnit;

    sget-short v12, LX/100;->cf:S

    const/4 v13, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v12, v13}, LX/0ad;->a(SZ)Z

    move-result v12

    if-eqz v12, :cond_2

    :goto_0
    invoke-virtual {v10, v11, v1}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v2

    const-class v10, Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;

    sget-short v1, LX/100;->cf:S

    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v1, v11}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object v1, v3

    :goto_1
    invoke-virtual {v2, v10, v1}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v1

    const-class v2, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    sget-short v3, LX/100;->cf:S

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 p15, v5

    :cond_0
    move-object/from16 v0, p15

    invoke-virtual {v1, v2, v0}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v1

    const-class v2, Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;

    move-object/from16 v0, p16

    invoke-virtual {v1, v2, v0}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v1

    const-class v2, Lcom/facebook/search/model/SeeMoreTypeaheadUnit;

    move-object/from16 v0, p17

    invoke-virtual {v1, v2, v0}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v2

    const-class v3, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    sget-short v1, LX/100;->ce:S

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/3Qm;->m:LX/0Tn;

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-nez v1, :cond_1

    move-object/from16 p18, v6

    :cond_1
    move-object/from16 v0, p18

    invoke-virtual {v2, v3, v0}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v1

    const-class v2, Lcom/facebook/search/model/ShortcutTypeaheadUnit;

    move-object/from16 v0, p20

    invoke-virtual {v1, v2, v0}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v1

    const-class v2, Lcom/facebook/search/model/EmptyScopedNullStateTypeaheadUnit;

    invoke-virtual {v1, v2, v8}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v1

    const-class v2, Lcom/facebook/search/model/SearchSpotlightCollectionUnit;

    invoke-virtual {v1, v2, v7}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v1

    const-class v2, Lcom/facebook/search/model/GapTypeaheadUnit;

    move-object/from16 v0, p23

    invoke-virtual {v1, v2, v0}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;->b:LX/1T5;

    .line 2275971
    sget v1, LX/2SU;->L:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v1

    .line 2275972
    sget-short v2, LX/100;->ce:S

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 2275973
    if-eqz v2, :cond_4

    .line 2275974
    iget-object v1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;->b:LX/1T5;

    const-class v2, Lcom/facebook/search/model/EntityTypeaheadUnit;

    move-object/from16 v0, p7

    invoke-virtual {v1, v2, v0}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    .line 2275975
    :goto_2
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;->c:Ljava/util/Map;

    .line 2275976
    iget-object v1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;->c:Ljava/util/Map;

    const-class v2, Lcom/facebook/search/model/NullStateModuleCollectionUnit;

    move-object/from16 v0, p25

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275977
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;->d:LX/0Ot;

    .line 2275978
    sget v1, LX/2SU;->J:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;->e:Z

    .line 2275979
    return-void

    :cond_2
    move-object v1, v2

    .line 2275980
    goto/16 :goto_0

    :cond_3
    move-object v1, v4

    goto/16 :goto_1

    .line 2275981
    :cond_4
    if-eqz v1, :cond_5

    .line 2275982
    iget-object v1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;->b:LX/1T5;

    const-class v2, Lcom/facebook/search/model/EntityTypeaheadUnit;

    invoke-virtual {v1, v2, v9}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    goto :goto_2

    .line 2275983
    :cond_5
    iget-object v1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;->b:LX/1T5;

    const-class v2, Lcom/facebook/search/model/EntityTypeaheadUnit;

    move-object/from16 v0, p6

    invoke-virtual {v1, v2, v0}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    goto :goto_2
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;
    .locals 3

    .prologue
    .line 2275984
    const-class v1, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;

    monitor-enter v1

    .line 2275985
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2275986
    sput-object v2, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2275987
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2275988
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;->b(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2275989
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2275990
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2275991
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;
    .locals 28

    .prologue
    .line 2275992
    new-instance v2, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    const/16 v5, 0xf9a

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x11cb

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x34f2

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x11be

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x11ca

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x11c3

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x11bc

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x11c6

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x11c1

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x3509

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x3501

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x11cc

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x11c4

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x11c5

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x11bf

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x11c2

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x34f8

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const/16 v22, 0x11c9

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v22

    const/16 v23, 0x34ef

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const/16 v24, 0x34ee

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v24

    const/16 v25, 0x11c0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v25

    const/16 v26, 0x3505

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v26

    invoke-static/range {p0 .. p0}, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;

    move-result-object v27

    check-cast v27, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;

    invoke-direct/range {v2 .. v27}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;-><init>(LX/0Uh;LX/0ad;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;)V

    .line 2275993
    return-object v2
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2275994
    check-cast p2, Lcom/facebook/graphql/model/FeedUnit;

    const/4 v3, 0x0

    .line 2275995
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;->b:LX/1T5;

    invoke-virtual {v0, p1, p2}, LX/1T5;->a(LX/1RF;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2275996
    :cond_0
    :goto_0
    return-object v3

    .line 2275997
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2275998
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    .line 2275999
    invoke-virtual {v1, p2}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2276000
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RB;

    .line 2276001
    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 2276002
    :cond_3
    instance-of v0, p2, Lcom/facebook/search/model/TypeaheadUnit;

    if-eqz v0, :cond_0

    .line 2276003
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    check-cast p2, Lcom/facebook/search/model/TypeaheadUnit;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V
    .locals 2

    .prologue
    .line 2276004
    iget-boolean v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;->e:Z

    if-nez v0, :cond_0

    .line 2276005
    :goto_0
    return-void

    .line 2276006
    :cond_0
    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v0

    .line 2276007
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2276008
    const/4 v0, 0x1

    return v0
.end method
