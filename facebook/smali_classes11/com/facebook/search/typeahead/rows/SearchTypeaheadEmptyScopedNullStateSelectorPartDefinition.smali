.class public Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/search/model/EmptyScopedNullStateTypeaheadUnit;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition;Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2275710
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2275711
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateSelectorPartDefinition;->a:Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition;

    .line 2275712
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateSelectorPartDefinition;->b:Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;

    .line 2275713
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateSelectorPartDefinition;
    .locals 5

    .prologue
    .line 2275699
    const-class v1, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateSelectorPartDefinition;

    monitor-enter v1

    .line 2275700
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateSelectorPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2275701
    sput-object v2, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateSelectorPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2275702
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2275703
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2275704
    new-instance p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition;->a(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;->a(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateSelectorPartDefinition;-><init>(Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition;Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;)V

    .line 2275705
    move-object v0, p0

    .line 2275706
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2275707
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2275708
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2275709
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2275695
    check-cast p2, Lcom/facebook/search/model/EmptyScopedNullStateTypeaheadUnit;

    .line 2275696
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateSelectorPartDefinition;->a:Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateSelectorPartDefinition;->b:Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2275697
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2275698
    const/4 v0, 0x1

    return v0
.end method
