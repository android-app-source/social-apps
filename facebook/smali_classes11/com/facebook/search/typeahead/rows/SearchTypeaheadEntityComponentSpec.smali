.class public Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/8i7;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2275848
    const-class v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentSpec;

    const-string v1, "search"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/8i7;LX/0Or;LX/0Uh;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8i7;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2275849
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2275850
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentSpec;->b:LX/8i7;

    .line 2275851
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentSpec;->c:LX/0Or;

    .line 2275852
    iput-object p3, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentSpec;->d:LX/0Uh;

    .line 2275853
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentSpec;
    .locals 6

    .prologue
    .line 2275854
    const-class v1, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentSpec;

    monitor-enter v1

    .line 2275855
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2275856
    sput-object v2, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2275857
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2275858
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2275859
    new-instance v5, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentSpec;

    invoke-static {v0}, LX/8i7;->a(LX/0QB;)LX/8i7;

    move-result-object v3

    check-cast v3, LX/8i7;

    const/16 v4, 0x509

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {v5, v3, p0, v4}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentSpec;-><init>(LX/8i7;LX/0Or;LX/0Uh;)V

    .line 2275860
    move-object v0, v5

    .line 2275861
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2275862
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2275863
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2275864
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
