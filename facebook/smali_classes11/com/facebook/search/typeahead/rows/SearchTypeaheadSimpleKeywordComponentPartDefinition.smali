.class public Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleKeywordComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/search/model/KeywordTypeaheadUnit;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/Fiz;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Fiz;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2276080
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2276081
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleKeywordComponentPartDefinition;->d:LX/Fiz;

    .line 2276082
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/search/model/KeywordTypeaheadUnit;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/search/model/KeywordTypeaheadUnit;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2276083
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleKeywordComponentPartDefinition;->d:LX/Fiz;

    const/4 v1, 0x0

    .line 2276084
    new-instance v2, LX/Fiy;

    invoke-direct {v2, v0}, LX/Fiy;-><init>(LX/Fiz;)V

    .line 2276085
    sget-object p0, LX/Fiz;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Fix;

    .line 2276086
    if-nez p0, :cond_0

    .line 2276087
    new-instance p0, LX/Fix;

    invoke-direct {p0}, LX/Fix;-><init>()V

    .line 2276088
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/Fix;->a$redex0(LX/Fix;LX/1De;IILX/Fiy;)V

    .line 2276089
    move-object v2, p0

    .line 2276090
    move-object v1, v2

    .line 2276091
    move-object v0, v1

    .line 2276092
    iget-object v1, v0, LX/Fix;->a:LX/Fiy;

    iput-object p2, v1, LX/Fiy;->a:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 2276093
    iget-object v1, v0, LX/Fix;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2276094
    move-object v0, v0

    .line 2276095
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleKeywordComponentPartDefinition;
    .locals 5

    .prologue
    .line 2276096
    const-class v1, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleKeywordComponentPartDefinition;

    monitor-enter v1

    .line 2276097
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleKeywordComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2276098
    sput-object v2, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleKeywordComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2276099
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2276100
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2276101
    new-instance p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleKeywordComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/Fiz;->a(LX/0QB;)LX/Fiz;

    move-result-object v4

    check-cast v4, LX/Fiz;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleKeywordComponentPartDefinition;-><init>(Landroid/content/Context;LX/Fiz;)V

    .line 2276102
    move-object v0, p0

    .line 2276103
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2276104
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleKeywordComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2276105
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2276106
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2276107
    check-cast p2, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-direct {p0, p1, p2}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleKeywordComponentPartDefinition;->a(LX/1De;Lcom/facebook/search/model/KeywordTypeaheadUnit;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2276108
    check-cast p2, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-direct {p0, p1, p2}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleKeywordComponentPartDefinition;->a(LX/1De;Lcom/facebook/search/model/KeywordTypeaheadUnit;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2276109
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2276110
    const/4 v0, 0x0

    return-object v0
.end method
