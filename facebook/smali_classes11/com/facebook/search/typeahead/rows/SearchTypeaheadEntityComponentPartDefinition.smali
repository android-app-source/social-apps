.class public Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/search/model/EntityTypeaheadUnit;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/Fin;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Fin;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2275845
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2275846
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentPartDefinition;->d:LX/Fin;

    .line 2275847
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/search/model/EntityTypeaheadUnit;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/search/model/EntityTypeaheadUnit;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2275817
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentPartDefinition;->d:LX/Fin;

    const/4 v1, 0x0

    .line 2275818
    new-instance v2, LX/Fim;

    invoke-direct {v2, v0}, LX/Fim;-><init>(LX/Fin;)V

    .line 2275819
    sget-object p0, LX/Fin;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Fil;

    .line 2275820
    if-nez p0, :cond_0

    .line 2275821
    new-instance p0, LX/Fil;

    invoke-direct {p0}, LX/Fil;-><init>()V

    .line 2275822
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/Fil;->a$redex0(LX/Fil;LX/1De;IILX/Fim;)V

    .line 2275823
    move-object v2, p0

    .line 2275824
    move-object v1, v2

    .line 2275825
    move-object v0, v1

    .line 2275826
    iget-object v1, v0, LX/Fil;->a:LX/Fim;

    iput-object p2, v1, LX/Fim;->a:Lcom/facebook/search/model/EntityTypeaheadUnit;

    .line 2275827
    iget-object v1, v0, LX/Fil;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2275828
    move-object v0, v0

    .line 2275829
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentPartDefinition;
    .locals 5

    .prologue
    .line 2275834
    const-class v1, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentPartDefinition;

    monitor-enter v1

    .line 2275835
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2275836
    sput-object v2, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2275837
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2275838
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2275839
    new-instance p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/Fin;->a(LX/0QB;)LX/Fin;

    move-result-object v4

    check-cast v4, LX/Fin;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentPartDefinition;-><init>(Landroid/content/Context;LX/Fin;)V

    .line 2275840
    move-object v0, p0

    .line 2275841
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2275842
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2275843
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2275844
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2275833
    check-cast p2, Lcom/facebook/search/model/EntityTypeaheadUnit;

    invoke-direct {p0, p1, p2}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentPartDefinition;->a(LX/1De;Lcom/facebook/search/model/EntityTypeaheadUnit;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2275832
    check-cast p2, Lcom/facebook/search/model/EntityTypeaheadUnit;

    invoke-direct {p0, p1, p2}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentPartDefinition;->a(LX/1De;Lcom/facebook/search/model/EntityTypeaheadUnit;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2275831
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2275830
    const/4 v0, 0x0

    return-object v0
.end method
