.class public Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemClickPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/search/model/NullStateModuleSuggestionUnit;",
        "Landroid/view/View$OnClickListener;",
        "LX/FhE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2276197
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2276198
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemClickPartDefinition;
    .locals 3

    .prologue
    .line 2276199
    const-class v1, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemClickPartDefinition;

    monitor-enter v1

    .line 2276200
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemClickPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2276201
    sput-object v2, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemClickPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2276202
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2276203
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2276204
    new-instance v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemClickPartDefinition;

    invoke-direct {v0}, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemClickPartDefinition;-><init>()V

    .line 2276205
    move-object v0, v0

    .line 2276206
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2276207
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemClickPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2276208
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2276209
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2276210
    check-cast p2, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;

    check-cast p3, LX/FhE;

    .line 2276211
    new-instance v0, LX/Fj3;

    invoke-direct {v0, p0, p3, p2}, LX/Fj3;-><init>(Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemClickPartDefinition;LX/FhE;Lcom/facebook/search/model/NullStateModuleSuggestionUnit;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x70ec20c6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2276212
    check-cast p2, Landroid/view/View$OnClickListener;

    .line 2276213
    invoke-virtual {p4, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2276214
    const/16 v1, 0x1f

    const v2, 0x1eef9928

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2276215
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2276216
    return-void
.end method
