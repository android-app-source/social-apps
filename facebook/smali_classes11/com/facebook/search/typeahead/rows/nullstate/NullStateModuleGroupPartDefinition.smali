.class public Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/search/model/NullStateModuleCollectionUnit;",
        "Ljava/lang/Void;",
        "LX/FhE;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;

.field private final c:Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;

.field private final d:Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;

.field private final e:Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGapPartDefinition;


# direct methods
.method public constructor <init>(LX/0Ot;Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGapPartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;",
            ">;",
            "Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;",
            "Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;",
            "Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;",
            "Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGapPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2276187
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2276188
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;->a:LX/0Ot;

    .line 2276189
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;->b:Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;

    .line 2276190
    iput-object p3, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;->c:Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;

    .line 2276191
    iput-object p4, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;->d:Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;

    .line 2276192
    iput-object p5, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;->e:Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGapPartDefinition;

    .line 2276193
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;
    .locals 9

    .prologue
    .line 2276176
    const-class v1, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;

    monitor-enter v1

    .line 2276177
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2276178
    sput-object v2, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2276179
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2276180
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2276181
    new-instance v3, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;

    const/16 v4, 0x11d0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;->a(LX/0QB;)Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGapPartDefinition;->a(LX/0QB;)Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGapPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGapPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;-><init>(LX/0Ot;Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGapPartDefinition;)V

    .line 2276182
    move-object v0, v3

    .line 2276183
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2276184
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2276185
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2276186
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2276157
    check-cast p2, Lcom/facebook/search/model/NullStateModuleCollectionUnit;

    check-cast p3, LX/FhE;

    .line 2276158
    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2276159
    const/4 v2, 0x0

    .line 2276160
    iget-object v1, p3, LX/FhE;->p:LX/Fid;

    .line 2276161
    iget-object v3, v1, LX/Fid;->a:Ljava/util/List;

    move-object v1, v3

    .line 2276162
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/model/TypeaheadUnit;

    .line 2276163
    instance-of v4, v1, Lcom/facebook/search/model/GapTypeaheadUnit;

    if-nez v4, :cond_0

    .line 2276164
    if-ne v1, p2, :cond_2

    const/4 v1, 0x1

    .line 2276165
    :goto_0
    move v1, v1

    .line 2276166
    if-nez v1, :cond_1

    .line 2276167
    iget-object v1, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;->e:Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGapPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2276168
    :cond_1
    iget-object v1, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;->b:Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;

    invoke-static {p1, v1, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;->a:LX/0Ot;

    invoke-virtual {v1, v2, v0}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 2276169
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;->c:Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2276170
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGroupPartDefinition;->d:Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2276171
    const/4 v0, 0x0

    return-object v0

    :cond_2
    move v1, v2

    .line 2276172
    goto :goto_0

    :cond_3
    move v1, v2

    .line 2276173
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2276174
    check-cast p1, Lcom/facebook/search/model/NullStateModuleCollectionUnit;

    .line 2276175
    invoke-virtual {p1}, Lcom/facebook/search/model/TypeaheadCollectionUnit;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
