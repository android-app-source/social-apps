.class public Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/0ad;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1vg;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nu",
            "<TE;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2276615
    const-class v0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;

    const-string v1, "search"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/1vg;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1nu;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2276610
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2276611
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;->b:LX/0ad;

    .line 2276612
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;->c:LX/0Ot;

    .line 2276613
    iput-object p3, p0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;->d:LX/0Ot;

    .line 2276614
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;
    .locals 6

    .prologue
    .line 2276599
    const-class v1, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;

    monitor-enter v1

    .line 2276600
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2276601
    sput-object v2, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2276602
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2276603
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2276604
    new-instance v4, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    const/16 v5, 0x3b2

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x963

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;-><init>(LX/0ad;LX/0Ot;LX/0Ot;)V

    .line 2276605
    move-object v0, v4

    .line 2276606
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2276607
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2276608
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2276609
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
