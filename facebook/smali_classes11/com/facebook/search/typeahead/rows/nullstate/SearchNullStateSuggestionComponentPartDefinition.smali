.class public Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/search/suggestions/environment/CanVisitTypeaheadSuggestion;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/FjF;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/FjF;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2276564
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2276565
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentPartDefinition;->d:LX/FjF;

    .line 2276566
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;LX/FhE;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2276567
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentPartDefinition;->d:LX/FjF;

    const/4 v1, 0x0

    .line 2276568
    new-instance v2, LX/FjE;

    invoke-direct {v2, v0}, LX/FjE;-><init>(LX/FjF;)V

    .line 2276569
    iget-object p0, v0, LX/FjF;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/FjD;

    .line 2276570
    if-nez p0, :cond_0

    .line 2276571
    new-instance p0, LX/FjD;

    invoke-direct {p0, v0}, LX/FjD;-><init>(LX/FjF;)V

    .line 2276572
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/FjD;->a$redex0(LX/FjD;LX/1De;IILX/FjE;)V

    .line 2276573
    move-object v2, p0

    .line 2276574
    move-object v1, v2

    .line 2276575
    move-object v0, v1

    .line 2276576
    check-cast p3, LX/1Pp;

    .line 2276577
    iget-object v1, v0, LX/FjD;->a:LX/FjE;

    iput-object p3, v1, LX/FjE;->a:LX/1Pp;

    .line 2276578
    iget-object v1, v0, LX/FjD;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2276579
    move-object v0, v0

    .line 2276580
    iget-object v1, v0, LX/FjD;->a:LX/FjE;

    iput-object p2, v1, LX/FjE;->b:Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    .line 2276581
    iget-object v1, v0, LX/FjD;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2276582
    move-object v0, v0

    .line 2276583
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentPartDefinition;
    .locals 5

    .prologue
    .line 2276584
    const-class v1, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentPartDefinition;

    monitor-enter v1

    .line 2276585
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2276586
    sput-object v2, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2276587
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2276588
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2276589
    new-instance p0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/FjF;->a(LX/0QB;)LX/FjF;

    move-result-object v4

    check-cast v4, LX/FjF;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentPartDefinition;-><init>(Landroid/content/Context;LX/FjF;)V

    .line 2276590
    move-object v0, p0

    .line 2276591
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2276592
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2276593
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2276594
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2276595
    check-cast p2, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    check-cast p3, LX/FhE;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentPartDefinition;->a(LX/1De;Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;LX/FhE;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2276596
    check-cast p2, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    check-cast p3, LX/FhE;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentPartDefinition;->a(LX/1De;Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;LX/FhE;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2276597
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2276598
    const/4 v0, 0x0

    return-object v0
.end method
