.class public Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/search/model/NullStateModuleCollectionUnit;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/0ad;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0ad;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2276302
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2276303
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;->d:LX/0ad;

    .line 2276304
    return-void
.end method

.method private static a(LX/1De;Lcom/facebook/search/model/NullStateModuleCollectionUnit;)LX/1X1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/search/model/NullStateModuleCollectionUnit;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2276299
    invoke-static {p0}, LX/Fj8;->c(LX/1De;)LX/Fj6;

    move-result-object v0

    .line 2276300
    iget-object v1, p1, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2276301
    invoke-virtual {v0, v1}, LX/Fj6;->b(Ljava/lang/String;)LX/Fj6;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;
    .locals 5

    .prologue
    .line 2276288
    const-class v1, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;

    monitor-enter v1

    .line 2276289
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2276290
    sput-object v2, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2276291
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2276292
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2276293
    new-instance p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;-><init>(Landroid/content/Context;LX/0ad;)V

    .line 2276294
    move-object v0, p0

    .line 2276295
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2276296
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2276297
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2276298
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2276287
    check-cast p2, Lcom/facebook/search/model/NullStateModuleCollectionUnit;

    invoke-static {p1, p2}, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;->a(LX/1De;Lcom/facebook/search/model/NullStateModuleCollectionUnit;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2276284
    check-cast p2, Lcom/facebook/search/model/NullStateModuleCollectionUnit;

    invoke-static {p1, p2}, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;->a(LX/1De;Lcom/facebook/search/model/NullStateModuleCollectionUnit;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2276286
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitleComponentPartDefinition;->d:LX/0ad;

    sget-short v1, LX/100;->cf:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2276285
    const/4 v0, 0x0

    return-object v0
.end method
