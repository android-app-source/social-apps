.class public Lcom/facebook/search/typeahead/rows/nullstate/SearchTypeaheadHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/search/model/HeaderRowTypeaheadUnit;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/FjC;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/FjC;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2276746
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2276747
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/nullstate/SearchTypeaheadHeaderComponentPartDefinition;->d:LX/FjC;

    .line 2276748
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/search/model/HeaderRowTypeaheadUnit;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/search/model/HeaderRowTypeaheadUnit;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2276733
    iget-object v0, p2, Lcom/facebook/search/model/HeaderRowTypeaheadUnit;->a:LX/Cwc;

    move-object v0, v0

    .line 2276734
    invoke-static {p1}, LX/Fj8;->c(LX/1De;)LX/Fj6;

    move-result-object v1

    .line 2276735
    iget-object v2, v0, LX/Cwc;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2276736
    invoke-virtual {v1, v2}, LX/Fj6;->b(Ljava/lang/String;)LX/Fj6;

    move-result-object v1

    .line 2276737
    iget-object v2, v0, LX/Cwc;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2276738
    iget-object p1, v1, LX/Fj6;->a:LX/Fj7;

    iput-object v2, p1, LX/Fj7;->b:Ljava/lang/String;

    .line 2276739
    move-object v1, v1

    .line 2276740
    iget-object v2, p0, Lcom/facebook/search/typeahead/rows/nullstate/SearchTypeaheadHeaderComponentPartDefinition;->d:LX/FjC;

    .line 2276741
    iget-object p0, v0, LX/Cwc;->a:LX/Cwb;

    move-object v0, p0

    .line 2276742
    invoke-virtual {v2, v0}, LX/FjC;->a(LX/Cwb;)Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 2276743
    iget-object v2, v1, LX/Fj6;->a:LX/Fj7;

    iput-object v0, v2, LX/Fj7;->c:Landroid/view/View$OnClickListener;

    .line 2276744
    move-object v0, v1

    .line 2276745
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/nullstate/SearchTypeaheadHeaderComponentPartDefinition;
    .locals 5

    .prologue
    .line 2276722
    const-class v1, Lcom/facebook/search/typeahead/rows/nullstate/SearchTypeaheadHeaderComponentPartDefinition;

    monitor-enter v1

    .line 2276723
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/nullstate/SearchTypeaheadHeaderComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2276724
    sput-object v2, Lcom/facebook/search/typeahead/rows/nullstate/SearchTypeaheadHeaderComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2276725
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2276726
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2276727
    new-instance p0, Lcom/facebook/search/typeahead/rows/nullstate/SearchTypeaheadHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/FjC;->b(LX/0QB;)LX/FjC;

    move-result-object v4

    check-cast v4, LX/FjC;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/typeahead/rows/nullstate/SearchTypeaheadHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/FjC;)V

    .line 2276728
    move-object v0, p0

    .line 2276729
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2276730
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/nullstate/SearchTypeaheadHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2276731
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2276732
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2276749
    check-cast p2, Lcom/facebook/search/model/HeaderRowTypeaheadUnit;

    invoke-direct {p0, p1, p2}, Lcom/facebook/search/typeahead/rows/nullstate/SearchTypeaheadHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/search/model/HeaderRowTypeaheadUnit;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2276721
    check-cast p2, Lcom/facebook/search/model/HeaderRowTypeaheadUnit;

    invoke-direct {p0, p1, p2}, Lcom/facebook/search/typeahead/rows/nullstate/SearchTypeaheadHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/search/model/HeaderRowTypeaheadUnit;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2276720
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2276719
    const/4 v0, 0x0

    return-object v0
.end method
