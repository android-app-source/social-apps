.class public Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/search/model/NullStateModuleCollectionUnit;",
        "Ljava/lang/Void;",
        "LX/FhE;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateModuleItemComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateModuleItemComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2276245
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2276246
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;->a:LX/0Ot;

    .line 2276247
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;->b:LX/0Ot;

    .line 2276248
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;
    .locals 5

    .prologue
    .line 2276223
    const-class v1, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;

    monitor-enter v1

    .line 2276224
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2276225
    sput-object v2, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2276226
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2276227
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2276228
    new-instance v3, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;

    const/16 v4, 0x11ce

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x3503

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;-><init>(LX/0Ot;LX/0Ot;)V

    .line 2276229
    move-object v0, v3

    .line 2276230
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2276231
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2276232
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2276233
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2276234
    check-cast p2, Lcom/facebook/search/model/NullStateModuleCollectionUnit;

    .line 2276235
    iget-boolean v0, p2, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->g:Z

    move v0, v0

    .line 2276236
    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/search/model/TypeaheadCollectionUnit;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    move v1, v0

    .line 2276237
    :goto_0
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v1, :cond_1

    .line 2276238
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {p2}, Lcom/facebook/search/model/TypeaheadCollectionUnit;->o()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {p1, v0, v3}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemsGroupPartDefinition;->a:LX/0Ot;

    invoke-virtual {p2}, Lcom/facebook/search/model/TypeaheadCollectionUnit;->o()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 2276239
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2276240
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/search/model/TypeaheadCollectionUnit;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2276241
    iget v1, p2, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->f:I

    move v1, v1

    .line 2276242
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v1, v0

    goto :goto_0

    .line 2276243
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2276244
    const/4 v0, 0x1

    return v0
.end method
