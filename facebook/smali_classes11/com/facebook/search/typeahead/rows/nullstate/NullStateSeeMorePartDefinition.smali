.class public Lcom/facebook/search/typeahead/rows/nullstate/NullStateSeeMorePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/search/suggestions/environment/CanVisitTypeaheadSuggestion;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/1Ua;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final e:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2276412
    invoke-static {}, LX/1UY;->c()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3f800000    # -4.0f

    .line 2276413
    iput v1, v0, LX/1UY;->b:F

    .line 2276414
    move-object v0, v0

    .line 2276415
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateSeeMorePartDefinition;->a:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2276406
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2276407
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateSeeMorePartDefinition;->b:Landroid/content/Context;

    .line 2276408
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateSeeMorePartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2276409
    iput-object p3, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateSeeMorePartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2276410
    iput-object p4, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateSeeMorePartDefinition;->e:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    .line 2276411
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/nullstate/NullStateSeeMorePartDefinition;
    .locals 7

    .prologue
    .line 2276395
    const-class v1, Lcom/facebook/search/typeahead/rows/nullstate/NullStateSeeMorePartDefinition;

    monitor-enter v1

    .line 2276396
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateSeeMorePartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2276397
    sput-object v2, Lcom/facebook/search/typeahead/rows/nullstate/NullStateSeeMorePartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2276398
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2276399
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2276400
    new-instance p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateSeeMorePartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/typeahead/rows/nullstate/NullStateSeeMorePartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;)V

    .line 2276401
    move-object v0, p0

    .line 2276402
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2276403
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateSeeMorePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2276404
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2276405
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2276394
    sget-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2276389
    check-cast p2, Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;

    check-cast p3, LX/FhE;

    .line 2276390
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateSeeMorePartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v1, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateSeeMorePartDefinition;->b:Landroid/content/Context;

    const v2, 0x7f082322

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2276391
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateSeeMorePartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/FjA;

    invoke-direct {v1, p0, p3, p2}, LX/FjA;-><init>(Lcom/facebook/search/typeahead/rows/nullstate/NullStateSeeMorePartDefinition;LX/FhE;Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2276392
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateSeeMorePartDefinition;->e:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/search/typeahead/rows/nullstate/NullStateSeeMorePartDefinition;->a:LX/1Ua;

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2276393
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2276388
    const/4 v0, 0x1

    return v0
.end method
