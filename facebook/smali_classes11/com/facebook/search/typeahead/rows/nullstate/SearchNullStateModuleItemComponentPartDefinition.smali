.class public Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateModuleItemComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/search/suggestions/environment/CanVisitTypeaheadSuggestion;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/search/model/NullStateModuleSuggestionUnit;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/0ad;

.field private final e:LX/FjI;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0ad;LX/FjI;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2276471
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2276472
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateModuleItemComponentPartDefinition;->d:LX/0ad;

    .line 2276473
    iput-object p3, p0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateModuleItemComponentPartDefinition;->e:LX/FjI;

    .line 2276474
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/search/model/NullStateModuleSuggestionUnit;LX/FhE;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/search/model/NullStateModuleSuggestionUnit;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2276455
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateModuleItemComponentPartDefinition;->e:LX/FjI;

    const/4 v1, 0x0

    .line 2276456
    new-instance v2, LX/FjH;

    invoke-direct {v2, v0}, LX/FjH;-><init>(LX/FjI;)V

    .line 2276457
    iget-object p0, v0, LX/FjI;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/FjG;

    .line 2276458
    if-nez p0, :cond_0

    .line 2276459
    new-instance p0, LX/FjG;

    invoke-direct {p0, v0}, LX/FjG;-><init>(LX/FjI;)V

    .line 2276460
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/FjG;->a$redex0(LX/FjG;LX/1De;IILX/FjH;)V

    .line 2276461
    move-object v2, p0

    .line 2276462
    move-object v1, v2

    .line 2276463
    move-object v0, v1

    .line 2276464
    iget-object v1, v0, LX/FjG;->a:LX/FjH;

    iput-object p2, v1, LX/FjH;->a:Lcom/facebook/search/model/NullStateModuleSuggestionUnit;

    .line 2276465
    iget-object v1, v0, LX/FjG;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2276466
    move-object v0, v0

    .line 2276467
    iget-object v1, v0, LX/FjG;->a:LX/FjH;

    iput-object p3, v1, LX/FjH;->b:LX/FhE;

    .line 2276468
    iget-object v1, v0, LX/FjG;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2276469
    move-object v0, v0

    .line 2276470
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateModuleItemComponentPartDefinition;
    .locals 6

    .prologue
    .line 2276444
    const-class v1, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateModuleItemComponentPartDefinition;

    monitor-enter v1

    .line 2276445
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateModuleItemComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2276446
    sput-object v2, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateModuleItemComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2276447
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2276448
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2276449
    new-instance p0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateModuleItemComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/FjI;->a(LX/0QB;)LX/FjI;

    move-result-object v5

    check-cast v5, LX/FjI;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateModuleItemComponentPartDefinition;-><init>(Landroid/content/Context;LX/0ad;LX/FjI;)V

    .line 2276450
    move-object v0, p0

    .line 2276451
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2276452
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateModuleItemComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2276453
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2276454
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2276475
    check-cast p2, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;

    check-cast p3, LX/FhE;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateModuleItemComponentPartDefinition;->a(LX/1De;Lcom/facebook/search/model/NullStateModuleSuggestionUnit;LX/FhE;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2276443
    check-cast p2, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;

    check-cast p3, LX/FhE;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateModuleItemComponentPartDefinition;->a(LX/1De;Lcom/facebook/search/model/NullStateModuleSuggestionUnit;LX/FhE;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2276442
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateModuleItemComponentPartDefinition;->d:LX/0ad;

    sget-short v1, LX/100;->cf:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2276441
    const/4 v0, 0x0

    return-object v0
.end method
