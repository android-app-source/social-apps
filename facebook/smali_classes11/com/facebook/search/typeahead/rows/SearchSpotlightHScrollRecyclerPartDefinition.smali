.class public Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/model/SearchSpotlightCollectionUnit;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

.field public final d:Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;

.field public final e:Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightIntroPagePartDefinition;

.field public final f:LX/13B;

.field public final g:LX/2dq;

.field public final h:LX/0hB;

.field private final i:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightIntroPagePartDefinition;LX/13B;LX/2dq;LX/0hB;LX/0Uh;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2275614
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2275615
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->a:Landroid/content/Context;

    .line 2275616
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->b:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    .line 2275617
    iput-object p3, p0, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2275618
    iput-object p4, p0, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->d:Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;

    .line 2275619
    iput-object p5, p0, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->e:Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightIntroPagePartDefinition;

    .line 2275620
    iput-object p6, p0, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->f:LX/13B;

    .line 2275621
    iput-object p7, p0, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->g:LX/2dq;

    .line 2275622
    iput-object p8, p0, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->h:LX/0hB;

    .line 2275623
    iput-object p9, p0, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->i:LX/0Uh;

    .line 2275624
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;
    .locals 13

    .prologue
    .line 2275639
    const-class v1, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;

    monitor-enter v1

    .line 2275640
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2275641
    sput-object v2, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2275642
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2275643
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2275644
    new-instance v3, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;->a(LX/0QB;)Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightIntroPagePartDefinition;->a(LX/0QB;)Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightIntroPagePartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightIntroPagePartDefinition;

    invoke-static {v0}, LX/13B;->b(LX/0QB;)LX/13B;

    move-result-object v9

    check-cast v9, LX/13B;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v10

    check-cast v10, LX/2dq;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v11

    check-cast v11, LX/0hB;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightIntroPagePartDefinition;LX/13B;LX/2dq;LX/0hB;LX/0Uh;)V

    .line 2275645
    move-object v0, v3

    .line 2275646
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2275647
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2275648
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2275649
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2275638
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2275626
    check-cast p2, Lcom/facebook/search/model/SearchSpotlightCollectionUnit;

    .line 2275627
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->b:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    new-instance v1, LX/1X6;

    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    sget-object v4, LX/1X9;->BOTTOM:LX/1X9;

    invoke-direct {v1, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2275628
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    const/4 v9, 0x1

    .line 2275629
    new-instance v5, Landroid/util/TypedValue;

    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    .line 2275630
    iget-object v6, p0, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->a:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b14f4

    invoke-virtual {v6, v7, v5, v9}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 2275631
    iget-object v6, p0, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->h:LX/0hB;

    invoke-virtual {v6}, LX/0hB;->c()I

    move-result v6

    iget-object v7, p0, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->h:LX/0hB;

    invoke-virtual {v7}, LX/0hB;->d()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    int-to-float v6, v6

    .line 2275632
    iget-object v7, p0, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->a:Landroid/content/Context;

    invoke-static {v7, v6}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v5}, Landroid/util/TypedValue;->getFloat()F

    move-result v5

    mul-float/2addr v6, v5

    .line 2275633
    new-instance v5, LX/2eG;

    iget-object v7, p0, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->g:LX/2dq;

    const/high16 v8, 0x41000000    # 8.0f

    add-float/2addr v6, v8

    sget-object v8, LX/2eF;->a:LX/1Ua;

    invoke-virtual {v7, v6, v8, v9}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v6

    const/4 v7, 0x0

    .line 2275634
    new-instance v8, LX/Fif;

    invoke-direct {v8, p0, p2}, LX/Fif;-><init>(Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;Lcom/facebook/search/model/SearchSpotlightCollectionUnit;)V

    move-object v8, v8

    .line 2275635
    invoke-virtual {p2}, Lcom/facebook/graphql/model/BaseFeedUnit;->g()Ljava/lang/String;

    move-result-object v9

    move-object v10, p2

    invoke-direct/range {v5 .. v10}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    move-object v1, v5

    .line 2275636
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2275637
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2275625
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->i:LX/0Uh;

    sget v1, LX/2SU;->E:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
