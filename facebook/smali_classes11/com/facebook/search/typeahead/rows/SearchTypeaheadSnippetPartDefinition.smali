.class public Lcom/facebook/search/typeahead/rows/SearchTypeaheadSnippetPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/util/List",
        "<",
        "LX/Cwf;",
        ">;",
        "Ljava/lang/CharSequence;",
        "LX/1PW;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2276130
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2276131
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSnippetPartDefinition;->a:Landroid/content/Context;

    .line 2276132
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSnippetPartDefinition;->b:Landroid/content/res/Resources;

    .line 2276133
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2276137
    check-cast p2, Ljava/util/List;

    .line 2276138
    new-instance v1, LX/3DI;

    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSnippetPartDefinition;->b:Landroid/content/res/Resources;

    const v2, 0x7f082a7b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/3DI;-><init>(Ljava/lang/CharSequence;)V

    .line 2276139
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cwf;

    .line 2276140
    iget-object v3, v0, LX/Cwf;->a:Ljava/lang/CharSequence;

    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    iget-object v5, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSnippetPartDefinition;->a:Landroid/content/Context;

    iget v0, v0, LX/Cwf;->b:I

    invoke-direct {v4, v5, v0}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const/16 v0, 0x11

    invoke-virtual {v1, v3, v4, v0}, LX/3DI;->a(Ljava/lang/CharSequence;Ljava/lang/Object;I)LX/3DI;

    goto :goto_0

    .line 2276141
    :cond_0
    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1050007d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2276134
    check-cast p2, Ljava/lang/CharSequence;

    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    .line 2276135
    invoke-virtual {p4, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2276136
    const/16 v1, 0x1f

    const v2, 0x10f5dbaa

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
