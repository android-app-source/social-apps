.class public Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightIntroPagePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/model/SearchSpotlightIntroUnit;",
        "Ljava/lang/Void;",
        "LX/1Pn;",
        "Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightIntroView;",
        ">;"
    }
.end annotation


# static fields
.field public static a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightIntroView;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2275583
    new-instance v0, LX/Fie;

    invoke-direct {v0}, LX/Fie;-><init>()V

    sput-object v0, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightIntroPagePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2275584
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2275585
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightIntroPagePartDefinition;->b:LX/0Uh;

    .line 2275586
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightIntroPagePartDefinition;
    .locals 4

    .prologue
    .line 2275587
    const-class v1, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightIntroPagePartDefinition;

    monitor-enter v1

    .line 2275588
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightIntroPagePartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2275589
    sput-object v2, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightIntroPagePartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2275590
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2275591
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2275592
    new-instance p0, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightIntroPagePartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightIntroPagePartDefinition;-><init>(LX/0Uh;)V

    .line 2275593
    move-object v0, p0

    .line 2275594
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2275595
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightIntroPagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2275596
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2275597
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightIntroView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2275598
    sget-object v0, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightIntroPagePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x76361290

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2275599
    check-cast p1, Lcom/facebook/search/model/SearchSpotlightIntroUnit;

    check-cast p4, Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightIntroView;

    .line 2275600
    iget-object v1, p1, Lcom/facebook/search/model/SearchSpotlightIntroUnit;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2275601
    invoke-virtual {p4, v1}, Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightIntroView;->setText(Ljava/lang/CharSequence;)V

    .line 2275602
    const/16 v1, 0x1f

    const v2, 0x47fa6c8b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2275603
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightIntroPagePartDefinition;->b:LX/0Uh;

    sget v1, LX/2SU;->E:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
