.class public Lcom/facebook/search/typeahead/rows/SearchTypeaheadThumbnailPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/Cwj;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2276142
    const-class v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadThumbnailPartDefinition;

    const-string v1, "search"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadThumbnailPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2276143
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2276144
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0xf59dec6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2276145
    check-cast p1, LX/Cwj;

    check-cast p4, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2276146
    iget-object v1, p1, LX/Cwj;->a:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/search/typeahead/rows/SearchTypeaheadThumbnailPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2276147
    invoke-virtual {p4}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v1

    check-cast v1, LX/1af;

    iget-object v2, p1, LX/Cwj;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2276148
    const/16 v1, 0x1f

    const v2, 0x21826856

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
