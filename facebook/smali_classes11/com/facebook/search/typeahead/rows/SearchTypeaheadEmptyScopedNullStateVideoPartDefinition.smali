.class public Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/search/model/EmptyScopedNullStateTypeaheadUnit;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/Fit;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Fit;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2275714
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2275715
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition;->d:LX/Fit;

    .line 2275716
    return-void
.end method

.method private a(LX/1De;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2275717
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition;->d:LX/Fit;

    const/4 v1, 0x0

    .line 2275718
    new-instance v2, LX/Fis;

    invoke-direct {v2, v0}, LX/Fis;-><init>(LX/Fit;)V

    .line 2275719
    sget-object p0, LX/Fit;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Fir;

    .line 2275720
    if-nez p0, :cond_0

    .line 2275721
    new-instance p0, LX/Fir;

    invoke-direct {p0}, LX/Fir;-><init>()V

    .line 2275722
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/Fir;->a$redex0(LX/Fir;LX/1De;IILX/Fis;)V

    .line 2275723
    move-object v2, p0

    .line 2275724
    move-object v1, v2

    .line 2275725
    move-object v0, v1

    .line 2275726
    const v1, 0x7f0820b3

    .line 2275727
    iget-object v2, v0, LX/Fir;->a:LX/Fis;

    invoke-virtual {v0, v1}, LX/1Dp;->b(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v2, LX/Fis;->a:Ljava/lang/String;

    .line 2275728
    iget-object v2, v0, LX/Fir;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2275729
    move-object v0, v0

    .line 2275730
    const v1, 0x7f0820bc

    .line 2275731
    iget-object v2, v0, LX/Fir;->a:LX/Fis;

    invoke-virtual {v0, v1}, LX/1Dp;->b(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v2, LX/Fis;->b:Ljava/lang/String;

    .line 2275732
    iget-object v2, v0, LX/Fir;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2275733
    move-object v0, v0

    .line 2275734
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition;
    .locals 5

    .prologue
    .line 2275735
    const-class v1, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition;

    monitor-enter v1

    .line 2275736
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2275737
    sput-object v2, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2275738
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2275739
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2275740
    new-instance p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/Fit;->a(LX/0QB;)LX/Fit;

    move-result-object v4

    check-cast v4, LX/Fit;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition;-><init>(Landroid/content/Context;LX/Fit;)V

    .line 2275741
    move-object v0, p0

    .line 2275742
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2275743
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2275744
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2275745
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2275746
    invoke-direct {p0, p1}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition;->a(LX/1De;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2275747
    invoke-direct {p0, p1}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateVideoPartDefinition;->a(LX/1De;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2275748
    check-cast p1, Lcom/facebook/search/model/EmptyScopedNullStateTypeaheadUnit;

    .line 2275749
    iget-object v0, p1, Lcom/facebook/search/model/EmptyScopedNullStateTypeaheadUnit;->a:LX/103;

    move-object v0, v0

    .line 2275750
    sget-object v1, LX/103;->VIDEO:LX/103;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2275751
    const/4 v0, 0x0

    return-object v0
.end method
