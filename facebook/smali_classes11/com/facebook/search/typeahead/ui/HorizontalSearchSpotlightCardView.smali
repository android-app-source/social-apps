.class public Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightCardView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2276750
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2276751
    invoke-direct {p0}, Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightCardView;->a()V

    .line 2276752
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2276753
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2276754
    invoke-direct {p0}, Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightCardView;->a()V

    .line 2276755
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2276756
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2276757
    invoke-direct {p0}, Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightCardView;->a()V

    .line 2276758
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2276759
    const v0, 0x7f030893

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2276760
    const v0, 0x7f0d163f

    invoke-virtual {p0, v0}, Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightCardView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2276761
    return-void
.end method


# virtual methods
.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 2276762
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 2276763
    iget-object v0, p0, Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightCardView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2276764
    :goto_0
    return-void

    .line 2276765
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightCardView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2276766
    iget-object v0, p0, Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightCardView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2276767
    return-void
.end method
