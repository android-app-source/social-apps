.class public Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightIntroView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2276779
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2276780
    invoke-direct {p0}, Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightIntroView;->a()V

    .line 2276781
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2276776
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2276777
    invoke-direct {p0}, Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightIntroView;->a()V

    .line 2276778
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2276768
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2276769
    invoke-direct {p0}, Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightIntroView;->a()V

    .line 2276770
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2276773
    const v0, 0x7f030895

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2276774
    const v0, 0x7f0d1640

    invoke-virtual {p0, v0}, Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightIntroView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightIntroView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2276775
    return-void
.end method


# virtual methods
.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2276771
    iget-object v0, p0, Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightIntroView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2276772
    return-void
.end method
