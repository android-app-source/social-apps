.class public Lcom/facebook/search/suggestions/ui/TypeaheadDividerView;
.super Landroid/view/View;
.source ""


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2275102
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2275103
    invoke-direct {p0}, Lcom/facebook/search/suggestions/ui/TypeaheadDividerView;->a()V

    .line 2275104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2275087
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2275088
    invoke-direct {p0}, Lcom/facebook/search/suggestions/ui/TypeaheadDividerView;->a()V

    .line 2275089
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2275099
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2275100
    invoke-direct {p0}, Lcom/facebook/search/suggestions/ui/TypeaheadDividerView;->a()V

    .line 2275101
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2275093
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/suggestions/ui/TypeaheadDividerView;->a:Landroid/graphics/Paint;

    .line 2275094
    iget-object v0, p0, Lcom/facebook/search/suggestions/ui/TypeaheadDividerView;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/search/suggestions/ui/TypeaheadDividerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2275095
    iget-object v0, p0, Lcom/facebook/search/suggestions/ui/TypeaheadDividerView;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/search/suggestions/ui/TypeaheadDividerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b16d9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2275096
    invoke-virtual {p0}, Lcom/facebook/search/suggestions/ui/TypeaheadDividerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1769

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/suggestions/ui/TypeaheadDividerView;->b:I

    .line 2275097
    invoke-virtual {p0}, Lcom/facebook/search/suggestions/ui/TypeaheadDividerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b176a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/suggestions/ui/TypeaheadDividerView;->c:I

    .line 2275098
    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2275090
    invoke-super {p0, p1}, Landroid/view/View;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2275091
    iget v0, p0, Lcom/facebook/search/suggestions/ui/TypeaheadDividerView;->b:I

    int-to-float v1, v0

    iget v0, p0, Lcom/facebook/search/suggestions/ui/TypeaheadDividerView;->c:I

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/facebook/search/suggestions/ui/TypeaheadDividerView;->getWidth()I

    move-result v0

    iget v3, p0, Lcom/facebook/search/suggestions/ui/TypeaheadDividerView;->b:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    iget v0, p0, Lcom/facebook/search/suggestions/ui/TypeaheadDividerView;->c:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/search/suggestions/ui/TypeaheadDividerView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2275092
    return-void
.end method
