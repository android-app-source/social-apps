.class public final Lcom/facebook/search/suggestions/fetchers/BaseRemoteTypeaheadFetcher$1$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/7Hi;

.field public final synthetic b:I

.field public final synthetic c:LX/FhG;


# direct methods
.method public constructor <init>(LX/FhG;LX/7Hi;I)V
    .locals 0

    .prologue
    .line 2272465
    iput-object p1, p0, Lcom/facebook/search/suggestions/fetchers/BaseRemoteTypeaheadFetcher$1$1;->c:LX/FhG;

    iput-object p2, p0, Lcom/facebook/search/suggestions/fetchers/BaseRemoteTypeaheadFetcher$1$1;->a:LX/7Hi;

    iput p3, p0, Lcom/facebook/search/suggestions/fetchers/BaseRemoteTypeaheadFetcher$1$1;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 2272466
    iget-object v0, p0, Lcom/facebook/search/suggestions/fetchers/BaseRemoteTypeaheadFetcher$1$1;->c:LX/FhG;

    iget-object v0, v0, LX/FhG;->b:LX/FhH;

    iget-object v1, p0, Lcom/facebook/search/suggestions/fetchers/BaseRemoteTypeaheadFetcher$1$1;->a:LX/7Hi;

    iget v2, p0, Lcom/facebook/search/suggestions/fetchers/BaseRemoteTypeaheadFetcher$1$1;->b:I

    .line 2272467
    iget-object v3, v0, LX/FhH;->c:LX/Fhc;

    invoke-virtual {v0}, LX/7HT;->b()LX/7HY;

    move-result-object v4

    .line 2272468
    iget-object v5, v1, LX/7Hi;->b:LX/7Hc;

    move-object v5, v5

    .line 2272469
    iget-object v6, v5, LX/7Hc;->b:LX/0Px;

    move-object v5, v6

    .line 2272470
    iget-object v6, v1, LX/7Hi;->a:LX/7B6;

    move-object v6, v6

    .line 2272471
    invoke-static {v3, v4}, LX/Fhc;->a(LX/Fhc;LX/7HY;)LX/Fhd;

    move-result-object v7

    .line 2272472
    iget-object v8, v7, LX/Fhd;->f:Ljava/util/HashMap;

    invoke-virtual {v8, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    .line 2272473
    if-eqz v8, :cond_0

    iget-object v9, v7, LX/Fhd;->g:Ljava/util/BitSet;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-virtual {v9, p0}, Ljava/util/BitSet;->get(I)Z

    move-result v9

    if-nez v9, :cond_2

    .line 2272474
    :cond_0
    :goto_0
    iget-object v3, v0, LX/7HT;->f:LX/7HZ;

    move-object v3, v3

    .line 2272475
    sget-object v4, LX/7HZ;->IDLE:LX/7HZ;

    if-eq v3, v4, :cond_1

    .line 2272476
    iget-object v3, v0, LX/7HT;->f:LX/7HZ;

    move-object v3, v3

    .line 2272477
    sget-object v4, LX/7HZ;->ERROR:LX/7HZ;

    if-eq v3, v4, :cond_1

    .line 2272478
    invoke-static {v0, v1}, LX/7HT;->e(LX/7HT;LX/7Hi;)V

    .line 2272479
    invoke-static {v0, v1}, LX/7HT;->c(LX/7HT;LX/7Hi;)V

    .line 2272480
    invoke-static {v0}, LX/7HT;->f(LX/7HT;)V

    .line 2272481
    :cond_1
    return-void

    .line 2272482
    :cond_2
    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2272483
    invoke-virtual {v7}, LX/Fhd;->b()LX/11o;

    move-result-object v9

    .line 2272484
    const-string p0, "network_batch_"

    invoke-static {p0, v2}, LX/Fhd;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p0

    const-string v3, "fetched_count"

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v3

    const v4, -0x566cae3b

    invoke-static {v9, p0, v8, v3, v4}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2272485
    const-string p0, "post_processing_batch_"

    invoke-static {p0, v2}, LX/Fhd;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p0

    const/4 v3, 0x0

    const v4, 0x4dfae40a

    invoke-static {v9, p0, v8, v3, v4}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2272486
    iget-object v9, v7, LX/Fhd;->i:Ljava/util/List;

    new-instance p0, LX/Fhf;

    invoke-direct {p0, v2, v8}, LX/Fhf;-><init>(ILjava/lang/String;)V

    invoke-interface {v9, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
