.class public Lcom/facebook/search/suggestions/SuggestionsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/FZU;
.implements LX/Ffo;
.implements LX/F5P;
.implements LX/7HP;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/base/fragment/FbFragment;",
        "LX/0fh;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/FZU;",
        "LX/Ffo;",
        "LX/F5P;",
        "LX/7HP",
        "<",
        "Lcom/facebook/search/model/TypeaheadUnit;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:Ljava/lang/String;

.field public static final c:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

.field public static final d:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;


# instance fields
.field private A:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2d0;",
            ">;"
        }
    .end annotation
.end field

.field public B:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2cw;",
            ">;"
        }
    .end annotation
.end field

.field private C:LX/0Or;
    .annotation runtime Lcom/facebook/search/abtest/gk/IsTypeaheadBackstackEnabled;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public D:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8bf;",
            ">;"
        }
    .end annotation
.end field

.field private E:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field public F:LX/EQ9;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private G:LX/Fgq;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private H:LX/Fgg;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private I:LX/Fgv;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public J:LX/0x9;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public K:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FaY;",
            ">;"
        }
    .end annotation
.end field

.field public L:LX/2Sd;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public M:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public N:LX/FiS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private O:LX/0zG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public P:LX/8i2;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public Q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/viewport/ViewportMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private R:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FhC;",
            ">;"
        }
    .end annotation
.end field

.field public S:LX/FgW;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private T:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2SS;",
            ">;"
        }
    .end annotation
.end field

.field public U:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field public W:LX/FZb;

.field public X:Lcom/facebook/search/api/GraphSearchQuery;

.field public Y:Landroid/view/ContextThemeWrapper;

.field public Z:LX/Fgb;

.field public aa:LX/Cwd;

.field private ab:LX/0gc;

.field public ac:LX/FgV;

.field public ad:Z

.field private ae:Z

.field public af:Z

.field public ag:Z

.field public ah:Z

.field public ai:Z

.field public aj:I

.field public ak:Z

.field public final e:LX/Fh8;

.field private final f:LX/FhB;

.field public final g:LX/F5P;

.field public h:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/FgS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/Fhc;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:LX/3Qe;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private l:LX/8iG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cvp;",
            ">;"
        }
    .end annotation
.end field

.field private n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EQ6;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private q:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/Cvy;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field public v:LX/2Sg;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public w:LX/2Sc;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private x:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private y:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public z:LX/8ht;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2272251
    const-class v0, Lcom/facebook/search/suggestions/SuggestionsFragment;

    const-string v1, "search"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2272252
    const-class v0, Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->b:Ljava/lang/String;

    .line 2272253
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->LEARNING_NUX_SECOND_STEP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    sput-object v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->c:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    .line 2272254
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->LEARNING_TYPEAHEAD_TOOLTIP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    sput-object v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->d:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2272129
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2272130
    new-instance v0, LX/Fh8;

    invoke-direct {v0, p0}, LX/Fh8;-><init>(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->e:LX/Fh8;

    .line 2272131
    new-instance v0, LX/FhB;

    invoke-direct {v0, p0}, LX/FhB;-><init>(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->f:LX/FhB;

    .line 2272132
    iput-object p0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->g:LX/F5P;

    .line 2272133
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2272134
    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->n:LX/0Ot;

    .line 2272135
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2272136
    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->o:LX/0Ot;

    .line 2272137
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2272138
    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->r:LX/0Ot;

    .line 2272139
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2272140
    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->t:LX/0Ot;

    .line 2272141
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2272142
    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->u:LX/0Ot;

    .line 2272143
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2272144
    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->x:LX/0Ot;

    .line 2272145
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2272146
    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->A:LX/0Ot;

    .line 2272147
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2272148
    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->B:LX/0Ot;

    .line 2272149
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2272150
    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->D:LX/0Ot;

    .line 2272151
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2272152
    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->E:LX/0Ot;

    .line 2272153
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2272154
    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->K:LX/0Ot;

    .line 2272155
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2272156
    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Q:LX/0Ot;

    .line 2272157
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2272158
    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->R:LX/0Ot;

    .line 2272159
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2272160
    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->T:LX/0Ot;

    .line 2272161
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->V:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2272162
    sget-object v0, Lcom/facebook/search/api/GraphSearchQuery;->e:Lcom/facebook/search/api/GraphSearchQuery;

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2272163
    sget-object v0, LX/Cwd;->GLOBAL:LX/Cwd;

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->aa:LX/Cwd;

    .line 2272164
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ad:Z

    .line 2272165
    iput-boolean v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ae:Z

    .line 2272166
    iput-boolean v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->af:Z

    .line 2272167
    iput-boolean v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ag:Z

    .line 2272168
    iput-boolean v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ah:Z

    .line 2272169
    iput-boolean v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ai:Z

    .line 2272170
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->aj:I

    .line 2272171
    return-void
.end method

.method private B()Z
    .locals 3

    .prologue
    .line 2272172
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->M:LX/0Uh;

    sget v1, LX/2SU;->o:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public static D(Lcom/facebook/search/suggestions/SuggestionsFragment;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2272173
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2272174
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2272175
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2272176
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2272177
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 2272178
    if-nez v0, :cond_1

    .line 2272179
    :cond_0
    const-string v0, ""

    .line 2272180
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2272181
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2272182
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 2272183
    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private G()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2272184
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->l()LX/Fid;

    move-result-object v0

    .line 2272185
    if-eqz v0, :cond_0

    .line 2272186
    iget-object p0, v0, LX/Fid;->a:Ljava/util/List;

    move-object v0, p0

    .line 2272187
    :goto_0
    return-object v0

    .line 2272188
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2272189
    goto :goto_0
.end method

.method private H()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2272190
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->j()LX/FgU;

    move-result-object v0

    .line 2272191
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/7HQ;->c()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private I()V
    .locals 4

    .prologue
    .line 2272192
    :try_start_0
    const-string v0, "SuggestionsFragment.maybeInitializeTitleBox"

    const v1, 0x27d1f12e

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2272193
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mHidden:Z

    move v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2272194
    if-eqz v0, :cond_0

    .line 2272195
    const v0, -0x5c49c718

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2272196
    :goto_0
    return-void

    .line 2272197
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->H()Ljava/lang/String;

    move-result-object v0

    .line 2272198
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->i()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v1

    .line 2272199
    iget-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->z:LX/8ht;

    invoke-virtual {v2, v1}, LX/8ht;->e(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2272200
    iget-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2272201
    iget-object v3, v2, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v2, v3

    .line 2272202
    invoke-virtual {v2, v1}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a(Lcom/facebook/search/api/GraphSearchQuery;)V

    .line 2272203
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2272204
    iget-object v2, v1, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v1, v2

    .line 2272205
    if-eqz v1, :cond_2

    .line 2272206
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2272207
    iget-object v2, v1, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v1, v2

    .line 2272208
    iget-object v2, v1, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v1, v2

    .line 2272209
    invoke-virtual {v1, v0}, Lcom/facebook/ui/search/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2272210
    iget-boolean v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ae:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2272211
    iget-object v3, v2, LX/7B6;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2272212
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2272213
    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->selectAll()V

    .line 2272214
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/ui/search/SearchEditText;->setSelectAllOnFocus(Z)V

    .line 2272215
    :cond_2
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2272216
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ad:Z

    .line 2272217
    :cond_3
    iget-boolean v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ad:Z

    if-eqz v0, :cond_4

    .line 2272218
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2272219
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2272220
    if-nez v0, :cond_6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2272221
    :cond_4
    :goto_2
    const v0, -0x542f5192

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 2272222
    :cond_5
    :try_start_2
    iget-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->z:LX/8ht;

    invoke-virtual {v2, v1}, LX/8ht;->h(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2272223
    iget-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2272224
    iget-object v3, v2, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v2, v3

    .line 2272225
    invoke-virtual {v2, v1}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->b(Lcom/facebook/search/api/GraphSearchQuery;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 2272226
    :catchall_0
    move-exception v0

    const v1, 0x7e7e44e7

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2272227
    :cond_6
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2272228
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2272229
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 2272230
    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->b()Z

    goto :goto_2
.end method

.method public static J(Lcom/facebook/search/suggestions/SuggestionsFragment;)Z
    .locals 1

    .prologue
    .line 2272231
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->g()LX/0g8;

    move-result-object v0

    .line 2272232
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/0g8;->t()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static K(Lcom/facebook/search/suggestions/SuggestionsFragment;)V
    .locals 1

    .prologue
    .line 2272233
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2272234
    iget-object p0, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, p0

    .line 2272235
    if-eqz v0, :cond_0

    .line 2272236
    iget-object p0, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, p0

    .line 2272237
    invoke-static {v0}, Lcom/facebook/ui/search/SearchEditText;->i(Lcom/facebook/ui/search/SearchEditText;)V

    .line 2272238
    :cond_0
    return-void
.end method

.method public static L$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;)Z
    .locals 2

    .prologue
    .line 2272239
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->z:LX/8ht;

    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, v1}, LX/8ht;->e(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->J:LX/0x9;

    sget-object v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->c:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-virtual {v0, v1}, LX/0x9;->a(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static N(Lcom/facebook/search/suggestions/SuggestionsFragment;)V
    .locals 4

    .prologue
    .line 2272240
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->U:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2272241
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->U:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2272242
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/Fgb;->a(I)V

    .line 2272243
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->J:LX/0x9;

    const/4 v1, 0x1

    .line 2272244
    iget-object v2, v0, LX/0x9;->i:Ljava/util/Map;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->LEARNING_NUX_SECOND_STEP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FaS;

    .line 2272245
    if-eqz v2, :cond_0

    .line 2272246
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->LEARNING_NUX_SECOND_STEP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-virtual {v0, v3, v1}, LX/0x9;->a(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;Z)V

    .line 2272247
    iget-boolean v3, v2, LX/FaS;->f:Z

    if-eqz v3, :cond_0

    .line 2272248
    iget-object v3, v2, LX/FaS;->b:LX/13B;

    iget-object p0, v2, LX/FaS;->a:LX/CwT;

    const-string v0, "dismissed_by_user"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    invoke-virtual {v3, p0, v0}, LX/13B;->b(LX/CwT;LX/0P1;)V

    .line 2272249
    const/4 v3, 0x0

    iput-boolean v3, v2, LX/FaS;->f:Z

    .line 2272250
    :cond_0
    return-void
.end method

.method public static O(Lcom/facebook/search/suggestions/SuggestionsFragment;)Landroid/view/View;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2272255
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->z:LX/8ht;

    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, v1}, LX/8ht;->e(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2272256
    :cond_0
    :goto_0
    return-object v2

    .line 2272257
    :cond_1
    invoke-direct {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->G()Ljava/util/List;

    move-result-object v3

    .line 2272258
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v1, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/TypeaheadUnit;

    .line 2272259
    invoke-virtual {v0}, Lcom/facebook/search/model/TypeaheadUnit;->m()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2272260
    instance-of v5, v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v5, :cond_2

    instance-of v5, v0, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v5, :cond_3

    :cond_2
    :goto_2
    move-object v1, v0

    .line 2272261
    goto :goto_1

    .line 2272262
    :cond_3
    const/4 v0, 0x0

    .line 2272263
    invoke-interface {v3, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 2272264
    :cond_4
    :goto_3
    move v0, v0

    .line 2272265
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->g()LX/0g8;

    move-result-object v0

    invoke-interface {v3, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-interface {v0, v1}, LX/0g8;->c(I)Landroid/view/View;

    move-result-object v2

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_2

    .line 2272266
    :cond_6
    iget-object v4, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v4}, LX/Fgb;->g()LX/0g8;

    move-result-object v4

    .line 2272267
    invoke-interface {v3, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v5

    .line 2272268
    invoke-interface {v4}, LX/0g8;->q()I

    move-result v6

    invoke-interface {v4}, LX/0g8;->p()I

    move-result v7

    add-int/2addr v6, v7

    .line 2272269
    invoke-interface {v4}, LX/0g8;->q()I

    move-result v4

    if-lt v5, v4, :cond_4

    if-ge v5, v6, :cond_4

    const/4 v0, 0x1

    goto :goto_3
.end method

.method public static P(Lcom/facebook/search/suggestions/SuggestionsFragment;)V
    .locals 3

    .prologue
    .line 2272270
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->z:LX/8ht;

    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, v1}, LX/8ht;->e(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ah:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-static {v0}, LX/8ht;->c(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-static {v0}, LX/8ht;->b(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2272271
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2272272
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2272273
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    iget-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->aa:LX/Cwd;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a(Lcom/facebook/search/api/GraphSearchQuery;LX/Cwd;)V

    .line 2272274
    :cond_0
    return-void
.end method

.method public static a(LX/0g7;)LX/1P0;
    .locals 3
    .param p0    # LX/0g7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2272276
    if-nez p0, :cond_0

    move-object v0, v1

    .line 2272277
    :goto_0
    return-object v0

    .line 2272278
    :cond_0
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2272279
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    .line 2272280
    if-eqz v0, :cond_1

    instance-of v2, v0, LX/1P0;

    if-nez v2, :cond_2

    :cond_1
    move-object v0, v1

    .line 2272281
    goto :goto_0

    .line 2272282
    :cond_2
    check-cast v0, LX/1P0;

    goto :goto_0
.end method

.method private static a(Lcom/facebook/search/suggestions/SuggestionsFragment;LX/0SG;LX/FgS;LX/Fhc;LX/3Qe;LX/8iG;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/lang/String;Landroid/content/Context;LX/0Ot;LX/Cvy;LX/0Ot;LX/0Ot;LX/2Sg;LX/2Sc;LX/0Ot;LX/0ad;LX/8ht;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/EQ9;LX/Fgq;LX/Fgg;LX/Fgv;LX/0x9;LX/0Ot;LX/2Sd;LX/0Uh;LX/FiS;LX/0zG;LX/8i2;LX/0Ot;LX/0Ot;LX/FgW;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/suggestions/SuggestionsFragment;",
            "LX/0SG;",
            "LX/FgS;",
            "LX/Fhc;",
            "LX/3Qe;",
            "LX/8iG;",
            "LX/0Ot",
            "<",
            "LX/Cvp;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EQ6;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;",
            "LX/Cvy;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/2Sg;",
            "LX/2Sc;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0ad;",
            "LX/8ht;",
            "LX/0Ot",
            "<",
            "LX/2d0;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2cw;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8bf;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/EQ9;",
            "LX/Fgq;",
            "LX/Fgg;",
            "LX/Fgv;",
            "LX/0x9;",
            "LX/0Ot",
            "<",
            "LX/FaY;",
            ">;",
            "LX/2Sd;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/FiS;",
            "LX/0zG;",
            "LX/8i2;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/viewport/ViewportMonitor;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FhC;",
            ">;",
            "LX/FgW;",
            "LX/0Ot",
            "<",
            "LX/2SS;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2272275
    iput-object p1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->h:LX/0SG;

    iput-object p2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    iput-object p3, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->j:LX/Fhc;

    iput-object p4, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->k:LX/3Qe;

    iput-object p5, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->l:LX/8iG;

    iput-object p6, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->m:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->n:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->o:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->p:Ljava/lang/String;

    iput-object p10, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->q:Landroid/content/Context;

    iput-object p11, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->r:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->s:LX/Cvy;

    iput-object p13, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->t:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->u:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->v:LX/2Sg;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->w:LX/2Sc;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->x:LX/0Ot;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->y:LX/0ad;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->z:LX/8ht;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->A:LX/0Ot;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->B:LX/0Ot;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->C:LX/0Or;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->D:LX/0Ot;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->E:LX/0Ot;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->F:LX/EQ9;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->G:LX/Fgq;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->H:LX/Fgg;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->I:LX/Fgv;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->J:LX/0x9;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->K:LX/0Ot;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->L:LX/2Sd;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->M:LX/0Uh;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->N:LX/FiS;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->O:LX/0zG;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->P:LX/8i2;

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Q:LX/0Ot;

    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->R:LX/0Ot;

    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->S:LX/FgW;

    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->T:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 43

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v41

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static/range {v41 .. v41}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static/range {v41 .. v41}, LX/FgS;->a(LX/0QB;)LX/FgS;

    move-result-object v4

    check-cast v4, LX/FgS;

    invoke-static/range {v41 .. v41}, LX/Fhc;->a(LX/0QB;)LX/Fhc;

    move-result-object v5

    check-cast v5, LX/Fhc;

    invoke-static/range {v41 .. v41}, LX/3Qe;->a(LX/0QB;)LX/3Qe;

    move-result-object v6

    check-cast v6, LX/3Qe;

    invoke-static/range {v41 .. v41}, LX/8iG;->a(LX/0QB;)LX/8iG;

    move-result-object v7

    check-cast v7, LX/8iG;

    const/16 v8, 0x32d8

    move-object/from16 v0, v41

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x34d5

    move-object/from16 v0, v41

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2eb

    move-object/from16 v0, v41

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static/range {v41 .. v41}, LX/0dG;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    const-class v12, Landroid/content/Context;

    move-object/from16 v0, v41

    invoke-interface {v0, v12}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/content/Context;

    const/16 v13, 0xbca

    move-object/from16 v0, v41

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {v41 .. v41}, LX/Cvy;->a(LX/0QB;)LX/Cvy;

    move-result-object v14

    check-cast v14, LX/Cvy;

    const/16 v15, 0x271

    move-object/from16 v0, v41

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x97

    move-object/from16 v0, v41

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    invoke-static/range {v41 .. v41}, LX/2Sg;->a(LX/0QB;)LX/2Sg;

    move-result-object v17

    check-cast v17, LX/2Sg;

    invoke-static/range {v41 .. v41}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v18

    check-cast v18, LX/2Sc;

    const/16 v19, 0x455

    move-object/from16 v0, v41

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    invoke-static/range {v41 .. v41}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v20

    check-cast v20, LX/0ad;

    invoke-static/range {v41 .. v41}, LX/8ht;->a(LX/0QB;)LX/8ht;

    move-result-object v21

    check-cast v21, LX/8ht;

    const/16 v22, 0x10bd

    move-object/from16 v0, v41

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v22

    const/16 v23, 0xf57

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const/16 v24, 0x156b

    move-object/from16 v0, v41

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v24

    const/16 v25, 0x32b2

    move-object/from16 v0, v41

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v25

    const/16 v26, 0x19c6

    move-object/from16 v0, v41

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v26

    invoke-static/range {v41 .. v41}, LX/EQ9;->a(LX/0QB;)LX/EQ9;

    move-result-object v27

    check-cast v27, LX/EQ9;

    const-class v28, LX/Fgq;

    move-object/from16 v0, v41

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v28

    check-cast v28, LX/Fgq;

    const-class v29, LX/Fgg;

    move-object/from16 v0, v41

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v29

    check-cast v29, LX/Fgg;

    const-class v30, LX/Fgv;

    move-object/from16 v0, v41

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v30

    check-cast v30, LX/Fgv;

    invoke-static/range {v41 .. v41}, LX/0x9;->a(LX/0QB;)LX/0x9;

    move-result-object v31

    check-cast v31, LX/0x9;

    const/16 v32, 0x32f6

    move-object/from16 v0, v41

    move/from16 v1, v32

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v32

    invoke-static/range {v41 .. v41}, LX/2Sd;->a(LX/0QB;)LX/2Sd;

    move-result-object v33

    check-cast v33, LX/2Sd;

    invoke-static/range {v41 .. v41}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v34

    check-cast v34, LX/0Uh;

    invoke-static/range {v41 .. v41}, LX/FiS;->a(LX/0QB;)LX/FiS;

    move-result-object v35

    check-cast v35, LX/FiS;

    invoke-static/range {v41 .. v41}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v36

    check-cast v36, LX/0zG;

    invoke-static/range {v41 .. v41}, LX/8i2;->a(LX/0QB;)LX/8i2;

    move-result-object v37

    check-cast v37, LX/8i2;

    const/16 v38, 0x38d

    move-object/from16 v0, v41

    move/from16 v1, v38

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v38

    const/16 v39, 0x34b7

    move-object/from16 v0, v41

    move/from16 v1, v39

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v39

    const-class v40, LX/FgW;

    move-object/from16 v0, v41

    move-object/from16 v1, v40

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v40

    check-cast v40, LX/FgW;

    const/16 v42, 0x11b3

    invoke-static/range {v41 .. v42}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v41

    invoke-static/range {v2 .. v41}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a(Lcom/facebook/search/suggestions/SuggestionsFragment;LX/0SG;LX/FgS;LX/Fhc;LX/3Qe;LX/8iG;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/lang/String;Landroid/content/Context;LX/0Ot;LX/Cvy;LX/0Ot;LX/0Ot;LX/2Sg;LX/2Sc;LX/0Ot;LX/0ad;LX/8ht;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/EQ9;LX/Fgq;LX/Fgg;LX/Fgv;LX/0x9;LX/0Ot;LX/2Sd;LX/0Uh;LX/FiS;LX/0zG;LX/8i2;LX/0Ot;LX/0Ot;LX/FgW;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;LX/7HZ;)V
    .locals 9

    .prologue
    .line 2272396
    sget-object v0, LX/7HZ;->ERROR:LX/7HZ;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2272397
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->l:LX/8iG;

    new-instance v1, LX/27k;

    const v2, 0x7f0822eb

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    .line 2272398
    iget-object v3, v0, LX/8iG;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v3

    .line 2272399
    iget-wide v5, v0, LX/8iG;->d:J

    sub-long v5, v3, v5

    iget-wide v7, v0, LX/8iG;->c:J

    cmp-long v5, v5, v7

    if-lez v5, :cond_0

    .line 2272400
    iget-object v5, v0, LX/8iG;->a:LX/0kL;

    invoke-virtual {v5, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2272401
    iput-wide v3, v0, LX/8iG;->d:J

    .line 2272402
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Landroid/widget/ProgressBar;LX/0g8;LX/7HZ;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2272405
    sget-object v0, LX/Fgw;->a:[I

    invoke-virtual {p3}, LX/7HZ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2272406
    :cond_0
    :goto_0
    return-void

    .line 2272407
    :pswitch_0
    invoke-virtual {p1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 2272408
    :pswitch_1
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2272409
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->V:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->V:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/FbSwipeRefreshLayout;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2272410
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->V:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    goto :goto_0

    .line 2272411
    :pswitch_2
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2272412
    invoke-virtual {p1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2272413
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->V:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->V:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/FbSwipeRefreshLayout;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2272414
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->V:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2272415
    :cond_1
    invoke-interface {p2, v1}, LX/0g8;->a(I)V

    .line 2272416
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->U:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 2272417
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->U:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2272418
    :cond_2
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Lcom/facebook/search/model/TypeaheadUnit;)V
    .locals 3

    .prologue
    .line 2272403
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EQ6;

    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->i()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->H()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, LX/EQ6;->a(Lcom/facebook/search/api/GraphSearchQuery;Lcom/facebook/search/model/TypeaheadUnit;Ljava/lang/String;)V

    .line 2272404
    return-void
.end method

.method public static a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Ljava/lang/String;Lcom/facebook/search/model/TypeaheadUnit;J)V
    .locals 15

    .prologue
    .line 2272387
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->s:LX/Cvy;

    invoke-virtual {v0}, LX/Cvy;->f()V

    .line 2272388
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Cvp;

    invoke-direct {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->H()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->w(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/0Px;

    move-result-object v5

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->J(Lcom/facebook/search/suggestions/SuggestionsFragment;)Z

    move-result v6

    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->e()LX/7HZ;

    move-result-object v7

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->p(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/CwU;

    move-result-object v8

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->q(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/Cwd;

    move-result-object v9

    iget-object v10, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->r(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/7B5;

    move-result-object v11

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v12, p3

    invoke-virtual/range {v1 .. v13}, LX/Cvp;->a(Ljava/lang/String;Lcom/facebook/search/model/TypeaheadUnit;Ljava/lang/String;LX/0Px;ZLX/7HZ;LX/CwU;LX/Cwd;Lcom/facebook/search/api/GraphSearchQuery;LX/7B5;J)V

    .line 2272389
    return-void
.end method

.method public static b(Ljava/util/List;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2272390
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2272391
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/TypeaheadUnit;

    .line 2272392
    instance-of v3, v0, Lcom/facebook/search/model/TypeaheadCollectionUnit;

    if-eqz v3, :cond_0

    .line 2272393
    check-cast v0, Lcom/facebook/search/model/TypeaheadCollectionUnit;

    invoke-virtual {v0}, Lcom/facebook/search/model/TypeaheadCollectionUnit;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    goto :goto_0

    .line 2272394
    :cond_0
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2272395
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static b$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2272350
    iget-boolean v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ae:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2272351
    iget-object v1, v0, LX/7B6;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2272352
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2272353
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2272354
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2272355
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 2272356
    invoke-virtual {v0, v2}, Lcom/facebook/ui/search/SearchEditText;->setSelectAllOnFocus(Z)V

    .line 2272357
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->m()V

    .line 2272358
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->i()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/search/api/GraphSearchQuery;->a(Lcom/facebook/search/api/GraphSearchQuery;Ljava/lang/String;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    .line 2272359
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->j:LX/Fhc;

    .line 2272360
    iget-object v3, v1, LX/Fhc;->c:LX/Fhi;

    invoke-virtual {v3, v0}, LX/Fhd;->a(LX/7B6;)V

    .line 2272361
    iget-object v3, v1, LX/Fhc;->d:LX/Fhj;

    invoke-virtual {v3, v0}, LX/Fhd;->a(LX/7B6;)V

    .line 2272362
    iget-object v3, v1, LX/Fhc;->b:LX/Fhk;

    invoke-virtual {v3, v0}, LX/Fhd;->a(LX/7B6;)V

    .line 2272363
    iget-object v3, v1, LX/Fhc;->a:LX/Fhe;

    invoke-virtual {v3, v0}, LX/Fhd;->a(LX/7B6;)V

    .line 2272364
    iget-object v3, v1, LX/Fhc;->e:LX/Fhh;

    invoke-virtual {v3, v0}, LX/Fhd;->a(LX/7B6;)V

    .line 2272365
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->j()LX/FgU;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->j()LX/FgU;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/7HQ;->b(LX/7B6;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2272366
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->j:LX/Fhc;

    .line 2272367
    iget-object v3, v1, LX/Fhc;->c:LX/Fhi;

    invoke-virtual {v3, v0}, LX/Fhd;->b(LX/7B6;)V

    .line 2272368
    iget-object v3, v1, LX/Fhc;->d:LX/Fhj;

    invoke-virtual {v3, v0}, LX/Fhd;->b(LX/7B6;)V

    .line 2272369
    iget-object v3, v1, LX/Fhc;->b:LX/Fhk;

    invoke-virtual {v3, v0}, LX/Fhd;->b(LX/7B6;)V

    .line 2272370
    iget-object v3, v1, LX/Fhc;->a:LX/Fhe;

    invoke-virtual {v3, v0}, LX/Fhd;->b(LX/7B6;)V

    .line 2272371
    invoke-static {p0, p1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->c$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Ljava/lang/String;)V

    .line 2272372
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->k()LX/Fi7;

    move-result-object v0

    .line 2272373
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    .line 2272374
    invoke-interface {v0}, LX/Fi7;->a()V

    .line 2272375
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->V:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v0, :cond_2

    .line 2272376
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->V:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbSwipeRefreshLayout;->setEnabled(Z)V

    .line 2272377
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cvp;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 2272378
    new-instance v2, Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iget-object v3, v0, LX/Cvp;->l:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iget-object v3, v3, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, v3, p1}, Lcom/facebook/search/logging/api/SearchTypeaheadSession;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, v0, LX/Cvp;->l:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    .line 2272379
    iget v2, v0, LX/Cvp;->n:I

    iget v3, v0, LX/Cvp;->o:I

    sub-int/2addr v3, v1

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v0, LX/Cvp;->n:I

    .line 2272380
    iput v1, v0, LX/Cvp;->o:I

    .line 2272381
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->y(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/0g7;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a(LX/0g7;)LX/1P0;

    move-result-object v0

    .line 2272382
    if-eqz v0, :cond_3

    .line 2272383
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->aj:I

    .line 2272384
    :cond_3
    return-void

    .line 2272385
    :cond_4
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->V:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v0, :cond_2

    .line 2272386
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->V:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/FbSwipeRefreshLayout;->setEnabled(Z)V

    goto :goto_0
.end method

.method private c(Landroid/view/View;)V
    .locals 10

    .prologue
    .line 2272316
    const v0, 0x7f0d2b5d

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->U:Landroid/view/View;

    .line 2272317
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->J:LX/0x9;

    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->U:Landroid/view/View;

    .line 2272318
    iget-object v2, v0, LX/0x9;->i:Ljava/util/Map;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->LEARNING_NUX_SECOND_STEP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FaS;

    .line 2272319
    if-eqz v2, :cond_0

    .line 2272320
    const/16 v9, 0x8

    const/4 p1, 0x0

    .line 2272321
    iget-object v3, v2, LX/FaS;->a:LX/CwT;

    .line 2272322
    iget-object v4, v3, LX/CwT;->a:LX/A0Z;

    move-object v0, v4

    .line 2272323
    const v3, 0x7f0d2b58

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/widget/text/BetterTextView;

    .line 2272324
    const v4, 0x7f0d2b59

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/widget/text/BetterTextView;

    .line 2272325
    const v5, 0x7f0d2b5a

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/widget/text/BetterTextView;

    .line 2272326
    const v6, 0x7f0d2b5b

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/facebook/widget/text/BetterTextView;

    .line 2272327
    const v7, 0x7f0d2b5c

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/facebook/widget/text/BetterTextView;

    .line 2272328
    invoke-interface {v0}, LX/A0Z;->c()LX/0Px;

    move-result-object v8

    invoke-static {v8, p1}, LX/FaS;->a(LX/0Px;I)Ljava/lang/String;

    move-result-object v8

    .line 2272329
    invoke-virtual {v5, v8}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2272330
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    move v8, v9

    :goto_0
    invoke-virtual {v5, v8}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2272331
    invoke-interface {v0}, LX/A0Z;->c()LX/0Px;

    move-result-object v5

    const/4 v8, 0x1

    invoke-static {v5, v8}, LX/FaS;->a(LX/0Px;I)Ljava/lang/String;

    move-result-object v5

    .line 2272332
    invoke-virtual {v6, v5}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2272333
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v5, v9

    :goto_1
    invoke-virtual {v6, v5}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2272334
    invoke-interface {v0}, LX/A0Z;->c()LX/0Px;

    move-result-object v5

    const/4 v6, 0x2

    invoke-static {v5, v6}, LX/FaS;->a(LX/0Px;I)Ljava/lang/String;

    move-result-object v5

    .line 2272335
    invoke-virtual {v7, v5}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2272336
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    move v5, v9

    :goto_2
    invoke-virtual {v7, v5}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2272337
    invoke-interface {v0}, LX/A0Z;->fQ_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2272338
    invoke-interface {v0}, LX/A0Z;->fQ_()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    move v5, v9

    :goto_3
    invoke-virtual {v3, v5}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2272339
    invoke-interface {v0}, LX/A0Z;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2272340
    invoke-interface {v0}, LX/A0Z;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    :goto_4
    invoke-virtual {v4, v9}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2272341
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->U:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 2272342
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->U:Landroid/view/View;

    const v1, 0x7f0d0632

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    .line 2272343
    new-instance v1, LX/Fh2;

    invoke-direct {v1, p0, v0}, LX/Fh2;-><init>(Lcom/facebook/search/suggestions/SuggestionsFragment;Lcom/facebook/fbui/glyph/GlyphView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2272344
    :cond_1
    return-void

    :cond_2
    move v8, p1

    .line 2272345
    goto :goto_0

    :cond_3
    move v5, p1

    .line 2272346
    goto :goto_1

    :cond_4
    move v5, p1

    .line 2272347
    goto :goto_2

    :cond_5
    move v5, p1

    .line 2272348
    goto :goto_3

    :cond_6
    move v9, p1

    .line 2272349
    goto :goto_4
.end method

.method public static c(Lcom/facebook/search/suggestions/SuggestionsFragment;Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 1

    .prologue
    .line 2272313
    iput-object p1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2272314
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0, p1}, LX/Fgb;->a(Lcom/facebook/search/api/GraphSearchQuery;)V

    .line 2272315
    return-void
.end method

.method public static c$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2272304
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->k()LX/Fi7;

    move-result-object v0

    .line 2272305
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->l()LX/Fid;

    move-result-object v1

    .line 2272306
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v2}, LX/Fgb;->i()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v2

    invoke-interface {v0, v2, v1}, LX/Fi7;->a(Lcom/facebook/search/api/GraphSearchQuery;LX/Fid;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2272307
    const/4 v2, 0x3

    invoke-static {v2}, LX/01m;->b(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2272308
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2272309
    iget-object p1, v1, LX/Fid;->a:Ljava/util/List;

    move-object v1, p1

    .line 2272310
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 2272311
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->d()LX/1Qq;

    move-result-object v0

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2272312
    :cond_1
    return-void
.end method

.method public static d(Lcom/facebook/search/suggestions/SuggestionsFragment;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2272283
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->h()LX/2SP;

    move-result-object v0

    .line 2272284
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->k()LX/Fi7;

    move-result-object v2

    .line 2272285
    if-eqz v0, :cond_2

    invoke-virtual {v0}, LX/2SP;->kE_()LX/CwU;

    move-result-object v1

    .line 2272286
    :goto_0
    if-eqz v2, :cond_3

    invoke-interface {v2}, LX/Fi7;->b()LX/CwU;

    move-result-object v0

    .line 2272287
    :goto_1
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2272288
    :goto_2
    sget-object v0, LX/CwU;->SINGLE_STATE:LX/CwU;

    if-eq v1, v0, :cond_0

    sget-object v0, LX/CwU;->NULL_STATE:LX/CwU;

    if-ne v1, v0, :cond_1

    .line 2272289
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cvp;

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->w(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/0Px;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->aa:LX/Cwd;

    const/4 v5, 0x1

    const/4 p0, 0x0

    .line 2272290
    sget-object v4, LX/CwU;->SINGLE_STATE:LX/CwU;

    if-ne v1, v4, :cond_5

    iget-object v4, v0, LX/Cvp;->t:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_5

    sget-object v4, LX/Cwd;->SCOPED:LX/Cwd;

    if-eq v3, v4, :cond_5

    move v4, v5

    .line 2272291
    :goto_3
    sget-object p1, LX/CwU;->SINGLE_STATE:LX/CwU;

    if-ne v1, p1, :cond_6

    iget-object p1, v0, LX/Cvp;->u:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_6

    sget-object p1, LX/Cwd;->SCOPED:LX/Cwd;

    if-ne v3, p1, :cond_6

    .line 2272292
    :goto_4
    sget-object p0, LX/CwU;->NULL_STATE:LX/CwU;

    invoke-virtual {p0, v1}, LX/CwU;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    iget-object p0, v0, LX/Cvp;->s:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_7

    .line 2272293
    iget-object v4, v0, LX/Cvp;->s:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2272294
    :cond_1
    :goto_5
    return-void

    .line 2272295
    :cond_2
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->p(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/CwU;

    move-result-object v1

    goto :goto_0

    .line 2272296
    :cond_3
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->p(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/CwU;

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object v1, v0

    .line 2272297
    goto :goto_2

    :cond_5
    move v4, p0

    .line 2272298
    goto :goto_3

    :cond_6
    move v5, p0

    .line 2272299
    goto :goto_4

    .line 2272300
    :cond_7
    if-eqz v4, :cond_8

    .line 2272301
    iget-object v4, v0, LX/Cvp;->t:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5

    .line 2272302
    :cond_8
    if-eqz v5, :cond_1

    .line 2272303
    iget-object v4, v0, LX/Cvp;->u:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5
.end method

.method public static p(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/CwU;
    .locals 1

    .prologue
    .line 2272121
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->D(Lcom/facebook/search/suggestions/SuggestionsFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2272122
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->h()LX/2SP;

    move-result-object v0

    .line 2272123
    if-nez v0, :cond_0

    sget-object v0, LX/CwU;->NULL_STATE:LX/CwU;

    .line 2272124
    :goto_0
    return-object v0

    .line 2272125
    :cond_0
    invoke-virtual {v0}, LX/2SP;->kE_()LX/CwU;

    move-result-object v0

    goto :goto_0

    .line 2272126
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->k()LX/Fi7;

    move-result-object v0

    .line 2272127
    if-nez v0, :cond_2

    sget-object v0, LX/CwU;->TYPED:LX/CwU;

    goto :goto_0

    :cond_2
    invoke-interface {v0}, LX/Fi7;->b()LX/CwU;

    move-result-object v0

    goto :goto_0
.end method

.method public static q(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/Cwd;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2272128
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->aa:LX/Cwd;

    return-object v0
.end method

.method public static r(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/7B5;
    .locals 2

    .prologue
    .line 2271810
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->z:LX/8ht;

    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, v1}, LX/8ht;->e(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2271811
    iget-boolean v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ah:Z

    if-eqz v0, :cond_0

    .line 2271812
    sget-object v0, LX/7B5;->PILL:LX/7B5;

    .line 2271813
    :goto_0
    return-object v0

    .line 2271814
    :cond_0
    sget-object v0, LX/7B5;->TAB:LX/7B5;

    goto :goto_0

    .line 2271815
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s(Lcom/facebook/search/suggestions/SuggestionsFragment;)V
    .locals 3

    .prologue
    .line 2271916
    invoke-direct {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->B()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2271917
    :cond_0
    :goto_0
    return-void

    .line 2271918
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->g()LX/0g8;

    move-result-object v1

    .line 2271919
    if-eqz v1, :cond_0

    .line 2271920
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->u(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    .line 2271921
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fx;

    invoke-interface {v1, v0}, LX/0g8;->b(LX/0fx;)V

    .line 2271922
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kt;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/1Kt;->a(ZLX/0g8;)V

    goto :goto_0
.end method

.method public static t(Lcom/facebook/search/suggestions/SuggestionsFragment;)V
    .locals 3

    .prologue
    .line 2271910
    invoke-direct {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->B()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2271911
    :cond_0
    :goto_0
    return-void

    .line 2271912
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->g()LX/0g8;

    move-result-object v1

    .line 2271913
    if-eqz v1, :cond_0

    .line 2271914
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fx;

    invoke-interface {v1, v0}, LX/0g8;->c(LX/0fx;)V

    .line 2271915
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kt;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, LX/1Kt;->a(ZLX/0g8;)V

    goto :goto_0
.end method

.method public static u(Lcom/facebook/search/suggestions/SuggestionsFragment;)V
    .locals 2

    .prologue
    .line 2271903
    invoke-direct {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->B()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2271904
    :goto_0
    return-void

    .line 2271905
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kt;

    .line 2271906
    iget-object v1, v0, LX/1Kt;->g:LX/01J;

    invoke-virtual {v1}, LX/01J;->clear()V

    .line 2271907
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->R:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FhC;

    .line 2271908
    iget-object v1, v0, LX/FhC;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 2271909
    goto :goto_0
.end method

.method public static v(Lcom/facebook/search/suggestions/SuggestionsFragment;)Z
    .locals 2

    .prologue
    .line 2271900
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->z:LX/8ht;

    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, v1}, LX/8ht;->e(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2271901
    iget-object v1, v0, Lcom/facebook/search/api/GraphSearchQuery;->j:LX/7B5;

    move-object v0, v1

    .line 2271902
    sget-object v1, LX/7B5;->TAB:LX/7B5;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static w(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2271893
    invoke-direct {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->B()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2271894
    invoke-direct {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->G()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->b(Ljava/util/List;)LX/0Px;

    move-result-object v0

    .line 2271895
    :goto_0
    return-object v0

    .line 2271896
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->R:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FhC;

    .line 2271897
    iget-object v1, v0, LX/FhC;->a:Ljava/util/Set;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 2271898
    move-object v0, v1

    .line 2271899
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->u(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    goto :goto_0
.end method

.method public static y(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/0g7;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2271887
    invoke-direct {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->B()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 2271888
    :goto_0
    return-object v0

    .line 2271889
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->g()LX/0g8;

    move-result-object v0

    .line 2271890
    if-eqz v0, :cond_1

    instance-of v2, v0, LX/0g7;

    if-nez v2, :cond_2

    :cond_1
    move-object v0, v1

    .line 2271891
    goto :goto_0

    .line 2271892
    :cond_2
    check-cast v0, LX/0g7;

    goto :goto_0
.end method

.method public static z(Lcom/facebook/search/suggestions/SuggestionsFragment;)V
    .locals 1

    .prologue
    .line 2271883
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->y(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/0g7;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a(LX/0g7;)LX/1P0;

    move-result-object v0

    .line 2271884
    if-eqz v0, :cond_0

    .line 2271885
    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v0

    iput v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->aj:I

    .line 2271886
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2271882
    const-string v0, "search_typeahead"

    return-object v0
.end method

.method public final a(LX/7Hi;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2271860
    iget-object v0, p1, LX/7Hi;->a:LX/7B6;

    move-object v0, v0

    .line 2271861
    iget-object v0, v0, LX/7B6;->c:Ljava/lang/String;

    .line 2271862
    iget-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v2, p1}, LX/Fgb;->a(LX/7Hi;)LX/0Px;

    move-result-object v2

    .line 2271863
    const/4 v3, 0x3

    invoke-static {v3}, LX/01m;->b(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2271864
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->D(Lcom/facebook/search/suggestions/SuggestionsFragment;)Ljava/lang/String;

    .line 2271865
    :cond_0
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->D(Lcom/facebook/search/suggestions/SuggestionsFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2271866
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->u(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    .line 2271867
    :cond_1
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1, v0, v2}, LX/Fgb;->a(Ljava/lang/String;LX/0Px;)V

    .line 2271868
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    sget-object v2, LX/7BE;->READY:LX/7BE;

    const/4 v3, 0x0

    invoke-interface {v1, v0, v2, v3}, LX/Fgb;->a(Ljava/lang/String;LX/7BE;Z)V

    .line 2271869
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->j:LX/Fhc;

    iget-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v2, v0}, LX/Fgb;->a(Ljava/lang/String;)LX/Fid;

    move-result-object v0

    .line 2271870
    iget-object v2, v0, LX/Fid;->a:Ljava/util/List;

    move-object v0, v2

    .line 2271871
    iget-object v2, v1, LX/Fhc;->e:LX/Fhh;

    .line 2271872
    iget-object v3, v2, LX/Fhh;->f:LX/0Sh;

    invoke-virtual {v3}, LX/0Sh;->a()V

    .line 2271873
    iget-object v3, v2, LX/Fhd;->a:LX/11i;

    invoke-virtual {v2}, LX/Fhh;->a()LX/Fhg;

    move-result-object v4

    invoke-interface {v3, v4}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v3

    .line 2271874
    if-nez v3, :cond_3

    .line 2271875
    :cond_2
    :goto_0
    return-void

    .line 2271876
    :cond_3
    iget v4, v2, LX/Fhd;->d:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 2271877
    const-string p0, "end_to_end_prerendering"

    invoke-interface {v3, p0, v4}, LX/11o;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 2271878
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_4

    .line 2271879
    const-string p0, "end_to_end_prerendering"

    const/4 p1, 0x0

    const v1, -0x57565ca6

    invoke-static {v3, p0, v4, p1, v1}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2271880
    :goto_1
    iget v3, v2, LX/Fhd;->d:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, LX/Fhh;->d:I

    goto :goto_0

    .line 2271881
    :cond_4
    const-string p0, "end_to_end_prerendering"

    const p1, -0x77e9b3fa

    invoke-static {v3, p0, v4, p1}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;I)LX/11o;

    goto :goto_1
.end method

.method public final a(LX/FZb;)V
    .locals 0

    .prologue
    .line 2271858
    iput-object p1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->W:LX/FZb;

    .line 2271859
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 2271831
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2271832
    new-instance v0, LX/4nE;

    invoke-direct {v0}, LX/4nE;-><init>()V

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 2271833
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2271834
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v0

    .line 2271835
    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ab:LX/0gc;

    .line 2271836
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2271837
    if-eqz v0, :cond_0

    const-string v1, "initial_typeahead_query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2271838
    const-string v1, "initial_typeahead_query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/GraphSearchQuery;

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2271839
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2271840
    iget-object v3, v1, Lcom/facebook/search/api/GraphSearchQuery;->f:LX/7BH;

    move-object v1, v3

    .line 2271841
    invoke-static {v0, v1}, LX/FjM;->a(Landroid/content/Context;LX/7BH;)Landroid/view/ContextThemeWrapper;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Y:Landroid/view/ContextThemeWrapper;

    .line 2271842
    if-eqz p1, :cond_1

    const-string v0, "recycler_view_enabled"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2271843
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->M:LX/0Uh;

    sget v1, LX/2SU;->J:I

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ak:Z

    .line 2271844
    :goto_0
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->y:LX/0ad;

    sget-short v1, LX/100;->aa:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ah:Z

    .line 2271845
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->y:LX/0ad;

    sget-short v1, LX/100;->Z:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ai:Z

    .line 2271846
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->z:LX/8ht;

    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, v1}, LX/8ht;->e(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-static {v0}, LX/8ht;->c(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->I:LX/Fgv;

    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Y:Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    iget-object v3, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ab:LX/0gc;

    invoke-virtual {v0, v1, v2, v3}, LX/Fgv;->a(Landroid/content/Context;Lcom/facebook/search/api/GraphSearchQuery;LX/0gc;)LX/Fgu;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    .line 2271847
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0, p0}, LX/Fgb;->a(LX/7HP;)V

    .line 2271848
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Y:Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->e:LX/Fh8;

    invoke-interface {v0, v1, v2}, LX/Fgb;->a(Landroid/content/Context;LX/Fh8;)V

    .line 2271849
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kt;

    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->R:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ce;

    invoke-virtual {v0, v1}, LX/1Kt;->a(LX/1Ce;)V

    .line 2271850
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->l:LX/8iG;

    const-wide/16 v2, 0x3a98

    .line 2271851
    iput-wide v2, v0, LX/8iG;->c:J

    .line 2271852
    return-void

    .line 2271853
    :cond_2
    const-string v0, "recycler_view_enabled"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ak:Z

    goto :goto_0

    .line 2271854
    :cond_3
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->H:LX/Fgg;

    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Y:Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    iget-boolean v3, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ak:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ab:LX/0gc;

    .line 2271855
    new-instance v5, LX/Fgf;

    const-class v6, LX/Fgq;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/Fgq;

    const-class v6, LX/Fgv;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/Fgv;

    move-object v6, v1

    move-object v7, v2

    move-object v8, v3

    move-object v9, v4

    invoke-direct/range {v5 .. v11}, LX/Fgf;-><init>(Landroid/content/Context;Lcom/facebook/search/api/GraphSearchQuery;Ljava/lang/Boolean;LX/0gc;LX/Fgq;LX/Fgv;)V

    .line 2271856
    move-object v0, v5

    .line 2271857
    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->G:LX/Fgq;

    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Y:Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, v1, v2}, LX/Fgq;->a(Landroid/content/Context;Lcom/facebook/search/api/GraphSearchQuery;)LX/Fgp;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2271816
    if-eqz p1, :cond_0

    .line 2271817
    invoke-static {p0, p1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->c(Lcom/facebook/search/suggestions/SuggestionsFragment;Lcom/facebook/search/api/GraphSearchQuery;)V

    .line 2271818
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_1

    .line 2271819
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {p1}, Lcom/facebook/search/api/GraphSearchQuery;->a(Lcom/facebook/search/api/GraphSearchQuery;)LX/7BH;

    move-result-object v2

    invoke-static {v0, v2}, LX/FjL;->a(Landroid/app/Activity;LX/7BH;)V

    .line 2271820
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->z:LX/8ht;

    iget-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, v2}, LX/8ht;->i(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->z:LX/8ht;

    iget-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, v2}, LX/8ht;->g(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->z:LX/8ht;

    iget-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, v2}, LX/8ht;->h(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ae:Z

    .line 2271821
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2271822
    iget-object v2, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v2

    .line 2271823
    iget-object v2, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v2

    .line 2271824
    iget-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2271825
    iget-object p1, v2, LX/7B6;->b:Ljava/lang/String;

    move-object v2, p1

    .line 2271826
    invoke-virtual {v0, v2}, Lcom/facebook/ui/search/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2271827
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->P(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    .line 2271828
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0, v1}, LX/Fgb;->a(I)V

    .line 2271829
    return-void

    :cond_3
    move v0, v1

    .line 2271830
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2271794
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2271795
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->k()LX/Fi7;

    move-result-object v0

    .line 2271796
    if-nez v0, :cond_3

    .line 2271797
    :cond_0
    :goto_0
    move v0, v2

    .line 2271798
    if-nez v0, :cond_2

    .line 2271799
    :cond_1
    :goto_1
    return-void

    .line 2271800
    :cond_2
    invoke-static {p0, p1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->d(Lcom/facebook/search/suggestions/SuggestionsFragment;Ljava/lang/String;)V

    .line 2271801
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->p(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/CwU;

    move-result-object v0

    sget-object v1, LX/CwU;->SINGLE_STATE:LX/CwU;

    if-ne v0, v1, :cond_1

    .line 2271802
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->m()V

    goto :goto_1

    .line 2271803
    :cond_3
    invoke-interface {v0}, LX/Fi7;->b()LX/CwU;

    move-result-object v0

    sget-object v3, LX/CwU;->SINGLE_STATE:LX/CwU;

    if-ne v0, v3, :cond_5

    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2271804
    iget-object v3, v0, LX/7B6;->b:Ljava/lang/String;

    move-object v0, v3

    .line 2271805
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 2271806
    :goto_2
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    move v3, v1

    .line 2271807
    :goto_3
    if-nez v3, :cond_4

    if-eqz v0, :cond_0

    :cond_4
    move v2, v1

    goto :goto_0

    :cond_5
    move v0, v2

    .line 2271808
    goto :goto_2

    :cond_6
    move v3, v2

    .line 2271809
    goto :goto_3
.end method

.method public final b(Z)Z
    .locals 12

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2271923
    if-eqz p1, :cond_0

    move v0, v9

    .line 2271924
    :goto_0
    return v0

    .line 2271925
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2271926
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v11, v1

    .line 2271927
    iget-boolean v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->af:Z

    if-eqz v0, :cond_1

    .line 2271928
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->J:LX/0x9;

    sget-object v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->c:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-virtual {v0, v1, v10}, LX/0x9;->b(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;Z)V

    .line 2271929
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ag:Z

    if-eqz v0, :cond_2

    .line 2271930
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->J:LX/0x9;

    sget-object v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->d:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-virtual {v0, v1, v10}, LX/0x9;->b(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;Z)V

    .line 2271931
    :cond_2
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->T:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SS;

    invoke-virtual {v0}, LX/2SP;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->D(Lcom/facebook/search/suggestions/SuggestionsFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->C:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2271932
    :cond_3
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->T:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SS;

    invoke-virtual {v0}, LX/2SP;->c()V

    .line 2271933
    :cond_4
    if-nez v11, :cond_5

    move v0, v9

    .line 2271934
    goto :goto_0

    .line 2271935
    :cond_5
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->C:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->p(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/CwU;

    move-result-object v0

    sget-object v1, LX/CwU;->TYPED:LX/CwU;

    if-ne v0, v1, :cond_6

    .line 2271936
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cvp;

    invoke-direct {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->H()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->w(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/0Px;

    move-result-object v2

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->J(Lcom/facebook/search/suggestions/SuggestionsFragment;)Z

    move-result v3

    iget-object v4, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v4}, LX/Fgb;->e()LX/7HZ;

    move-result-object v4

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->p(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/CwU;

    move-result-object v5

    .line 2271937
    iget-object v6, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->aa:LX/Cwd;

    move-object v6, v6

    .line 2271938
    iget-object v7, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->r(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/7B5;

    move-result-object v8

    invoke-virtual/range {v0 .. v8}, LX/Cvp;->a(Ljava/lang/String;Ljava/util/List;ZLX/7HZ;LX/CwU;LX/Cwd;Lcom/facebook/search/api/GraphSearchQuery;LX/7B5;)V

    .line 2271939
    invoke-virtual {v11}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->h()V

    .line 2271940
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cvp;

    const-string v1, ""

    invoke-virtual {v0, v1}, LX/Cvp;->a(Ljava/lang/String;)V

    move v0, v10

    .line 2271941
    goto/16 :goto_0

    .line 2271942
    :cond_6
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cvp;

    invoke-direct {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->H()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->w(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/0Px;

    move-result-object v2

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->J(Lcom/facebook/search/suggestions/SuggestionsFragment;)Z

    move-result v3

    iget-object v4, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v4}, LX/Fgb;->e()LX/7HZ;

    move-result-object v4

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->p(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/CwU;

    move-result-object v5

    .line 2271943
    iget-object v6, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->aa:LX/Cwd;

    move-object v6, v6

    .line 2271944
    iget-object v7, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->r(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/7B5;

    move-result-object v8

    invoke-virtual/range {v0 .. v8}, LX/Cvp;->a(Ljava/lang/String;Ljava/util/List;ZLX/7HZ;LX/CwU;LX/Cwd;Lcom/facebook/search/api/GraphSearchQuery;LX/7B5;)V

    move v0, v9

    .line 2271945
    goto/16 :goto_0
.end method

.method public final c()V
    .locals 8

    .prologue
    .line 2271946
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->q()V

    .line 2271947
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mHidden:Z

    move v0, v0

    .line 2271948
    if-eqz v0, :cond_0

    .line 2271949
    :goto_0
    return-void

    .line 2271950
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cvp;

    invoke-direct {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->H()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->p(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/CwU;

    move-result-object v2

    .line 2271951
    iget-object v3, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->aa:LX/Cwd;

    move-object v3, v3

    .line 2271952
    iget-object v4, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->r(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/7B5;

    move-result-object v5

    .line 2271953
    const/4 v6, 0x0

    iput v6, v0, LX/Cvp;->o:I

    .line 2271954
    const-string v6, "clear_button"

    invoke-static {v0, v6}, LX/Cvp;->c(LX/Cvp;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "selected_input_query"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "last_state"

    invoke-virtual {v6, v7, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 2271955
    invoke-static {v6, v4}, LX/Cvp;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/search/api/GraphSearchQuery;)V

    .line 2271956
    invoke-static {v6, v5}, LX/Cvp;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7B5;)V

    .line 2271957
    iget-object v7, v4, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v7, v7

    .line 2271958
    invoke-static {v6, v3, v7}, LX/Cvp;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/Cwd;LX/103;)V

    .line 2271959
    iget-object v7, v0, LX/Cvp;->b:LX/0Zb;

    invoke-interface {v7, v6}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2271960
    const/4 v7, 0x3

    invoke-static {v7}, LX/01m;->b(I)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2271961
    invoke-virtual {v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->s()Ljava/lang/String;

    .line 2271962
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15W;

    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->q:Landroid/content/Context;

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SIMPLE_SEARCH_CLEAR_TEXT_ICON_CLICK:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    .line 2271963
    const-class v3, LX/0i1;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 2271964
    goto :goto_0
.end method

.method public final d()Landroid/support/v4/app/Fragment;
    .locals 0

    .prologue
    .line 2271965
    return-object p0
.end method

.method public final k()Lcom/facebook/search/logging/api/SearchTypeaheadSession;
    .locals 1

    .prologue
    .line 2272118
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cvp;

    .line 2272119
    iget-object p0, v0, LX/Cvp;->l:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v0, p0

    .line 2272120
    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2271966
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const/16 v0, 0x6dc

    if-ne p1, v0, :cond_0

    .line 2271967
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2271968
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    const/16 v2, 0x2a

    const v3, 0x28f9b89a

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2271969
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->v(Lcom/facebook/search/suggestions/SuggestionsFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2271970
    const v0, 0x7f0306bc

    .line 2271971
    :goto_0
    iget-object v3, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->P:LX/8i2;

    iget-object v4, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Y:Landroid/view/ContextThemeWrapper;

    invoke-virtual {v3, v4}, LX/8i2;->a(Landroid/content/Context;)V

    .line 2271972
    iget-object v3, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Y:Landroid/view/ContextThemeWrapper;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-virtual {v3, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 2271973
    const/4 p3, 0x0

    .line 2271974
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->v(Lcom/facebook/search/suggestions/SuggestionsFragment;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2271975
    :goto_1
    invoke-direct {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->I()V

    .line 2271976
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2271977
    iget-object v4, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v4

    .line 2271978
    iget-object v4, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v4

    .line 2271979
    new-instance v4, LX/Fgx;

    invoke-direct {v4, p0}, LX/Fgx;-><init>(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    invoke-virtual {v0, v4}, Lcom/facebook/ui/search/SearchEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2271980
    new-instance v4, LX/Fgy;

    invoke-direct {v4, p0}, LX/Fgy;-><init>(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    .line 2271981
    iput-object v4, v0, Lcom/facebook/ui/search/SearchEditText;->f:LX/7HK;

    .line 2271982
    new-instance v4, LX/Fgz;

    invoke-direct {v4, p0}, LX/Fgz;-><init>(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    .line 2271983
    iput-object v4, v0, Lcom/facebook/ui/search/SearchEditText;->g:LX/7HL;

    .line 2271984
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    .line 2271985
    iget-object v4, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->z:LX/8ht;

    iget-object v5, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v4, v5}, LX/8ht;->e(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 2271986
    const/4 v4, 0x0

    .line 2271987
    :goto_2
    move-object v4, v4

    .line 2271988
    new-instance v5, LX/FhA;

    invoke-direct {v5, p0}, LX/FhA;-><init>(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    iget-boolean v6, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ak:Z

    invoke-interface {v0, v4, v5, v6}, LX/Fgb;->a(Landroid/view/View;LX/FhA;Z)V

    .line 2271989
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->L$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2271990
    invoke-direct {p0, v3}, Lcom/facebook/search/suggestions/SuggestionsFragment;->c(Landroid/view/View;)V

    .line 2271991
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->J:LX/0x9;

    sget-object v4, Lcom/facebook/search/suggestions/SuggestionsFragment;->c:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-virtual {v0, v4}, LX/0x9;->b(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;)V

    .line 2271992
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->af:Z

    .line 2271993
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->z:LX/8ht;

    iget-object v4, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, v4}, LX/8ht;->e(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2271994
    :goto_3
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->c()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    :goto_4
    if-ge v1, v5, :cond_4

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2271995
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 2271996
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 2271997
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-static {v0}, LX/8ht;->c(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2271998
    const v0, 0x7f0306bb

    goto/16 :goto_0

    .line 2271999
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ak:Z

    if-eqz v0, :cond_3

    .line 2272000
    const v0, 0x7f0306b9

    goto/16 :goto_0

    .line 2272001
    :cond_3
    const v0, 0x7f0306ba

    goto/16 :goto_0

    .line 2272002
    :cond_4
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2272003
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2272004
    if-eqz v0, :cond_5

    .line 2272005
    invoke-virtual {v0, p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a(LX/Ffo;)V

    .line 2272006
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->g:LX/F5P;

    .line 2272007
    iput-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->o:LX/F5P;

    .line 2272008
    :cond_5
    iget-object v4, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->z:LX/8ht;

    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, v1}, LX/8ht;->e(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->U:Landroid/view/View;

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->L$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;)Z

    move-result v5

    invoke-interface {v4, v0, v1, v5}, LX/Fgb;->a(LX/0zw;Landroid/view/View;Z)V

    .line 2272009
    const v0, -0x73f1b7f7

    invoke-static {v0, v2}, LX/02F;->f(II)V

    return-object v3

    .line 2272010
    :cond_6
    new-instance v1, LX/0zw;

    const v0, 0x7f0d2bb1

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    move-object v0, v1

    goto :goto_5

    .line 2272011
    :cond_7
    const v0, 0x7f0d1240

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2272012
    const v4, 0x7f0d1241

    invoke-static {v3, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/widget/CustomViewPager;

    .line 2272013
    iget-object v5, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    check-cast v5, LX/Fgf;

    .line 2272014
    new-instance v6, LX/Fh4;

    invoke-direct {v6, p0, v5}, LX/Fh4;-><init>(Lcom/facebook/search/suggestions/SuggestionsFragment;LX/Fgf;)V

    .line 2272015
    iput-object v6, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 2272016
    invoke-virtual {v4, v5}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2272017
    invoke-virtual {v0, v4}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2272018
    sget-object p1, LX/Cwd;->GLOBAL:LX/Cwd;

    .line 2272019
    iget-object v6, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    sget-object p2, LX/7B4;->SCOPED_TAB:LX/7B4;

    invoke-virtual {v6, p2}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7B4;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/api/GraphSearchQueryTabModifier;

    .line 2272020
    if-eqz v6, :cond_b

    .line 2272021
    iget-boolean p1, v6, Lcom/facebook/search/api/GraphSearchQueryTabModifier;->b:Z

    move v6, p1

    .line 2272022
    if-eqz v6, :cond_a

    sget-object v6, LX/Cwd;->SCOPED:LX/Cwd;

    .line 2272023
    :goto_6
    iput-object v6, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->aa:LX/Cwd;

    .line 2272024
    iget-boolean v6, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ah:Z

    if-eqz v6, :cond_9

    iget-object v6, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-static {v6}, LX/8ht;->c(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v6

    if-nez v6, :cond_9

    iget-object v6, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-static {v6}, LX/8ht;->b(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v6

    if-nez v6, :cond_9

    .line 2272025
    const/16 v6, 0x8

    invoke-virtual {v0, v6}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setVisibility(I)V

    .line 2272026
    iget-boolean v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ai:Z

    if-eqz v0, :cond_8

    .line 2272027
    sget-object v0, LX/Cwd;->SCOPED:LX/Cwd;

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->aa:LX/Cwd;

    .line 2272028
    :cond_8
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->P(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    .line 2272029
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2272030
    iget-object v6, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v6

    .line 2272031
    new-instance v6, LX/Fh6;

    invoke-direct {v6, p0, v4}, LX/Fh6;-><init>(Lcom/facebook/search/suggestions/SuggestionsFragment;Lcom/facebook/widget/CustomViewPager;)V

    .line 2272032
    iput-object v6, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->p:LX/Fh5;

    .line 2272033
    new-instance v0, LX/Fh7;

    invoke-direct {v0, p0}, LX/Fh7;-><init>(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    invoke-virtual {v4, p3, v0}, Landroid/support/v4/view/ViewPager;->a(ZLX/3sI;)V

    .line 2272034
    iput-boolean p3, v4, Lcom/facebook/widget/CustomViewPager;->a:Z

    .line 2272035
    :cond_9
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-static {v0}, LX/Cwd;->getTabs(Lcom/facebook/search/api/GraphSearchQuery;)LX/0Px;

    move-result-object v0

    .line 2272036
    iget-object v6, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->aa:LX/Cwd;

    invoke-virtual {v0, v6}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2272037
    invoke-virtual {v5, v0}, LX/Fgf;->b(I)V

    .line 2272038
    invoke-virtual {v4, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto/16 :goto_1

    .line 2272039
    :cond_a
    sget-object v6, LX/Cwd;->GLOBAL:LX/Cwd;

    goto :goto_6

    :cond_b
    move-object v6, p1

    goto :goto_6

    :cond_c
    iget-boolean v4, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ak:Z

    if-eqz v4, :cond_d

    const v4, 0x7f0d2bb0

    invoke-static {v3, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    goto/16 :goto_2

    :cond_d
    const v4, 0x7f0d2bb2

    invoke-static {v3, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    goto/16 :goto_2

    .line 2272040
    :cond_e
    const v0, 0x7f0d0595

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->V:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2272041
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->V:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/facebook/widget/FbSwipeRefreshLayout;->setEnabled(Z)V

    .line 2272042
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->V:Lcom/facebook/widget/FbSwipeRefreshLayout;

    new-instance v4, LX/Fh3;

    invoke-direct {v4, p0}, LX/Fh3;-><init>(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    invoke-virtual {v0, v4}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    goto/16 :goto_3
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1737e595

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2272043
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->n()V

    .line 2272044
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2272045
    const/16 v1, 0x2b

    const v2, 0x3db74eea

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x542e7fb0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2272046
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2272047
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->o()V

    .line 2272048
    iput-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->U:Landroid/view/View;

    .line 2272049
    iput-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->V:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2272050
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2272051
    iget-object v4, v1, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v1, v4

    .line 2272052
    if-eqz v1, :cond_0

    .line 2272053
    invoke-virtual {v1, p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->b(LX/Ffo;)V

    .line 2272054
    iput-object v2, v1, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->o:LX/F5P;

    .line 2272055
    invoke-virtual {v1, v2}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2272056
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x6b79c27c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onHiddenChanged(Z)V
    .locals 2

    .prologue
    .line 2272057
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onHiddenChanged(Z)V

    .line 2272058
    :try_start_0
    const-string v0, "SuggestionsFragment.onHiddenChanged"

    const v1, -0x3633ab12

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2272059
    invoke-direct {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->I()V

    .line 2272060
    if-nez p1, :cond_2

    .line 2272061
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ad:Z

    .line 2272062
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->u(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    .line 2272063
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->z(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    .line 2272064
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    if-eqz v0, :cond_1

    .line 2272065
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    if-eqz p1, :cond_3

    const/16 v0, 0x8

    :goto_1
    invoke-interface {v1, v0}, LX/Fgb;->a(I)V

    .line 2272066
    if-eqz p1, :cond_4

    .line 2272067
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2272068
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2272069
    invoke-virtual {v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a()V

    .line 2272070
    :goto_2
    if-nez p1, :cond_1

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->D(Lcom/facebook/search/suggestions/SuggestionsFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->j()LX/FgU;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2272071
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->j()LX/FgU;

    move-result-object v0

    invoke-virtual {v0}, LX/FgU;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2272072
    :cond_1
    const v0, -0x6528c53c

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2272073
    return-void

    .line 2272074
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->v:LX/2Sg;

    if-eqz v0, :cond_0

    .line 2272075
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->v:LX/2Sg;

    .line 2272076
    invoke-static {v0}, LX/2Sg;->k(LX/2Sg;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2272077
    goto :goto_0

    .line 2272078
    :catchall_0
    move-exception v0

    const v1, 0x56be1746

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2272079
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 2272080
    :cond_4
    :try_start_2
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->P(Lcom/facebook/search/suggestions/SuggestionsFragment;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x7b691bcd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2272081
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->f:LX/FhB;

    if-eqz v0, :cond_0

    .line 2272082
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2d0;

    iget-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->f:LX/FhB;

    invoke-virtual {v0, v2}, LX/2d0;->b(LX/2dB;)V

    .line 2272083
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, LX/Fgb;->a(LX/Fh9;)V

    .line 2272084
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->k:LX/3Qe;

    invoke-virtual {v0}, LX/3Qe;->e()V

    .line 2272085
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->v:LX/2Sg;

    .line 2272086
    invoke-static {v0}, LX/2Sg;->k(LX/2Sg;)V

    .line 2272087
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->L:LX/2Sd;

    sget-object v2, Lcom/facebook/search/suggestions/SuggestionsFragment;->b:Ljava/lang/String;

    sget-object v3, LX/7CQ;->FRAGMENT_PAUSED:LX/7CQ;

    invoke-virtual {v0, v2, v3}, LX/2Sd;->a(Ljava/lang/String;LX/7CQ;)V

    .line 2272088
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->t(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    .line 2272089
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2272090
    const/16 v0, 0x2b

    const v2, -0x3c05da6c

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, 0x735d68f

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2272091
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2272092
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->s(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    .line 2272093
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    new-instance v2, LX/Fh9;

    invoke-direct {v2, p0}, LX/Fh9;-><init>(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    invoke-interface {v0, v2}, LX/Fgb;->a(LX/Fh9;)V

    .line 2272094
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->D(Lcom/facebook/search/suggestions/SuggestionsFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2272095
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->p()V

    .line 2272096
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2d0;

    iget-object v2, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->f:LX/FhB;

    invoke-virtual {v0, v2}, LX/2d0;->a(LX/2dB;)V

    .line 2272097
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    sget-object v2, Lcom/facebook/search/suggestions/SuggestionsFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    sget-object v3, LX/EPu;->MEMORY:LX/EPu;

    invoke-interface {v0, v2, v3}, LX/Fgb;->a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V

    .line 2272098
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->L:LX/2Sd;

    sget-object v2, Lcom/facebook/search/suggestions/SuggestionsFragment;->b:Ljava/lang/String;

    sget-object v3, LX/7CQ;->FRAGMENT_RESUMED:LX/7CQ;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Typeahead launched with text: \""

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->D(Lcom/facebook/search/suggestions/SuggestionsFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, LX/2Sd;->a(Ljava/lang/String;LX/7CQ;Ljava/lang/String;)V

    .line 2272099
    const/16 v0, 0x2b

    const v2, 0x1f0bee16

    invoke-static {v6, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2272100
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2272101
    const-string v0, "recycler_view_enabled"

    iget-boolean v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ak:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2272102
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x59b0383f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2272103
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2272104
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2272105
    if-eqz v0, :cond_0

    .line 2272106
    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/1ZF;->k_(Z)V

    .line 2272107
    :cond_0
    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->D(Lcom/facebook/search/suggestions/SuggestionsFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2272108
    iget-object v0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->v:LX/2Sg;

    .line 2272109
    sget-object v2, LX/2Si;->WARM_START:LX/2Si;

    invoke-static {v0, v2}, LX/2Sg;->a(LX/2Sg;LX/2Si;)LX/11o;

    move-result-object v2

    .line 2272110
    sget-object p0, LX/Cvr;->BACK_PRESSED:LX/Cvr;

    invoke-static {v2, p0}, LX/2Sg;->a(LX/11o;LX/Cvr;)V

    .line 2272111
    :cond_1
    const/16 v0, 0x2b

    const v2, 0xa3fcb02

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0xc69ad06

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2272112
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2272113
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->j()LX/FgU;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2272114
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->j()LX/FgU;

    move-result-object v1

    const-string v2, "typeahead_sid"

    invoke-virtual {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->k()Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/FgU;->b(LX/0P1;)V

    .line 2272115
    :cond_0
    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->v:LX/2Sg;

    .line 2272116
    invoke-static {v1}, LX/2Sg;->k(LX/2Sg;)V

    .line 2272117
    const/16 v1, 0x2b

    const v2, -0x474be40b

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
