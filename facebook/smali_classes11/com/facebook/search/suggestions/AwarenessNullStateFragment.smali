.class public Lcom/facebook/search/suggestions/AwarenessNullStateFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/FZU;


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/Fan;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/FgS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Fas;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:LX/Fam;

.field private f:Lcom/facebook/widget/text/BetterTextView;

.field public g:Lcom/facebook/widget/CustomLinearLayout;

.field private h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2270388
    const-class v0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;

    const-string v1, "search_awareness"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2270387
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;

    const-class v1, LX/Fan;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/Fan;

    invoke-static {p0}, LX/FgS;->a(LX/0QB;)LX/FgS;

    move-result-object v2

    check-cast v2, LX/FgS;

    invoke-static {p0}, LX/Fas;->a(LX/0QB;)LX/Fas;

    move-result-object p0

    check-cast p0, LX/Fas;

    iput-object v1, p1, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->a:LX/Fan;

    iput-object v2, p1, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->b:LX/FgS;

    iput-object p0, p1, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->c:LX/Fas;

    return-void
.end method


# virtual methods
.method public final a(LX/FZb;)V
    .locals 0

    .prologue
    .line 2270386
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2270381
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2270382
    const-class v0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;

    invoke-static {v0, p0}, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2270383
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 2270384
    iget-object v1, p0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->a:LX/Fan;

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v1, v0}, LX/Fan;->a(I)LX/Fam;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->e:LX/Fam;

    .line 2270385
    return-void
.end method

.method public final b(Z)Z
    .locals 1

    .prologue
    .line 2270380
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Landroid/support/v4/app/Fragment;
    .locals 0

    .prologue
    .line 2270379
    return-object p0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x33060b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2270378
    const v1, 0x7f030151

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x325b3591

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x465407ef

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2270369
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2270370
    iget-object v1, p0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->b:LX/FgS;

    .line 2270371
    iget-object v2, v1, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v1, v2

    .line 2270372
    if-nez v1, :cond_0

    .line 2270373
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x556f57ce

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2270374
    :cond_0
    iget-object v1, p0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->b:LX/FgS;

    .line 2270375
    iget-object v2, v1, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v1, v2

    .line 2270376
    iget-object v2, v1, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v1, v2

    .line 2270377
    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->b()Z

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2270343
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2270344
    iget-object v0, p0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->c:LX/Fas;

    .line 2270345
    iget-object v1, v0, LX/Fas;->l:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    move-object v0, v1

    .line 2270346
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2270347
    iget-object v0, p0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->c:LX/Fas;

    .line 2270348
    iget-object v1, v0, LX/Fas;->l:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    move-object v0, v1

    .line 2270349
    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;->a()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2270350
    iget-object v0, p0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->c:LX/Fas;

    .line 2270351
    iget-object v1, v0, LX/Fas;->l:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    move-object v0, v1

    .line 2270352
    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;->a()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;

    move-result-object v1

    .line 2270353
    const v0, 0x7f0d0633

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->g:Lcom/facebook/widget/CustomLinearLayout;

    .line 2270354
    const v0, 0x7f0d0634

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2270355
    const v0, 0x7f0d0635

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2270356
    iget-object v0, p0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2270357
    invoke-virtual {v1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->j()LX/2uF;

    move-result-object v0

    .line 2270358
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, LX/2sN;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, LX/2sN;->b()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2270359
    iget-object v5, p0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->g:Lcom/facebook/widget/CustomLinearLayout;

    .line 2270360
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string p1, "layout_inflater"

    invoke-virtual {v6, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 2270361
    const p1, 0x7f030152

    iget-object p2, p0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->g:Lcom/facebook/widget/CustomLinearLayout;

    const/4 v0, 0x0

    invoke-virtual {v6, p1, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/facebook/widget/text/BetterTextView;

    .line 2270362
    const/4 p1, 0x7

    invoke-virtual {v4, v3, p1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2270363
    move-object v3, v6

    .line 2270364
    iget-object v4, p0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->g:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v4}, Lcom/facebook/widget/CustomLinearLayout;->getChildCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v5, v3, v4}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;I)V

    goto :goto_0

    .line 2270365
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->e:LX/Fam;

    invoke-virtual {v0, v1}, LX/Fam;->a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2270366
    iget-object v1, p0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v2, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2270367
    iget-object v0, p0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    sget-object v1, LX/FaJ;->i:LX/1Up;

    invoke-virtual {v0, v1}, LX/1af;->a(LX/1Up;)V

    .line 2270368
    return-void
.end method
