.class public Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;
.super Landroid/widget/ImageView;
.source ""


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2275284
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 2275285
    invoke-direct {p0}, Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;->a()V

    .line 2275286
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2275269
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2275270
    invoke-direct {p0}, Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;->a()V

    .line 2275271
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2275281
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2275282
    invoke-direct {p0}, Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;->a()V

    .line 2275283
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2275276
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;->a:Landroid/graphics/Paint;

    .line 2275277
    iget-object v0, p0, Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2275278
    iget-object v0, p0, Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b16d9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2275279
    invoke-virtual {p0}, Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1769

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;->b:I

    .line 2275280
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2275272
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 2275273
    iget v0, p0, Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;->b:I

    int-to-float v1, v0

    invoke-virtual {p0}, Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;->getWidth()I

    move-result v0

    iget v3, p0, Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;->b:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    iget-object v5, p0, Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2275274
    iget v0, p0, Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;->b:I

    int-to-float v1, v0

    invoke-virtual {p0}, Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;->getWidth()I

    move-result v0

    iget v3, p0, Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;->b:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/search/suggestions/viewbinder/RecentSearchSeeMoreView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2275275
    return-void
.end method
