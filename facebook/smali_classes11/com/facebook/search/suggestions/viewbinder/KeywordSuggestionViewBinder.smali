.class public Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:LX/8iB;

.field private static final c:LX/8iB;

.field private static final d:LX/8iB;

.field private static m:LX/0Xm;


# instance fields
.field public final e:Landroid/content/res/Resources;

.field private final f:LX/0wM;

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8iE;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FgS;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8i7;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final j:LX/0Uh;

.field public final k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/8iB;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/8iB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2275206
    const-class v0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;

    const-string v1, "search_typeahead"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2275207
    new-instance v0, LX/FiN;

    invoke-direct {v0}, LX/FiN;-><init>()V

    sput-object v0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->b:LX/8iB;

    .line 2275208
    new-instance v0, LX/FiO;

    invoke-direct {v0}, LX/FiO;-><init>()V

    sput-object v0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->c:LX/8iB;

    .line 2275209
    new-instance v0, LX/FiP;

    invoke-direct {v0}, LX/FiP;-><init>()V

    sput-object v0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->d:LX/8iB;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0wM;LX/0Uh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2275210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2275211
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2275212
    iput-object v0, p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->g:LX/0Ot;

    .line 2275213
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2275214
    iput-object v0, p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->h:LX/0Ot;

    .line 2275215
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2275216
    iput-object v0, p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->i:LX/0Ot;

    .line 2275217
    iput-object p1, p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->e:Landroid/content/res/Resources;

    .line 2275218
    iput-object p2, p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->f:LX/0wM;

    .line 2275219
    iput-object p3, p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->j:LX/0Uh;

    .line 2275220
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2275221
    iput-object v0, p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->k:LX/0Px;

    .line 2275222
    sget-object v0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->b:LX/8iB;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->l:LX/0Px;

    .line 2275223
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;
    .locals 6

    .prologue
    .line 2275224
    const-class v1, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;

    monitor-enter v1

    .line 2275225
    :try_start_0
    sget-object v0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2275226
    sput-object v2, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2275227
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2275228
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2275229
    new-instance p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v4

    check-cast v4, LX/0wM;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;-><init>(Landroid/content/res/Resources;LX/0wM;LX/0Uh;)V

    .line 2275230
    const/16 v3, 0x3515

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x34b1

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x3512

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    .line 2275231
    iput-object v3, p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->g:LX/0Ot;

    iput-object v4, p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->h:LX/0Ot;

    iput-object v5, p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->i:LX/0Ot;

    .line 2275232
    move-object v0, p0

    .line 2275233
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2275234
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2275235
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2275236
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2275237
    iget-object v0, p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FgS;

    .line 2275238
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2275239
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 2275240
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FgS;

    .line 2275241
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2275242
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 2275243
    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static a(Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;Lcom/facebook/fbui/widget/contentview/ContentView;Landroid/graphics/drawable/Drawable;)V
    .locals 5

    .prologue
    .line 2275244
    sget-object v0, LX/6VF;->SMALL:LX/6VF;

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2275245
    invoke-virtual {p1, p2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2275246
    iget-object v0, p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->e:Landroid/content/res/Resources;

    const v1, 0x7f0b17b1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 2275247
    invoke-virtual {p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->getPaddingLeft()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->e:Landroid/content/res/Resources;

    const v2, 0x7f0b17b2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->getPaddingRight()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->e:Landroid/content/res/Resources;

    const v4, 0x7f0b17b2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setPadding(IIII)V

    .line 2275248
    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 2275249
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/CwF;Z)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 2275250
    sget-object v0, LX/FiM;->b:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/FiM;->b:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    move v0, v0

    .line 2275251
    iget-object v1, p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->f:LX/0wM;

    .line 2275252
    sget-object v2, LX/FiM;->f:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 2275253
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :goto_1
    move v2, v2

    .line 2275254
    invoke-virtual {v1, v0, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2275255
    invoke-virtual {p0, p1, v0, p3}, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0

    :cond_0
    sget v0, LX/FiM;->c:I

    goto :goto_0

    :cond_1
    const v2, -0xc5a768

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;
    .locals 6

    .prologue
    .line 2275256
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2275257
    iget-object v1, p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->e:Landroid/content/res/Resources;

    const v2, 0x7f020ddc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2275258
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2275259
    if-eqz p3, :cond_0

    .line 2275260
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 2275261
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x7f0106ce

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 2275262
    iget-object v2, p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->e:Landroid/content/res/Resources;

    const v3, 0x7f020ddb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2275263
    iget-object v3, p0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->f:LX/0wM;

    const/high16 v4, -0x67000000

    iget v1, v1, Landroid/util/TypedValue;->data:I

    const v5, 0xffffff

    and-int/2addr v1, v5

    or-int/2addr v1, v4

    invoke-virtual {v3, v2, v1}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2275264
    :cond_0
    new-instance v1, Landroid/graphics/drawable/LayerDrawable;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/graphics/drawable/Drawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    return-object v1
.end method
