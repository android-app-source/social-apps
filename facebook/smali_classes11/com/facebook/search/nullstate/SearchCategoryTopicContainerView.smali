.class public Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/widget/FlowLayout;

.field public b:LX/Fa6;

.field public c:LX/Fa1;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2258284
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2258285
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2258280
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2258281
    const v0, 0x7f031279

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2258282
    const v0, 0x7f0d2b67

    invoke-virtual {p0, v0}, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FlowLayout;

    iput-object v0, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;->a:Lcom/facebook/widget/FlowLayout;

    .line 2258283
    return-void
.end method

.method private b()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2258279
    new-instance v0, LX/Fa0;

    invoke-direct {v0, p0}, LX/Fa0;-><init>(Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;)V

    return-object v0
.end method

.method public static c(Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;I)V
    .locals 5

    .prologue
    .line 2258256
    invoke-direct {p0}, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;->b()Landroid/view/View$OnClickListener;

    move-result-object v1

    .line 2258257
    :goto_0
    iget-object v0, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;->b:LX/Fa6;

    invoke-virtual {v0}, LX/Fa6;->a()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 2258258
    invoke-virtual {p0}, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f031277

    iget-object v3, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;->a:Lcom/facebook/widget/FlowLayout;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 2258259
    iget-object v2, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;->b:LX/Fa6;

    invoke-virtual {v2, p1}, LX/Fa6;->a(I)LX/CwV;

    move-result-object v2

    .line 2258260
    iget-object v3, v2, LX/CwV;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2258261
    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2258262
    iget-boolean v3, v2, LX/CwV;->d:Z

    move v2, v3

    .line 2258263
    if-eqz v2, :cond_0

    .line 2258264
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 2258265
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setTag(Ljava/lang/Object;)V

    .line 2258266
    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2258267
    iget-object v2, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;->a:Lcom/facebook/widget/FlowLayout;

    invoke-virtual {v2, v0}, Lcom/facebook/widget/FlowLayout;->addView(Landroid/view/View;)V

    .line 2258268
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 2258269
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2258276
    iget-object v0, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;->b:LX/Fa6;

    invoke-virtual {v0}, LX/Fa6;->a()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;->a:Lcom/facebook/widget/FlowLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/FlowLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2258277
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;->c(Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;I)V

    .line 2258278
    :cond_0
    return-void
.end method

.method public setTopicDataModel(LX/Fa6;)V
    .locals 2

    .prologue
    .line 2258272
    iput-object p1, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;->b:LX/Fa6;

    .line 2258273
    iget-object v0, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;->b:LX/Fa6;

    new-instance v1, LX/FZz;

    invoke-direct {v1, p0}, LX/FZz;-><init>(Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;)V

    .line 2258274
    iput-object v1, v0, LX/Fa6;->d:LX/FZz;

    .line 2258275
    return-void
.end method

.method public setTopicOnClickListener(LX/Fa1;)V
    .locals 0

    .prologue
    .line 2258270
    iput-object p1, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;->c:LX/Fa1;

    .line 2258271
    return-void
.end method
