.class public Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final p:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fho;",
            ">;"
        }
    .end annotation
.end field

.field private r:LX/Fa5;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private s:Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;

.field private t:LX/Fa6;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2258235
    const-class v0, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;

    const-string v1, "search"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2258232
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2258233
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2258234
    iput-object v0, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->q:LX/0Ot;

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2258220
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2258221
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2258222
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const v2, 0x7f08233c

    invoke-virtual {p0, v2}, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2258223
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2258224
    move-object v1, v1

    .line 2258225
    const/4 v2, -0x2

    .line 2258226
    iput v2, v1, LX/108;->h:I

    .line 2258227
    move-object v1, v1

    .line 2258228
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2258229
    new-instance v1, LX/FZx;

    invoke-direct {v1, p0}, LX/FZx;-><init>(Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2258230
    new-instance v1, LX/FZy;

    invoke-direct {v1, p0}, LX/FZy;-><init>(Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2258231
    return-void
.end method

.method private static a(Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;LX/0Ot;LX/Fa5;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;",
            "LX/0Ot",
            "<",
            "LX/Fho;",
            ">;",
            "LX/Fa5;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2258219
    iput-object p1, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->q:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->r:LX/Fa5;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;

    const/16 v1, 0x34ca

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    new-instance v2, LX/Fa5;

    new-instance v5, LX/FaA;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {v5, v3, v4}, LX/FaA;-><init>(LX/0tX;LX/0ad;)V

    move-object v3, v5

    check-cast v3, LX/FaA;

    invoke-static {v0}, LX/Fa6;->a(LX/0QB;)LX/Fa6;

    move-result-object v4

    check-cast v4, LX/Fa6;

    new-instance v7, LX/Fa7;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-direct {v7, v5, v6}, LX/Fa7;-><init>(LX/0tX;Ljava/lang/String;)V

    move-object v5, v7

    check-cast v5, LX/Fa7;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v8

    check-cast v8, LX/0kL;

    invoke-static {v0}, LX/Cvp;->b(LX/0QB;)LX/Cvp;

    move-result-object v9

    check-cast v9, LX/Cvp;

    invoke-direct/range {v2 .. v9}, LX/Fa5;-><init>(LX/FaA;LX/Fa6;LX/Fa7;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0kL;LX/Cvp;)V

    move-object v0, v2

    check-cast v0, LX/Fa5;

    invoke-static {p0, v1, v0}, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->a(Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;LX/0Ot;LX/Fa5;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2258134
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2258135
    invoke-static {p0, p0}, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2258136
    const v0, 0x7f031284

    invoke-virtual {p0, v0}, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->setContentView(I)V

    .line 2258137
    invoke-direct {p0}, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->a()V

    .line 2258138
    iget-object v0, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->r:LX/Fa5;

    .line 2258139
    iget-object v1, v0, LX/Fa5;->b:LX/Fa6;

    move-object v0, v1

    .line 2258140
    iput-object v0, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->t:LX/Fa6;

    .line 2258141
    const v0, 0x7f0d2b6f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;

    iput-object v0, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->s:Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;

    .line 2258142
    iget-object v0, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->s:Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;

    iget-object v1, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->t:LX/Fa6;

    invoke-virtual {v0, v1}, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;->setTopicDataModel(LX/Fa6;)V

    .line 2258143
    iget-object v0, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->s:Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;

    iget-object v1, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->r:LX/Fa5;

    .line 2258144
    iput-object v1, v0, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;->c:LX/Fa1;

    .line 2258145
    iget-object v0, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->t:LX/Fa6;

    invoke-virtual {v0}, LX/Fa6;->a()I

    move-result v0

    if-lez v0, :cond_0

    .line 2258146
    iget-object v0, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->s:Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;

    invoke-virtual {v0}, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;->a()V

    .line 2258147
    :goto_0
    return-void

    .line 2258148
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->r:LX/Fa5;

    .line 2258149
    iget-object v1, v0, LX/Fa5;->a:LX/FaA;

    .line 2258150
    new-instance p0, LX/A1h;

    invoke-direct {p0}, LX/A1h;-><init>()V

    move-object p0, p0

    .line 2258151
    invoke-static {p0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object p0

    const/4 p1, 0x1

    .line 2258152
    iput-boolean p1, p0, LX/0zO;->p:Z

    .line 2258153
    move-object p0, p0

    .line 2258154
    iget-object p1, v1, LX/FaA;->a:LX/0tX;

    invoke-virtual {p1, p0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object p0

    .line 2258155
    new-instance p1, LX/Fa8;

    invoke-direct {p1, v1}, LX/Fa8;-><init>(LX/FaA;)V

    invoke-static {p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p0

    move-object v1, p0

    .line 2258156
    new-instance p0, LX/Fa2;

    invoke-direct {p0, v0}, LX/Fa2;-><init>(LX/Fa5;)V

    .line 2258157
    iget-object p1, v0, LX/Fa5;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2258158
    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 11

    .prologue
    .line 2258159
    iget-object v0, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->r:LX/Fa5;

    .line 2258160
    iget-object v1, v0, LX/Fa5;->b:LX/Fa6;

    .line 2258161
    iget-object v2, v1, LX/Fa6;->c:Ljava/util/Set;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 2258162
    iget-object v2, v0, LX/Fa5;->g:LX/Cvp;

    .line 2258163
    const-string v3, "end_topic_selection"

    invoke-static {v2, v3}, LX/Cvp;->c(LX/Cvp;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "session_end_reason"

    const-string v5, "back_press"

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2258164
    new-instance v6, LX/162;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v3}, LX/162;-><init>(LX/0mC;)V

    .line 2258165
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v7

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v7, :cond_0

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CwV;

    .line 2258166
    new-instance v8, LX/0m9;

    sget-object v9, LX/0mC;->a:LX/0mC;

    invoke-direct {v8, v9}, LX/0m9;-><init>(LX/0mC;)V

    .line 2258167
    const-string v9, "id"

    .line 2258168
    iget-object v10, v3, LX/CwV;->a:Ljava/lang/String;

    move-object v10, v10

    .line 2258169
    invoke-virtual {v8, v9, v10}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2258170
    const-string v9, "name"

    .line 2258171
    iget-object v10, v3, LX/CwV;->b:Ljava/lang/String;

    move-object v10, v10

    .line 2258172
    invoke-virtual {v8, v9, v10}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2258173
    const-string v9, "is_selected"

    .line 2258174
    iget-boolean v10, v3, LX/CwV;->d:Z

    move v3, v10

    .line 2258175
    invoke-virtual {v8, v9, v3}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 2258176
    invoke-virtual {v6, v8}, LX/162;->a(LX/0lF;)LX/162;

    .line 2258177
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 2258178
    :cond_0
    const-string v3, "toggled_category_topics"

    invoke-virtual {v5, v3, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2258179
    iget-object v3, v2, LX/Cvp;->b:LX/0Zb;

    invoke-interface {v3, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2258180
    const/4 v3, 0x3

    invoke-static {v3}, LX/01m;->b(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2258181
    invoke-virtual {v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->s()Ljava/lang/String;

    .line 2258182
    :cond_1
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2258183
    const/4 v1, 0x0

    .line 2258184
    :goto_1
    move v0, v1

    .line 2258185
    if-eqz v0, :cond_2

    .line 2258186
    iget-object v0, p0, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fho;

    sget-object v1, Lcom/facebook/search/nullstate/SearchCategoryTopicChooserActivity;->p:Lcom/facebook/common/callercontext/CallerContext;

    sget-object v2, LX/EPu;->NETWORK_BYPASS_CACHE_ONLY:LX/EPu;

    invoke-virtual {v0, v1, v2}, LX/2SP;->a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V

    .line 2258187
    :cond_2
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2258188
    return-void

    .line 2258189
    :cond_3
    iget-object v2, v0, LX/Fa5;->c:LX/Fa7;

    .line 2258190
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2258191
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v6

    const/4 v3, 0x0

    move v4, v3

    :goto_2
    if-ge v4, v6, :cond_4

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CwV;

    .line 2258192
    new-instance v7, LX/4Js;

    invoke-direct {v7}, LX/4Js;-><init>()V

    .line 2258193
    iget-object v8, v3, LX/CwV;->a:Ljava/lang/String;

    move-object v8, v8

    .line 2258194
    const-string v9, "topic_id"

    invoke-virtual {v7, v9, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2258195
    move-object v7, v7

    .line 2258196
    iget-boolean v8, v3, LX/CwV;->d:Z

    move v3, v8

    .line 2258197
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 2258198
    const-string v8, "is_selected"

    invoke-virtual {v7, v8, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2258199
    move-object v3, v7

    .line 2258200
    invoke-virtual {v5, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2258201
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    .line 2258202
    :cond_4
    new-instance v3, LX/4JH;

    invoke-direct {v3}, LX/4JH;-><init>()V

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2258203
    const-string v6, "client_mutation_id"

    invoke-virtual {v3, v6, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2258204
    move-object v3, v3

    .line 2258205
    iget-object v4, v2, LX/Fa7;->b:Ljava/lang/String;

    .line 2258206
    const-string v6, "actor_id"

    invoke-virtual {v3, v6, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2258207
    move-object v3, v3

    .line 2258208
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 2258209
    const-string v5, "topic_feed_or_subtopic"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2258210
    move-object v3, v3

    .line 2258211
    move-object v3, v3

    .line 2258212
    new-instance v4, LX/A1n;

    invoke-direct {v4}, LX/A1n;-><init>()V

    move-object v4, v4

    .line 2258213
    const-string v5, "input"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/A1n;

    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 2258214
    iget-object v4, v2, LX/Fa7;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v1, v3

    .line 2258215
    new-instance v2, LX/Fa3;

    invoke-direct {v2, v0}, LX/Fa3;-><init>(LX/Fa5;)V

    .line 2258216
    iget-object v3, v0, LX/Fa5;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2258217
    iget-object v1, v0, LX/Fa5;->b:LX/Fa6;

    invoke-virtual {v1}, LX/Fa6;->c()V

    .line 2258218
    const/4 v1, 0x1

    goto/16 :goto_1
.end method
