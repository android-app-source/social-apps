.class public Lcom/facebook/search/fragment/GraphSearchFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/0fj;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/5vN;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:LX/FiU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/FgR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/FZf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:LX/FZY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:LX/Fi6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:LX/EQ9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:LX/Cvs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:LX/FZS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private j:LX/8ht;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0x9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fhp;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

.field public p:LX/FZe;

.field private q:LX/FZX;

.field private r:Landroid/view/View$OnTouchListener;

.field private s:LX/FZb;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2257661
    const-class v0, Lcom/facebook/search/fragment/GraphSearchFragment;

    const-string v1, "search"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/fragment/GraphSearchFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2257655
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2257656
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2257657
    iput-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->m:LX/0Ot;

    .line 2257658
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2257659
    iput-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->n:LX/0Ot;

    .line 2257660
    new-instance v0, LX/FZb;

    invoke-direct {v0, p0}, LX/FZb;-><init>(Lcom/facebook/search/fragment/GraphSearchFragment;)V

    iput-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->s:LX/FZb;

    return-void
.end method

.method private static a(Lcom/facebook/search/fragment/GraphSearchFragment;LX/FiU;LX/FgR;LX/FZf;LX/FZY;LX/Fi6;LX/EQ9;LX/Cvs;LX/FZS;LX/8ht;LX/0Uh;LX/0Or;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/fragment/GraphSearchFragment;",
            "LX/FiU;",
            "LX/FgR;",
            "LX/FZf;",
            "LX/FZY;",
            "LX/Fi6;",
            "LX/EQ9;",
            "LX/Cvs;",
            "LX/FZS;",
            "LX/8ht;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0x9;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fhp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2257654
    iput-object p1, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->b:LX/FiU;

    iput-object p2, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->c:LX/FgR;

    iput-object p3, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->d:LX/FZf;

    iput-object p4, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->e:LX/FZY;

    iput-object p5, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->f:LX/Fi6;

    iput-object p6, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->g:LX/EQ9;

    iput-object p7, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->h:LX/Cvs;

    iput-object p8, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->i:LX/FZS;

    iput-object p9, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->j:LX/8ht;

    iput-object p10, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->k:LX/0Uh;

    iput-object p11, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->l:LX/0Or;

    iput-object p12, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->m:LX/0Ot;

    iput-object p13, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->n:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 15

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v13

    move-object v0, p0

    check-cast v0, Lcom/facebook/search/fragment/GraphSearchFragment;

    invoke-static {v13}, LX/FiU;->a(LX/0QB;)LX/FiU;

    move-result-object v1

    check-cast v1, LX/FiU;

    invoke-static {v13}, LX/FgR;->a(LX/0QB;)LX/FgR;

    move-result-object v2

    check-cast v2, LX/FgR;

    const-class v3, LX/FZf;

    invoke-interface {v13, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/FZf;

    const-class v4, LX/FZY;

    invoke-interface {v13, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/FZY;

    invoke-static {v13}, LX/Fi6;->a(LX/0QB;)LX/Fi6;

    move-result-object v5

    check-cast v5, LX/Fi6;

    invoke-static {v13}, LX/EQ9;->a(LX/0QB;)LX/EQ9;

    move-result-object v6

    check-cast v6, LX/EQ9;

    invoke-static {v13}, LX/FZw;->b(LX/0QB;)LX/Cvs;

    move-result-object v7

    check-cast v7, LX/Cvs;

    invoke-static {v13}, LX/FZS;->a(LX/0QB;)LX/FZS;

    move-result-object v8

    check-cast v8, LX/FZS;

    invoke-static {v13}, LX/8ht;->a(LX/0QB;)LX/8ht;

    move-result-object v9

    check-cast v9, LX/8ht;

    invoke-static {v13}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    const/16 v11, 0x122d

    invoke-static {v13, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v12, 0x1147

    invoke-static {v13, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v14, 0x34cb

    invoke-static {v13, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {v0 .. v13}, Lcom/facebook/search/fragment/GraphSearchFragment;->a(Lcom/facebook/search/fragment/GraphSearchFragment;LX/FiU;LX/FgR;LX/FZf;LX/FZY;LX/Fi6;LX/EQ9;LX/Cvs;LX/FZS;LX/8ht;LX/0Uh;LX/0Or;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method private k()LX/8ci;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2257648
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2257649
    if-nez v0, :cond_0

    .line 2257650
    const/4 v0, 0x0

    .line 2257651
    :goto_0
    return-object v0

    .line 2257652
    :cond_0
    const-string v1, "source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2257653
    invoke-static {v0}, LX/8ci;->a(Ljava/lang/String;)LX/8ci;

    move-result-object v0

    goto :goto_0
.end method

.method private l()LX/CwB;
    .locals 14

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 2257578
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2257579
    if-nez v1, :cond_1

    .line 2257580
    :cond_0
    :goto_0
    return-object v0

    .line 2257581
    :cond_1
    const-string v2, "extra_sport_query_live_page_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2257582
    const-string v3, "extra_sport_query_live_page_title"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2257583
    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    .line 2257584
    new-instance v0, LX/CwH;

    invoke-direct {v0}, LX/CwH;-><init>()V

    .line 2257585
    const-string v1, "keywords_topic_sport_match(%s)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v4, v6

    invoke-static {v1, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2257586
    iput-object v1, v0, LX/CwH;->c:Ljava/lang/String;

    .line 2257587
    move-object v0, v0

    .line 2257588
    iput-object v3, v0, LX/CwH;->b:Ljava/lang/String;

    .line 2257589
    move-object v0, v0

    .line 2257590
    const-string v1, "news_v2"

    .line 2257591
    iput-object v1, v0, LX/CwH;->e:Ljava/lang/String;

    .line 2257592
    move-object v0, v0

    .line 2257593
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2257594
    iput-object v1, v0, LX/CwH;->f:Ljava/lang/Boolean;

    .line 2257595
    move-object v0, v0

    .line 2257596
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2257597
    iput-object v1, v0, LX/CwH;->x:LX/0Px;

    .line 2257598
    move-object v0, v0

    .line 2257599
    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    goto :goto_0

    .line 2257600
    :cond_2
    const-string v2, "extra_query_function"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2257601
    const-string v3, "extra_query_display_style"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2257602
    const-string v4, "extra_query_title"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2257603
    if-eqz v2, :cond_3

    if-eqz v3, :cond_3

    if-eqz v4, :cond_3

    .line 2257604
    new-instance v0, LX/CwD;

    invoke-direct {v0}, LX/CwD;-><init>()V

    .line 2257605
    iput-object v4, v0, LX/CwD;->a:Ljava/lang/String;

    .line 2257606
    move-object v0, v0

    .line 2257607
    iput-object v2, v0, LX/CwD;->b:Ljava/lang/String;

    .line 2257608
    move-object v0, v0

    .line 2257609
    const-string v1, "news_v2"

    .line 2257610
    iput-object v1, v0, LX/CwD;->d:Ljava/lang/String;

    .line 2257611
    move-object v0, v0

    .line 2257612
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2257613
    iput-object v1, v0, LX/CwD;->e:Ljava/lang/Boolean;

    .line 2257614
    move-object v0, v0

    .line 2257615
    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2257616
    iput-object v1, v0, LX/CwD;->f:LX/0Px;

    .line 2257617
    move-object v0, v0

    .line 2257618
    invoke-virtual {v0}, LX/CwD;->j()LX/CwE;

    move-result-object v0

    goto/16 :goto_0

    .line 2257619
    :cond_3
    const-string v2, "query_vertical"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2257620
    const-string v3, "source"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2257621
    const-string v4, "query_title"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2257622
    const-string v5, "query_function"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2257623
    const-string v6, "exact_match"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 2257624
    const-string v7, "graph_search_keyword_type"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2257625
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    .line 2257626
    const-string v0, "display_style"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2257627
    if-eqz v0, :cond_5

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v0

    .line 2257628
    :goto_1
    new-instance v3, LX/CwH;

    invoke-direct {v3}, LX/CwH;-><init>()V

    .line 2257629
    iput-object v4, v3, LX/CwH;->b:Ljava/lang/String;

    .line 2257630
    move-object v3, v3

    .line 2257631
    iput-object v5, v3, LX/CwH;->c:Ljava/lang/String;

    .line 2257632
    move-object v3, v3

    .line 2257633
    iput-object v2, v3, LX/CwH;->e:Ljava/lang/String;

    .line 2257634
    move-object v2, v3

    .line 2257635
    iput-object v6, v2, LX/CwH;->f:Ljava/lang/Boolean;

    .line 2257636
    move-object v2, v2

    .line 2257637
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2257638
    iput-object v0, v2, LX/CwH;->x:LX/0Px;

    .line 2257639
    move-object v0, v2

    .line 2257640
    new-instance v8, Lcom/facebook/search/model/ReactionSearchData;

    const-string v9, "place_id"

    invoke-virtual {v1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "ranking_data"

    invoke-virtual {v1, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "semantic"

    invoke-virtual {v1, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "reaction_session_id"

    invoke-virtual {v1, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "reaction_surface"

    invoke-virtual {v1, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-direct/range {v8 .. v13}, Lcom/facebook/search/model/ReactionSearchData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v8

    .line 2257641
    iput-object v1, v0, LX/CwH;->k:Lcom/facebook/search/model/ReactionSearchData;

    .line 2257642
    move-object v0, v0

    .line 2257643
    if-eqz v7, :cond_4

    .line 2257644
    invoke-static {v7}, LX/CwF;->valueOf(Ljava/lang/String;)LX/CwF;

    move-result-object v1

    .line 2257645
    iput-object v1, v0, LX/CwH;->g:LX/CwF;

    .line 2257646
    :cond_4
    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    goto/16 :goto_0

    .line 2257647
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_1
.end method

.method private m()Lcom/facebook/search/api/GraphSearchQuery;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2257561
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v0

    .line 2257562
    if-nez v2, :cond_1

    .line 2257563
    const/4 v0, 0x0

    .line 2257564
    :cond_0
    :goto_0
    return-object v0

    .line 2257565
    :cond_1
    const-string v0, "initial_typeahead_query"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/GraphSearchQuery;

    .line 2257566
    if-eqz v0, :cond_3

    .line 2257567
    iget-object v1, v0, LX/7B6;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2257568
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2257569
    iget-object v1, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->j:LX/8ht;

    invoke-virtual {v1, v0}, LX/8ht;->i(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->j:LX/8ht;

    invoke-virtual {v1, v0}, LX/8ht;->g(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_2
    const/4 v1, 0x1

    .line 2257570
    :goto_1
    if-eqz v1, :cond_3

    .line 2257571
    iget-object v1, v0, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    move-object v1, v1

    .line 2257572
    invoke-static {v0, v1}, Lcom/facebook/search/api/GraphSearchQuery;->a(Lcom/facebook/search/api/GraphSearchQuery;Ljava/lang/String;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    .line 2257573
    :cond_3
    const-string v1, "search_awareness_config"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2257574
    if-nez v0, :cond_4

    .line 2257575
    sget-object v0, Lcom/facebook/search/api/GraphSearchQuery;->e:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2257576
    :cond_4
    sget-object v1, LX/7B4;->AWARENESS:LX/7B4;

    const-string v3, "search_awareness_config"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7B4;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 2257577
    :cond_5
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2257554
    invoke-direct {p0}, Lcom/facebook/search/fragment/GraphSearchFragment;->l()LX/CwB;

    move-result-object v0

    .line 2257555
    invoke-direct {p0}, Lcom/facebook/search/fragment/GraphSearchFragment;->m()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v1

    .line 2257556
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/CwB;->a()Ljava/lang/String;

    move-result-object v0

    .line 2257557
    :goto_0
    invoke-static {v0}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2257558
    :cond_0
    if-eqz v1, :cond_1

    .line 2257559
    iget-object v0, v1, LX/7B6;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2257560
    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()V
    .locals 13

    .prologue
    .line 2257534
    invoke-static {p0}, Lcom/facebook/search/fragment/GraphSearchFragment;->p(Lcom/facebook/search/fragment/GraphSearchFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2257535
    iget-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x9;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    .line 2257536
    iget-object v2, v0, LX/0x9;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fas;

    .line 2257537
    invoke-virtual {v2}, LX/Fas;->c()Z

    move-result v3

    if-nez v3, :cond_1

    .line 2257538
    :cond_0
    :goto_0
    return-void

    .line 2257539
    :cond_1
    new-instance v4, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SEARCH_TUTORIAL_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v4, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    .line 2257540
    iget-object v3, v2, LX/Fas;->i:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0iA;

    const-class v5, LX/Fb8;

    invoke-virtual {v3, v4, v5}, LX/0iA;->b(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2257541
    iget-object v6, v2, LX/Fas;->l:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    if-nez v6, :cond_4

    .line 2257542
    iget-object v6, v2, LX/Fas;->f:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Fap;

    .line 2257543
    iput-object v2, v6, LX/Fap;->d:LX/2ST;

    .line 2257544
    iget-object v6, v2, LX/Fas;->f:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Fap;

    .line 2257545
    iget-object v8, v6, LX/Fap;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v8, :cond_2

    iget-object v8, v6, LX/Fap;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v8}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2257546
    :cond_2
    invoke-static {}, LX/A0Y;->b()LX/A0W;

    move-result-object v9

    invoke-static {v9}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v10

    .line 2257547
    iget-object v9, v6, LX/Fap;->a:LX/0Or;

    invoke-interface {v9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2257548
    sget-object v11, LX/7CP;->g:LX/0Tn;

    const/4 v12, 0x0

    invoke-interface {v9, v11, v12}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v9

    move v9, v9

    .line 2257549
    if-eqz v9, :cond_5

    sget-object v9, LX/0zS;->c:LX/0zS;

    :goto_1
    invoke-virtual {v10, v9}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v9

    const-wide/32 v11, 0x15180

    invoke-virtual {v9, v11, v12}, LX/0zO;->a(J)LX/0zO;

    move-result-object v9

    move-object v8, v9

    .line 2257550
    invoke-static {v6, v8}, LX/Fap;->a(LX/Fap;LX/0zO;)V

    .line 2257551
    :cond_3
    :goto_2
    invoke-static {v2, v4}, LX/Fas;->a(LX/Fas;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    goto :goto_0

    .line 2257552
    :cond_4
    invoke-static {v2}, LX/Fas;->e(LX/Fas;)LX/Fam;

    move-result-object v6

    invoke-virtual {v6}, LX/Fam;->a()Z

    move-result v6

    if-nez v6, :cond_3

    .line 2257553
    invoke-static {v2}, LX/Fas;->e(LX/Fas;)LX/Fam;

    move-result-object v6

    iget-object v7, v2, LX/Fas;->l:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    invoke-virtual {v6, v7}, LX/Fam;->a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;)V

    goto :goto_2

    :cond_5
    sget-object v9, LX/0zS;->a:LX/0zS;

    goto :goto_1
.end method

.method private static p(Lcom/facebook/search/fragment/GraphSearchFragment;)Z
    .locals 3

    .prologue
    .line 2257532
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2257533
    const-string v1, "is_awareness_unit_eligible_intent_flag"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final S_()Z
    .locals 2

    .prologue
    .line 2257531
    iget-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->p:LX/FZe;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/FZe;->a(Z)Z

    move-result v0

    return v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2257530
    iget-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->q:LX/FZX;

    invoke-virtual {v0}, LX/FZX;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 2257662
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2257663
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/search/fragment/GraphSearchFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2257664
    iget-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fhp;

    .line 2257665
    iget-object v2, v0, LX/Fhp;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2257666
    iget-object v2, v0, LX/Fhp;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    .line 2257667
    new-instance v3, Lcom/facebook/search/suggestions/nullstate/NullStatePartDefinitionInitializer$1;

    invoke-direct {v3, v0}, Lcom/facebook/search/suggestions/nullstate/NullStatePartDefinitionInitializer$1;-><init>(LX/Fhp;)V

    move-object v3, v3

    .line 2257668
    const v4, 0x2fa70366

    invoke-static {v2, v3, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2257669
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    .line 2257670
    new-instance v2, LX/FZW;

    const v3, 0x7f0d1202

    invoke-direct {v2, v0, v3}, LX/FZW;-><init>(LX/0gc;I)V

    .line 2257671
    invoke-direct {p0}, Lcom/facebook/search/fragment/GraphSearchFragment;->l()LX/CwB;

    move-result-object v3

    .line 2257672
    invoke-direct {p0}, Lcom/facebook/search/fragment/GraphSearchFragment;->m()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v4

    .line 2257673
    iget-object v5, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->e:LX/FZY;

    .line 2257674
    new-instance v9, LX/FZX;

    invoke-static {v5}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {v5}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v7

    check-cast v7, LX/0gh;

    invoke-static {v5}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v8

    check-cast v8, Landroid/os/Handler;

    invoke-direct {v9, v0, v6, v7, v8}, LX/FZX;-><init>(LX/0gc;LX/0Uh;LX/0gh;Landroid/os/Handler;)V

    .line 2257675
    move-object v0, v9

    .line 2257676
    iput-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->q:LX/FZX;

    .line 2257677
    iget-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->d:LX/FZf;

    iget-object v5, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->q:LX/FZX;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v6

    invoke-virtual {v0, v2, v5, v6}, LX/FZf;->a(LX/FZW;LX/FZX;LX/0gc;)LX/FZe;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->p:LX/FZe;

    .line 2257678
    if-eqz v3, :cond_3

    .line 2257679
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2257680
    if-eqz v0, :cond_5

    .line 2257681
    const-string v1, "typeahead_session_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2257682
    const-string v2, "candidate_session_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2257683
    :goto_0
    iget-object v2, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->p:LX/FZe;

    new-instance v4, Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    invoke-direct {v4, v1, v0}, Lcom/facebook/search/logging/api/SearchTypeaheadSession;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/facebook/search/fragment/GraphSearchFragment;->k()LX/8ci;

    move-result-object v0

    .line 2257684
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2257685
    iput-object v4, v2, LX/FZe;->w:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    .line 2257686
    iput-object v3, v2, LX/FZe;->x:LX/CwB;

    .line 2257687
    iput-object v0, v2, LX/FZe;->z:LX/8ci;

    .line 2257688
    iput-object v1, v2, LX/FZe;->E:Landroid/os/Bundle;

    .line 2257689
    :cond_1
    :goto_1
    if-eqz p1, :cond_2

    .line 2257690
    iget-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->q:LX/FZX;

    const-string v1, "facebook:graphsearch:current_fragment_tag"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2257691
    iput-object v1, v0, LX/FZX;->c:Ljava/lang/String;

    .line 2257692
    :cond_2
    invoke-direct {p0}, Lcom/facebook/search/fragment/GraphSearchFragment;->o()V

    .line 2257693
    iget-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->h:LX/Cvs;

    invoke-interface {v0}, LX/Cvs;->d()V

    .line 2257694
    return-void

    .line 2257695
    :cond_3
    if-eqz v4, :cond_4

    .line 2257696
    iget-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->p:LX/FZe;

    invoke-direct {p0}, Lcom/facebook/search/fragment/GraphSearchFragment;->k()LX/8ci;

    move-result-object v1

    .line 2257697
    iput-object v4, v0, LX/FZe;->y:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2257698
    iput-object v1, v0, LX/FZe;->z:LX/8ci;

    .line 2257699
    iget-object v2, v0, LX/FZe;->y:Lcom/facebook/search/api/GraphSearchQuery;

    sget-object v3, LX/7B4;->AWARENESS:LX/7B4;

    invoke-virtual {v2, v3}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7B4;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/model/awareness/AwarenessGraphSearchConfiguration;

    iput-object v2, v0, LX/FZe;->A:Lcom/facebook/search/model/awareness/AwarenessGraphSearchConfiguration;

    .line 2257700
    goto :goto_1

    .line 2257701
    :cond_4
    invoke-static {p0}, Lcom/facebook/search/fragment/GraphSearchFragment;->p(Lcom/facebook/search/fragment/GraphSearchFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2257702
    iget-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x9;

    invoke-virtual {v0}, LX/0x9;->a()V

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2257529
    iget-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->q:LX/FZX;

    invoke-virtual {v0}, LX/FZX;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final kR_()LX/0cQ;
    .locals 1

    .prologue
    .line 2257528
    sget-object v0, LX/0cQ;->SEARCH_FRAGMENT:LX/0cQ;

    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xb7a7d40

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2257526
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2257527
    const/16 v1, 0x2b

    const v2, 0x119a9f4b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2257509
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2257510
    if-eqz p3, :cond_1

    const-string v0, "try_show_survey_on_result_integration_point_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2257511
    const-string v0, "try_show_survey_on_result_integration_point_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2257512
    iget-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    .line 2257513
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 2257514
    move-object v1, v0

    .line 2257515
    const-string v0, "try_show_survey_on_result_extra_data"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    instance-of v0, v0, Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 2257516
    const-string v0, "try_show_survey_on_result_extra_data"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {v1, v0}, LX/0gt;->a(Landroid/os/Bundle;)LX/0gt;

    .line 2257517
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0gt;->a(Landroid/content/Context;)V

    .line 2257518
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->p:LX/FZe;

    .line 2257519
    const/4 v1, -0x1

    if-eq p2, v1, :cond_3

    .line 2257520
    :cond_2
    :goto_0
    return-void

    .line 2257521
    :cond_3
    iget-object v1, v0, LX/FZe;->e:LX/FZX;

    invoke-virtual {v1}, LX/FZX;->c()Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 2257522
    if-eqz v1, :cond_4

    const/16 p0, 0x6dc

    if-eq p1, p0, :cond_5

    .line 2257523
    :cond_4
    if-eqz v1, :cond_6

    instance-of p0, v1, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;

    if-eqz p0, :cond_6

    const/16 p0, 0x2714

    if-ne p1, p0, :cond_6

    const/4 p0, 0x1

    :goto_1
    move p0, p0

    .line 2257524
    if-eqz p0, :cond_2

    .line 2257525
    :cond_5
    invoke-virtual {v1, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    :cond_6
    const/4 p0, 0x0

    goto :goto_1
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 2257505
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 2257506
    instance-of v0, p1, LX/FZU;

    if-eqz v0, :cond_0

    .line 2257507
    check-cast p1, LX/FZU;

    iget-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->s:LX/FZb;

    invoke-interface {p1, v0}, LX/FZU;->a(LX/FZb;)V

    .line 2257508
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, 0x21e31da4

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2257497
    invoke-direct {p0}, Lcom/facebook/search/fragment/GraphSearchFragment;->m()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v2

    .line 2257498
    invoke-static {v2}, Lcom/facebook/search/api/GraphSearchQuery;->a(Lcom/facebook/search/api/GraphSearchQuery;)LX/7BH;

    move-result-object v0

    .line 2257499
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, LX/8i2;->a(Landroid/content/Context;LX/7BH;)Landroid/view/ContextThemeWrapper;

    move-result-object v0

    .line 2257500
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030691

    const/4 v5, 0x0

    invoke-virtual {v3, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 2257501
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v4, 0x7f0307f0

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    iput-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->o:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    .line 2257502
    iget-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->b:LX/FiU;

    iget-object v4, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->o:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-direct {p0}, Lcom/facebook/search/fragment/GraphSearchFragment;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, LX/FiU;->a(Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;Ljava/lang/String;)V

    .line 2257503
    iget-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->g:LX/EQ9;

    invoke-virtual {v0, v2}, LX/EQ9;->a(Lcom/facebook/search/api/GraphSearchQuery;)LX/2SZ;

    move-result-object v0

    invoke-interface {v0}, LX/2SZ;->d()V

    .line 2257504
    const/16 v0, 0x2b

    const v2, -0x51693870

    invoke-static {v6, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v3
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6c33dd2c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2257493
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2257494
    iget-object v1, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->i:LX/FZS;

    .line 2257495
    iget-object v2, v1, LX/FZS;->a:LX/2do;

    iget-object p0, v1, LX/FZS;->b:LX/FZR;

    invoke-virtual {v2, p0}, LX/0b4;->b(LX/0b2;)Z

    .line 2257496
    const/16 v1, 0x2b

    const v2, -0x2f9ad782

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6b137922

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2257485
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2257486
    iget-object v1, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->c:LX/FgR;

    .line 2257487
    iget-object v2, v1, LX/FgR;->a:Lcom/facebook/ui/search/SearchEditText;

    move-object v1, v2

    .line 2257488
    if-eqz v1, :cond_0

    .line 2257489
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setTextInteractionListener(LX/633;)V

    .line 2257490
    iget-object v2, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->r:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Lcom/facebook/ui/search/SearchEditText;->b(Landroid/view/View$OnTouchListener;)V

    .line 2257491
    :cond_0
    iget-object v1, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->b:LX/FiU;

    invoke-virtual {v1}, LX/FiU;->b()V

    .line 2257492
    const/16 v1, 0x2b

    const v2, 0xfcd1370

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xe4731cc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2257481
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2257482
    iget-object v1, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->b:LX/FiU;

    invoke-virtual {v1}, LX/FiU;->a()V

    .line 2257483
    iget-object v1, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->h:LX/Cvs;

    invoke-interface {v1}, LX/Cvs;->g()V

    .line 2257484
    const/16 v1, 0x2b

    const v2, -0x469eed71

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x8f38400

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2257478
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2257479
    iget-object v1, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->h:LX/Cvs;

    invoke-interface {v1}, LX/Cvs;->f()V

    .line 2257480
    const/16 v1, 0x2b

    const v2, 0x46cf141e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2257472
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2257473
    iget-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->q:LX/FZX;

    if-eqz v0, :cond_0

    .line 2257474
    const-string v0, "facebook:graphsearch:current_fragment_tag"

    iget-object v1, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->q:LX/FZX;

    invoke-virtual {v1}, LX/FZX;->c()Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 2257475
    iget-object p0, v1, Landroid/support/v4/app/Fragment;->mTag:Ljava/lang/String;

    move-object v1, p0

    .line 2257476
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2257477
    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, -0x189fd939

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2257435
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2257436
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    move-object v1, v1

    .line 2257437
    if-eqz v1, :cond_0

    .line 2257438
    iget-object v2, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->b:LX/FiU;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "search_titles_app_diable_animation"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-direct {p0}, Lcom/facebook/search/fragment/GraphSearchFragment;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v1, v4}, LX/FiU;->a(ZLX/1ZF;Ljava/lang/String;)V

    .line 2257439
    :cond_0
    iget-object v1, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->f:LX/Fi6;

    invoke-direct {p0}, Lcom/facebook/search/fragment/GraphSearchFragment;->m()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Fi6;->a(Lcom/facebook/search/api/GraphSearchQuery;)LX/2SP;

    move-result-object v1

    sget-object v2, Lcom/facebook/search/fragment/GraphSearchFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    sget-object v3, LX/EPu;->MEMORY:LX/EPu;

    invoke-virtual {v1, v2, v3}, LX/2SP;->a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V

    .line 2257440
    const/16 v1, 0x2b

    const v2, -0x3a7003a9

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 2257441
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2257442
    :try_start_0
    const-string v0, "GraphSearchFragment.onViewCreated"

    const v1, 0x5dc56026

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2257443
    iget-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->p:LX/FZe;

    .line 2257444
    iget-boolean v2, v0, LX/FZe;->C:Z

    if-eqz v2, :cond_0

    .line 2257445
    const/4 v7, 0x0

    .line 2257446
    iget-boolean v3, v0, LX/FZe;->C:Z

    invoke-static {v3}, LX/0PB;->checkState(Z)V

    .line 2257447
    iget-object v3, v0, LX/FZe;->x:LX/CwB;

    if-eqz v3, :cond_1

    .line 2257448
    iget-object v4, v0, LX/FZe;->x:LX/CwB;

    iget-object v5, v0, LX/FZe;->z:LX/8ci;

    iget-object v6, v0, LX/FZe;->w:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    const/4 v8, 0x1

    move-object v3, v0

    invoke-static/range {v3 .. v8}, LX/FZe;->a(LX/FZe;LX/CwB;LX/8ci;Lcom/facebook/search/logging/api/SearchTypeaheadSession;ZZ)V

    .line 2257449
    :cond_0
    :goto_0
    const/4 v2, 0x0

    iput-boolean v2, v0, LX/FZe;->C:Z

    .line 2257450
    iget-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->c:LX/FgR;

    .line 2257451
    iget-object v1, v0, LX/FgR;->a:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 2257452
    new-instance v1, LX/FZZ;

    invoke-direct {v1, p0}, LX/FZZ;-><init>(Lcom/facebook/search/fragment/GraphSearchFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setTextInteractionListener(LX/633;)V

    .line 2257453
    new-instance v1, LX/FZa;

    invoke-direct {v1, p0, v0}, LX/FZa;-><init>(Lcom/facebook/search/fragment/GraphSearchFragment;Lcom/facebook/ui/search/SearchEditText;)V

    iput-object v1, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->r:Landroid/view/View$OnTouchListener;

    .line 2257454
    iget-object v1, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->r:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->a(Landroid/view/View$OnTouchListener;)V

    .line 2257455
    iget-object v0, p0, Lcom/facebook/search/fragment/GraphSearchFragment;->h:LX/Cvs;

    invoke-interface {v0}, LX/Cvs;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2257456
    const v0, -0x1ea92a62

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2257457
    return-void

    .line 2257458
    :catchall_0
    move-exception v0

    const v1, 0x3175dc0

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2257459
    :cond_1
    iget-object v3, v0, LX/FZe;->s:LX/0Uh;

    sget v4, LX/2SU;->q:I

    invoke-virtual {v3, v4, v7}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v0, LX/FZe;->A:Lcom/facebook/search/model/awareness/AwarenessGraphSearchConfiguration;

    if-eqz v3, :cond_2

    .line 2257460
    iget-object v3, v0, LX/FZe;->d:LX/FZW;

    invoke-virtual {v3}, LX/FZW;->b()Lcom/facebook/search/suggestions/AwarenessNullStateFragment;

    move-result-object v3

    .line 2257461
    iget-object v4, v0, LX/FZe;->e:LX/FZX;

    invoke-virtual {v4, v3}, LX/FZX;->b(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 2257462
    :cond_2
    iget-object v3, v0, LX/FZe;->d:LX/FZW;

    iget-object v4, v0, LX/FZe;->y:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v3, v4}, LX/FZW;->a(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/facebook/search/suggestions/SuggestionsFragment;

    move-result-object v3

    .line 2257463
    iget-object v4, v0, LX/FZe;->e:LX/FZX;

    invoke-virtual {v4, v3}, LX/FZX;->b(Landroid/support/v4/app/Fragment;)V

    .line 2257464
    iget-object v4, v0, LX/FZe;->y:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2257465
    invoke-virtual {v3, v4}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a(Lcom/facebook/search/api/GraphSearchQuery;)V

    .line 2257466
    iget-object v5, v3, Lcom/facebook/search/suggestions/SuggestionsFragment;->m:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Cvp;

    .line 2257467
    iget-object v6, v3, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2257468
    iget-object v4, v6, LX/7B6;->b:Ljava/lang/String;

    move-object v6, v4

    .line 2257469
    iput-object v6, v5, LX/Cvp;->m:Ljava/lang/String;

    .line 2257470
    iget-object v6, v3, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v6, v5}, LX/Fgb;->a(LX/Cvp;)V

    .line 2257471
    goto :goto_0
.end method
