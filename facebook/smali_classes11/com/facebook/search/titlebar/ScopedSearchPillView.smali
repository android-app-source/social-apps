.class public Lcom/facebook/search/titlebar/ScopedSearchPillView;
.super Lcom/facebook/widget/text/BetterTextView;
.source ""


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2275477
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/search/titlebar/ScopedSearchPillView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2275478
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2275461
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/search/titlebar/ScopedSearchPillView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2275462
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 2275467
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2275468
    sget-object v0, LX/03r;->ScopedSearchPillView:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2275469
    const/16 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/facebook/search/titlebar/ScopedSearchPillView;->setEnabled(Z)V

    .line 2275470
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2275471
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2275472
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-gt v0, v1, :cond_1

    .line 2275473
    invoke-virtual {p0}, Lcom/facebook/search/titlebar/ScopedSearchPillView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p0, Lcom/facebook/search/titlebar/ScopedSearchPillView;->a:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0217b4

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/search/titlebar/ScopedSearchPillView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2275474
    :goto_1
    return-void

    .line 2275475
    :cond_0
    const v0, 0x7f0217b3

    goto :goto_0

    .line 2275476
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/search/titlebar/ScopedSearchPillView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p0, Lcom/facebook/search/titlebar/ScopedSearchPillView;->a:Z

    if-eqz v0, :cond_2

    const v0, 0x7f0217b4

    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/search/titlebar/ScopedSearchPillView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_2
    const v0, 0x7f0217b3

    goto :goto_2
.end method


# virtual methods
.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 2275466
    iget-boolean v0, p0, Lcom/facebook/search/titlebar/ScopedSearchPillView;->a:Z

    return v0
.end method

.method public setEnabled(Z)V
    .locals 0

    .prologue
    .line 2275463
    iput-boolean p1, p0, Lcom/facebook/search/titlebar/ScopedSearchPillView;->a:Z

    .line 2275464
    invoke-direct {p0}, Lcom/facebook/search/titlebar/ScopedSearchPillView;->a()V

    .line 2275465
    return-void
.end method
