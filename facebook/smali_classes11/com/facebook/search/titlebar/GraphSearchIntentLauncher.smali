.class public Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/facebook/content/SecureContextHelper;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2SV;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Cvs;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2275341
    const-class v0, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;

    const-string v1, "search"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2SV;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Cvs;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2275334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2275335
    iput-object p1, p0, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->b:Landroid/content/Context;

    .line 2275336
    iput-object p2, p0, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2275337
    iput-object p3, p0, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->d:LX/0Or;

    .line 2275338
    iput-object p4, p0, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->e:LX/0Or;

    .line 2275339
    iput-object p5, p0, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->f:LX/0Or;

    .line 2275340
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;
    .locals 9

    .prologue
    .line 2275313
    const-class v1, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;

    monitor-enter v1

    .line 2275314
    :try_start_0
    sget-object v0, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2275315
    sput-object v2, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2275316
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2275317
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2275318
    new-instance v3, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    const/16 v6, 0xc

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x11b2

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x32da

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/0Or;LX/0Or;)V

    .line 2275319
    move-object v0, v3

    .line 2275320
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2275321
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2275322
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2275323
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/search/api/GraphSearchQuery;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2275342
    if-eqz p1, :cond_0

    .line 2275343
    const-string v0, "initial_typeahead_query"

    invoke-virtual {p2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2275344
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cvs;

    invoke-interface {v0}, LX/Cvs;->b()V

    .line 2275345
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SV;

    sget-object v1, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->a:Lcom/facebook/common/callercontext/CallerContext;

    sget-object v2, LX/EPu;->MEMORY:LX/EPu;

    invoke-virtual {v0, v1, v2}, LX/2SP;->a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V

    .line 2275346
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->b:Landroid/content/Context;

    invoke-interface {v0, p2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2275347
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 1

    .prologue
    .line 2275332
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->a(Lcom/facebook/search/api/GraphSearchQuery;Landroid/os/Bundle;)V

    .line 2275333
    return-void
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2275327
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->SEARCH_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 2275328
    if-eqz p2, :cond_0

    .line 2275329
    invoke-virtual {v0, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2275330
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->a(Lcom/facebook/search/api/GraphSearchQuery;Landroid/content/Intent;)V

    .line 2275331
    return-void
.end method

.method public final b(Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 3

    .prologue
    .line 2275324
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->SEARCH_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 2275325
    invoke-direct {p0, p1, v0}, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->a(Lcom/facebook/search/api/GraphSearchQuery;Landroid/content/Intent;)V

    .line 2275326
    return-void
.end method
