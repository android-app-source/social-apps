.class public Lcom/facebook/search/results/fragment/SearchResultsFragment;
.super Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/FcC;
.implements LX/FdZ;
.implements LX/Fe2;
.implements LX/1DI;


# static fields
.field private static final H:LX/1PT;

.field public static final I:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;


# instance fields
.field public A:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0x9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/Cve;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CyY;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/FcV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/8ht;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cyz;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/CvM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private K:Ljava/lang/String;

.field public L:LX/CzA;

.field private M:LX/Fe8;

.field public N:LX/1Qq;

.field private O:LX/CvZ;

.field public P:LX/CyE;

.field private Q:LX/Fje;

.field public R:LX/EQG;

.field public S:LX/0g8;

.field public T:LX/195;

.field public U:LX/Cyn;

.field private V:Ljava/lang/String;

.field public W:Z

.field public X:Z

.field public Y:Z

.field public Z:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;"
        }
    .end annotation
.end field

.field public aa:LX/5uu;

.field private ab:Z

.field public ac:Z

.field public ad:Z

.field public ae:I

.field public i:LX/193;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/2Sc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/1DS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Db;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/Fe9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/Cxm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/Cyo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/FgI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/CvY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/CyZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/BwE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/1k3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/Cva;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FcD;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fcs;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2264940
    new-instance v0, LX/Fdw;

    invoke-direct {v0}, LX/Fdw;-><init>()V

    sput-object v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->H:LX/1PT;

    .line 2264941
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->LEARNING_NUX_SERP_SUCCESS:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    sput-object v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->I:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2264942
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;-><init>()V

    .line 2264943
    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->W:Z

    .line 2264944
    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->X:Z

    .line 2264945
    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Y:Z

    .line 2264946
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2264947
    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Z:LX/0Px;

    .line 2264948
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->aa:LX/5uu;

    .line 2264949
    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ab:Z

    .line 2264950
    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ac:Z

    .line 2264951
    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ad:Z

    .line 2264952
    iput v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ae:I

    return-void
.end method

.method private B()LX/Fdy;
    .locals 1

    .prologue
    .line 2264953
    new-instance v0, LX/Fdy;

    invoke-direct {v0, p0}, LX/Fdy;-><init>(Lcom/facebook/search/results/fragment/SearchResultsFragment;)V

    return-object v0
.end method

.method public static C(Lcom/facebook/search/results/fragment/SearchResultsFragment;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2264954
    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ad:Z

    .line 2264955
    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ac:Z

    .line 2264956
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    invoke-virtual {v0}, LX/CzA;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/EQG;->ERROR:LX/EQG;

    .line 2264957
    :goto_0
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->a$redex0(Lcom/facebook/search/results/fragment/SearchResultsFragment;LX/EQG;Z)V

    .line 2264958
    return-void

    .line 2264959
    :cond_0
    sget-object v0, LX/EQG;->ERROR_LOADING_MORE:LX/EQG;

    goto :goto_0
.end method

.method public static D(Lcom/facebook/search/results/fragment/SearchResultsFragment;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2264960
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->U:LX/Cyn;

    invoke-virtual {v0}, LX/Cyn;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2264961
    :cond_0
    :goto_0
    return-void

    .line 2264962
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ad:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ac:Z

    if-eqz v0, :cond_0

    .line 2264963
    iput-boolean v2, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ad:Z

    .line 2264964
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->C:LX/Cve;

    .line 2264965
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2264966
    invoke-virtual {v0, v1}, LX/Cve;->a(LX/CwB;)V

    .line 2264967
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v0, v0

    .line 2264968
    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2264969
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    invoke-virtual {v0}, LX/CzA;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/EQG;->LOADING:LX/EQG;

    .line 2264970
    :goto_1
    invoke-static {p0, v0, v2}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->a$redex0(Lcom/facebook/search/results/fragment/SearchResultsFragment;LX/EQG;Z)V

    .line 2264971
    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->W:Z

    if-eqz v0, :cond_3

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    invoke-virtual {v0}, LX/FcD;->g()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Z:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2264972
    :goto_2
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2264973
    invoke-direct {p0, v0, v1}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->a(LX/0Px;LX/0Px;)V

    goto :goto_0

    .line 2264974
    :cond_2
    sget-object v0, LX/EQG;->LOADING_MORE:LX/EQG;

    goto :goto_1

    .line 2264975
    :cond_3
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Z:LX/0Px;

    goto :goto_2
.end method

.method private E()LX/0fx;
    .locals 1

    .prologue
    .line 2264976
    new-instance v0, LX/Fdz;

    invoke-direct {v0, p0}, LX/Fdz;-><init>(Lcom/facebook/search/results/fragment/SearchResultsFragment;)V

    return-object v0
.end method

.method private F()LX/0fu;
    .locals 1

    .prologue
    .line 2264918
    new-instance v0, LX/Fe0;

    invoke-direct {v0, p0}, LX/Fe0;-><init>(Lcom/facebook/search/results/fragment/SearchResultsFragment;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/search/results/fragment/SearchResultsFragment;
    .locals 1

    .prologue
    .line 2264977
    new-instance v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;

    invoke-direct {v0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;-><init>()V

    .line 2264978
    iput-object p0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->K:Ljava/lang/String;

    .line 2264979
    return-object v0
.end method

.method private a(LX/0Px;LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2264980
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->U:LX/Cyn;

    .line 2264981
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2264982
    invoke-virtual {v0, v1, p1, p2}, LX/Cyn;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0Px;LX/0Px;)V

    .line 2264983
    return-void
.end method

.method private static a(Lcom/facebook/search/results/fragment/SearchResultsFragment;LX/193;LX/2Sc;LX/1DS;LX/0Or;LX/Fe9;LX/Cxm;LX/Cyo;LX/FgI;LX/CvY;LX/CyZ;LX/0Ot;LX/BwE;LX/1k3;LX/Cva;LX/0Ot;LX/0Ot;LX/0ad;LX/0Ot;LX/0Ot;Landroid/content/Context;LX/Cve;LX/CvM;LX/0Ot;LX/FcV;LX/8ht;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/fragment/SearchResultsFragment;",
            "LX/193;",
            "LX/2Sc;",
            "LX/1DS;",
            "LX/0Or",
            "<",
            "LX/1Db;",
            ">;",
            "LX/Fe9;",
            "LX/Cxm;",
            "LX/Cyo;",
            "LX/FgI;",
            "LX/CvY;",
            "LX/CyZ;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;",
            ">;",
            "LX/BwE;",
            "LX/1k3;",
            "LX/Cva;",
            "LX/0Ot",
            "<",
            "LX/FcD;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/Fcs;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0x9;",
            ">;",
            "Landroid/content/Context;",
            "LX/Cve;",
            "LX/CvM;",
            "LX/0Ot",
            "<",
            "LX/CyY;",
            ">;",
            "LX/FcV;",
            "LX/8ht;",
            "LX/0Ot",
            "<",
            "LX/Cyz;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2264984
    iput-object p1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->i:LX/193;

    iput-object p2, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->j:LX/2Sc;

    iput-object p3, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->k:LX/1DS;

    iput-object p4, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->l:LX/0Or;

    iput-object p5, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->m:LX/Fe9;

    iput-object p6, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->n:LX/Cxm;

    iput-object p7, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->o:LX/Cyo;

    iput-object p8, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->p:LX/FgI;

    iput-object p9, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->q:LX/CvY;

    iput-object p10, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->r:LX/CyZ;

    iput-object p11, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->s:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->t:LX/BwE;

    iput-object p13, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->u:LX/1k3;

    iput-object p14, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->v:LX/Cva;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w:LX/0Ot;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->x:LX/0Ot;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->y:LX/0ad;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->z:LX/0Ot;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->A:LX/0Ot;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->B:Landroid/content/Context;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->C:LX/Cve;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->J:LX/CvM;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->D:LX/0Ot;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->E:LX/FcV;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->F:LX/8ht;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->G:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 30

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v28

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/search/results/fragment/SearchResultsFragment;

    const-class v3, LX/193;

    move-object/from16 v0, v28

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/193;

    invoke-static/range {v28 .. v28}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v4

    check-cast v4, LX/2Sc;

    invoke-static/range {v28 .. v28}, LX/1DS;->a(LX/0QB;)LX/1DS;

    move-result-object v5

    check-cast v5, LX/1DS;

    const/16 v6, 0x6c9

    move-object/from16 v0, v28

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static/range {v28 .. v28}, LX/Fe9;->a(LX/0QB;)LX/Fe9;

    move-result-object v7

    check-cast v7, LX/Fe9;

    const-class v8, LX/Cxm;

    move-object/from16 v0, v28

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/Cxm;

    invoke-static/range {v28 .. v28}, LX/Cyo;->a(LX/0QB;)LX/Cyo;

    move-result-object v9

    check-cast v9, LX/Cyo;

    invoke-static/range {v28 .. v28}, LX/FgI;->a(LX/0QB;)LX/FgI;

    move-result-object v10

    check-cast v10, LX/FgI;

    invoke-static/range {v28 .. v28}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v11

    check-cast v11, LX/CvY;

    invoke-static/range {v28 .. v28}, LX/CyZ;->a(LX/0QB;)LX/CyZ;

    move-result-object v12

    check-cast v12, LX/CyZ;

    const/16 v13, 0x3376

    move-object/from16 v0, v28

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const-class v14, LX/BwE;

    move-object/from16 v0, v28

    invoke-interface {v0, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/BwE;

    invoke-static/range {v28 .. v28}, LX/1k3;->a(LX/0QB;)LX/1k3;

    move-result-object v15

    check-cast v15, LX/1k3;

    const-class v16, LX/Cva;

    move-object/from16 v0, v28

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/Cva;

    const/16 v17, 0x3331

    move-object/from16 v0, v28

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x271

    move-object/from16 v0, v28

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    invoke-static/range {v28 .. v28}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v19

    check-cast v19, LX/0ad;

    const/16 v20, 0x333c

    move-object/from16 v0, v28

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x1147

    move-object/from16 v0, v28

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const-class v22, Landroid/content/Context;

    move-object/from16 v0, v28

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/content/Context;

    invoke-static/range {v28 .. v28}, LX/Cve;->a(LX/0QB;)LX/Cve;

    move-result-object v23

    check-cast v23, LX/Cve;

    invoke-static/range {v28 .. v28}, LX/CvM;->a(LX/0QB;)LX/CvM;

    move-result-object v24

    check-cast v24, LX/CvM;

    const/16 v25, 0x335f

    move-object/from16 v0, v28

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v25

    invoke-static/range {v28 .. v28}, LX/FcV;->a(LX/0QB;)LX/FcV;

    move-result-object v26

    check-cast v26, LX/FcV;

    invoke-static/range {v28 .. v28}, LX/8ht;->a(LX/0QB;)LX/8ht;

    move-result-object v27

    check-cast v27, LX/8ht;

    const/16 v29, 0x336f

    invoke-static/range {v28 .. v29}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v28

    invoke-static/range {v2 .. v28}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->a(Lcom/facebook/search/results/fragment/SearchResultsFragment;LX/193;LX/2Sc;LX/1DS;LX/0Or;LX/Fe9;LX/Cxm;LX/Cyo;LX/FgI;LX/CvY;LX/CyZ;LX/0Ot;LX/BwE;LX/1k3;LX/Cva;LX/0Ot;LX/0Ot;LX/0ad;LX/0Ot;LX/0Ot;Landroid/content/Context;LX/Cve;LX/CvM;LX/0Ot;LX/FcV;LX/8ht;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/search/results/fragment/SearchResultsFragment;LX/EQG;Z)V
    .locals 1

    .prologue
    .line 2264985
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Q:LX/Fje;

    if-eqz v0, :cond_0

    .line 2264986
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Q:LX/Fje;

    invoke-virtual {v0, p1, p2}, LX/Fje;->a(LX/EQG;Z)V

    .line 2264987
    :cond_0
    iput-object p1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->R:LX/EQG;

    .line 2264988
    return-void
.end method

.method private static c(Lcom/facebook/search/results/fragment/SearchResultsFragment;LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/high16 v4, -0x1000000

    .line 2264989
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2264990
    :cond_0
    invoke-static {p0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->y(Lcom/facebook/search/results/fragment/SearchResultsFragment;)V

    .line 2264991
    :goto_0
    return-void

    .line 2264992
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->aa:LX/5uu;

    if-nez v0, :cond_2

    .line 2264993
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Q:LX/Fje;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->B:Landroid/content/Context;

    invoke-static {v1, p1}, LX/Fdj;->a(Landroid/content/Context;LX/0Px;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v2, v1, v4}, LX/Fje;->a(IILjava/lang/String;I)V

    goto :goto_0

    .line 2264994
    :cond_2
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Q:LX/Fje;

    sget-object v0, LX/Fdj;->a:LX/0P1;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->aa:LX/5uu;

    invoke-interface {v2}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const v2, -0xbd984e

    iget-object v3, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->B:Landroid/content/Context;

    invoke-static {v3, p1}, LX/Fdj;->a(Landroid/content/Context;LX/0Px;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3, v4}, LX/Fje;->a(IILjava/lang/String;I)V

    goto :goto_0
.end method

.method private u()V
    .locals 5

    .prologue
    .line 2264995
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    .line 2264996
    iget-object v2, v0, LX/FcD;->i:LX/0Px;

    move-object v0, v2

    .line 2264997
    new-instance v2, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;

    invoke-direct {v2}, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;-><init>()V

    .line 2264998
    const/4 v3, 0x2

    const v4, 0x7f0e0895

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 2264999
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2265000
    const-string v4, "main_filter_list"

    invoke-static {v3, v4, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 2265001
    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2265002
    iput-object p0, v2, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->n:LX/FdZ;

    .line 2265003
    move-object v0, v2

    .line 2265004
    const-string v2, "general_filter_fragment"

    invoke-virtual {v1, v0, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2265005
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->q:LX/CvY;

    .line 2265006
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2265007
    invoke-virtual {v0, v1}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    .line 2265008
    return-void
.end method

.method private w()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2265009
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->q:LX/CvY;

    .line 2265010
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v2, v0

    .line 2265011
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    invoke-virtual {v0}, LX/FcD;->g()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0Px;)V

    .line 2265012
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    invoke-virtual {v0}, LX/CzA;->d()V

    .line 2265013
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->U:LX/Cyn;

    invoke-virtual {v0}, LX/Cyn;->h()V

    .line 2265014
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2265015
    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Z:LX/0Px;

    .line 2265016
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->N:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2265017
    iput-object v3, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->V:Ljava/lang/String;

    .line 2265018
    sget-object v0, LX/EQG;->LOADING:LX/EQG;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->a$redex0(Lcom/facebook/search/results/fragment/SearchResultsFragment;LX/EQG;Z)V

    .line 2265019
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v0, v0

    .line 2265020
    iget-boolean v1, v0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->y:Z

    if-nez v1, :cond_0

    .line 2265021
    :goto_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    invoke-virtual {v0}, LX/FcD;->g()LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->a(LX/0Px;LX/0Px;)V

    .line 2265022
    return-void

    .line 2265023
    :cond_0
    invoke-static {}, LX/7CN;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u:Ljava/lang/String;

    goto :goto_0
.end method

.method private x()LX/1PT;
    .locals 3

    .prologue
    .line 2265024
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v0, v0

    .line 2265025
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->F:LX/8ht;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v2

    .line 2265026
    iget-object p0, v0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    move-object v0, p0

    .line 2265027
    invoke-virtual {v1, v2, v0}, LX/8ht;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;LX/8ci;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2265028
    sget-object v0, LX/EIx;->a:LX/EIx;

    move-object v0, v0

    .line 2265029
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->H:LX/1PT;

    goto :goto_0
.end method

.method public static y(Lcom/facebook/search/results/fragment/SearchResultsFragment;)V
    .locals 5

    .prologue
    const v4, -0x6f6b64

    .line 2265030
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    .line 2265031
    iget-object v1, v0, LX/FcD;->i:LX/0Px;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/FcD;->i:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2265032
    if-eqz v0, :cond_0

    .line 2265033
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    .line 2265034
    iget-object v1, v0, LX/FcD;->i:LX/0Px;

    move-object v0, v1

    .line 2265035
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5uu;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->aa:LX/5uu;

    .line 2265036
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Q:LX/Fje;

    sget-object v0, LX/Fdj;->a:LX/0P1;

    const-string v2, "city"

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Add "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->aa:LX/5uu;

    invoke-interface {v3}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " filter"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v4, v2, v4}, LX/Fje;->a(IILjava/lang/String;I)V

    .line 2265037
    :goto_1
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->aa:LX/5uu;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->B:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0820ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2265038
    :goto_2
    iget-object v2, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Q:LX/Fje;

    sget-object v0, LX/Fdj;->a:LX/0P1;

    const-string v3, "city"

    invoke-virtual {v0, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0, v4, v1, v4}, LX/Fje;->a(IILjava/lang/String;I)V

    .line 2265039
    return-void

    .line 2265040
    :cond_0
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Q:LX/Fje;

    sget-object v0, LX/Fdj;->a:LX/0P1;

    const-string v2, "city"

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v2, "Add location filter"

    invoke-virtual {v1, v0, v4, v2, v4}, LX/Fje;->a(IILjava/lang/String;I)V

    goto :goto_1

    .line 2265041
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->aa:LX/5uu;

    invoke-interface {v0}, LX/5uu;->l()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method private z()V
    .locals 4

    .prologue
    .line 2264919
    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->W:Z

    if-nez v0, :cond_0

    .line 2264920
    :goto_0
    return-void

    .line 2264921
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    .line 2264922
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Q:LX/Fje;

    .line 2264923
    iput-object v1, v0, LX/FcD;->g:LX/Fje;

    .line 2264924
    iput-object p0, v0, LX/FcD;->k:LX/FcC;

    .line 2264925
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f031293

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/CustomFrameLayout;

    .line 2264926
    iget-object v2, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->E:LX/FcV;

    .line 2264927
    iget-object v3, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v3, v3

    .line 2264928
    invoke-virtual {v3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/FcV;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2264929
    const v2, 0x7f0d2b79

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2264930
    iget-boolean v2, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ab:Z

    if-eqz v2, :cond_1

    .line 2264931
    iget-object v2, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FcD;

    .line 2264932
    iget-object v3, v2, LX/FcD;->j:LX/0Px;

    move-object v2, v3

    .line 2264933
    invoke-static {p0, v2}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->c(Lcom/facebook/search/results/fragment/SearchResultsFragment;LX/0Px;)V

    .line 2264934
    :cond_1
    const v2, 0x7f0d2b7a

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2264935
    iput-object v2, v0, LX/FcD;->h:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2264936
    iget-object v2, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Q:LX/Fje;

    invoke-virtual {v2, v1}, LX/Fje;->a(Landroid/view/View;)V

    .line 2264937
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Q:LX/Fje;

    .line 2264938
    iput-object p0, v1, LX/Fje;->O:LX/Fe2;

    .line 2264939
    invoke-virtual {v0}, LX/FcD;->c()V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2264714
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->K:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2264715
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->K:Ljava/lang/String;

    .line 2264716
    :cond_0
    :goto_0
    return-object v0

    .line 2264717
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v0, v0

    .line 2264718
    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->jC_()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7CN;->a(LX/0Px;)Ljava/lang/String;

    move-result-object v0

    .line 2264719
    if-nez v0, :cond_0

    const-string v0, "unknown"

    goto :goto_0
.end method

.method public final a(LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2264720
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2264721
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    .line 2264722
    iget-object v1, v0, LX/FcD;->j:LX/0Px;

    move-object v0, v1

    .line 2264723
    invoke-virtual {p1, v0}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v1, v2

    .line 2264724
    :goto_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Z:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v2, :cond_5

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v2, :cond_5

    .line 2264725
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Z:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4FP;

    invoke-virtual {v0}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v4

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CyH;

    .line 2264726
    iget-object v5, v0, LX/CyH;->c:LX/4FP;

    move-object v0, v5

    .line 2264727
    invoke-virtual {v0}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    move v4, v0

    .line 2264728
    :goto_1
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Z:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v2, :cond_3

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    .line 2264729
    iget-object v5, v0, LX/FcD;->j:LX/0Px;

    move-object v0, v5

    .line 2264730
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2264731
    :cond_0
    :goto_2
    move v0, v2

    .line 2264732
    if-eqz v0, :cond_1

    .line 2264733
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    invoke-virtual {v0, p1}, LX/FcD;->b(LX/0Px;)V

    .line 2264734
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w()V

    .line 2264735
    :cond_1
    return-void

    :cond_2
    move v1, v3

    .line 2264736
    goto :goto_0

    .line 2264737
    :cond_3
    if-eqz v1, :cond_4

    if-eqz v4, :cond_0

    :cond_4
    move v2, v3

    goto :goto_2

    :cond_5
    move v4, v3

    goto :goto_1
.end method

.method public final a(LX/4FP;)V
    .locals 1

    .prologue
    .line 2264738
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    invoke-virtual {v0, p1}, LX/FcD;->a(LX/4FP;)LX/0Px;

    .line 2264739
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w()V

    .line 2264740
    return-void
.end method

.method public final a(LX/5uu;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5uu;",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2264741
    iput-object p1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->aa:LX/5uu;

    .line 2264742
    invoke-static {p0, p2}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->c(Lcom/facebook/search/results/fragment/SearchResultsFragment;LX/0Px;)V

    .line 2264743
    invoke-virtual {p0, p2}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->a(LX/0Px;)V

    .line 2264744
    return-void
.end method

.method public final a(LX/Fjd;)V
    .locals 5

    .prologue
    .line 2264745
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->E:LX/FcV;

    .line 2264746
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2264747
    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/FcV;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2264748
    sget-object v0, LX/Fe1;->a:[I

    invoke-virtual {p1}, LX/Fjd;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2264749
    :goto_0
    return-void

    .line 2264750
    :pswitch_0
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->u()V

    goto :goto_0

    .line 2264751
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->aa:LX/5uu;

    if-nez v0, :cond_0

    .line 2264752
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->u()V

    goto :goto_0

    .line 2264753
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    .line 2264754
    iget-object v1, v0, LX/FcD;->i:LX/0Px;

    move-object v2, v1

    .line 2264755
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5uu;

    .line 2264756
    invoke-interface {v0}, LX/5uu;->l()Ljava/lang/String;

    move-result-object v4

    iget-object p1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->aa:LX/5uu;

    invoke-interface {p1}, LX/5uu;->l()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2264757
    :goto_2
    move-object v0, v0

    .line 2264758
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    invoke-static {v0, p0}, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->a(LX/5uu;LX/FdZ;)Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;

    move-result-object v2

    const-string v3, "FILTER_FRAGMENT_TAG"

    invoke-virtual {v1, v2, v3}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 2264759
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->q:LX/CvY;

    .line 2264760
    iget-object v2, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v2, v2

    .line 2264761
    invoke-virtual {v1, v2}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    .line 2264762
    goto :goto_0

    .line 2264763
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->c()V

    goto :goto_0

    .line 2264764
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2264765
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2264766
    invoke-super {p0, p1}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->a(Landroid/os/Bundle;)V

    .line 2264767
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2264768
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v0, v0

    .line 2264769
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->g:LX/0Uh;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->y:LX/0ad;

    .line 2264770
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2264771
    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a(LX/0Uh;LX/0ad;Landroid/os/Bundle;)V

    .line 2264772
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->m:LX/Fe9;

    .line 2264773
    new-instance v1, LX/Fe8;

    iget-object v2, v0, LX/Fe9;->a:LX/FeD;

    invoke-direct {v1, v2}, LX/Fe8;-><init>(LX/FeD;)V

    .line 2264774
    iget-object v2, v1, LX/Fe8;->a:LX/FeD;

    invoke-virtual {v2}, LX/0hD;->kw_()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v1, LX/Fe8;->a:LX/FeD;

    :goto_0
    iput-object v2, v1, LX/Fe8;->a:LX/FeD;

    .line 2264775
    move-object v0, v1

    .line 2264776
    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->M:LX/Fe8;

    .line 2264777
    new-instance v0, LX/CzA;

    invoke-direct {v0}, LX/CzA;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    .line 2264778
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->v:LX/Cva;

    .line 2264779
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2264780
    iget-object v2, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    .line 2264781
    new-instance p1, LX/CvZ;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v4

    check-cast v4, LX/CvY;

    invoke-direct {p1, v3, v4, v1, v2}, LX/CvZ;-><init>(LX/0SG;LX/CvY;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzA;)V

    .line 2264782
    move-object v0, p1

    .line 2264783
    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->O:LX/CvZ;

    .line 2264784
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->i:LX/193;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "search_results_scroll_perf"

    invoke-virtual {v0, v1, v2}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->T:LX/195;

    .line 2264785
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->u:LX/1k3;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->O:LX/CvZ;

    invoke-virtual {v0, v1}, LX/1Kt;->a(LX/1Ce;)V

    .line 2264786
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->u:LX/1k3;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->J:LX/CvM;

    invoke-virtual {v0, v1}, LX/1Kt;->a(LX/1Ce;)V

    .line 2264787
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->u:LX/1k3;

    new-instance v1, LX/Fdx;

    invoke-direct {v1, p0}, LX/Fdx;-><init>(Lcom/facebook/search/results/fragment/SearchResultsFragment;)V

    invoke-virtual {v0, v1}, LX/1Kt;->a(LX/1Ce;)V

    .line 2264788
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->J:LX/CvM;

    .line 2264789
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2264790
    iput-object v1, v0, LX/CvM;->d:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2264791
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2264792
    if-eqz v0, :cond_0

    .line 2264793
    const-string v1, "open_filter_dialog"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Y:Z

    .line 2264794
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2264795
    invoke-static {v0, v1}, LX/FcD;->a(Landroid/os/Bundle;LX/CwB;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Z:LX/0Px;

    .line 2264796
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->E:LX/FcV;

    .line 2264797
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2264798
    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v1

    .line 2264799
    iget-object v2, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v2, v2

    .line 2264800
    invoke-virtual {v2}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Z:LX/0Px;

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2264801
    if-nez v1, :cond_3

    .line 2264802
    :cond_1
    :goto_1
    move v0, v4

    .line 2264803
    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->W:Z

    .line 2264804
    return-void

    .line 2264805
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2264806
    :cond_3
    sget-object v6, LX/FcU;->a:[I

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ordinal()I

    move-result p1

    aget v6, v6, p1

    packed-switch v6, :pswitch_data_0

    goto :goto_1

    .line 2264807
    :pswitch_0
    iget-object v5, v0, LX/FcV;->c:LX/0ad;

    sget-short v6, LX/100;->F:S

    invoke-interface {v5, v6, v4}, LX/0ad;->a(SZ)Z

    move-result v4

    goto :goto_1

    .line 2264808
    :pswitch_1
    iget-object v5, v0, LX/FcV;->c:LX/0ad;

    sget-short v6, LX/100;->aG:S

    invoke-interface {v5, v6, v4}, LX/0ad;->a(SZ)Z

    move-result v4

    goto :goto_1

    .line 2264809
    :pswitch_2
    iget-object v5, v0, LX/FcV;->c:LX/0ad;

    sget-short v6, LX/100;->aH:S

    invoke-interface {v5, v6, v4}, LX/0ad;->a(SZ)Z

    move-result v4

    goto :goto_1

    .line 2264810
    :pswitch_3
    invoke-static {v2}, LX/7BG;->c(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    move v4, v5

    .line 2264811
    goto :goto_1

    .line 2264812
    :cond_4
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, v0, LX/FcV;->c:LX/0ad;

    sget-short p1, LX/100;->aG:S

    invoke-interface {v6, p1, v4}, LX/0ad;->a(SZ)Z

    move-result v6

    if-eqz v6, :cond_1

    move v4, v5

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2264813
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    .line 2264814
    iget-object v2, v0, LX/FcD;->i:LX/0Px;

    move-object v0, v2

    .line 2264815
    invoke-static {v0, p0}, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->a(LX/0Px;LX/FdZ;)Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    move-result-object v0

    const-string v2, "FILTER_FRAGMENT_TAG"

    invoke-virtual {v1, v0, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2264816
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->q:LX/CvY;

    .line 2264817
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2264818
    invoke-virtual {v0, v1}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    .line 2264819
    return-void
.end method

.method public final k()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2264820
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->U:LX/Cyn;

    .line 2264821
    iget-object v1, v0, LX/Cyn;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Cyf;

    invoke-virtual {v1}, LX/Cyf;->b()Z

    move-result v1

    move v0, v1

    .line 2264822
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->U:LX/Cyn;

    .line 2264823
    iget-object v1, v0, LX/Cyn;->q:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2264824
    if-eqz v0, :cond_0

    .line 2264825
    :goto_1
    return-void

    .line 2264826
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    invoke-virtual {v0}, LX/CzA;->d()V

    .line 2264827
    iput v2, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ae:I

    .line 2264828
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Q:LX/Fje;

    .line 2264829
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2264830
    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Fje;->setTextViewQueryString(Ljava/lang/String;)V

    .line 2264831
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->S:LX/0g8;

    invoke-interface {v0, v2}, LX/0g8;->g(I)V

    .line 2264832
    sget-object v0, LX/EQG;->LOADING:LX/EQG;

    invoke-static {p0, v0, v2}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->a$redex0(Lcom/facebook/search/results/fragment/SearchResultsFragment;LX/EQG;Z)V

    .line 2264833
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Z:LX/0Px;

    .line 2264834
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2264835
    const-string v2, "preloaded_story_ids"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 2264836
    const-string v3, "preloaded_story_ids"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 2264837
    if-nez v2, :cond_2

    const/4 v1, 0x0

    :goto_2
    move-object v1, v1

    .line 2264838
    invoke-direct {p0, v0, v1}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->a(LX/0Px;LX/0Px;)V

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    invoke-static {v2}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    goto :goto_2
.end method

.method public final kS_()V
    .locals 3

    .prologue
    .line 2264706
    iget v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ae:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ae:I

    .line 2264707
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->U:LX/Cyn;

    .line 2264708
    iget-object v1, v0, LX/Cyn;->o:LX/Cyd;

    sget-object v2, LX/Cyd;->STREAMING:LX/Cyd;

    if-ne v1, v2, :cond_0

    .line 2264709
    sget-object v1, LX/Cyc;->NOT_SENT:LX/Cyc;

    iput-object v1, v0, LX/Cyn;->m:LX/Cyc;

    .line 2264710
    :goto_0
    iget-object v1, v0, LX/Cyn;->o:LX/Cyd;

    iput-object v1, v0, LX/Cyn;->p:LX/Cyd;

    .line 2264711
    invoke-static {p0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->D(Lcom/facebook/search/results/fragment/SearchResultsFragment;)V

    .line 2264712
    return-void

    .line 2264713
    :cond_0
    sget-object v1, LX/Cyc;->LOADED_WITH_NEXT:LX/Cyc;

    iput-object v1, v0, LX/Cyn;->m:LX/Cyc;

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2264839
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    invoke-virtual {v0}, LX/CzA;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()V
    .locals 3

    .prologue
    .line 2264840
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->n()V

    .line 2264841
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->U:LX/Cyn;

    .line 2264842
    :goto_0
    iget-object v1, v0, LX/Cyn;->q:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v0, LX/Cyn;->s:LX/Fdy;

    if-eqz v1, :cond_2

    .line 2264843
    iget-object v1, v0, LX/Cyn;->q:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Cym;

    .line 2264844
    iget-object v2, v0, LX/Cyn;->i:LX/CyU;

    if-eqz v2, :cond_1

    iget-object v2, v0, LX/Cyn;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Cyf;

    invoke-virtual {v2}, LX/Cyf;->c()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, LX/Cym;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/Cyn;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2264845
    :cond_0
    iget-object v2, v0, LX/Cyn;->i:LX/CyU;

    invoke-virtual {v1}, LX/Cym;->b()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0, v1}, LX/CyU;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 2264846
    :cond_1
    invoke-static {v0, v1}, LX/Cyn;->a$redex0(LX/Cyn;LX/Cym;)V

    goto :goto_0

    .line 2264847
    :cond_2
    iget-boolean v1, v0, LX/Cyn;->r:Z

    if-eqz v1, :cond_3

    .line 2264848
    invoke-static {v0}, LX/Cyn;->l(LX/Cyn;)V

    .line 2264849
    :cond_3
    return-void
.end method

.method public final o()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2264850
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->C:LX/Cve;

    .line 2264851
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2264852
    invoke-virtual {v0, v1}, LX/Cve;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    .line 2264853
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->T:LX/195;

    invoke-virtual {v0}, LX/195;->b()V

    .line 2264854
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->u:LX/1k3;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->S:LX/0g8;

    invoke-virtual {v0, v1}, LX/1Kt;->c(LX/0g8;)V

    .line 2264855
    iput-boolean v2, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->X:Z

    .line 2264856
    iput v2, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ae:I

    .line 2264857
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->o()V

    .line 2264858
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 17
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x2

    const/16 v2, 0x2a

    const v3, -0x5617db91

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v16

    .line 2264859
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->s()Landroid/content/Context;

    move-result-object v7

    .line 2264860
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->q()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->z()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v1

    .line 2264861
    new-instance v2, LX/Fje;

    invoke-static {v1}, LX/Cw8;->from(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/Cw8;

    move-result-object v1

    sget-object v3, LX/EQE;->SPINNING_WHEEL:LX/EQE;

    invoke-direct {v2, v7, v1, v3}, LX/Fje;-><init>(Landroid/content/Context;LX/Cw8;LX/EQE;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Q:LX/Fje;

    .line 2264862
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Q:LX/Fje;

    const/16 v2, 0x1f4

    invoke-virtual {v1, v2}, LX/Fje;->setResultPageFadeTransitionDuration(I)V

    .line 2264863
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 2264864
    invoke-virtual {v7}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x7f0106cd

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 2264865
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Q:LX/Fje;

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    iget v1, v1, Landroid/util/TypedValue;->data:I

    invoke-direct {v3, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, LX/Fje;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2264866
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Q:LX/Fje;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, LX/Fje;->setRetryClickedListener(LX/1DI;)V

    .line 2264867
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Q:LX/Fje;

    invoke-virtual {v1}, LX/Fje;->getScrollingViewProxy()LX/0g8;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->S:LX/0g8;

    .line 2264868
    new-instance v10, LX/CxF;

    invoke-direct {v10}, LX/CxF;-><init>()V

    .line 2264869
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->q()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v2

    .line 2264870
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->n:LX/Cxm;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->P:LX/CyE;

    const-string v6, "graph_search_results_page"

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->x()LX/1PT;

    move-result-object v9

    new-instance v11, Lcom/facebook/search/results/fragment/SearchResultsFragment$3;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcom/facebook/search/results/fragment/SearchResultsFragment$3;-><init>(Lcom/facebook/search/results/fragment/SearchResultsFragment;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->S:LX/0g8;

    invoke-static {v12}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    move-object v15, v2

    invoke-virtual/range {v1 .. v15}, LX/Cxm;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzA;LX/CzA;LX/CyE;Ljava/lang/String;Landroid/content/Context;LX/CzA;LX/1PT;LX/CxF;Ljava/lang/Runnable;LX/1PY;LX/CzA;LX/CzA;Lcom/facebook/search/results/model/SearchResultsMutableContext;)LX/Cxl;

    move-result-object v1

    .line 2264871
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->t:LX/BwE;

    sget-object v4, LX/0wD;->SEARCH_RESULTS:LX/0wD;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v1, v4, v5}, LX/BwE;->a(LX/1Pf;LX/0wD;Ljava/lang/String;)LX/Bur;

    move-result-object v3

    invoke-virtual {v10, v3}, LX/CxF;->a(LX/1SX;)V

    .line 2264872
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->M:LX/Fe8;

    invoke-virtual {v3, v1}, LX/Fe8;->a(LX/Cxk;)V

    .line 2264873
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->k:LX/1DS;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->s:LX/0Ot;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    invoke-virtual {v3, v4, v5}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/1Ql;->a(LX/1PW;)LX/1Ql;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ql;->e()LX/1Qq;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->N:LX/1Qq;

    .line 2264874
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->z()V

    .line 2264875
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->S:LX/0g8;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->N:LX/1Qq;

    invoke-interface {v1, v3}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2264876
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->S:LX/0g8;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->l:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Db;

    invoke-virtual {v1}, LX/1Db;->a()LX/1St;

    move-result-object v1

    invoke-interface {v3, v1}, LX/0g8;->a(LX/1St;)V

    .line 2264877
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->R:LX/EQG;

    if-eqz v1, :cond_0

    .line 2264878
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Q:LX/Fje;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/Fje;->setIsInitialLoad(Z)V

    .line 2264879
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Q:LX/Fje;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->R:LX/EQG;

    invoke-virtual {v1, v3}, LX/Fje;->setState(LX/EQG;)V

    .line 2264880
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->S:LX/0g8;

    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->F()LX/0fu;

    move-result-object v3

    invoke-interface {v1, v3}, LX/0g8;->b(LX/0fu;)V

    .line 2264881
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->S:LX/0g8;

    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->E()LX/0fx;

    move-result-object v3

    invoke-interface {v1, v3}, LX/0g8;->a(LX/0fx;)V

    .line 2264882
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->u:LX/1k3;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->S:LX/0g8;

    invoke-virtual {v1, v3, v4}, LX/1Kt;->a(ZLX/0g8;)V

    .line 2264883
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->J:LX/CvM;

    invoke-virtual {v1}, LX/CvM;->a()V

    .line 2264884
    invoke-static {}, LX/1fG;->b()V

    .line 2264885
    invoke-virtual {v2}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->w()Ljava/lang/String;

    move-result-object v2

    .line 2264886
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2264887
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "early_fetch_view_id"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2264888
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2264889
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->o:LX/Cyo;

    invoke-virtual {v2, v1}, LX/Cyo;->a(Ljava/lang/String;)LX/Cyn;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->U:LX/Cyn;

    .line 2264890
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->U:LX/Cyn;

    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->B()LX/Fdy;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Cyn;->a(LX/Fdy;)V

    .line 2264891
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Q:LX/Fje;

    const/4 v2, 0x2

    const/16 v3, 0x2b

    const v4, 0x70b30c4

    move/from16 v0, v16

    invoke-static {v2, v3, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5c2d2fa7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2264892
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->N:LX/1Qq;

    if-eqz v1, :cond_0

    .line 2264893
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->N:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2264894
    :cond_0
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onDestroy()V

    .line 2264895
    const/16 v1, 0x2b

    const v2, 0x69c97cae

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x5d8c79ff

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2264896
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->U:LX/Cyn;

    if-eqz v1, :cond_0

    .line 2264897
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->U:LX/Cyn;

    .line 2264898
    iput-object v2, v1, LX/Cyn;->s:LX/Fdy;

    .line 2264899
    :cond_0
    iput-object v2, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->S:LX/0g8;

    .line 2264900
    iput-object v2, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Q:LX/Fje;

    .line 2264901
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ab:Z

    .line 2264902
    iget-boolean v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->W:Z

    if-nez v1, :cond_1

    .line 2264903
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ad:Z

    .line 2264904
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onDestroyView()V

    .line 2264905
    const/16 v1, 0x2b

    const v2, -0x69066911

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2264906
    :cond_1
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FcD;

    .line 2264907
    invoke-virtual {v1}, LX/FcD;->a()V

    .line 2264908
    invoke-virtual {v1}, LX/FcD;->b()V

    .line 2264909
    const/4 v2, 0x0

    .line 2264910
    iput-object v2, v1, LX/FcD;->k:LX/FcC;

    .line 2264911
    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x53784215

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2264912
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->M:LX/Fe8;

    invoke-virtual {v1}, LX/Fe8;->d()V

    .line 2264913
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onPause()V

    .line 2264914
    const/16 v1, 0x2b

    const v2, 0x641f9b15

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x25efb868

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2264915
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onResume()V

    .line 2264916
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->M:LX/Fe8;

    invoke-virtual {v1}, LX/Fe8;->c()V

    .line 2264917
    const/16 v1, 0x2b

    const v2, -0x1e232a83

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
