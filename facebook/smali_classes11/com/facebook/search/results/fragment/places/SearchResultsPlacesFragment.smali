.class public Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;
.super Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;
.source ""

# interfaces
.implements LX/FPO;
.implements LX/FdZ;


# instance fields
.field public i:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/FOx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fcw;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0lC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/FPb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

.field private p:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

.field private q:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

.field private r:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/widget/CustomLinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private s:Z

.field private t:Z

.field private u:Z

.field public v:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

.field private w:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

.field public x:LX/FPa;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2267571
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;-><init>()V

    .line 2267572
    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->s:Z

    .line 2267573
    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->t:Z

    .line 2267574
    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->u:Z

    .line 2267575
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->v:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    return-void
.end method

.method private a(Z)LX/0zw;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/0zw",
            "<",
            "Lcom/facebook/widget/CustomLinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2267221
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2267222
    if-eqz v0, :cond_0

    .line 2267223
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2267224
    const v1, 0x7f0d088c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2267225
    :cond_0
    :goto_0
    return-object v3

    .line 2267226
    :cond_1
    const v0, 0x7f0d088c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2267227
    new-instance v1, LX/0zw;

    new-instance v2, LX/Ff5;

    invoke-direct {v2, p0}, LX/Ff5;-><init>(Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;)V

    invoke-direct {v1, v0, v2}, LX/0zw;-><init>(Landroid/view/ViewStub;LX/0zy;)V

    iput-object v1, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->r:LX/0zw;

    .line 2267228
    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->r:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    .line 2267229
    if-eqz p1, :cond_3

    .line 2267230
    invoke-virtual {v0}, Lcom/facebook/widget/CustomLinearLayout;->getHeight()I

    move-result v1

    .line 2267231
    if-nez v1, :cond_2

    .line 2267232
    new-instance v1, LX/Ff6;

    invoke-direct {v1, p0, v0}, LX/Ff6;-><init>(Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;Lcom/facebook/widget/CustomLinearLayout;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    goto :goto_0

    .line 2267233
    :cond_2
    invoke-static {p0, v1}, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->a$redex0(Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;I)V

    goto :goto_0

    .line 2267234
    :cond_3
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;Ljava/lang/String;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2267566
    :try_start_0
    new-instance v0, LX/Ff7;

    invoke-direct {v0, p0}, LX/Ff7;-><init>(Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;)V

    .line 2267567
    iget-object v1, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->l:LX/0lC;

    invoke-virtual {v1, p1, v0}, LX/0lC;->a(Ljava/lang/String;LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2267568
    :goto_0
    return-object v0

    .line 2267569
    :catch_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->m:LX/03V;

    const-string v1, "filter_string_value_to_map"

    const-string v2, "An error occurred converting the string filter value to a map."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2267570
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2267557
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    int-to-float v0, p1

    invoke-direct {v1, v2, v2, v0, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2267558
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2267559
    const v2, 0x10e0001

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 2267560
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/view/animation/TranslateAnimation;->setFillBefore(Z)V

    .line 2267561
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v0}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2267562
    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->r:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    .line 2267563
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 2267564
    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2267565
    return-void
.end method

.method public static u(Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;)LX/0Px;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "LX/5uu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2267385
    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->q:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2267386
    iget-object v1, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v0, v1

    .line 2267387
    iget-boolean v1, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->n:Z

    move v1, v1

    .line 2267388
    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->w:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    if-nez v0, :cond_6

    .line 2267389
    new-instance v0, LX/Fcq;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1}, LX/Fcq;-><init>(LX/0Px;Z)V

    move-object v1, v0

    .line 2267390
    :goto_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fcw;

    .line 2267391
    :try_start_0
    const/4 v4, 0x0

    .line 2267392
    new-instance v5, LX/0P2;

    invoke-direct {v5}, LX/0P2;-><init>()V

    .line 2267393
    iget-object v2, v1, LX/Fcq;->a:LX/0Px;

    move-object v6, v2

    .line 2267394
    if-eqz v6, :cond_0

    .line 2267395
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v3, v4

    :goto_1
    if-ge v3, v7, :cond_0

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CyH;

    .line 2267396
    iget-object v8, v2, LX/CyH;->a:Ljava/lang/String;

    move-object v8, v8

    .line 2267397
    invoke-virtual {v5, v8, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2267398
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 2267399
    :cond_0
    invoke-virtual {v5}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    .line 2267400
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    const/4 v2, 0x4

    new-array v6, v2, [LX/5uu;

    const-string v2, "set_search_sort"

    invoke-virtual {v3, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CyH;

    .line 2267401
    iget-boolean v7, v1, LX/Fcq;->b:Z

    move v7, v7

    .line 2267402
    const/4 v1, 0x0

    .line 2267403
    if-eqz v2, :cond_7

    .line 2267404
    iget-object v8, v2, LX/CyH;->c:LX/4FP;

    move-object v8, v8

    .line 2267405
    invoke-virtual {v8}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v8

    const-string v9, "value"

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 2267406
    sget-object v9, LX/Fcw;->a:LX/0Rf;

    invoke-virtual {v9, v8}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 2267407
    :goto_2
    new-instance v9, LX/0Pz;

    invoke-direct {v9}, LX/0Pz;-><init>()V

    .line 2267408
    const-string v10, "default"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    iget-object v11, v0, LX/Fcw;->c:Landroid/content/Context;

    const v12, 0x7f082093

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "default"

    invoke-static {v10, v11, v12}, LX/Fcw;->a(ZLjava/lang/String;Ljava/lang/String;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2267409
    if-eqz v7, :cond_1

    .line 2267410
    const-string v10, "sort_distance"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    iget-object v11, v0, LX/Fcw;->c:Landroid/content/Context;

    const v12, 0x7f082094

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "sort_distance"

    invoke-static {v10, v11, v12}, LX/Fcw;->a(ZLjava/lang/String;Ljava/lang/String;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2267411
    :cond_1
    const-string v10, "sort_rating"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    iget-object v11, v0, LX/Fcw;->c:Landroid/content/Context;

    const v12, 0x7f082095

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "sort_rating"

    invoke-static {v10, v11, v12}, LX/Fcw;->a(ZLjava/lang/String;Ljava/lang/String;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2267412
    const-string v10, "sort_popularity"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    iget-object v10, v0, LX/Fcw;->c:Landroid/content/Context;

    const v11, 0x7f082096

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "sort_popularity"

    invoke-static {v8, v10, v11}, LX/Fcw;->a(ZLjava/lang/String;Ljava/lang/String;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    move-result-object v8

    invoke-virtual {v9, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2267413
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    .line 2267414
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    new-array v9, v9, [Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    invoke-virtual {v8, v9}, LX/0Px;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    .line 2267415
    invoke-static {v8}, LX/Fcw;->a([Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v8

    .line 2267416
    new-instance v9, LX/5v2;

    invoke-direct {v9}, LX/5v2;-><init>()V

    .line 2267417
    iput-object v1, v9, LX/5v2;->b:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    .line 2267418
    move-object v9, v9

    .line 2267419
    iget-object v10, v0, LX/Fcw;->c:Landroid/content/Context;

    const v11, 0x7f082093

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 2267420
    iput-object v10, v9, LX/5v2;->c:Ljava/lang/String;

    .line 2267421
    move-object v9, v9

    .line 2267422
    iput-object v8, v9, LX/5v2;->d:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    .line 2267423
    move-object v8, v9

    .line 2267424
    iput-object v1, v8, LX/5v2;->e:Ljava/lang/String;

    .line 2267425
    move-object v8, v8

    .line 2267426
    const-string v9, "setSearchSortFilter"

    .line 2267427
    iput-object v9, v8, LX/5v2;->f:Ljava/lang/String;

    .line 2267428
    move-object v8, v8

    .line 2267429
    const-string v9, "set_search_sort"

    .line 2267430
    iput-object v9, v8, LX/5v2;->g:Ljava/lang/String;

    .line 2267431
    move-object v8, v8

    .line 2267432
    iget-object v9, v0, LX/Fcw;->c:Landroid/content/Context;

    const v10, 0x7f082092

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 2267433
    iput-object v9, v8, LX/5v2;->h:Ljava/lang/String;

    .line 2267434
    move-object v8, v8

    .line 2267435
    iget-object v9, v0, LX/Fcw;->c:Landroid/content/Context;

    const v10, 0x7f082091

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v9, v10}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    .line 2267436
    iput-object v9, v8, LX/5v2;->i:Ljava/lang/String;

    .line 2267437
    move-object v8, v8

    .line 2267438
    invoke-virtual {v8}, LX/5v2;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;

    move-result-object v8

    move-object v2, v8

    .line 2267439
    aput-object v2, v6, v4

    const/4 v4, 0x1

    const-string v2, "set_search_open_now"

    invoke-virtual {v3, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CyH;

    const/4 v1, 0x0

    .line 2267440
    if-nez v2, :cond_8

    .line 2267441
    const-string v7, "default"

    .line 2267442
    :goto_3
    const/4 v8, 0x1

    new-array v8, v8, [Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    const/4 v9, 0x0

    const-string v10, "on"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    iget-object v11, v0, LX/Fcw;->c:Landroid/content/Context;

    const v12, 0x7f08208d

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11, v7}, LX/Fcw;->a(ZLjava/lang/String;Ljava/lang/String;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    move-result-object v7

    aput-object v7, v8, v9

    invoke-static {v8}, LX/Fcw;->a([Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v7

    .line 2267443
    new-instance v8, LX/5v2;

    invoke-direct {v8}, LX/5v2;-><init>()V

    .line 2267444
    iput-object v1, v8, LX/5v2;->b:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    .line 2267445
    move-object v8, v8

    .line 2267446
    const-string v9, ""

    .line 2267447
    iput-object v9, v8, LX/5v2;->c:Ljava/lang/String;

    .line 2267448
    move-object v8, v8

    .line 2267449
    iput-object v7, v8, LX/5v2;->d:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    .line 2267450
    move-object v7, v8

    .line 2267451
    iput-object v1, v7, LX/5v2;->e:Ljava/lang/String;

    .line 2267452
    move-object v7, v7

    .line 2267453
    const-string v8, "setSearchOpenNowFilter"

    .line 2267454
    iput-object v8, v7, LX/5v2;->f:Ljava/lang/String;

    .line 2267455
    move-object v7, v7

    .line 2267456
    const-string v8, "set_search_open_now"

    .line 2267457
    iput-object v8, v7, LX/5v2;->g:Ljava/lang/String;

    .line 2267458
    move-object v7, v7

    .line 2267459
    iget-object v8, v0, LX/Fcw;->c:Landroid/content/Context;

    const v9, 0x7f0820a5

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2267460
    iput-object v8, v7, LX/5v2;->h:Ljava/lang/String;

    .line 2267461
    move-object v7, v7

    .line 2267462
    iget-object v8, v0, LX/Fcw;->c:Landroid/content/Context;

    const v9, 0x7f0820a4

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v8, v9}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    .line 2267463
    iput-object v8, v7, LX/5v2;->i:Ljava/lang/String;

    .line 2267464
    move-object v7, v7

    .line 2267465
    invoke-virtual {v7}, LX/5v2;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;

    move-result-object v7

    move-object v2, v7

    .line 2267466
    aput-object v2, v6, v4

    const/4 v4, 0x2

    const-string v2, "set_search_price_category"

    invoke-virtual {v3, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CyH;

    const/4 v1, 0x0

    .line 2267467
    if-nez v2, :cond_2

    .line 2267468
    sget-object v7, LX/0Rg;->a:LX/0Rg;

    move-object v7, v7

    .line 2267469
    :goto_4
    iget-object v8, v0, LX/Fcw;->d:LX/0W9;

    invoke-virtual {v8}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v8

    invoke-static {v8}, Ljava/util/Currency;->getInstance(Ljava/util/Locale;)Ljava/util/Currency;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Currency;->getSymbol()Ljava/lang/String;

    move-result-object v9

    .line 2267470
    invoke-static {}, LX/Fcv;->values()[LX/Fcv;

    move-result-object v10

    .line 2267471
    array-length v8, v10

    new-array v11, v8, [Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    .line 2267472
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 2267473
    const/4 v8, 0x0

    :goto_5
    array-length v13, v10

    if-ge v8, v13, :cond_3

    .line 2267474
    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2267475
    aget-object v13, v10, v8

    invoke-virtual {v13}, LX/Fcv;->toString()Ljava/lang/String;

    move-result-object v13

    .line 2267476
    sget-object v14, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v7, v13}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    invoke-virtual {v14, v13}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v13

    .line 2267477
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aget-object p0, v10, v8

    invoke-virtual {p0}, LX/Fcv;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {v13, v14, p0}, LX/Fcw;->a(ZLjava/lang/String;Ljava/lang/String;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    move-result-object v13

    aput-object v13, v11, v8

    .line 2267478
    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 2267479
    :cond_2
    iget-object v7, v2, LX/CyH;->c:LX/4FP;

    move-object v7, v7

    .line 2267480
    invoke-virtual {v7}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v7

    const-string v8, "value"

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 2267481
    invoke-static {v0, v7}, LX/Fcw;->a(LX/Fcw;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v7

    invoke-static {v7}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v7

    goto :goto_4

    .line 2267482
    :cond_3
    new-instance v7, LX/5v2;

    invoke-direct {v7}, LX/5v2;-><init>()V

    .line 2267483
    iput-object v1, v7, LX/5v2;->b:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    .line 2267484
    move-object v7, v7

    .line 2267485
    const-string v8, ""

    .line 2267486
    iput-object v8, v7, LX/5v2;->c:Ljava/lang/String;

    .line 2267487
    move-object v7, v7

    .line 2267488
    invoke-static {v11}, LX/Fcw;->a([Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v8

    .line 2267489
    iput-object v8, v7, LX/5v2;->d:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    .line 2267490
    move-object v7, v7

    .line 2267491
    iput-object v1, v7, LX/5v2;->e:Ljava/lang/String;

    .line 2267492
    move-object v7, v7

    .line 2267493
    const-string v8, "setSearchPriceCategoryFilter"

    .line 2267494
    iput-object v8, v7, LX/5v2;->f:Ljava/lang/String;

    .line 2267495
    move-object v7, v7

    .line 2267496
    const-string v8, "set_search_price_category"

    .line 2267497
    iput-object v8, v7, LX/5v2;->g:Ljava/lang/String;

    .line 2267498
    move-object v7, v7

    .line 2267499
    iget-object v8, v0, LX/Fcw;->c:Landroid/content/Context;

    const v9, 0x7f082098

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2267500
    iput-object v8, v7, LX/5v2;->h:Ljava/lang/String;

    .line 2267501
    move-object v7, v7

    .line 2267502
    iget-object v8, v0, LX/Fcw;->c:Landroid/content/Context;

    const v9, 0x7f082097

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v8, v9}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    .line 2267503
    iput-object v8, v7, LX/5v2;->i:Ljava/lang/String;

    .line 2267504
    move-object v7, v7

    .line 2267505
    invoke-virtual {v7}, LX/5v2;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;

    move-result-object v7

    move-object v2, v7

    .line 2267506
    aput-object v2, v6, v4

    const/4 v4, 0x3

    const-string v2, "set_search_features"

    invoke-virtual {v3, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CyH;

    const/4 v14, 0x0

    .line 2267507
    if-nez v2, :cond_4

    .line 2267508
    sget-object v3, LX/0Rg;->a:LX/0Rg;

    move-object v3, v3

    .line 2267509
    move-object v7, v3

    .line 2267510
    :goto_6
    invoke-static {}, LX/Fcu;->values()[LX/Fcu;

    move-result-object v9

    .line 2267511
    array-length v3, v9

    new-array v10, v3, [Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    .line 2267512
    const/4 v3, 0x0

    move v8, v3

    :goto_7
    array-length v3, v9

    if-ge v8, v3, :cond_5

    .line 2267513
    aget-object v3, v9, v8

    invoke-virtual {v3}, LX/Fcu;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2267514
    sget-object v11, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v7, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v11

    .line 2267515
    iget-object v12, v0, LX/Fcw;->c:Landroid/content/Context;

    sget-object v3, LX/Fcw;->b:LX/0P1;

    aget-object v13, v9, v8

    invoke-virtual {v3, v13}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v12, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aget-object v12, v9, v8

    invoke-virtual {v12}, LX/Fcu;->name()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v3, v12}, LX/Fcw;->a(ZLjava/lang/String;Ljava/lang/String;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    move-result-object v3

    aput-object v3, v10, v8

    .line 2267516
    add-int/lit8 v3, v8, 0x1

    move v8, v3

    goto :goto_7

    .line 2267517
    :cond_4
    iget-object v3, v2, LX/CyH;->c:LX/4FP;

    move-object v3, v3

    .line 2267518
    invoke-virtual {v3}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v3

    const-string v7, "value"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2267519
    invoke-static {v0, v3}, LX/Fcw;->a(LX/Fcw;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    invoke-static {v3}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v3

    move-object v7, v3

    goto :goto_6

    .line 2267520
    :cond_5
    new-instance v3, LX/5v2;

    invoke-direct {v3}, LX/5v2;-><init>()V

    .line 2267521
    iput-object v14, v3, LX/5v2;->b:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    .line 2267522
    move-object v3, v3

    .line 2267523
    const-string v7, ""

    .line 2267524
    iput-object v7, v3, LX/5v2;->c:Ljava/lang/String;

    .line 2267525
    move-object v3, v3

    .line 2267526
    invoke-static {v10}, LX/Fcw;->a([Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v7

    .line 2267527
    iput-object v7, v3, LX/5v2;->d:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    .line 2267528
    move-object v3, v3

    .line 2267529
    iput-object v14, v3, LX/5v2;->e:Ljava/lang/String;

    .line 2267530
    move-object v3, v3

    .line 2267531
    const-string v7, "setSearchFeaturesFilter"

    .line 2267532
    iput-object v7, v3, LX/5v2;->f:Ljava/lang/String;

    .line 2267533
    move-object v3, v3

    .line 2267534
    const-string v7, "set_search_features"

    .line 2267535
    iput-object v7, v3, LX/5v2;->g:Ljava/lang/String;

    .line 2267536
    move-object v3, v3

    .line 2267537
    iget-object v7, v0, LX/Fcw;->c:Landroid/content/Context;

    const v8, 0x7f08209a

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2267538
    iput-object v7, v3, LX/5v2;->h:Ljava/lang/String;

    .line 2267539
    move-object v3, v3

    .line 2267540
    iget-object v7, v0, LX/Fcw;->c:Landroid/content/Context;

    const v8, 0x7f082099

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    .line 2267541
    iput-object v7, v3, LX/5v2;->i:Ljava/lang/String;

    .line 2267542
    move-object v3, v3

    .line 2267543
    invoke-virtual {v3}, LX/5v2;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;

    move-result-object v3

    move-object v2, v3

    .line 2267544
    aput-object v2, v6, v4

    invoke-virtual {v5, v6}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    move-result-object v2

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v2, v2
    :try_end_0
    .catch LX/7C4; {:try_start_0 .. :try_end_0} :catch_0

    .line 2267545
    :goto_8
    move-object v0, v2

    .line 2267546
    return-object v0

    .line 2267547
    :cond_6
    new-instance v0, LX/Fcq;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->w:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    sget-object v3, LX/CyI;->PLACES:LX/CyI;

    invoke-virtual {v2, v3}, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->a(LX/CyI;)LX/0Px;

    move-result-object v2

    invoke-direct {v0, v2, v1}, LX/Fcq;-><init>(LX/0Px;Z)V

    move-object v1, v0

    goto/16 :goto_0

    .line 2267548
    :catch_0
    iget-object v2, v0, LX/Fcw;->f:LX/03V;

    sget-object v3, LX/3Ql;->FETCH_TOP_FILTERS_FAIL:LX/3Ql;

    invoke-virtual {v3}, LX/3Ql;->name()Ljava/lang/String;

    move-result-object v3

    const-string v4, "There was an error generating the filters for set search"

    invoke-static {v3, v4}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v3

    const/4 v4, 0x1

    .line 2267549
    iput-boolean v4, v3, LX/0VK;->d:Z

    .line 2267550
    move-object v3, v3

    .line 2267551
    invoke-virtual {v3}, LX/0VK;->g()LX/0VG;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/03V;->a(LX/0VG;)V

    .line 2267552
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2267553
    goto :goto_8

    .line 2267554
    :cond_7
    const-string v8, "default"

    goto/16 :goto_2

    .line 2267555
    :cond_8
    iget-object v7, v2, LX/CyH;->c:LX/4FP;

    move-object v7, v7

    .line 2267556
    invoke-virtual {v7}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v7

    const-string v8, "value"

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    goto/16 :goto_3
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2267384
    const-string v0, "graph_search_results_page_place"

    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2267311
    if-nez p1, :cond_5

    .line 2267312
    const/4 v0, 0x0

    .line 2267313
    :goto_0
    move-object v0, v0

    .line 2267314
    iget-object v1, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->w:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    if-eqz v1, :cond_0

    .line 2267315
    iget-object v1, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->w:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    sget-object v2, LX/CyI;->PLACES:LX/CyI;

    invoke-virtual {v1, v2, p1}, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->b(LX/CyI;LX/0Px;)V

    .line 2267316
    :cond_0
    iget-object v1, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->o:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    invoke-virtual {v1, v0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->a(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;)V

    .line 2267317
    iget-object v1, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->x:LX/FPa;

    .line 2267318
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267319
    const-string v2, "places_advanced_filters_applied"

    invoke-static {v1, v2}, LX/FPa;->a(LX/FPa;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2267320
    iget-object v2, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->a:LX/FPo;

    move-object v2, v2

    .line 2267321
    if-eqz v2, :cond_1

    .line 2267322
    const-string v4, "current_sort_state"

    sget-object v5, LX/FPa;->e:LX/0P1;

    invoke-virtual {v5, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2267323
    :cond_1
    iget-object v2, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->b:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    move-object v2, v2

    .line 2267324
    if-eqz v2, :cond_2

    .line 2267325
    iget v4, v2, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;->a:I

    and-int/lit8 v4, v4, 0x1

    if-nez v4, :cond_14

    .line 2267326
    const-string v4, ""

    .line 2267327
    :goto_1
    move-object v2, v4

    .line 2267328
    const-string v4, "current_filter_state"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2267329
    :cond_2
    iget-object v2, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;

    move-object v2, v2

    .line 2267330
    if-eqz v2, :cond_3

    .line 2267331
    const-string v4, "price_filters_applied"

    .line 2267332
    iget-object v5, v2, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;->a:LX/0Px;

    move-object v5, v5

    .line 2267333
    if-nez v5, :cond_15

    const-string v5, ""

    :goto_2
    move-object v2, v5

    .line 2267334
    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2267335
    :cond_3
    iget-object v2, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;

    move-object v2, v2

    .line 2267336
    if-eqz v2, :cond_4

    .line 2267337
    const-string v4, "features_filters_applied"

    .line 2267338
    iget-object v5, v2, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;->a:LX/0Px;

    move-object v5, v5

    .line 2267339
    if-nez v5, :cond_16

    const-string v5, ""

    :goto_3
    move-object v2, v5

    .line 2267340
    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2267341
    :cond_4
    iget-object v2, v1, LX/FPa;->a:LX/0Zb;

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2267342
    return-void

    .line 2267343
    :cond_5
    new-instance v3, LX/FPh;

    invoke-direct {v3}, LX/FPh;-><init>()V

    .line 2267344
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_4
    if-ge v2, v4, :cond_11

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CyH;

    .line 2267345
    iget-object v1, v0, LX/CyH;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2267346
    iget-object v5, v0, LX/CyH;->c:LX/4FP;

    move-object v0, v5

    .line 2267347
    invoke-virtual {v0}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v0

    const-string v5, "value"

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2267348
    const-string v5, "set_search_sort"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 2267349
    const-string v1, "sort_distance"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2267350
    sget-object v0, LX/FPo;->DISTANCE:LX/FPo;

    .line 2267351
    :goto_5
    iput-object v0, v3, LX/FPh;->a:LX/FPo;

    .line 2267352
    :cond_6
    :goto_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 2267353
    :cond_7
    const-string v1, "sort_popularity"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2267354
    sget-object v0, LX/FPo;->POPULARITY:LX/FPo;

    goto :goto_5

    .line 2267355
    :cond_8
    const-string v1, "sort_rating"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2267356
    sget-object v0, LX/FPo;->RATING:LX/FPo;

    goto :goto_5

    .line 2267357
    :cond_9
    sget-object v0, LX/FPo;->RELEVANCE:LX/FPo;

    goto :goto_5

    .line 2267358
    :cond_a
    const-string v5, "set_search_open_now"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 2267359
    new-instance v1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    invoke-direct {v1, v0}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;-><init>(Ljava/lang/String;)V

    .line 2267360
    iput-object v1, v3, LX/FPh;->b:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    .line 2267361
    goto :goto_6

    .line 2267362
    :cond_b
    const-string v5, "set_search_price_category"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 2267363
    new-instance v5, LX/FPl;

    invoke-direct {v5}, LX/FPl;-><init>()V

    .line 2267364
    invoke-static {p0, v0}, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->a(Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v6

    .line 2267365
    if-eqz v6, :cond_d

    .line 2267366
    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_c
    :goto_7
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2267367
    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 2267368
    invoke-virtual {v5, v0}, LX/FPl;->a(Ljava/lang/String;)V

    goto :goto_7

    .line 2267369
    :cond_d
    new-instance v1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;

    iget-object v0, v5, LX/FPl;->a:LX/0Pz;

    if-nez v0, :cond_12

    const/4 v0, 0x0

    :goto_8
    invoke-direct {v1, v0}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;-><init>(Ljava/util/List;)V

    move-object v0, v1

    .line 2267370
    iput-object v0, v3, LX/FPh;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;

    .line 2267371
    goto :goto_6

    .line 2267372
    :cond_e
    const-string v5, "set_search_features"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2267373
    new-instance v5, LX/FPe;

    invoke-direct {v5}, LX/FPe;-><init>()V

    .line 2267374
    invoke-static {p0, v0}, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->a(Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v6

    .line 2267375
    if-eqz v6, :cond_10

    .line 2267376
    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_f
    :goto_9
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2267377
    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 2267378
    invoke-virtual {v5, v0}, LX/FPe;->a(Ljava/lang/String;)V

    goto :goto_9

    .line 2267379
    :cond_10
    new-instance v1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;

    iget-object v0, v5, LX/FPe;->a:LX/0Pz;

    if-nez v0, :cond_13

    const/4 v0, 0x0

    :goto_a
    invoke-direct {v1, v0}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;-><init>(Ljava/util/List;)V

    move-object v0, v1

    .line 2267380
    iput-object v0, v3, LX/FPh;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;

    .line 2267381
    goto/16 :goto_6

    .line 2267382
    :cond_11
    new-instance v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    invoke-direct {v0, v3}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;-><init>(LX/FPh;)V

    move-object v0, v0

    .line 2267383
    goto/16 :goto_0

    :cond_12
    iget-object v0, v5, LX/FPl;->a:LX/0Pz;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_8

    :cond_13
    iget-object v0, v5, LX/FPe;->a:LX/0Pz;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_a

    :cond_14
    const-string v4, "FBPlacesFilteringToggleStateOpenNow"

    goto/16 :goto_1

    :cond_15
    const-string v6, ","

    invoke-static {v6, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    :cond_16
    const-string v6, ","

    invoke-static {v6, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_3
.end method

.method public final a(LX/5uu;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5uu;",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2267310
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v5, 0x0

    .line 2267261
    invoke-super {p0, p1}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->a(Landroid/os/Bundle;)V

    .line 2267262
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2267263
    if-eqz v0, :cond_3

    const-string v1, "tab_bar_tap"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, LX/CQC;->tab_bar_tap:LX/CQC;

    .line 2267264
    :goto_0
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v10

    move-object v3, p0

    check-cast v3, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;

    invoke-static {v10}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v10}, LX/FOx;->a(LX/0QB;)LX/FOx;

    move-result-object v6

    check-cast v6, LX/FOx;

    const/16 v7, 0x333d

    invoke-static {v10, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v10}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v8

    check-cast v8, LX/0lC;

    invoke-static {v10}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    const-class v1, LX/FPb;

    invoke-interface {v10, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/FPb;

    iput-object v4, v3, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->i:LX/0ad;

    iput-object v6, v3, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->j:LX/FOx;

    iput-object v7, v3, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->k:LX/0Ot;

    iput-object v8, v3, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->l:LX/0lC;

    iput-object v9, v3, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->m:LX/03V;

    iput-object v10, v3, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->n:LX/FPb;

    .line 2267265
    if-nez p1, :cond_4

    .line 2267266
    iget-object v1, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->j:LX/FOx;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/FOx;->a(Landroid/app/Activity;)LX/FOw;

    move-result-object v1

    .line 2267267
    iget-object v2, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v2, v2

    .line 2267268
    new-instance v3, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    sget-object v4, LX/CQB;->SEARCH_SUGGESTION:LX/CQB;

    invoke-direct {v3, v4, v0, v1, v2}, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;-><init>(LX/CQB;LX/CQC;LX/FOw;Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    iput-object v3, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->p:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2267269
    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->p:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2267270
    iget-object v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v0, v1

    .line 2267271
    new-instance v1, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v3, v2}, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2267272
    iput-object v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    .line 2267273
    iput-boolean v5, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    .line 2267274
    :goto_1
    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->p:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267275
    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->p:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2267276
    iget-object v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v0, v1

    .line 2267277
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267278
    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->n:LX/FPb;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->p:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->p:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/FPb;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;LX/FOz;Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)LX/FPa;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->x:LX/FPa;

    .line 2267279
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    .line 2267280
    const-string v0, "resultsFragment"

    invoke-virtual {v1, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->o:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    .line 2267281
    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->o:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    if-nez v0, :cond_1

    .line 2267282
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2267283
    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->v:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    if-nez v0, :cond_0

    .line 2267284
    new-instance v0, LX/FPY;

    invoke-direct {v0}, LX/FPY;-><init>()V

    .line 2267285
    iput-boolean v2, v0, LX/FPY;->a:Z

    .line 2267286
    move-object v0, v0

    .line 2267287
    iput-boolean v2, v0, LX/FPY;->b:Z

    .line 2267288
    move-object v0, v0

    .line 2267289
    iput-boolean v2, v0, LX/FPY;->c:Z

    .line 2267290
    move-object v0, v0

    .line 2267291
    iput-boolean v2, v0, LX/FPY;->d:Z

    .line 2267292
    move-object v0, v0

    .line 2267293
    iput-boolean v3, v0, LX/FPY;->e:Z

    .line 2267294
    move-object v0, v0

    .line 2267295
    iput-boolean v3, v0, LX/FPY;->f:Z

    .line 2267296
    move-object v0, v0

    .line 2267297
    invoke-virtual {v0}, LX/FPY;->a()Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->v:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    .line 2267298
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->v:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    move-object v0, v0

    .line 2267299
    invoke-static {v0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->b(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;)Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->o:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    .line 2267300
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d1243

    iget-object v2, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->o:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    const-string v3, "resultsFragment"

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2267301
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->o:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->p:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;)V

    .line 2267302
    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->o:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->p:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->a(LX/FOz;)V

    .line 2267303
    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->o:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    invoke-virtual {v0, p0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->a(LX/FPO;)V

    .line 2267304
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 2267305
    instance-of v1, v0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    if-eqz v1, :cond_2

    .line 2267306
    check-cast v0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->w:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    .line 2267307
    :cond_2
    return-void

    .line 2267308
    :cond_3
    sget-object v0, LX/CQC;->open:LX/CQC;

    goto/16 :goto_0

    .line 2267309
    :cond_4
    const-string v0, "nearby_places_fragment_model_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->p:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    goto/16 :goto_1
.end method

.method public final a(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2267255
    iput-boolean v3, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->s:Z

    .line 2267256
    iput-object p1, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->q:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2267257
    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->n:LX/FPb;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->p:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->p:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    invoke-virtual {v0, v1, v2, p1}, LX/FPb;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;LX/FOz;Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)LX/FPa;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->x:LX/FPa;

    .line 2267258
    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->q:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->q:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2267259
    invoke-direct {p0, v3}, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->a(Z)LX/0zw;

    .line 2267260
    :cond_0
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 2267250
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->s:Z

    .line 2267251
    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->o:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    if-nez v0, :cond_0

    .line 2267252
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->t:Z

    .line 2267253
    :goto_0
    return-void

    .line 2267254
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->o:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->b()V

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2267249
    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->s:Z

    return v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x434b9272

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2267244
    invoke-super {p0, p1}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2267245
    iget-boolean v1, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->t:Z

    if-eqz v1, :cond_0

    .line 2267246
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->t:Z

    .line 2267247
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->k()V

    .line 2267248
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x4afedfb6    # 8351707.0f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x2169eefd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2267243
    const v1, 0x7f0306b7

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x4218914a

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2267240
    invoke-super {p0, p1}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2267241
    const-string v0, "nearby_places_fragment_model_state"

    iget-object v1, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->p:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2267242
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2267235
    invoke-super {p0, p1, p2}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2267236
    iget-object v1, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->i:LX/0ad;

    sget-short v2, LX/100;->bv:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->u:Z

    .line 2267237
    iget-boolean v1, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->u:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->q:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->q:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-virtual {v1}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2267238
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-direct {p0, v0}, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->a(Z)LX/0zw;

    .line 2267239
    :cond_1
    return-void
.end method
