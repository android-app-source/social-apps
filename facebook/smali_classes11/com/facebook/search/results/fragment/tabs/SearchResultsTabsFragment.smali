.class public Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/CyE;
.implements LX/Fdv;
.implements LX/Ffo;


# static fields
.field public static final k:Ljava/lang/String;


# instance fields
.field public a:LX/Ffq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CvY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/2Sd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Ffl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/CyI;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/7BH;

.field public n:Landroid/support/v4/view/ViewPager;

.field public o:LX/Ffp;

.field private p:LX/FZb;

.field public q:Lcom/facebook/search/model/KeywordTypeaheadUnit;

.field public r:LX/CyI;

.field private s:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field private t:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

.field private u:Landroid/view/View$OnTouchListener;

.field public v:Z

.field public w:Z

.field private x:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/CyI;",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2269050
    const-class v0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2269044
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2269045
    new-instance v0, Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-direct {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2269046
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2269047
    iput-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->j:LX/0Ot;

    .line 2269048
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->w:Z

    .line 2269049
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->x:Ljava/util/HashMap;

    return-void
.end method

.method private d(I)I
    .locals 1

    .prologue
    .line 2268919
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->e:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->l:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    sub-int/2addr v0, p1

    add-int/lit8 p1, v0, -0x1

    :cond_0
    return p1
.end method

.method private m()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 2269003
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->q:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->v:Z

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2269004
    if-nez v0, :cond_1

    .line 2269005
    :cond_0
    :goto_1
    return-void

    .line 2269006
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2269007
    if-eqz v0, :cond_4

    .line 2269008
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2269009
    const-string v1, "tabs_override"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2269010
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2269011
    const-string v1, "tabs_override"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2269012
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->l:LX/0Px;

    .line 2269013
    :goto_2
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->a:LX/Ffq;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->m:LX/7BH;

    iget-object v3, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->q:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    iget-object v4, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2269014
    iget-object v5, v4, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v4, v5

    .line 2269015
    iget-object v5, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2269016
    iget-object v6, v5, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    move-object v5, v6

    .line 2269017
    iget-object v6, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->p:LX/FZb;

    iget-object v8, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->l:LX/0Px;

    move-object v7, p0

    invoke-virtual/range {v0 .. v8}, LX/Ffq;->a(LX/0gc;LX/7BH;Lcom/facebook/search/model/KeywordTypeaheadUnit;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;LX/FZb;LX/CyE;LX/0Px;)LX/Ffp;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->o:LX/Ffp;

    .line 2269018
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->n:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->o:LX/Ffp;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2269019
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->q:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-nez v0, :cond_5

    const/4 v0, 0x0

    .line 2269020
    :goto_3
    sget-object v1, LX/CwF;->local_category:LX/CwF;

    invoke-virtual {v1, v0}, LX/CwF;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->l:LX/0Px;

    sget-object v1, LX/CyI;->PLACES:LX/CyI;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2269021
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->l:LX/0Px;

    sget-object v1, LX/CyI;->PLACES:LX/CyI;

    invoke-virtual {v0, v1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    move v1, v0

    .line 2269022
    :goto_4
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->s:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    if-eqz v0, :cond_2

    .line 2269023
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->s:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2269024
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->e:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2269025
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->s:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->fullScroll(I)Z

    .line 2269026
    :goto_5
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->s:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v9}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->a(IFI)V

    .line 2269027
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2269028
    if-nez v0, :cond_9

    .line 2269029
    :cond_3
    :goto_6
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->l:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CyI;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->r:LX/CyI;

    .line 2269030
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-eq v1, v0, :cond_0

    .line 2269031
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->c:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment$3;

    invoke-direct {v2, p0, v1}, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment$3;-><init>(Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;I)V

    const v1, 0x63838bd3

    invoke-static {v0, v2, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto/16 :goto_1

    .line 2269032
    :cond_4
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->f:LX/Ffl;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->q:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-virtual {v0, v1}, LX/Ffl;->a(LX/CwB;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->l:LX/0Px;

    goto/16 :goto_2

    .line 2269033
    :cond_5
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->q:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-virtual {v0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v0

    goto :goto_3

    .line 2269034
    :cond_6
    invoke-direct {p0, v9}, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->d(I)I

    move-result v0

    move v1, v0

    goto :goto_4

    .line 2269035
    :cond_7
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->s:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v0, v9, v9}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->scrollTo(II)V

    goto :goto_5

    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2269036
    :cond_9
    const-string v2, "browse_session_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2269037
    iget-object v2, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->o:LX/Ffp;

    invoke-virtual {v2, v1}, LX/2s5;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 2269038
    iget-object v3, v2, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v3

    .line 2269039
    const-string v3, "browse_session_id"

    const-string v4, "browse_session_id"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2269040
    :cond_a
    const-string v2, "early_fetch_view_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2269041
    iget-object v2, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->o:LX/Ffp;

    invoke-virtual {v2, v1}, LX/2s5;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 2269042
    iget-object v3, v2, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v3

    .line 2269043
    const-string v3, "early_fetch_view_id"

    const-string v4, "early_fetch_view_id"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6
.end method


# virtual methods
.method public final a(LX/CyI;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CyI;",
            ")",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2269000
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->x:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2269001
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2269002
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->x:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2268999
    const-string v0, "graph_search_results_page"

    return-object v0
.end method

.method public final a(LX/CwB;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;)V
    .locals 1

    .prologue
    .line 2268994
    instance-of v0, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    move-object v0, p1

    .line 2268995
    check-cast v0, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->q:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 2268996
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a(LX/CwB;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;)V

    .line 2268997
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->m()V

    .line 2268998
    return-void
.end method

.method public final a(LX/CyI;LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CyI;",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2268988
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->l:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2268989
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 2268990
    :goto_0
    return-void

    .line 2268991
    :cond_0
    if-eqz p2, :cond_1

    .line 2268992
    invoke-virtual {p0, p1, p2}, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->b(LX/CyI;LX/0Px;)V

    .line 2268993
    :cond_1
    iget-object v1, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->n:Landroid/support/v4/view/ViewPager;

    invoke-direct {p0, v0}, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->d(I)I

    move-result v0

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    goto :goto_0
.end method

.method public final a(LX/FZb;)V
    .locals 0

    .prologue
    .line 2268986
    iput-object p1, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->p:LX/FZb;

    .line 2268987
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2268980
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2268981
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    const-class v3, LX/Ffq;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Ffq;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v4

    check-cast v4, LX/CvY;

    const/16 v5, 0x19c6

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v6

    check-cast v6, Landroid/os/Handler;

    invoke-static {v0}, LX/2Sd;->a(LX/0QB;)LX/2Sd;

    move-result-object v7

    check-cast v7, LX/2Sd;

    invoke-static {v0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v8

    check-cast v8, LX/0hL;

    invoke-static {v0}, LX/Ffl;->a(LX/0QB;)LX/Ffl;

    move-result-object v9

    check-cast v9, LX/Ffl;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p1

    check-cast p1, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    iput-object v3, v2, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->a:LX/Ffq;

    iput-object v4, v2, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->b:LX/CvY;

    iput-object v5, v2, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->j:LX/0Ot;

    iput-object v6, v2, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->c:Landroid/os/Handler;

    iput-object v7, v2, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->d:LX/2Sd;

    iput-object v8, v2, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->e:LX/0hL;

    iput-object v9, v2, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->f:LX/Ffl;

    iput-object p1, v2, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->g:LX/0ad;

    iput-object v0, v2, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->h:LX/0Uh;

    .line 2268982
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->h:LX/0Uh;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->g:LX/0ad;

    .line 2268983
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2268984
    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a(LX/0Uh;LX/0ad;Landroid/os/Bundle;)V

    .line 2268985
    return-void
.end method

.method public final b(LX/CyI;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CyI;",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2268978
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->x:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2268979
    return-void
.end method

.method public final b(Z)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2268967
    iget-object v2, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->n:Landroid/support/v4/view/ViewPager;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    if-lez v2, :cond_0

    move v2, v0

    .line 2268968
    :goto_0
    if-nez p1, :cond_1

    if-eqz v2, :cond_1

    move v2, v0

    .line 2268969
    :goto_1
    if-eqz v2, :cond_2

    .line 2268970
    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->w:Z

    .line 2268971
    iget-object v2, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v1, v0}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 2268972
    :goto_2
    return v0

    :cond_0
    move v2, v1

    .line 2268973
    goto :goto_0

    :cond_1
    move v2, v1

    .line 2268974
    goto :goto_1

    .line 2268975
    :cond_2
    iget-object v2, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->b:LX/CvY;

    iget-object v3, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    if-eqz p1, :cond_3

    sget-object v0, LX/7CL;->UP_BUTTON:LX/7CL;

    :goto_3
    invoke-virtual {v2, v3, v0}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/7CL;)V

    move v0, v1

    .line 2268976
    goto :goto_2

    .line 2268977
    :cond_3
    sget-object v0, LX/7CL;->BACK_BUTTON:LX/7CL;

    goto :goto_3
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2268965
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->b:LX/CvY;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    sget-object v2, LX/7CL;->CLEAR_BUTTON:LX/7CL;

    invoke-virtual {v0, v1, v2}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/7CL;)V

    .line 2268966
    return-void
.end method

.method public final d()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 2268964
    return-object p0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2268961
    const/16 v0, 0x6dc

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2268962
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2268963
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x2a

    const v1, 0xaab5c63

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2268945
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2268946
    invoke-static {v0}, LX/D0Q;->a(Landroid/os/Bundle;)LX/7BH;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->m:LX/7BH;

    .line 2268947
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->m:LX/7BH;

    invoke-static {v0, v2}, LX/8i2;->a(Landroid/content/Context;LX/7BH;)Landroid/view/ContextThemeWrapper;

    move-result-object v0

    .line 2268948
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_0

    .line 2268949
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 2268950
    const v3, 0x1010451

    invoke-static {v0, v3, v4}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v3

    .line 2268951
    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 2268952
    :cond_0
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0306b8

    invoke-virtual {v0, v2, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2268953
    const v0, 0x7f0d1241

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->n:Landroid/support/v4/view/ViewPager;

    .line 2268954
    const v0, 0x7f0d1240

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->s:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2268955
    new-instance v0, LX/Ffm;

    invoke-direct {v0, p0}, LX/Ffm;-><init>(Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;)V

    .line 2268956
    iget-object v3, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->s:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2268957
    iput-object v0, v3, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 2268958
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->v:Z

    .line 2268959
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->m()V

    .line 2268960
    const/16 v0, 0x2b

    const v3, -0x24dbb289

    invoke-static {v5, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x16a5b4b0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2268938
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2268939
    iget-object v1, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->t:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    if-eqz v1, :cond_0

    .line 2268940
    iget-object v1, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->t:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-virtual {v1, p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->b(LX/Ffo;)V

    .line 2268941
    iget-object v1, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->t:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    .line 2268942
    iget-object v2, v1, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v1, v2

    .line 2268943
    iget-object v2, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->u:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Lcom/facebook/ui/search/SearchEditText;->b(Landroid/view/View$OnTouchListener;)V

    .line 2268944
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x2ed550e3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x32219f57

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2268935
    iget-object v1, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->d:LX/2Sd;

    sget-object v2, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->k:Ljava/lang/String;

    sget-object v3, LX/7CQ;->FRAGMENT_PAUSED:LX/7CQ;

    invoke-virtual {v1, v2, v3}, LX/2Sd;->a(Ljava/lang/String;LX/7CQ;)V

    .line 2268936
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2268937
    const/16 v1, 0x2b

    const v2, 0x35b80961

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x5fe9eb27

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2268932
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2268933
    iget-object v1, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->d:LX/2Sd;

    sget-object v2, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->k:Ljava/lang/String;

    sget-object v3, LX/7CQ;->FRAGMENT_RESUMED:LX/7CQ;

    invoke-virtual {v1, v2, v3}, LX/2Sd;->a(Ljava/lang/String;LX/7CQ;)V

    .line 2268934
    const/16 v1, 0x2b

    const v2, 0x7554c8ca

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1fb7f551

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2268920
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2268921
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2268922
    if-eqz v0, :cond_0

    .line 2268923
    invoke-interface {v0}, LX/1ZF;->f()Landroid/view/View;

    move-result-object v0

    .line 2268924
    if-eqz v0, :cond_0

    instance-of v2, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    if-eqz v2, :cond_0

    .line 2268925
    check-cast v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->t:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    .line 2268926
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->t:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-virtual {v0, p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a(LX/Ffo;)V

    .line 2268927
    new-instance v0, LX/Ffn;

    invoke-direct {v0, p0}, LX/Ffn;-><init>(Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;)V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->u:Landroid/view/View$OnTouchListener;

    .line 2268928
    iget-object v0, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->t:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    .line 2268929
    iget-object v2, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v2

    .line 2268930
    iget-object v2, p0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->u:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v2}, Lcom/facebook/ui/search/SearchEditText;->a(Landroid/view/View$OnTouchListener;)V

    .line 2268931
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x665bbea9

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
