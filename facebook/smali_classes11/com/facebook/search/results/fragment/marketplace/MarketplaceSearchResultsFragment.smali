.class public Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;
.super Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;
.source ""


# instance fields
.field public i:LX/FgS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/Bnj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

.field private l:LX/Bnu;

.field private m:LX/Bno;

.field public n:LX/0gc;

.field public o:I

.field public p:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2266853
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;

    invoke-static {p0}, LX/FgS;->a(LX/0QB;)LX/FgS;

    move-result-object v1

    check-cast v1, LX/FgS;

    invoke-static {p0}, LX/Bnj;->a(LX/0QB;)LX/Bnj;

    move-result-object p0

    check-cast p0, LX/Bnj;

    iput-object v1, p1, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->i:LX/FgS;

    iput-object p0, p1, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->j:LX/Bnj;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2266850
    iget-object v0, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2266851
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "react_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2266852
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "marketplace"

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2266843
    invoke-super {p0, p1}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->a(Landroid/os/Bundle;)V

    .line 2266844
    const-class v0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;

    invoke-static {v0, p0}, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2266845
    new-instance v0, LX/Bnu;

    new-instance v1, LX/Fet;

    invoke-direct {v1, p0}, LX/Fet;-><init>(Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;)V

    invoke-direct {v0, v1}, LX/Bnu;-><init>(LX/Bnp;)V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->l:LX/Bnu;

    .line 2266846
    new-instance v0, LX/Bno;

    new-instance v1, LX/Feu;

    invoke-direct {v1, p0, p0}, LX/Feu;-><init>(Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;Landroid/support/v4/app/Fragment;)V

    invoke-direct {v0, v1}, LX/Bno;-><init>(LX/Bnp;)V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->m:LX/Bno;

    .line 2266847
    iget-object v0, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->j:LX/Bnj;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->l:LX/Bnu;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2266848
    iget-object v0, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->j:LX/Bnj;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->m:LX/Bno;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2266849
    return-void
.end method

.method public final b(Z)Z
    .locals 3

    .prologue
    .line 2266838
    invoke-super {p0, p1}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->b(Z)Z

    move-result v0

    .line 2266839
    iget-object v1, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->n:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->k:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    invoke-virtual {v1, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 2266840
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->k:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    .line 2266841
    iget-object v1, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->n:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->b()Z

    .line 2266842
    return v0
.end method

.method public final e()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2266837
    const-class v0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;

    return-object v0
.end method

.method public final k()V
    .locals 0

    .prologue
    .line 2266836
    return-void
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2266835
    const/4 v0, 0x0

    return v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2266831
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2266832
    iget-object v0, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->k:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    if-eqz v0, :cond_0

    .line 2266833
    iget-object v0, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->k:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2266834
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x1d0d9571

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2266826
    iget-object v0, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->k:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    if-nez v0, :cond_0

    .line 2266827
    const/4 v0, 0x0

    const/16 v2, 0x2b

    const v3, -0x45ffb6ca

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2266828
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->k:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    .line 2266829
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v2

    .line 2266830
    const v2, -0x6357287a

    invoke-static {v2, v1}, LX/02F;->f(II)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3792b7cf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2266815
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onDestroy()V

    .line 2266816
    iget-object v1, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->j:LX/Bnj;

    if-eqz v1, :cond_0

    .line 2266817
    iget-object v1, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->j:LX/Bnj;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->l:LX/Bnu;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2266818
    iget-object v1, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->j:LX/Bnj;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->m:LX/Bno;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2266819
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x1338c7c1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7811aa2f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2266820
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onDestroyView()V

    .line 2266821
    iget-object v1, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->k:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    if-eqz v1, :cond_0

    .line 2266822
    iget-object v1, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->k:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 2266823
    iget-object v1, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->k:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 2266824
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->k:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    .line 2266825
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x23008d85

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
