.class public Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;
.super Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;
.source ""


# static fields
.field private static final u:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public A:Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;

.field public B:I

.field public C:Z

.field public final D:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final E:LX/Ff2;

.field private final F:LX/Ff1;

.field private final G:LX/Ff3;

.field public i:LX/CvY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/DxK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/Fex;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dv9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/23R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dvh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/D3w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/FZb;

.field public w:LX/DxJ;

.field public x:Landroid/view/View;

.field public y:Landroid/view/View;

.field public z:LX/EQC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2267203
    new-instance v0, LX/Fez;

    invoke-direct {v0}, LX/Fez;-><init>()V

    sput-object v0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->u:LX/0QK;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2267196
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;-><init>()V

    .line 2267197
    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->C:Z

    .line 2267198
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->D:Ljava/util/Map;

    .line 2267199
    new-instance v0, LX/Ff2;

    invoke-direct {v0, p0}, LX/Ff2;-><init>(Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;)V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->E:LX/Ff2;

    .line 2267200
    new-instance v0, LX/Ff1;

    invoke-direct {v0, p0}, LX/Ff1;-><init>(Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;)V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->F:LX/Ff1;

    .line 2267201
    new-instance v0, LX/Ff3;

    invoke-direct {v0, p0}, LX/Ff3;-><init>(Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;)V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->G:LX/Ff3;

    .line 2267202
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2267195
    const-string v0, "graph_search_results_page_pandora_photo"

    return-object v0
.end method

.method public final a(LX/FZb;)V
    .locals 0

    .prologue
    .line 2267193
    iput-object p1, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->v:LX/FZb;

    .line 2267194
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    .line 2267190
    invoke-super {p0, p1}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->a(Landroid/os/Bundle;)V

    .line 2267191
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v3

    check-cast v3, LX/CvY;

    const-class v4, LX/DxK;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/DxK;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-class v6, LX/Fex;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/Fex;

    const/16 v7, 0x2e8a

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xf2f

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2e95

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/D3w;->b(LX/0QB;)LX/D3w;

    move-result-object v10

    check-cast v10, LX/D3w;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v12

    check-cast v12, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p1

    check-cast p1, LX/0wM;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v3, v2, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->i:LX/CvY;

    iput-object v4, v2, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->j:LX/DxK;

    iput-object v5, v2, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->k:Ljava/lang/String;

    iput-object v6, v2, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->l:LX/Fex;

    iput-object v7, v2, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->m:LX/0Ot;

    iput-object v8, v2, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->n:LX/0Ot;

    iput-object v9, v2, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->o:LX/0Ot;

    iput-object v10, v2, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->p:LX/D3w;

    iput-object v11, v2, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->q:LX/0Uh;

    iput-object v12, v2, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->r:Lcom/facebook/content/SecureContextHelper;

    iput-object p1, v2, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->s:LX/0wM;

    iput-object v0, v2, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->t:LX/0ad;

    .line 2267192
    return-void
.end method

.method public final e()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2267189
    const-class v0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    return-object v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 2267185
    iget-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->w:LX/DxJ;

    .line 2267186
    const/4 p0, 0x1

    iput-boolean p0, v0, LX/DxJ;->p:Z

    .line 2267187
    invoke-virtual {v0}, LX/Dvb;->d()V

    .line 2267188
    return-void
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2267184
    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->C:Z

    return v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x92dfffb

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2267126
    new-instance v8, Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v8, v0}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2267127
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v8, v0}, Lcom/facebook/widget/CustomFrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2267128
    new-instance v9, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v9, v0}, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;-><init>(Landroid/content/Context;)V

    .line 2267129
    const v0, 0x7f0d00c3

    invoke-virtual {v9, v0}, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->setId(I)V

    .line 2267130
    iget-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->q:LX/0Uh;

    sget v1, LX/2SU;->k:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->u:LX/0QK;

    .line 2267131
    :goto_0
    iget-object v1, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->j:LX/DxK;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->l:LX/Fex;

    iget-object v3, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->G:LX/Ff3;

    .line 2267132
    iget-object v4, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v4, v4

    .line 2267133
    iget-object v5, v4, Lcom/facebook/search/results/model/SearchResultsMutableContext;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v4, v5

    .line 2267134
    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const v5, -0x2238d5d

    if-ne v4, v5, :cond_5

    const/4 v4, 0x1

    :goto_1
    move v4, v4

    .line 2267135
    invoke-virtual {v2, v0, v3, v4}, LX/Fex;->a(LX/0QK;LX/0TF;Z)LX/Few;

    move-result-object v2

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iget-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dvf;

    invoke-virtual {v1, v2, v3, v4, v0}, LX/DxK;->a(LX/Dcc;Ljava/lang/Boolean;Ljava/lang/Boolean;LX/Dvf;)LX/DxJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->w:LX/DxJ;

    .line 2267136
    iget-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->w:LX/DxJ;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->k:Ljava/lang/String;

    new-instance v2, Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;

    .line 2267137
    iget-object v3, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v3, v3

    .line 2267138
    invoke-virtual {v3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v3

    .line 2267139
    iget-object v4, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v4, v4

    .line 2267140
    iget-object v5, v4, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v4, v5

    .line 2267141
    iget-object v4, v4, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "LoadScreenImagesSearch"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, LX/Dvb;->a(Ljava/lang/String;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;Ljava/lang/String;ZZZ)V

    .line 2267142
    iget-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->w:LX/DxJ;

    new-instance v1, LX/Ff0;

    invoke-direct {v1, p0}, LX/Ff0;-><init>(Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;)V

    invoke-virtual {v0, v1}, LX/DxJ;->a(LX/Dce;)V

    .line 2267143
    iget-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->w:LX/DxJ;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->E:LX/Ff2;

    invoke-virtual {v0, v1}, LX/DxJ;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2267144
    new-instance v0, LX/DxF;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->w:LX/DxJ;

    invoke-direct {v0, v1}, LX/DxF;-><init>(LX/Dvb;)V

    .line 2267145
    new-instance v1, LX/EQC;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/EQC;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->z:LX/EQC;

    .line 2267146
    iget-object v1, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->z:LX/EQC;

    invoke-virtual {v1}, LX/EQC;->b()V

    .line 2267147
    iget-object v1, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->z:LX/EQC;

    invoke-virtual {v9, v1}, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->addFooterView(Landroid/view/View;)V

    .line 2267148
    invoke-virtual {v9, v0}, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2267149
    iput-object v0, v9, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->a:LX/DxF;

    .line 2267150
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2267151
    invoke-virtual {v8, v9, v0}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2267152
    const v0, 0x7f03127b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v8, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->x:Landroid/view/View;

    .line 2267153
    iget-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->x:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2267154
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2267155
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 2267156
    iget-object v1, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->x:Landroid/view/View;

    invoke-virtual {v8, v1, v0}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2267157
    iget-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->t:LX/0ad;

    sget-short v1, LX/100;->x:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2267158
    const v0, 0x7f0312fb

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->y:Landroid/view/View;

    .line 2267159
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-gt v0, v1, :cond_2

    .line 2267160
    iget-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->y:Landroid/view/View;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a04ba

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2267161
    :goto_2
    iget-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->y:Landroid/view/View;

    const v1, 0x7f0d1732

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->A:Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;

    .line 2267162
    iget-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->A:Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04b5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->a(I)V

    .line 2267163
    iget-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->t:LX/0ad;

    sget-short v1, LX/100;->v:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2267164
    iget-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->y:Landroid/view/View;

    const v1, 0x7f0d2c28

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2267165
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00a8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2267166
    iget-object v1, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->t:LX/0ad;

    sget-char v2, LX/100;->u:C

    const-string v3, ""

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2267167
    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 2267168
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2267169
    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    const-string v1, ""

    :goto_3
    aput-object v1, v3, v4

    invoke-static {v2, v3}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2267170
    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2267171
    iget-object v2, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->s:LX/0wM;

    const v3, 0x7f0217c2

    const v4, -0xc4a668

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2267172
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2267173
    const/4 v3, 0x0

    aget-object v3, v1, v3

    const/4 v4, 0x2

    aget-object v4, v1, v4

    const/4 v5, 0x3

    aget-object v1, v1, v5

    invoke-virtual {v0, v3, v2, v4, v1}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2267174
    :cond_0
    :goto_4
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2267175
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 2267176
    iget-object v1, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->y:Landroid/view/View;

    invoke-virtual {v8, v1, v0}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2267177
    const v0, 0x32a64b90

    invoke-static {v0, v7}, LX/02F;->f(II)V

    return-object v8

    .line 2267178
    :cond_1
    sget-object v0, LX/3J4;->INSTANCE:LX/3J4;

    move-object v0, v0

    .line 2267179
    goto/16 :goto_0

    .line 2267180
    :cond_2
    iget-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->y:Landroid/view/View;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a04ba

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 2267181
    :cond_3
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2267182
    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 2267183
    :cond_4
    new-instance v0, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->y:Landroid/view/View;

    goto :goto_4

    :cond_5
    const/4 v4, 0x0

    goto/16 :goto_1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x74c18bfc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2267109
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onDestroyView()V

    .line 2267110
    iget-object v1, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->w:LX/DxJ;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->E:LX/Ff2;

    invoke-virtual {v1, v2}, LX/DxJ;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2267111
    iget-object v1, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->w:LX/DxJ;

    invoke-virtual {v1}, LX/Dvb;->j()V

    .line 2267112
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->C:Z

    .line 2267113
    iget-object v1, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->D:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 2267114
    const/16 v1, 0x2b

    const v2, 0xdf57f2d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x26954461

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2267123
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onPause()V

    .line 2267124
    iget-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dv9;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->F:LX/Ff1;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2267125
    const/16 v0, 0x2b

    const v2, -0x7c36170b

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3dd2a676

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2267120
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onResume()V

    .line 2267121
    iget-object v0, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dv9;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->F:LX/Ff1;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2267122
    const/16 v0, 0x2b

    const v2, 0x6209f8b5

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7813a275

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2267115
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onStart()V

    .line 2267116
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2267117
    if-eqz v0, :cond_0

    .line 2267118
    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/1ZF;->k_(Z)V

    .line 2267119
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x2dffd86e

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
