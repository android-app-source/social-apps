.class public abstract Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/Fdv;


# instance fields
.field public a:LX/0gh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/CvY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/8i2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/D0Q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Be6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field private j:Z

.field private k:Z

.field private l:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2264420
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2264421
    new-instance v0, Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-direct {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2264422
    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->j:Z

    .line 2264423
    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->k:Z

    .line 2264424
    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->l:Z

    return-void
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 2264406
    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->k:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Landroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 2264425
    const-string v0, "query_function"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "query_title"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(LX/CwB;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2264426
    invoke-interface {p1}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2264427
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->e()Ljava/lang/Class;

    .line 2264428
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2264429
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->e()Ljava/lang/Class;

    .line 2264430
    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->j:Z

    .line 2264431
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->e()Ljava/lang/Class;

    .line 2264432
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a(LX/CwB;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;)V

    .line 2264433
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->e()Ljava/lang/Class;

    .line 2264434
    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->j:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2264435
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->e()Ljava/lang/Class;

    .line 2264436
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->k()V

    .line 2264437
    iput-boolean v2, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->j:Z

    .line 2264438
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 2264439
    goto :goto_0
.end method

.method public a(LX/FZb;)V
    .locals 0

    .prologue
    .line 2264440
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 2264441
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2264442
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->e()Ljava/lang/Class;

    .line 2264443
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;

    invoke-static {v0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v3

    check-cast v3, LX/0gh;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v4

    check-cast v4, Landroid/os/Handler;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v5

    check-cast v5, LX/CvY;

    invoke-static {v0}, LX/8i2;->a(LX/0QB;)LX/8i2;

    move-result-object v6

    check-cast v6, LX/8i2;

    invoke-static {v0}, LX/D0Q;->a(LX/0QB;)LX/D0Q;

    move-result-object v7

    check-cast v7, LX/D0Q;

    invoke-static {v0}, LX/Be7;->a(LX/0QB;)LX/Be6;

    move-result-object v8

    check-cast v8, LX/Be6;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v3, v2, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->a:LX/0gh;

    iput-object v4, v2, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->b:Landroid/os/Handler;

    iput-object v5, v2, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->c:LX/CvY;

    iput-object v6, v2, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->d:LX/8i2;

    iput-object v7, v2, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->e:LX/D0Q;

    iput-object v8, v2, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->f:LX/Be6;

    iput-object v9, v2, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->g:LX/0Uh;

    iput-object v0, v2, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->h:LX/0ad;

    .line 2264444
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->f:LX/Be6;

    invoke-virtual {v0}, LX/Be6;->a()V

    .line 2264445
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->g:LX/0Uh;

    sget v1, LX/2SU;->A:I

    invoke-virtual {v0, v1, v12}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "SEARCH_RESULTS_CONTEXT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2264446
    :goto_0
    if-eqz v0, :cond_0

    .line 2264447
    iput-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2264448
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v0

    .line 2264449
    if-nez v3, :cond_3

    .line 2264450
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->e()Ljava/lang/Class;

    .line 2264451
    :cond_1
    :goto_1
    return-void

    .line 2264452
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 2264453
    :cond_3
    invoke-static {v3}, LX/D0Q;->a(Landroid/os/Bundle;)LX/7BH;

    move-result-object v0

    .line 2264454
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2264455
    iput-object v0, v1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a:LX/7BH;

    .line 2264456
    invoke-static {v3}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->c(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2264457
    const-string v0, "query_title"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2264458
    const-string v0, "query_function"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2264459
    const-string v0, "query_vertical"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2264460
    const-string v0, "exact_match"

    invoke-virtual {v3, v0, v12}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 2264461
    const-string v0, "display_style"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v7

    .line 2264462
    const-string v0, "source"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2264463
    const-string v0, "graph_search_scoped_entity_type"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/103;

    .line 2264464
    const-string v1, "graph_search_scoped_entity_id"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 2264465
    const-string v1, "graph_search_scoped_entity_name"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2264466
    const-string v1, "graph_search_query_modifiers"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    if-eqz v1, :cond_4

    const-string v1, "graph_search_query_modifiers"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-static {v1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v1

    .line 2264467
    :goto_2
    new-instance v11, LX/CwD;

    invoke-direct {v11}, LX/CwD;-><init>()V

    .line 2264468
    iput-object v2, v11, LX/CwD;->a:Ljava/lang/String;

    .line 2264469
    move-object v11, v11

    .line 2264470
    iput-object v2, v11, LX/CwD;->c:Ljava/lang/String;

    .line 2264471
    move-object v2, v11

    .line 2264472
    iput-object v4, v2, LX/CwD;->b:Ljava/lang/String;

    .line 2264473
    move-object v2, v2

    .line 2264474
    iput-object v5, v2, LX/CwD;->d:Ljava/lang/String;

    .line 2264475
    move-object v2, v2

    .line 2264476
    iput-object v6, v2, LX/CwD;->e:Ljava/lang/Boolean;

    .line 2264477
    move-object v4, v2

    .line 2264478
    if-eqz v7, :cond_5

    invoke-static {v7}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2264479
    :goto_3
    iput-object v2, v4, LX/CwD;->f:LX/0Px;

    .line 2264480
    move-object v2, v4

    .line 2264481
    iput-object v1, v2, LX/CwD;->g:LX/0P1;

    .line 2264482
    move-object v1, v2

    .line 2264483
    iput-object v0, v1, LX/CwD;->k:LX/103;

    .line 2264484
    move-object v0, v1

    .line 2264485
    iput-object v9, v0, LX/CwD;->i:Ljava/lang/String;

    .line 2264486
    move-object v0, v0

    .line 2264487
    iput-object v10, v0, LX/CwD;->j:Ljava/lang/String;

    .line 2264488
    move-object v0, v0

    .line 2264489
    invoke-virtual {v0}, LX/CwD;->j()LX/CwE;

    move-result-object v2

    .line 2264490
    invoke-static {v8}, LX/8ci;->a(Ljava/lang/String;)LX/8ci;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    sget-object v1, LX/8ci;->K:LX/8ci;

    invoke-virtual {v0, v1}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8ci;

    .line 2264491
    const-string v1, "typeahead_session_id"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2264492
    const-string v1, "candidate_session_id"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2264493
    const-string v1, "results_query_role"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v6

    .line 2264494
    const-string v1, "results_query_type"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const-string v7, "results_query_type"

    invoke-virtual {v3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    .line 2264495
    :goto_4
    new-instance v3, Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    invoke-direct {v3, v4, v5}, Lcom/facebook/search/logging/api/SearchTypeaheadSession;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3, v0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->a(LX/CwB;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;)V

    .line 2264496
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2264497
    iput-object v6, v0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 2264498
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2264499
    if-eqz v1, :cond_7

    :goto_5
    iput-object v1, v0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2264500
    goto/16 :goto_1

    .line 2264501
    :cond_4
    sget-object v1, LX/0Rg;->a:LX/0Rg;

    move-object v1, v1

    .line 2264502
    goto :goto_2

    .line 2264503
    :cond_5
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2264504
    goto :goto_3

    .line 2264505
    :cond_6
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v12}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    goto :goto_4

    .line 2264506
    :cond_7
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    goto :goto_5
.end method

.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2264512
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2264513
    iget-object p0, v0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->f:Ljava/lang/String;

    move-object v0, p0

    .line 2264514
    invoke-static {v0}, LX/CvY;->b(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public b(Z)Z
    .locals 3

    .prologue
    .line 2264507
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->c:LX/CvY;

    .line 2264508
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v2, v0

    .line 2264509
    if-eqz p1, :cond_0

    sget-object v0, LX/7CL;->UP_BUTTON:LX/7CL;

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/7CL;)V

    .line 2264510
    const/4 v0, 0x0

    return v0

    .line 2264511
    :cond_0
    sget-object v0, LX/7CL;->BACK_BUTTON:LX/7CL;

    goto :goto_0
.end method

.method public final d()Landroid/support/v4/app/Fragment;
    .locals 0

    .prologue
    .line 2264515
    return-object p0
.end method

.method public e()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2264516
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public abstract k()V
.end method

.method public abstract l()Z
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2264414
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public n()V
    .locals 3

    .prologue
    .line 2264415
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->e()Ljava/lang/Class;

    .line 2264416
    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->l:Z

    if-nez v0, :cond_0

    .line 2264417
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->b:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment$1;

    invoke-direct {v1, p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment$1;-><init>(Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;)V

    const v2, 0x4263edb

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2264418
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->r()V

    .line 2264419
    return-void
.end method

.method public o()V
    .locals 0

    .prologue
    .line 2264353
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->e()Ljava/lang/Class;

    .line 2264354
    return-void
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x10626e07

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2264355
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->k:Z

    .line 2264356
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->e()Ljava/lang/Class;

    .line 2264357
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2264358
    const/16 v1, 0x2b

    const v2, -0x47729c0e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x645efa3f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2264359
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2264360
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->o()V

    .line 2264361
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2264362
    const/16 v1, 0x2b

    const v2, 0x2f5cfcbd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x557fc23a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2264363
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2264364
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2264365
    const/16 v1, 0x2b

    const v2, 0x3bdcc552

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2264366
    :goto_0
    return-void

    .line 2264367
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->n()V

    .line 2264368
    const v1, 0x3ba0f5ea

    invoke-static {v1, v0}, LX/02F;->f(II)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2264369
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2264370
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->g:LX/0Uh;

    sget v1, LX/2SU;->A:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2264371
    const-string v0, "SEARCH_RESULTS_CONTEXT"

    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2264372
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x507e52f5

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2264373
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2264374
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->e()Ljava/lang/Class;

    .line 2264375
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v0

    .line 2264376
    if-eqz v2, :cond_1

    invoke-static {v2}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->c(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->l:Z

    .line 2264377
    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->l:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 2264378
    if-nez v2, :cond_4

    .line 2264379
    :goto_1
    move v0, v0

    .line 2264380
    if-eqz v0, :cond_2

    .line 2264381
    :cond_0
    const v0, 0x6f9b7a7f

    invoke-static {v0, v1}, LX/02F;->f(II)V

    .line 2264382
    :goto_2
    return-void

    .line 2264383
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2264384
    :cond_2
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2264385
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->m()Ljava/lang/String;

    move-result-object v2

    .line 2264386
    if-eqz v0, :cond_3

    if-eqz v2, :cond_3

    .line 2264387
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->e()Ljava/lang/Class;

    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->m()Ljava/lang/String;

    .line 2264388
    invoke-interface {v0, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2264389
    :cond_3
    const v0, 0x617fa37e

    invoke-static {v0, v1}, LX/02F;->f(II)V

    goto :goto_2

    :cond_4
    const-string v3, "use_open_search_bar"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_1
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2264390
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2264391
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->e()Ljava/lang/Class;

    .line 2264392
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->k:Z

    .line 2264393
    return-void
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 2264394
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->j:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Lcom/facebook/search/results/model/SearchResultsMutableContext;
    .locals 1

    .prologue
    .line 2264395
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    return-object v0
.end method

.method public final r()V
    .locals 1

    .prologue
    .line 2264396
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2264397
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->e()Ljava/lang/Class;

    .line 2264398
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->k()V

    .line 2264399
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->j:Z

    .line 2264400
    :cond_0
    return-void
.end method

.method public final s()Landroid/content/Context;
    .locals 2

    .prologue
    .line 2264401
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2264402
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2264403
    invoke-static {v0, v1}, LX/D0Q;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/view/ContextThemeWrapper;

    move-result-object v0

    .line 2264404
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->d:LX/8i2;

    invoke-virtual {v1, v0}, LX/8i2;->a(Landroid/content/Context;)V

    .line 2264405
    return-object v0
.end method

.method public final setUserVisibleHint(Z)V
    .locals 1

    .prologue
    .line 2264407
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 2264408
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v0

    .line 2264409
    if-eqz v0, :cond_0

    .line 2264410
    if-eqz p1, :cond_1

    .line 2264411
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->n()V

    .line 2264412
    :cond_0
    :goto_0
    return-void

    .line 2264413
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->o()V

    goto :goto_0
.end method
