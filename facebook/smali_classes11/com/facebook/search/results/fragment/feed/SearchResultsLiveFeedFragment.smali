.class public Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;
.super Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;
.source ""


# static fields
.field public static final r:LX/1PT;


# instance fields
.field public A:LX/1Qq;

.field public B:LX/1Jg;

.field public C:LX/Fg2;

.field public D:Z

.field public E:LX/Fje;

.field public F:LX/0g8;

.field public G:Z

.field public H:Z

.field public I:LX/2i4;

.field public J:I

.field public K:Landroid/view/View;

.field public i:LX/CzF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/1DS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/Cxt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Db;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fg3;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/2Sc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/CvY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/1DL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/Feo;

.field public t:Landroid/content/Context;

.field public u:Landroid/view/ViewGroup;

.field private v:I

.field public final w:LX/0fx;

.field public final x:LX/1DI;

.field public final y:Landroid/view/View$OnClickListener;

.field public z:LX/CzE;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2266771
    new-instance v0, LX/Fei;

    invoke-direct {v0}, LX/Fei;-><init>()V

    sput-object v0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->r:LX/1PT;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2266762
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;-><init>()V

    .line 2266763
    iput v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->v:I

    .line 2266764
    new-instance v0, LX/Fej;

    invoke-direct {v0, p0}, LX/Fej;-><init>(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->w:LX/0fx;

    .line 2266765
    new-instance v0, LX/Fek;

    invoke-direct {v0, p0}, LX/Fek;-><init>(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->x:LX/1DI;

    .line 2266766
    new-instance v0, LX/Fel;

    invoke-direct {v0, p0}, LX/Fel;-><init>(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->y:Landroid/view/View$OnClickListener;

    .line 2266767
    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->D:Z

    .line 2266768
    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->G:Z

    .line 2266769
    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->H:Z

    .line 2266770
    return-void
.end method

.method public static B(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)V
    .locals 1

    .prologue
    .line 2266760
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->A:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2266761
    return-void
.end method

.method public static a$redex0(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;LX/0Px;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2266753
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2266754
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;

    .line 2266755
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2266756
    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2266757
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2266758
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2266759
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic k(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)I
    .locals 2

    .prologue
    .line 2266752
    iget v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->v:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->v:I

    return v0
.end method

.method public static t(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)Z
    .locals 1

    .prologue
    .line 2266751
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static u(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)Z
    .locals 1

    .prologue
    .line 2266644
    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->D:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->z:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static w(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)V
    .locals 1

    .prologue
    .line 2266748
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->C:LX/Fg2;

    invoke-interface {v0}, LX/Fg2;->c()V

    .line 2266749
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->H:Z

    .line 2266750
    return-void
.end method

.method public static y(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)V
    .locals 2

    .prologue
    .line 2266745
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->e()Ljava/lang/Class;

    .line 2266746
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->C:LX/Fg2;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->z:LX/CzE;

    invoke-virtual {v1}, LX/CzE;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/Fg2;->a(Ljava/lang/String;)V

    .line 2266747
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2266772
    const-string v0, "graph_search_results_live_conversation_fragment"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2266738
    invoke-super {p0, p1}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->a(Landroid/os/Bundle;)V

    .line 2266739
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    const-class v3, LX/CzF;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/CzF;

    invoke-static {v0}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v4

    check-cast v4, LX/1DS;

    const/16 v5, 0x3434

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const-class v6, LX/Cxt;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/Cxt;

    const/16 v7, 0x6c9

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x335d

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v9

    check-cast v9, LX/2Sc;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object p1

    check-cast p1, LX/CvY;

    invoke-static {v0}, LX/1DL;->a(LX/0QB;)LX/1DL;

    move-result-object v0

    check-cast v0, LX/1DL;

    iput-object v3, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->i:LX/CzF;

    iput-object v4, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->j:LX/1DS;

    iput-object v5, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->k:LX/0Ot;

    iput-object v6, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->l:LX/Cxt;

    iput-object v7, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->m:LX/0Or;

    iput-object v8, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->n:LX/0Or;

    iput-object v9, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->o:LX/2Sc;

    iput-object p1, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->p:LX/CvY;

    iput-object v0, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->q:LX/1DL;

    .line 2266740
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->i:LX/CzF;

    .line 2266741
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2266742
    invoke-virtual {v0, v1}, LX/CzF;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;)LX/CzE;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->z:LX/CzE;

    .line 2266743
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->q:LX/1DL;

    invoke-virtual {v0}, LX/1DL;->a()LX/1Jg;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->B:LX/1Jg;

    .line 2266744
    return-void
.end method

.method public final e()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2266737
    const-class v0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    return-object v0
.end method

.method public final k()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2266714
    invoke-static {p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->t(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2266715
    :goto_0
    return-void

    .line 2266716
    :cond_0
    iput v4, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->v:I

    .line 2266717
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->z:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->d()V

    .line 2266718
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    .line 2266719
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2266720
    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Fje;->setTextViewQueryString(Ljava/lang/String;)V

    .line 2266721
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->F:LX/0g8;

    invoke-interface {v0, v4}, LX/0g8;->g(I)V

    .line 2266722
    invoke-static {p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->B(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)V

    .line 2266723
    new-instance v0, LX/Ffx;

    invoke-direct {v0}, LX/Ffx;-><init>()V

    move-object v1, v0

    .line 2266724
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->C:LX/Fg2;

    if-eqz v0, :cond_1

    .line 2266725
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->C:LX/Fg2;

    invoke-interface {v0}, LX/Fg2;->b()V

    .line 2266726
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->n:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fg2;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->C:LX/Fg2;

    .line 2266727
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->C:LX/Fg2;

    .line 2266728
    iget-object v2, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v2, v2

    .line 2266729
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v3

    invoke-interface {v0, v2, v1, v3}, LX/Fg2;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/Ffx;LX/0am;)V

    .line 2266730
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->C:LX/Fg2;

    .line 2266731
    new-instance v1, LX/Fen;

    invoke-direct {v1, p0}, LX/Fen;-><init>(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)V

    move-object v1, v1

    .line 2266732
    invoke-interface {v0, v1}, LX/Fg2;->a(LX/Fee;)V

    .line 2266733
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->C:LX/Fg2;

    invoke-interface {v0}, LX/Fg2;->a()V

    .line 2266734
    iput-boolean v4, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->D:Z

    .line 2266735
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->e()Ljava/lang/Class;

    .line 2266736
    invoke-static {p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->w(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)V

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2266713
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->z:LX/CzE;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->z:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2266709
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v0, v0

    .line 2266710
    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082321

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2266711
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2266712
    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082320

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final n()V
    .locals 9

    .prologue
    .line 2266665
    invoke-static {p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->t(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2266666
    :goto_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->B:LX/1Jg;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->F:LX/0g8;

    invoke-interface {v0, v1}, LX/1Jg;->a(LX/0g8;)V

    .line 2266667
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->F:LX/0g8;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->B:LX/1Jg;

    invoke-interface {v1}, LX/1Jg;->a()LX/0fx;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    .line 2266668
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->p()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->C:LX/Fg2;

    if-eqz v0, :cond_2

    .line 2266669
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    .line 2266670
    iget-object v1, v0, LX/Fje;->B:LX/EQG;

    move-object v0, v1

    .line 2266671
    sget-object v1, LX/EQG;->LOADING:LX/EQG;

    if-ne v0, v1, :cond_0

    .line 2266672
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->z:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->size()I

    move-result v0

    if-nez v0, :cond_4

    .line 2266673
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->H:Z

    .line 2266674
    :cond_0
    :goto_1
    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->H:Z

    if-eqz v0, :cond_1

    .line 2266675
    invoke-static {p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->w(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)V

    .line 2266676
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->C:LX/Fg2;

    invoke-interface {v0}, LX/Fg2;->a()V

    .line 2266677
    :cond_2
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->n()V

    .line 2266678
    return-void

    .line 2266679
    :cond_3
    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->s:LX/Feo;

    invoke-virtual {v2}, LX/4nv;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, LX/Fje;

    iput-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    .line 2266680
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->s:LX/Feo;

    .line 2266681
    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    .line 2266682
    iget-object v3, v2, LX/Fje;->u:Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-object v2, v3

    .line 2266683
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/facebook/widget/FbSwipeRefreshLayout;->setEnabled(Z)V

    .line 2266684
    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    .line 2266685
    iget-object v3, v2, LX/Fje;->u:Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-object v2, v3

    .line 2266686
    new-instance v3, LX/Fem;

    invoke-direct {v3, p0}, LX/Fem;-><init>(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2266687
    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    iget-object v3, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->x:LX/1DI;

    .line 2266688
    iput-object v3, v2, LX/Fje;->S:LX/1DI;

    .line 2266689
    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    .line 2266690
    iget-object v3, v2, LX/Fje;->q:LX/0g8;

    move-object v2, v3

    .line 2266691
    iput-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->F:LX/0g8;

    .line 2266692
    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->l:LX/Cxt;

    const-string v3, "graph_search_results_page"

    iget-object v4, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->t:Landroid/content/Context;

    sget-object v5, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->r:LX/1PT;

    new-instance v6, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment$6;

    invoke-direct {v6, p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment$6;-><init>(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)V

    .line 2266693
    iget-object v7, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v7, v7

    .line 2266694
    iget-object v8, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->F:LX/0g8;

    invoke-static {v8}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v8

    invoke-virtual/range {v2 .. v8}, LX/Cxt;->a(Ljava/lang/String;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/1PY;)LX/Cxs;

    move-result-object v2

    .line 2266695
    iget-object v3, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->j:LX/1DS;

    iget-object v4, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->k:LX/0Ot;

    iget-object v5, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->z:LX/CzE;

    invoke-virtual {v3, v4, v5}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v3

    .line 2266696
    iput-object v2, v3, LX/1Ql;->f:LX/1PW;

    .line 2266697
    move-object v2, v3

    .line 2266698
    invoke-virtual {v2}, LX/1Ql;->e()LX/1Qq;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->A:LX/1Qq;

    .line 2266699
    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->F:LX/0g8;

    iget-object v3, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->w:LX/0fx;

    invoke-interface {v2, v3}, LX/0g8;->a(LX/0fx;)V

    .line 2266700
    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->F:LX/0g8;

    iget-object v3, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->A:LX/1Qq;

    invoke-interface {v2, v3}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2266701
    iget-object v3, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->F:LX/0g8;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->m:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Db;

    invoke-virtual {v2}, LX/1Db;->a()LX/1St;

    move-result-object v2

    invoke-interface {v3, v2}, LX/0g8;->a(LX/1St;)V

    .line 2266702
    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->B:LX/1Jg;

    iget-object v3, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->A:LX/1Qq;

    invoke-interface {v3}, LX/1Qr;->e()LX/1R4;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Jg;->a(LX/1R4;)V

    .line 2266703
    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->t:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030692

    iget-object v4, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->u:Landroid/view/ViewGroup;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2266704
    const v3, 0x7f0d0d53

    invoke-static {v2, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->K:Landroid/view/View;

    .line 2266705
    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->K:Landroid/view/View;

    iget-object v3, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->y:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 2266706
    :cond_4
    invoke-static {p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->u(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2266707
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    sget-object v1, LX/EQG;->LOADING_MORE:LX/EQG;

    invoke-virtual {v0, v1}, LX/Fje;->setState(LX/EQG;)V

    goto/16 :goto_1

    .line 2266708
    :cond_5
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    sget-object v1, LX/EQG;->LOADING_FINISHED:LX/EQG;

    invoke-virtual {v0, v1}, LX/Fje;->setState(LX/EQG;)V

    goto/16 :goto_1
.end method

.method public final o()V
    .locals 2

    .prologue
    .line 2266659
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->C:LX/Fg2;

    if-eqz v0, :cond_0

    .line 2266660
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->C:LX/Fg2;

    invoke-interface {v0}, LX/Fg2;->b()V

    .line 2266661
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->B:LX/1Jg;

    invoke-interface {v0}, LX/1Jg;->b()V

    .line 2266662
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->F:LX/0g8;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->B:LX/1Jg;

    invoke-interface {v1}, LX/1Jg;->a()LX/0fx;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0g8;->c(LX/0fx;)V

    .line 2266663
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->o()V

    .line 2266664
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x1609eb17

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2266655
    iput-object p2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->u:Landroid/view/ViewGroup;

    .line 2266656
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->s()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->t:Landroid/content/Context;

    .line 2266657
    new-instance v1, LX/Feo;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->t:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/Feo;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->s:LX/Feo;

    .line 2266658
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->s:LX/Feo;

    const/16 v2, 0x2b

    const v3, 0x1399dc2d

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x703e8ba2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2266645
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->F:LX/0g8;

    if-eqz v1, :cond_0

    .line 2266646
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->F:LX/0g8;

    invoke-interface {v1, v2}, LX/0g8;->a(LX/0fx;)V

    .line 2266647
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->F:LX/0g8;

    invoke-interface {v1, v2}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2266648
    :cond_0
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->K:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 2266649
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->K:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2266650
    :cond_1
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->A:LX/1Qq;

    if-eqz v1, :cond_2

    .line 2266651
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->A:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2266652
    :cond_2
    iput-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    .line 2266653
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onDestroyView()V

    .line 2266654
    const/16 v1, 0x2b

    const v2, -0x45e9e56a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
