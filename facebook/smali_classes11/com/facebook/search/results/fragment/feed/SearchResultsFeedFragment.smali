.class public Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;
.super Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;
.source ""

# interfaces
.implements LX/0fn;
.implements LX/5OO;
.implements LX/FcC;
.implements LX/FdZ;
.implements LX/Fe2;
.implements LX/1DI;


# static fields
.field private static final L:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

.field public static final M:Ljava/lang/String;


# instance fields
.field public A:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/1k3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/1nD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/CzG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/Ffw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/193;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/BwE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/8ht;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private N:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public P:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private Q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5Mk;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private R:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0x9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private S:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public T:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Sd;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public U:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FcD;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private V:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fg7;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public W:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public X:LX/CvM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Y:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CyY;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final Z:LX/Fea;

.field public aA:Z

.field public aB:Z

.field public aC:LX/CyE;

.field private aD:Ljava/lang/String;

.field private aE:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

.field private final aa:LX/6Ve;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/6Ve",
            "<",
            "LX/EJ1;",
            ">;"
        }
    .end annotation
.end field

.field public ab:LX/CzE;

.field private ac:LX/FeS;

.field public ad:LX/Fje;

.field private ae:LX/EQG;

.field public af:LX/0g8;

.field public ag:LX/1Qq;

.field private ah:LX/1Jg;

.field public ai:LX/CzK;

.field public aj:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ak:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private al:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;"
        }
    .end annotation
.end field

.field private am:LX/FeY;

.field public an:Z

.field public ao:Z

.field public ap:Z

.field public aq:Z

.field public ar:I

.field public as:I

.field private at:LX/6Vi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/6Vi",
            "<",
            "LX/EJ1;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private au:LX/CvQ;

.field public av:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/FgP;",
            ">;"
        }
    .end annotation
.end field

.field private aw:Z

.field public ax:LX/195;

.field public ay:LX/Ffv;

.field public az:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field public i:LX/CzF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/FeT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/1K9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/FgH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/Cve;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/9yN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/FeZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/FgF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/CvY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/1DS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/SearchResultsFeedRootPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/Cxp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Db;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/FgQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/FgM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/CvR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/CvP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/1DL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2266409
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->LEARNING_NUX_SERP_SUCCESS:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    sput-object v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->L:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    .line 2266410
    const-class v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->M:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2266411
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;-><init>()V

    .line 2266412
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2266413
    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->N:LX/0Ot;

    .line 2266414
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2266415
    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->O:LX/0Ot;

    .line 2266416
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2266417
    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->P:LX/0Ot;

    .line 2266418
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2266419
    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->Q:LX/0Ot;

    .line 2266420
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2266421
    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->R:LX/0Ot;

    .line 2266422
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2266423
    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->S:LX/0Ot;

    .line 2266424
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2266425
    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->T:LX/0Ot;

    .line 2266426
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2266427
    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->U:LX/0Ot;

    .line 2266428
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2266429
    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->V:LX/0Ot;

    .line 2266430
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->W:Ljava/util/Map;

    .line 2266431
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2266432
    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->Y:LX/0Ot;

    .line 2266433
    new-instance v0, LX/Fea;

    invoke-direct {v0, p0}, LX/Fea;-><init>(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->Z:LX/Fea;

    .line 2266434
    new-instance v0, LX/Feh;

    invoke-direct {v0, p0}, LX/Feh;-><init>(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V

    move-object v0, v0

    .line 2266435
    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aa:LX/6Ve;

    .line 2266436
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2266437
    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->al:LX/0Px;

    .line 2266438
    iput-boolean v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->an:Z

    .line 2266439
    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ao:Z

    .line 2266440
    iput-boolean v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ap:Z

    .line 2266441
    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aq:Z

    .line 2266442
    iput v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ar:I

    .line 2266443
    iput v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->as:I

    .line 2266444
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->av:LX/0am;

    .line 2266445
    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aA:Z

    .line 2266446
    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aB:Z

    return-void
.end method

.method private C()LX/0fx;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2266447
    new-instance v0, LX/Feg;

    invoke-direct {v0, p0}, LX/Feg;-><init>(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V

    return-object v0
.end method

.method public static E(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)LX/FgM;
    .locals 2

    .prologue
    .line 2266448
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v0, v0

    .line 2266449
    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->h()LX/0P1;

    move-result-object v0

    sget-object v1, LX/7B4;->GROUP_COMMERCE:LX/7B4;

    invoke-virtual {v1}, LX/7B4;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2266450
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->w:LX/FgM;

    .line 2266451
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private F()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2266452
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2266453
    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->I:LX/0Uh;

    sget v3, LX/2SU;->K:I

    invoke-virtual {v2, v3, v0}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v2, v3, :cond_0

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/7BG;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static synthetic a(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 2266454
    iput-object p1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ak:Ljava/lang/String;

    return-object p1
.end method

.method private static a(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;LX/CzF;LX/FeT;LX/1K9;LX/FgH;LX/Cve;LX/9yN;LX/0Ot;LX/FeZ;LX/FgF;LX/CvY;LX/0Ot;LX/0Ot;LX/1DS;LX/0Ot;LX/Cxp;LX/0Or;LX/FgQ;LX/FgM;LX/CvR;LX/CvP;LX/1DL;LX/0iA;LX/0ad;LX/0Ot;LX/1k3;LX/0Ot;LX/0Sh;LX/1nD;LX/0Ot;LX/0Ot;LX/CzG;LX/Ffw;LX/193;LX/0Ot;LX/0Uh;LX/BwE;LX/0Ot;LX/8ht;LX/CvM;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;",
            "LX/CzF;",
            "LX/FeT;",
            "LX/1K9;",
            "LX/FgH;",
            "LX/Cve;",
            "LX/9yN;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/FeZ;",
            "LX/FgF;",
            "LX/CvY;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;",
            "LX/1DS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/SearchResultsFeedRootPartDefinition;",
            ">;",
            "LX/Cxp;",
            "LX/0Or",
            "<",
            "LX/1Db;",
            ">;",
            "LX/FgQ;",
            "LX/FgM;",
            "LX/CvR;",
            "LX/CvP;",
            "LX/1DL;",
            "LX/0iA;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/5Mk;",
            ">;",
            "LX/1k3;",
            "LX/0Ot",
            "<",
            "LX/0x9;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/1nD;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Sd;",
            ">;",
            "LX/CzG;",
            "LX/Ffw;",
            "LX/193;",
            "LX/0Ot",
            "<",
            "LX/FcD;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/BwE;",
            "LX/0Ot",
            "<",
            "LX/Fg7;",
            ">;",
            "LX/8ht;",
            "LX/CvM;",
            "LX/0Ot",
            "<",
            "LX/CyY;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2266455
    iput-object p1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->i:LX/CzF;

    iput-object p2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->j:LX/FeT;

    iput-object p3, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->k:LX/1K9;

    iput-object p4, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->l:LX/FgH;

    iput-object p5, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->m:LX/Cve;

    iput-object p6, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->n:LX/9yN;

    iput-object p7, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->N:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->o:LX/FeZ;

    iput-object p9, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->p:LX/FgF;

    iput-object p10, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->q:LX/CvY;

    iput-object p11, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->O:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->P:LX/0Ot;

    iput-object p13, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->r:LX/1DS;

    iput-object p14, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->s:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->t:LX/Cxp;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->u:LX/0Or;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->v:LX/FgQ;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->w:LX/FgM;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->x:LX/CvR;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->y:LX/CvP;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->z:LX/1DL;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->A:LX/0iA;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->B:LX/0ad;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->Q:LX/0Ot;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->C:LX/1k3;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->R:LX/0Ot;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->D:LX/0Sh;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->E:LX/1nD;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->S:LX/0Ot;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->T:LX/0Ot;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->F:LX/CzG;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->G:LX/Ffw;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->H:LX/193;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->U:LX/0Ot;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->I:LX/0Uh;

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->J:LX/BwE;

    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->V:LX/0Ot;

    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->K:LX/8ht;

    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->X:LX/CvM;

    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->Y:LX/0Ot;

    return-void
.end method

.method public static a(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;LX/EQG;)V
    .locals 5

    .prologue
    .line 2266456
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ae:LX/EQG;

    if-eq v0, p1, :cond_0

    .line 2266457
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->T:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Sd;

    sget-object v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->M:Ljava/lang/String;

    sget-object v2, LX/7CQ;->FETCH_STATE_CHANGED:LX/7CQ;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Old: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ae:LX/EQG;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", New: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/2Sd;->a(Ljava/lang/String;LX/7CQ;Ljava/lang/String;)V

    .line 2266458
    :cond_0
    iput-object p1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ae:LX/EQG;

    .line 2266459
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    if-eqz v0, :cond_1

    .line 2266460
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    invoke-virtual {v0, p1}, LX/Fje;->setState(LX/EQG;)V

    .line 2266461
    :cond_1
    return-void
.end method

.method public static a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2266462
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u:Ljava/lang/String;

    move-object v0, v0

    .line 2266463
    if-nez v0, :cond_0

    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2266464
    invoke-virtual {p0, p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->d(Ljava/lang/String;)V

    .line 2266465
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 44

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v42

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    const-class v3, LX/CzF;

    move-object/from16 v0, v42

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/CzF;

    const-class v4, LX/FeT;

    move-object/from16 v0, v42

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/FeT;

    invoke-static/range {v42 .. v42}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v5

    check-cast v5, LX/1K9;

    invoke-static/range {v42 .. v42}, LX/FgH;->a(LX/0QB;)LX/FgH;

    move-result-object v6

    check-cast v6, LX/FgH;

    invoke-static/range {v42 .. v42}, LX/Cve;->a(LX/0QB;)LX/Cve;

    move-result-object v7

    check-cast v7, LX/Cve;

    invoke-static/range {v42 .. v42}, LX/9yN;->a(LX/0QB;)LX/9yN;

    move-result-object v8

    check-cast v8, LX/9yN;

    const/16 v9, 0x19c6

    move-object/from16 v0, v42

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const-class v10, LX/FeZ;

    move-object/from16 v0, v42

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/FeZ;

    invoke-static/range {v42 .. v42}, LX/FgF;->a(LX/0QB;)LX/FgF;

    move-result-object v11

    check-cast v11, LX/FgF;

    invoke-static/range {v42 .. v42}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v12

    check-cast v12, LX/CvY;

    const/16 v13, 0x12c4

    move-object/from16 v0, v42

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x113f

    move-object/from16 v0, v42

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static/range {v42 .. v42}, LX/1DS;->a(LX/0QB;)LX/1DS;

    move-result-object v15

    check-cast v15, LX/1DS;

    const/16 v16, 0x3374

    move-object/from16 v0, v42

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const-class v17, LX/Cxp;

    move-object/from16 v0, v42

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/Cxp;

    const/16 v18, 0x6c9

    move-object/from16 v0, v42

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v18

    const-class v19, LX/FgQ;

    move-object/from16 v0, v42

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v19

    check-cast v19, LX/FgQ;

    invoke-static/range {v42 .. v42}, LX/FgM;->a(LX/0QB;)LX/FgM;

    move-result-object v20

    check-cast v20, LX/FgM;

    const-class v21, LX/CvR;

    move-object/from16 v0, v42

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v21

    check-cast v21, LX/CvR;

    invoke-static/range {v42 .. v42}, LX/CvP;->a(LX/0QB;)LX/CvP;

    move-result-object v22

    check-cast v22, LX/CvP;

    invoke-static/range {v42 .. v42}, LX/1DL;->a(LX/0QB;)LX/1DL;

    move-result-object v23

    check-cast v23, LX/1DL;

    invoke-static/range {v42 .. v42}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v24

    check-cast v24, LX/0iA;

    invoke-static/range {v42 .. v42}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v25

    check-cast v25, LX/0ad;

    const/16 v26, 0x1a75

    move-object/from16 v0, v42

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v26

    invoke-static/range {v42 .. v42}, LX/1k3;->a(LX/0QB;)LX/1k3;

    move-result-object v27

    check-cast v27, LX/1k3;

    const/16 v28, 0x1147

    move-object/from16 v0, v42

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v28

    invoke-static/range {v42 .. v42}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v29

    check-cast v29, LX/0Sh;

    invoke-static/range {v42 .. v42}, LX/1nD;->a(LX/0QB;)LX/1nD;

    move-result-object v30

    check-cast v30, LX/1nD;

    const/16 v31, 0x455

    move-object/from16 v0, v42

    move/from16 v1, v31

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v31

    const/16 v32, 0x11d1

    move-object/from16 v0, v42

    move/from16 v1, v32

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v32

    invoke-static/range {v42 .. v42}, LX/CzG;->a(LX/0QB;)LX/CzG;

    move-result-object v33

    check-cast v33, LX/CzG;

    const-class v34, LX/Ffw;

    move-object/from16 v0, v42

    move-object/from16 v1, v34

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v34

    check-cast v34, LX/Ffw;

    const-class v35, LX/193;

    move-object/from16 v0, v42

    move-object/from16 v1, v35

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v35

    check-cast v35, LX/193;

    const/16 v36, 0x3331

    move-object/from16 v0, v42

    move/from16 v1, v36

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v36

    invoke-static/range {v42 .. v42}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v37

    check-cast v37, LX/0Uh;

    const-class v38, LX/BwE;

    move-object/from16 v0, v42

    move-object/from16 v1, v38

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v38

    check-cast v38, LX/BwE;

    const/16 v39, 0x3369

    move-object/from16 v0, v42

    move/from16 v1, v39

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v39

    invoke-static/range {v42 .. v42}, LX/8ht;->a(LX/0QB;)LX/8ht;

    move-result-object v40

    check-cast v40, LX/8ht;

    invoke-static/range {v42 .. v42}, LX/CvM;->a(LX/0QB;)LX/CvM;

    move-result-object v41

    check-cast v41, LX/CvM;

    const/16 v43, 0x335f

    invoke-static/range {v42 .. v43}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v42

    invoke-static/range {v2 .. v42}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->a(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;LX/CzF;LX/FeT;LX/1K9;LX/FgH;LX/Cve;LX/9yN;LX/0Ot;LX/FeZ;LX/FgF;LX/CvY;LX/0Ot;LX/0Ot;LX/1DS;LX/0Ot;LX/Cxp;LX/0Or;LX/FgQ;LX/FgM;LX/CvR;LX/CvP;LX/1DL;LX/0iA;LX/0ad;LX/0Ot;LX/1k3;LX/0Ot;LX/0Sh;LX/1nD;LX/0Ot;LX/0Ot;LX/CzG;LX/Ffw;LX/193;LX/0Ot;LX/0Uh;LX/BwE;LX/0Ot;LX/8ht;LX/CvM;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;LX/Fg8;)V
    .locals 5

    .prologue
    .line 2266466
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v0

    .line 2266467
    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2266468
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/EQG;->LOADING:LX/EQG;

    .line 2266469
    :goto_0
    invoke-static {p0, v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->a(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;LX/EQG;)V

    .line 2266470
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->an:Z

    .line 2266471
    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->p:LX/FgF;

    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->U:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    invoke-virtual {v0}, LX/FcD;->g()LX/0Px;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    iget-object v4, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->al:LX/0Px;

    invoke-virtual {v0, v4}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v1, v3, p1, v0}, LX/FgF;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;LX/Fg8;LX/0Px;)V

    .line 2266472
    return-void

    .line 2266473
    :cond_0
    sget-object v0, LX/EQG;->LOADING_MORE:LX/EQG;

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;
    .locals 1

    .prologue
    .line 2266474
    new-instance v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    invoke-direct {v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;-><init>()V

    .line 2266475
    iput-object p0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aD:Ljava/lang/String;

    .line 2266476
    return-object v0
.end method

.method public static synthetic q(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)Lcom/facebook/search/results/model/SearchResultsMutableContext;
    .locals 1

    .prologue
    .line 2266477
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v0, v0

    .line 2266478
    return-object v0
.end method

.method public static synthetic r(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)Lcom/facebook/search/results/model/SearchResultsMutableContext;
    .locals 1

    .prologue
    .line 2266479
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v0, v0

    .line 2266480
    return-object v0
.end method

.method private t()LX/0fu;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2266481
    new-instance v0, LX/Fed;

    invoke-direct {v0, p0}, LX/Fed;-><init>(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V

    return-object v0
.end method

.method private v()V
    .locals 5

    .prologue
    .line 2266482
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v0, v0

    .line 2266483
    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->h()LX/0P1;

    move-result-object v0

    sget-object v1, LX/7B4;->GROUP_COMMERCE:LX/7B4;

    invoke-virtual {v1}, LX/7B4;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2266484
    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aw:Z

    if-eqz v0, :cond_1

    .line 2266485
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->av:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2266486
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->av:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FgP;

    .line 2266487
    iget-object v1, v0, LX/FgP;->a:LX/1ZF;

    sget-object p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-interface {v1, p0}, LX/1ZF;->b(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2266488
    iget-object v1, v0, LX/FgP;->a:LX/1ZF;

    const/4 p0, 0x0

    invoke-interface {v1, p0}, LX/1ZF;->a(LX/63W;)V

    .line 2266489
    :cond_0
    :goto_0
    return-void

    .line 2266490
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->av:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2266491
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->av:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FgP;

    .line 2266492
    iget-object v1, v0, LX/FgP;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    if-nez v1, :cond_3

    .line 2266493
    iget-object v1, v0, LX/FgP;->d:LX/Fea;

    .line 2266494
    iget-object v2, v1, LX/Fea;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    invoke-static {v2}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->E(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)LX/FgM;

    move-result-object v2

    .line 2266495
    if-eqz v2, :cond_5

    .line 2266496
    iget-object v3, v1, LX/Fea;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    .line 2266497
    iget-object v1, v3, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2266498
    move-object v3, v1

    .line 2266499
    invoke-virtual {v3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->h()LX/0P1;

    move-result-object v3

    .line 2266500
    sget-object v4, LX/7B4;->GROUP_COMMERCE:LX/7B4;

    invoke-virtual {v4}, LX/7B4;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;

    .line 2266501
    if-eqz v4, :cond_2

    .line 2266502
    iget-boolean p0, v4, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->b:Z

    move v4, p0

    .line 2266503
    if-nez v4, :cond_6

    .line 2266504
    :cond_2
    sget-object v4, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2266505
    :goto_1
    move-object v2, v4

    .line 2266506
    :goto_2
    move-object v1, v2

    .line 2266507
    iput-object v1, v0, LX/FgP;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2266508
    :cond_3
    iget-object v1, v0, LX/FgP;->a:LX/1ZF;

    iget-object v2, v0, LX/FgP;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-interface {v1, v2}, LX/1ZF;->b(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2266509
    iget-object v1, v0, LX/FgP;->a:LX/1ZF;

    iget-object v2, v0, LX/FgP;->b:LX/63W;

    invoke-interface {v1, v2}, LX/1ZF;->a(LX/63W;)V

    .line 2266510
    :cond_4
    goto :goto_0

    :cond_5
    sget-object v2, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    goto :goto_2

    :cond_6
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v4

    iget-object p0, v2, LX/FgM;->a:Landroid/content/res/Resources;

    const v1, 0x7f082323

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 2266511
    iput-object p0, v4, LX/108;->g:Ljava/lang/String;

    .line 2266512
    move-object v4, v4

    .line 2266513
    invoke-virtual {v4}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v4

    goto :goto_1
.end method

.method private w()LX/1PT;
    .locals 3

    .prologue
    .line 2266514
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v0, v0

    .line 2266515
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->K:LX/8ht;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v2

    .line 2266516
    iget-object p0, v0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    move-object v0, p0

    .line 2266517
    invoke-virtual {v1, v2, v0}, LX/8ht;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;LX/8ci;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2266518
    sget-object v0, LX/EIx;->a:LX/EIx;

    move-object v0, v0

    .line 2266519
    :goto_0
    return-object v0

    .line 2266520
    :cond_0
    sget-object v0, LX/EIy;->a:LX/EIy;

    move-object v0, v0

    .line 2266521
    goto :goto_0
.end method

.method public static x(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V
    .locals 4

    .prologue
    .line 2266522
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v0, v0

    .line 2266523
    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v1

    .line 2266524
    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->jA_()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->jA_()Ljava/lang/String;

    move-result-object v2

    const-string v3, "news_v2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, LX/7BG;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2266525
    :goto_0
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->az:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v1, v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b(Ljava/lang/String;)V

    .line 2266526
    return-void

    .line 2266527
    :cond_0
    invoke-static {v1}, LX/7BG;->o(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static y(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V
    .locals 1

    .prologue
    .line 2266528
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ag:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2266529
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2266530
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aD:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2266531
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->U:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    invoke-virtual {v0, p1}, LX/FcD;->b(LX/0Px;)V

    .line 2266532
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->d()V

    .line 2266533
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ag:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2266534
    sget-object v0, LX/EQG;->LOADING_MORE:LX/EQG;

    invoke-static {p0, v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->a(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;LX/EQG;)V

    .line 2266535
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->p:LX/FgF;

    .line 2266536
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v2, v0

    .line 2266537
    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->U:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    invoke-virtual {v0}, LX/FcD;->g()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/FgF;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ZLX/0Px;LX/0Px;)V

    .line 2266538
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->q:LX/CvY;

    .line 2266539
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v2, v0

    .line 2266540
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->U:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    invoke-virtual {v0}, LX/FcD;->g()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0Px;)V

    .line 2266541
    return-void
.end method

.method public final a(LX/4FP;)V
    .locals 5

    .prologue
    .line 2266390
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->U:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    invoke-virtual {v0, p1}, LX/FcD;->a(LX/4FP;)LX/0Px;

    move-result-object v0

    .line 2266391
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v1}, LX/CzE;->d()V

    .line 2266392
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ag:LX/1Qq;

    invoke-interface {v1}, LX/1Cw;->notifyDataSetChanged()V

    .line 2266393
    sget-object v1, LX/EQG;->LOADING_MORE:LX/EQG;

    invoke-static {p0, v1}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->a(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;LX/EQG;)V

    .line 2266394
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->p:LX/FgF;

    .line 2266395
    iget-object v2, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v2, v2

    .line 2266396
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/FgF;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ZLX/0Px;LX/0Px;)V

    .line 2266397
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->q:LX/CvY;

    .line 2266398
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v2, v0

    .line 2266399
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->U:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    invoke-virtual {v0}, LX/FcD;->g()LX/0Px;

    move-result-object v0

    .line 2266400
    sget-object v3, LX/CvJ;->RESULTS_FILTER:LX/CvJ;

    invoke-static {v3, v2}, LX/CvY;->a(LX/CvJ;Lcom/facebook/search/results/model/SearchResultsMutableContext;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "filter_action_type"

    const-string p0, "filter_cleared"

    invoke-virtual {v3, v4, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2266401
    const-string v4, "cleared_filter"

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p0

    invoke-static {p0}, LX/CvY;->a(LX/0Px;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, v4, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2266402
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2266403
    const-string v4, "applied_filters"

    invoke-static {v0}, LX/CvY;->a(LX/0Px;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, v4, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2266404
    :cond_0
    invoke-static {v1, v2, v3}, LX/CvY;->a(LX/CvY;Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2266405
    return-void
.end method

.method public final a(LX/5Mj;)V
    .locals 2

    .prologue
    .line 2266406
    iget-object v0, p1, LX/5Mj;->f:LX/5Mk;

    move-object v0, v0

    .line 2266407
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ag:LX/1Qq;

    invoke-virtual {v0, v1, p1}, LX/5Mk;->a(Ljava/lang/Object;LX/5Mj;)V

    .line 2266408
    return-void
.end method

.method public final a(LX/5uu;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5uu;",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2266102
    return-void
.end method

.method public final a(LX/CwB;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;)V
    .locals 2

    .prologue
    .line 2266104
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->a(LX/CwB;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;)V

    .line 2266105
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    if-eqz v0, :cond_0

    .line 2266106
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    .line 2266107
    iget-object v1, v0, LX/Fje;->u:Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-object v0, v1

    .line 2266108
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->F()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbSwipeRefreshLayout;->setEnabled(Z)V

    .line 2266109
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    const/4 v1, 0x1

    .line 2266110
    iput-boolean v1, v0, LX/Fje;->W:Z

    .line 2266111
    :cond_0
    return-void
.end method

.method public final a(LX/Fjd;)V
    .locals 3

    .prologue
    .line 2266112
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->U:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    .line 2266113
    iget-object v2, v0, LX/FcD;->i:LX/0Px;

    move-object v0, v2

    .line 2266114
    invoke-static {v0, p0}, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->a(LX/0Px;LX/FdZ;)Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    move-result-object v0

    const-string v2, "FILTER_FRAGMENT_TAG"

    invoke-virtual {v1, v0, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2266115
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->q:LX/CvY;

    .line 2266116
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2266117
    invoke-virtual {v0, v1}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    .line 2266118
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    .line 2266119
    invoke-super {p0, p1}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->a(Landroid/os/Bundle;)V

    .line 2266120
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2266121
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v0, v0

    .line 2266122
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->I:LX/0Uh;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->B:LX/0ad;

    .line 2266123
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2266124
    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a(LX/0Uh;LX/0ad;Landroid/os/Bundle;)V

    .line 2266125
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->an:Z

    .line 2266126
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->i:LX/CzF;

    invoke-virtual {v1, v0}, LX/CzF;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;)LX/CzE;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    .line 2266127
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->j:LX/FeT;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    .line 2266128
    new-instance p1, LX/FeS;

    invoke-static {v1}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v3

    check-cast v3, LX/0bH;

    const-class v4, LX/Feq;

    invoke-interface {v1, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/Feq;

    const-class v5, LX/Fes;

    invoke-interface {v1, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/Fes;

    invoke-direct {p1, v2, v3, v4, v5}, LX/FeS;-><init>(LX/CzE;LX/0bH;LX/Feq;LX/Fes;)V

    .line 2266129
    move-object v1, p1

    .line 2266130
    iput-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ac:LX/FeS;

    .line 2266131
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->z:LX/1DL;

    invoke-virtual {v1}, LX/1DL;->a()LX/1Jg;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ah:LX/1Jg;

    .line 2266132
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->x:LX/CvR;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    .line 2266133
    new-instance v4, LX/CvQ;

    invoke-static {v1}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v1}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v6

    check-cast v6, LX/CvY;

    invoke-static {v1}, LX/1BX;->a(LX/0QB;)LX/1BX;

    move-result-object v7

    check-cast v7, LX/1BX;

    invoke-static {v1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    move-object v9, v0

    move-object v10, v2

    invoke-direct/range {v4 .. v10}, LX/CvQ;-><init>(LX/0SG;LX/CvY;LX/1BX;LX/0ad;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzE;)V

    .line 2266134
    move-object v1, v4

    .line 2266135
    iput-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->au:LX/CvQ;

    .line 2266136
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->C:LX/1k3;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->y:LX/CvP;

    invoke-virtual {v1, v2}, LX/1Kt;->a(LX/1Ce;)V

    .line 2266137
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->C:LX/1k3;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->au:LX/CvQ;

    invoke-virtual {v1, v2}, LX/1Kt;->a(LX/1Ce;)V

    .line 2266138
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->C:LX/1k3;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->X:LX/CvM;

    invoke-virtual {v1, v2}, LX/1Kt;->a(LX/1Ce;)V

    .line 2266139
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->X:LX/CvM;

    .line 2266140
    iget-object v2, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v2, v2

    .line 2266141
    iput-object v2, v1, LX/CvM;->d:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2266142
    new-instance v1, Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-direct {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;-><init>()V

    iput-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->az:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2266143
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->az:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2266144
    iget-object v2, v0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v2, v2

    .line 2266145
    iget-object v3, v0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    move-object v3, v3

    .line 2266146
    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a(LX/CwB;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;)V

    .line 2266147
    invoke-static {p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->x(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V

    .line 2266148
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->H:LX/193;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "search_results_feed_scroll_perf"

    invoke-virtual {v1, v2, v3}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ax:LX/195;

    .line 2266149
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 2266150
    if-eqz v1, :cond_0

    .line 2266151
    invoke-static {v1, v0}, LX/FcD;->a(Landroid/os/Bundle;LX/CwB;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->al:LX/0Px;

    .line 2266152
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 2266153
    instance-of v1, v0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    if-eqz v1, :cond_1

    .line 2266154
    check-cast v0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aE:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    .line 2266155
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    .line 2266156
    invoke-static {p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->E(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)LX/FgM;

    move-result-object v0

    .line 2266157
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2266158
    if-eqz v0, :cond_1

    .line 2266159
    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->h()LX/0P1;

    move-result-object v1

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2266160
    sget-object v2, LX/7B4;->GROUP_COMMERCE:LX/7B4;

    invoke-virtual {v2}, LX/7B4;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;

    .line 2266161
    if-nez v2, :cond_2

    .line 2266162
    :cond_0
    :goto_0
    move v0, v3

    .line 2266163
    if-eqz v0, :cond_1

    .line 2266164
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->d()V

    .line 2266165
    invoke-static {p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->y(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V

    .line 2266166
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->r()V

    .line 2266167
    :cond_1
    const/4 v0, 0x1

    return v0

    .line 2266168
    :cond_2
    sget-object v5, LX/FgK;->a:[I

    invoke-static {}, LX/FgL;->values()[LX/FgL;

    move-result-object v6

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v7

    aget-object v6, v6, v7

    invoke-virtual {v6}, LX/FgL;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    goto :goto_0

    .line 2266169
    :pswitch_0
    iget-boolean v4, v2, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->c:Z

    move v4, v4

    .line 2266170
    iput-boolean v3, v2, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->c:Z

    .line 2266171
    move v3, v4

    .line 2266172
    goto :goto_0

    .line 2266173
    :pswitch_1
    iget-boolean v5, v2, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->c:Z

    move v5, v5

    .line 2266174
    if-nez v5, :cond_3

    move v3, v4

    .line 2266175
    :cond_3
    iput-boolean v4, v2, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->c:Z

    .line 2266176
    if-eqz v3, :cond_0

    .line 2266177
    const-string v4, "available"

    .line 2266178
    iput-object v4, v2, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->a:Ljava/lang/String;

    .line 2266179
    goto :goto_0

    .line 2266180
    :pswitch_2
    const-string v3, "available"

    invoke-static {v2, v3}, LX/FgM;->a(Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;Ljava/lang/String;)Z

    move-result v3

    goto :goto_0

    .line 2266181
    :pswitch_3
    const-string v3, "sold"

    invoke-static {v2, v3}, LX/FgM;->a(Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;Ljava/lang/String;)Z

    move-result v3

    goto :goto_0

    .line 2266182
    :pswitch_4
    const-string v3, "expired"

    invoke-static {v2, v3}, LX/FgM;->a(Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;Ljava/lang/String;)Z

    move-result v3

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final b(Z)Z
    .locals 2

    .prologue
    .line 2266183
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->R:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x9;

    sget-object v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->L:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-virtual {v0, v1}, LX/0x9;->c(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;)V

    .line 2266184
    invoke-super {p0, p1}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 12

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 2266185
    iput-boolean v9, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ap:Z

    .line 2266186
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->a()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aq:Z

    if-nez v0, :cond_3

    move v8, v9

    .line 2266187
    :goto_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->a()I

    move-result v0

    if-nez v0, :cond_2

    if-nez v8, :cond_2

    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->p:LX/FgF;

    invoke-virtual {v0}, LX/FgF;->c()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->I:LX/0Uh;

    sget v1, LX/2SU;->r:I

    invoke-virtual {v0, v1, v10}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2266188
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    const/4 v2, 0x1

    .line 2266189
    sget-object v1, LX/CzE;->b:LX/0us;

    iput-object v1, v0, LX/CzE;->n:LX/0us;

    .line 2266190
    invoke-virtual {v0}, LX/CzE;->a()I

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, v0, LX/CzE;->m:Z

    if-nez v1, :cond_1

    .line 2266191
    new-instance v3, Lcom/facebook/search/results/model/unit/SearchResultsResultsNoUnit;

    iget-object v1, v0, LX/CzE;->e:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v1, v0, LX/CzE;->e:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v5

    iget-object v1, v0, LX/CzE;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    move v1, v2

    :goto_1
    invoke-direct {v3, v4, v5, v1}, Lcom/facebook/search/results/model/unit/SearchResultsResultsNoUnit;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2266192
    iget-object v1, v0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2266193
    if-eqz v10, :cond_0

    .line 2266194
    iget-object v1, v0, LX/CzE;->f:Ljava/util/List;

    new-instance v3, Lcom/facebook/search/results/model/unit/SearchResultsRelatedCategoriesUnit;

    invoke-direct {v3}, Lcom/facebook/search/results/model/unit/SearchResultsRelatedCategoriesUnit;-><init>()V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2266195
    :cond_0
    iget v1, v0, LX/CzE;->l:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/CzE;->l:I

    .line 2266196
    iput-boolean v2, v0, LX/CzE;->m:Z

    .line 2266197
    :cond_1
    invoke-static {p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->y(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V

    .line 2266198
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->l:LX/FgH;

    .line 2266199
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2266200
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2266201
    iget-object v3, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    iget-object v4, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->U:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/FcD;

    invoke-virtual {v4}, LX/FcD;->g()LX/0Px;

    move-result-object v4

    iget v5, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ar:I

    iget v6, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->as:I

    .line 2266202
    iget-object v7, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v7, v7

    .line 2266203
    iget-object v11, v7, Lcom/facebook/search/results/model/SearchResultsMutableContext;->v:Ljava/lang/String;

    move-object v7, v11

    .line 2266204
    invoke-virtual/range {v0 .. v7}, LX/FgH;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0Px;LX/CzE;LX/0Px;IILjava/lang/String;)V

    .line 2266205
    iget v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ar:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ar:I

    .line 2266206
    :cond_2
    if-eqz v8, :cond_4

    .line 2266207
    iput-boolean v9, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aq:Z

    .line 2266208
    sget-object v0, LX/Fg8;->ON_SCROLL:LX/Fg8;

    invoke-static {p0, v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->a$redex0(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;LX/Fg8;)V

    .line 2266209
    :goto_2
    return-void

    :cond_3
    move v8, v10

    .line 2266210
    goto/16 :goto_0

    .line 2266211
    :cond_4
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->a()I

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, LX/EQG;->LOADING_FINISHED_NO_RESULTS:LX/EQG;

    :goto_3
    invoke-static {p0, v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->a(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;LX/EQG;)V

    .line 2266212
    iput-boolean v10, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aq:Z

    goto :goto_2

    .line 2266213
    :cond_5
    sget-object v0, LX/EQG;->LOADING_FINISHED:LX/EQG;

    goto :goto_3

    .line 2266214
    :cond_6
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2266215
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2266216
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ag:LX/1Qq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ag:LX/1Qq;

    invoke-interface {v0}, LX/0Vf;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2266217
    new-instance v0, LX/5Mj;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->Q:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/5Mk;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/5Mj;-><init>(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;LX/5Mk;)V

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->a(LX/5Mj;)V

    .line 2266218
    :cond_0
    return-void
.end method

.method public final e()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2266103
    const-class v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    return-object v0
.end method

.method public final k()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2266219
    iput v4, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->as:I

    .line 2266220
    iput v4, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ar:I

    .line 2266221
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    .line 2266222
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2266223
    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Fje;->setTextViewQueryString(Ljava/lang/String;)V

    .line 2266224
    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ao:Z

    if-nez v0, :cond_2

    .line 2266225
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2266226
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->d()V

    .line 2266227
    invoke-static {p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->y(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V

    .line 2266228
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ai:LX/CzK;

    if-eqz v0, :cond_1

    .line 2266229
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v0, v0

    .line 2266230
    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7BG;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2266231
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ai:LX/CzK;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aj:Ljava/lang/String;

    .line 2266232
    iput-object v1, v0, LX/Fje;->G:LX/CzK;

    .line 2266233
    iput-object v2, v0, LX/Fje;->H:Ljava/lang/String;

    .line 2266234
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/Fje;->J:Z

    .line 2266235
    iget-object v3, v0, LX/Fje;->B:LX/EQG;

    sget-object v5, LX/EQG;->LOADING:LX/EQG;

    if-ne v3, v5, :cond_1

    .line 2266236
    invoke-static {v0}, LX/Fje;->h(LX/Fje;)V

    .line 2266237
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, LX/EQG;->LOADING:LX/EQG;

    :goto_0
    invoke-static {p0, v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->a(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;LX/EQG;)V

    .line 2266238
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->an:Z

    .line 2266239
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->p:LX/FgF;

    .line 2266240
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2266241
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 2266242
    const-string v3, "preloaded_story_ids"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 2266243
    const-string v5, "preloaded_story_ids"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 2266244
    if-nez v3, :cond_4

    const/4 v2, 0x0

    :goto_1
    move-object v2, v2

    .line 2266245
    iget-object v3, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->al:LX/0Px;

    invoke-virtual {v0, v1, v4, v2, v3}, LX/FgF;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ZLX/0Px;LX/0Px;)V

    .line 2266246
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->v()V

    .line 2266247
    return-void

    .line 2266248
    :cond_3
    sget-object v0, LX/EQG;->LOADING_MORE:LX/EQG;

    goto :goto_0

    :cond_4
    invoke-static {v3}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    goto :goto_1
.end method

.method public final kS_()V
    .locals 1

    .prologue
    .line 2266249
    iget v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->as:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->as:I

    .line 2266250
    sget-object v0, LX/Fg8;->ON_TIMEOUT_RETRY:LX/Fg8;

    invoke-static {p0, v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->a$redex0(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;LX/Fg8;)V

    .line 2266251
    return-void
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2266252
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()V
    .locals 6

    .prologue
    .line 2266253
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->n()V

    .line 2266254
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aA:Z

    .line 2266255
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->at:LX/6Vi;

    if-nez v0, :cond_0

    .line 2266256
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->k:LX/1K9;

    const-class v1, LX/EJ1;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aa:LX/6Ve;

    invoke-virtual {v0, v1, v2}, LX/1K9;->a(Ljava/lang/Class;LX/6Ve;)LX/6Vi;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->at:LX/6Vi;

    .line 2266257
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ac:LX/FeS;

    .line 2266258
    iget-object v1, v0, LX/FeS;->e:LX/1B1;

    iget-object v2, v0, LX/FeS;->b:LX/0bH;

    invoke-virtual {v1, v2}, LX/1B1;->a(LX/0b4;)V

    .line 2266259
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->n:LX/9yN;

    .line 2266260
    invoke-virtual {v0}, LX/9yN;->b()V

    .line 2266261
    iget-object v1, v0, LX/9yN;->b:Landroid/os/Handler;

    iget-object v2, v0, LX/9yN;->f:Ljava/lang/Runnable;

    const v3, -0x10b416de

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2266262
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->p:LX/FgF;

    .line 2266263
    iget-object v1, v0, LX/FgF;->u:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    if-eqz v1, :cond_1

    .line 2266264
    invoke-virtual {v0}, LX/FgF;->b()V

    .line 2266265
    :cond_1
    iput-object p0, v0, LX/FgF;->u:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    .line 2266266
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->p:LX/FgF;

    invoke-virtual {v0}, LX/FgF;->e()V

    .line 2266267
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->am:LX/FeY;

    .line 2266268
    invoke-virtual {v0}, LX/FeY;->b()V

    .line 2266269
    iget-object v1, v0, LX/FeY;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2266270
    iget-object v1, v0, LX/FeY;->g:Ljava/util/List;

    iget-object v2, v0, LX/FeY;->a:LX/1K9;

    const-class v3, LX/EJ2;

    new-instance v4, LX/FeW;

    invoke-direct {v4, v0}, LX/FeW;-><init>(LX/FeY;)V

    invoke-virtual {v2, v3, v4}, LX/1K9;->a(Ljava/lang/Class;LX/6Ve;)LX/6Vi;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2266271
    iget-object v1, v0, LX/FeY;->g:Ljava/util/List;

    iget-object v2, v0, LX/FeY;->a:LX/1K9;

    const-class v3, LX/EJ3;

    new-instance v4, LX/FeX;

    invoke-direct {v4, v0}, LX/FeX;-><init>(LX/FeY;)V

    invoke-virtual {v2, v3, v4}, LX/1K9;->a(Ljava/lang/Class;LX/6Ve;)LX/6Vi;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2266272
    new-instance v1, LX/FeV;

    invoke-direct {v1, v0}, LX/FeV;-><init>(LX/FeY;)V

    iput-object v1, v0, LX/FeY;->m:LX/FeV;

    .line 2266273
    iget-object v1, v0, LX/FeY;->b:LX/0bH;

    iget-object v2, v0, LX/FeY;->m:LX/FeV;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2266274
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->size()I

    move-result v0

    if-lez v0, :cond_3

    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ap:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ae:LX/EQG;

    sget-object v1, LX/EQG;->LOADING_MORE:LX/EQG;

    if-ne v0, v1, :cond_3

    .line 2266275
    sget-object v0, LX/Fg8;->ON_RESUME_RETRY:LX/Fg8;

    invoke-static {p0, v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->a$redex0(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;LX/Fg8;)V

    .line 2266276
    :cond_2
    :goto_0
    return-void

    .line 2266277
    :cond_3
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->h()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2266278
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->m:LX/Cve;

    invoke-virtual {v0}, LX/Cve;->b()V

    goto :goto_0
.end method

.method public final o()V
    .locals 3

    .prologue
    .line 2266279
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->at:LX/6Vi;

    if-eqz v0, :cond_0

    .line 2266280
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->k:LX/1K9;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->at:LX/6Vi;

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/6Vi;)V

    .line 2266281
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->at:LX/6Vi;

    .line 2266282
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ac:LX/FeS;

    .line 2266283
    iget-object v1, v0, LX/FeS;->e:LX/1B1;

    iget-object v2, v0, LX/FeS;->b:LX/0bH;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2266284
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->n:LX/9yN;

    invoke-virtual {v0}, LX/9yN;->b()V

    .line 2266285
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->am:LX/FeY;

    invoke-virtual {v0}, LX/FeY;->b()V

    .line 2266286
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->p:LX/FgF;

    .line 2266287
    const/4 v1, 0x0

    iput-object v1, v0, LX/FgF;->u:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    .line 2266288
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->p:LX/FgF;

    invoke-virtual {v0}, LX/FgF;->b()V

    .line 2266289
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ay:LX/Ffv;

    if-eqz v0, :cond_1

    .line 2266290
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ay:LX/Ffv;

    invoke-virtual {v0}, LX/Ffv;->b()V

    .line 2266291
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->m:LX/Cve;

    .line 2266292
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2266293
    invoke-virtual {v0, v1}, LX/Cve;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    .line 2266294
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->C:LX/1k3;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->af:LX/0g8;

    invoke-virtual {v0, v1}, LX/1Kt;->c(LX/0g8;)V

    .line 2266295
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->o()V

    .line 2266296
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2266297
    const/16 v0, 0x6dc

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2266298
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->N:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2266299
    :cond_0
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2266300
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ag:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qr;->a(Landroid/content/res/Configuration;)V

    .line 2266301
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 23

    .prologue
    const/4 v1, 0x2

    const/16 v2, 0x2a

    const v3, 0x65072150

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v22

    .line 2266302
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->s()Landroid/content/Context;

    move-result-object v3

    .line 2266303
    new-instance v1, LX/Fje;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->q()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->z()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v2

    invoke-static {v2}, LX/Cw8;->from(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/Cw8;

    move-result-object v2

    sget-object v4, LX/EQE;->SPINNING_WHEEL:LX/EQE;

    invoke-direct {v1, v3, v2, v4}, LX/Fje;-><init>(Landroid/content/Context;LX/Cw8;LX/EQE;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    .line 2266304
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->q()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/7BG;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2266305
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    invoke-virtual {v1}, LX/Fje;->getSwipeLayout()Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/widget/FbSwipeRefreshLayout;->setEnabled(Z)V

    .line 2266306
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    invoke-virtual {v1}, LX/Fje;->getSwipeLayout()Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v1

    new-instance v2, LX/Feb;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/Feb;-><init>(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2266307
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->B:LX/0ad;

    sget-short v4, LX/100;->bp:S

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2, v1}, LX/Fje;->setResultPageFadeTransitionDuration(I)V

    .line 2266308
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->U:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FcD;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    invoke-virtual {v1, v2}, LX/FcD;->a(LX/Fje;)V

    .line 2266309
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->U:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FcD;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, LX/FcD;->a(LX/FcC;)V

    .line 2266310
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, LX/Fje;->setRetryClickedListener(LX/1DI;)V

    .line 2266311
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, LX/Fje;->setSearchPivotClickListener(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V

    .line 2266312
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, LX/Fje;->setFilterButtonClickListener(LX/Fe2;)V

    .line 2266313
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ae:LX/EQG;

    if-eqz v1, :cond_0

    .line 2266314
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/Fje;->setIsInitialLoad(Z)V

    .line 2266315
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ae:LX/EQG;

    invoke-virtual {v1, v2}, LX/Fje;->setState(LX/EQG;)V

    .line 2266316
    :cond_0
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 2266317
    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v4, 0x7f0106cd

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v1, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 2266318
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    iget v1, v1, Landroid/util/TypedValue;->data:I

    invoke-direct {v4, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v4}, LX/Fje;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2266319
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    invoke-virtual {v1}, LX/Fje;->getScrollingViewProxy()LX/0g8;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->af:LX/0g8;

    .line 2266320
    new-instance v5, LX/CxF;

    invoke-direct {v5}, LX/CxF;-><init>()V

    .line 2266321
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "display_style"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v1

    .line 2266322
    :goto_2
    if-eqz v1, :cond_1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v1, v2, :cond_2

    .line 2266323
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->q()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2266324
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->t:LX/Cxp;

    const-string v2, "graph_search_results_page"

    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->w()LX/1PT;

    move-result-object v4

    new-instance v6, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment$4;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment$4;-><init>(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ah:LX/1Jg;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->af:LX/0g8;

    invoke-static {v8}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->q()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->q()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aC:LX/CyE;

    move-object/from16 v18, v0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->q()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    move-object/from16 v21, v0

    invoke-virtual/range {v1 .. v21}, LX/Cxp;->a(Ljava/lang/String;Landroid/content/Context;LX/1PT;LX/CxF;Ljava/lang/Runnable;LX/1Jg;LX/1PY;LX/CzE;LX/CzA;LX/CzE;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzE;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzE;LX/CzE;LX/Cz2;LX/CyE;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzE;LX/CzE;)LX/Cxo;

    move-result-object v1

    .line 2266325
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->J:LX/BwE;

    sget-object v3, LX/0wD;->SEARCH_RESULTS:LX/0wD;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, LX/BwE;->a(LX/1Pf;LX/0wD;Ljava/lang/String;)LX/Bur;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/CxF;->a(LX/1SX;)V

    .line 2266326
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->o:LX/FeZ;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->q()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v2, v3, v4, v1}, LX/FeZ;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzE;LX/Cxo;)LX/FeY;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->am:LX/FeY;

    .line 2266327
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->r:LX/1DS;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->s:LX/0Ot;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v2, v3, v4}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1Ql;->a(LX/1PW;)LX/1Ql;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ql;->e()LX/1Qq;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ag:LX/1Qq;

    .line 2266328
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->q()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/7BG;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2266329
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f031293

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/CustomFrameLayout;

    .line 2266330
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->U:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FcD;

    const v3, 0x7f0d2b7a

    invoke-virtual {v1, v3}, Lcom/facebook/widget/CustomFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v2, v3}, LX/FcD;->a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    .line 2266331
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    invoke-virtual {v2, v1}, LX/Fje;->a(Landroid/view/View;)V

    .line 2266332
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->af:LX/0g8;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ag:LX/1Qq;

    invoke-interface {v1, v2}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2266333
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->af:LX/0g8;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->u:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Db;

    invoke-virtual {v1}, LX/1Db;->a()LX/1St;

    move-result-object v1

    invoke-interface {v2, v1}, LX/0g8;->a(LX/1St;)V

    .line 2266334
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ah:LX/1Jg;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ag:LX/1Qq;

    invoke-interface {v2}, LX/1Qr;->e()LX/1R4;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Jg;->a(LX/1R4;)V

    .line 2266335
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->X:LX/CvM;

    invoke-virtual {v1}, LX/CvM;->a()V

    .line 2266336
    invoke-static {}, LX/1fG;->b()V

    .line 2266337
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->af:LX/0g8;

    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->t()LX/0fu;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0g8;->b(LX/0fu;)V

    .line 2266338
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->af:LX/0g8;

    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->C()LX/0fx;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0g8;->a(LX/0fx;)V

    .line 2266339
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->C:LX/1k3;

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->af:LX/0g8;

    invoke-virtual {v1, v2, v3}, LX/1Kt;->a(ZLX/0g8;)V

    .line 2266340
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ak:Ljava/lang/String;

    .line 2266341
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->W:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 2266342
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->U:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FcD;

    invoke-virtual {v1}, LX/FcD;->c()V

    .line 2266343
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aE:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    if-eqz v1, :cond_4

    .line 2266344
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->U:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FcD;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aE:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    sget-object v3, LX/CyI;->MARKETPLACE:LX/CyI;

    invoke-virtual {v2, v3}, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->a(LX/CyI;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/FcD;->b(LX/0Px;)V

    .line 2266345
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    const v2, 0x284f600d

    move/from16 v0, v22

    invoke-static {v2, v0}, LX/02F;->f(II)V

    return-object v1

    .line 2266346
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->F()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2266347
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    invoke-virtual {v1}, LX/Fje;->getSwipeLayout()Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/widget/FbSwipeRefreshLayout;->setEnabled(Z)V

    .line 2266348
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    invoke-virtual {v1}, LX/Fje;->getSwipeLayout()Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v1

    new-instance v2, LX/Fec;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/Fec;-><init>(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    goto/16 :goto_0

    .line 2266349
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    invoke-virtual {v1}, LX/Fje;->getSwipeLayout()Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/FbSwipeRefreshLayout;->setEnabled(Z)V

    goto/16 :goto_0

    .line 2266350
    :cond_7
    const/16 v1, 0x1f4

    goto/16 :goto_1

    .line 2266351
    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_2
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7d0d515d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2266352
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aE:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    .line 2266353
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "filters"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 2266354
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onDestroy()V

    .line 2266355
    const/16 v1, 0x2b

    const v2, -0xf4f9448

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, 0x4bc39065    # 2.563297E7f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2266356
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ag:LX/1Qq;

    invoke-interface {v0}, LX/0Vf;->dispose()V

    .line 2266357
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->R:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x9;

    sget-object v2, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->L:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-virtual {v0, v2}, LX/0x9;->c(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;)V

    .line 2266358
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    .line 2266359
    iput-object v3, v0, LX/Fje;->N:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    .line 2266360
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    .line 2266361
    iput-object v3, v0, LX/Fje;->O:LX/Fe2;

    .line 2266362
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->U:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    invoke-virtual {v0}, LX/FcD;->a()V

    .line 2266363
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->U:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    invoke-virtual {v0}, LX/FcD;->b()V

    .line 2266364
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->U:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    .line 2266365
    iput-object v3, v0, LX/FcD;->k:LX/FcC;

    .line 2266366
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onDestroyView()V

    .line 2266367
    const/16 v0, 0x2b

    const v2, -0x160a41bc

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onHiddenChanged(Z)V
    .locals 0

    .prologue
    .line 2266368
    invoke-super {p0, p1}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onHiddenChanged(Z)V

    .line 2266369
    iput-boolean p1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aw:Z

    .line 2266370
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->v()V

    .line 2266371
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2347218e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2266372
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ah:LX/1Jg;

    invoke-interface {v1}, LX/1Jg;->b()V

    .line 2266373
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->af:LX/0g8;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ah:LX/1Jg;

    invoke-interface {v2}, LX/1Jg;->a()LX/0fx;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0g8;->c(LX/0fx;)V

    .line 2266374
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ax:LX/195;

    invoke-virtual {v1}, LX/195;->b()V

    .line 2266375
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onPause()V

    .line 2266376
    const/16 v1, 0x2b

    const v2, 0x626bc204

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x504d2624

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2266377
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onResume()V

    .line 2266378
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ah:LX/1Jg;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->af:LX/0g8;

    invoke-interface {v1, v2}, LX/1Jg;->a(LX/0g8;)V

    .line 2266379
    iget-object v1, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->af:LX/0g8;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ah:LX/1Jg;

    invoke-interface {v2}, LX/1Jg;->a()LX/0fx;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0g8;->b(LX/0fx;)V

    .line 2266380
    const/16 v1, 0x2b

    const v2, 0x1560e369

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5f29e946

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2266381
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onStart()V

    .line 2266382
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->Z:LX/Fea;

    .line 2266383
    new-instance v4, LX/FgP;

    invoke-direct {v4, v0, v2}, LX/FgP;-><init>(LX/1ZF;LX/Fea;)V

    .line 2266384
    move-object v0, v4

    .line 2266385
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->av:LX/0am;

    .line 2266386
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->av:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2266387
    iget-object v0, p0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->av:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FgP;

    .line 2266388
    iput-object p0, v0, LX/FgP;->c:LX/5OO;

    .line 2266389
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x1e2fe60

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
