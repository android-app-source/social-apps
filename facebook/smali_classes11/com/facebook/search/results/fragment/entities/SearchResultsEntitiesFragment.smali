.class public Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;
.super Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;
.source ""


# instance fields
.field public i:LX/Fe7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/Cxg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/1DS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/CzB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntitiesRootGroupPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/Cvq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/1Kt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/CvO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Landroid/content/Context;

.field private r:LX/Fje;

.field public s:LX/Fe6;

.field public t:LX/1Qq;

.field private u:LX/CvN;

.field private v:LX/0g8;

.field public w:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2265497
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;-><init>()V

    .line 2265498
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->w:Z

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2265494
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v0, v0

    .line 2265495
    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->jC_()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7CN;->a(LX/0Px;)Ljava/lang/String;

    move-result-object v0

    .line 2265496
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "unknown"

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2265416
    invoke-super {p0, p1}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->a(Landroid/os/Bundle;)V

    .line 2265417
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;

    const-class v3, LX/Fe7;

    invoke-interface {p1, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Fe7;

    const-class v4, LX/Cxg;

    invoke-interface {p1, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/Cxg;

    invoke-static {p1}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v5

    check-cast v5, LX/1DS;

    new-instance v6, LX/CzB;

    invoke-direct {v6}, LX/CzB;-><init>()V

    move-object v6, v6

    move-object v6, v6

    check-cast v6, LX/CzB;

    const/16 v7, 0x33e4

    invoke-static {p1, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p1}, LX/Cvq;->a(LX/0QB;)LX/Cvq;

    move-result-object v8

    check-cast v8, LX/Cvq;

    invoke-static {p1}, LX/23N;->b(LX/0QB;)LX/23N;

    move-result-object v9

    check-cast v9, LX/1Kt;

    const-class v0, LX/CvO;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/CvO;

    iput-object v3, v2, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->i:LX/Fe7;

    iput-object v4, v2, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->j:LX/Cxg;

    iput-object v5, v2, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->k:LX/1DS;

    iput-object v6, v2, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->l:LX/CzB;

    iput-object v7, v2, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->m:LX/0Ot;

    iput-object v8, v2, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->n:LX/Cvq;

    iput-object v9, v2, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->o:LX/1Kt;

    iput-object p1, v2, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->p:LX/CvO;

    .line 2265418
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->s()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->q:Landroid/content/Context;

    .line 2265419
    iget-object v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->k:LX/1DS;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->m:LX/0Ot;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->l:LX/CzB;

    invoke-virtual {v0, v1, v2}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v8

    iget-object v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->j:LX/Cxg;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->q:Landroid/content/Context;

    new-instance v2, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment$1;

    invoke-direct {v2, p0}, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment$1;-><init>(Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;)V

    iget-object v3, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->l:LX/CzB;

    .line 2265420
    iget-object v4, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v4, v4

    .line 2265421
    iget-object v5, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->l:LX/CzB;

    iget-object v6, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->l:LX/CzB;

    iget-object v7, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->l:LX/CzB;

    invoke-virtual/range {v0 .. v7}, LX/Cxg;->a(Landroid/content/Context;Ljava/lang/Runnable;LX/CzB;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/Cz2;LX/CzB;LX/CzB;)LX/Cxf;

    move-result-object v0

    .line 2265422
    iput-object v0, v8, LX/1Ql;->f:LX/1PW;

    .line 2265423
    move-object v0, v8

    .line 2265424
    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->t:LX/1Qq;

    .line 2265425
    iget-object v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->i:LX/Fe7;

    .line 2265426
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2265427
    iget-object v2, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->t:LX/1Qq;

    invoke-virtual {v0, v1, v2}, LX/Fe7;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/1Cw;)LX/Fe6;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->s:LX/Fe6;

    .line 2265428
    iget-object v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->s:LX/Fe6;

    .line 2265429
    iput-object p0, v0, LX/Fe6;->i:Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;

    .line 2265430
    iget-object v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->p:LX/CvO;

    .line 2265431
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2265432
    iget-object v2, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->l:LX/CzB;

    .line 2265433
    new-instance v5, LX/CvN;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v4

    check-cast v4, LX/CvY;

    invoke-direct {v5, v3, v4, v1, v2}, LX/CvN;-><init>(LX/0SG;LX/CvY;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzB;)V

    .line 2265434
    move-object v0, v5

    .line 2265435
    iput-object v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->u:LX/CvN;

    .line 2265436
    iget-object v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->o:LX/1Kt;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->u:LX/CvN;

    invoke-virtual {v0, v1}, LX/1Kt;->a(LX/1Ce;)V

    .line 2265437
    return-void
.end method

.method public final e()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2265493
    const-class v0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;

    return-object v0
.end method

.method public final k()V
    .locals 5

    .prologue
    .line 2265478
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->w:Z

    .line 2265479
    iget-object v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->l:LX/CzB;

    .line 2265480
    iget-object v1, v0, LX/CzB;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2265481
    iget-object v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->r:LX/Fje;

    .line 2265482
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2265483
    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Fje;->setTextViewQueryString(Ljava/lang/String;)V

    .line 2265484
    iget-object v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->s:LX/Fe6;

    .line 2265485
    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2265486
    const/4 v4, 0x0

    .line 2265487
    iget-object v2, v0, LX/Fe6;->b:LX/Cvq;

    invoke-interface {v1}, LX/CwB;->jC_()LX/0Px;

    move-result-object v3

    .line 2265488
    sget-object p0, LX/Cvx;->a:LX/Cvv;

    const/4 v1, 0x1

    invoke-static {v2, p0, v3, v1}, LX/Cvq;->a(LX/Cvq;LX/0Pq;LX/0Px;Z)V

    .line 2265489
    iput-object v4, v0, LX/Fe6;->k:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    .line 2265490
    sget-object v2, LX/EQG;->LOADING:LX/EQG;

    invoke-static {v0, v2}, LX/Fe6;->a(LX/Fe6;LX/EQG;)V

    .line 2265491
    invoke-static {v0, v4}, LX/Fe6;->a(LX/Fe6;Ljava/lang/String;)V

    .line 2265492
    return-void
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2265477
    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->w:Z

    return v0
.end method

.method public final n()V
    .locals 3

    .prologue
    .line 2265473
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->n()V

    .line 2265474
    iget-object v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->v:LX/0g8;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->o:LX/1Kt;

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    .line 2265475
    iget-object v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->o:LX/1Kt;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->v:LX/0g8;

    invoke-virtual {v0, v1, v2}, LX/1Kt;->a(ZLX/0g8;)V

    .line 2265476
    return-void
.end method

.method public final o()V
    .locals 2

    .prologue
    .line 2265469
    iget-object v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->v:LX/0g8;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->o:LX/1Kt;

    invoke-interface {v0, v1}, LX/0g8;->c(LX/0fx;)V

    .line 2265470
    iget-object v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->o:LX/1Kt;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->v:LX/0g8;

    invoke-virtual {v0, v1}, LX/1Kt;->c(LX/0g8;)V

    .line 2265471
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->o()V

    .line 2265472
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2265467
    iget-object v0, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->t:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qr;->a(Landroid/content/res/Configuration;)V

    .line 2265468
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x48da196d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2265450
    new-instance v1, LX/Fje;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->q:Landroid/content/Context;

    .line 2265451
    iget-object v3, p0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v3, v3

    .line 2265452
    invoke-virtual {v3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->z()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v3

    invoke-static {v3}, LX/Cw8;->from(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/Cw8;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/Fje;-><init>(Landroid/content/Context;LX/Cw8;)V

    iput-object v1, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->r:LX/Fje;

    .line 2265453
    iget-object v1, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->s:LX/Fe6;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->r:LX/Fje;

    .line 2265454
    iput-object v2, v1, LX/Fe6;->e:LX/Fje;

    .line 2265455
    iget-object v3, v1, LX/Fe6;->f:LX/EQG;

    if-eqz v3, :cond_0

    iget-object v3, v1, LX/Fe6;->f:LX/EQG;

    :goto_0
    invoke-static {v1, v3}, LX/Fe6;->a(LX/Fe6;LX/EQG;)V

    .line 2265456
    iget-object v3, v1, LX/Fe6;->e:LX/Fje;

    .line 2265457
    const/4 p1, 0x3

    invoke-virtual {v3, v1, p1}, LX/Fje;->a(LX/EQF;I)V

    .line 2265458
    iget-object v3, v2, LX/Fje;->q:LX/0g8;

    move-object v3, v3

    .line 2265459
    iget-object p1, v1, LX/Fe6;->h:LX/1Cw;

    invoke-interface {v3, p1}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2265460
    iget-object p1, v1, LX/Fe6;->d:LX/1Db;

    invoke-virtual {p1}, LX/1Db;->a()LX/1St;

    move-result-object p1

    invoke-interface {v3, p1}, LX/0g8;->a(LX/1St;)V

    .line 2265461
    invoke-interface {v3, v1}, LX/0g8;->b(LX/0fu;)V

    .line 2265462
    iget-object v1, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->r:LX/Fje;

    .line 2265463
    iget-object v2, v1, LX/Fje;->q:LX/0g8;

    move-object v1, v2

    .line 2265464
    iput-object v1, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->v:LX/0g8;

    .line 2265465
    iget-object v1, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->r:LX/Fje;

    const/16 v2, 0x2b

    const v3, -0x7e75490b

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1

    .line 2265466
    :cond_0
    sget-object v3, LX/EQG;->LOADING:LX/EQG;

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x695359d4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2265446
    iget-object v1, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->t:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2265447
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->q:Landroid/content/Context;

    .line 2265448
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onDestroy()V

    .line 2265449
    const/16 v1, 0x2b

    const v2, 0x38860424

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4322a772

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2265438
    iget-object v1, p0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->s:LX/Fe6;

    .line 2265439
    iget-object v2, v1, LX/Fe6;->a:LX/Fe5;

    .line 2265440
    iget-object v4, v2, LX/Fe5;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v4, :cond_0

    .line 2265441
    iget-object v4, v2, LX/Fe5;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2265442
    const/4 v4, 0x0

    iput-object v4, v2, LX/Fe5;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2265443
    :cond_0
    const/4 v2, 0x0

    iput-object v2, v1, LX/Fe6;->e:LX/Fje;

    .line 2265444
    invoke-super {p0}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->onDestroyView()V

    .line 2265445
    const/16 v1, 0x2b

    const v2, 0x5097d79d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
