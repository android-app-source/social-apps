.class public Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""

# interfaces
.implements LX/CyE;


# instance fields
.field public m:LX/FeO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private p:LX/7BH;

.field private q:LX/FeN;

.field public r:Lcom/facebook/search/model/KeywordTypeaheadUnit;

.field private s:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

.field private t:LX/8ci;

.field private u:Landroid/view/View;

.field public v:Landroid/support/v4/view/ViewPager;

.field private w:Lcom/facebook/fbui/glyph/GlyphView;

.field private x:LX/FZb;

.field private y:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleInterfaces$SearchResultsSerpTabsModule$Edges;",
            ">;"
        }
    .end annotation
.end field

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2265582
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method

.method private static b(Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;I)I
    .locals 1

    .prologue
    .line 2265583
    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->n:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->y:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    sub-int/2addr v0, p1

    add-int/lit8 p1, v0, -0x1

    :cond_0
    return p1
.end method

.method private b()V
    .locals 9

    .prologue
    .line 2265584
    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->r:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->z:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2265585
    if-nez v0, :cond_1

    .line 2265586
    :cond_0
    :goto_1
    return-void

    .line 2265587
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->m:LX/FeO;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->p:LX/7BH;

    iget-object v3, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->r:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    iget-object v4, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->s:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iget-object v5, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->t:LX/8ci;

    iget-object v6, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->x:LX/FZb;

    iget-object v8, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->y:LX/0Px;

    move-object v7, p0

    invoke-virtual/range {v0 .. v8}, LX/FeO;->a(LX/0gc;LX/7BH;Lcom/facebook/search/model/KeywordTypeaheadUnit;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;LX/FZb;LX/CyE;LX/0Px;)LX/FeN;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->q:LX/FeN;

    .line 2265588
    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->v:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->q:LX/FeN;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2265589
    const/4 v0, 0x0

    move v1, v0

    .line 2265590
    :goto_2
    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->y:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2265591
    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->y:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel$EdgesModel;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2265592
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2265593
    :cond_2
    invoke-static {p0, v1}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->b(Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;I)I

    move-result v0

    .line 2265594
    iget-object v1, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->v:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 2265595
    iget-object v1, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->o:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment$2;

    invoke-direct {v2, p0, v0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment$2;-><init>(Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;I)V

    const v0, 0x3c9d87ef

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/CyI;LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CyI;",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2265596
    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->y:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2265597
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 2265598
    :goto_0
    return-void

    .line 2265599
    :cond_0
    iget-object v1, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->v:Landroid/support/v4/view/ViewPager;

    invoke-static {p0, v0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->b(Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;I)I

    move-result v0

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, -0x1

    const/16 v0, 0x2a

    const v1, -0x1e4125d7

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2265600
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2265601
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 2265602
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 2265603
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a008a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2265604
    invoke-virtual {v1, v5, v5}, Landroid/view/Window;->setLayout(II)V

    .line 2265605
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const v2, 0x7f0e0962

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 2265606
    const/16 v1, 0x2b

    const v2, 0x76f0600e

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6df83fa3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2265607
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2265608
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;

    const-class v4, LX/FeO;

    invoke-interface {v1, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/FeO;

    invoke-static {v1}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object p1

    check-cast p1, LX/0hL;

    invoke-static {v1}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    iput-object v4, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->m:LX/FeO;

    iput-object p1, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->n:LX/0hL;

    iput-object v1, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->o:Landroid/os/Handler;

    .line 2265609
    const/16 v1, 0x2b

    const v2, -0x5c3833c7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/16 v0, 0x2a

    const v1, -0x589497ee

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2265610
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2265611
    invoke-static {v0}, LX/D0Q;->a(Landroid/os/Bundle;)LX/7BH;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->p:LX/7BH;

    .line 2265612
    const-string v2, "display_style"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v2

    .line 2265613
    iget-object v3, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v3, v3

    .line 2265614
    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/Window;->requestFeature(I)Z

    .line 2265615
    new-instance v3, LX/0P2;

    invoke-direct {v3}, LX/0P2;-><init>()V

    const-string v4, "source"

    const-string v5, "source"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/8ci;->a(Ljava/lang/String;)LX/8ci;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v3

    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    .line 2265616
    const-string v4, "graph_search_explore_tabs"

    invoke-static {v0, v4}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->y:LX/0Px;

    .line 2265617
    new-instance v4, LX/CwH;

    invoke-direct {v4}, LX/CwH;-><init>()V

    const-string v5, "query_title"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2265618
    iput-object v5, v4, LX/CwH;->b:Ljava/lang/String;

    .line 2265619
    move-object v4, v4

    .line 2265620
    const-string v5, "query_function"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2265621
    iput-object v5, v4, LX/CwH;->c:Ljava/lang/String;

    .line 2265622
    move-object v4, v4

    .line 2265623
    const-string v5, "query_vertical"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2265624
    iput-object v5, v4, LX/CwH;->e:Ljava/lang/String;

    .line 2265625
    move-object v4, v4

    .line 2265626
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2265627
    iput-object v2, v4, LX/CwH;->x:LX/0Px;

    .line 2265628
    move-object v2, v4

    .line 2265629
    iput-object v3, v2, LX/CwH;->y:LX/0P1;

    .line 2265630
    move-object v2, v2

    .line 2265631
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 2265632
    iput-object v3, v2, LX/CwH;->f:Ljava/lang/Boolean;

    .line 2265633
    move-object v2, v2

    .line 2265634
    invoke-virtual {v2}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->r:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 2265635
    new-instance v2, Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    const-string v3, "typeahead_session_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "candidate_session_id"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/facebook/search/logging/api/SearchTypeaheadSession;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->s:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    .line 2265636
    const-string v2, "source"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/8ci;->a(Ljava/lang/String;)LX/8ci;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->t:LX/8ci;

    .line 2265637
    const v0, 0x7f0306b6

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->u:Landroid/view/View;

    .line 2265638
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_0

    .line 2265639
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 2265640
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 2265641
    const/high16 v2, 0x4000000

    invoke-virtual {v0, v2}, Landroid/view/Window;->addFlags(I)V

    .line 2265642
    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->u:Landroid/view/View;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->u:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->u:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const/high16 v5, 0x41c00000    # 24.0f

    invoke-static {v4, v5}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->u:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->u:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 2265643
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->u:Landroid/view/View;

    const v2, 0x7f0d1241

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->v:Landroid/support/v4/view/ViewPager;

    .line 2265644
    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->u:Landroid/view/View;

    const v2, 0x7f0d1242

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->w:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2265645
    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->w:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v2, LX/FeL;

    invoke-direct {v2, p0}, LX/FeL;-><init>(Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2265646
    iput-boolean v7, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->z:Z

    .line 2265647
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->b()V

    .line 2265648
    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersiveHostFragment;->u:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, 0x3e4e816

    invoke-static {v8, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method
