.class public Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/CyE;


# instance fields
.field public a:LX/FeP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/7BH;

.field private e:LX/FeM;

.field public f:Lcom/facebook/search/model/KeywordTypeaheadUnit;

.field private g:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

.field private h:LX/8ci;

.field public i:Landroid/support/v4/view/ViewPager;

.field private j:Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;

.field private k:LX/FZb;

.field private l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleInterfaces$SearchResultsSerpTabsModule$Edges;",
            ">;"
        }
    .end annotation
.end field

.field public m:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2265504
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;I)I
    .locals 1

    .prologue
    .line 2265505
    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->b:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->l:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    sub-int/2addr v0, p1

    add-int/lit8 p1, v0, -0x1

    :cond_0
    return p1
.end method

.method private c()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 2265506
    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->f:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->m:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2265507
    if-nez v0, :cond_1

    .line 2265508
    :cond_0
    :goto_1
    return-void

    .line 2265509
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->a:LX/FeP;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->d:LX/7BH;

    iget-object v3, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->f:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    iget-object v4, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->g:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iget-object v5, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->h:LX/8ci;

    iget-object v6, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->k:LX/FZb;

    iget-object v8, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->l:LX/0Px;

    move-object v7, p0

    invoke-virtual/range {v0 .. v8}, LX/FeP;->a(LX/0gc;LX/7BH;Lcom/facebook/search/model/KeywordTypeaheadUnit;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;LX/FZb;LX/CyE;LX/0Px;)LX/FeM;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->e:LX/FeM;

    .line 2265510
    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->i:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->e:LX/FeM;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    move v1, v9

    .line 2265511
    :goto_2
    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->l:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2265512
    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->l:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel$EdgesModel;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2265513
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2265514
    :cond_2
    invoke-static {p0, v1}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->a(Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;I)I

    move-result v0

    .line 2265515
    iget-object v1, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->j:Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;

    if-eqz v1, :cond_3

    .line 2265516
    iget-object v1, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->j:Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->i:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2265517
    iget-object v1, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->b:LX/0hL;

    invoke-virtual {v1}, LX/0hL;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2265518
    iget-object v1, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->j:Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;

    invoke-virtual {v1, v9}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;->fullScroll(I)Z

    .line 2265519
    :goto_3
    iget-object v1, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->j:Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v9}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->a(IFI)V

    .line 2265520
    :cond_3
    iget-object v1, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->i:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 2265521
    iget-object v1, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->c:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment$2;

    invoke-direct {v2, p0, v0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment$2;-><init>(Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;I)V

    const v0, 0x64d4318b

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_1

    .line 2265522
    :cond_4
    iget-object v1, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->j:Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;

    invoke-virtual {v1, v9, v9}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;->scrollTo(II)V

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/CyI;LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CyI;",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2265523
    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->l:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2265524
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 2265525
    :goto_0
    return-void

    .line 2265526
    :cond_0
    iget-object v1, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->i:Landroid/support/v4/view/ViewPager;

    invoke-static {p0, v0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->a(Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;I)I

    move-result v0

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2265527
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2265528
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;

    const-class v2, LX/FeP;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/FeP;

    invoke-static {v0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object p1

    check-cast p1, LX/0hL;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v2, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->a:LX/FeP;

    iput-object p1, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->b:LX/0hL;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->c:Landroid/os/Handler;

    .line 2265529
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/16 v0, 0x2a

    const v1, -0x2b322182

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2265530
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2265531
    invoke-static {v0}, LX/D0Q;->a(Landroid/os/Bundle;)LX/7BH;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->d:LX/7BH;

    .line 2265532
    const-string v2, "display_style"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v2

    .line 2265533
    new-instance v3, LX/0P2;

    invoke-direct {v3}, LX/0P2;-><init>()V

    const-string v4, "source"

    const-string v5, "source"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/8ci;->a(Ljava/lang/String;)LX/8ci;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v3

    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    .line 2265534
    const-string v4, "graph_search_explore_tabs"

    invoke-static {v0, v4}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->l:LX/0Px;

    .line 2265535
    new-instance v4, LX/CwH;

    invoke-direct {v4}, LX/CwH;-><init>()V

    const-string v5, "query_title"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2265536
    iput-object v5, v4, LX/CwH;->b:Ljava/lang/String;

    .line 2265537
    move-object v4, v4

    .line 2265538
    const-string v5, "query_function"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2265539
    iput-object v5, v4, LX/CwH;->c:Ljava/lang/String;

    .line 2265540
    move-object v4, v4

    .line 2265541
    const-string v5, "query_vertical"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2265542
    iput-object v5, v4, LX/CwH;->e:Ljava/lang/String;

    .line 2265543
    move-object v4, v4

    .line 2265544
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2265545
    iput-object v2, v4, LX/CwH;->x:LX/0Px;

    .line 2265546
    move-object v2, v4

    .line 2265547
    iput-object v3, v2, LX/CwH;->y:LX/0P1;

    .line 2265548
    move-object v2, v2

    .line 2265549
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 2265550
    iput-object v3, v2, LX/CwH;->f:Ljava/lang/Boolean;

    .line 2265551
    move-object v2, v2

    .line 2265552
    invoke-virtual {v2}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->f:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 2265553
    new-instance v2, Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    const-string v3, "typeahead_session_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "candidate_session_id"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/facebook/search/logging/api/SearchTypeaheadSession;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->g:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    .line 2265554
    const-string v2, "source"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/8ci;->a(Ljava/lang/String;)LX/8ci;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->h:LX/8ci;

    .line 2265555
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->d:LX/7BH;

    invoke-static {v0, v2}, LX/8i2;->a(Landroid/content/Context;LX/7BH;)Landroid/view/ContextThemeWrapper;

    move-result-object v0

    .line 2265556
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_0

    .line 2265557
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 2265558
    const v3, 0x1010451

    invoke-static {v0, v3, v6}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v0

    .line 2265559
    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 2265560
    :cond_0
    const v0, 0x7f0306b5

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2265561
    const v0, 0x7f0d1241

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->i:Landroid/support/v4/view/ViewPager;

    .line 2265562
    const v0, 0x7f0d1240

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->j:Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;

    .line 2265563
    new-instance v0, LX/FeK;

    invoke-direct {v0, p0}, LX/FeK;-><init>(Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;)V

    .line 2265564
    iget-object v3, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->j:Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;

    .line 2265565
    iput-object v0, v3, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 2265566
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->m:Z

    .line 2265567
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->c()V

    .line 2265568
    const/16 v0, 0x2b

    const v3, 0x20db4690

    invoke-static {v7, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x350e7ad1    # -7914135.5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2265569
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2265570
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2265571
    if-eqz v0, :cond_0

    .line 2265572
    iget-object v2, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreHostFragment;->f:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-virtual {v2}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2265573
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x5e1525d5

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
