.class public Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersivePagerTitleStrip;
.super LX/3rt;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2265649
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersivePagerTitleStrip;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2265650
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2265660
    invoke-direct {p0, p1, p2}, LX/3rt;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2265661
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersivePagerTitleStrip;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0097

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2265662
    iput v0, p0, LX/3rt;->e:I

    .line 2265663
    iget-object v1, p0, LX/3rt;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2265664
    iget v1, p0, LX/3rt;->p:I

    shl-int/lit8 v1, v1, 0x18

    iget v3, p0, LX/3rt;->e:I

    const v4, 0xffffff

    and-int/2addr v3, v4

    or-int/2addr v1, v3

    .line 2265665
    iget-object v3, p0, LX/3rt;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2265666
    iget-object v3, p0, LX/3rt;->d:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2265667
    const/16 v0, 0x30

    .line 2265668
    iput v0, p0, LX/3rt;->i:I

    .line 2265669
    invoke-virtual {p0}, LX/3rt;->requestLayout()V

    .line 2265670
    const v0, 0x3f19999a    # 0.6f

    invoke-virtual {p0, v0}, LX/3rt;->setNonPrimaryAlpha(F)V

    .line 2265671
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersivePagerTitleStrip;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b004e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p0, v2, v0}, LX/3rt;->a(IF)V

    .line 2265672
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersivePagerTitleStrip;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b174e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/3rt;->setTextSpacing(I)V

    move v1, v2

    .line 2265673
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersivePagerTitleStrip;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2265674
    invoke-virtual {p0, v1}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersivePagerTitleStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 2265675
    invoke-virtual {p0, v1}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersivePagerTitleStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2265676
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 2265677
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 2265678
    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLines(I)V

    .line 2265679
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersivePagerTitleStrip;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b174b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setWidth(I)V

    .line 2265680
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2265681
    :cond_1
    return-void
.end method

.method private static a(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2265682
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, p0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2265683
    const-class v0, Landroid/text/style/ImageSpan;

    invoke-virtual {v1, v4, v3, v0}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/ImageSpan;

    .line 2265684
    array-length v2, v0

    if-nez v2, :cond_0

    move-object v0, v1

    .line 2265685
    :goto_0
    return-object v0

    .line 2265686
    :cond_0
    aget-object v0, v0, v4

    .line 2265687
    invoke-virtual {v0}, Landroid/text/style/ImageSpan;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2265688
    if-ne p1, v3, :cond_1

    .line 2265689
    const/16 v2, 0xff

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2265690
    :goto_1
    new-instance v2, Landroid/text/style/ImageSpan;

    invoke-direct {v2, v0, v3}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 2265691
    const/16 v0, 0x21

    invoke-virtual {v1, v2, v4, v3, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    move-object v0, v1

    .line 2265692
    goto :goto_0

    .line 2265693
    :cond_1
    const/16 v2, 0x99

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_1
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 2265651
    invoke-super {p0, p1}, LX/3rt;->onDraw(Landroid/graphics/Canvas;)V

    .line 2265652
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersivePagerTitleStrip;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2265653
    invoke-virtual {p0, v1}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersivePagerTitleStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 2265654
    invoke-virtual {p0, v1}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersivePagerTitleStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2265655
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2265656
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreImmersivePagerTitleStrip;->a(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 2265657
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2265658
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2265659
    :cond_1
    return-void
.end method
