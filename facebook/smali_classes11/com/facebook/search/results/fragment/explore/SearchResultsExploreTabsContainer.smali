.class public Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsContainer;
.super Lcom/facebook/fbui/pagerindicator/IconTabbedViewPagerIndicator$IconTabsContainer;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2265793
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2265794
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2265795
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2265796
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2265791
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/pagerindicator/IconTabbedViewPagerIndicator$IconTabsContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2265792
    return-void
.end method


# virtual methods
.method public final a(LX/0gG;I)Landroid/view/View;
    .locals 9

    .prologue
    .line 2265773
    invoke-virtual {p0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->d()Landroid/view/View;

    move-result-object v2

    .line 2265774
    instance-of v0, p1, LX/6Ue;

    if-nez v0, :cond_0

    .line 2265775
    new-instance v0, Landroid/view/InflateException;

    const-string v1, "Pager adapter should be a subclass of IconAndTextPagerAdapter"

    invoke-direct {v0, v1}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p1

    .line 2265776
    check-cast v0, LX/6Ue;

    move-object v1, v2

    .line 2265777
    check-cast v1, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;

    .line 2265778
    iget-object v3, v1, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    move-object v3, v3

    .line 2265779
    invoke-virtual {p1, p2}, LX/0gG;->G_(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 2265780
    invoke-interface {v0, p2}, LX/6Ue;->A_(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 2265781
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v1, ""

    .line 2265782
    :cond_1
    invoke-interface {v0, v3, p2}, LX/6Ue;->a(Landroid/widget/TextView;I)V

    .line 2265783
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2265784
    if-eqz v4, :cond_2

    .line 2265785
    invoke-virtual {v3}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 2265786
    const/4 v6, 0x0

    aget-object v6, v5, v6

    const/4 v7, 0x2

    aget-object v7, v5, v7

    const/4 v8, 0x3

    aget-object v5, v5, v8

    invoke-virtual {v3, v6, v4, v7, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2265787
    :cond_2
    invoke-interface {v0, p2}, LX/6Ue;->k_(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2265788
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    move-object v1, v0

    :cond_3
    invoke-virtual {v2, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2265789
    invoke-virtual {p0, v2}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsContainer;->addView(Landroid/view/View;)V

    .line 2265790
    return-object v2
.end method
