.class public Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;
.super Lcom/facebook/fbui/pagerindicator/IconAndTextTabbedViewPagerIndicator;
.source ""


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2265847
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2265848
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2265845
    const v0, 0x7f01018d

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2265846
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2265841
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/pagerindicator/IconAndTextTabbedViewPagerIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2265842
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;->a:Ljava/util/Map;

    .line 2265843
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;->b:Ljava/util/Set;

    .line 2265844
    return-void
.end method


# virtual methods
.method public final c(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 2265834
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    move-object v0, v0

    .line 2265835
    check-cast v0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsContainer;

    .line 2265836
    iget-object v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->d:LX/0gG;

    move-object v1, v1

    .line 2265837
    invoke-virtual {v0, v1, p1}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsContainer;->a(LX/0gG;I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2265838
    check-cast v0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;

    new-instance v2, LX/FeR;

    invoke-direct {v2, p0, p1}, LX/FeR;-><init>(Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;I)V

    .line 2265839
    iput-object v2, v0, Lcom/facebook/search/widget/exploretabs/SearchResultsExploreTabsViewPagerIndicatorBadgeTextView;->b:LX/FeQ;

    .line 2265840
    return-object v1
.end method

.method public getTabsContainerResource()I
    .locals 1

    .prologue
    .line 2265833
    const v0, 0x7f0312b0

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    .line 2265805
    invoke-super {p0, p1}, Lcom/facebook/fbui/pagerindicator/IconAndTextTabbedViewPagerIndicator;->onDraw(Landroid/graphics/Canvas;)V

    .line 2265806
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 2265807
    iget-object v0, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2265808
    iget-object v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    move-object v1, v1

    .line 2265809
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2265810
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v5

    add-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    .line 2265811
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;->getMeasuredHeight()I

    move-result v5

    int-to-float v5, v5

    .line 2265812
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b1752

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    sub-float v6, v4, v6

    .line 2265813
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v7

    int-to-float v7, v7

    .line 2265814
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b1752

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    add-float/2addr v8, v4

    .line 2265815
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    int-to-float v9, v1

    .line 2265816
    new-instance v10, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v10, v1}, Landroid/graphics/Paint;-><init>(I)V

    .line 2265817
    iget-object v1, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v11, 0x437f0000    # 255.0f

    mul-float/2addr v1, v11

    float-to-int v1, v1

    .line 2265818
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a00d2

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setColor(I)V

    .line 2265819
    invoke-virtual {v10, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2265820
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v10, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2265821
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 2265822
    invoke-virtual {v1, v8, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2265823
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2265824
    invoke-virtual {v1, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2265825
    invoke-virtual {v1, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2265826
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 2265827
    invoke-virtual {p1, v1, v10}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2265828
    iget-object v1, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const v4, 0x3d4ccccd    # 0.05f

    cmpg-float v1, v1, v4

    if-gtz v1, :cond_0

    .line 2265829
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2265830
    :cond_1
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2265831
    iget-object v2, p0, Lcom/facebook/search/results/fragment/explore/SearchResultsExploreTabsViewPagerIndicator;->b:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2265832
    :cond_2
    return-void
.end method
