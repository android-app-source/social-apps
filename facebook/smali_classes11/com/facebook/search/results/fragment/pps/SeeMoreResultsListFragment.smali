.class public Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/FZU;


# instance fields
.field public a:LX/FfG;

.field public b:LX/FgS;

.field public c:LX/Cvm;

.field public d:LX/EQ6;

.field public e:LX/2SY;

.field public f:LX/FZb;

.field public g:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field public h:LX/FfH;

.field public i:Landroid/support/v4/view/ViewPager;

.field public j:Ljava/lang/String;

.field public k:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2267689
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2267671
    if-eqz p1, :cond_0

    .line 2267672
    const-string v0, "typeahead_text"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->j:Ljava/lang/String;

    .line 2267673
    const-string v0, "tab_position"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->k:I

    .line 2267674
    iget-object v0, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->c:LX/Cvm;

    .line 2267675
    new-instance v2, Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    const-string v3, "typeahead_session_id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "candidate_results_id"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/facebook/search/logging/api/SearchTypeaheadSession;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, v0, LX/Cvm;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    .line 2267676
    const-string v2, "see_more_session_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/Cvm;->f:Ljava/lang/String;

    .line 2267677
    iget-object v2, v0, LX/Cvm;->f:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 2267678
    invoke-static {v0}, LX/Cvm;->c(LX/Cvm;)V

    .line 2267679
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->j:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2267680
    iget-object v0, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->b:LX/FgS;

    .line 2267681
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2267682
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 2267683
    iget-object v1, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2267684
    iget-object v0, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->i:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->k:I

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2267685
    :cond_1
    return-void

    .line 2267686
    :cond_2
    const-string v2, "see_more_session_start_time_ms"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v0, LX/Cvm;->g:J

    .line 2267687
    const-string v2, "see_more_results_clicked"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v0, LX/Cvm;->h:I

    .line 2267688
    invoke-static {v0}, LX/Cvm;->d(LX/Cvm;)V

    goto :goto_0
.end method

.method public static c(Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2267667
    iget-object v0, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->b:LX/FgS;

    .line 2267668
    iget-object p0, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, p0

    .line 2267669
    iget-object p0, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, p0

    .line 2267670
    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2267666
    const-string v0, "pps"

    return-object v0
.end method

.method public final a(LX/FZb;)V
    .locals 0

    .prologue
    .line 2267664
    iput-object p1, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->f:LX/FZb;

    .line 2267665
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2267661
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2267662
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;

    const-class v3, LX/FfH;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/FfH;

    invoke-static {v0}, LX/FgS;->a(LX/0QB;)LX/FgS;

    move-result-object v4

    check-cast v4, LX/FgS;

    new-instance v1, LX/Cvm;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object p1

    check-cast p1, LX/0SG;

    invoke-static {v0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object p0

    check-cast p0, LX/2Sc;

    invoke-direct {v1, v5, p1, p0}, LX/Cvm;-><init>(LX/0Zb;LX/0SG;LX/2Sc;)V

    move-object v5, v1

    check-cast v5, LX/Cvm;

    invoke-static {v0}, LX/EQ6;->b(LX/0QB;)LX/EQ6;

    move-result-object p1

    check-cast p1, LX/EQ6;

    invoke-static {v0}, LX/2SY;->a(LX/0QB;)LX/2SY;

    move-result-object v0

    check-cast v0, LX/2SY;

    iput-object v4, v2, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->b:LX/FgS;

    iput-object v5, v2, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->c:LX/Cvm;

    iput-object p1, v2, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->d:LX/EQ6;

    iput-object v0, v2, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->e:LX/2SY;

    iput-object v3, v2, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->h:LX/FfH;

    .line 2267663
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2267618
    iget-object v0, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->c:LX/Cvm;

    .line 2267619
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    const-string v2, "search_sid"

    iget-object p0, v0, LX/Cvm;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, p0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    move-object v0, v1

    .line 2267620
    return-object v0
.end method

.method public final b(Z)Z
    .locals 5

    .prologue
    .line 2267656
    iget-object v0, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->c:LX/Cvm;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->a:LX/FfG;

    invoke-virtual {v1}, LX/FfG;->f()LX/0Px;

    move-result-object v1

    invoke-static {p0}, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->c(Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;)Ljava/lang/String;

    move-result-object v2

    .line 2267657
    invoke-virtual {v0, v1}, LX/CvH;->a(Ljava/util/List;)LX/162;

    move-result-object v3

    .line 2267658
    const-string v4, "click"

    invoke-static {v0, v4}, LX/Cvm;->a(LX/Cvm;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string p0, "action"

    const-string p1, "end_back_button"

    invoke-virtual {v4, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string p0, "query"

    invoke-virtual {v4, p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string p0, "end_session_candidate_results"

    invoke-virtual {v4, p0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2267659
    iget-object v4, v0, LX/Cvm;->b:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2267660
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Landroid/support/v4/app/Fragment;
    .locals 0

    .prologue
    .line 2267655
    return-object p0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/16 v0, 0x2a

    const v1, -0x79854a06

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2267645
    const v0, 0x7f0306b8

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2267646
    const v0, 0x7f0d1240

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->g:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2267647
    const v0, 0x7f0d1241

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->i:Landroid/support/v4/view/ViewPager;

    .line 2267648
    iget-object v0, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->h:LX/FfH;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->i:Landroid/support/v4/view/ViewPager;

    new-instance v5, LX/Ff9;

    invoke-direct {v5, p0}, LX/Ff9;-><init>(Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;)V

    iget-object v6, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->c:LX/Cvm;

    invoke-virtual {v0, v3, v4, v5, v6}, LX/FfH;->a(LX/0gc;Landroid/support/v4/view/ViewPager;LX/Ff9;LX/Cvm;)LX/FfG;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->a:LX/FfG;

    .line 2267649
    iget-object v0, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->g:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    new-instance v3, LX/FfA;

    invoke-direct {v3, p0}, LX/FfA;-><init>(Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;)V

    .line 2267650
    iput-object v3, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 2267651
    iget-object v0, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->i:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->a:LX/FfG;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2267652
    iget-object v0, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->g:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v3, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->i:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2267653
    invoke-direct {p0, p3}, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->b(Landroid/os/Bundle;)V

    .line 2267654
    const/16 v0, 0x2b

    const v3, -0x7c52324b

    invoke-static {v7, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x347d353

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2267638
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2267639
    iget-object v1, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->a:LX/FfG;

    .line 2267640
    iget-object v2, v1, LX/FfG;->g:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 2267641
    iget-object v2, v1, LX/FfG;->h:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 2267642
    iget-object v2, v1, LX/FfG;->f:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 2267643
    iget-object v2, v1, LX/FfG;->e:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 2267644
    const/16 v1, 0x2b

    const v2, 0x24ec20a6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onHiddenChanged(Z)V
    .locals 1

    .prologue
    .line 2267633
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onHiddenChanged(Z)V

    .line 2267634
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->a:LX/FfG;

    if-eqz v0, :cond_0

    .line 2267635
    iget-object v0, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->a:LX/FfG;

    .line 2267636
    iget-object p0, v0, LX/FfG;->d:LX/FfJ;

    invoke-virtual {p0}, LX/FfJ;->b()V

    .line 2267637
    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2267623
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2267624
    const-string v0, "typeahead_text"

    iget-object v1, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2267625
    const-string v0, "tab_position"

    iget v1, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->k:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2267626
    iget-object v0, p0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->c:LX/Cvm;

    .line 2267627
    const-string v2, "typeahead_session_id"

    iget-object v3, v0, LX/Cvm;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iget-object v3, v3, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2267628
    const-string v2, "candidate_results_id"

    iget-object v3, v0, LX/Cvm;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iget-object v3, v3, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->c:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2267629
    const-string v2, "see_more_session_id"

    iget-object v3, v0, LX/Cvm;->f:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2267630
    const-string v2, "see_more_session_start_time_ms"

    iget-wide v4, v0, LX/Cvm;->g:J

    invoke-virtual {p1, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2267631
    const-string v2, "see_more_results_clicked"

    iget v3, v0, LX/Cvm;->h:I

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2267632
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x22c1e67c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2267621
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2267622
    const/16 v1, 0x2b

    const v2, -0x47d10032

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
