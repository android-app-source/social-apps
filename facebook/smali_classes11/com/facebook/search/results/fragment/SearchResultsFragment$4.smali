.class public final Lcom/facebook/search/results/fragment/SearchResultsFragment$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Lcom/facebook/search/results/fragment/SearchResultsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/fragment/SearchResultsFragment;LX/0Px;)V
    .locals 0

    .prologue
    .line 2264530
    iput-object p1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment$4;->b:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iput-object p2, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment$4;->a:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 2264531
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment$4;->b:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    .line 2264532
    iget-object v1, v0, LX/FcD;->i:LX/0Px;

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2264533
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment$4;->b:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcD;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment$4;->b:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v2, v2, Lcom/facebook/search/results/fragment/SearchResultsFragment;->z:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment$4;->a:LX/0Px;

    invoke-static {v2}, LX/Fcs;->a(LX/0Px;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/FcD;->a(LX/0Px;)V

    .line 2264534
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment$4;->b:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->E:LX/FcV;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment$4;->b:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    .line 2264535
    iget-object v2, v1, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v2

    .line 2264536
    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/FcV;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2264537
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment$4;->b:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    invoke-static {v0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->y(Lcom/facebook/search/results/fragment/SearchResultsFragment;)V

    .line 2264538
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment$4;->b:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-boolean v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Y:Z

    if-eqz v0, :cond_1

    .line 2264539
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment$4;->b:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    invoke-virtual {v0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->c()V

    .line 2264540
    iget-object v0, p0, Lcom/facebook/search/results/fragment/SearchResultsFragment$4;->b:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    const/4 v1, 0x0

    .line 2264541
    iput-boolean v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->Y:Z

    .line 2264542
    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
