.class public Lcom/facebook/search/results/fragment/elections/SearchResultsElectionDetailsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/Cys;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/FeF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/components/ComponentView;

.field public d:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field public e:LX/FeE;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2265362
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static c(Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionInterfaces$SearchResultsElection;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionInterfaces$SearchResultsElectionRace;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2265363
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2265364
    if-nez p0, :cond_0

    .line 2265365
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2265366
    :goto_0
    return-object v0

    .line 2265367
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_2

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;

    .line 2265368
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v1, v2

    :goto_2
    if-ge v1, v8, :cond_1

    invoke-virtual {v7, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;

    .line 2265369
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2265370
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2265371
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 2265372
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionInterfaces$SearchResultsElection;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionInterfaces$SearchResultsCandidateInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2265373
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2265374
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsPartyInfoModel;

    .line 2265375
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsPartyInfoModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2265376
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2265377
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2265378
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2265379
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/search/results/fragment/elections/SearchResultsElectionDetailsFragment;

    invoke-static {p1}, LX/Cys;->a(LX/0QB;)LX/Cys;

    move-result-object v2

    check-cast v2, LX/Cys;

    const-class v0, LX/FeF;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/FeF;

    iput-object v2, p0, Lcom/facebook/search/results/fragment/elections/SearchResultsElectionDetailsFragment;->a:LX/Cys;

    iput-object p1, p0, Lcom/facebook/search/results/fragment/elections/SearchResultsElectionDetailsFragment;->b:LX/FeF;

    .line 2265380
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x46d0a0eb

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2265381
    const v0, 0x7f0312ad

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2265382
    const v0, 0x7f0d0be3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/components/ComponentView;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/elections/SearchResultsElectionDetailsFragment;->c:Lcom/facebook/components/ComponentView;

    .line 2265383
    const v0, 0x7f0d0553

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/search/results/fragment/elections/SearchResultsElectionDetailsFragment;->d:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2265384
    iget-object v0, p0, Lcom/facebook/search/results/fragment/elections/SearchResultsElectionDetailsFragment;->d:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2265385
    new-instance v3, LX/FeG;

    invoke-direct {v3, p0}, LX/FeG;-><init>(Lcom/facebook/search/results/fragment/elections/SearchResultsElectionDetailsFragment;)V

    move-object v3, v3

    .line 2265386
    invoke-virtual {v0, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2265387
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v0

    .line 2265388
    if-nez v3, :cond_0

    .line 2265389
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2265390
    const/16 v0, 0x2b

    const v3, -0x5da2d368

    invoke-static {v4, v0, v3, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    move-object v0, v1

    .line 2265391
    :goto_0
    return-object v0

    .line 2265392
    :cond_0
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2265393
    const-string v4, "query_title"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2265394
    if-eqz v0, :cond_1

    if-eqz v4, :cond_1

    .line 2265395
    invoke-interface {v0, v4}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2265396
    :cond_1
    const-string v0, "model"

    invoke-static {v3, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    .line 2265397
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->d()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/search/results/fragment/elections/SearchResultsElectionDetailsFragment;->f:Ljava/lang/String;

    .line 2265398
    invoke-static {v0}, Lcom/facebook/search/results/fragment/elections/SearchResultsElectionDetailsFragment;->c(Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;)LX/0Px;

    move-result-object v3

    invoke-static {v0}, Lcom/facebook/search/results/fragment/elections/SearchResultsElectionDetailsFragment;->d(Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;)LX/0Px;

    move-result-object v0

    .line 2265399
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    .line 2265400
    new-instance v5, LX/1P0;

    invoke-direct {v5, v4}, LX/1P0;-><init>(Landroid/content/Context;)V

    .line 2265401
    iget-object v6, p0, Lcom/facebook/search/results/fragment/elections/SearchResultsElectionDetailsFragment;->b:LX/FeF;

    .line 2265402
    new-instance p3, LX/FeE;

    const-class v7, Landroid/content/Context;

    invoke-interface {v6, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v6}, LX/EKb;->a(LX/0QB;)LX/EKb;

    move-result-object p1

    check-cast p1, LX/EKb;

    invoke-static {v6}, LX/EKn;->a(LX/0QB;)LX/EKn;

    move-result-object p2

    check-cast p2, LX/EKn;

    invoke-direct {p3, v5, v7, p1, p2}, LX/FeE;-><init>(LX/1P0;Landroid/content/Context;LX/EKb;LX/EKn;)V

    .line 2265403
    move-object v6, p3

    .line 2265404
    iput-object v6, p0, Lcom/facebook/search/results/fragment/elections/SearchResultsElectionDetailsFragment;->e:LX/FeE;

    .line 2265405
    iget-object v6, p0, Lcom/facebook/search/results/fragment/elections/SearchResultsElectionDetailsFragment;->e:LX/FeE;

    invoke-virtual {v6, v3}, LX/FeE;->a(LX/0Px;)V

    .line 2265406
    iget-object v6, p0, Lcom/facebook/search/results/fragment/elections/SearchResultsElectionDetailsFragment;->e:LX/FeE;

    invoke-virtual {v6, v0}, LX/FeE;->b(LX/0Px;)V

    .line 2265407
    new-instance v6, LX/1De;

    invoke-direct {v6, v4}, LX/1De;-><init>(Landroid/content/Context;)V

    .line 2265408
    invoke-static {v6}, LX/5K1;->c(LX/1De;)LX/5Jz;

    move-result-object v4

    iget-object v7, p0, Lcom/facebook/search/results/fragment/elections/SearchResultsElectionDetailsFragment;->e:LX/FeE;

    invoke-virtual {v4, v7}, LX/5Jz;->a(LX/5Je;)LX/5Jz;

    move-result-object v4

    .line 2265409
    new-instance v7, LX/FeJ;

    invoke-direct {v7, p0, v5}, LX/FeJ;-><init>(Lcom/facebook/search/results/fragment/elections/SearchResultsElectionDetailsFragment;LX/1P0;)V

    move-object v5, v7

    .line 2265410
    invoke-virtual {v4, v5}, LX/5Jz;->a(LX/1OX;)LX/5Jz;

    move-result-object v4

    invoke-static {v6, v4}, LX/1dV;->a(LX/1De;LX/1X5;)LX/1me;

    move-result-object v4

    invoke-virtual {v4}, LX/1me;->b()LX/1dV;

    move-result-object v4

    .line 2265411
    iget-object v5, p0, Lcom/facebook/search/results/fragment/elections/SearchResultsElectionDetailsFragment;->c:Lcom/facebook/components/ComponentView;

    invoke-virtual {v5, v4}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 2265412
    const v0, -0x1efdfe04

    invoke-static {v0, v2}, LX/02F;->f(II)V

    move-object v0, v1

    goto/16 :goto_0
.end method
