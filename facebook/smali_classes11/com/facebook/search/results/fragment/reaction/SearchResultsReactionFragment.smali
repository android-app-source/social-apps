.class public Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;
.super Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/0yL;
.implements LX/Fdv;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public A:Ljava/lang/String;

.field public B:Ljava/lang/String;

.field public C:Ljava/lang/String;

.field public D:LX/8ci;

.field public E:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field

.field public F:J

.field private G:J

.field private H:Z

.field public I:Z

.field private J:Z

.field private K:Z

.field private L:Z

.field public i:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/CIs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0gh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/E1m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/2d1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/1vC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/CfW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cga;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field public v:LX/0Yb;

.field public w:LX/0Yb;

.field public x:Landroid/os/Bundle;

.field private y:J

.field public z:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2268295
    const-class v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2268296
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;-><init>()V

    .line 2268297
    new-instance v0, Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-direct {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268298
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->x:Landroid/os/Bundle;

    .line 2268299
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->y:J

    .line 2268300
    iput-boolean v2, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->H:Z

    .line 2268301
    iput-boolean v2, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->I:Z

    .line 2268302
    iput-boolean v2, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->J:Z

    .line 2268303
    iput-boolean v2, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->K:Z

    .line 2268304
    iput-boolean v2, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->L:Z

    return-void
.end method

.method private Q()V
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 2268305
    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->L:Z

    if-eqz v0, :cond_0

    .line 2268306
    :goto_0
    return-void

    .line 2268307
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->D:LX/8ci;

    if-nez v0, :cond_1

    .line 2268308
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->j:LX/03V;

    sget-object v1, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->a:Ljava/lang/String;

    const-string v2, "mSerpSource should not be null, look at sSourceMap in GraphSearchNavigationController"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268309
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->l:LX/CIs;

    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->C:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->E:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->z:Ljava/lang/Long;

    if-nez v3, :cond_2

    move-object v3, v6

    :goto_1
    iget-object v4, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v4}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->B:Ljava/lang/String;

    iget-object v7, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->D:LX/8ci;

    if-nez v7, :cond_3

    :goto_2
    iget-object v7, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268310
    iget-object v8, v7, Lcom/facebook/search/results/model/SearchResultsMutableContext;->f:Ljava/lang/String;

    move-object v7, v8

    .line 2268311
    iget-object v8, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268312
    iget-object v9, v8, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v8, v9

    .line 2268313
    iget-object v8, v8, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->c:Ljava/lang/String;

    iget-object v9, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268314
    iget-object v10, v9, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v9, v10

    .line 2268315
    iget-object v9, v9, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    iget-object v10, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v10}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->jA_()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {v0 .. v10}, LX/CIs;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2268316
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->L:Z

    goto :goto_0

    .line 2268317
    :cond_2
    iget-object v3, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->z:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_3
    iget-object v6, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->D:LX/8ci;

    invoke-virtual {v6}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_2
.end method

.method public static R(Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;)V
    .locals 13

    .prologue
    const/4 v6, 0x0

    .line 2268318
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->D:LX/8ci;

    if-nez v0, :cond_0

    .line 2268319
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->j:LX/03V;

    sget-object v1, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->a:Ljava/lang/String;

    const-string v2, "mSerpSource should not be null, look at sSourceMap in GraphSearchNavigationController"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268320
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->l:LX/CIs;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->z:Ljava/lang/Long;

    if-nez v3, :cond_1

    move-object v3, v6

    :goto_0
    iget-object v4, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v4}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->B:Ljava/lang/String;

    iget-object v7, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->D:LX/8ci;

    if-nez v7, :cond_2

    :goto_1
    iget-object v7, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268321
    iget-object v8, v7, Lcom/facebook/search/results/model/SearchResultsMutableContext;->f:Ljava/lang/String;

    move-object v7, v8

    .line 2268322
    iget-object v8, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268323
    iget-object v9, v8, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v8, v9

    .line 2268324
    iget-object v8, v8, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->c:Ljava/lang/String;

    iget-object v9, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268325
    iget-object v10, v9, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v9, v10

    .line 2268326
    iget-object v9, v9, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    iget-object v10, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v10}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->jA_()Ljava/lang/String;

    move-result-object v10

    .line 2268327
    iget-object v11, v0, LX/CIs;->a:LX/0Zb;

    const-string v12, "local_serp_impression"

    const-string p0, "local_serp"

    invoke-static {v12, v1, p0, v2}, LX/CIs;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    const-string p0, "candidate_result_sid"

    invoke-virtual {v12, p0, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    const-string p0, "place_id"

    invoke-virtual {v12, p0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    const-string p0, "query"

    invoke-virtual {v12, p0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    const-string p0, "reaction_session_id"

    invoke-virtual {v12, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    const-string p0, "results_vertical"

    invoke-virtual {v12, p0, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    const-string p0, "semantic"

    invoke-virtual {v12, p0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    const-string p0, "source"

    invoke-virtual {v12, p0, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    const-string p0, "search_ts_token"

    invoke-virtual {v12, p0, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    const-string p0, "typeahead_sid"

    invoke-virtual {v12, p0, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    invoke-interface {v11, v12}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2268328
    return-void

    .line 2268329
    :cond_1
    iget-object v3, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->z:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->D:LX/8ci;

    invoke-virtual {v6}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_1
.end method

.method private S()V
    .locals 1

    .prologue
    .line 2268330
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->v:LX/0Yb;

    if-eqz v0, :cond_0

    .line 2268331
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->v:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 2268332
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->w:LX/0Yb;

    if-eqz v0, :cond_1

    .line 2268333
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->w:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 2268334
    :cond_1
    return-void
.end method

.method private T()V
    .locals 2

    .prologue
    .line 2268335
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->x:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->x:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m()LX/2ja;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2268336
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m()LX/2ja;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->x:Landroid/os/Bundle;

    .line 2268337
    iput-object v1, v0, LX/2ja;->b:Landroid/os/Bundle;

    .line 2268338
    :cond_0
    return-void
.end method

.method public static synthetic a(Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;J)J
    .locals 3

    .prologue
    .line 2268263
    iget-wide v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->G:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->G:J

    return-wide v0
.end method

.method private a(LX/8ci;)V
    .locals 2

    .prologue
    .line 2268339
    iput-object p1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->D:LX/8ci;

    .line 2268340
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->D:LX/8ci;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m()LX/2ja;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2268341
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m()LX/2ja;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->D:LX/8ci;

    invoke-virtual {v1}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2268342
    iput-object v1, v0, LX/2ja;->k:Ljava/lang/String;

    .line 2268343
    :cond_0
    return-void
.end method

.method public static r(Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;)Z
    .locals 1

    .prologue
    .line 2268344
    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->K:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->I:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2268345
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->C:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/content/Context;)LX/E8m;
    .locals 4

    .prologue
    .line 2268346
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(Landroid/content/Context;)LX/E8m;

    move-result-object v0

    .line 2268347
    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->o:LX/2d1;

    .line 2268348
    iget-object v2, v1, LX/2d1;->a:LX/0Uh;

    const/16 v3, 0x3f8

    const/4 p1, 0x0

    invoke-virtual {v2, v3, p1}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v1, v2

    .line 2268349
    if-eqz v1, :cond_0

    .line 2268350
    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->n:LX/E1m;

    invoke-virtual {v0, v1}, LX/E8m;->a(LX/1DZ;)V

    .line 2268351
    :cond_0
    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2268155
    const-string v0, "graph_search_results_page_reaction"

    return-object v0
.end method

.method public final a(LX/2jY;)V
    .locals 6
    .param p1    # LX/2jY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2268352
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(LX/2jY;)V

    .line 2268353
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    move-object v0, v0

    .line 2268354
    invoke-virtual {v0}, LX/E8m;->d()I

    move-result v0

    if-lez v0, :cond_0

    .line 2268355
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->H:Z

    .line 2268356
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->t()J

    .line 2268357
    :cond_0
    const/4 v2, -0x1

    .line 2268358
    const/4 v0, 0x0

    .line 2268359
    iget-object v1, p1, LX/2jY;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2268360
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_1
    move v1, v2

    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 2268361
    :goto_1
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2268362
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2268363
    const-string v3, "try_show_survey_on_result_integration_point_id"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2268364
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2268365
    iget-object v3, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268366
    iget-object v4, v3, Lcom/facebook/search/results/model/SearchResultsMutableContext;->t:Ljava/lang/String;

    move-object v3, v4

    .line 2268367
    if-eqz v3, :cond_2

    .line 2268368
    const-string v3, "results_vertical"

    iget-object v4, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268369
    iget-object v5, v4, Lcom/facebook/search/results/model/SearchResultsMutableContext;->t:Ljava/lang/String;

    move-object v4, v5

    .line 2268370
    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268371
    :cond_2
    iget-object v3, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 2268372
    const-string v3, "query_function"

    iget-object v4, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v4}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268373
    :cond_3
    iget-object v3, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 2268374
    const-string v3, "query"

    iget-object v4, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v4}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268375
    :cond_4
    iget-object v3, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268376
    iget-object v4, v3, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    move-object v3, v4

    .line 2268377
    if-eqz v3, :cond_5

    .line 2268378
    const-string v3, "results_source"

    iget-object v4, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268379
    iget-object v5, v4, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    move-object v4, v5

    .line 2268380
    invoke-virtual {v4}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268381
    :cond_5
    iget-object v3, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268382
    iget-object v4, v3, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v3, v4

    .line 2268383
    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268384
    iget-object v4, v3, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v3, v4

    .line 2268385
    iget-object v3, v3, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 2268386
    const-string v3, "typeahead_sid"

    iget-object v4, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268387
    iget-object v5, v4, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v4, v5

    .line 2268388
    iget-object v4, v4, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268389
    :cond_6
    const-string v3, "reaction_session_id"

    .line 2268390
    iget-object v4, p1, LX/2jY;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2268391
    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268392
    const-string v3, "try_show_survey_on_result_extra_data"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2268393
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2268394
    :cond_7
    return-void

    .line 2268395
    :sswitch_0
    const-string v3, "ANDROID_SEARCH_LOCAL_NULL"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto/16 :goto_0

    :sswitch_1
    const-string v3, "ANDROID_SEARCH_LOCAL_PLACE_TIPS"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto/16 :goto_0

    :sswitch_2
    const-string v3, "ANDROID_SEARCH_LOCAL_PLACE_TIPS_CHECKIN"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    goto/16 :goto_0

    :sswitch_3
    const-string v3, "ANDROID_SEARCH_LOCAL_PLACE_TIPS_LOCATION"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    goto/16 :goto_0

    :sswitch_4
    const-string v3, "ANDROID_SEARCH_LOCAL_SINGLE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x4

    goto/16 :goto_0

    :sswitch_5
    const-string v3, "ANDROID_SEARCH_LOCAL_TYPEAHEAD"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x5

    goto/16 :goto_0

    :sswitch_6
    const-string v3, "ANDROID_SEARCH_LOCAL_PUSH"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x6

    goto/16 :goto_0

    .line 2268396
    :pswitch_0
    const-string v0, "517105395138503"

    goto/16 :goto_1

    .line 2268397
    :pswitch_1
    const-string v0, "1705302933021543"

    goto/16 :goto_1

    .line 2268398
    :pswitch_2
    const-string v0, "1528797434114560"

    goto/16 :goto_1

    .line 2268399
    :pswitch_3
    const-string v0, "1761511150749287"

    goto/16 :goto_1

    .line 2268400
    :pswitch_4
    const-string v0, "566380456861524"

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x421081f4 -> :sswitch_5
        -0x7cf3f57 -> :sswitch_3
        0x9219f02 -> :sswitch_0
        0x9228895 -> :sswitch_6
        0x2f8134cb -> :sswitch_1
        0x4f15e703 -> :sswitch_4
        0x7aeb3c99 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(LX/9qT;)V
    .locals 3

    .prologue
    .line 2268401
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(LX/9qT;)V

    .line 2268402
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->n()Ljava/lang/String;

    move-result-object v0

    .line 2268403
    const-string v1, "NO_SESSION_ID"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2268404
    :goto_0
    return-void

    .line 2268405
    :cond_0
    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->k:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment$1;-><init>(Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;Ljava/lang/String;)V

    const v0, -0x1f048bbd

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final a(LX/CwB;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;)V
    .locals 4

    .prologue
    .line 2268406
    invoke-direct {p0, p3}, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->a(LX/8ci;)V

    .line 2268407
    invoke-interface {p1}, LX/CwB;->n()Lcom/facebook/search/model/ReactionSearchData;

    move-result-object v1

    .line 2268408
    if-eqz v1, :cond_0

    .line 2268409
    iget-object v0, v1, Lcom/facebook/search/model/ReactionSearchData;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2268410
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->z:Ljava/lang/Long;

    .line 2268411
    iget-object v0, v1, Lcom/facebook/search/model/ReactionSearchData;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2268412
    iput-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->A:Ljava/lang/String;

    .line 2268413
    iget-object v0, v1, Lcom/facebook/search/model/ReactionSearchData;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2268414
    iput-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->B:Ljava/lang/String;

    .line 2268415
    iget-object v0, v1, Lcom/facebook/search/model/ReactionSearchData;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2268416
    iput-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->C:Ljava/lang/String;

    .line 2268417
    iget-object v0, v1, Lcom/facebook/search/model/ReactionSearchData;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2268418
    iput-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->E:Ljava/lang/String;

    .line 2268419
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a(LX/CwB;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;)V

    .line 2268420
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->x:Landroid/os/Bundle;

    if-nez v0, :cond_2

    .line 2268421
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->x:Landroid/os/Bundle;

    .line 2268422
    :goto_1
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->x:Landroid/os/Bundle;

    const-string v1, "candidate_result_sid"

    iget-object v2, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268423
    iget-object v3, v2, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v2, v3

    .line 2268424
    iget-object v2, v2, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268425
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->x:Landroid/os/Bundle;

    const-string v1, "query"

    iget-object v2, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268426
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->x:Landroid/os/Bundle;

    const-string v1, "results_vertical"

    iget-object v2, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->jA_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268427
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->x:Landroid/os/Bundle;

    const-string v1, "search_ts_token"

    iget-object v2, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268428
    iget-object v3, v2, Lcom/facebook/search/results/model/SearchResultsMutableContext;->f:Ljava/lang/String;

    move-object v2, v3

    .line 2268429
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268430
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->x:Landroid/os/Bundle;

    const-string v1, "typeahead_sid"

    iget-object v2, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268431
    iget-object v3, v2, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v2, v3

    .line 2268432
    iget-object v2, v2, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268433
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->T()V

    .line 2268434
    return-void

    .line 2268435
    :cond_1
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 2268436
    :cond_2
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->x:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    goto :goto_1
.end method

.method public final a(LX/FZb;)V
    .locals 0

    .prologue
    .line 2268437
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2268264
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v5

    check-cast v5, Landroid/os/Handler;

    invoke-static {v0}, LX/CIs;->b(LX/0QB;)LX/CIs;

    move-result-object v6

    check-cast v6, LX/CIs;

    invoke-static {v0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v7

    check-cast v7, LX/0gh;

    invoke-static {v0}, LX/E1m;->a(LX/0QB;)LX/E1m;

    move-result-object v8

    check-cast v8, LX/E1m;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v9

    check-cast v9, LX/2d1;

    invoke-static {v0}, LX/1vC;->a(LX/0QB;)LX/1vC;

    move-result-object v10

    check-cast v10, LX/1vC;

    invoke-static {v0}, LX/CfW;->b(LX/0QB;)LX/CfW;

    move-result-object v11

    check-cast v11, LX/CfW;

    const/16 v12, 0x314c

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v13

    check-cast v13, LX/0Uh;

    invoke-static {v0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v0

    check-cast v0, LX/0yc;

    iput-object v3, v2, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->i:LX/0Xl;

    iput-object v4, v2, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->j:LX/03V;

    iput-object v5, v2, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->k:Landroid/os/Handler;

    iput-object v6, v2, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->l:LX/CIs;

    iput-object v7, v2, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->m:LX/0gh;

    iput-object v8, v2, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->n:LX/E1m;

    iput-object v9, v2, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->o:LX/2d1;

    iput-object v10, v2, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->p:LX/1vC;

    iput-object v11, v2, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->q:LX/CfW;

    iput-object v12, v2, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->r:LX/0Ot;

    iput-object v13, v2, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->s:LX/0Uh;

    iput-object v0, v2, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->t:LX/0yc;

    .line 2268265
    if-eqz p1, :cond_6

    .line 2268266
    const-string v4, "place_id"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2268267
    const-string v4, "place_id"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->z:Ljava/lang/Long;

    .line 2268268
    :cond_0
    const-string v4, "ranking_data"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2268269
    const-string v4, "ranking_data"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->A:Ljava/lang/String;

    .line 2268270
    :cond_1
    const-string v4, "semantic"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2268271
    const-string v4, "semantic"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->B:Ljava/lang/String;

    .line 2268272
    :cond_2
    const-string v4, "reaction_session_id"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2268273
    const-string v4, "reaction_session_id"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->C:Ljava/lang/String;

    .line 2268274
    :cond_3
    const-string v4, "search_results_source"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2268275
    const-string v4, "search_results_source"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/8ci;->a(Ljava/lang/String;)LX/8ci;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->D:LX/8ci;

    .line 2268276
    :cond_4
    const-string v4, "reaction_surface"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2268277
    const-string v4, "reaction_surface"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->E:Ljava/lang/String;

    .line 2268278
    :cond_5
    const-string v4, "extra_logging_data"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    instance-of v4, v4, Landroid/os/Bundle;

    if-eqz v4, :cond_6

    .line 2268279
    const-string v4, "extra_logging_data"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/os/Bundle;

    iput-object v4, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->x:Landroid/os/Bundle;

    .line 2268280
    :cond_6
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->C:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->E:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 2268281
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->p:LX/1vC;

    const v1, 0x1e000a

    iget-object v2, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->C:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1vC;->a(ILjava/lang/String;)V

    .line 2268282
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->p:LX/1vC;

    const v1, 0x1e000c

    iget-object v2, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->C:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/1vC;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2268283
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->Q()V

    .line 2268284
    :cond_7
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(Landroid/os/Bundle;)V

    .line 2268285
    new-instance v0, LX/FfO;

    invoke-direct {v0, p0}, LX/FfO;-><init>(Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;)V

    .line 2268286
    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->i:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->v:LX/0Yb;

    .line 2268287
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->v:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2268288
    new-instance v0, LX/FfP;

    invoke-direct {v0, p0}, LX/FfP;-><init>(Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;)V

    .line 2268289
    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->i:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->w:LX/0Yb;

    .line 2268290
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->w:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2268291
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->D:LX/8ci;

    invoke-direct {p0, v0}, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->a(LX/8ci;)V

    .line 2268292
    invoke-direct {p0}, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->T()V

    .line 2268293
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->t:LX/0yc;

    invoke-virtual {v0, p0}, LX/0yc;->a(LX/0yL;)V

    .line 2268294
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2268146
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268147
    iget-object v1, v0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->f:Ljava/lang/String;

    move-object v0, v1

    .line 2268148
    invoke-static {v0}, LX/CvY;->b(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 2268149
    const-string v1, "reaction_session_id"

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->n()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2268150
    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->B:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2268151
    const-string v1, "semantic"

    iget-object v2, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->B:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2268152
    :cond_0
    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->z:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 2268153
    const-string v1, "place_id"

    iget-object v2, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->z:Ljava/lang/Long;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2268154
    :cond_1
    return-object v0
.end method

.method public final b(Z)Z
    .locals 1

    .prologue
    .line 2268156
    const/4 v0, 0x0

    return v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2268157
    return-void
.end method

.method public final d()Landroid/support/v4/app/Fragment;
    .locals 0

    .prologue
    .line 2268158
    return-object p0
.end method

.method public final e_(Z)V
    .locals 1

    .prologue
    .line 2268159
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    move-object v0, v0

    .line 2268160
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2268161
    return-void
.end method

.method public final ig_()V
    .locals 0

    .prologue
    .line 2268162
    return-void
.end method

.method public final kL_()V
    .locals 4

    .prologue
    .line 2268163
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v0, v0

    .line 2268164
    iget-boolean v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->K:Z

    if-nez v1, :cond_0

    if-nez v0, :cond_1

    .line 2268165
    :cond_0
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->kL_()V

    .line 2268166
    :goto_0
    return-void

    .line 2268167
    :cond_1
    invoke-static {p0}, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->R(Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;)V

    .line 2268168
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->K:Z

    .line 2268169
    iget-object v1, v0, LX/2jY;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2268170
    iget-object v2, v0, LX/2jY;->b:Ljava/lang/String;

    move-object v0, v2

    .line 2268171
    iget-object v2, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->p:LX/1vC;

    const v3, 0x1e0009

    invoke-virtual {v2, v3, v1, v0}, LX/1vC;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2268172
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->kL_()V

    .line 2268173
    iget-object v2, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->p:LX/1vC;

    const v3, 0x1e000b

    invoke-virtual {v2, v3, v1, v0}, LX/1vC;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2268174
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->k:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment$2;

    invoke-direct {v2, p0, v1}, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment$2;-><init>(Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;Ljava/lang/String;)V

    const v1, 0x7a73b480

    invoke-static {v0, v2, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    .line 2268175
    sget-object v0, LX/Cfc;->WRITE_REVIEW_TAP:LX/Cfc;

    invoke-virtual {v0}, LX/Cfc;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2268176
    const-string v0, "reaction_unit_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2268177
    if-nez v4, :cond_1

    .line 2268178
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->j:LX/03V;

    sget-object v1, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->a:Ljava/lang/String;

    const-string v2, "WRITE_REVIEW_TAP should have value for EXTRA_REACTION_UNIT_ID in the return intent"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268179
    :cond_0
    :goto_0
    return-void

    .line 2268180
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cga;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->z:Ljava/lang/Long;

    iget-object v7, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->B:Ljava/lang/String;

    move v1, p2

    move-object v2, p3

    move-object v5, p0

    invoke-virtual/range {v0 .. v7}, LX/Cga;->a(ILandroid/content/Intent;Ljava/lang/String;Ljava/lang/String;LX/0o8;Ljava/lang/Long;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 19

    .prologue
    const/4 v2, 0x2

    const/16 v3, 0x2a

    const v4, -0x5d75a2ab

    invoke-static {v2, v3, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v18

    .line 2268181
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->I()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->y:J

    sub-long/2addr v2, v4

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->G:J

    sub-long v12, v2, v4

    .line 2268182
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m()LX/2ja;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2268183
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m()LX/2ja;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->J()J

    move-result-wide v4

    invoke-virtual {v2, v12, v13, v4, v5}, LX/2ja;->a(JJ)V

    .line 2268184
    invoke-static/range {p0 .. p0}, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->r(Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2268185
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->l:LX/CIs;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->E()LX/2jY;

    move-result-object v5

    invoke-virtual {v5}, LX/2jY;->j()J

    move-result-wide v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->z:Ljava/lang/Long;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->B:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->D:LX/8ci;

    if-nez v9, :cond_1

    const/4 v9, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v10}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->w()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v11}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->r()Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-result-object v11

    iget-object v11, v11, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->J()J

    move-result-wide v14

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m()LX/2ja;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, LX/2ja;->d()LX/0lF;

    move-result-object v16

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m()LX/2ja;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, LX/2ja;->b()LX/0Px;

    move-result-object v17

    invoke-virtual/range {v2 .. v17}, LX/CIs;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLX/0lF;Ljava/lang/Iterable;)V

    .line 2268186
    :cond_0
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->S()V

    .line 2268187
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->t:LX/0yc;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, LX/0yc;->b(LX/0yL;)V

    .line 2268188
    invoke-super/range {p0 .. p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->onDestroy()V

    .line 2268189
    const v2, 0x3731ab6f

    move/from16 v0, v18

    invoke-static {v2, v0}, LX/02F;->f(II)V

    return-void

    .line 2268190
    :cond_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->D:LX/8ci;

    invoke-virtual {v9}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    .line 2268191
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->l:LX/CIs;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->z:Ljava/lang/Long;

    if-nez v5, :cond_3

    const/4 v5, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v6}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->B:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->D:LX/8ci;

    if-nez v8, :cond_4

    const/4 v8, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v9}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->w()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v10}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->r()Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-result-object v10

    iget-object v10, v10, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v11}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->r()Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-result-object v11

    iget-object v11, v11, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v12}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->jA_()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {v2 .. v12}, LX/CIs;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->z:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->D:LX/8ci;

    invoke-virtual {v8}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_3
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2268192
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2268193
    if-eqz p1, :cond_6

    .line 2268194
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->z:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2268195
    const-string v0, "place_id"

    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->z:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2268196
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->A:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2268197
    const-string v0, "ranking_data"

    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->A:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268198
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->B:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2268199
    const-string v0, "semantic"

    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268200
    :cond_2
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->C:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2268201
    const-string v0, "reaction_session_id"

    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->C:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268202
    :cond_3
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->D:LX/8ci;

    if-eqz v0, :cond_4

    .line 2268203
    const-string v0, "search_results_source"

    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->D:LX/8ci;

    invoke-virtual {v1}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268204
    :cond_4
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->E:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 2268205
    const-string v0, "reaction_surface"

    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->E:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268206
    :cond_5
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->x:Landroid/os/Bundle;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->x:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2268207
    const-string v0, "extra_logging_data"

    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->x:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2268208
    :cond_6
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2268209
    invoke-super {p0, p1, p2}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2268210
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    move-object v0, v0

    .line 2268211
    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268212
    iput-object v1, v0, LX/E8m;->e:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268213
    invoke-virtual {v0}, LX/E8m;->e()I

    move-result v0

    if-lez v0, :cond_0

    .line 2268214
    const/4 v0, 0x0

    .line 2268215
    iput-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->t:Z

    .line 2268216
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->H:Z

    .line 2268217
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v0, v0

    .line 2268218
    invoke-static {p0}, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->r(Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;)Z

    move-result v1

    if-nez v1, :cond_0

    if-nez v0, :cond_1

    .line 2268219
    :cond_0
    :goto_0
    return-void

    .line 2268220
    :cond_1
    invoke-static {p0}, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->R(Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;)V

    .line 2268221
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->I:Z

    .line 2268222
    iget-object v1, v0, LX/2jY;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2268223
    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->k:Landroid/os/Handler;

    new-instance p1, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment$6;

    invoke-direct {p1, p0, v0}, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment$6;-><init>(Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;Ljava/lang/String;)V

    const v0, -0x19ba9ac6

    invoke-static {v1, p1, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final setUserVisibleHint(Z)V
    .locals 0

    .prologue
    .line 2268224
    if-eqz p1, :cond_0

    .line 2268225
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->setUserVisibleHint(Z)V

    .line 2268226
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->t()J

    .line 2268227
    :goto_0
    return-void

    .line 2268228
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->u()J

    .line 2268229
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->setUserVisibleHint(Z)V

    goto :goto_0
.end method

.method public final t()J
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 2268230
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v0

    .line 2268231
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->H:Z

    if-eqz v0, :cond_1

    .line 2268232
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->t()J

    move-result-wide v0

    .line 2268233
    iget-wide v4, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->y:J

    cmp-long v2, v4, v2

    if-nez v2, :cond_0

    .line 2268234
    iput-wide v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->y:J

    .line 2268235
    :cond_0
    iget-object v2, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->k:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment$3;

    invoke-direct {v3, p0}, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment$3;-><init>(Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;)V

    const v4, -0x7a34c5da

    invoke-static {v2, v3, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2268236
    :goto_0
    return-wide v0

    :cond_1
    move-wide v0, v2

    goto :goto_0
.end method

.method public final u()J
    .locals 3

    .prologue
    .line 2268237
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2268238
    invoke-static {p0}, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->r(Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->J:Z

    if-nez v0, :cond_0

    .line 2268239
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->n()Ljava/lang/String;

    move-result-object v0

    .line 2268240
    const-string v1, "NO_SESSION_ID"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2268241
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->J:Z

    .line 2268242
    :cond_0
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->u()J

    move-result-wide v0

    .line 2268243
    :goto_1
    return-wide v0

    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_1

    .line 2268244
    :cond_2
    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->p:LX/1vC;

    const v2, 0x1e000c

    invoke-virtual {v1, v2, v0}, LX/1vC;->c(ILjava/lang/String;)V

    .line 2268245
    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->p:LX/1vC;

    const v2, 0x1e0009

    invoke-virtual {v1, v2, v0}, LX/1vC;->c(ILjava/lang/String;)V

    .line 2268246
    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->p:LX/1vC;

    const v2, 0x1e000b

    invoke-virtual {v1, v2, v0}, LX/1vC;->c(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public final v()LX/2jY;
    .locals 5

    .prologue
    .line 2268247
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->z:Ljava/lang/Long;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->B:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->E:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 2268248
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->j:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "missing session data - surface: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->E:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " pageId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->z:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " semantic: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->B:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268249
    :cond_2
    const/4 v0, 0x1

    .line 2268250
    iput-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->t:Z

    .line 2268251
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->B:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->B:Ljava/lang/String;

    .line 2268252
    :goto_0
    iget-object v1, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->q:LX/CfW;

    iget-object v2, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->C:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->E:Ljava/lang/String;

    new-instance v4, Lcom/facebook/reaction/ReactionQueryParams;

    invoke-direct {v4}, Lcom/facebook/reaction/ReactionQueryParams;-><init>()V

    .line 2268253
    iput-object v0, v4, Lcom/facebook/reaction/ReactionQueryParams;->e:Ljava/lang/String;

    .line 2268254
    move-object v0, v4

    .line 2268255
    iget-object v4, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->z:Ljava/lang/Long;

    .line 2268256
    iput-object v4, v0, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    .line 2268257
    move-object v0, v0

    .line 2268258
    iget-object v4, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->z:Ljava/lang/Long;

    .line 2268259
    iput-object v4, v0, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    .line 2268260
    move-object v0, v0

    .line 2268261
    invoke-virtual {v1, v2, v3, v0}, LX/CfW;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    move-result-object v0

    return-object v0

    .line 2268262
    :cond_3
    iget-object v0, p0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->A:Ljava/lang/String;

    goto :goto_0
.end method
