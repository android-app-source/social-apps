.class public final Lcom/facebook/search/results/loader/modules/SearchResultsFeedModulesDataLoader$NetworkRequestTimer$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/FgF;

.field public final synthetic b:LX/FgD;


# direct methods
.method public constructor <init>(LX/FgD;LX/FgF;)V
    .locals 0

    .prologue
    .line 2269714
    iput-object p1, p0, Lcom/facebook/search/results/loader/modules/SearchResultsFeedModulesDataLoader$NetworkRequestTimer$1;->b:LX/FgD;

    iput-object p2, p0, Lcom/facebook/search/results/loader/modules/SearchResultsFeedModulesDataLoader$NetworkRequestTimer$1;->a:LX/FgF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 2269715
    iget-object v0, p0, Lcom/facebook/search/results/loader/modules/SearchResultsFeedModulesDataLoader$NetworkRequestTimer$1;->b:LX/FgD;

    iget-object v0, v0, LX/FgD;->a:LX/FgF;

    invoke-virtual {v0}, LX/FgF;->b()V

    .line 2269716
    iget-object v0, p0, Lcom/facebook/search/results/loader/modules/SearchResultsFeedModulesDataLoader$NetworkRequestTimer$1;->b:LX/FgD;

    const/4 v1, 0x0

    .line 2269717
    iput-boolean v1, v0, LX/FgD;->d:Z

    .line 2269718
    iget-object v0, p0, Lcom/facebook/search/results/loader/modules/SearchResultsFeedModulesDataLoader$NetworkRequestTimer$1;->b:LX/FgD;

    iget-object v0, v0, LX/FgD;->a:LX/FgF;

    iget-object v0, v0, LX/FgF;->u:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    if-eqz v0, :cond_0

    .line 2269719
    iget-object v0, p0, Lcom/facebook/search/results/loader/modules/SearchResultsFeedModulesDataLoader$NetworkRequestTimer$1;->b:LX/FgD;

    iget-object v0, v0, LX/FgD;->a:LX/FgF;

    iget-object v0, v0, LX/FgF;->u:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    const/16 v1, 0x19

    const/4 v3, 0x0

    .line 2269720
    iget-object v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->W:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 2269721
    iput-boolean v3, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ap:Z

    .line 2269722
    iput-boolean v3, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aq:Z

    .line 2269723
    iget-object v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->m:LX/Cve;

    .line 2269724
    iget-object v3, v0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v3, v3

    .line 2269725
    invoke-virtual {v2, v3, v1}, LX/Cve;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;I)V

    .line 2269726
    sget-object v2, LX/EQG;->REQUEST_TIMED_OUT:LX/EQG;

    invoke-static {v0, v2}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->a(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;LX/EQG;)V

    .line 2269727
    iget-object v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->q:LX/CvY;

    .line 2269728
    iget-object v3, v0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v3, v3

    .line 2269729
    iget v4, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ar:I

    iget v5, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->as:I

    new-instance v6, Ljava/lang/StringBuilder;

    const-string p0, "Timeout: "

    invoke-direct {v6, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;IILjava/lang/String;)V

    .line 2269730
    :cond_0
    return-void
.end method
