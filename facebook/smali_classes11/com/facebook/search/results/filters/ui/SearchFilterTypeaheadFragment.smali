.class public Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/Fc7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Fcy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Fc6;

.field private e:LX/CwL;

.field public f:Lcom/facebook/widget/listview/BetterListView;

.field public g:Lcom/facebook/ui/search/SearchEditText;

.field public h:Lcom/facebook/fbui/glyph/GlyphView;

.field public i:LX/Fcx;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2263534
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2263535
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;

    const-class v1, LX/Fc7;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/Fc7;

    const-class v2, LX/Fcy;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/Fcy;

    invoke-static {p0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object p0

    check-cast p0, LX/0zG;

    iput-object v1, p1, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->a:LX/Fc7;

    iput-object v2, p1, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->b:LX/Fcy;

    iput-object p0, p1, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->c:LX/0zG;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;Lcom/facebook/search/results/protocol/filters/FilterValue;)V
    .locals 4

    .prologue
    .line 2263521
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2263522
    const-string v1, "filter_value_group_name"

    iget-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->e:LX/CwL;

    .line 2263523
    iget-object v3, v2, LX/CwL;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2263524
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2263525
    const-string v1, "filter_value_selected_text"

    .line 2263526
    iget-object v2, p1, Lcom/facebook/search/results/protocol/filters/FilterValue;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2263527
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2263528
    const-string v1, "filter_value_selected_value"

    .line 2263529
    iget-object v2, p1, Lcom/facebook/search/results/protocol/filters/FilterValue;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2263530
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2263531
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2263532
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2263533
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2263514
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->c:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2263515
    if-nez v0, :cond_0

    .line 2263516
    :goto_0
    return-void

    .line 2263517
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSearchButtonVisible(Z)V

    .line 2263518
    iget-object v1, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->e:LX/CwL;

    .line 2263519
    iget-object p0, v1, LX/CwL;->c:Ljava/lang/String;

    move-object v1, p0

    .line 2263520
    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2263494
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2263495
    const-class v0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;

    invoke-static {v0, p0}, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2263496
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->a:LX/Fc7;

    new-instance v1, LX/FdO;

    invoke-direct {v1, p0}, LX/FdO;-><init>(Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;)V

    invoke-virtual {v0, v1}, LX/Fc7;->a(LX/Fc4;)LX/Fc6;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->d:LX/Fc6;

    .line 2263497
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2263498
    if-eqz v0, :cond_0

    .line 2263499
    const-string v1, "search_filter_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2263500
    const-string v2, "search_filter_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2263501
    const-string v3, "search_filter_text"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2263502
    const-string v4, "search_filter_free_text_filter_string"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2263503
    new-instance v4, LX/CwK;

    invoke-direct {v4}, LX/CwK;-><init>()V

    .line 2263504
    iput-object v1, v4, LX/CwK;->a:Ljava/lang/String;

    .line 2263505
    move-object v1, v4

    .line 2263506
    iput-object v2, v1, LX/CwK;->b:Ljava/lang/String;

    .line 2263507
    move-object v1, v1

    .line 2263508
    iput-object v3, v1, LX/CwK;->c:Ljava/lang/String;

    .line 2263509
    move-object v1, v1

    .line 2263510
    iput-object v0, v1, LX/CwK;->d:Ljava/lang/String;

    .line 2263511
    move-object v0, v1

    .line 2263512
    invoke-virtual {v0}, LX/CwK;->a()LX/CwL;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->e:LX/CwL;

    .line 2263513
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/16 v0, 0x2a

    const v1, 0x65205ad0

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2263467
    const v0, 0x7f03127f

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2263468
    const v0, 0x7f0d2b6b

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    .line 2263469
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    new-instance v3, LX/FdK;

    invoke-direct {v3, p0}, LX/FdK;-><init>(Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2263470
    const v0, 0x7f0d1478

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->h:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2263471
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->h:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v3, LX/FdL;

    invoke-direct {v3, p0}, LX/FdL;-><init>(Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2263472
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    new-instance v3, LX/FdM;

    invoke-direct {v3, p0}, LX/FdM;-><init>(Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;)V

    .line 2263473
    iput-object v3, v0, Lcom/facebook/widget/listview/BetterListView;->y:LX/2i6;

    .line 2263474
    const v0, 0x7f0d1477

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/search/SearchEditText;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->g:Lcom/facebook/ui/search/SearchEditText;

    .line 2263475
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f08207e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/ui/search/SearchEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2263476
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->g:Lcom/facebook/ui/search/SearchEditText;

    new-instance v3, LX/FdP;

    invoke-direct {v3, p0}, LX/FdP;-><init>(Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/ui/search/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2263477
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->g:Lcom/facebook/ui/search/SearchEditText;

    new-instance v3, LX/FdN;

    invoke-direct {v3, p0}, LX/FdN;-><init>(Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;)V

    .line 2263478
    iput-object v3, v0, Lcom/facebook/ui/search/SearchEditText;->f:LX/7HK;

    .line 2263479
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->b:LX/Fcy;

    iget-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->e:LX/CwL;

    .line 2263480
    iget-object v4, v3, LX/CwL;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2263481
    iget-object v4, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->e:LX/CwL;

    .line 2263482
    iget-object v5, v4, LX/CwL;->c:Ljava/lang/String;

    move-object v4, v5

    .line 2263483
    iget-object v5, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->e:LX/CwL;

    .line 2263484
    iget-object p1, v5, LX/CwL;->d:Ljava/lang/String;

    move-object v5, p1

    .line 2263485
    invoke-virtual {v0, v3, v4, v5}, LX/Fcy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Fcx;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->i:LX/Fcx;

    .line 2263486
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->i:LX/Fcx;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2263487
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->e:LX/CwL;

    .line 2263488
    iget-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->d:LX/Fc6;

    .line 2263489
    iput-object v0, v3, LX/Fc6;->h:LX/CwL;

    .line 2263490
    iget-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v3}, Lcom/facebook/widget/text/BetterEditTextView;->a()V

    .line 2263491
    iget-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/widget/listview/BetterListView;->smoothScrollToPosition(I)V

    .line 2263492
    iget-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->d:LX/Fc6;

    iget-object v4, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v4}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/Fc6;->a(Ljava/lang/String;)V

    .line 2263493
    const/16 v0, 0x2b

    const v3, -0x4a15d86c

    invoke-static {v6, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x2e7f2a1d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2263462
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2263463
    iput-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    .line 2263464
    iput-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->g:Lcom/facebook/ui/search/SearchEditText;

    .line 2263465
    iput-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->i:LX/Fcx;

    .line 2263466
    const/16 v1, 0x2b

    const v2, -0x45f4ff40

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x69d84781

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2263458
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2263459
    invoke-direct {p0}, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->b()V

    .line 2263460
    iget-object v1, p0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->b()Z

    .line 2263461
    const/16 v1, 0x2b

    const v2, 0x3dd21cff

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
