.class public Lcom/facebook/search/results/filters/ui/FilterPopoverListView;
.super Lcom/facebook/widget/listview/BetterListView;
.source ""


# instance fields
.field public a:I

.field public b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2262952
    invoke-direct {p0, p1}, Lcom/facebook/widget/listview/BetterListView;-><init>(Landroid/content/Context;)V

    .line 2262953
    const/16 v0, 0x12c

    iput v0, p0, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;->a:I

    .line 2262954
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;->b:Z

    .line 2262955
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2262956
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/listview/BetterListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2262957
    const/16 v0, 0x12c

    iput v0, p0, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;->a:I

    .line 2262958
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;->b:Z

    .line 2262959
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2262933
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/listview/BetterListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2262934
    const/16 v0, 0x12c

    iput v0, p0, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;->a:I

    .line 2262935
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;->b:Z

    .line 2262936
    return-void
.end method


# virtual methods
.method public getShowFullWidth()Z
    .locals 1

    .prologue
    .line 2262951
    iget-boolean v0, p0, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;->b:Z

    return v0
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    .line 2262941
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 2262942
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 2262943
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 2262944
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 2262945
    invoke-virtual {p0}, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2262946
    iget v5, p0, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;->a:I

    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 2262947
    iget-boolean v5, p0, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;->b:Z

    if-eqz v5, :cond_0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    :cond_0
    invoke-static {v4, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-super {p0, p1, v2}, Lcom/facebook/widget/listview/BetterListView;->onMeasure(II)V

    .line 2262948
    iget-boolean v2, p0, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;->b:Z

    if-eqz v2, :cond_1

    :goto_0
    invoke-virtual {p0, v0, v4}, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;->setMeasuredDimension(II)V

    .line 2262949
    return-void

    :cond_1
    move v0, v1

    .line 2262950
    goto :goto_0
.end method

.method public setHeight(I)V
    .locals 0

    .prologue
    .line 2262939
    iput p1, p0, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;->a:I

    .line 2262940
    return-void
.end method

.method public setShowFullWidth(Z)V
    .locals 0

    .prologue
    .line 2262937
    iput-boolean p1, p0, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;->b:Z

    .line 2262938
    return-void
.end method
