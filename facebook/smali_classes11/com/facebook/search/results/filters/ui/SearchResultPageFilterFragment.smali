.class public Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/FdS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8cd;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/5uu;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/FdZ;

.field public r:Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;

.field public s:LX/FdR;

.field private t:Landroid/widget/ExpandableListView;

.field private u:Lcom/facebook/widget/CustomLinearLayout;

.field private v:Lcom/facebook/widget/text/BetterButton;

.field private w:Lcom/facebook/widget/text/BetterButton;

.field private x:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2263715
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2263716
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2263717
    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->n:LX/0Ot;

    .line 2263718
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2263719
    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->o:LX/0Ot;

    return-void
.end method

.method public static a(LX/0Px;LX/FdZ;)Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/5uu;",
            ">;",
            "LX/FdZ;",
            ")",
            "Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;"
        }
    .end annotation

    .prologue
    .line 2263720
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->a(LX/0Px;LX/FdZ;Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;)Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0Px;LX/FdZ;Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;)Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/5uu;",
            ">;",
            "LX/FdZ;",
            "Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment$OnFilterValuesResetOrCanceledListener;",
            ")",
            "Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;"
        }
    .end annotation

    .prologue
    .line 2263721
    new-instance v0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    invoke-direct {v0}, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;-><init>()V

    .line 2263722
    const/4 v1, 0x2

    const v2, 0x7f0e0895

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 2263723
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2263724
    const-string v2, "main_filter_list"

    invoke-static {v1, v2, p0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 2263725
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2263726
    iput-object p1, v0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->q:LX/FdZ;

    .line 2263727
    iput-object p2, v0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->r:Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;

    .line 2263728
    return-object v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    const-class v1, LX/FdS;

    invoke-interface {v2, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/FdS;

    const/16 v3, 0x32cf

    invoke-static {v2, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 p0, 0x455

    invoke-static {v2, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v1, p1, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->m:LX/FdS;

    iput-object v3, p1, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->n:LX/0Ot;

    iput-object v2, p1, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->o:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 2263729
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2263730
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-eqz p1, :cond_1

    .line 2263731
    :cond_0
    :goto_0
    return-void

    .line 2263732
    :cond_1
    const-string v0, "filter_value_group_name"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2263733
    const-string v1, "filter_value_selected_text"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2263734
    const-string v2, "filter_value_selected_value"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2263735
    iget-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->s:LX/FdR;

    new-instance v4, LX/CyH;

    invoke-direct {v4, v0, v1, v2}, LX/CyH;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2263736
    iget-object v0, v3, LX/FdR;->e:LX/FdQ;

    invoke-virtual {v0, v4}, LX/FdQ;->a(LX/CyH;)V

    .line 2263737
    goto :goto_0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 2263738
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 2263739
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->r:Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;

    if-eqz v0, :cond_0

    .line 2263740
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->r:Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;

    .line 2263741
    iget-object p0, v0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->x:LX/FPa;

    .line 2263742
    iget-object p1, p0, LX/FPa;->a:LX/0Zb;

    const-string v0, "places_advanced_filters_canceled"

    invoke-static {p0, v0}, LX/FPa;->a(LX/FPa;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {p1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2263743
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x185987d1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2263744
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2263745
    const-class v1, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    invoke-static {v1, p0}, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2263746
    iget-object v1, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->m:LX/FdS;

    new-instance v2, LX/FdU;

    invoke-direct {v2, p0}, LX/FdU;-><init>(Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;)V

    .line 2263747
    new-instance p1, LX/FdR;

    invoke-static {v1}, LX/FcV;->b(LX/0QB;)LX/FcV;

    move-result-object v4

    check-cast v4, LX/FcV;

    invoke-direct {p1, v2, v4}, LX/FdR;-><init>(LX/FdU;LX/FcV;)V

    .line 2263748
    move-object v1, p1

    .line 2263749
    iput-object v1, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->s:LX/FdR;

    .line 2263750
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2263751
    if-eqz v1, :cond_0

    .line 2263752
    const-string v2, "main_filter_list"

    invoke-static {v1, v2}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->p:LX/0Px;

    .line 2263753
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x1c7d5d44

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/16 v0, 0x2a

    const v1, -0x500a39fd

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2263754
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v2, v0

    .line 2263755
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/Window;->requestFeature(I)Z

    .line 2263756
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e0241

    invoke-direct {v0, v3, v4}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f031294

    invoke-virtual {v0, v3, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 2263757
    const v0, 0x7f0d2b7c

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ExpandableListView;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->t:Landroid/widget/ExpandableListView;

    .line 2263758
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->s:LX/FdR;

    iget-object v4, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->p:LX/0Px;

    invoke-virtual {v0, v4}, LX/FdR;->a(LX/0Px;)V

    .line 2263759
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->t:Landroid/widget/ExpandableListView;

    iget-object v4, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->s:LX/FdR;

    invoke-virtual {v0, v4}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 2263760
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->t:Landroid/widget/ExpandableListView;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 2263761
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->t:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v5}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 2263762
    const v0, 0x7f0d2b7d

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->u:Lcom/facebook/widget/CustomLinearLayout;

    .line 2263763
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->u:Lcom/facebook/widget/CustomLinearLayout;

    const v4, 0x7f0d09a9

    invoke-static {v0, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->v:Lcom/facebook/widget/text/BetterButton;

    .line 2263764
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->u:Lcom/facebook/widget/CustomLinearLayout;

    const v4, 0x7f0d0f14

    invoke-static {v0, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->w:Lcom/facebook/widget/text/BetterButton;

    .line 2263765
    const v0, 0x7f0d062f

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->x:Lcom/facebook/widget/text/BetterTextView;

    .line 2263766
    new-instance v0, LX/FdV;

    invoke-direct {v0, p0, v2}, LX/FdV;-><init>(Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;Landroid/app/Dialog;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2263767
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->v:Lcom/facebook/widget/text/BetterButton;

    new-instance v4, LX/FdW;

    invoke-direct {v4, p0, v2}, LX/FdW;-><init>(Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;Landroid/app/Dialog;)V

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2263768
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->w:Lcom/facebook/widget/text/BetterButton;

    new-instance v2, LX/FdX;

    invoke-direct {v2, p0}, LX/FdX;-><init>(Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2263769
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->x:Lcom/facebook/widget/text/BetterTextView;

    new-instance v2, LX/FdY;

    invoke-direct {v2, p0}, LX/FdY;-><init>(Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2263770
    const/16 v0, 0x2b

    const v2, -0x68af76d0

    invoke-static {v6, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v3
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, 0xf12c4f8

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2263771
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroyView()V

    .line 2263772
    iput-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->v:Lcom/facebook/widget/text/BetterButton;

    .line 2263773
    iput-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->w:Lcom/facebook/widget/text/BetterButton;

    .line 2263774
    iput-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->x:Lcom/facebook/widget/text/BetterTextView;

    .line 2263775
    iget-object v1, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->t:Landroid/widget/ExpandableListView;

    iget-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->u:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/ExpandableListView;->removeFooterView(Landroid/view/View;)Z

    .line 2263776
    iput-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->u:Lcom/facebook/widget/CustomLinearLayout;

    .line 2263777
    const/16 v1, 0x2b

    const v2, 0x2da9cce6

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x39929146

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2263778
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onResume()V

    .line 2263779
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 2263780
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 2263781
    const/16 v2, 0x50

    invoke-virtual {v1, v2}, Landroid/view/Window;->setGravity(I)V

    .line 2263782
    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setLayout(II)V

    .line 2263783
    const/16 v1, 0x2b

    const v2, 0x50bbcbd0

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
