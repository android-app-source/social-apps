.class public Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/Fc7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/Fdp;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public o:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public p:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;

.field public r:LX/Fc6;

.field private s:LX/CwL;

.field public t:Lcom/facebook/widget/listview/BetterListView;

.field public u:Lcom/facebook/ui/search/SearchEditText;

.field private v:Lcom/facebook/widget/text/BetterTextView;

.field public w:LX/Fdo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2264290
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2264291
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;

    const-class v1, LX/Fc7;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/Fc7;

    const-class v2, LX/Fdp;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/Fdp;

    const-class v3, Landroid/content/Context;

    invoke-interface {p0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p0

    check-cast p0, LX/0ad;

    iput-object v1, p1, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->m:LX/Fc7;

    iput-object v2, p1, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->n:LX/Fdp;

    iput-object v3, p1, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->o:Landroid/content/Context;

    iput-object p0, p1, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->p:LX/0ad;

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x23330de3

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2264292
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2264293
    const-class v0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;

    invoke-static {v0, p0}, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2264294
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->m:LX/Fc7;

    new-instance v2, LX/Fdt;

    invoke-direct {v2, p0}, LX/Fdt;-><init>(Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;)V

    invoke-virtual {v0, v2}, LX/Fc7;->a(LX/Fc4;)LX/Fc6;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->r:LX/Fc6;

    .line 2264295
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2264296
    if-eqz v0, :cond_0

    .line 2264297
    const-string v2, "main_filter"

    invoke-static {v0, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5uu;

    .line 2264298
    new-instance v2, LX/CwK;

    invoke-direct {v2}, LX/CwK;-><init>()V

    invoke-interface {v0}, LX/5uu;->bl_()Ljava/lang/String;

    move-result-object v3

    .line 2264299
    iput-object v3, v2, LX/CwK;->a:Ljava/lang/String;

    .line 2264300
    move-object v2, v2

    .line 2264301
    invoke-interface {v0}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v3

    .line 2264302
    iput-object v3, v2, LX/CwK;->b:Ljava/lang/String;

    .line 2264303
    move-object v2, v2

    .line 2264304
    invoke-interface {v0}, LX/5uu;->l()Ljava/lang/String;

    move-result-object v3

    .line 2264305
    iput-object v3, v2, LX/CwK;->c:Ljava/lang/String;

    .line 2264306
    move-object v2, v2

    .line 2264307
    invoke-interface {v0}, LX/5uu;->c()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2264308
    iput-object v0, v2, LX/CwK;->d:Ljava/lang/String;

    .line 2264309
    move-object v0, v2

    .line 2264310
    invoke-virtual {v0}, LX/CwK;->a()LX/CwL;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->s:LX/CwL;

    .line 2264311
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x777be61a

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x1e7d3c3b

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2264312
    const v0, 0x7f0312c3

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2264313
    const v0, 0x7f0d2b6b

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->t:Lcom/facebook/widget/listview/BetterListView;

    .line 2264314
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->t:Lcom/facebook/widget/listview/BetterListView;

    new-instance v3, LX/Fdq;

    invoke-direct {v3, p0}, LX/Fdq;-><init>(Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;)V

    .line 2264315
    iput-object v3, v0, Lcom/facebook/widget/listview/BetterListView;->y:LX/2i6;

    .line 2264316
    const v0, 0x7f0d2bae

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/search/SearchEditText;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->u:Lcom/facebook/ui/search/SearchEditText;

    .line 2264317
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->u:Lcom/facebook/ui/search/SearchEditText;

    iget-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->o:Landroid/content/Context;

    const v4, 0x7f08207e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/ui/search/SearchEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2264318
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->u:Lcom/facebook/ui/search/SearchEditText;

    new-instance v3, LX/Fdu;

    invoke-direct {v3, p0}, LX/Fdu;-><init>(Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/ui/search/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2264319
    const v0, 0x7f0d2baf

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->v:Lcom/facebook/widget/text/BetterTextView;

    .line 2264320
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->p:LX/0ad;

    sget-short v3, LX/100;->I:S

    invoke-interface {v0, v3, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2264321
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->v:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2264322
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->v:Lcom/facebook/widget/text/BetterTextView;

    new-instance v3, LX/Fdr;

    invoke-direct {v3, p0}, LX/Fdr;-><init>(Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2264323
    :goto_0
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->n:LX/Fdp;

    iget-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->s:LX/CwL;

    .line 2264324
    iget-object v4, v3, LX/CwL;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2264325
    iget-object v4, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->s:LX/CwL;

    .line 2264326
    iget-object v5, v4, LX/CwL;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2264327
    new-instance p1, LX/Fdo;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-direct {p1, v3, v4, v5}, LX/Fdo;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 2264328
    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    .line 2264329
    iput-object v5, p1, LX/Fdo;->a:LX/0ad;

    .line 2264330
    move-object v0, p1

    .line 2264331
    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->w:LX/Fdo;

    .line 2264332
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->t:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->w:LX/Fdo;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2264333
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->s:LX/CwL;

    .line 2264334
    iget-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->r:LX/Fc6;

    .line 2264335
    iput-object v0, v3, LX/Fc6;->h:LX/CwL;

    .line 2264336
    iget-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->u:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v3}, Lcom/facebook/widget/text/BetterEditTextView;->a()V

    .line 2264337
    iget-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->t:Lcom/facebook/widget/listview/BetterListView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/widget/listview/BetterListView;->smoothScrollToPosition(I)V

    .line 2264338
    const v0, -0x7329e055

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-object v2

    .line 2264339
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->v:Lcom/facebook/widget/text/BetterTextView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2264340
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->t:Lcom/facebook/widget/listview/BetterListView;

    new-instance v3, LX/Fds;

    invoke-direct {v3, p0}, LX/Fds;-><init>(Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x85c151a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2264341
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroyView()V

    .line 2264342
    iput-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->v:Lcom/facebook/widget/text/BetterTextView;

    .line 2264343
    iput-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->t:Lcom/facebook/widget/listview/BetterListView;

    .line 2264344
    iput-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->u:Lcom/facebook/ui/search/SearchEditText;

    .line 2264345
    iput-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->w:LX/Fdo;

    .line 2264346
    const/16 v1, 0x2b

    const v2, 0x1990a8c2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xdab232a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2264347
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onStart()V

    .line 2264348
    iget-object v1, p0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->u:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->b()Z

    .line 2264349
    const/16 v1, 0x2b

    const v2, 0x3f3e4031

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
