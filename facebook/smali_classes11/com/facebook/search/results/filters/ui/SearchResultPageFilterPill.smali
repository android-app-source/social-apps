.class public Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/fbui/glyph/GlyphView;

.field public final b:Lcom/facebook/widget/text/BetterTextView;

.field public final c:Lcom/facebook/fbui/glyph/GlyphView;

.field private d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2263784
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2263785
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2263803
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2263804
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2263795
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2263796
    iput-object p1, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;->d:Landroid/content/Context;

    .line 2263797
    const v0, 0x7f031297

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2263798
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;->setOrientation(I)V

    .line 2263799
    const v0, 0x7f0d0343

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2263800
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2263801
    const v0, 0x7f0d088b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;->c:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2263802
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;LX/4FP;)V
    .locals 3

    .prologue
    .line 2263791
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/glyph/GlyphView;->setTag(Ljava/lang/Object;)V

    .line 2263792
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;->c:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0820a8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2263793
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2263794
    return-void
.end method

.method public setFilterIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 2263788
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2263789
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;->a:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2263790
    return-void
.end method

.method public setFilterTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2263786
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2263787
    return-void
.end method
