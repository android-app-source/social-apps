.class public Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Lcom/facebook/widget/text/BetterTextView;

.field private final c:Lcom/facebook/widget/text/BetterTextView;

.field private final d:Ljava/text/NumberFormat;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2263594
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2263595
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2263596
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2263597
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2263598
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2263599
    const-class v0, Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;

    invoke-static {v0, p0}, Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2263600
    const v0, 0x7f031299

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2263601
    const v0, 0x7f0d2b81

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2263602
    const v0, 0x7f0d2b82

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2263603
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;->a:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getCurrencyInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;->d:Ljava/text/NumberFormat;

    .line 2263604
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;->d:Ljava/text/NumberFormat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 2263605
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v0

    check-cast v0, LX/0W9;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;->a:LX/0W9;

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 4

    .prologue
    .line 2263606
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;->d:Ljava/text/NumberFormat;

    int-to-long v2, p1

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2263607
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;->d:Ljava/text/NumberFormat;

    int-to-long v2, p2

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2263608
    return-void
.end method
