.class public Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "LX/CwL;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/view/View$OnClickListener;

.field private final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:I

.field private f:Landroid/graphics/ColorFilter;

.field private g:Landroid/graphics/ColorFilter;

.field private h:I

.field private i:I

.field private j:I

.field private k:Z

.field public l:LX/Fd6;

.field private m:LX/Fd4;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 2264185
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2264186
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->a:Ljava/util/Map;

    .line 2264187
    new-instance v0, LX/Fdm;

    invoke-direct {v0, p0}, LX/Fdm;-><init>(Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;)V

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->b:Landroid/view/View$OnClickListener;

    .line 2264188
    const-string v0, "city"

    const v1, 0x7f0207f6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "employer"

    const v3, 0x7f02078c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "friends"

    const v5, 0x7f0208a6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "school"

    const v7, 0x7f0209ba

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static/range {v0 .. v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->c:LX/0P1;

    .line 2264189
    invoke-direct {p0}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->a()V

    .line 2264190
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2264178
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2264179
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->a:Ljava/util/Map;

    .line 2264180
    new-instance v0, LX/Fdm;

    invoke-direct {v0, p0}, LX/Fdm;-><init>(Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;)V

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->b:Landroid/view/View$OnClickListener;

    .line 2264181
    const-string v0, "city"

    const v1, 0x7f0207f6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "employer"

    const v3, 0x7f02078c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "friends"

    const v5, 0x7f0208a6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "school"

    const v7, 0x7f0209ba

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static/range {v0 .. v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->c:LX/0P1;

    .line 2264182
    invoke-direct {p0, p1, p2, v8, v8}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 2264183
    invoke-direct {p0}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->a()V

    .line 2264184
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    .line 2264171
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2264172
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->a:Ljava/util/Map;

    .line 2264173
    new-instance v0, LX/Fdm;

    invoke-direct {v0, p0}, LX/Fdm;-><init>(Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;)V

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->b:Landroid/view/View$OnClickListener;

    .line 2264174
    const-string v0, "city"

    const v1, 0x7f0207f6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "employer"

    const v3, 0x7f02078c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "friends"

    const v5, 0x7f0208a6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "school"

    const v7, 0x7f0209ba

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static/range {v0 .. v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->c:LX/0P1;

    .line 2264175
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 2264176
    invoke-direct {p0}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->a()V

    .line 2264177
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 2264157
    const-class v0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;

    invoke-static {v0, p0}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2264158
    invoke-virtual {p0}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2264159
    const v1, 0x7f0a06a2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->d:I

    .line 2264160
    const v1, 0x7f0a06a1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->e:I

    .line 2264161
    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    iget v2, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->d:I

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    iput-object v1, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->f:Landroid/graphics/ColorFilter;

    .line 2264162
    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    iget v2, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->e:I

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    iput-object v1, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->g:Landroid/graphics/ColorFilter;

    .line 2264163
    const v1, 0x7f0b14d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->h:I

    .line 2264164
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->l:LX/Fd6;

    iget v1, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->j:I

    .line 2264165
    iget-object v2, v0, LX/Fd6;->w:Lcom/facebook/search/results/filters/ui/FilterPopoverListView;

    .line 2264166
    iput v1, v2, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;->a:I

    .line 2264167
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->l:LX/Fd6;

    iget-boolean v1, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->k:Z

    .line 2264168
    iget-object v2, v0, LX/Fd6;->w:Lcom/facebook/search/results/filters/ui/FilterPopoverListView;

    .line 2264169
    iput-boolean v1, v2, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;->b:Z

    .line 2264170
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 3

    .prologue
    .line 2264150
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->SearchResultsPageFilter:[I

    invoke-virtual {v0, p2, v1, p3, p4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 2264151
    :try_start_0
    const/16 v0, 0x0

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->i:I

    .line 2264152
    const/16 v0, 0x1

    const/16 v2, 0x12c

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->j:I

    .line 2264153
    const/16 v0, 0x2

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2264154
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 2264155
    return-void

    .line 2264156
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 2264191
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 2264192
    invoke-virtual {p0}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2264193
    :goto_0
    return-void

    .line 2264194
    :cond_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;

    new-instance p1, LX/Fd6;

    const-class v1, Landroid/content/Context;

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const-class v2, LX/Fc7;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/Fc7;

    invoke-direct {p1, v1, v2}, LX/Fd6;-><init>(Landroid/content/Context;LX/Fc7;)V

    move-object v0, p1

    check-cast v0, LX/Fd6;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->l:LX/Fd6;

    return-void
.end method


# virtual methods
.method public setFilters(LX/0Px;)V
    .locals 12
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/CwL;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/4 v4, 0x0

    .line 2264078
    invoke-virtual {p0}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->removeAllViews()V

    .line 2264079
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2264080
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2264081
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->setVisibility(I)V

    .line 2264082
    :goto_0
    return-void

    .line 2264083
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 2264084
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->l:LX/Fd6;

    .line 2264085
    iget-object v1, v0, LX/Fd6;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2264086
    iget-object v1, v0, LX/Fd6;->x:LX/Fd7;

    const v2, -0x7cb4054f

    invoke-static {v1, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2264087
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->l:LX/Fd6;

    .line 2264088
    iget-object v1, v0, LX/Fd6;->l:LX/Fc6;

    .line 2264089
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 2264090
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 2264091
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v9

    const/4 v2, 0x0

    move v5, v2

    :goto_1
    if-ge v5, v9, :cond_3

    invoke-virtual {p1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CwL;

    .line 2264092
    iget-object v3, v1, LX/Fc6;->d:Ljava/util/Map;

    .line 2264093
    iget-object v10, v2, LX/CwL;->b:Ljava/lang/String;

    move-object v10, v10

    .line 2264094
    invoke-interface {v3, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, v1, LX/Fc6;->d:Ljava/util/Map;

    .line 2264095
    iget-object v10, v2, LX/CwL;->b:Ljava/lang/String;

    move-object v10, v10

    .line 2264096
    invoke-interface {v3, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2264097
    iget-object v10, v2, LX/CwL;->a:Ljava/lang/String;

    move-object v10, v10

    .line 2264098
    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2264099
    iget-object v3, v2, LX/CwL;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2264100
    iget-object v10, v1, LX/Fc6;->c:Ljava/util/Map;

    .line 2264101
    iget-object v0, v2, LX/CwL;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2264102
    invoke-interface {v10, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    invoke-interface {v7, v3, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2264103
    iget-object v3, v2, LX/CwL;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2264104
    iget-object v10, v1, LX/Fc6;->d:Ljava/util/Map;

    .line 2264105
    iget-object v0, v2, LX/CwL;->b:Ljava/lang/String;

    move-object v2, v0

    .line 2264106
    invoke-interface {v10, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v8, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2264107
    :cond_2
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_1

    .line 2264108
    :cond_3
    iget-object v2, v1, LX/Fc6;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 2264109
    iget-object v2, v1, LX/Fc6;->d:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 2264110
    iget-object v2, v1, LX/Fc6;->c:Ljava/util/Map;

    invoke-interface {v2, v7}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2264111
    iget-object v2, v1, LX/Fc6;->d:Ljava/util/Map;

    invoke-interface {v2, v8}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2264112
    invoke-virtual {p0}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    move v3, v4

    .line 2264113
    :goto_2
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v3, v0, :cond_9

    iget v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->i:I

    if-ge v3, v0, :cond_9

    .line 2264114
    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CwL;

    .line 2264115
    iget-object v1, v0, LX/CwL;->e:Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-object v1, v1

    .line 2264116
    if-eqz v1, :cond_4

    const/4 v1, 0x1

    move v5, v1

    .line 2264117
    :goto_3
    const v1, 0x7f0312ba

    invoke-virtual {v7, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2264118
    invoke-virtual {p0}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 2264119
    const v1, 0x7f0d02a7

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2264120
    iget-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->a:Ljava/util/Map;

    invoke-interface {v2, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2264121
    iget-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->c:LX/0P1;

    .line 2264122
    iget-object v9, v0, LX/CwL;->b:Ljava/lang/String;

    move-object v9, v9

    .line 2264123
    invoke-virtual {v2, v9}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v9

    .line 2264124
    invoke-virtual {v8}, Landroid/view/View;->getPaddingLeft()I

    move-result v10

    .line 2264125
    if-eqz v5, :cond_5

    const v2, 0x7f02079a

    .line 2264126
    :goto_4
    invoke-direct {p0, v8, v2}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->a(Landroid/view/View;I)V

    .line 2264127
    if-eqz v5, :cond_6

    iget v2, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->d:I

    :goto_5
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2264128
    if-eqz v5, :cond_7

    iget-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->f:Landroid/graphics/ColorFilter;

    :goto_6
    invoke-virtual {v9, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 2264129
    if-eqz v5, :cond_8

    .line 2264130
    iget-object v2, v0, LX/CwL;->e:Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-object v0, v2

    .line 2264131
    iget-object v2, v0, Lcom/facebook/search/results/protocol/filters/FilterValue;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2264132
    :goto_7
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2264133
    iget v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->h:I

    iget v2, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->h:I

    invoke-virtual {v8, v10, v0, v10, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 2264134
    invoke-virtual {v1, v9, v11, v11, v11}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2264135
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v8, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2264136
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_2

    :cond_4
    move v5, v4

    .line 2264137
    goto :goto_3

    .line 2264138
    :cond_5
    const v2, 0x7f0217c6

    goto :goto_4

    .line 2264139
    :cond_6
    iget v2, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->e:I

    goto :goto_5

    .line 2264140
    :cond_7
    iget-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->g:Landroid/graphics/ColorFilter;

    goto :goto_6

    .line 2264141
    :cond_8
    iget-object v2, v0, LX/CwL;->c:Ljava/lang/String;

    move-object v0, v2

    .line 2264142
    goto :goto_7

    .line 2264143
    :cond_9
    invoke-virtual {p0, v4}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public setNumButtons(I)V
    .locals 0

    .prologue
    .line 2264148
    iput p1, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->i:I

    .line 2264149
    return-void
.end method

.method public setOnFilterSelectedListener(LX/Fd4;)V
    .locals 2

    .prologue
    .line 2264144
    iput-object p1, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->m:LX/Fd4;

    .line 2264145
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->l:LX/Fd6;

    iget-object v1, p0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->m:LX/Fd4;

    .line 2264146
    iput-object v1, v0, LX/Fd6;->y:LX/Fd4;

    .line 2264147
    return-void
.end method
