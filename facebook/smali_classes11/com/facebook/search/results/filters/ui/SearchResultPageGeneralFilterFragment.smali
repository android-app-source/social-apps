.class public Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""

# interfaces
.implements LX/FdZ;


# instance fields
.field private m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/5uu;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/FdZ;

.field public o:LX/FdT;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private p:Landroid/widget/ListView;

.field private q:Lcom/facebook/widget/CustomLinearLayout;

.field private r:Lcom/facebook/widget/text/BetterButton;

.field private s:Lcom/facebook/widget/text/BetterButton;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2263824
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;

    new-instance v0, LX/FdT;

    const-class p0, Landroid/content/Context;

    invoke-interface {v1, p0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/Context;

    invoke-direct {v0, p0}, LX/FdT;-><init>(Landroid/content/Context;)V

    move-object v1, v0

    check-cast v1, LX/FdT;

    iput-object v1, p1, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->o:LX/FdT;

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2263825
    return-void
.end method

.method public final a(LX/5uu;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5uu;",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2263826
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->o:LX/FdT;

    .line 2263827
    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2263828
    iget-object p0, v0, LX/FdT;->b:Ljava/util/HashMap;

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 2263829
    iget-object p0, v0, LX/FdT;->b:Ljava/util/HashMap;

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2263830
    :cond_0
    :goto_0
    const p0, 0x73ad30ba

    invoke-static {v0, p0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2263831
    return-void

    .line 2263832
    :cond_1
    iget-object p0, v0, LX/FdT;->b:Ljava/util/HashMap;

    invoke-virtual {p0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4842b5cf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2263833
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2263834
    const-class v1, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;

    invoke-static {v1, p0}, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2263835
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2263836
    if-eqz v1, :cond_0

    .line 2263837
    const-string v2, "main_filter_list"

    invoke-static {v1, v2}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->m:LX/0Px;

    .line 2263838
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x79c70b12

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v0, 0x2a

    const v1, -0x3d95fe95

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2263839
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 2263840
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->requestFeature(I)Z

    .line 2263841
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e0241

    invoke-direct {v0, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f03129b

    invoke-virtual {v0, v2, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2263842
    const v0, 0x7f0d2b86

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->p:Landroid/widget/ListView;

    .line 2263843
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->o:LX/FdT;

    iget-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->m:LX/0Px;

    invoke-virtual {v0, v3}, LX/FdT;->a(LX/0Px;)V

    .line 2263844
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->p:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->o:LX/FdT;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2263845
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->p:Landroid/widget/ListView;

    new-instance v3, LX/Fda;

    invoke-direct {v3, p0}, LX/Fda;-><init>(Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2263846
    const v0, 0x7f0d2b87

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->q:Lcom/facebook/widget/CustomLinearLayout;

    .line 2263847
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->q:Lcom/facebook/widget/CustomLinearLayout;

    const v3, 0x7f0d2b7e

    invoke-static {v0, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->r:Lcom/facebook/widget/text/BetterButton;

    .line 2263848
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->q:Lcom/facebook/widget/CustomLinearLayout;

    const v3, 0x7f0d2b7f

    invoke-static {v0, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->s:Lcom/facebook/widget/text/BetterButton;

    .line 2263849
    new-instance v0, LX/Fdb;

    invoke-direct {v0, p0}, LX/Fdb;-><init>(Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2263850
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->r:Lcom/facebook/widget/text/BetterButton;

    new-instance v3, LX/Fdc;

    invoke-direct {v3, p0}, LX/Fdc;-><init>(Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2263851
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->s:Lcom/facebook/widget/text/BetterButton;

    new-instance v3, LX/Fdd;

    invoke-direct {v3, p0}, LX/Fdd;-><init>(Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2263852
    const/16 v0, 0x2b

    const v3, 0x263ee8db

    invoke-static {v5, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x28bc3882

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2263853
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroyView()V

    .line 2263854
    iput-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->r:Lcom/facebook/widget/text/BetterButton;

    .line 2263855
    iput-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->s:Lcom/facebook/widget/text/BetterButton;

    .line 2263856
    iput-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->q:Lcom/facebook/widget/CustomLinearLayout;

    .line 2263857
    iput-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->p:Landroid/widget/ListView;

    .line 2263858
    const/16 v1, 0x2b

    const v2, -0x3e1105f5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x68cbd914

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2263859
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onResume()V

    .line 2263860
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 2263861
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 2263862
    const/16 v2, 0x50

    invoke-virtual {v1, v2}, Landroid/view/Window;->setGravity(I)V

    .line 2263863
    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setLayout(II)V

    .line 2263864
    const/16 v1, 0x2b

    const v2, 0x297e5e1

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
