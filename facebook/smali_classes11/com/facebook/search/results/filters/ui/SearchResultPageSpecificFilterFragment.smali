.class public Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/Fdl;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public o:LX/5uu;

.field private p:Landroid/widget/ListView;

.field private q:Lcom/facebook/widget/CustomLinearLayout;

.field private r:Lcom/facebook/widget/text/BetterButton;

.field private s:Lcom/facebook/widget/text/BetterButton;

.field private t:Lcom/facebook/widget/text/BetterTextView;

.field public u:LX/FdZ;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2263957
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method

.method public static a(LX/5uu;LX/FdZ;)Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;
    .locals 3

    .prologue
    .line 2263958
    new-instance v0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;

    invoke-direct {v0}, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;-><init>()V

    .line 2263959
    const/4 v1, 0x2

    const v2, 0x7f0e0895

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 2263960
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2263961
    const-string v2, "main_filter"

    invoke-static {v1, v2, p0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2263962
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2263963
    iput-object p1, v0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->u:LX/FdZ;

    .line 2263964
    return-object v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;

    new-instance p0, LX/Fdl;

    const-class v1, Landroid/content/Context;

    invoke-interface {v2, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {p0, v1}, LX/Fdl;-><init>(Landroid/content/Context;)V

    invoke-static {v2}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    iput-object v1, p0, LX/Fdl;->b:LX/0ad;

    move-object v1, p0

    check-cast v1, LX/Fdl;

    const-class p0, Landroid/content/Context;

    invoke-interface {v2, p0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    iput-object v1, p1, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->m:LX/Fdl;

    iput-object v2, p1, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->n:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2263917
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->m:LX/Fdl;

    invoke-virtual {v0, p1}, LX/Fdl;->a(LX/0Px;)V

    .line 2263918
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x47901e14

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2263951
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2263952
    const-class v0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;

    invoke-static {v0, p0}, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2263953
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2263954
    if-eqz v0, :cond_0

    .line 2263955
    const-string v2, "main_filter"

    invoke-static {v0, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5uu;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->o:LX/5uu;

    .line 2263956
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x12141ecf

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v0, 0x2a

    const v1, -0x1a16f3b8

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2263932
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 2263933
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->requestFeature(I)Z

    .line 2263934
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->n:Landroid/content/Context;

    const v3, 0x7f0e0241

    invoke-direct {v0, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0312a1

    invoke-virtual {v0, v2, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2263935
    const v0, 0x7f0d2b91

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->p:Landroid/widget/ListView;

    .line 2263936
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->m:LX/Fdl;

    iget-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->o:LX/5uu;

    invoke-virtual {v0, v3}, LX/Fdl;->a(LX/5uu;)V

    .line 2263937
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->p:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->m:LX/Fdl;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2263938
    const v0, 0x7f0d2b8f

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->t:Lcom/facebook/widget/text/BetterTextView;

    .line 2263939
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->t:Lcom/facebook/widget/text/BetterTextView;

    iget-object v3, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->o:LX/5uu;

    invoke-interface {v3}, LX/5uu;->c()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2263940
    const v0, 0x7f0d2b8e

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    .line 2263941
    const v0, 0x7f0d2b92

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->q:Lcom/facebook/widget/CustomLinearLayout;

    .line 2263942
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->q:Lcom/facebook/widget/CustomLinearLayout;

    const v4, 0x7f0d2b7e

    invoke-static {v0, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->r:Lcom/facebook/widget/text/BetterButton;

    .line 2263943
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->q:Lcom/facebook/widget/CustomLinearLayout;

    const v4, 0x7f0d2b7f

    invoke-static {v0, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->s:Lcom/facebook/widget/text/BetterButton;

    .line 2263944
    const v0, 0x7f0d2b8d

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 2263945
    new-instance v4, LX/Fde;

    invoke-direct {v4, p0}, LX/Fde;-><init>(Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2263946
    new-instance v0, LX/Fdf;

    invoke-direct {v0, p0}, LX/Fdf;-><init>(Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2263947
    new-instance v0, LX/Fdg;

    invoke-direct {v0, p0}, LX/Fdg;-><init>(Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2263948
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->r:Lcom/facebook/widget/text/BetterButton;

    new-instance v3, LX/Fdh;

    invoke-direct {v3, p0}, LX/Fdh;-><init>(Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2263949
    iget-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->s:Lcom/facebook/widget/text/BetterButton;

    new-instance v3, LX/Fdi;

    invoke-direct {v3, p0}, LX/Fdi;-><init>(Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2263950
    const/16 v0, 0x2b

    const v3, 0x5e284ead

    invoke-static {v5, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x746c89ca

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2263925
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroyView()V

    .line 2263926
    iput-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->r:Lcom/facebook/widget/text/BetterButton;

    .line 2263927
    iput-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->s:Lcom/facebook/widget/text/BetterButton;

    .line 2263928
    iput-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->q:Lcom/facebook/widget/CustomLinearLayout;

    .line 2263929
    iput-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->p:Landroid/widget/ListView;

    .line 2263930
    iput-object v2, p0, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->t:Lcom/facebook/widget/text/BetterTextView;

    .line 2263931
    const/16 v1, 0x2b

    const v2, -0x682e3565

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x3cf8597b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2263919
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onResume()V

    .line 2263920
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 2263921
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 2263922
    const/16 v2, 0x50

    invoke-virtual {v1, v2}, Landroid/view/Window;->setGravity(I)V

    .line 2263923
    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setLayout(II)V

    .line 2263924
    const/16 v1, 0x2b

    const v2, -0x2cf09910

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
