.class public Lcom/facebook/search/results/filters/ui/SearchResultFilterDistanceSeekbar;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/widget/text/BetterTextView;

.field private final b:Lcom/facebook/uicontrib/seekbar/FbSeekBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2263536
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/search/results/filters/ui/SearchResultFilterDistanceSeekbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2263537
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2263538
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/search/results/filters/ui/SearchResultFilterDistanceSeekbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2263539
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2263540
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2263541
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/filters/ui/SearchResultFilterDistanceSeekbar;->setOrientation(I)V

    .line 2263542
    const v0, 0x7f03129a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2263543
    const v0, 0x7f0d2b84

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultFilterDistanceSeekbar;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2263544
    const v0, 0x7f0d2b85

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;

    iput-object v0, p0, Lcom/facebook/search/results/filters/ui/SearchResultFilterDistanceSeekbar;->b:Lcom/facebook/uicontrib/seekbar/FbSeekBar;

    .line 2263545
    return-void
.end method


# virtual methods
.method public final c(I)V
    .locals 6

    .prologue
    .line 2263546
    int-to-float v0, p1

    const/high16 v1, 0x42c80000    # 100.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const-string v0, "%d+"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2263547
    :goto_0
    iget-object v1, p0, Lcom/facebook/search/results/filters/ui/SearchResultFilterDistanceSeekbar;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/search/results/filters/ui/SearchResultFilterDistanceSeekbar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08208c    # 1.80944E38f

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2263548
    return-void

    .line 2263549
    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
