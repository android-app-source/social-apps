.class public final Lcom/facebook/search/results/livefeed/loader/RefreshController$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Ffz;


# direct methods
.method public constructor <init>(LX/Ffz;)V
    .locals 0

    .prologue
    .line 2269286
    iput-object p1, p0, Lcom/facebook/search/results/livefeed/loader/RefreshController$1;->a:LX/Ffz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 2269287
    iget-object v0, p0, Lcom/facebook/search/results/livefeed/loader/RefreshController$1;->a:LX/Ffz;

    iget-object v0, v0, LX/Ffz;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/facebook/search/results/livefeed/loader/RefreshController$1;->a:LX/Ffz;

    iget-wide v2, v2, LX/Ffz;->d:J

    sub-long/2addr v0, v2

    iget-object v2, p0, Lcom/facebook/search/results/livefeed/loader/RefreshController$1;->a:LX/Ffz;

    iget-wide v2, v2, LX/Ffz;->f:J

    sub-long/2addr v0, v2

    .line 2269288
    iget-object v2, p0, Lcom/facebook/search/results/livefeed/loader/RefreshController$1;->a:LX/Ffz;

    iget v2, v2, LX/Ffz;->b:I

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 2269289
    iget-object v2, p0, Lcom/facebook/search/results/livefeed/loader/RefreshController$1;->a:LX/Ffz;

    iget-object v2, v2, LX/Ffz;->a:Landroid/os/Handler;

    iget-object v3, p0, Lcom/facebook/search/results/livefeed/loader/RefreshController$1;->a:LX/Ffz;

    iget-object v3, v3, LX/Ffz;->h:Ljava/lang/Runnable;

    iget-object v4, p0, Lcom/facebook/search/results/livefeed/loader/RefreshController$1;->a:LX/Ffz;

    iget v4, v4, LX/Ffz;->b:I

    int-to-long v4, v4

    sub-long v0, v4, v0

    const v4, -0x2b47e5c8

    invoke-static {v2, v3, v0, v1, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2269290
    :goto_0
    return-void

    .line 2269291
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/livefeed/loader/RefreshController$1;->a:LX/Ffz;

    iget-object v1, p0, Lcom/facebook/search/results/livefeed/loader/RefreshController$1;->a:LX/Ffz;

    iget-object v1, v1, LX/Ffz;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 2269292
    iput-wide v2, v0, LX/Ffz;->d:J

    .line 2269293
    iget-object v0, p0, Lcom/facebook/search/results/livefeed/loader/RefreshController$1;->a:LX/Ffz;

    .line 2269294
    iput-wide v4, v0, LX/Ffz;->f:J

    .line 2269295
    iget-object v0, p0, Lcom/facebook/search/results/livefeed/loader/RefreshController$1;->a:LX/Ffz;

    .line 2269296
    iput-wide v4, v0, LX/Ffz;->e:J

    .line 2269297
    iget-object v0, p0, Lcom/facebook/search/results/livefeed/loader/RefreshController$1;->a:LX/Ffz;

    invoke-virtual {v0}, LX/Ffz;->a()V

    .line 2269298
    iget-object v0, p0, Lcom/facebook/search/results/livefeed/loader/RefreshController$1;->a:LX/Ffz;

    iget-object v0, v0, LX/Ffz;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/search/results/livefeed/loader/RefreshController$1;->a:LX/Ffz;

    iget-object v1, v1, LX/Ffz;->h:Ljava/lang/Runnable;

    iget-object v2, p0, Lcom/facebook/search/results/livefeed/loader/RefreshController$1;->a:LX/Ffz;

    iget v2, v2, LX/Ffz;->b:I

    int-to-long v2, v2

    const v4, 0x3289550d

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method
