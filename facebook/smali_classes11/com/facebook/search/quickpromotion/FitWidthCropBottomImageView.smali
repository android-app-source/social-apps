.class public Lcom/facebook/search/quickpromotion/FitWidthCropBottomImageView;
.super Lcom/facebook/widget/FbImageView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2258558
    invoke-direct {p0, p1}, Lcom/facebook/widget/FbImageView;-><init>(Landroid/content/Context;)V

    .line 2258559
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/facebook/search/quickpromotion/FitWidthCropBottomImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 2258560
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2258561
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/FbImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2258562
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/facebook/search/quickpromotion/FitWidthCropBottomImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 2258563
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2258564
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/FbImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2258565
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/facebook/search/quickpromotion/FitWidthCropBottomImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 2258566
    return-void
.end method


# virtual methods
.method public final onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2258567
    invoke-virtual {p0}, Lcom/facebook/search/quickpromotion/FitWidthCropBottomImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2258568
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/FbImageView;->onLayout(ZIIII)V

    .line 2258569
    :goto_0
    return-void

    .line 2258570
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/quickpromotion/FitWidthCropBottomImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 2258571
    invoke-virtual {p0}, Lcom/facebook/search/quickpromotion/FitWidthCropBottomImageView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/facebook/search/quickpromotion/FitWidthCropBottomImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 2258572
    invoke-virtual {v0, v1, v1, v3, v3}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 2258573
    invoke-virtual {p0}, Lcom/facebook/search/quickpromotion/FitWidthCropBottomImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 2258574
    invoke-virtual {p0}, Lcom/facebook/search/quickpromotion/FitWidthCropBottomImageView;->getHeight()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 2258575
    invoke-virtual {p0}, Lcom/facebook/search/quickpromotion/FitWidthCropBottomImageView;->getHeight()I

    move-result v2

    sub-int v1, v2, v1

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 2258576
    :cond_1
    invoke-virtual {p0, v0}, Lcom/facebook/search/quickpromotion/FitWidthCropBottomImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 2258577
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/FbImageView;->onLayout(ZIIII)V

    goto :goto_0
.end method
