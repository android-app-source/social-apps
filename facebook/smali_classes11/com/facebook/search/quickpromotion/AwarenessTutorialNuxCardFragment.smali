.class public Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/resources/ui/FbAutoFitTextView;

.field public d:Lcom/facebook/resources/ui/FbAutoFitTextView;

.field public e:Lcom/facebook/resources/ui/FbAutoFitTextView;

.field public f:Lcom/facebook/widget/CustomLinearLayout;

.field public g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public h:Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2258548
    const-class v0, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;

    const-string v1, "search_awareness"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2258498
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2258499
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2258500
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    iput-object v0, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->a:LX/0wM;

    .line 2258501
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x67cec129

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2258502
    const v0, 0x7f03014e

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2258503
    const v0, 0x7f0d0621

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbAutoFitTextView;

    iput-object v0, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->c:Lcom/facebook/resources/ui/FbAutoFitTextView;

    .line 2258504
    const v0, 0x7f0d0622

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbAutoFitTextView;

    iput-object v0, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->d:Lcom/facebook/resources/ui/FbAutoFitTextView;

    .line 2258505
    const v0, 0x7f0d0624

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbAutoFitTextView;

    iput-object v0, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->e:Lcom/facebook/resources/ui/FbAutoFitTextView;

    .line 2258506
    const v0, 0x7f0d0620

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->f:Lcom/facebook/widget/CustomLinearLayout;

    .line 2258507
    const v0, 0x7f0d0623

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2258508
    const/16 v0, 0x2b

    const v3, -0x786cd018

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onResume()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1c0729ce

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2258509
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2258510
    const/16 v5, 0x10

    const/high16 v4, 0x41400000    # 12.0f

    .line 2258511
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2258512
    if-nez v1, :cond_0

    .line 2258513
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x12d6a0e2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2258514
    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2258515
    const-string v2, "configuration"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;

    iput-object v1, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->h:Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;

    .line 2258516
    iget-object v1, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->c:Lcom/facebook/resources/ui/FbAutoFitTextView;

    iget-object v2, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->h:Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;

    .line 2258517
    iget-object v6, v2, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->a:Ljava/lang/String;

    move-object v2, v6

    .line 2258518
    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbAutoFitTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2258519
    iget-object v1, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->c:Lcom/facebook/resources/ui/FbAutoFitTextView;

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbAutoFitTextView;->setMinTextSizeSp(F)V

    .line 2258520
    iget-object v1, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->d:Lcom/facebook/resources/ui/FbAutoFitTextView;

    iget-object v2, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->h:Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;

    .line 2258521
    iget-object v6, v2, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->b:Ljava/lang/String;

    move-object v2, v6

    .line 2258522
    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbAutoFitTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2258523
    iget-object v1, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->d:Lcom/facebook/resources/ui/FbAutoFitTextView;

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbAutoFitTextView;->setMinTextSizeSp(F)V

    .line 2258524
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200ef

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2258525
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v5, :cond_2

    .line 2258526
    iget-object v2, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->d:Lcom/facebook/resources/ui/FbAutoFitTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbAutoFitTextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2258527
    :goto_1
    iget-object v1, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->e:Lcom/facebook/resources/ui/FbAutoFitTextView;

    iget-object v2, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->h:Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;

    .line 2258528
    iget-object v6, v2, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->d:Ljava/lang/String;

    move-object v2, v6

    .line 2258529
    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbAutoFitTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2258530
    iget-object v1, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->e:Lcom/facebook/resources/ui/FbAutoFitTextView;

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbAutoFitTextView;->setMinTextSizeSp(F)V

    .line 2258531
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v2, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->h:Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;

    .line 2258532
    iget v4, v2, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->e:I

    move v2, v4

    .line 2258533
    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2258534
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v5, :cond_3

    .line 2258535
    iget-object v2, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->f:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/CustomLinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2258536
    :goto_2
    iget-object v1, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->h:Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;

    .line 2258537
    iget-object v4, v2, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->c:Landroid/net/Uri;

    move-object v2, v4

    .line 2258538
    sget-object v4, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2258539
    iget-object v1, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v1

    check-cast v1, LX/1af;

    sget-object v2, LX/FaJ;->i:LX/1Up;

    invoke-virtual {v1, v2}, LX/1af;->a(LX/1Up;)V

    .line 2258540
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 2258541
    iget-object v1, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->a:LX/0wM;

    const v2, 0x7f02091b

    const/4 v4, -0x1

    invoke-virtual {v1, v2, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2258542
    if-eqz v1, :cond_1

    .line 2258543
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {v1, v6, v6, v2, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2258544
    iget-object v2, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->d:Lcom/facebook/resources/ui/FbAutoFitTextView;

    invoke-virtual {v2, v1, v5, v5, v5}, Lcom/facebook/resources/ui/FbAutoFitTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2258545
    :cond_1
    goto/16 :goto_0

    .line 2258546
    :cond_2
    iget-object v2, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->d:Lcom/facebook/resources/ui/FbAutoFitTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbAutoFitTextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 2258547
    :cond_3
    iget-object v2, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;->f:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/CustomLinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method
