.class public Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile e:Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;


# instance fields
.field public final b:LX/1HI;

.field public final c:LX/03V;

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1bf;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2258747
    const-class v0, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;

    const-string v1, "search_awareness"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1HI;LX/03V;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2258748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2258749
    iput-object p1, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;->b:LX/1HI;

    .line 2258750
    iput-object p2, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;->c:LX/03V;

    .line 2258751
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;->d:Ljava/util/Map;

    .line 2258752
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;
    .locals 5

    .prologue
    .line 2258753
    sget-object v0, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;->e:Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;

    if-nez v0, :cond_1

    .line 2258754
    const-class v1, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;

    monitor-enter v1

    .line 2258755
    :try_start_0
    sget-object v0, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;->e:Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2258756
    if-eqz v2, :cond_0

    .line 2258757
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2258758
    new-instance p0, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;

    invoke-static {v0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v3

    check-cast v3, LX/1HI;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;-><init>(LX/1HI;LX/03V;)V

    .line 2258759
    move-object v0, p0

    .line 2258760
    sput-object v0, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;->e:Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2258761
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2258762
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2258763
    :cond_1
    sget-object v0, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;->e:Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;

    return-object v0

    .line 2258764
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2258765
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
