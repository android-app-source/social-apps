.class public Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private A:Lcom/facebook/resources/ui/FbButton;

.field private B:Lcom/facebook/resources/ui/FbButton;

.field private C:Lcom/facebook/resources/ui/FbButton;

.field private D:Lcom/facebook/fbui/glyph/GlyphView;

.field private E:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

.field private p:LX/FaD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fas;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FaY;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13B;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private w:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:Landroid/support/v4/view/ViewPager;

.field public y:Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;

.field private z:LX/FaC;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2259078
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2259079
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259080
    iput-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->q:LX/0Ot;

    .line 2259081
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259082
    iput-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->r:LX/0Ot;

    .line 2259083
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259084
    iput-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->s:LX/0Ot;

    .line 2259085
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259086
    iput-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->t:LX/0Ot;

    .line 2259087
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259088
    iput-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->u:LX/0Ot;

    .line 2259089
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259090
    iput-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->v:LX/0Ot;

    .line 2259091
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259092
    iput-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->w:LX/0Ot;

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2259093
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2259094
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->z:LX/FaC;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    .line 2259095
    :goto_0
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->x:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2259096
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->y:Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->setCurrentItem(I)V

    .line 2259097
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13B;

    invoke-static {p0, v1}, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->d(Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/13B;->a(I)V

    .line 2259098
    return-void

    .line 2259099
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;LX/FaD;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;",
            "LX/FaD;",
            "LX/0Ot",
            "<",
            "LX/Fas;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FaY;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/13B;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0hL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2259101
    iput-object p1, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->p:LX/FaD;

    iput-object p2, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->q:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->r:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->s:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->t:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->u:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->v:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->w:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;

    const-class v1, LX/FaD;

    invoke-interface {v8, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/FaD;

    const/16 v2, 0x32fa

    invoke-static {v8, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x32f6

    invoke-static {v8, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0xf9a

    invoke-static {v8, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xc49

    invoke-static {v8, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1141

    invoke-static {v8, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xac0

    invoke-static {v8, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v9, 0x298

    invoke-static {v8, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static/range {v0 .. v8}, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->a(Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;LX/FaD;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method private b()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;
    .locals 1

    .prologue
    .line 2259100
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->E:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;->a()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->a()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    move-result-object v0

    return-object v0
.end method

.method private b(I)Z
    .locals 1

    .prologue
    .line 2259076
    invoke-direct {p0}, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->n()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;I)I
    .locals 1

    .prologue
    .line 2259077
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->z:LX/FaC;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    sub-int/2addr v0, p1

    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, p1, 0x1

    goto :goto_0
.end method

.method public static l(Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2259053
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->x:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    .line 2259054
    sget-object v0, LX/Faj;->a:[I

    invoke-direct {p0}, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->b()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    .line 2259055
    :goto_0
    return-void

    .line 2259056
    :pswitch_0
    iget-object v4, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->A:Lcom/facebook/resources/ui/FbButton;

    invoke-direct {p0, v3}, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2259057
    iget-object v4, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->B:Lcom/facebook/resources/ui/FbButton;

    invoke-direct {p0, v3}, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_2
    invoke-virtual {v4, v0}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2259058
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->C:Lcom/facebook/resources/ui/FbButton;

    invoke-direct {p0, v3}, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->b(I)Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_3
    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2259059
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->D:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0

    :cond_0
    move v0, v2

    .line 2259060
    goto :goto_1

    :cond_1
    move v0, v2

    .line 2259061
    goto :goto_2

    :cond_2
    move v2, v1

    .line 2259062
    goto :goto_3

    .line 2259063
    :pswitch_1
    invoke-direct {p0, v3}, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->b(I)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->p()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    .line 2259064
    :goto_4
    iget-object v4, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->A:Lcom/facebook/resources/ui/FbButton;

    if-eqz v0, :cond_5

    move v3, v1

    :goto_5
    invoke-virtual {v4, v3}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2259065
    iget-object v3, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->B:Lcom/facebook/resources/ui/FbButton;

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {v3, v0}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2259066
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->C:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2259067
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->D:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-direct {p0}, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->p()Z

    move-result v3

    if-eqz v3, :cond_7

    :goto_7
    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 2259068
    goto :goto_4

    :cond_5
    move v3, v2

    .line 2259069
    goto :goto_5

    :cond_6
    move v0, v2

    .line 2259070
    goto :goto_6

    :cond_7
    move v2, v1

    .line 2259071
    goto :goto_7

    .line 2259072
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->A:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2259073
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->B:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2259074
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->C:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2259075
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->D:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static m(Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;)V
    .locals 3

    .prologue
    .line 2259051
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    const-string v1, "4084"

    invoke-static {v1}, LX/7CP;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2259052
    return-void
.end method

.method private n()I
    .locals 1

    .prologue
    .line 2259050
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->z:LX/FaC;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public static o(Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;)I
    .locals 2

    .prologue
    .line 2259049
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->x:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0}, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->n()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->x:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0}, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->n()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method private p()Z
    .locals 4

    .prologue
    .line 2259047
    const-string v0, "4084"

    invoke-static {v0}, LX/7CP;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    .line 2259048
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2259014
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2259015
    const v0, 0x7f0400c8

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->overridePendingTransition(II)V

    .line 2259016
    const v0, 0x7f030150

    invoke-virtual {p0, v0}, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->setContentView(I)V

    .line 2259017
    invoke-static {p0, p0}, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2259018
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fas;

    .line 2259019
    iget-object v1, v0, LX/Fas;->l:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    move-object v0, v1

    .line 2259020
    iput-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->E:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    .line 2259021
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->E:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2259022
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->E:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;->a()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2259023
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->p:LX/FaD;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    .line 2259024
    new-instance v3, LX/FaC;

    const-class v4, LX/Fan;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/Fan;

    invoke-static {v0}, LX/Fas;->a(LX/0QB;)LX/Fas;

    move-result-object v6

    check-cast v6, LX/Fas;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static {v0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v9

    check-cast v9, LX/0hL;

    move-object v5, v1

    invoke-direct/range {v3 .. v9}, LX/FaC;-><init>(LX/Fan;LX/0gc;LX/Fas;Landroid/content/Context;LX/0Uh;LX/0hL;)V

    .line 2259025
    move-object v0, v3

    .line 2259026
    iput-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->z:LX/FaC;

    .line 2259027
    const v0, 0x7f0d0630

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->x:Landroid/support/v4/view/ViewPager;

    .line 2259028
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->x:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->z:LX/FaC;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2259029
    const v0, 0x7f0d0631

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;

    iput-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->y:Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;

    .line 2259030
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->y:Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;

    iget-object v1, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->x:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2259031
    invoke-direct {p0}, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->a()V

    .line 2259032
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->y:Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;

    new-instance v1, LX/Fad;

    invoke-direct {v1, p0}, LX/Fad;-><init>(Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;)V

    .line 2259033
    iput-object v1, v0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->f:LX/0hc;

    .line 2259034
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->E:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;->a()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;

    move-result-object v1

    .line 2259035
    const v0, 0x7f0d061f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->A:Lcom/facebook/resources/ui/FbButton;

    .line 2259036
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->A:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2259037
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->A:Lcom/facebook/resources/ui/FbButton;

    new-instance v2, LX/Fae;

    invoke-direct {v2, p0, v1}, LX/Fae;-><init>(Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;)V

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2259038
    const v0, 0x7f0d061e

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->B:Lcom/facebook/resources/ui/FbButton;

    .line 2259039
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->B:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2259040
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->B:Lcom/facebook/resources/ui/FbButton;

    new-instance v2, LX/Faf;

    invoke-direct {v2, p0, v1}, LX/Faf;-><init>(Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;)V

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2259041
    const v0, 0x7f0d061d

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->C:Lcom/facebook/resources/ui/FbButton;

    .line 2259042
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->C:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/Fag;

    invoke-direct {v1, p0}, LX/Fag;-><init>(Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2259043
    const v0, 0x7f0d0632

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->D:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2259044
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->D:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/Fai;

    invoke-direct {v1, p0}, LX/Fai;-><init>(Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2259045
    invoke-static {p0}, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->l(Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;)V

    .line 2259046
    return-void
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 2259003
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2259004
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13B;

    const-string v1, "back_button_action"

    invoke-virtual {v0, v1}, LX/13B;->b(Ljava/lang/String;)V

    .line 2259005
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x22

    const v1, 0x714d67b5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2259006
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2259007
    iget-object v1, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->A:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2259008
    iget-object v1, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->B:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2259009
    iget-object v1, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->C:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2259010
    iget-object v1, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->D:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2259011
    iget-object v1, p0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->y:Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;

    .line 2259012
    iput-object v2, v1, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->f:LX/0hc;

    .line 2259013
    const/16 v1, 0x23

    const v2, -0x79a20214

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
