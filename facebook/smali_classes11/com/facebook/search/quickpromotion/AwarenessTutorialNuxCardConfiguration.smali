.class public Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field public final e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2258490
    new-instance v0, LX/FaE;

    invoke-direct {v0}, LX/FaE;-><init>()V

    sput-object v0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2258482
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2258483
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->a:Ljava/lang/String;

    .line 2258484
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->b:Ljava/lang/String;

    .line 2258485
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->c:Landroid/net/Uri;

    .line 2258486
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->d:Ljava/lang/String;

    .line 2258487
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->e:I

    .line 2258488
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2258491
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2258492
    iput-object p1, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->a:Ljava/lang/String;

    .line 2258493
    iput-object p2, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->b:Ljava/lang/String;

    .line 2258494
    iput-object p3, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->c:Landroid/net/Uri;

    .line 2258495
    iput-object p4, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->d:Ljava/lang/String;

    .line 2258496
    iput p5, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->e:I

    .line 2258497
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2258489
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2258475
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2258476
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2258477
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->c:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2258478
    iget-object v0, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2258479
    iget v0, p0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2258480
    return-void

    .line 2258481
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
