.class public Lcom/facebook/saved/interstitial/SavedDashboardInterstitialActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/16H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2252113
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2252114
    iget-object v0, p0, Lcom/facebook/saved/interstitial/SavedDashboardInterstitialActivity;->q:Landroid/widget/Button;

    new-instance v1, LX/FWP;

    invoke-direct {v1, p0}, LX/FWP;-><init>(Lcom/facebook/saved/interstitial/SavedDashboardInterstitialActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2252115
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/saved/interstitial/SavedDashboardInterstitialActivity;

    invoke-static {v0}, LX/16H;->a(LX/0QB;)LX/16H;

    move-result-object v0

    check-cast v0, LX/16H;

    iput-object v0, p0, Lcom/facebook/saved/interstitial/SavedDashboardInterstitialActivity;->p:LX/16H;

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2252116
    const v0, 0x7f040004

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/saved/interstitial/SavedDashboardInterstitialActivity;->overridePendingTransition(II)V

    .line 2252117
    return-void
.end method

.method public static l(Lcom/facebook/saved/interstitial/SavedDashboardInterstitialActivity;)V
    .locals 2

    .prologue
    .line 2252118
    const/4 v0, 0x0

    const v1, 0x7f040006

    invoke-virtual {p0, v0, v1}, Lcom/facebook/saved/interstitial/SavedDashboardInterstitialActivity;->overridePendingTransition(II)V

    .line 2252119
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2252120
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2252121
    invoke-static {p0, p0}, Lcom/facebook/saved/interstitial/SavedDashboardInterstitialActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2252122
    invoke-direct {p0}, Lcom/facebook/saved/interstitial/SavedDashboardInterstitialActivity;->b()V

    .line 2252123
    const v0, 0x7f031258

    invoke-virtual {p0, v0}, Lcom/facebook/saved/interstitial/SavedDashboardInterstitialActivity;->setContentView(I)V

    .line 2252124
    const v0, 0x7f0d2b2e

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/saved/interstitial/SavedDashboardInterstitialActivity;->q:Landroid/widget/Button;

    .line 2252125
    invoke-direct {p0}, Lcom/facebook/saved/interstitial/SavedDashboardInterstitialActivity;->a()V

    .line 2252126
    iget-object v0, p0, Lcom/facebook/saved/interstitial/SavedDashboardInterstitialActivity;->p:LX/16H;

    .line 2252127
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "saved_dashboard_nux_imp"

    invoke-direct {v1, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "saved_dashboard"

    .line 2252128
    iput-object p0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2252129
    move-object v1, v1

    .line 2252130
    const-string p0, "event_id"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2252131
    iget-object p0, v0, LX/16H;->a:LX/0Zb;

    invoke-interface {p0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2252132
    return-void
.end method

.method public final onBackPressed()V
    .locals 0

    .prologue
    .line 2252133
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2252134
    invoke-static {p0}, Lcom/facebook/saved/interstitial/SavedDashboardInterstitialActivity;->l(Lcom/facebook/saved/interstitial/SavedDashboardInterstitialActivity;)V

    .line 2252135
    return-void
.end method
