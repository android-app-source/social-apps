.class public Lcom/facebook/saved/views/SavedDashboardEmptyView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/ImageView;

.field private c:Z

.field private d:LX/FVP;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2252578
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2252579
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/saved/views/SavedDashboardEmptyView;->c:Z

    .line 2252580
    invoke-direct {p0}, Lcom/facebook/saved/views/SavedDashboardEmptyView;->c()V

    .line 2252581
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2252574
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2252575
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/saved/views/SavedDashboardEmptyView;->c:Z

    .line 2252576
    invoke-direct {p0}, Lcom/facebook/saved/views/SavedDashboardEmptyView;->c()V

    .line 2252577
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2252565
    const v0, 0x7f0308cb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2252566
    const v0, 0x7f0d16dd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/saved/views/SavedDashboardEmptyView;->a:Landroid/widget/TextView;

    .line 2252567
    const v0, 0x7f0d16dc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/saved/views/SavedDashboardEmptyView;->b:Landroid/widget/ImageView;

    .line 2252568
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/facebook/saved/views/SavedDashboardEmptyView;->setGravity(I)V

    .line 2252569
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/saved/views/SavedDashboardEmptyView;->setOrientation(I)V

    .line 2252570
    invoke-virtual {p0}, Lcom/facebook/saved/views/SavedDashboardEmptyView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1156

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2252571
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/facebook/saved/views/SavedDashboardEmptyView;->setPadding(IIII)V

    .line 2252572
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/saved/views/SavedDashboardEmptyView;->c:Z

    .line 2252573
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2252562
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardEmptyView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2252563
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardEmptyView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2252564
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2252582
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardEmptyView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2252583
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardEmptyView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2252584
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 2252556
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2252557
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardEmptyView;->d:LX/FVP;

    if-eqz v0, :cond_0

    .line 2252558
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardEmptyView;->d:LX/FVP;

    invoke-interface {v0}, LX/FVP;->a()V

    .line 2252559
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/saved/views/SavedDashboardEmptyView;->d:LX/FVP;

    .line 2252560
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/saved/views/SavedDashboardEmptyView;->c:Z

    .line 2252561
    return-void
.end method

.method public setIcon(I)V
    .locals 1

    .prologue
    .line 2252554
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardEmptyView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2252555
    return-void
.end method

.method public setOnEmptyViewReadyListener(LX/FVP;)V
    .locals 1

    .prologue
    .line 2252550
    iget-boolean v0, p0, Lcom/facebook/saved/views/SavedDashboardEmptyView;->c:Z

    if-eqz v0, :cond_0

    .line 2252551
    invoke-interface {p1}, LX/FVP;->a()V

    .line 2252552
    :goto_0
    return-void

    .line 2252553
    :cond_0
    iput-object p1, p0, Lcom/facebook/saved/views/SavedDashboardEmptyView;->d:LX/FVP;

    goto :goto_0
.end method

.method public setTitle(I)V
    .locals 1

    .prologue
    .line 2252548
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardEmptyView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 2252549
    return-void
.end method
