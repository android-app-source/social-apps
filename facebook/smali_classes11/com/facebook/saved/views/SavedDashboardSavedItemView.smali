.class public Lcom/facebook/saved/views/SavedDashboardSavedItemView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/FWJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

.field private c:Landroid/view/ViewStub;

.field private d:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

.field public e:LX/FWF;

.field private f:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2252753
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2252754
    invoke-direct {p0}, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->a()V

    .line 2252755
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2252687
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2252688
    invoke-direct {p0}, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->a()V

    .line 2252689
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2252750
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2252751
    invoke-direct {p0}, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->a()V

    .line 2252752
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2252741
    const-class v0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;

    invoke-static {v0, p0}, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2252742
    const v0, 0x7f031261

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2252743
    const v0, 0x7f0d2b35

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    iput-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->b:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 2252744
    const v0, 0x7f0d2b36

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->c:Landroid/view/ViewStub;

    .line 2252745
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->b:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    invoke-virtual {p0}, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1159

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 2252746
    invoke-virtual {p0}, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020afe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->f:Landroid/graphics/drawable/Drawable;

    .line 2252747
    invoke-virtual {p0}, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b115a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2252748
    iget-object v1, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->b:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    invoke-virtual {v1, v0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->c(II)V

    .line 2252749
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;

    invoke-static {v0}, LX/FWJ;->a(LX/0QB;)LX/FWJ;

    move-result-object v0

    check-cast v0, LX/FWJ;

    iput-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->a:LX/FWJ;

    return-void
.end method

.method private b(LX/FVt;)V
    .locals 2

    .prologue
    .line 2252756
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->d:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    if-eqz v0, :cond_0

    .line 2252757
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->d:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setVisibility(I)V

    .line 2252758
    :cond_0
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->b:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setVisibility(I)V

    .line 2252759
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->b:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    invoke-virtual {p1}, LX/FVt;->i()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2252760
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->b:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 2252761
    iget-object v1, p1, LX/FVt;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2252762
    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2252763
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->b:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 2252764
    iget-object v1, p1, LX/FVt;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2252765
    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2252766
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->b:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 2252767
    iget-object v1, p1, LX/FVt;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2252768
    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2252769
    iget-object v1, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->b:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    invoke-virtual {p1}, LX/FVt;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->f:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOverlayDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2252770
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->b:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setShowActionButton(Z)V

    .line 2252771
    return-void

    .line 2252772
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(LX/FVt;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2252712
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->b:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setVisibility(I)V

    .line 2252713
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->d:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    if-nez v0, :cond_0

    .line 2252714
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->c:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    iput-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->d:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 2252715
    :cond_0
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->d:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setVisibility(I)V

    .line 2252716
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->d:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 2252717
    iget-object v1, p1, LX/FVt;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2252718
    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2252719
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->d:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    invoke-direct {p0, p1}, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->d(LX/FVt;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2252720
    iget-object v1, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->d:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 2252721
    iget-object v0, p1, LX/FVt;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2252722
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2252723
    invoke-static {p1}, LX/FWJ;->f(LX/FVt;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2252724
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->d:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setShowActionButton(Z)V

    .line 2252725
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->e:LX/FWF;

    if-eqz v0, :cond_1

    .line 2252726
    iget-object v1, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->e:LX/FWF;

    .line 2252727
    invoke-static {p1}, LX/FWJ;->f(LX/FVt;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2252728
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    .line 2252729
    :goto_1
    move-object v0, v2

    .line 2252730
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8bb;

    invoke-interface {v1, p1, v0}, LX/FWF;->a(LX/FVt;LX/8bb;)V

    .line 2252731
    :cond_1
    :goto_2
    return-void

    .line 2252732
    :cond_2
    const-string v0, " "

    goto :goto_0

    .line 2252733
    :cond_3
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->d:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setShowActionButton(Z)V

    goto :goto_2

    .line 2252734
    :cond_4
    invoke-static {p1}, LX/FWJ;->d(LX/FVt;)Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    .line 2252735
    if-nez v2, :cond_5

    .line 2252736
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    goto :goto_1

    .line 2252737
    :cond_5
    sget-object p0, LX/FWI;->a:[I

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLSavedState;->ordinal()I

    move-result v2

    aget v2, p0, v2

    packed-switch v2, :pswitch_data_0

    .line 2252738
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    goto :goto_1

    .line 2252739
    :pswitch_0
    sget-object v2, LX/8bb;->UNDO_ARCHIVE:LX/8bb;

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    goto :goto_1

    .line 2252740
    :pswitch_1
    sget-object v2, LX/8bb;->UNDO_UNARCHIVE:LX/8bb;

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private d(LX/FVt;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2252706
    invoke-static {p1}, LX/FWJ;->d(LX/FVt;)Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    .line 2252707
    sget-object v1, LX/FWq;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSavedState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2252708
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2252709
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081ab7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2252710
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081ab8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2252711
    :pswitch_2
    invoke-virtual {p0}, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081ab9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setItemActionButtonOnClickListener(LX/FVt;)V
    .locals 2

    .prologue
    .line 2252702
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->d:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    if-eqz v0, :cond_0

    .line 2252703
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->d:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2252704
    :cond_0
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->b:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    new-instance v1, LX/FWp;

    invoke-direct {v1, p0, p1}, LX/FWp;-><init>(Lcom/facebook/saved/views/SavedDashboardSavedItemView;LX/FVt;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2252705
    return-void
.end method

.method private setUndoButtonOnClickListener(LX/FVt;)V
    .locals 2

    .prologue
    .line 2252699
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->b:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2252700
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->d:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    new-instance v1, LX/FWo;

    invoke-direct {v1, p0, p1}, LX/FWo;-><init>(Lcom/facebook/saved/views/SavedDashboardSavedItemView;LX/FVt;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2252701
    return-void
.end method


# virtual methods
.method public final a(LX/FVt;)V
    .locals 1

    .prologue
    .line 2252692
    iget-boolean v0, p1, LX/FVt;->j:Z

    move v0, v0

    .line 2252693
    if-eqz v0, :cond_0

    .line 2252694
    invoke-direct {p0, p1}, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->c(LX/FVt;)V

    .line 2252695
    invoke-direct {p0, p1}, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->setUndoButtonOnClickListener(LX/FVt;)V

    .line 2252696
    :goto_0
    return-void

    .line 2252697
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->b(LX/FVt;)V

    .line 2252698
    invoke-direct {p0, p1}, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->setItemActionButtonOnClickListener(LX/FVt;)V

    goto :goto_0
.end method

.method public setSavedItemEventListener(LX/FWF;)V
    .locals 0

    .prologue
    .line 2252690
    iput-object p1, p0, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->e:LX/FWF;

    .line 2252691
    return-void
.end method
