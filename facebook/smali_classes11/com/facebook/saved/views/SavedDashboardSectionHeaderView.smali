.class public Lcom/facebook/saved/views/SavedDashboardSectionHeaderView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2252786
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2252787
    invoke-direct {p0}, Lcom/facebook/saved/views/SavedDashboardSectionHeaderView;->a()V

    .line 2252788
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2252783
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2252784
    invoke-direct {p0}, Lcom/facebook/saved/views/SavedDashboardSectionHeaderView;->a()V

    .line 2252785
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2252780
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2252781
    invoke-direct {p0}, Lcom/facebook/saved/views/SavedDashboardSectionHeaderView;->a()V

    .line 2252782
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2252773
    const v0, 0x7f031264

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2252774
    const v0, 0x7f0d2b39

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSectionHeaderView;->a:Landroid/widget/TextView;

    .line 2252775
    return-void
.end method


# virtual methods
.method public final a(LX/FVx;)V
    .locals 2
    .param p1    # LX/FVx;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2252776
    iget-object v0, p0, Lcom/facebook/saved/views/SavedDashboardSectionHeaderView;->a:Landroid/widget/TextView;

    .line 2252777
    iget-object v1, p1, LX/FVx;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2252778
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2252779
    return-void
.end method
