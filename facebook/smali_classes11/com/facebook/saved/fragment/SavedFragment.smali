.class public Lcom/facebook/saved/fragment/SavedFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/16H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/FWh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/FWL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Sk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/FWS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/FWU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/FVb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/FW6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/FVV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private j:LX/FW5;

.field private k:LX/FVU;

.field private l:Z

.field public m:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2251655
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2251565
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v11

    move-object v2, p0

    check-cast v2, Lcom/facebook/saved/fragment/SavedFragment;

    invoke-static {v11}, LX/16H;->a(LX/0QB;)LX/16H;

    move-result-object v3

    check-cast v3, LX/16H;

    invoke-static {v11}, LX/FWh;->a(LX/0QB;)LX/FWh;

    move-result-object v4

    check-cast v4, LX/FWh;

    invoke-static {v11}, LX/FWL;->a(LX/0QB;)LX/FWL;

    move-result-object v5

    check-cast v5, LX/FWL;

    invoke-static {v11}, LX/1Sk;->a(LX/0QB;)LX/1Sk;

    move-result-object v6

    check-cast v6, LX/1Sk;

    const-class v7, LX/FWS;

    invoke-interface {v11, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/FWS;

    invoke-static {v11}, LX/FWU;->a(LX/0QB;)LX/FWU;

    move-result-object v8

    check-cast v8, LX/FWU;

    invoke-static {v11}, LX/FVb;->a(LX/0QB;)LX/FVb;

    move-result-object v9

    check-cast v9, LX/FVb;

    const-class v10, LX/FW6;

    invoke-interface {v11, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/FW6;

    const-class v0, LX/FVV;

    invoke-interface {v11, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/FVV;

    iput-object v3, v2, Lcom/facebook/saved/fragment/SavedFragment;->a:LX/16H;

    iput-object v4, v2, Lcom/facebook/saved/fragment/SavedFragment;->b:LX/FWh;

    iput-object v5, v2, Lcom/facebook/saved/fragment/SavedFragment;->c:LX/FWL;

    iput-object v6, v2, Lcom/facebook/saved/fragment/SavedFragment;->d:LX/1Sk;

    iput-object v7, v2, Lcom/facebook/saved/fragment/SavedFragment;->e:LX/FWS;

    iput-object v8, v2, Lcom/facebook/saved/fragment/SavedFragment;->f:LX/FWU;

    iput-object v9, v2, Lcom/facebook/saved/fragment/SavedFragment;->g:LX/FVb;

    iput-object v10, v2, Lcom/facebook/saved/fragment/SavedFragment;->h:LX/FW6;

    iput-object v11, v2, Lcom/facebook/saved/fragment/SavedFragment;->i:LX/FVV;

    .line 2251566
    iget-object v0, p0, Lcom/facebook/saved/fragment/SavedFragment;->b:LX/FWh;

    .line 2251567
    iget-object v3, v0, LX/FWh;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v3

    iput-wide v3, v0, LX/FWh;->d:J

    .line 2251568
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2251569
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2251570
    const-string v1, "extra_section_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2251571
    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 2251572
    const-string v0, "extra_section_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2251573
    :cond_0
    invoke-static {v0}, LX/FWL;->a(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/fragment/SavedFragment;->m:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 2251574
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2251575
    const-string v1, "extra_referer"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/fragment/SavedFragment;->n:Ljava/lang/String;

    .line 2251576
    iget-object v0, p0, Lcom/facebook/saved/fragment/SavedFragment;->e:LX/FWS;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/FWS;->a(Landroid/content/Context;)LX/FWR;

    move-result-object v0

    invoke-virtual {v0}, LX/FWR;->a()V

    .line 2251577
    if-nez p1, :cond_1

    .line 2251578
    iget-object v0, p0, Lcom/facebook/saved/fragment/SavedFragment;->a:LX/16H;

    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedFragment;->n:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/saved/fragment/SavedFragment;->m:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 2251579
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "saved_dashboard_imp"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "saved_dashboard"

    .line 2251580
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2251581
    move-object v3, v3

    .line 2251582
    const-string v4, "referrer_name"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "section_type"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "event_id"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2251583
    iget-object v4, v0, LX/16H;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2251584
    :cond_1
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 9

    .prologue
    .line 2251643
    iget-object v0, p0, Lcom/facebook/saved/fragment/SavedFragment;->f:LX/FWU;

    .line 2251644
    iget-object v1, v0, LX/FWU;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FWX;

    move-object v0, v1

    .line 2251645
    if-eqz v0, :cond_0

    .line 2251646
    const/4 v1, -0x1

    if-eq p2, v1, :cond_1

    .line 2251647
    :cond_0
    :goto_0
    return-void

    .line 2251648
    :cond_1
    const-string v1, "publishReviewParams"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/protocol/PostReviewParams;

    .line 2251649
    if-nez v1, :cond_2

    .line 2251650
    iget-object v1, v0, LX/FWX;->e:LX/0kL;

    new-instance v2, LX/27k;

    iget-object v3, v0, LX/FWX;->b:Landroid/content/res/Resources;

    const v4, 0x7f0814e6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0

    .line 2251651
    :cond_2
    iget-object v2, v0, LX/FWX;->d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Lcom/facebook/composer/protocol/PostReviewParams;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2251652
    new-instance v3, LX/FWV;

    invoke-direct {v3, v0, v1}, LX/FWV;-><init>(LX/FWX;Lcom/facebook/composer/protocol/PostReviewParams;)V

    .line 2251653
    iget-object v4, v0, LX/FWX;->e:LX/0kL;

    new-instance v5, LX/27k;

    iget-object v6, v0, LX/FWX;->b:Landroid/content/res/Resources;

    const v7, 0x7f0814e4

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v5}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2251654
    iget-object v4, v0, LX/FWX;->c:LX/1Ck;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "task_key_saved_dashboard_post_review"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v7, v1, Lcom/facebook/composer/protocol/PostReviewParams;->b:J

    invoke-virtual {v5, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x7590b8f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2251641
    iput-boolean v2, p0, Lcom/facebook/saved/fragment/SavedFragment;->l:Z

    .line 2251642
    const v1, 0x7f031262

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x7459ea97

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7876f317

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2251632
    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedFragment;->g:LX/FVb;

    .line 2251633
    const/4 v2, 0x0

    iput-object v2, v1, LX/FVb;->a:LX/1ZF;

    .line 2251634
    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedFragment;->g:LX/FVb;

    .line 2251635
    iget-object v2, v1, LX/FVb;->c:LX/Ei6;

    move-object v1, v2

    .line 2251636
    iget-object v2, p0, Lcom/facebook/saved/fragment/SavedFragment;->k:LX/FVU;

    .line 2251637
    iget-object v4, v1, LX/Ei6;->a:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2251638
    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedFragment;->d:LX/1Sk;

    invoke-virtual {v1}, LX/1Sk;->c()V

    .line 2251639
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2251640
    const/16 v1, 0x2b

    const v2, 0x3ce4c6b0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7e3c4cf0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2251627
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2251628
    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedFragment;->b:LX/FWh;

    .line 2251629
    invoke-static {v1}, LX/FWh;->n(LX/FWh;)LX/11o;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2251630
    iget-object v2, v1, LX/FWh;->a:LX/11i;

    sget-object p0, LX/FWg;->a:LX/FWf;

    invoke-interface {v2, p0}, LX/11i;->d(LX/0Pq;)V

    .line 2251631
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x7907fdc3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x610e8eb5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2251610
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2251611
    iget-boolean v1, p0, Lcom/facebook/saved/fragment/SavedFragment;->l:Z

    if-nez v1, :cond_0

    .line 2251612
    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedFragment;->k:LX/FVU;

    iget-object v2, p0, Lcom/facebook/saved/fragment/SavedFragment;->m:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 2251613
    iget-object v4, v1, LX/FVU;->d:LX/FWL;

    invoke-virtual {v4, v2}, LX/FWL;->a(Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;)LX/0am;

    move-result-object v4

    sget-object v5, LX/FW7;->SAVED_ITEMS_LIST:LX/FW7;

    invoke-static {v1, v4, v5}, LX/FVU;->a(LX/FVU;LX/0am;LX/FW7;)V

    .line 2251614
    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedFragment;->g:LX/FVb;

    iget-object v2, p0, Lcom/facebook/saved/fragment/SavedFragment;->m:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 2251615
    iget-object v4, v1, LX/FVb;->e:LX/FWL;

    invoke-virtual {v4, v2}, LX/FWL;->a(Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;)LX/0am;

    move-result-object v4

    iput-object v4, v1, LX/FVb;->b:LX/0am;

    .line 2251616
    iget-object v4, v1, LX/FVb;->a:LX/1ZF;

    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v5

    iget-object v6, v1, LX/FVb;->d:Landroid/content/res/Resources;

    const v7, 0x7f020b19

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 2251617
    iput-object v6, v5, LX/108;->b:Landroid/graphics/drawable/Drawable;

    .line 2251618
    move-object v5, v5

    .line 2251619
    iget-object v6, v1, LX/FVb;->d:Landroid/content/res/Resources;

    const v7, 0x7f081ac4

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2251620
    iput-object v6, v5, LX/108;->j:Ljava/lang/String;

    .line 2251621
    move-object v5, v5

    .line 2251622
    invoke-virtual {v5}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2251623
    iget-object v4, v1, LX/FVb;->a:LX/1ZF;

    new-instance v5, LX/FVZ;

    invoke-direct {v5, v1}, LX/FVZ;-><init>(LX/FVb;)V

    invoke-interface {v4, v5}, LX/1ZF;->a(LX/63W;)V

    .line 2251624
    iget-object v4, v1, LX/FVb;->b:LX/0am;

    invoke-static {v1, v4}, LX/FVb;->a$redex0(LX/FVb;LX/0am;)V

    .line 2251625
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/saved/fragment/SavedFragment;->l:Z

    .line 2251626
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x34f2400e    # -9289714.0f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2251607
    const-string v0, "extra_section_name"

    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedFragment;->k:LX/FVU;

    invoke-virtual {v1}, LX/FVU;->a()Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2251608
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2251609
    return-void
.end method

.method public final onStart()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x69dcf439

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2251601
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2251602
    iget-object v2, p0, Lcom/facebook/saved/fragment/SavedFragment;->g:LX/FVb;

    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2251603
    iput-object v0, v2, LX/FVb;->a:LX/1ZF;

    .line 2251604
    iget-object v4, v2, LX/FVb;->a:LX/1ZF;

    iget-object v5, v2, LX/FVb;->d:Landroid/content/res/Resources;

    const p0, 0x7f081a8e

    invoke-virtual {v5, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2251605
    iget-object v4, v2, LX/FVb;->a:LX/1ZF;

    const/4 v5, 0x1

    invoke-interface {v4, v5}, LX/1ZF;->k_(Z)V

    .line 2251606
    const/16 v0, 0x2b

    const v2, -0x2307f556

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2251585
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2251586
    iget-object v0, p0, Lcom/facebook/saved/fragment/SavedFragment;->j:LX/FW5;

    if-nez v0, :cond_0

    .line 2251587
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d2b37

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2251588
    new-instance p1, LX/FW5;

    invoke-direct {p1, v0, v1}, LX/FW5;-><init>(LX/0gc;Ljava/lang/Integer;)V

    .line 2251589
    move-object v0, p1

    .line 2251590
    iput-object v0, p0, Lcom/facebook/saved/fragment/SavedFragment;->j:LX/FW5;

    .line 2251591
    :cond_0
    iget-object v0, p0, Lcom/facebook/saved/fragment/SavedFragment;->k:LX/FVU;

    if-nez v0, :cond_1

    .line 2251592
    iget-object v0, p0, Lcom/facebook/saved/fragment/SavedFragment;->i:LX/FVV;

    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedFragment;->j:LX/FW5;

    .line 2251593
    new-instance p2, LX/FVU;

    invoke-static {v0}, LX/16H;->a(LX/0QB;)LX/16H;

    move-result-object v2

    check-cast v2, LX/16H;

    invoke-static {v0}, LX/FWL;->a(LX/0QB;)LX/FWL;

    move-result-object p1

    check-cast p1, LX/FWL;

    invoke-direct {p2, v1, v2, p1}, LX/FVU;-><init>(LX/FW5;LX/16H;LX/FWL;)V

    .line 2251594
    move-object v0, p2

    .line 2251595
    iput-object v0, p0, Lcom/facebook/saved/fragment/SavedFragment;->k:LX/FVU;

    .line 2251596
    :cond_1
    iget-object v0, p0, Lcom/facebook/saved/fragment/SavedFragment;->g:LX/FVb;

    .line 2251597
    iget-object v1, v0, LX/FVb;->c:LX/Ei6;

    move-object v0, v1

    .line 2251598
    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedFragment;->k:LX/FVU;

    .line 2251599
    iget-object v2, v0, LX/Ei6;->a:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2251600
    return-void
.end method
