.class public Lcom/facebook/saved/fragment/SavedItemsListFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/FVT;


# instance fields
.field public a:LX/FWC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/FWi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/FVo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/FVL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0g8;

.field private f:Landroid/view/ViewStub;

.field private g:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

.field private h:Landroid/view/ViewStub;

.field public i:LX/FVn;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2251908
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/saved/fragment/SavedItemsListFragment;

    new-instance v0, LX/FWC;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    invoke-static {p0}, LX/FWK;->a(LX/0QB;)LX/FWK;

    move-result-object v3

    check-cast v3, LX/FWK;

    invoke-direct {v0, v1, v2, v3}, LX/FWC;-><init>(Landroid/content/res/Resources;Landroid/view/LayoutInflater;LX/FWK;)V

    move-object v1, v0

    check-cast v1, LX/FWC;

    invoke-static {p0}, LX/FWi;->a(LX/0QB;)LX/FWi;

    move-result-object v2

    check-cast v2, LX/FWi;

    const-class v3, LX/FVo;

    invoke-interface {p0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/FVo;

    invoke-static {p0}, LX/FVL;->a(LX/0QB;)LX/FVL;

    move-result-object p0

    check-cast p0, LX/FVL;

    iput-object v1, p1, Lcom/facebook/saved/fragment/SavedItemsListFragment;->a:LX/FWC;

    iput-object v2, p1, Lcom/facebook/saved/fragment/SavedItemsListFragment;->b:LX/FWi;

    iput-object v3, p1, Lcom/facebook/saved/fragment/SavedItemsListFragment;->c:LX/FVo;

    iput-object p0, p1, Lcom/facebook/saved/fragment/SavedItemsListFragment;->d:LX/FVL;

    return-void
.end method


# virtual methods
.method public final a(LX/0am;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "LX/79Y;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2251906
    iget-object v0, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->i:LX/FVn;

    invoke-virtual {v0, p1}, LX/FVn;->a(LX/0am;)V

    .line 2251907
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2251902
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2251903
    const-class v0, Lcom/facebook/saved/fragment/SavedItemsListFragment;

    invoke-static {v0, p0}, Lcom/facebook/saved/fragment/SavedItemsListFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2251904
    iget-object v0, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->c:LX/FVo;

    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->a:LX/FWC;

    invoke-virtual {v0, v1}, LX/FVo;->a(LX/FWC;)LX/FVn;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->i:LX/FVn;

    .line 2251905
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x619b1d7e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2251909
    const v0, 0x7f031260

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2251910
    new-instance v3, LX/2iI;

    const v0, 0x7f0d2b32

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    invoke-direct {v3, v0}, LX/2iI;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    iput-object v3, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->e:LX/0g8;

    .line 2251911
    const v0, 0x7f0d2b34

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->f:Landroid/view/ViewStub;

    .line 2251912
    const v0, 0x7f0d2b31    # 1.876454E38f

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    iput-object v0, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->g:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    .line 2251913
    const v0, 0x7f0d2b33

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->h:Landroid/view/ViewStub;

    .line 2251914
    const/16 v0, 0x2b

    const v3, -0x10fbf60a

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroyView()V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x4deb2042

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2251864
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2251865
    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->i:LX/FVn;

    .line 2251866
    iget-object v2, v1, LX/FVn;->q:LX/0g8;

    invoke-interface {v2}, LX/0g8;->w()V

    .line 2251867
    iget-object v2, v1, LX/FVn;->q:LX/0g8;

    iget-object v5, v1, LX/FVn;->l:LX/FVX;

    invoke-interface {v2, v5}, LX/0g8;->c(LX/0fx;)V

    .line 2251868
    iget-object v2, v1, LX/FVn;->g:LX/7kj;

    const/4 v7, 0x0

    .line 2251869
    iget-object v5, v2, LX/7kj;->c:LX/0g8;

    iget-object v6, v2, LX/7kj;->a:LX/1Kt;

    invoke-interface {v5, v6}, LX/0g8;->c(LX/0fx;)V

    .line 2251870
    iget-object v5, v2, LX/7kj;->d:Landroid/widget/BaseAdapter;

    iget-object v6, v2, LX/7kj;->b:LX/7ki;

    invoke-virtual {v5, v6}, Landroid/widget/BaseAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2251871
    iput-object v7, v2, LX/7kj;->c:LX/0g8;

    .line 2251872
    iput-object v7, v2, LX/7kj;->d:Landroid/widget/BaseAdapter;

    .line 2251873
    iget-object v2, v1, LX/FVn;->g:LX/7kj;

    iget-object v5, v1, LX/FVn;->h:LX/FWj;

    .line 2251874
    iget-object v6, v2, LX/7kj;->a:LX/1Kt;

    invoke-virtual {v6, v5}, LX/1Kt;->b(LX/1Ce;)V

    .line 2251875
    iget-object v2, v1, LX/FVn;->b:LX/FWe;

    invoke-virtual {v2}, LX/FWe;->b()V

    .line 2251876
    iget-object v2, v1, LX/FVn;->j:LX/FVS;

    const/4 v5, 0x2

    .line 2251877
    iput v5, v2, LX/FVS;->c:I

    .line 2251878
    iput v5, v2, LX/FVS;->d:I

    .line 2251879
    const/4 v5, 0x0

    iput-object v5, v2, LX/FVS;->b:LX/62l;

    .line 2251880
    const/4 v5, 0x0

    iput-boolean v5, v2, LX/FVS;->e:Z

    .line 2251881
    iget-object v2, v1, LX/FVn;->k:LX/FVp;

    .line 2251882
    iget-object v5, v2, LX/FVp;->b:Lcom/facebook/saved/views/SavedDashboardEmptyView;

    if-eqz v5, :cond_0

    .line 2251883
    iget-object v5, v2, LX/FVp;->b:Lcom/facebook/saved/views/SavedDashboardEmptyView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/facebook/saved/views/SavedDashboardEmptyView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2251884
    :cond_0
    iget-object v2, v1, LX/FVn;->l:LX/FVX;

    const/4 v5, 0x0

    .line 2251885
    iput-object v5, v2, LX/FVX;->b:Landroid/view/ViewStub;

    .line 2251886
    iput-object v5, v2, LX/FVX;->a:Landroid/view/View;

    .line 2251887
    iput-object v5, v2, LX/FVX;->c:LX/FVn;

    .line 2251888
    iget-object v2, v1, LX/FVn;->d:LX/FVO;

    .line 2251889
    iget-object v5, v2, LX/FVO;->a:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->clear()V

    .line 2251890
    iget-object v2, v1, LX/FVn;->n:LX/1B1;

    iget-object v5, v1, LX/FVn;->e:LX/FW1;

    invoke-virtual {v2, v5}, LX/1B1;->b(LX/0b4;)V

    .line 2251891
    iget-object v2, v1, LX/FVn;->o:LX/FVq;

    const/4 v6, 0x0

    .line 2251892
    iget-object v5, v2, LX/FVq;->b:LX/0Yb;

    if-eqz v5, :cond_1

    iget-object v5, v2, LX/FVq;->b:LX/0Yb;

    invoke-virtual {v5}, LX/0Yb;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2251893
    iget-object v5, v2, LX/FVq;->b:LX/0Yb;

    invoke-virtual {v5}, LX/0Yb;->c()V

    .line 2251894
    :cond_1
    iput-object v6, v2, LX/FVq;->b:LX/0Yb;

    .line 2251895
    iput-object v6, v2, LX/FVq;->c:LX/FVd;

    .line 2251896
    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->a:LX/FWC;

    .line 2251897
    iput-object v3, v1, LX/FWC;->h:LX/FWF;

    .line 2251898
    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->e:LX/0g8;

    iget-object v2, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->b:LX/FWi;

    invoke-interface {v1, v2}, LX/0g8;->c(LX/0fx;)V

    .line 2251899
    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->e:LX/0g8;

    invoke-interface {v1, v3}, LX/0g8;->a(LX/2ii;)V

    .line 2251900
    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->e:LX/0g8;

    invoke-interface {v1, v3}, LX/0g8;->a(LX/3TV;)V

    .line 2251901
    const/16 v1, 0x2b

    const v2, -0x5715ea1a

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7a4f3af5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2251857
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2251858
    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->b:LX/FWi;

    .line 2251859
    iget-object v2, v1, LX/FWi;->a:LX/195;

    invoke-virtual {v2}, LX/195;->b()V

    .line 2251860
    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->i:LX/FVn;

    .line 2251861
    iget-object v2, v1, LX/FVn;->g:LX/7kj;

    .line 2251862
    iget-object p0, v2, LX/7kj;->a:LX/1Kt;

    iget-object v1, v2, LX/7kj;->c:LX/0g8;

    invoke-virtual {p0, v1}, LX/1Kt;->c(LX/0g8;)V

    .line 2251863
    const/16 v1, 0x2b

    const v2, -0x73cc45da

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5da4db88

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2251852
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2251853
    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->i:LX/FVn;

    .line 2251854
    iget-object v2, v1, LX/FVn;->g:LX/7kj;

    .line 2251855
    iget-object p0, v2, LX/7kj;->a:LX/1Kt;

    iget-object v1, v2, LX/7kj;->c:LX/0g8;

    invoke-virtual {p0, v1}, LX/1Kt;->a(LX/0g8;)V

    .line 2251856
    const/16 v1, 0x2b

    const v2, 0x4d8fa1b6    # 3.01217472E8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2251812
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2251813
    iget-object v0, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->g:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->i:LX/FVn;

    invoke-virtual {v0, v1}, LX/62l;->setOnRefreshListener(LX/62n;)V

    .line 2251814
    iget-object v0, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->i:LX/FVn;

    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->e:LX/0g8;

    iget-object v2, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->g:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    iget-object v3, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->f:Landroid/view/ViewStub;

    iget-object v4, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->h:Landroid/view/ViewStub;

    .line 2251815
    iput-object v1, v0, LX/FVn;->q:LX/0g8;

    .line 2251816
    iget-object v5, v0, LX/FVn;->g:LX/7kj;

    iget-object v6, v0, LX/FVn;->r:LX/FWC;

    .line 2251817
    iput-object v1, v5, LX/7kj;->c:LX/0g8;

    .line 2251818
    iput-object v6, v5, LX/7kj;->d:Landroid/widget/BaseAdapter;

    .line 2251819
    iget-object p1, v5, LX/7kj;->c:LX/0g8;

    iget-object p2, v5, LX/7kj;->a:LX/1Kt;

    invoke-interface {p1, p2}, LX/0g8;->b(LX/0fx;)V

    .line 2251820
    iget-object p1, v5, LX/7kj;->d:Landroid/widget/BaseAdapter;

    iget-object p2, v5, LX/7kj;->b:LX/7ki;

    invoke-virtual {p1, p2}, Landroid/widget/BaseAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2251821
    iget-object v5, v0, LX/FVn;->g:LX/7kj;

    iget-object v6, v0, LX/FVn;->h:LX/FWj;

    .line 2251822
    iget-object p1, v5, LX/7kj;->a:LX/1Kt;

    invoke-virtual {p1, v6}, LX/1Kt;->a(LX/1Ce;)V

    .line 2251823
    iget-object v5, v0, LX/FVn;->q:LX/0g8;

    new-instance v6, LX/FVc;

    invoke-direct {v6, v0}, LX/FVc;-><init>(LX/FVn;)V

    invoke-interface {v5, v6}, LX/0g8;->b(LX/0fu;)V

    .line 2251824
    iget-object v5, v0, LX/FVn;->k:LX/FVp;

    .line 2251825
    iput-object v1, v5, LX/FVp;->a:LX/0g8;

    .line 2251826
    iput-object v3, v5, LX/FVp;->c:Landroid/view/ViewStub;

    .line 2251827
    iget-object v5, v0, LX/FVn;->j:LX/FVS;

    iget-object v6, v0, LX/FVn;->k:LX/FVp;

    .line 2251828
    iput-object v2, v5, LX/FVS;->b:LX/62l;

    .line 2251829
    iput-object v6, v5, LX/FVS;->i:LX/FVp;

    .line 2251830
    iget-object v5, v0, LX/FVn;->l:LX/FVX;

    .line 2251831
    iput-object v4, v5, LX/FVX;->b:Landroid/view/ViewStub;

    .line 2251832
    iput-object v0, v5, LX/FVX;->c:LX/FVn;

    .line 2251833
    iget-object v5, v0, LX/FVn;->q:LX/0g8;

    iget-object v6, v0, LX/FVn;->l:LX/FVX;

    invoke-interface {v5, v6}, LX/0g8;->b(LX/0fx;)V

    .line 2251834
    iget-object v5, v0, LX/FVn;->n:LX/1B1;

    new-instance v6, LX/FVi;

    invoke-direct {v6, v0}, LX/FVi;-><init>(LX/FVn;)V

    invoke-virtual {v5, v6}, LX/1B1;->a(LX/0b2;)Z

    .line 2251835
    iget-object v5, v0, LX/FVn;->n:LX/1B1;

    new-instance v6, LX/FVk;

    invoke-direct {v6, v0}, LX/FVk;-><init>(LX/FVn;)V

    invoke-virtual {v5, v6}, LX/1B1;->a(LX/0b2;)Z

    .line 2251836
    iget-object v5, v0, LX/FVn;->n:LX/1B1;

    iget-object v6, v0, LX/FVn;->e:LX/FW1;

    invoke-virtual {v5, v6}, LX/1B1;->a(LX/0b4;)V

    .line 2251837
    iget-object v5, v0, LX/FVn;->q:LX/0g8;

    iget-object v6, v0, LX/FVn;->d:LX/FVO;

    invoke-interface {v5, v6}, LX/0g8;->b(LX/0fx;)V

    .line 2251838
    iget-object v5, v0, LX/FVn;->d:LX/FVO;

    invoke-virtual {v5, v0}, LX/FVO;->a(LX/FVN;)V

    .line 2251839
    iget-object v5, v0, LX/FVn;->d:LX/FVO;

    iget-object v6, v0, LX/FVn;->r:LX/FWC;

    invoke-virtual {v5, v6}, LX/FVO;->a(LX/FVN;)V

    .line 2251840
    iget-object v5, v0, LX/FVn;->r:LX/FWC;

    new-instance v6, LX/FVf;

    invoke-direct {v6, v0}, LX/FVf;-><init>(LX/FVn;)V

    .line 2251841
    iput-object v6, v5, LX/FWC;->i:Landroid/view/View$OnClickListener;

    .line 2251842
    iget-object v5, v0, LX/FVn;->o:LX/FVq;

    new-instance v6, LX/FVd;

    invoke-direct {v6, v0}, LX/FVd;-><init>(LX/FVn;)V

    .line 2251843
    iput-object v6, v5, LX/FVq;->c:LX/FVd;

    .line 2251844
    iget-object v0, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->e:LX/0g8;

    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->b:LX/FWi;

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    .line 2251845
    iget-object v0, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->e:LX/0g8;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0g8;->c(Z)V

    .line 2251846
    iget-object v0, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->e:LX/0g8;

    iget-object v1, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->a:LX/FWC;

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2251847
    iget-object v0, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->e:LX/0g8;

    new-instance v1, LX/FWD;

    invoke-direct {v1, p0}, LX/FWD;-><init>(Lcom/facebook/saved/fragment/SavedItemsListFragment;)V

    invoke-interface {v0, v1}, LX/0g8;->a(LX/2ii;)V

    .line 2251848
    iget-object v0, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->e:LX/0g8;

    new-instance v1, LX/FWE;

    invoke-direct {v1, p0}, LX/FWE;-><init>(Lcom/facebook/saved/fragment/SavedItemsListFragment;)V

    invoke-interface {v0, v1}, LX/0g8;->a(LX/3TV;)V

    .line 2251849
    iget-object v0, p0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->a:LX/FWC;

    new-instance v1, LX/FWG;

    invoke-direct {v1, p0}, LX/FWG;-><init>(Lcom/facebook/saved/fragment/SavedItemsListFragment;)V

    .line 2251850
    iput-object v1, v0, LX/FWC;->h:LX/FWF;

    .line 2251851
    return-void
.end method
