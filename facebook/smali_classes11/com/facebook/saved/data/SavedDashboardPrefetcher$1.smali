.class public final Lcom/facebook/saved/data/SavedDashboardPrefetcher$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/FVz;


# direct methods
.method public constructor <init>(LX/FVz;)V
    .locals 0

    .prologue
    .line 2251506
    iput-object p1, p0, Lcom/facebook/saved/data/SavedDashboardPrefetcher$1;->a:LX/FVz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 2251507
    iget-object v0, p0, Lcom/facebook/saved/data/SavedDashboardPrefetcher$1;->a:LX/FVz;

    iget-object v0, v0, LX/FVz;->a:LX/FWe;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->ALL:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 2251508
    iget-object v2, v0, LX/FWe;->a:LX/1Ck;

    const-string v3, "task_key_fetch_cached_only_saved_items"

    iget-object v4, v0, LX/FWe;->b:LX/FWZ;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object p0

    invoke-virtual {v4, v1, p0}, LX/FWZ;->b(LX/0am;LX/0am;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance p0, LX/FWc;

    invoke-direct {p0, v0, v1}, LX/FWc;-><init>(LX/FWe;LX/0am;)V

    invoke-virtual {v2, v3, v4, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2251509
    return-void
.end method
