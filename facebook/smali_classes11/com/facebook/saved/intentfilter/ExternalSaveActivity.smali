.class public Lcom/facebook/saved/intentfilter/ExternalSaveActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0l6;


# instance fields
.field public p:LX/5up;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0WJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/GvB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2252065
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2252066
    return-void
.end method

.method private static a(Lcom/facebook/saved/intentfilter/ExternalSaveActivity;LX/5up;LX/0WJ;LX/GvB;)V
    .locals 0

    .prologue
    .line 2252067
    iput-object p1, p0, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;->p:LX/5up;

    iput-object p2, p0, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;->q:LX/0WJ;

    iput-object p3, p0, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;->r:LX/GvB;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;

    invoke-static {v2}, LX/5up;->a(LX/0QB;)LX/5up;

    move-result-object v0

    check-cast v0, LX/5up;

    invoke-static {v2}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v1

    check-cast v1, LX/0WJ;

    invoke-static {v2}, LX/GvB;->b(LX/0QB;)LX/GvB;

    move-result-object v2

    check-cast v2, LX/GvB;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;->a(Lcom/facebook/saved/intentfilter/ExternalSaveActivity;LX/5up;LX/0WJ;LX/GvB;)V

    return-void
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2252068
    if-nez p0, :cond_1

    .line 2252069
    :cond_0
    :goto_0
    return-object v0

    .line 2252070
    :cond_1
    sget-object v1, Landroid/util/Patterns;->WEB_URL:Ljava/util/regex/Pattern;

    .line 2252071
    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 2252072
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2252073
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2252074
    const v0, 0x7f080d2a

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2252075
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2252076
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2252077
    invoke-static {p0, p0}, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2252078
    if-eqz p1, :cond_0

    .line 2252079
    const-string v0, "has_launched_login"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;->s:Z

    .line 2252080
    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x22

    const v1, 0x6b777347

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2252081
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2252082
    iget-object v1, p0, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;->q:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2252083
    iget-boolean v1, p0, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;->s:Z

    if-nez v1, :cond_0

    .line 2252084
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;->s:Z

    .line 2252085
    iget-object v1, p0, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;->r:LX/GvB;

    invoke-virtual {v1, p0, v4}, LX/GvB;->a(Landroid/app/Activity;Z)V

    .line 2252086
    :cond_0
    const/16 v1, 0x23

    const v2, -0x19c1a76b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2252087
    :goto_0
    return-void

    .line 2252088
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2252089
    invoke-virtual {p0}, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 2252090
    if-nez v1, :cond_2

    .line 2252091
    invoke-static {v2}, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;->b(Landroid/content/Context;)V

    .line 2252092
    invoke-virtual {p0}, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;->finish()V

    .line 2252093
    const v1, -0x53af388c

    invoke-static {v1, v0}, LX/02F;->c(II)V

    goto :goto_0

    .line 2252094
    :cond_2
    const-string v3, "android.intent.extra.TEXT"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2252095
    if-nez v1, :cond_3

    .line 2252096
    invoke-static {v2}, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;->b(Landroid/content/Context;)V

    .line 2252097
    invoke-virtual {p0}, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;->finish()V

    .line 2252098
    const v1, 0x3e15ef81

    invoke-static {v1, v0}, LX/02F;->c(II)V

    goto :goto_0

    .line 2252099
    :cond_3
    const v3, 0x7f080d2b

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 2252100
    iget-object v3, p0, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;->p:LX/5up;

    const-string v4, "native_share_extension"

    const-string v5, "saved_add"

    new-instance v6, LX/FWN;

    invoke-direct {v6, p0, v2}, LX/FWN;-><init>(Lcom/facebook/saved/intentfilter/ExternalSaveActivity;Landroid/content/Context;)V

    invoke-virtual {v3, v1, v4, v5, v6}, LX/5up;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    .line 2252101
    invoke-virtual {p0}, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;->finish()V

    .line 2252102
    const v1, -0x5a297367

    invoke-static {v1, v0}, LX/02F;->c(II)V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2252103
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2252104
    const-string v0, "has_launched_login"

    iget-boolean v1, p0, Lcom/facebook/saved/intentfilter/ExternalSaveActivity;->s:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2252105
    return-void
.end method
