.class public Lcom/facebook/instantshopping/InstantShoppingDocumentFragment;
.super Lcom/facebook/richdocument/RichDocumentFragment;
.source ""


# instance fields
.field private n:LX/FAa;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2207838
    invoke-direct {p0}, Lcom/facebook/richdocument/RichDocumentFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final k()LX/ChL;
    .locals 1

    .prologue
    .line 2207839
    new-instance v0, LX/FAa;

    invoke-direct {v0}, LX/FAa;-><init>()V

    iput-object v0, p0, Lcom/facebook/instantshopping/InstantShoppingDocumentFragment;->n:LX/FAa;

    .line 2207840
    iget-object v0, p0, Lcom/facebook/instantshopping/InstantShoppingDocumentFragment;->n:LX/FAa;

    return-object v0
.end method

.method public final onViewStateRestored(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x25cd1089

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2207841
    invoke-super {p0, p1}, Lcom/facebook/richdocument/RichDocumentFragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 2207842
    iget-object v1, p0, Lcom/facebook/instantshopping/InstantShoppingDocumentFragment;->n:LX/FAa;

    .line 2207843
    if-eqz p1, :cond_0

    .line 2207844
    const-string v2, "instant_shopping_catalog_session_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2207845
    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 2207846
    iput-object v2, v1, LX/FAa;->as:Ljava/lang/String;

    .line 2207847
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x7a08a79b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
