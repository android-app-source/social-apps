.class public final Lcom/facebook/instantshopping/action/InstantShoppingActionUtils$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/GnF;


# direct methods
.method public constructor <init>(LX/GnF;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2393467
    iput-object p1, p0, Lcom/facebook/instantshopping/action/InstantShoppingActionUtils$1;->b:LX/GnF;

    iput-object p2, p0, Lcom/facebook/instantshopping/action/InstantShoppingActionUtils$1;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 2393468
    iget-object v0, p0, Lcom/facebook/instantshopping/action/InstantShoppingActionUtils$1;->b:LX/GnF;

    iget-object v1, p0, Lcom/facebook/instantshopping/action/InstantShoppingActionUtils$1;->a:Ljava/lang/String;

    .line 2393469
    :try_start_0
    iget-object v2, v0, LX/GnF;->b:LX/18V;

    iget-object v3, v0, LX/GnF;->i:LX/GnG;

    invoke-virtual {v2, v3, v1}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2393470
    :goto_0
    return-void

    .line 2393471
    :catch_0
    iget-object v2, v0, LX/GnF;->h:LX/03V;

    const-string v3, "InstantShoppingPostAction"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string p0, "Post action to postUrl: "

    invoke-direct {v4, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string p0, " failed"

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2393472
    new-instance v2, LX/GnA;

    invoke-direct {v2, v0, v1}, LX/GnA;-><init>(LX/GnF;Ljava/lang/String;)V

    .line 2393473
    iget-object v3, v0, LX/GnF;->n:LX/Go0;

    const-string v4, "instant_shopping_post_action_failure"

    invoke-virtual {v3, v4, v2}, LX/Go0;->a(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0
.end method
