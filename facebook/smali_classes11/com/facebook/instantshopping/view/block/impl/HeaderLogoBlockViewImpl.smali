.class public Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;
.super LX/Cod;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Gpv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/GpR;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/Gpv;"
    }
.end annotation


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public b:LX/GnF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Go0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/GoE;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2396324
    const-class v0, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2396325
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2396326
    const-class v0, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;

    invoke-static {v0, p0}, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;->a(Ljava/lang/Class;LX/02k;)V

    .line 2396327
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;->d()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2396328
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;

    invoke-static {p0}, LX/GnF;->a(LX/0QB;)LX/GnF;

    move-result-object v1

    check-cast v1, LX/GnF;

    invoke-static {p0}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object p0

    check-cast p0, LX/Go0;

    iput-object v1, p1, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;->b:LX/GnF;

    iput-object p0, p1, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;->c:LX/Go0;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2396329
    iget-object v0, p0, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2396330
    return-void
.end method

.method public final a(LX/CHX;)V
    .locals 2

    .prologue
    .line 2396331
    iget-object v0, p0, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v1, LX/GqL;

    invoke-direct {v1, p0, p1}, LX/GqL;-><init>(Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;LX/CHX;)V

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2396332
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 2396333
    const v0, 0x7f0d1884

    return v0
.end method

.method public f()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 2396334
    sget-object v0, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method
