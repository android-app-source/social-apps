.class public final Lcom/facebook/instantshopping/view/block/impl/InstantShoppingScrubbableGIFBlockViewImpl$DownloadTask;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Landroid/net/Uri;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GqW;


# direct methods
.method public constructor <init>(LX/GqW;)V
    .locals 0

    .prologue
    .line 2396572
    iput-object p1, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingScrubbableGIFBlockViewImpl$DownloadTask;->a:LX/GqW;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2396573
    check-cast p1, [Landroid/net/Uri;

    .line 2396574
    :try_start_0
    iget-object v0, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingScrubbableGIFBlockViewImpl$DownloadTask;->a:LX/GqW;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    .line 2396575
    new-instance v2, LX/34X;

    new-instance p0, LX/GqV;

    invoke-direct {p0, v0}, LX/GqV;-><init>(LX/GqW;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-static {p1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p1

    invoke-direct {v2, v1, p0, p1}, LX/34X;-><init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2396576
    iget-object p0, v0, LX/GqW;->b:LX/DLX;

    invoke-virtual {p0, v2}, LX/3AP;->a(LX/34X;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2396577
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2396578
    :catch_0
    move-exception v0

    .line 2396579
    sget-object v1, LX/GqW;->e:Ljava/lang/Class;

    const-string v2, "Downloading Video Failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
