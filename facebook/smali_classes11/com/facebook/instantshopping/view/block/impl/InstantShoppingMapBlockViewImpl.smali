.class public Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;
.super LX/Cos;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cos",
        "<",
        "LX/Gpg;",
        "Lcom/facebook/instantshopping/view/widget/InstantShoppingMapMediaView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final e:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:LX/697;

.field public a:LX/Go0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Gqx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/D15;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Landroid/view/View;

.field public l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public m:Lcom/facebook/instantshopping/view/widget/InstantShoppingMapView;

.field public n:F

.field public o:Lcom/facebook/resources/ui/FbTextView;

.field public p:Lcom/facebook/fbui/glyph/GlyphView;

.field public q:Ljava/lang/String;

.field public r:Lcom/facebook/storelocator/StoreLocatorRecyclerView;

.field public s:Lcom/facebook/fbui/glyph/GlyphView;

.field public t:Landroid/view/View;

.field public u:LX/Grg;

.field public v:LX/Grj;

.field public w:LX/Grn;

.field public x:LX/Grp;

.field public y:Lcom/facebook/resources/ui/FbTextView;

.field public z:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2396517
    const-class v0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/Ctg;Landroid/view/View;)V
    .locals 8

    .prologue
    .line 2396518
    invoke-direct {p0, p1, p2}, LX/Cos;-><init>(LX/Ctg;Landroid/view/View;)V

    .line 2396519
    const-class v0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;

    invoke-static {v0, p0}, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->a(Ljava/lang/Class;LX/02k;)V

    .line 2396520
    const v0, 0x7f0d1831

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->k:Landroid/view/View;

    .line 2396521
    const v0, 0x7f0d1832

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2396522
    const v0, 0x7f0d182d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/view/widget/InstantShoppingMapView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->m:Lcom/facebook/instantshopping/view/widget/InstantShoppingMapView;

    .line 2396523
    const v0, 0x7f0d1834

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 2396524
    const v0, 0x7f0d144a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->p:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2396525
    const v0, 0x7f0d182f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->s:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2396526
    const v0, 0x7f0d182e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/storelocator/StoreLocatorRecyclerView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->r:Lcom/facebook/storelocator/StoreLocatorRecyclerView;

    .line 2396527
    const v0, 0x7f0d1830

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->t:Landroid/view/View;

    .line 2396528
    const v0, 0x7f0d1836

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->y:Lcom/facebook/resources/ui/FbTextView;

    .line 2396529
    const v0, 0x7f0d1835

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->z:Lcom/facebook/resources/ui/FbTextView;

    .line 2396530
    new-instance v2, LX/Grg;

    iget-object v3, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    iget-object v4, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->r:Lcom/facebook/storelocator/StoreLocatorRecyclerView;

    invoke-direct {v2, p1, v3, v4}, LX/Grg;-><init>(LX/Ctg;Lcom/facebook/storelocator/StoreLocatorMapDelegate;Lcom/facebook/storelocator/StoreLocatorRecyclerView;)V

    iput-object v2, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->u:LX/Grg;

    .line 2396531
    new-instance v2, LX/Grj;

    iget-object v4, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    iget-object v5, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->k:Landroid/view/View;

    iget-object v6, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->m:Lcom/facebook/instantshopping/view/widget/InstantShoppingMapView;

    iget-object v7, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->p:Lcom/facebook/fbui/glyph/GlyphView;

    move-object v3, p1

    invoke-direct/range {v2 .. v7}, LX/Grj;-><init>(LX/Ctg;Lcom/facebook/storelocator/StoreLocatorMapDelegate;Landroid/view/View;Lcom/facebook/instantshopping/view/widget/InstantShoppingMapView;Lcom/facebook/fbui/glyph/GlyphView;)V

    iput-object v2, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->v:LX/Grj;

    .line 2396532
    new-instance v2, LX/Grn;

    iget-object v4, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    iget-object v5, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->k:Landroid/view/View;

    iget-object v6, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->m:Lcom/facebook/instantshopping/view/widget/InstantShoppingMapView;

    iget-object v7, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->s:Lcom/facebook/fbui/glyph/GlyphView;

    move-object v3, p1

    invoke-direct/range {v2 .. v7}, LX/Grn;-><init>(LX/Ctg;Lcom/facebook/storelocator/StoreLocatorMapDelegate;Landroid/view/View;Lcom/facebook/instantshopping/view/widget/InstantShoppingMapView;Lcom/facebook/fbui/glyph/GlyphView;)V

    iput-object v2, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->w:LX/Grn;

    .line 2396533
    new-instance v2, LX/Grp;

    iget-object v3, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    iget-object v4, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->k:Landroid/view/View;

    iget-object v5, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->t:Landroid/view/View;

    invoke-direct {v2, p1, v3, v4, v5}, LX/Grp;-><init>(LX/Ctg;Lcom/facebook/storelocator/StoreLocatorMapDelegate;Landroid/view/View;Landroid/view/View;)V

    iput-object v2, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->x:LX/Grp;

    .line 2396534
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1c2f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->n:F

    .line 2396535
    iget-object v0, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->m:Lcom/facebook/instantshopping/view/widget/InstantShoppingMapView;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingMapView;->requestLayout()V

    .line 2396536
    iget-object v0, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->m:Lcom/facebook/instantshopping/view/widget/InstantShoppingMapView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/MapView;->a(Landroid/os/Bundle;)V

    .line 2396537
    iget-object v0, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->requestLayout()V

    .line 2396538
    iget-object v0, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    new-instance v1, LX/GqS;

    invoke-direct {v1, p0}, LX/GqS;-><init>(Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;)V

    .line 2396539
    iput-object v1, v0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->y:LX/D17;

    .line 2396540
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;

    invoke-static {p0}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object v1

    check-cast v1, LX/Go0;

    invoke-static {p0}, LX/Gqx;->b(LX/0QB;)LX/Gqx;

    move-result-object v2

    check-cast v2, LX/Gqx;

    invoke-static {p0}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a(LX/0QB;)Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    move-result-object v3

    check-cast v3, Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    invoke-static {p0}, LX/D15;->a(LX/0QB;)LX/D15;

    move-result-object p0

    check-cast p0, LX/D15;

    iput-object v1, p1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->a:LX/Go0;

    iput-object v2, p1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->b:LX/Gqx;

    iput-object v3, p1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    iput-object p0, p1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->d:LX/D15;

    return-void
.end method


# virtual methods
.method public final a(LX/Ctg;LX/CrN;Z)LX/CqX;
    .locals 7

    .prologue
    .line 2396541
    iget-object v0, p0, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->b:LX/Gqx;

    sget-object v1, LX/CrN;->ASPECT_FIT:LX/CrN;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v3, p1

    move v4, p3

    invoke-virtual/range {v0 .. v6}, LX/Gqx;->a(LX/CrN;Landroid/content/Context;LX/Ctg;ZZZ)LX/CqX;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2396542
    invoke-super {p0, p1}, LX/Cos;->a(Landroid/os/Bundle;)V

    .line 2396543
    const-class p1, LX/Grg;

    invoke-virtual {p0, p1}, LX/Cos;->b(Ljava/lang/Class;)V

    .line 2396544
    const-class p1, LX/Grj;

    invoke-virtual {p0, p1}, LX/Cos;->b(Ljava/lang/Class;)V

    .line 2396545
    const-class p1, LX/Grn;

    invoke-virtual {p0, p1}, LX/Cos;->b(Ljava/lang/Class;)V

    .line 2396546
    const-class p1, LX/Grp;

    invoke-virtual {p0, p1}, LX/Cos;->b(Ljava/lang/Class;)V

    .line 2396547
    return-void
.end method
