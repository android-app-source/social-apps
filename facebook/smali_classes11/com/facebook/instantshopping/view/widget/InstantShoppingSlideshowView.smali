.class public Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;
.super Lcom/facebook/richdocument/view/widget/SlideshowView;
.source ""


# instance fields
.field public o:LX/CIh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/Go0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/CjL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/GrF;

.field public t:I

.field private u:LX/Clo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2397735
    invoke-direct {p0, p1}, Lcom/facebook/richdocument/view/widget/SlideshowView;-><init>(Landroid/content/Context;)V

    .line 2397736
    const-class v0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;

    invoke-static {v0, p0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2397737
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2397781
    invoke-direct {p0, p1, p2}, Lcom/facebook/richdocument/view/widget/SlideshowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2397782
    const-class v0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;

    invoke-static {v0, p0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2397783
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2397779
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/richdocument/view/widget/SlideshowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2397780
    return-void
.end method

.method private a(LX/Clo;)F
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2397763
    invoke-virtual {p1}, LX/Clo;->d()I

    move-result v0

    if-nez v0, :cond_0

    .line 2397764
    const/4 v0, 0x0

    .line 2397765
    :goto_0
    return v0

    .line 2397766
    :cond_0
    invoke-virtual {p1}, LX/Clo;->d()I

    move-result v0

    new-array v3, v0, [F

    move v0, v1

    .line 2397767
    :goto_1
    invoke-virtual {p1}, LX/Clo;->d()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2397768
    invoke-virtual {p1, v0}, LX/Clo;->a(I)LX/Clr;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->a(LX/Clr;)F

    move-result v2

    aput v2, v3, v0

    .line 2397769
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2397770
    :cond_1
    invoke-static {p1}, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->b(LX/Clo;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2397771
    invoke-static {v3}, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->a([F)F

    move-result v0

    goto :goto_0

    .line 2397772
    :cond_2
    invoke-virtual {p1}, LX/Clo;->d()I

    move-result v0

    new-array v4, v0, [I

    move v0, v1

    move v2, v1

    .line 2397773
    :goto_2
    invoke-virtual {p1}, LX/Clo;->d()I

    move-result v5

    if-ge v1, v5, :cond_4

    .line 2397774
    aget v5, v3, v1

    sget-wide v6, LX/CoL;->n:D

    double-to-float v6, v6

    invoke-static {v3, v5, v6}, Lcom/facebook/richdocument/view/widget/SlideshowView;->a([FFF)I

    move-result v5

    aput v5, v4, v1

    .line 2397775
    aget v5, v4, v1

    if-le v5, v2, :cond_3

    .line 2397776
    aget v2, v4, v1

    move v0, v1

    .line 2397777
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2397778
    :cond_4
    aget v0, v3, v0

    goto :goto_0
.end method

.method private a(LX/Clr;)F
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2397754
    instance-of v0, p1, LX/Clw;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, LX/Clw;

    invoke-interface {v0}, LX/Clw;->a()LX/8Yr;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, LX/Clw;

    invoke-interface {v0}, LX/Clw;->a()LX/8Yr;

    move-result-object v0

    invoke-interface {v0}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 2397755
    check-cast v0, LX/Clw;

    invoke-interface {v0}, LX/Clw;->a()LX/8Yr;

    move-result-object v0

    invoke-interface {v0}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v2

    .line 2397756
    invoke-direct {p0, p1}, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->b(LX/Clr;)F

    move-result v3

    .line 2397757
    cmpl-float v0, v3, v1

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    .line 2397758
    :goto_0
    invoke-interface {v2}, LX/1Fb;->a()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v1, v3

    int-to-float v0, v0

    add-float/2addr v0, v1

    .line 2397759
    invoke-interface {v2}, LX/1Fb;->c()I

    move-result v1

    int-to-float v1, v1

    div-float v0, v1, v0

    .line 2397760
    :goto_1
    return v0

    .line 2397761
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2397762
    goto :goto_1
.end method

.method private static a([F)F
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2397749
    aget v1, p0, v0

    .line 2397750
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 2397751
    aget v2, p0, v0

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 2397752
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2397753
    :cond_0
    return v1
.end method

.method private a(LX/Clf;Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2397739
    new-instance v0, Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;)V

    .line 2397740
    iget-object v1, p1, LX/Clf;->a:Ljava/lang/CharSequence;

    move-object v1, v1

    .line 2397741
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2397742
    if-eqz p2, :cond_0

    .line 2397743
    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->o:LX/CIh;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;->LEFT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;

    invoke-virtual {v1, v0, v2, p2}, LX/CIh;->a(Landroid/widget/TextView;Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;)V

    .line 2397744
    :cond_0
    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->p:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x41a00000    # 20.0f

    invoke-static {v2, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    .line 2397745
    const/high16 v2, -0x80000000

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 2397746
    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 2397747
    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/text/BetterTextView;->measure(II)V

    .line 2397748
    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method private static a(Landroid/content/Context;LX/GoW;)LX/Clf;
    .locals 2

    .prologue
    .line 2397738
    new-instance v0, LX/GoU;

    invoke-virtual {p1}, LX/GoW;->f()LX/GoE;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/GoU;-><init>(Landroid/content/Context;LX/GoE;)V

    invoke-virtual {v0, p1}, LX/Cle;->a(LX/GoW;)LX/Cle;

    move-result-object v0

    invoke-virtual {v0}, LX/Cle;->a()LX/Clf;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;LX/CIh;LX/0hB;LX/Go0;LX/CjL;)V
    .locals 0

    .prologue
    .line 2397734
    iput-object p1, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->o:LX/CIh;

    iput-object p2, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->p:LX/0hB;

    iput-object p3, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->q:LX/Go0;

    iput-object p4, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->r:LX/CjL;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;

    invoke-static {v3}, LX/CIh;->b(LX/0QB;)LX/CIh;

    move-result-object v0

    check-cast v0, LX/CIh;

    invoke-static {v3}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v1

    check-cast v1, LX/0hB;

    invoke-static {v3}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object v2

    check-cast v2, LX/Go0;

    const-class v4, LX/CjL;

    invoke-interface {v3, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/CjL;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->a(Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;LX/CIh;LX/0hB;LX/Go0;LX/CjL;)V

    return-void
.end method

.method private b(LX/Clr;)F
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2397694
    instance-of v0, p1, LX/Gon;

    if-nez v0, :cond_0

    move v0, v1

    .line 2397695
    :goto_0
    return v0

    :cond_0
    move-object v0, p1

    .line 2397696
    check-cast v0, LX/Gon;

    invoke-interface {v0}, LX/Gon;->K()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, LX/Gon;

    invoke-interface {v0}, LX/Gon;->K()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    .line 2397697
    goto :goto_0

    .line 2397698
    :cond_2
    check-cast p1, LX/Gon;

    invoke-interface {p1}, LX/Gon;->K()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CHZ;

    .line 2397699
    new-instance v1, LX/GoW;

    invoke-direct {v1, v0}, LX/GoW;-><init>(LX/CHZ;)V

    .line 2397700
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->a(Landroid/content/Context;LX/GoW;)LX/Clf;

    move-result-object v0

    .line 2397701
    invoke-virtual {v1}, LX/GoW;->g()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->a(LX/Clf;Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;)I

    move-result v0

    int-to-float v0, v0

    goto :goto_0
.end method

.method private static b(LX/Clo;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2397723
    invoke-virtual {p0}, LX/Clo;->d()I

    move-result v4

    move v3, v2

    .line 2397724
    :goto_0
    if-ge v3, v4, :cond_2

    .line 2397725
    invoke-virtual {p0, v3}, LX/Clo;->a(I)LX/Clr;

    move-result-object v0

    .line 2397726
    instance-of v5, v0, LX/Gon;

    if-eqz v5, :cond_1

    .line 2397727
    check-cast v0, LX/Gon;

    .line 2397728
    invoke-interface {v0}, LX/Gon;->K()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, LX/Gon;->K()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v0}, LX/Gon;->L()Ljava/util/List;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->FIT_TO_HEIGHT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2397729
    :goto_1
    if-nez v0, :cond_1

    move v0, v1

    .line 2397730
    :goto_2
    return v0

    :cond_0
    move v0, v2

    .line 2397731
    goto :goto_1

    .line 2397732
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 2397733
    goto :goto_2
.end method


# virtual methods
.method public final b(Z)V
    .locals 3

    .prologue
    .line 2397716
    iget v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->t:I

    iget v1, p0, Lcom/facebook/richdocument/view/widget/SlideshowView;->l:I

    if-le v0, v1, :cond_1

    if-nez p1, :cond_1

    .line 2397717
    iget v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->t:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->t:I

    .line 2397718
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->q:LX/Go0;

    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->u:LX/Clo;

    iget v2, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->t:I

    invoke-virtual {v0, v2}, LX/Clo;->a(I)LX/Clr;

    move-result-object v0

    check-cast v0, LX/Gob;

    invoke-interface {v0}, LX/GoY;->D()LX/GoE;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Go0;->a(LX/GoE;)V

    .line 2397719
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->s:LX/GrF;

    iget v1, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->t:I

    invoke-interface {v0, v1}, LX/GrF;->a(I)V

    .line 2397720
    return-void

    .line 2397721
    :cond_1
    iget v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->t:I

    iget v1, p0, Lcom/facebook/richdocument/view/widget/SlideshowView;->m:I

    if-ge v0, v1, :cond_0

    if-eqz p1, :cond_0

    .line 2397722
    iget v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->t:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->t:I

    goto :goto_0
.end method

.method public getCurrentSnappedSlide()I
    .locals 1

    .prologue
    .line 2397715
    iget v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->t:I

    return v0
.end method

.method public final o()V
    .locals 1

    .prologue
    .line 2397713
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->t:I

    .line 2397714
    return-void
.end method

.method public setCurrentSnappedSlide(I)V
    .locals 0

    .prologue
    .line 2397711
    iput p1, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->t:I

    .line 2397712
    return-void
.end method

.method public setInstantShoppingSlides(LX/Clo;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2397704
    iput-object p1, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->u:LX/Clo;

    .line 2397705
    new-instance v0, LX/CoJ;

    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->r:LX/CjL;

    invoke-virtual {v2, v6}, LX/CjL;->a(LX/0Pq;)LX/CjK;

    move-result-object v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v4

    check-cast v4, LX/1P1;

    move-object v2, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, LX/CoJ;-><init>(Landroid/content/Context;LX/Clo;LX/CjK;LX/1P1;Landroid/support/v7/widget/RecyclerView;LX/0Pq;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2397706
    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->q:LX/Go0;

    iget v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->t:I

    invoke-virtual {p1, v0}, LX/Clo;->a(I)LX/Clr;

    move-result-object v0

    check-cast v0, LX/Gob;

    invoke-interface {v0}, LX/GoY;->D()LX/GoE;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Go0;->a(LX/GoE;)V

    .line 2397707
    invoke-direct {p0, p1}, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->a(LX/Clo;)F

    move-result v0

    .line 2397708
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/SlideshowView;->k:LX/Ctm;

    .line 2397709
    iput v0, v1, LX/Ctm;->b:F

    .line 2397710
    return-void
.end method

.method public setSlideChangeListener(LX/GrF;)V
    .locals 0

    .prologue
    .line 2397702
    iput-object p1, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->s:LX/GrF;

    .line 2397703
    return-void
.end method
