.class public Lcom/facebook/instantshopping/view/widget/PickerItemColorView;
.super Lcom/facebook/drawee/fbpipeline/FbDraweeView;
.source ""


# instance fields
.field public c:I

.field public d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2397884
    invoke-direct {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2397885
    invoke-direct {p0}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->d()V

    .line 2397886
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2397881
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2397882
    invoke-direct {p0}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->d()V

    .line 2397883
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2397878
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2397879
    invoke-direct {p0}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->d()V

    .line 2397880
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 2397876
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->setIsSelected(Z)V

    .line 2397877
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 3

    .prologue
    .line 2397867
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 2397868
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    .line 2397869
    iget-object v2, v0, LX/1af;->c:LX/4Ab;

    move-object v2, v2

    .line 2397870
    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {v2, v1, v0}, LX/4Ab;->a(IF)LX/4Ab;

    .line 2397871
    const/4 v0, 0x1

    .line 2397872
    iput-boolean v0, v2, LX/4Ab;->b:Z

    .line 2397873
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, v2}, LX/1af;->a(LX/4Ab;)V

    .line 2397874
    const v0, 0xffffff

    iput v0, p0, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->c:I

    .line 2397875
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 2397863
    invoke-super/range {p0 .. p5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->onLayout(ZIIII)V

    .line 2397864
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->getHeight()I

    move-result v0

    if-gtz v0, :cond_0

    .line 2397865
    :goto_0
    return-void

    .line 2397866
    :cond_0
    iget v0, p0, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->c:I

    invoke-virtual {p0, v0}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->setColorFilter(I)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 2397859
    invoke-super {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->onMeasure(II)V

    .line 2397860
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->d:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-static {v1, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    sub-int/2addr v0, v1

    .line 2397861
    invoke-virtual {p0, v0, v0}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->setMeasuredDimension(II)V

    .line 2397862
    return-void
.end method

.method public setColor(I)V
    .locals 0

    .prologue
    .line 2397857
    iput p1, p0, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->c:I

    .line 2397858
    return-void
.end method

.method public setIsSelected(Z)V
    .locals 3

    .prologue
    .line 2397846
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a07bb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v1, v0

    .line 2397847
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    .line 2397848
    iget-object v2, v0, LX/1af;->c:LX/4Ab;

    move-object v2, v2

    .line 2397849
    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {v2, v1, v0}, LX/4Ab;->a(IF)LX/4Ab;

    .line 2397850
    const/4 v0, 0x1

    .line 2397851
    iput-boolean v0, v2, LX/4Ab;->b:Z

    .line 2397852
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, v2}, LX/1af;->a(LX/4Ab;)V

    .line 2397853
    return-void

    .line 2397854
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a004f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v1, v0

    goto :goto_0
.end method

.method public setSizeOffset(I)V
    .locals 0

    .prologue
    .line 2397855
    iput p1, p0, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->d:I

    .line 2397856
    return-void
.end method
