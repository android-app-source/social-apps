.class public Lcom/facebook/instantshopping/view/widget/ColorPickerPileImageItemLayout;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Lcom/facebook/instantshopping/view/widget/PickerItemImageView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2397242
    const-class v0, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/instantshopping/view/widget/ColorPickerPileImageItemLayout;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2397240
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2397241
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2397238
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2397239
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2397236
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2397237
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2397232
    const v0, 0x7f0d1814

    invoke-virtual {p0, v0}, Lcom/facebook/instantshopping/view/widget/ColorPickerPileImageItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/view/widget/PickerItemImageView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerPileImageItemLayout;->b:Lcom/facebook/instantshopping/view/widget/PickerItemImageView;

    .line 2397233
    return-void
.end method

.method public setImageUrl(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2397234
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerPileImageItemLayout;->b:Lcom/facebook/instantshopping/view/widget/PickerItemImageView;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/instantshopping/view/widget/ColorPickerPileImageItemLayout;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2397235
    return-void
.end method
