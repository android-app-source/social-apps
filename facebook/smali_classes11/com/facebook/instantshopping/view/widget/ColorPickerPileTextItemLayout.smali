.class public Lcom/facebook/instantshopping/view/widget/ColorPickerPileTextItemLayout;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2397243
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2397244
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2397245
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2397246
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2397247
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2397248
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2397249
    const v0, 0x7f0d1816

    invoke-virtual {p0, v0}, Lcom/facebook/instantshopping/view/widget/ColorPickerPileTextItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerPileTextItemLayout;->a:Landroid/widget/TextView;

    .line 2397250
    return-void
.end method

.method public setMoreText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2397251
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerPileTextItemLayout;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2397252
    return-void
.end method
