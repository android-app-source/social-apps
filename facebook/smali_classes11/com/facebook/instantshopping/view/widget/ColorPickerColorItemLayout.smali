.class public Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements LX/Gpu;


# instance fields
.field private a:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

.field private b:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

.field private c:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

.field public d:LX/CHX;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2397174
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2397175
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2397176
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2397177
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2397178
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2397179
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2397180
    const v0, 0x7f0d1811

    invoke-virtual {p0, v0}, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->a:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    .line 2397181
    const v0, 0x7f0d1813

    invoke-virtual {p0, v0}, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->b:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    .line 2397182
    const v0, 0x7f0d1812

    invoke-virtual {p0, v0}, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->c:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    .line 2397183
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->b:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    const/4 v1, 0x2

    .line 2397184
    iput v1, v0, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->d:I

    .line 2397185
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->b:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->c()V

    .line 2397186
    return-void
.end method

.method public getAction()LX/CHX;
    .locals 1

    .prologue
    .line 2397187
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->d:LX/CHX;

    return-object v0
.end method

.method public setAction(LX/CHX;)V
    .locals 0

    .prologue
    .line 2397188
    iput-object p1, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->d:LX/CHX;

    .line 2397189
    return-void
.end method

.method public setColor(I)V
    .locals 1

    .prologue
    .line 2397190
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->a:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    .line 2397191
    iput p1, v0, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->c:I

    .line 2397192
    return-void
.end method

.method public setDisabled(Z)V
    .locals 2

    .prologue
    .line 2397193
    iput-boolean p1, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->e:Z

    .line 2397194
    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->c:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    iget-boolean v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->setVisibility(I)V

    .line 2397195
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->c:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getTopLevelDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0xdc

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2397196
    return-void

    .line 2397197
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setIsSelected(Z)V
    .locals 2

    .prologue
    .line 2397198
    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->b:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->setVisibility(I)V

    .line 2397199
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->a:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    invoke-virtual {v0, p1}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->setIsSelected(Z)V

    .line 2397200
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->c:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    invoke-virtual {v0, p1}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->setIsSelected(Z)V

    .line 2397201
    return-void

    .line 2397202
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
