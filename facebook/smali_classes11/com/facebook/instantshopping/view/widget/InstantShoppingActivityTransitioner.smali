.class public Lcom/facebook/instantshopping/view/widget/InstantShoppingActivityTransitioner;
.super Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;
.source ""


# instance fields
.field public g:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2397329
    invoke-direct {p0, p1}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;-><init>(Landroid/content/Context;)V

    .line 2397330
    const-class v0, Lcom/facebook/instantshopping/view/widget/InstantShoppingActivityTransitioner;

    invoke-static {v0, p0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingActivityTransitioner;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2397331
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2397339
    invoke-direct {p0, p1, p2}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2397340
    const-class v0, Lcom/facebook/instantshopping/view/widget/InstantShoppingActivityTransitioner;

    invoke-static {v0, p0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingActivityTransitioner;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2397341
    invoke-direct {p0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingActivityTransitioner;->f()V

    .line 2397342
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2397335
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2397336
    const-class v0, Lcom/facebook/instantshopping/view/widget/InstantShoppingActivityTransitioner;

    invoke-static {v0, p0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingActivityTransitioner;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2397337
    invoke-direct {p0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingActivityTransitioner;->f()V

    .line 2397338
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingActivityTransitioner;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingActivityTransitioner;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingActivityTransitioner;->g:LX/0ad;

    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2397332
    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingActivityTransitioner;->g:LX/0ad;

    sget-short v2, LX/CHN;->b:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 2397333
    :cond_0
    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->u:Z

    .line 2397334
    return-void
.end method
