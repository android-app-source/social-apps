.class public Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements LX/Gpu;


# instance fields
.field private a:LX/CtG;

.field public b:LX/CHX;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2397274
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2397275
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2397272
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2397273
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2397270
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2397271
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2397266
    const v0, 0x7f0d1818

    invoke-virtual {p0, v0}, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2397267
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 2397268
    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;->a:LX/CtG;

    .line 2397269
    return-void
.end method

.method public getAction()LX/CHX;
    .locals 1

    .prologue
    .line 2397265
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;->b:LX/CHX;

    return-object v0
.end method

.method public setAction(LX/CHX;)V
    .locals 0

    .prologue
    .line 2397253
    iput-object p1, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;->b:LX/CHX;

    .line 2397254
    return-void
.end method

.method public setDisabled(Z)V
    .locals 3

    .prologue
    .line 2397260
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;->a:LX/CtG;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/CtG;->setClickable(Z)V

    .line 2397261
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;->a:LX/CtG;

    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a07b9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/CtG;->setTextColor(I)V

    .line 2397262
    const v0, 0x7f020db3

    invoke-virtual {p0, v0}, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;->setBackgroundResource(I)V

    .line 2397263
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;->a:LX/CtG;

    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;->a:LX/CtG;

    invoke-virtual {v1}, LX/CtG;->getPaintFlags()I

    move-result v1

    or-int/lit8 v1, v1, 0x10

    invoke-virtual {v0, v1}, LX/CtG;->setPaintFlags(I)V

    .line 2397264
    return-void
.end method

.method public setIsSelected(Z)V
    .locals 3

    .prologue
    .line 2397257
    const v0, 0x7f020db7

    invoke-virtual {p0, v0}, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;->setBackgroundResource(I)V

    .line 2397258
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;->a:LX/CtG;

    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/CtG;->setTextColor(I)V

    .line 2397259
    return-void
.end method

.method public setOptionText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2397255
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;->a:LX/CtG;

    invoke-virtual {v0, p1}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 2397256
    return-void
.end method
