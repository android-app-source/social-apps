.class public Lcom/facebook/instantshopping/view/widget/InstantShoppingDividerView;
.super Landroid/view/View;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2397343
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2397344
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2397345
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2397346
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2397347
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2397348
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 2397349
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 2397350
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingDividerView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingDividerView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v1, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/instantshopping/view/widget/InstantShoppingDividerView;->setMeasuredDimension(II)V

    .line 2397351
    return-void
.end method
