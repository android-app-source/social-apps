.class public Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;
.super LX/Cts;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private d:LX/Ctg;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2397276
    const-class v0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/Ctg;)V
    .locals 2

    .prologue
    .line 2397277
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 2397278
    iput-object p1, p0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->d:LX/Ctg;

    .line 2397279
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->d:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v0

    const v1, 0x7f0d1829

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2397280
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2397281
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2397282
    return-void
.end method

.method public final a(LX/CrS;)V
    .locals 7

    .prologue
    .line 2397283
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v0, :cond_0

    .line 2397284
    :goto_0
    return-void

    .line 2397285
    :cond_0
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2397286
    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->d:LX/Ctg;

    invoke-interface {v1}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v1

    invoke-interface {v1}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v1

    invoke-static {p1, v1}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v1

    .line 2397287
    iget-object v2, v1, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v1, v2

    .line 2397288
    new-instance v2, Landroid/graphics/Rect;

    iget v3, v1, Landroid/graphics/Rect;->left:I

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v3, v4

    iget v4, v1, Landroid/graphics/Rect;->top:I

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v4, v5

    iget v5, v1, Landroid/graphics/Rect;->left:I

    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v5, v6

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    invoke-direct {v2, v3, v4, v5, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2397289
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->d:LX/Ctg;

    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-interface {v0, v1, v2}, LX/Ctg;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel;)V
    .locals 6

    .prologue
    .line 2397290
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel;->jp_()LX/1Fb;

    move-result-object v0

    .line 2397291
    if-nez v0, :cond_1

    .line 2397292
    :cond_0
    :goto_0
    return-void

    .line 2397293
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel;->b()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel$ElementDescriptorModel;

    move-result-object v5

    .line 2397294
    invoke-interface {v0}, LX/1Fb;->c()I

    move-result v1

    .line 2397295
    invoke-interface {v0}, LX/1Fb;->a()I

    move-result v2

    .line 2397296
    iget-object v3, p0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v4, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2397297
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2397298
    if-nez v0, :cond_2

    .line 2397299
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    move-object v4, v0

    .line 2397300
    :goto_1
    if-eqz v5, :cond_0

    .line 2397301
    invoke-virtual {v5}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel$ElementDescriptorModel;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2397302
    invoke-virtual {v5}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel$ElementDescriptorModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 2397303
    :goto_2
    invoke-virtual {v5}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel$ElementDescriptorModel;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2397304
    invoke-virtual {v5}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel$ElementDescriptorModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 2397305
    :goto_3
    invoke-virtual {v5}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel$ElementDescriptorModel;->e()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 2397306
    invoke-virtual {v5}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel$ElementDescriptorModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 2397307
    :goto_4
    invoke-virtual {v5}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel$ElementDescriptorModel;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 2397308
    invoke-virtual {v5}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel$ElementDescriptorModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 2397309
    :goto_5
    invoke-virtual {v4, v0, v2, v1, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2397310
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2397311
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v5}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel$ElementDescriptorModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2397312
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2397313
    :goto_6
    goto :goto_0

    .line 2397314
    :cond_2
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2397315
    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    move-object v4, v0

    goto :goto_1

    .line 2397316
    :cond_3
    iget v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_2

    .line 2397317
    :cond_4
    iget v1, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto :goto_3

    .line 2397318
    :cond_5
    iget v2, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto :goto_4

    .line 2397319
    :cond_6
    iget v3, v4, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto :goto_5

    .line 2397320
    :cond_7
    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2397321
    :goto_7
    :try_start_0
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2397322
    invoke-virtual {v0, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_6

    .line 2397323
    :catch_0
    move-exception v2

    .line 2397324
    iget-object v3, p0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->a:LX/03V;

    const-string v4, "ImageOverlayPlugin.setBackgroundColor"

    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 2397325
    :cond_8
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_7
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 2397326
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v0, :cond_0

    .line 2397327
    :goto_0
    return-void

    .line 2397328
    :cond_0
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method
