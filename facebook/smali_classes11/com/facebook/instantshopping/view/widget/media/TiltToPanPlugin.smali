.class public Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;
.super LX/Cts;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# instance fields
.field public a:LX/Ctg;

.field public b:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

.field public c:Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

.field private final d:Landroid/view/Display;

.field public final e:Landroid/graphics/Point;

.field public f:Z


# direct methods
.method public constructor <init>(LX/Ctg;)V
    .locals 2

    .prologue
    .line 2398788
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 2398789
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->f:Z

    .line 2398790
    iput-object p1, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->a:LX/Ctg;

    .line 2398791
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->a:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v0

    const v1, 0x7f0d16c3

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->b:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    .line 2398792
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->a:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v0

    const v1, 0x7f0d182b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->c:Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

    .line 2398793
    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 2398794
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->d:Landroid/view/Display;

    .line 2398795
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->e:Landroid/graphics/Point;

    .line 2398796
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->d:Landroid/view/Display;

    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2398797
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2398798
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->c:Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

    if-eqz v0, :cond_1

    .line 2398799
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->b:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->getMediaAspectRatio()F

    move-result v0

    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->e:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2398800
    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->f:Z

    if-nez v0, :cond_1

    .line 2398801
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->c:Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

    const-string v1, "https://lookaside.facebook.com/assets/519288584920644/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;->b(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2398802
    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->c:Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

    if-eqz v2, :cond_0

    .line 2398803
    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->c:Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;->setAlpha(F)V

    .line 2398804
    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->c:Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;->setVisibility(I)V

    .line 2398805
    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->c:Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

    invoke-virtual {v2}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v4, 0xbb8

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 2398806
    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->c:Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

    invoke-virtual {v2}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v3, LX/Grq;

    invoke-direct {v3, p0}, LX/Grq;-><init>(Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 2398807
    :cond_0
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->c:Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;->setVisibility(I)V

    .line 2398808
    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/CrS;)V
    .locals 4

    .prologue
    .line 2398779
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->c:Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

    if-nez v0, :cond_0

    .line 2398780
    :goto_0
    return-void

    .line 2398781
    :cond_0
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->e:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->c:Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

    invoke-virtual {v1}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 2398782
    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->c:Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

    invoke-virtual {v2}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 2398783
    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->e:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget-object v3, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->c:Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

    invoke-virtual {v3}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 2398784
    iget-object v3, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->e:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object p1, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->c:Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

    invoke-virtual {p1}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;->getMeasuredWidth()I

    move-result p1

    add-int/2addr v3, p1

    div-int/lit8 v3, v3, 0x2

    .line 2398785
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1, v2, v0, v3, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2398786
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->a:LX/Ctg;

    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->c:Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

    invoke-interface {v0, v1, p1}, LX/Ctg;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2398787
    goto :goto_0
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 2398775
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->c:Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

    if-eqz v0, :cond_0

    .line 2398776
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->c:Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;->setVisibility(I)V

    .line 2398777
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->f:Z

    .line 2398778
    :cond_0
    return-void
.end method
