.class public Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0dN;
.implements LX/Ct1;
.implements LX/3FT;


# static fields
.field private static final e:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/2mn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/CqV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/CIb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:LX/Ctm;

.field private final g:Lcom/facebook/video/player/RichVideoPlayer;

.field private h:Lcom/facebook/video/player/RichVideoPlayer;

.field private i:LX/2pa;

.field private j:F

.field public k:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2398502
    const-class v0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2398503
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2398504
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2398505
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2398506
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2398507
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2398508
    iput v1, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->k:I

    .line 2398509
    const-class v0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    invoke-static {v0, p0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2398510
    const v0, 0x7f03096b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2398511
    const v0, 0x7f0d0917

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2398512
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->getPlayerType()LX/04G;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 2398513
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v2, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {v2, p1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 2398514
    invoke-static {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 2398515
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->g:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2398516
    new-instance v0, LX/Ctm;

    invoke-direct {v0, p0}, LX/Ctm;-><init>(LX/Ct1;)V

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->f:LX/Ctm;

    .line 2398517
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->getAdditionalPlugins()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 2398518
    iget-object v4, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2398519
    invoke-static {v4, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 2398520
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2398521
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;LX/2mn;LX/1C2;LX/CqV;LX/CIb;)V
    .locals 0

    .prologue
    .line 2398522
    iput-object p1, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->a:LX/2mn;

    iput-object p2, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->b:LX/1C2;

    iput-object p3, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->c:LX/CqV;

    iput-object p4, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->d:LX/CIb;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    invoke-static {v3}, LX/2mn;->a(LX/0QB;)LX/2mn;

    move-result-object v0

    check-cast v0, LX/2mn;

    invoke-static {v3}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v1

    check-cast v1, LX/1C2;

    invoke-static {v3}, LX/CqV;->a(LX/0QB;)LX/CqV;

    move-result-object v2

    check-cast v2, LX/CqV;

    invoke-static {v3}, LX/CIb;->a(LX/0QB;)LX/CIb;

    move-result-object v3

    check-cast v3, LX/CIb;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->a(Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;LX/2mn;LX/1C2;LX/CqV;LX/CIb;)V

    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 2398523
    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->c:LX/CqV;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sget-object v2, LX/CqU;->FEED_VIDEO:LX/CqU;

    invoke-virtual {v1, v0, v2}, LX/CqV;->a(ZLX/CqU;)V

    .line 2398524
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 2398525
    return-void

    .line 2398526
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setupPlayerLayout(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 4

    .prologue
    .line 2398561
    if-nez p1, :cond_0

    .line 2398562
    :goto_0
    return-void

    .line 2398563
    :cond_0
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->a:LX/2mn;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/2mn;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;F)LX/3FO;

    move-result-object v0

    .line 2398564
    iget v1, v0, LX/3FO;->a:I

    int-to-float v1, v1

    iget v2, v0, LX/3FO;->b:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->j:F

    .line 2398565
    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->f:LX/Ctm;

    iget v2, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->j:F

    .line 2398566
    iput v2, v1, LX/Ctm;->b:F

    .line 2398567
    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, v0, LX/3FO;->a:I

    iget v0, v0, LX/3FO;->b:I

    invoke-direct {v2, v3, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/04G;LX/04G;I)V
    .locals 13

    .prologue
    .line 2398558
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getRichVideoPlayerParams()LX/2pa;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->i:LX/2pa;

    if-nez v0, :cond_1

    .line 2398559
    :cond_0
    :goto_0
    return-void

    .line 2398560
    :cond_1
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->b:LX/1C2;

    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->d:LX/CIb;

    invoke-virtual {v1}, LX/CIb;->c()LX/0lF;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->i:LX/2pa;

    iget-object v2, v2, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v4, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->getPlayerOrigin()LX/04D;

    move-result-object v5

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    iget-object v6, v2, LX/04g;->value:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->getLastStartPosition()I

    move-result v8

    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->i:LX/2pa;

    iget-object v9, v2, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v2, p1

    move-object v3, p2

    move/from16 v7, p3

    invoke-virtual/range {v0 .. v12}, LX/1C2;->a(LX/0lF;LX/04G;LX/04G;Ljava/lang/String;LX/04D;Ljava/lang/String;IILX/098;Ljava/util/Map;LX/0JG;Ljava/lang/String;)LX/1C2;

    goto :goto_0
.end method

.method public final a(LX/2pa;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 3

    .prologue
    .line 2398550
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x4ed245b

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2398551
    iput-object p1, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->i:LX/2pa;

    .line 2398552
    invoke-direct {p0, p2}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->setupPlayerLayout(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2398553
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 2398554
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/2oi;->HIGH_DEFINITION:LX/2oi;

    sget-object v2, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2oi;LX/04g;)V

    .line 2398555
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    iget v1, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->k:I

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 2398556
    return-void

    .line 2398557
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2398461
    invoke-interface {p1, p2, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    .line 2398462
    iget-object v4, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v3, :cond_0

    move v0, v1

    :goto_0
    sget-object v5, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v4, v0, v5}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 2398463
    if-nez v3, :cond_1

    :goto_1
    invoke-virtual {p0, v1}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->a(Z)V

    .line 2398464
    return-void

    :cond_0
    move v0, v2

    .line 2398465
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2398466
    goto :goto_1
.end method

.method public final a(Lcom/facebook/video/player/RichVideoPlayer;)V
    .locals 2

    .prologue
    .line 2398546
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->g:Lcom/facebook/video/player/RichVideoPlayer;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setVisibility(I)V

    .line 2398547
    iput-object p1, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2398548
    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/facebook/video/player/RichVideoPlayer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->attachRecyclableViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2398549
    return-void
.end method

.method public final a(Z)V
    .locals 8

    .prologue
    .line 2398527
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2398528
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v0, v1

    .line 2398529
    if-nez v0, :cond_0

    .line 2398530
    :goto_0
    return-void

    .line 2398531
    :cond_0
    if-eqz p1, :cond_1

    .line 2398532
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->b:LX/1C2;

    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->d:LX/CIb;

    .line 2398533
    iget-object v2, v1, LX/CIb;->c:LX/0lF;

    move-object v1, v2

    .line 2398534
    sget-object v2, LX/04G;->CANVAS:LX/04G;

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    iget-object v3, v3, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->getCurrentPositionMs()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v5}, Lcom/facebook/video/player/RichVideoPlayer;->getVideoId()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2398535
    iget-object v7, v6, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    move-object v6, v7

    .line 2398536
    iget-object v7, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2398537
    iget-object p0, v7, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v7, p0

    .line 2398538
    iget-object v7, v7, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v0 .. v7}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    goto :goto_0

    .line 2398539
    :cond_1
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->b:LX/1C2;

    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->d:LX/CIb;

    .line 2398540
    iget-object v2, v1, LX/CIb;->c:LX/0lF;

    move-object v1, v2

    .line 2398541
    sget-object v2, LX/04G;->CANVAS:LX/04G;

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    iget-object v3, v3, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->getCurrentPositionMs()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v5}, Lcom/facebook/video/player/RichVideoPlayer;->getVideoId()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2398542
    iget-object v7, v6, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    move-object v6, v7

    .line 2398543
    iget-object v7, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2398544
    iget-object p0, v7, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v7, p0

    .line 2398545
    iget-object v7, v7, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v0 .. v7}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    goto :goto_0
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2398494
    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getPlayerState()LX/2qV;

    move-result-object v1

    .line 2398495
    if-nez v1, :cond_0

    .line 2398496
    :goto_0
    return v0

    .line 2398497
    :cond_0
    sget-object v2, LX/Grd;->a:[I

    invoke-virtual {v1}, LX/2qV;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2398498
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final c()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 2398499
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 2398500
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->detachRecyclableViewFromParent(Landroid/view/View;)V

    .line 2398501
    :cond_0
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public final d()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 2398456
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->g:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public getAdditionalPlugins()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2398457
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    new-instance v1, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, v2, v3}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/SubtitlePlugin;

    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/SubtitlePlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/Cuu;

    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/Cuu;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;

    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    .line 2398458
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getAdditionalPlugins()Ljava/util/List;
    .locals 1

    .prologue
    .line 2398459
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->getAdditionalPlugins()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentPositionMs()I
    .locals 1

    .prologue
    .line 2398460
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    goto :goto_0
.end method

.method public getLastLoadedParams()LX/2pa;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2398467
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->i:LX/2pa;

    return-object v0
.end method

.method public getLastStartPosition()I
    .locals 1

    .prologue
    .line 2398468
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getLastStartPosition()I

    move-result v0

    goto :goto_0
.end method

.method public getMediaAspectRatio()F
    .locals 1

    .prologue
    .line 2398469
    iget v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->j:F

    return v0
.end method

.method public getPlayerType()LX/04G;
    .locals 1

    .prologue
    .line 2398470
    sget-object v0, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    return-object v0
.end method

.method public getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 2398471
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public getVideoResolution()LX/2oi;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2398472
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2398473
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v1

    .line 2398474
    if-nez v0, :cond_1

    .line 2398475
    :cond_0
    const/4 v0, 0x0

    .line 2398476
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2398477
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v1

    .line 2398478
    invoke-virtual {v0}, LX/2pb;->f()LX/2oi;

    move-result-object v0

    goto :goto_0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 2398479
    return-object p0
.end method

.method public final jk_()Z
    .locals 1

    .prologue
    .line 2398480
    const/4 v0, 0x1

    return v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2398481
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 2398482
    packed-switch v0, :pswitch_data_0

    .line 2398483
    :goto_0
    return v1

    .line 2398484
    :pswitch_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->b(Z)V

    goto :goto_0

    .line 2398485
    :pswitch_1
    invoke-direct {p0, v1}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->b(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 2398486
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->f:LX/Ctm;

    invoke-virtual {v0}, LX/Ctm;->a()Landroid/graphics/Rect;

    move-result-object v0

    .line 2398487
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 2398488
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 2398489
    invoke-super {p0, v1, v0}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 2398490
    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->h:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2, v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->measure(II)V

    .line 2398491
    return-void
.end method

.method public setSeekPosition(I)V
    .locals 0

    .prologue
    .line 2398492
    iput p1, p0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->k:I

    .line 2398493
    return-void
.end method
