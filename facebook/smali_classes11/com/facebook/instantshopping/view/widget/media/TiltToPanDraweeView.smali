.class public Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;
.super Lcom/facebook/drawee/fbpipeline/FbDraweeView;
.source ""


# instance fields
.field public c:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2398766
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2398767
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2398764
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2398765
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2398761
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2398762
    const-class v0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

    invoke-static {v0, p0}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2398763
    return-void
.end method

.method private a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;
    .locals 2

    .prologue
    .line 2398760
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;->c:LX/1Ad;

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;->c:LX/1Ad;

    return-void
.end method

.method private c(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 3

    .prologue
    .line 2398755
    invoke-direct {p0, p2}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    .line 2398756
    invoke-virtual {v0, p1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/1Ae;->c(Z)LX/1Ae;

    .line 2398757
    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2398758
    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2398759
    return-void
.end method


# virtual methods
.method public final b(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 2398752
    invoke-direct {p0, p1, p2}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;->c(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2398753
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanDraweeView;->setVisibility(I)V

    .line 2398754
    return-void
.end method
