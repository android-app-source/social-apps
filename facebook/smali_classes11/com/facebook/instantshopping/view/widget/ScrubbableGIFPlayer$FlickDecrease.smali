.class public final Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public a:D

.field public b:D

.field public c:D

.field public final synthetic d:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;)V
    .locals 0

    .prologue
    .line 2398130
    iput-object p1, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->d:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2398154
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->d:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v0, v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->u:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2398155
    return-void
.end method


# virtual methods
.method public final a(D)V
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 2398148
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->d:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->d:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v1, v1, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    .line 2398149
    iput-wide v2, v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->j:J

    .line 2398150
    iput-wide p1, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->b:D

    .line 2398151
    iput-wide v4, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->a:D

    .line 2398152
    iput-wide v4, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->c:D

    .line 2398153
    return-void
.end method

.method public final run()V
    .locals 13

    .prologue
    const-wide v10, 0x3f33a92a30553261L    # 3.0E-4

    const-wide/16 v8, 0x0

    .line 2398131
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->d:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v0, v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->d:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-wide v2, v2, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->j:J

    sub-long/2addr v0, v2

    .line 2398132
    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->d:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v3, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->d:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v3, v3, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    .line 2398133
    iput-wide v4, v2, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->j:J

    .line 2398134
    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->d:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v3, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->d:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-wide v4, v3, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->k:D

    iget-wide v6, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->a:D

    add-double/2addr v4, v6

    .line 2398135
    iput-wide v4, v2, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->k:D

    .line 2398136
    iget-wide v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->b:D

    const-wide v4, 0x3f9eb851eb851eb8L    # 0.03

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    .line 2398137
    iget-wide v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->b:D

    iget-object v4, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->d:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget v4, v4, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->n:I

    int-to-double v4, v4

    mul-double/2addr v4, v10

    sub-double/2addr v2, v4

    iput-wide v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->b:D

    .line 2398138
    iget-wide v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->b:D

    cmpg-double v2, v2, v8

    if-gtz v2, :cond_2

    .line 2398139
    invoke-direct {p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->a()V

    .line 2398140
    :goto_0
    return-void

    .line 2398141
    :cond_0
    iget-wide v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->b:D

    const-wide v4, -0x406147ae147ae148L    # -0.03

    cmpg-double v2, v2, v4

    if-gez v2, :cond_1

    .line 2398142
    iget-wide v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->b:D

    iget-object v4, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->d:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget v4, v4, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->n:I

    int-to-double v4, v4

    mul-double/2addr v4, v10

    add-double/2addr v2, v4

    iput-wide v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->b:D

    .line 2398143
    iget-wide v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->b:D

    cmpl-double v2, v2, v8

    if-ltz v2, :cond_2

    .line 2398144
    invoke-direct {p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->a()V

    goto :goto_0

    .line 2398145
    :cond_1
    invoke-direct {p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->a()V

    goto :goto_0

    .line 2398146
    :cond_2
    iget-wide v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->b:D

    long-to-double v0, v0

    mul-double/2addr v0, v2

    iput-wide v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->a:D

    .line 2398147
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->d:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    invoke-static {v0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->j(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;)V

    goto :goto_0
.end method
