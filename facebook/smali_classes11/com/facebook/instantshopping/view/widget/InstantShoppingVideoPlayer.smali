.class public Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoPlayer;
.super Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;
.source ""

# interfaces
.implements LX/0dN;


# instance fields
.field public n:Z

.field private o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2397830
    invoke-direct {p0, p1}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;-><init>(Landroid/content/Context;)V

    .line 2397831
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoPlayer;->o:Z

    .line 2397832
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2397833
    invoke-direct {p0, p1, p2}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2397834
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoPlayer;->o:Z

    .line 2397835
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2397822
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2397823
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoPlayer;->o:Z

    .line 2397824
    return-void
.end method


# virtual methods
.method public final a(LX/04g;)V
    .locals 1

    .prologue
    .line 2397825
    invoke-super {p0, p1}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->a(LX/04g;)V

    .line 2397826
    sget-object v0, LX/04g;->BY_USER:LX/04g;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoPlayer;->o:Z

    if-nez v0, :cond_0

    .line 2397827
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->k()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->b(Z)V

    .line 2397828
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoPlayer;->o:Z

    .line 2397829
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2397815
    invoke-interface {p1, p2, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    .line 2397816
    if-nez v3, :cond_1

    move v0, v1

    :goto_0
    sget-object v4, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {p0, v0, v4}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 2397817
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2397818
    if-nez v3, :cond_2

    :goto_1
    invoke-virtual {p0, v1}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->b(Z)V

    .line 2397819
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 2397820
    goto :goto_0

    :cond_2
    move v1, v2

    .line 2397821
    goto :goto_1
.end method

.method public getDefaultPlayerOrigin()LX/04D;
    .locals 1

    .prologue
    .line 2397814
    sget-object v0, LX/04D;->INSTANT_SHOPPING:LX/04D;

    return-object v0
.end method

.method public setDefaultClickEnabled(Z)V
    .locals 0

    .prologue
    .line 2397812
    iput-boolean p1, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoPlayer;->n:Z

    .line 2397813
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2397808
    iget-boolean v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoPlayer;->n:Z

    if-eqz v0, :cond_0

    .line 2397809
    invoke-super {p0, p1}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2397810
    :goto_0
    return-void

    .line 2397811
    :cond_0
    const/4 v0, 0x0

    invoke-super {p0, v0}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
