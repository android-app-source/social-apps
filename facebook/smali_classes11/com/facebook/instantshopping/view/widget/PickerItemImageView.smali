.class public Lcom/facebook/instantshopping/view/widget/PickerItemImageView;
.super Lcom/facebook/drawee/fbpipeline/FbDraweeView;
.source ""


# instance fields
.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2397887
    invoke-direct {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2397888
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1af;)V
    .locals 0

    .prologue
    .line 2397889
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;LX/1af;)V

    .line 2397890
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2397891
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2397892
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2397893
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2397894
    return-void
.end method

.method private setBorderColor(I)V
    .locals 4

    .prologue
    .line 2397895
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    .line 2397896
    if-eqz v0, :cond_0

    .line 2397897
    iget-object v1, v0, LX/1af;->c:LX/4Ab;

    move-object v1, v1

    .line 2397898
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/PickerItemImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, p1, v2}, LX/4Ab;->a(IF)LX/4Ab;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1af;->a(LX/4Ab;)V

    .line 2397899
    :cond_0
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 2397900
    invoke-super {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->onMeasure(II)V

    .line 2397901
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/PickerItemImageView;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/PickerItemImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/facebook/instantshopping/view/widget/PickerItemImageView;->c:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-static {v1, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    sub-int/2addr v0, v1

    .line 2397902
    invoke-virtual {p0, v0, v0}, Lcom/facebook/instantshopping/view/widget/PickerItemImageView;->setMeasuredDimension(II)V

    .line 2397903
    return-void
.end method

.method public setIsSelected(Z)V
    .locals 2

    .prologue
    .line 2397904
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/PickerItemImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a07bb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2397905
    :goto_0
    invoke-direct {p0, v0}, Lcom/facebook/instantshopping/view/widget/PickerItemImageView;->setBorderColor(I)V

    .line 2397906
    return-void

    .line 2397907
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/PickerItemImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a004f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method public setSizeOffset(I)V
    .locals 0

    .prologue
    .line 2397908
    iput p1, p0, Lcom/facebook/instantshopping/view/widget/PickerItemImageView;->c:I

    .line 2397909
    return-void
.end method
