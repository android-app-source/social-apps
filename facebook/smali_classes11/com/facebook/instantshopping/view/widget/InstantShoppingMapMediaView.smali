.class public Lcom/facebook/instantshopping/view/widget/InstantShoppingMapMediaView;
.super Landroid/widget/LinearLayout;
.source ""

# interfaces
.implements LX/Ct1;


# instance fields
.field public a:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2397507
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingMapMediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2397508
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2397497
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingMapMediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2397498
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2397504
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2397505
    const-class v0, Lcom/facebook/instantshopping/view/widget/InstantShoppingMapMediaView;

    invoke-static {v0, p0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingMapMediaView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2397506
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingMapMediaView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingMapMediaView;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v0

    check-cast v0, LX/0hB;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingMapMediaView;->a:LX/0hB;

    return-void
.end method


# virtual methods
.method public getMediaAspectRatio()F
    .locals 3

    .prologue
    .line 2397501
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingMapMediaView;->a:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    .line 2397502
    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingMapMediaView;->a:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->d()I

    move-result v1

    .line 2397503
    int-to-float v0, v0

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v0, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 2397500
    return-object p0
.end method

.method public final jk_()Z
    .locals 1

    .prologue
    .line 2397499
    const/4 v0, 0x0

    return v0
.end method
