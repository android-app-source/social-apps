.class public Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoControlsView;
.super LX/Cud;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2397794
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoControlsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2397795
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2397806
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoControlsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2397807
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2397804
    invoke-direct {p0, p1, p2, p3}, LX/Cud;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2397805
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;)LX/Cud;
    .locals 3

    .prologue
    .line 2397801
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2397802
    const v1, 0x7f030980

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/Cud;

    .line 2397803
    return-object v0
.end method


# virtual methods
.method public final a(LX/Cuw;LX/Cub;)V
    .locals 1

    .prologue
    .line 2397799
    new-instance v0, LX/GrG;

    invoke-direct {v0, p0, p2, p1}, LX/GrG;-><init>(Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoControlsView;LX/Cub;LX/Cuw;)V

    invoke-virtual {p0, v0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoControlsView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2397800
    return-void
.end method

.method public getContentView()I
    .locals 1

    .prologue
    .line 2397798
    const v0, 0x7f03099f

    return v0
.end method

.method public getPauseIcon()Landroid/view/View;
    .locals 1

    .prologue
    .line 2397797
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPlayIcon()Landroid/view/View;
    .locals 1

    .prologue
    .line 2397796
    const v0, 0x7f0d188d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
