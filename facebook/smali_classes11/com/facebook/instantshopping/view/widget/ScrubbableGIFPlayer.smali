.class public Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;
.super Lcom/facebook/widget/FbImageView;
.source ""

# interfaces
.implements LX/Ct1;


# static fields
.field public static final r:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:J

.field public B:J

.field private C:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public D:LX/GoE;

.field public E:LX/GrS;

.field private F:LX/Ctm;

.field public G:Z

.field public H:Z

.field public a:LX/BV0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0So;
    .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/CqV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Go0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Landroid/net/Uri;

.field public h:I

.field private i:Landroid/view/VelocityTracker;

.field public j:J

.field public k:D

.field private l:I

.field private m:I

.field public n:I

.field public o:Z

.field private p:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;

.field public q:Landroid/net/Uri;

.field public s:LX/BUz;

.field public t:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public u:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field private v:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field private w:D

.field public x:Z

.field public y:Landroid/widget/ProgressBar;

.field public z:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2398341
    const-class v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    sput-object v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->r:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2398276
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2398277
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2398278
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2398279
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2398280
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/FbImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2398281
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->H:Z

    .line 2398282
    invoke-direct {p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->h()V

    .line 2398283
    return-void
.end method

.method public static synthetic a(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;D)D
    .locals 3

    .prologue
    .line 2398284
    iget-wide v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->k:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->k:D

    return-wide v0
.end method

.method private a(D)V
    .locals 3

    .prologue
    .line 2398285
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->C:Ljava/util/HashMap;

    const-string v1, "instant_shopping_scrubbablegif_block_distance"

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2398286
    return-void
.end method

.method private a(F)V
    .locals 7

    .prologue
    .line 2398258
    iget v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->l:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->n:I

    if-nez v0, :cond_1

    .line 2398259
    :cond_0
    :goto_0
    return-void

    .line 2398260
    :cond_1
    float-to-double v0, p1

    invoke-direct {p0, v0, v1}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->b(D)V

    .line 2398261
    float-to-double v0, p1

    const-wide/high16 v2, 0x4014000000000000L    # 5.0

    iget v4, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->l:I

    iget v5, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->n:I

    div-int/2addr v4, v5

    int-to-double v4, v4

    mul-double/2addr v2, v4

    div-double/2addr v0, v2

    .line 2398262
    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->p:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;

    invoke-virtual {v2, v0, v1}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;->a(D)V

    .line 2398263
    :try_start_0
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->c:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->p:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1e

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->u:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2398264
    :catch_0
    move-exception v0

    .line 2398265
    sget-object v1, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->r:Ljava/lang/Class;

    const-string v2, "Rejected Execution"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;LX/BV0;LX/0So;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/ScheduledExecutorService;LX/CqV;LX/Go0;)V
    .locals 0

    .prologue
    .line 2398287
    iput-object p1, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->a:LX/BV0;

    iput-object p2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->b:LX/0So;

    iput-object p3, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->c:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p4, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->d:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p5, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->e:LX/CqV;

    iput-object p6, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->f:LX/Go0;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    const-class v1, LX/BV0;

    invoke-interface {v6, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/BV0;

    invoke-static {v6}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v2

    check-cast v2, LX/0So;

    invoke-static {v6}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v6}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v6}, LX/CqV;->a(LX/0QB;)LX/CqV;

    move-result-object v5

    check-cast v5, LX/CqV;

    invoke-static {v6}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object v6

    check-cast v6, LX/Go0;

    invoke-static/range {v0 .. v6}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->a(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;LX/BV0;LX/0So;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/ScheduledExecutorService;LX/CqV;LX/Go0;)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 2398296
    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->e:LX/CqV;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sget-object v2, LX/CqU;->SCRUBBABLE_GIFS:LX/CqU;

    invoke-virtual {v1, v0, v2}, LX/CqV;->a(ZLX/CqU;)V

    .line 2398297
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 2398298
    return-void

    .line 2398299
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;Landroid/graphics/Bitmap;Ljava/io/File;)V
    .locals 4

    .prologue
    .line 2398342
    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v0

    .line 2398343
    const-string v1, "tmp"

    const-string v2, ".jpg"

    invoke-static {v1, v2, p2}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    .line 2398344
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 2398345
    const v3, 0xf4240

    if-le v0, v3, :cond_0

    .line 2398346
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x14

    invoke-virtual {p1, v0, v3, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 2398347
    :goto_0
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 2398348
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->t:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2398349
    :goto_1
    return-void

    .line 2398350
    :cond_0
    const v3, 0x7a120

    if-le v0, v3, :cond_1

    .line 2398351
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x32

    invoke-virtual {p1, v0, v3, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2398352
    :catch_0
    move-exception v0

    .line 2398353
    sget-object v1, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->r:Ljava/lang/Class;

    const-string v2, "Saving Image Failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2398354
    :cond_1
    const v3, 0x30d40

    if-le v0, v3, :cond_2

    .line 2398355
    :try_start_1
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x50

    invoke-virtual {p1, v0, v3, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    goto :goto_0

    .line 2398356
    :cond_2
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {p1, v0, v3, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private b(D)V
    .locals 3

    .prologue
    .line 2398339
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->C:Ljava/util/HashMap;

    const-string v1, "instant_shopping_scrubbablegif_block_flick_speed"

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2398340
    return-void
.end method

.method private c(D)V
    .locals 9

    .prologue
    .line 2398329
    iget v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->l:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->n:I

    if-nez v0, :cond_1

    .line 2398330
    :cond_0
    :goto_0
    return-void

    .line 2398331
    :cond_1
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->E:LX/GrS;

    .line 2398332
    const/4 v1, 0x2

    iput v1, v0, LX/GrS;->k:I

    .line 2398333
    invoke-virtual {v0}, LX/GrS;->m()V

    .line 2398334
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->j:J

    sub-long/2addr v0, v2

    .line 2398335
    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->j:J

    .line 2398336
    iget-wide v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->w:D

    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    long-to-double v6, v0

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iput-wide v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->w:D

    .line 2398337
    iget-wide v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->k:D

    long-to-double v0, v0

    mul-double/2addr v0, p1

    iget v4, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->l:I

    iget v5, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->n:I

    div-int/2addr v4, v5

    int-to-double v4, v4

    div-double/2addr v0, v4

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->k:D

    .line 2398338
    invoke-static {p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->j(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;)V

    goto :goto_0
.end method

.method private h()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2398304
    const-class v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    invoke-static {v0, p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2398305
    iput v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->h:I

    .line 2398306
    iput-boolean v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->x:Z

    .line 2398307
    iput-boolean v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->x:Z

    .line 2398308
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->t:Ljava/util/ArrayList;

    .line 2398309
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->l:I

    .line 2398310
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 2398311
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 2398312
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 2398313
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2398314
    iget v0, v1, Landroid/graphics/Point;->y:I

    iput v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->m:I

    .line 2398315
    iput-object v3, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->g:Landroid/net/Uri;

    .line 2398316
    iput v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->n:I

    .line 2398317
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->k:D

    .line 2398318
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->j:J

    .line 2398319
    iput-object v3, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->q:Landroid/net/Uri;

    .line 2398320
    iput-boolean v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->G:Z

    .line 2398321
    new-instance v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;

    invoke-direct {v0, p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;-><init>(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;)V

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->p:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$FlickDecrease;

    .line 2398322
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->C:Ljava/util/HashMap;

    .line 2398323
    iput-wide v4, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->z:J

    .line 2398324
    iput-wide v4, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->A:J

    .line 2398325
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->B:J

    .line 2398326
    iput-boolean v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->o:Z

    .line 2398327
    new-instance v0, LX/Ctm;

    invoke-direct {v0, p0}, LX/Ctm;-><init>(LX/Ct1;)V

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->F:LX/Ctm;

    .line 2398328
    return-void
.end method

.method public static synthetic i(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;)I
    .locals 2

    .prologue
    .line 2398303
    iget v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->n:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->n:I

    return v0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 2398300
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->f:LX/Go0;

    const-string v1, "instant_shopping_scrubbablegif_block_actions"

    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->C:Ljava/util/HashMap;

    invoke-virtual {v0, v1, v2}, LX/Go0;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2398301
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->f:LX/Go0;

    const-string v1, "document_closed"

    new-instance v2, LX/GrU;

    invoke-direct {v2, p0}, LX/GrU;-><init>(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;)V

    invoke-virtual {v0, v1, v2}, LX/Go0;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2398302
    return-void
.end method

.method public static j(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;)V
    .locals 4

    .prologue
    .line 2398288
    iget-wide v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->k:D

    iget v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->n:I

    int-to-double v2, v2

    rem-double/2addr v0, v2

    iput-wide v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->k:D

    .line 2398289
    iget-wide v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->k:D

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 2398290
    iget v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->n:I

    add-int/lit8 v0, v0, -0x1

    int-to-double v0, v0

    iput-wide v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->k:D

    .line 2398291
    :cond_0
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->t:Ljava/util/ArrayList;

    iget-wide v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->k:D

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->setImageUsingFile(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;Ljava/lang/String;)V

    .line 2398292
    return-void
.end method

.method private k()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2398293
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->g:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 2398294
    :goto_0
    return-void

    .line 2398295
    :cond_0
    new-instance v0, LX/GrV;

    invoke-direct {v0, p0}, LX/GrV;-><init>(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;)V

    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->getContext()Landroid/content/Context;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, LX/3nE;->a(Landroid/content/Context;[Ljava/lang/Object;)LX/3nE;

    goto :goto_0
.end method

.method private l()V
    .locals 5

    .prologue
    .line 2398266
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->q:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 2398267
    :goto_0
    return-void

    .line 2398268
    :cond_0
    new-instance v1, Ljava/io/File;

    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->q:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2398269
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2398270
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 2398271
    const/4 v0, 0x0

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 2398272
    new-instance v3, Ljava/io/File;

    aget-object v4, v2, v0

    invoke-direct {v3, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 2398273
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2398274
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 2398275
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->q:Landroid/net/Uri;

    goto :goto_0
.end method

.method public static setImageUsingFile(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2398156
    new-instance v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$3;

    invoke-direct {v0, p0, p1}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$3;-><init>(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;Ljava/lang/String;)V

    invoke-static {v0}, LX/5fo;->a(Ljava/lang/Runnable;)V

    .line 2398157
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2398158
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->G:Z

    .line 2398159
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->E:LX/GrS;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->x:Z

    if-eqz v0, :cond_0

    .line 2398160
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->E:LX/GrS;

    invoke-virtual {v0}, LX/GrS;->a()V

    .line 2398161
    :cond_0
    return-void
.end method

.method public final a(JDI)V
    .locals 7

    .prologue
    .line 2398162
    iget-boolean v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->o:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->G:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->x:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->v:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->v:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2398163
    :cond_0
    :goto_0
    return-void

    .line 2398164
    :cond_1
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->d:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$2;

    invoke-direct {v1, p0, p3, p4}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$2;-><init>(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;D)V

    int-to-long v4, p5

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide v2, p1

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->v:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2398165
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->G:Z

    .line 2398166
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->E:LX/GrS;

    if-eqz v0, :cond_0

    .line 2398167
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->E:LX/GrS;

    .line 2398168
    invoke-virtual {v0}, LX/GrS;->m()V

    .line 2398169
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->e()V

    .line 2398170
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2398171
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->v:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->d:Ljava/util/concurrent/ScheduledExecutorService;

    if-eqz v0, :cond_0

    .line 2398172
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->v:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2398173
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2398174
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->u:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 2398175
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->u:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2398176
    :cond_0
    invoke-direct {p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->l()V

    .line 2398177
    invoke-direct {p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->i()V

    .line 2398178
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->G:Z

    .line 2398179
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->e()V

    .line 2398180
    return-void
.end method

.method public getMediaAspectRatio()F
    .locals 1

    .prologue
    .line 2398181
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->F:LX/Ctm;

    .line 2398182
    iget p0, v0, LX/Ctm;->b:F

    move v0, p0

    .line 2398183
    return v0
.end method

.method public getNumberOfTimesInstructionsAnimated()I
    .locals 1

    .prologue
    .line 2398184
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->E:LX/GrS;

    if-eqz v0, :cond_0

    .line 2398185
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->E:LX/GrS;

    .line 2398186
    iget p0, v0, LX/GrS;->k:I

    move v0, p0

    .line 2398187
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 2398257
    return-object p0
.end method

.method public final jk_()Z
    .locals 1

    .prologue
    .line 2398188
    const/4 v0, 0x1

    return v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    const v2, 0x66b34d0

    invoke-static {v4, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2398189
    iget-boolean v3, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->x:Z

    if-nez v3, :cond_0

    .line 2398190
    const v1, 0x4898ae34

    invoke-static {v4, v4, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2398191
    :goto_0
    return v0

    .line 2398192
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    .line 2398193
    packed-switch v3, :pswitch_data_0

    .line 2398194
    :cond_1
    :goto_1
    const v0, -0x132e00b7

    invoke-static {v0, v2}, LX/02F;->a(II)V

    move v0, v1

    goto :goto_0

    .line 2398195
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->e()V

    .line 2398196
    invoke-direct {p0, v1}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->a(Z)V

    .line 2398197
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->i:Landroid/view/VelocityTracker;

    if-nez v0, :cond_2

    .line 2398198
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->i:Landroid/view/VelocityTracker;

    .line 2398199
    :goto_2
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->i:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 2398200
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->j:J

    .line 2398201
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->w:D

    goto :goto_1

    .line 2398202
    :cond_2
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->i:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_2

    .line 2398203
    :pswitch_1
    iget-object v3, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->i:Landroid/view/VelocityTracker;

    invoke-virtual {v3, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 2398204
    iget-object v3, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->i:Landroid/view/VelocityTracker;

    const/high16 v4, 0x41000000    # 8.0f

    invoke-virtual {v3, v1, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 2398205
    iget-object v3, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->i:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v3

    float-to-double v4, v3

    .line 2398206
    iget-object v3, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->i:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v3

    float-to-double v6, v3

    .line 2398207
    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    const-wide v10, 0x3fe6666660000000L    # 0.699999988079071

    cmpl-double v3, v8, v10

    if-lez v3, :cond_3

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    const-wide/high16 v8, 0x4004000000000000L    # 2.5

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    cmpl-double v3, v4, v8

    if-lez v3, :cond_3

    .line 2398208
    invoke-direct {p0, v0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->a(Z)V

    .line 2398209
    :cond_3
    invoke-direct {p0, v6, v7}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->c(D)V

    .line 2398210
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->u:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_1

    .line 2398211
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->u:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    goto :goto_1

    .line 2398212
    :pswitch_2
    iget-wide v4, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->w:D

    invoke-direct {p0, v4, v5}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->a(D)V

    .line 2398213
    iget-object v3, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->i:Landroid/view/VelocityTracker;

    invoke-virtual {v3, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 2398214
    iget-object v3, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->i:Landroid/view/VelocityTracker;

    const/high16 v4, 0x42f00000    # 120.0f

    invoke-virtual {v3, v1, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 2398215
    iget-object v3, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->i:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    .line 2398216
    iget-object v3, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->i:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v3

    invoke-direct {p0, v3}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->a(F)V

    .line 2398217
    :cond_4
    invoke-direct {p0, v0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->a(Z)V

    goto/16 :goto_1

    .line 2398218
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->i:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 2398219
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->i:Landroid/view/VelocityTracker;

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public final onWindowFocusChanged(Z)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, 0x4be02db2    # 2.9383524E7f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2398220
    invoke-super {p0, p1}, Lcom/facebook/widget/FbImageView;->onWindowFocusChanged(Z)V

    .line 2398221
    if-nez p1, :cond_1

    .line 2398222
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->G:Z

    .line 2398223
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->e()V

    .line 2398224
    :cond_0
    :goto_0
    const v1, 0x7ee5a40b

    invoke-static {v1, v0}, LX/02F;->g(II)V

    return-void

    .line 2398225
    :cond_1
    iget v1, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->m:I

    invoke-static {p0, v1}, LX/GrS;->a(Landroid/view/View;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2398226
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->G:Z

    .line 2398227
    iget-boolean v1, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->x:Z

    if-eqz v1, :cond_0

    .line 2398228
    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->E:LX/GrS;

    invoke-virtual {v1}, LX/GrS;->a()V

    goto :goto_0
.end method

.method public setAspectRatio(F)V
    .locals 2

    .prologue
    .line 2398229
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->F:LX/Ctm;

    .line 2398230
    iput p1, v0, LX/Ctm;->b:F

    .line 2398231
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->l:I

    int-to-float v1, v1

    div-float/2addr v1, p1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2398232
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->l:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2398233
    invoke-virtual {p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->requestLayout()V

    .line 2398234
    return-void
.end method

.method public setAutoPlayEnabled(Z)V
    .locals 0

    .prologue
    .line 2398235
    iput-boolean p1, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->o:Z

    .line 2398236
    return-void
.end method

.method public setDuration(I)V
    .locals 1

    .prologue
    const/16 v0, 0x7530

    .line 2398237
    if-le p1, v0, :cond_0

    .line 2398238
    iput v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->h:I

    .line 2398239
    :goto_0
    return-void

    .line 2398240
    :cond_0
    iput p1, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->h:I

    goto :goto_0
.end method

.method public setFolder(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2398241
    iput-object p1, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->q:Landroid/net/Uri;

    .line 2398242
    return-void
.end method

.method public setInstructionPlugin(LX/GrS;)V
    .locals 0

    .prologue
    .line 2398243
    iput-object p1, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->E:LX/GrS;

    .line 2398244
    return-void
.end method

.method public setLoggingParams(LX/GoE;)V
    .locals 3

    .prologue
    .line 2398245
    iput-object p1, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->D:LX/GoE;

    .line 2398246
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->C:Ljava/util/HashMap;

    const-string v1, "instant_shopping_scrubbablegif_block_id"

    iget-object v2, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->D:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2398247
    return-void
.end method

.method public setNumberOfTimesInstructionsAnimated(I)V
    .locals 1

    .prologue
    .line 2398248
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->E:LX/GrS;

    if-eqz v0, :cond_0

    .line 2398249
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->E:LX/GrS;

    .line 2398250
    const/4 p0, -0x1

    if-ne p1, p0, :cond_1

    .line 2398251
    const/4 p0, 0x0

    iput p0, v0, LX/GrS;->k:I

    .line 2398252
    :cond_0
    :goto_0
    return-void

    .line 2398253
    :cond_1
    iput p1, v0, LX/GrS;->k:I

    goto :goto_0
.end method

.method public setVideoUri(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2398254
    iput-object p1, p0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->g:Landroid/net/Uri;

    .line 2398255
    invoke-direct {p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->k()V

    .line 2398256
    return-void
.end method
