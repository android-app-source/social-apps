.class public Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Gpu;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

.field private c:Lcom/facebook/instantshopping/view/widget/PickerItemImageView;

.field public d:LX/CHX;

.field private e:Z

.field private f:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2397203
    const-class v0, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2397230
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2397231
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2397228
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2397229
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2397226
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2397227
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2397219
    const v0, 0x7f0d1814

    invoke-virtual {p0, v0}, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/view/widget/PickerItemImageView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->c:Lcom/facebook/instantshopping/view/widget/PickerItemImageView;

    .line 2397220
    const v0, 0x7f0d1815

    invoke-virtual {p0, v0}, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->b:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    .line 2397221
    const v0, 0x7f0d1812

    invoke-virtual {p0, v0}, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->f:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    .line 2397222
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->b:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    const/4 v1, 0x2

    .line 2397223
    iput v1, v0, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->d:I

    .line 2397224
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->b:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->c()V

    .line 2397225
    return-void
.end method

.method public getAction()LX/CHX;
    .locals 1

    .prologue
    .line 2397218
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->d:LX/CHX;

    return-object v0
.end method

.method public setAction(LX/CHX;)V
    .locals 0

    .prologue
    .line 2397216
    iput-object p1, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->d:LX/CHX;

    .line 2397217
    return-void
.end method

.method public setDisabled(Z)V
    .locals 2

    .prologue
    .line 2397211
    iput-boolean p1, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->e:Z

    .line 2397212
    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->f:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    iget-boolean v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->setVisibility(I)V

    .line 2397213
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->f:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getTopLevelDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0xdc

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2397214
    return-void

    .line 2397215
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setImageUrl(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2397209
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->c:Lcom/facebook/instantshopping/view/widget/PickerItemImageView;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2397210
    return-void
.end method

.method public setIsSelected(Z)V
    .locals 2

    .prologue
    .line 2397204
    iget-object v1, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->b:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->setVisibility(I)V

    .line 2397205
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->f:Lcom/facebook/instantshopping/view/widget/PickerItemColorView;

    invoke-virtual {v0, p1}, Lcom/facebook/instantshopping/view/widget/PickerItemColorView;->setIsSelected(Z)V

    .line 2397206
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->c:Lcom/facebook/instantshopping/view/widget/PickerItemImageView;

    invoke-virtual {v0, p1}, Lcom/facebook/instantshopping/view/widget/PickerItemImageView;->setIsSelected(Z)V

    .line 2397207
    return-void

    .line 2397208
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
