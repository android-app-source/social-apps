.class public Lcom/facebook/instantshopping/view/widget/InstantShoppingLoadingIndicatorView;
.super Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;
.source ""

# interfaces
.implements LX/CtT;


# instance fields
.field private e:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2397487
    invoke-direct {p0, p1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;-><init>(Landroid/content/Context;)V

    .line 2397488
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2397489
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2397490
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2397491
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2397492
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 2397493
    iget-object v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingLoadingIndicatorView;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-static {v0, v1, v2}, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->d(Landroid/support/v7/widget/RecyclerView;II)Z

    move-result v0

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x64e12f31

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2397494
    invoke-super {p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->onAttachedToWindow()V

    .line 2397495
    const v0, 0x7f0d04e7

    invoke-virtual {p0, v0}, Lcom/facebook/instantshopping/view/widget/InstantShoppingLoadingIndicatorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingLoadingIndicatorView;->e:Landroid/support/v7/widget/RecyclerView;

    .line 2397496
    const/16 v0, 0x2d

    const v2, -0x3a5b8a57

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
