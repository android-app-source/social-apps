.class public Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;
.super Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/payments/picker/model/SimplePickerRunTimeData",
        "<",
        "Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;",
        "Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;",
        "Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;",
        "LX/FRw;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2240446
    new-instance v0, LX/FRV;

    invoke-direct {v0}, LX/FRV;-><init>()V

    sput-object v0, Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 2240447
    invoke-direct {p0, p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;-><init>(Landroid/os/Parcel;)V

    .line 2240448
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;)V
    .locals 0

    .prologue
    .line 2240442
    invoke-direct {p0, p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;-><init>(Lcom/facebook/payments/picker/model/PickerScreenConfig;)V

    .line 2240443
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;",
            "Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;",
            "Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;",
            "LX/0P1",
            "<",
            "LX/FRw;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2240444
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;-><init>(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)V

    .line 2240445
    return-void
.end method


# virtual methods
.method public final b()Landroid/content/Intent;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2240439
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2240440
    iget-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v0, v0

    .line 2240441
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
