.class public Lcom/facebook/payments/settings/protocol/GetPayAccountResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/settings/protocol/GetPayAccountResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/currency/CurrencyAmount;

.field public final b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2241197
    new-instance v0, LX/FRz;

    invoke-direct {v0}, LX/FRz;-><init>()V

    sput-object v0, Lcom/facebook/payments/settings/protocol/GetPayAccountResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2241206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2241207
    const-class v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/settings/protocol/GetPayAccountResult;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2241208
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/settings/protocol/GetPayAccountResult;->b:I

    .line 2241209
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/currency/CurrencyAmount;I)V
    .locals 1

    .prologue
    .line 2241202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2241203
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/settings/protocol/GetPayAccountResult;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2241204
    iput p2, p0, Lcom/facebook/payments/settings/protocol/GetPayAccountResult;->b:I

    .line 2241205
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2241201
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2241198
    iget-object v0, p0, Lcom/facebook/payments/settings/protocol/GetPayAccountResult;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2241199
    iget v0, p0, Lcom/facebook/payments/settings/protocol/GetPayAccountResult;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2241200
    return-void
.end method
