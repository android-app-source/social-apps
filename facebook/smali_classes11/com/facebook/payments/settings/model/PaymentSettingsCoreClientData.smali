.class public Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/picker/model/CoreClientData;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

.field public final b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/payments/history/model/PaymentTransactions;

.field public final d:Lcom/facebook/payments/currency/CurrencyAmount;

.field public final e:I

.field public final f:Lcom/facebook/payments/auth/pin/model/PaymentPin;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2241064
    new-instance v0, LX/FRs;

    invoke-direct {v0}, LX/FRs;-><init>()V

    sput-object v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/FRt;)V
    .locals 2

    .prologue
    .line 2241065
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2241066
    iget-object v0, p1, LX/FRt;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    move-object v0, v0

    .line 2241067
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    iput-object v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 2241068
    iget-object v0, p1, LX/FRt;->b:LX/0am;

    move-object v0, v0

    .line 2241069
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    iput-object v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->b:LX/0am;

    .line 2241070
    iget-object v0, p1, LX/FRt;->c:Lcom/facebook/payments/history/model/PaymentTransactions;

    move-object v0, v0

    .line 2241071
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/history/model/PaymentTransactions;

    iput-object v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->c:Lcom/facebook/payments/history/model/PaymentTransactions;

    .line 2241072
    iget-object v0, p1, LX/FRt;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v0, v0

    .line 2241073
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2241074
    iget v0, p1, LX/FRt;->e:I

    move v0, v0

    .line 2241075
    iput v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->e:I

    .line 2241076
    iget-object v0, p1, LX/FRt;->f:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    move-object v0, v0

    .line 2241077
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    iput-object v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->f:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 2241078
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2241079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2241080
    const-class v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    iput-object v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 2241081
    const-class v0, Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Class;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->b:LX/0am;

    .line 2241082
    const-class v0, Lcom/facebook/payments/history/model/PaymentTransactions;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/history/model/PaymentTransactions;

    iput-object v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->c:Lcom/facebook/payments/history/model/PaymentTransactions;

    .line 2241083
    const-class v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2241084
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->e:I

    .line 2241085
    const-class v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    iput-object v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->f:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 2241086
    return-void
.end method

.method public static newBuilder()LX/FRt;
    .locals 1

    .prologue
    .line 2241087
    new-instance v0, LX/FRt;

    invoke-direct {v0}, LX/FRt;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2241088
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2241089
    iget-object v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2241090
    iget-object v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->b:LX/0am;

    invoke-static {p1, v0, p2}, LX/46R;->a(Landroid/os/Parcel;LX/0am;I)V

    .line 2241091
    iget-object v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->c:Lcom/facebook/payments/history/model/PaymentTransactions;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2241092
    iget-object v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2241093
    iget v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2241094
    iget-object v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->f:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2241095
    return-void
.end method
