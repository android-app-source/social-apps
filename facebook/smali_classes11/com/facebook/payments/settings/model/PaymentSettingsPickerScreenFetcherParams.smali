.class public Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2241101
    new-instance v0, LX/FRu;

    invoke-direct {v0}, LX/FRu;-><init>()V

    sput-object v0, Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/FRv;)V
    .locals 1

    .prologue
    .line 2241102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2241103
    iget-boolean v0, p1, LX/FRv;->a:Z

    move v0, v0

    .line 2241104
    iput-boolean v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;->a:Z

    .line 2241105
    iget-boolean v0, p1, LX/FRv;->b:Z

    move v0, v0

    .line 2241106
    iput-boolean v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;->b:Z

    .line 2241107
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2241108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2241109
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;->a:Z

    .line 2241110
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;->b:Z

    .line 2241111
    return-void
.end method

.method public static newBuilder()LX/FRv;
    .locals 1

    .prologue
    .line 2241112
    new-instance v0, LX/FRv;

    invoke-direct {v0}, LX/FRv;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2241113
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2241114
    iget-boolean v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2241115
    iget-boolean v0, p0, Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2241116
    return-void
.end method
