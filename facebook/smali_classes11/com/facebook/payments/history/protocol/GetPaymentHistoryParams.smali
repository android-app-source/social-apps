.class public Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2240309
    new-instance v0, LX/FRN;

    invoke-direct {v0}, LX/FRN;-><init>()V

    sput-object v0, Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/FRO;)V
    .locals 1

    .prologue
    .line 2240310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2240311
    iget-object v0, p1, LX/FRO;->a:Ljava/lang/Long;

    move-object v0, v0

    .line 2240312
    iput-object v0, p0, Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;->a:Ljava/lang/Long;

    .line 2240313
    iget-object v0, p1, LX/FRO;->b:Ljava/lang/Integer;

    move-object v0, v0

    .line 2240314
    iput-object v0, p0, Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;->b:Ljava/lang/Integer;

    .line 2240315
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2240316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2240317
    invoke-static {p1}, LX/46R;->d(Landroid/os/Parcel;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;->a:Ljava/lang/Long;

    .line 2240318
    invoke-static {p1}, LX/46R;->c(Landroid/os/Parcel;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;->b:Ljava/lang/Integer;

    .line 2240319
    return-void
.end method

.method public static newBuilder()LX/FRO;
    .locals 1

    .prologue
    .line 2240320
    new-instance v0, LX/FRO;

    invoke-direct {v0}, LX/FRO;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2240321
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2240322
    iget-object v0, p0, Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;->a:Ljava/lang/Long;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Long;)V

    .line 2240323
    iget-object v0, p0, Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;->b:Ljava/lang/Integer;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Integer;)V

    .line 2240324
    return-void
.end method
