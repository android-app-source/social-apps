.class public Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;
.super Lcom/facebook/payments/ui/PaymentsComponentViewGroup;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/6vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/payments/ui/PaymentsComponentViewGroup;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/6vq",
        "<",
        "LX/FRD;",
        ">;"
    }
.end annotation


# static fields
.field public static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/11T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/payments/history/model/PaymentTransaction;

.field public e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public f:Lcom/facebook/widget/text/BetterTextView;

.field public g:Lcom/facebook/widget/text/BetterTextView;

.field public h:Lcom/facebook/widget/text/BetterTextView;

.field public i:Lcom/facebook/widget/text/BetterTextView;

.field public j:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2240202
    const-class v0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2240192
    invoke-direct {p0, p1}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;)V

    .line 2240193
    const p1, 0x7f030eff

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2240194
    const-class p1, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;

    invoke-static {p1, p0}, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2240195
    const p1, 0x7f0d0343

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2240196
    const p1, 0x7f0d02c4

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2240197
    const p1, 0x7f0d2125

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 2240198
    const p1, 0x7f0d2492

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->h:Lcom/facebook/widget/text/BetterTextView;

    .line 2240199
    const p1, 0x7f0d2493

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->i:Lcom/facebook/widget/text/BetterTextView;

    .line 2240200
    const p1, 0x7f0d2494

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->j:Lcom/facebook/widget/text/BetterTextView;

    .line 2240201
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;

    invoke-static {v2}, LX/11T;->a(LX/0QB;)LX/11T;

    move-result-object v1

    check-cast v1, LX/11T;

    const/16 p0, 0x12cb

    invoke-static {v2, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    iput-object v1, p1, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->a:LX/11T;

    iput-object v2, p1, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->b:LX/0Or;

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2240183
    invoke-static {p0}, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->h(Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2240184
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->f:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2240185
    :goto_0
    return-void

    .line 2240186
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->f:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2240187
    iget-object v1, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {p0}, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->i(Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v0}, Lcom/facebook/payments/history/model/PaymentTransaction;->b()Lcom/facebook/payments/history/model/PaymentProfile;

    move-result-object v0

    .line 2240188
    iget-object p0, v0, Lcom/facebook/payments/history/model/PaymentProfile;->b:Ljava/lang/String;

    move-object v0, p0

    .line 2240189
    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v0}, Lcom/facebook/payments/history/model/PaymentTransaction;->a()Lcom/facebook/payments/history/model/PaymentProfile;

    move-result-object v0

    .line 2240190
    iget-object p0, v0, Lcom/facebook/payments/history/model/PaymentProfile;->b:Ljava/lang/String;

    move-object v0, p0

    .line 2240191
    goto :goto_1
.end method

.method private static h(Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2240178
    invoke-static {p0}, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->i(Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v2}, Lcom/facebook/payments/history/model/PaymentTransaction;->b()Lcom/facebook/payments/history/model/PaymentProfile;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v2}, Lcom/facebook/payments/history/model/PaymentTransaction;->b()Lcom/facebook/payments/history/model/PaymentProfile;

    move-result-object v2

    .line 2240179
    iget-object p0, v2, Lcom/facebook/payments/history/model/PaymentProfile;->b:Ljava/lang/String;

    move-object v2, p0

    .line 2240180
    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v2}, Lcom/facebook/payments/history/model/PaymentTransaction;->a()Lcom/facebook/payments/history/model/PaymentProfile;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v2}, Lcom/facebook/payments/history/model/PaymentTransaction;->a()Lcom/facebook/payments/history/model/PaymentProfile;

    move-result-object v2

    .line 2240181
    iget-object p0, v2, Lcom/facebook/payments/history/model/PaymentProfile;->b:Ljava/lang/String;

    move-object v2, p0

    .line 2240182
    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static i(Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;)Z
    .locals 2

    .prologue
    .line 2240171
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v0}, Lcom/facebook/payments/history/model/PaymentTransaction;->a()Lcom/facebook/payments/history/model/PaymentProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v0}, Lcom/facebook/payments/history/model/PaymentTransaction;->a()Lcom/facebook/payments/history/model/PaymentProfile;

    move-result-object v0

    .line 2240172
    iget-object v1, v0, Lcom/facebook/payments/history/model/PaymentProfile;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2240173
    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2240174
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2240175
    iget-object v1, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v1}, Lcom/facebook/payments/history/model/PaymentTransaction;->a()Lcom/facebook/payments/history/model/PaymentProfile;

    move-result-object v1

    .line 2240176
    iget-object p0, v1, Lcom/facebook/payments/history/model/PaymentProfile;->a:Ljava/lang/String;

    move-object v1, p0

    .line 2240177
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/FRD;)V
    .locals 10

    .prologue
    .line 2240123
    iget-object v0, p1, LX/FRD;->b:Lcom/facebook/payments/history/model/PaymentTransaction;

    iput-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    .line 2240124
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2240125
    invoke-static {p0}, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->i(Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v2}, Lcom/facebook/payments/history/model/PaymentTransaction;->b()Lcom/facebook/payments/history/model/PaymentProfile;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v2}, Lcom/facebook/payments/history/model/PaymentTransaction;->b()Lcom/facebook/payments/history/model/PaymentProfile;

    move-result-object v2

    .line 2240126
    iget-object p1, v2, Lcom/facebook/payments/history/model/PaymentProfile;->c:Lcom/facebook/payments/history/model/ProfileImage;

    move-object v2, p1

    .line 2240127
    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v2}, Lcom/facebook/payments/history/model/PaymentTransaction;->b()Lcom/facebook/payments/history/model/PaymentProfile;

    move-result-object v2

    .line 2240128
    iget-object p1, v2, Lcom/facebook/payments/history/model/PaymentProfile;->c:Lcom/facebook/payments/history/model/ProfileImage;

    move-object v2, p1

    .line 2240129
    iget-object p1, v2, Lcom/facebook/payments/history/model/ProfileImage;->a:Ljava/lang/String;

    move-object v2, p1

    .line 2240130
    if-eqz v2, :cond_3

    :cond_0
    :goto_0
    move v0, v0

    .line 2240131
    if-nez v0, :cond_1

    .line 2240132
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2240133
    :goto_1
    invoke-direct {p0}, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->c()V

    .line 2240134
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2240135
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v0}, Lcom/facebook/payments/history/model/PaymentTransaction;->f()LX/FQv;

    move-result-object v0

    if-nez v0, :cond_6

    .line 2240136
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2240137
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->h:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2240138
    :goto_2
    iget-object v1, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v1}, Lcom/facebook/payments/history/model/PaymentTransaction;->c()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_7

    .line 2240139
    iget-object v1, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->i:Lcom/facebook/widget/text/BetterTextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2240140
    :goto_3
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v0}, Lcom/facebook/payments/history/model/PaymentTransaction;->d()Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    if-nez v0, :cond_8

    .line 2240141
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->j:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2240142
    :goto_4
    return-void

    .line 2240143
    :cond_1
    invoke-static {p0}, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->i(Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v0}, Lcom/facebook/payments/history/model/PaymentTransaction;->b()Lcom/facebook/payments/history/model/PaymentProfile;

    move-result-object v0

    .line 2240144
    iget-object v1, v0, Lcom/facebook/payments/history/model/PaymentProfile;->c:Lcom/facebook/payments/history/model/ProfileImage;

    move-object v0, v1

    .line 2240145
    iget-object v1, v0, Lcom/facebook/payments/history/model/ProfileImage;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2240146
    :goto_5
    iget-object v1, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v2, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_1

    .line 2240147
    :cond_2
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v0}, Lcom/facebook/payments/history/model/PaymentTransaction;->a()Lcom/facebook/payments/history/model/PaymentProfile;

    move-result-object v0

    .line 2240148
    iget-object v1, v0, Lcom/facebook/payments/history/model/PaymentProfile;->c:Lcom/facebook/payments/history/model/ProfileImage;

    move-object v0, v1

    .line 2240149
    iget-object v1, v0, Lcom/facebook/payments/history/model/ProfileImage;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2240150
    goto :goto_5

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v2}, Lcom/facebook/payments/history/model/PaymentTransaction;->a()Lcom/facebook/payments/history/model/PaymentProfile;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v2}, Lcom/facebook/payments/history/model/PaymentTransaction;->a()Lcom/facebook/payments/history/model/PaymentProfile;

    move-result-object v2

    .line 2240151
    iget-object p1, v2, Lcom/facebook/payments/history/model/PaymentProfile;->c:Lcom/facebook/payments/history/model/ProfileImage;

    move-object v2, p1

    .line 2240152
    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v2}, Lcom/facebook/payments/history/model/PaymentTransaction;->a()Lcom/facebook/payments/history/model/PaymentProfile;

    move-result-object v2

    .line 2240153
    iget-object p1, v2, Lcom/facebook/payments/history/model/PaymentProfile;->c:Lcom/facebook/payments/history/model/ProfileImage;

    move-object v2, p1

    .line 2240154
    iget-object p1, v2, Lcom/facebook/payments/history/model/ProfileImage;->a:Ljava/lang/String;

    move-object v2, p1

    .line 2240155
    if-nez v2, :cond_0

    :cond_5
    move v0, v1

    goto/16 :goto_0

    .line 2240156
    :cond_6
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->h:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2240157
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2240158
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v2}, Lcom/facebook/payments/history/model/PaymentTransaction;->f()LX/FQv;

    move-result-object v2

    invoke-virtual {v2}, LX/FQv;->getTextStringId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 2240159
    :cond_7
    iget-object v1, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->i:Lcom/facebook/widget/text/BetterTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2240160
    iget-object v1, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->i:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v2}, Lcom/facebook/payments/history/model/PaymentTransaction;->c()J

    move-result-wide v3

    .line 2240161
    new-instance v5, Ljava/util/Date;

    const-wide/16 v7, 0x3e8

    mul-long/2addr v7, v3

    invoke-direct {v5, v7, v8}, Ljava/util/Date;-><init>(J)V

    .line 2240162
    iget-object v6, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->a:LX/11T;

    invoke-virtual {v6}, LX/11T;->h()Ljava/text/SimpleDateFormat;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    move-object v2, v5

    .line 2240163
    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 2240164
    :cond_8
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->j:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2240165
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->i(Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "-"

    :goto_6
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v1}, Lcom/facebook/payments/history/model/PaymentTransaction;->d()Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/payments/currency/CurrencyAmount;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2240166
    invoke-static {p0}, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->i(Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;)Z

    move-result v0

    if-eqz v0, :cond_a

    const v0, 0x7f0a00fc

    .line 2240167
    :goto_7
    iget-object v2, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->j:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2240168
    iget-object v1, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->j:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    goto/16 :goto_4

    .line 2240169
    :cond_9
    const-string v0, "+"

    goto :goto_6

    .line 2240170
    :cond_a
    const v0, 0x7f0a00d4

    goto :goto_7
.end method

.method public onClick()V
    .locals 2

    .prologue
    .line 2240120
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v0}, Lcom/facebook/payments/history/model/PaymentTransaction;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2240121
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->d:Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v1}, Lcom/facebook/payments/history/model/PaymentTransaction;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a(Landroid/content/Intent;)V

    .line 2240122
    :cond_0
    return-void
.end method
