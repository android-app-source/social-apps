.class public Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/picker/model/PickerScreenConfig;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

.field public final b:Lcom/facebook/payments/history/model/PaymentTransactions;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2240041
    new-instance v0, LX/FR6;

    invoke-direct {v0}, LX/FR6;-><init>()V

    sput-object v0, Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/FR7;)V
    .locals 1

    .prologue
    .line 2240050
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2240051
    iget-object v0, p1, LX/FR7;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-object v0, v0

    .line 2240052
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    iput-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 2240053
    iget-object v0, p1, LX/FR7;->b:Lcom/facebook/payments/history/model/PaymentTransactions;

    move-object v0, v0

    .line 2240054
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/history/model/PaymentTransactions;

    iput-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;->b:Lcom/facebook/payments/history/model/PaymentTransactions;

    .line 2240055
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2240046
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2240047
    const-class v0, Lcom/facebook/payments/picker/model/PickerScreenConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    iput-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 2240048
    const-class v0, Lcom/facebook/payments/history/model/PaymentTransactions;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/history/model/PaymentTransactions;

    iput-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;->b:Lcom/facebook/payments/history/model/PaymentTransactions;

    .line 2240049
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;
    .locals 1

    .prologue
    .line 2240056
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2240045
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2240042
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2240043
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;->b:Lcom/facebook/payments/history/model/PaymentTransactions;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2240044
    return-void
.end method
