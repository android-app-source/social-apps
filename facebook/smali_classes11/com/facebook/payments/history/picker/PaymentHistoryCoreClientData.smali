.class public Lcom/facebook/payments/history/picker/PaymentHistoryCoreClientData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/picker/model/CoreClientData;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/history/picker/PaymentHistoryCoreClientData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/history/model/PaymentTransactions;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2239976
    new-instance v0, LX/FR1;

    invoke-direct {v0}, LX/FR1;-><init>()V

    sput-object v0, Lcom/facebook/payments/history/picker/PaymentHistoryCoreClientData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/FR8;)V
    .locals 1

    .prologue
    .line 2239980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2239981
    iget-object v0, p1, LX/FR8;->a:Lcom/facebook/payments/history/model/PaymentTransactions;

    move-object v0, v0

    .line 2239982
    iput-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryCoreClientData;->a:Lcom/facebook/payments/history/model/PaymentTransactions;

    .line 2239983
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2239984
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2239985
    const-class v0, Lcom/facebook/payments/history/model/PaymentTransactions;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/history/model/PaymentTransactions;

    iput-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryCoreClientData;->a:Lcom/facebook/payments/history/model/PaymentTransactions;

    .line 2239986
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2239979
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2239977
    iget-object v0, p0, Lcom/facebook/payments/history/picker/PaymentHistoryCoreClientData;->a:Lcom/facebook/payments/history/model/PaymentTransactions;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2239978
    return-void
.end method
