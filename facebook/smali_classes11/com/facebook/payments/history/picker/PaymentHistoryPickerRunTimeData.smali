.class public Lcom/facebook/payments/history/picker/PaymentHistoryPickerRunTimeData;
.super Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/payments/picker/model/SimplePickerRunTimeData",
        "<",
        "Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;",
        "Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;",
        "Lcom/facebook/payments/history/picker/PaymentHistoryCoreClientData;",
        "LX/FRJ;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/history/picker/PaymentHistoryPickerRunTimeData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2240010
    new-instance v0, LX/FR4;

    invoke-direct {v0}, LX/FR4;-><init>()V

    sput-object v0, Lcom/facebook/payments/history/picker/PaymentHistoryPickerRunTimeData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 2240019
    invoke-direct {p0, p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;-><init>(Landroid/os/Parcel;)V

    .line 2240020
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;)V
    .locals 0

    .prologue
    .line 2240017
    invoke-direct {p0, p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;-><init>(Lcom/facebook/payments/picker/model/PickerScreenConfig;)V

    .line 2240018
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;Lcom/facebook/payments/history/picker/PaymentHistoryCoreClientData;LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;",
            "Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;",
            "Lcom/facebook/payments/history/picker/PaymentHistoryCoreClientData;",
            "LX/0P1",
            "<",
            "LX/FRJ;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2240015
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;-><init>(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)V

    .line 2240016
    return-void
.end method


# virtual methods
.method public final b()Landroid/content/Intent;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2240014
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2240012
    iget-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v0, v0

    .line 2240013
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2240011
    const/4 v0, 0x1

    return v0
.end method
