.class public Lcom/facebook/payments/history/model/PaymentProfile;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/history/model/PaymentProfile;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/payments/history/model/ProfileImage;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2239858
    new-instance v0, LX/FQt;

    invoke-direct {v0}, LX/FQt;-><init>()V

    sput-object v0, Lcom/facebook/payments/history/model/PaymentProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/FQu;)V
    .locals 1

    .prologue
    .line 2239850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2239851
    iget-object v0, p1, LX/FQu;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2239852
    iput-object v0, p0, Lcom/facebook/payments/history/model/PaymentProfile;->a:Ljava/lang/String;

    .line 2239853
    iget-object v0, p1, LX/FQu;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2239854
    iput-object v0, p0, Lcom/facebook/payments/history/model/PaymentProfile;->b:Ljava/lang/String;

    .line 2239855
    iget-object v0, p1, LX/FQu;->c:Lcom/facebook/payments/history/model/ProfileImage;

    move-object v0, v0

    .line 2239856
    iput-object v0, p0, Lcom/facebook/payments/history/model/PaymentProfile;->c:Lcom/facebook/payments/history/model/ProfileImage;

    .line 2239857
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2239845
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2239846
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/history/model/PaymentProfile;->a:Ljava/lang/String;

    .line 2239847
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/history/model/PaymentProfile;->b:Ljava/lang/String;

    .line 2239848
    const-class v0, Lcom/facebook/payments/history/model/ProfileImage;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/history/model/ProfileImage;

    iput-object v0, p0, Lcom/facebook/payments/history/model/PaymentProfile;->c:Lcom/facebook/payments/history/model/ProfileImage;

    .line 2239849
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2239840
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2239841
    iget-object v0, p0, Lcom/facebook/payments/history/model/PaymentProfile;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2239842
    iget-object v0, p0, Lcom/facebook/payments/history/model/PaymentProfile;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2239843
    iget-object v0, p0, Lcom/facebook/payments/history/model/PaymentProfile;->c:Lcom/facebook/payments/history/model/ProfileImage;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2239844
    return-void
.end method
