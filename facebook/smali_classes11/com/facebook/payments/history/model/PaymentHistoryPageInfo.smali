.class public Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2239827
    new-instance v0, LX/FQs;

    invoke-direct {v0}, LX/FQs;-><init>()V

    sput-object v0, Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2239828
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2239829
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;->a:Z

    .line 2239830
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 2239831
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2239832
    iput-boolean p1, p0, Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;->a:Z

    .line 2239833
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2239834
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2239835
    iget-boolean v0, p0, Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2239836
    return-void
.end method
