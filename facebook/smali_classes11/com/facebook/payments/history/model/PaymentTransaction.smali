.class public interface abstract Lcom/facebook/payments/history/model/PaymentTransaction;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# virtual methods
.method public abstract a()Lcom/facebook/payments/history/model/PaymentProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract b()Lcom/facebook/payments/history/model/PaymentProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract c()J
.end method

.method public abstract d()Lcom/facebook/payments/currency/CurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract e()J
.end method

.method public abstract f()LX/FQv;
.end method

.method public abstract g()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
