.class public Lcom/facebook/payments/history/model/SimplePaymentTransactions;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/history/model/PaymentTransactions;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/history/model/SimplePaymentTransactions;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/history/model/PaymentTransaction;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2239972
    new-instance v0, LX/FR0;

    invoke-direct {v0}, LX/FR0;-><init>()V

    sput-object v0, Lcom/facebook/payments/history/model/SimplePaymentTransactions;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Px;Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;)V
    .locals 0
    .param p2    # Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/history/model/PaymentTransaction;",
            ">;",
            "Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2239968
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2239969
    iput-object p1, p0, Lcom/facebook/payments/history/model/SimplePaymentTransactions;->a:LX/0Px;

    .line 2239970
    iput-object p2, p0, Lcom/facebook/payments/history/model/SimplePaymentTransactions;->b:Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;

    .line 2239971
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2239957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2239958
    const-class v0, Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransactions;->a:LX/0Px;

    .line 2239959
    const-class v0, Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;

    iput-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransactions;->b:Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;

    .line 2239960
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/history/model/PaymentTransaction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2239967
    iget-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransactions;->a:LX/0Px;

    return-object v0
.end method

.method public final b()Ljava/lang/Long;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2239966
    iget-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransactions;->a:LX/0Px;

    iget-object v1, p0, Lcom/facebook/payments/history/model/SimplePaymentTransactions;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/history/model/PaymentTransaction;

    invoke-interface {v0}, Lcom/facebook/payments/history/model/PaymentTransaction;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2239965
    iget-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransactions;->b:Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransactions;->b:Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;

    iget-boolean v0, v0, Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;->a:Z

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2239964
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2239961
    iget-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransactions;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2239962
    iget-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransactions;->b:Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2239963
    return-void
.end method
