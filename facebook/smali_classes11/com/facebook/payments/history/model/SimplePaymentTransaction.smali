.class public Lcom/facebook/payments/history/model/SimplePaymentTransaction;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/history/model/PaymentTransaction;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/history/model/SimplePaymentTransaction;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/facebook/payments/history/model/PaymentProfile;

.field private final c:Lcom/facebook/payments/history/model/PaymentProfile;

.field private final d:Lcom/facebook/payments/currency/CurrencyAmount;

.field private final e:J

.field private final f:J

.field private final g:LX/FQv;

.field private final h:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2239933
    new-instance v0, LX/FQy;

    invoke-direct {v0}, LX/FQy;-><init>()V

    sput-object v0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/FQz;)V
    .locals 4

    .prologue
    .line 2239934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2239935
    iget-object v0, p1, LX/FQz;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2239936
    iput-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->a:Ljava/lang/String;

    .line 2239937
    iget-object v0, p1, LX/FQz;->b:Lcom/facebook/payments/history/model/PaymentProfile;

    move-object v0, v0

    .line 2239938
    iput-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->b:Lcom/facebook/payments/history/model/PaymentProfile;

    .line 2239939
    iget-object v0, p1, LX/FQz;->c:Lcom/facebook/payments/history/model/PaymentProfile;

    move-object v0, v0

    .line 2239940
    iput-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->c:Lcom/facebook/payments/history/model/PaymentProfile;

    .line 2239941
    iget-object v0, p1, LX/FQz;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v0, v0

    .line 2239942
    iput-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2239943
    iget-wide v2, p1, LX/FQz;->e:J

    move-wide v0, v2

    .line 2239944
    iput-wide v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->e:J

    .line 2239945
    iget-wide v2, p1, LX/FQz;->f:J

    move-wide v0, v2

    .line 2239946
    iput-wide v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->f:J

    .line 2239947
    iget-object v0, p1, LX/FQz;->g:LX/FQv;

    move-object v0, v0

    .line 2239948
    iput-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->g:LX/FQv;

    .line 2239949
    iget-object v0, p1, LX/FQz;->h:Ljava/lang/String;

    move-object v0, v0

    .line 2239950
    iput-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->h:Ljava/lang/String;

    .line 2239951
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2239920
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2239921
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->a:Ljava/lang/String;

    .line 2239922
    const-class v0, Lcom/facebook/payments/history/model/PaymentProfile;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/history/model/PaymentProfile;

    iput-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->b:Lcom/facebook/payments/history/model/PaymentProfile;

    .line 2239923
    const-class v0, Lcom/facebook/payments/history/model/PaymentProfile;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/history/model/PaymentProfile;

    iput-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->c:Lcom/facebook/payments/history/model/PaymentProfile;

    .line 2239924
    const-class v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2239925
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->e:J

    .line 2239926
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->f:J

    .line 2239927
    const-class v0, LX/FQv;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FQv;

    iput-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->g:LX/FQv;

    .line 2239928
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->h:Ljava/lang/String;

    .line 2239929
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/history/model/PaymentProfile;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2239932
    iget-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->b:Lcom/facebook/payments/history/model/PaymentProfile;

    return-object v0
.end method

.method public final b()Lcom/facebook/payments/history/model/PaymentProfile;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2239931
    iget-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->c:Lcom/facebook/payments/history/model/PaymentProfile;

    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 2239930
    iget-wide v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->f:J

    return-wide v0
.end method

.method public final d()Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2239952
    iget-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2239907
    const/4 v0, 0x0

    return v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 2239908
    iget-wide v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->e:J

    return-wide v0
.end method

.method public final f()LX/FQv;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2239909
    iget-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->g:LX/FQv;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2239910
    iget-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2239911
    iget-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2239912
    iget-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->b:Lcom/facebook/payments/history/model/PaymentProfile;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2239913
    iget-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->c:Lcom/facebook/payments/history/model/PaymentProfile;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2239914
    iget-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2239915
    iget-wide v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2239916
    iget-wide v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->f:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2239917
    iget-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->g:LX/FQv;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2239918
    iget-object v0, p0, Lcom/facebook/payments/history/model/SimplePaymentTransaction;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2239919
    return-void
.end method
