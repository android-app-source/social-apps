.class public Lcom/facebook/payments/history/model/ProfileImage;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/history/model/ProfileImage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:I

.field private final c:I

.field private final d:F

.field private final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2239902
    new-instance v0, LX/FQw;

    invoke-direct {v0}, LX/FQw;-><init>()V

    sput-object v0, Lcom/facebook/payments/history/model/ProfileImage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/FQx;)V
    .locals 1

    .prologue
    .line 2239876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2239877
    iget-object v0, p1, LX/FQx;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2239878
    iput-object v0, p0, Lcom/facebook/payments/history/model/ProfileImage;->a:Ljava/lang/String;

    .line 2239879
    iget v0, p1, LX/FQx;->b:I

    move v0, v0

    .line 2239880
    iput v0, p0, Lcom/facebook/payments/history/model/ProfileImage;->b:I

    .line 2239881
    iget v0, p1, LX/FQx;->c:I

    move v0, v0

    .line 2239882
    iput v0, p0, Lcom/facebook/payments/history/model/ProfileImage;->c:I

    .line 2239883
    iget v0, p1, LX/FQx;->d:F

    move v0, v0

    .line 2239884
    iput v0, p0, Lcom/facebook/payments/history/model/ProfileImage;->d:F

    .line 2239885
    iget-object v0, p1, LX/FQx;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2239886
    iput-object v0, p0, Lcom/facebook/payments/history/model/ProfileImage;->e:Ljava/lang/String;

    .line 2239887
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2239895
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2239896
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/history/model/ProfileImage;->a:Ljava/lang/String;

    .line 2239897
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/history/model/ProfileImage;->b:I

    .line 2239898
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/history/model/ProfileImage;->c:I

    .line 2239899
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/payments/history/model/ProfileImage;->d:F

    .line 2239900
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/history/model/ProfileImage;->e:Ljava/lang/String;

    .line 2239901
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2239894
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2239888
    iget-object v0, p0, Lcom/facebook/payments/history/model/ProfileImage;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2239889
    iget v0, p0, Lcom/facebook/payments/history/model/ProfileImage;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2239890
    iget v0, p0, Lcom/facebook/payments/history/model/ProfileImage;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2239891
    iget v0, p0, Lcom/facebook/payments/history/model/ProfileImage;->d:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2239892
    iget-object v0, p0, Lcom/facebook/payments/history/model/ProfileImage;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2239893
    return-void
.end method
