.class public Lcom/facebook/notifications/multirow/partdefinition/NotificationsVerticalListUnitComponentGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/2km;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;

.field private final b:LX/1s9;

.field private final c:LX/DqU;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;LX/1s9;LX/DqU;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2425237
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2425238
    iput-object p1, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsVerticalListUnitComponentGroupPartDefinition;->a:Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;

    .line 2425239
    iput-object p2, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsVerticalListUnitComponentGroupPartDefinition;->b:LX/1s9;

    .line 2425240
    iput-object p3, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsVerticalListUnitComponentGroupPartDefinition;->c:LX/DqU;

    .line 2425241
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/multirow/partdefinition/NotificationsVerticalListUnitComponentGroupPartDefinition;
    .locals 6

    .prologue
    .line 2425242
    const-class v1, Lcom/facebook/notifications/multirow/partdefinition/NotificationsVerticalListUnitComponentGroupPartDefinition;

    monitor-enter v1

    .line 2425243
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsVerticalListUnitComponentGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2425244
    sput-object v2, Lcom/facebook/notifications/multirow/partdefinition/NotificationsVerticalListUnitComponentGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2425245
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2425246
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2425247
    new-instance p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsVerticalListUnitComponentGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;

    invoke-static {v0}, LX/1s9;->a(LX/0QB;)LX/1s9;

    move-result-object v4

    check-cast v4, LX/1s9;

    invoke-static {v0}, LX/DqU;->a(LX/0QB;)LX/DqU;

    move-result-object v5

    check-cast v5, LX/DqU;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsVerticalListUnitComponentGroupPartDefinition;-><init>(Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;LX/1s9;LX/DqU;)V

    .line 2425248
    move-object v0, p0

    .line 2425249
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2425250
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsVerticalListUnitComponentGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2425251
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2425252
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2425253
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2425254
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2425255
    check-cast v0, LX/9ud;

    .line 2425256
    invoke-static {p2}, LX/Cfu;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v3

    .line 2425257
    invoke-interface {v0}, LX/9ud;->aJ()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2425258
    iget-object v1, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsVerticalListUnitComponentGroupPartDefinition;->a:Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2425259
    :cond_0
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 2425260
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9uc;

    .line 2425261
    invoke-interface {v0}, LX/9ud;->aI()Z

    move-result v4

    if-eqz v4, :cond_1

    if-lez v2, :cond_1

    .line 2425262
    iget-object v4, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsVerticalListUnitComponentGroupPartDefinition;->a:Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;

    invoke-virtual {p1, v4, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2425263
    :cond_1
    new-instance v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2425264
    iget-object v5, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v5, v5

    .line 2425265
    iget-object v6, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v6, v6

    .line 2425266
    iget-object v7, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v7, v7

    .line 2425267
    invoke-direct {v4, v5, v1, v6, v7}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    .line 2425268
    invoke-interface {v1}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    .line 2425269
    iget-object v5, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsVerticalListUnitComponentGroupPartDefinition;->b:LX/1s9;

    invoke-virtual {v5, v1}, LX/1s9;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v1

    .line 2425270
    invoke-virtual {p1, v1, v4}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2425271
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2425272
    :cond_2
    invoke-interface {v0}, LX/9ud;->aG()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2425273
    iget-object v0, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsVerticalListUnitComponentGroupPartDefinition;->a:Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2425274
    :cond_3
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2425275
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2425276
    const-string v0, "SUCCESS"

    iget-object v1, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsVerticalListUnitComponentGroupPartDefinition;->c:LX/DqU;

    invoke-virtual {v1, p1}, LX/DqU;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
