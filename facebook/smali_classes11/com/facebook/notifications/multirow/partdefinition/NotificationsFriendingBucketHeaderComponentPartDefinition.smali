.class public Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/3Ci;",
        "LX/3U5;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/H5X;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3TK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/H5X;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/H5X;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3TK;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2425010
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2425011
    iput-object p2, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;->d:LX/H5X;

    .line 2425012
    iput-object p3, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;->e:LX/0Ot;

    .line 2425013
    iput-object p4, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;->f:LX/0Ot;

    .line 2425014
    return-void
.end method

.method private a(LX/1De;LX/3Ci;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/3Ci;",
            ")",
            "LX/1X1",
            "<",
            "LX/H5X;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2424982
    iget-object v0, p2, LX/3Ci;->a:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    move-object v0, v0

    .line 2424983
    iget-object v1, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;->d:LX/H5X;

    const/4 v2, 0x0

    .line 2424984
    new-instance v3, LX/H5W;

    invoke-direct {v3, v1}, LX/H5W;-><init>(LX/H5X;)V

    .line 2424985
    sget-object v4, LX/H5X;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/H5V;

    .line 2424986
    if-nez v4, :cond_0

    .line 2424987
    new-instance v4, LX/H5V;

    invoke-direct {v4}, LX/H5V;-><init>()V

    .line 2424988
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/H5V;->a$redex0(LX/H5V;LX/1De;IILX/H5W;)V

    .line 2424989
    move-object v3, v4

    .line 2424990
    move-object v2, v3

    .line 2424991
    move-object v1, v2

    .line 2424992
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->hV_()Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2424993
    iget-object v2, v1, LX/H5V;->a:LX/H5W;

    iput-object v0, v2, LX/H5W;->a:Ljava/lang/String;

    .line 2424994
    iget-object v2, v1, LX/H5V;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2424995
    move-object v0, v1

    .line 2424996
    const v1, 0x7f081180

    invoke-virtual {p1, v1}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2424997
    iget-object v2, v0, LX/H5V;->a:LX/H5W;

    iput-object v1, v2, LX/H5W;->c:Ljava/lang/String;

    .line 2424998
    move-object v0, v0

    .line 2424999
    iget v1, p2, LX/3Ci;->b:I

    move v1, v1

    .line 2425000
    iget-object v2, p2, LX/3Ci;->a:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    move-object v2, v2

    .line 2425001
    if-eqz v2, :cond_1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->FRIEND_REQUESTS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    .line 2425002
    iget-object v3, p2, LX/3Ci;->a:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    move-object v3, v3

    .line 2425003
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2425004
    new-instance v2, LX/H5k;

    invoke-direct {v2, p0, v1}, LX/H5k;-><init>(Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;I)V

    .line 2425005
    :goto_0
    move-object v1, v2

    .line 2425006
    iget-object v2, v0, LX/H5V;->a:LX/H5W;

    iput-object v1, v2, LX/H5W;->d:Landroid/view/View$OnClickListener;

    .line 2425007
    move-object v0, v0

    .line 2425008
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2425009
    return-object v0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;
    .locals 7

    .prologue
    .line 2425015
    const-class v1, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;

    monitor-enter v1

    .line 2425016
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2425017
    sput-object v2, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2425018
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2425019
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2425020
    new-instance v5, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/H5X;->a(LX/0QB;)LX/H5X;

    move-result-object v4

    check-cast v4, LX/H5X;

    const/16 v6, 0x2eb

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0xe59

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, v6, p0}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/H5X;LX/0Ot;LX/0Ot;)V

    .line 2425021
    move-object v0, v5

    .line 2425022
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2425023
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2425024
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2425025
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2424981
    check-cast p2, LX/3Ci;

    invoke-direct {p0, p1, p2}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;->a(LX/1De;LX/3Ci;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2424980
    check-cast p2, LX/3Ci;

    invoke-direct {p0, p1, p2}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;->a(LX/1De;LX/3Ci;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2424975
    check-cast p1, LX/3Ci;

    .line 2424976
    iget-object v0, p1, LX/3Ci;->a:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    move-object v0, v0

    .line 2424977
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->hV_()Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2424978
    iget-object v0, p1, LX/3Ci;->a:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    move-object v0, v0

    .line 2424979
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->hV_()Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
