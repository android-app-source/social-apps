.class public Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/1s9;

.field private final b:LX/DqU;


# direct methods
.method public constructor <init>(LX/1s9;LX/DqU;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2425233
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2425234
    iput-object p1, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionRootPartDefinition;->a:LX/1s9;

    .line 2425235
    iput-object p2, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionRootPartDefinition;->b:LX/DqU;

    .line 2425236
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionRootPartDefinition;
    .locals 5

    .prologue
    .line 2425193
    const-class v1, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionRootPartDefinition;

    monitor-enter v1

    .line 2425194
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionRootPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2425195
    sput-object v2, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionRootPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2425196
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2425197
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2425198
    new-instance p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionRootPartDefinition;

    invoke-static {v0}, LX/1s9;->a(LX/0QB;)LX/1s9;

    move-result-object v3

    check-cast v3, LX/1s9;

    invoke-static {v0}, LX/DqU;->a(LX/0QB;)LX/DqU;

    move-result-object v4

    check-cast v4, LX/DqU;

    invoke-direct {p0, v3, v4}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionRootPartDefinition;-><init>(LX/1s9;LX/DqU;)V

    .line 2425199
    move-object v0, p0

    .line 2425200
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2425201
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2425202
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2425203
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2425204
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2425205
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2425206
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 2425207
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    .line 2425208
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    .line 2425209
    iget-object v3, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionRootPartDefinition;->a:LX/1s9;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1s9;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v3

    .line 2425210
    if-eqz v3, :cond_0

    .line 2425211
    new-instance v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, p2, v1, v5, v6}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    .line 2425212
    invoke-virtual {p1, v3, v4}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2425213
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2425214
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 2425215
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2425216
    const-string v0, "SUCCESS"

    iget-object v1, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionRootPartDefinition;->b:LX/DqU;

    .line 2425217
    iget-object v2, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2425218
    instance-of v3, v2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    if-nez v3, :cond_0

    .line 2425219
    const-string v3, "ERROR_INVALID_UNIT"

    .line 2425220
    :goto_0
    move-object v1, v3

    .line 2425221
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 2425222
    :cond_0
    check-cast v2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 2425223
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v5

    .line 2425224
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2425225
    :cond_1
    const-string v3, "ERROR_INVALID_UNIT"

    goto :goto_0

    .line 2425226
    :cond_2
    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2425227
    const-string v3, "EMPTY_COMPONENTS"

    goto :goto_0

    .line 2425228
    :cond_3
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result p0

    const/4 v3, 0x0

    move v4, v3

    :goto_1
    if-ge v4, p0, :cond_5

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    .line 2425229
    iget-object p1, v1, LX/DqU;->a:LX/1s9;

    invoke-virtual {p1, v3}, LX/1s9;->a(LX/9oK;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 2425230
    const-string v3, "UNSUPPORTED_COMPONENT_STYLE"

    goto :goto_0

    .line 2425231
    :cond_4
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 2425232
    :cond_5
    const-string v3, "SUCCESS"

    goto :goto_0
.end method
