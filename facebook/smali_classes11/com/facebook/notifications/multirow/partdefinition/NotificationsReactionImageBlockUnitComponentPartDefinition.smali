.class public Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionImageBlockUnitComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/H4i;

.field private final e:LX/2d1;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2d1;LX/H4i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2425121
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2425122
    iput-object p3, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionImageBlockUnitComponentPartDefinition;->d:LX/H4i;

    .line 2425123
    iput-object p2, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionImageBlockUnitComponentPartDefinition;->e:LX/2d1;

    .line 2425124
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<",
            "LX/H4i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2425133
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v0

    .line 2425134
    const/4 v0, 0x0

    .line 2425135
    invoke-interface {v1}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2425136
    invoke-interface {v1}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2425137
    :cond_0
    iget-object v2, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionImageBlockUnitComponentPartDefinition;->d:LX/H4i;

    const/4 v3, 0x0

    .line 2425138
    new-instance v4, LX/H4h;

    invoke-direct {v4, v2}, LX/H4h;-><init>(LX/H4i;)V

    .line 2425139
    iget-object p0, v2, LX/H4i;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/H4g;

    .line 2425140
    if-nez p0, :cond_1

    .line 2425141
    new-instance p0, LX/H4g;

    invoke-direct {p0, v2}, LX/H4g;-><init>(LX/H4i;)V

    .line 2425142
    :cond_1
    invoke-static {p0, p1, v3, v3, v4}, LX/H4g;->a$redex0(LX/H4g;LX/1De;IILX/H4h;)V

    .line 2425143
    move-object v4, p0

    .line 2425144
    move-object v3, v4

    .line 2425145
    move-object v2, v3

    .line 2425146
    invoke-interface {v1}, LX/9uc;->aV()LX/5sY;

    move-result-object v3

    invoke-interface {v3}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2425147
    iget-object v4, v2, LX/H4g;->a:LX/H4h;

    iput-object v3, v4, LX/H4h;->a:Landroid/net/Uri;

    .line 2425148
    iget-object v4, v2, LX/H4g;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v4, p0}, Ljava/util/BitSet;->set(I)V

    .line 2425149
    move-object v2, v2

    .line 2425150
    invoke-interface {v1}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2425151
    iget-object v4, v2, LX/H4g;->a:LX/H4h;

    iput-object v3, v4, LX/H4h;->b:Ljava/lang/String;

    .line 2425152
    iget-object v4, v2, LX/H4g;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v4, p0}, Ljava/util/BitSet;->set(I)V

    .line 2425153
    move-object v2, v2

    .line 2425154
    iget-object v3, v2, LX/H4g;->a:LX/H4h;

    iput-object v0, v3, LX/H4h;->d:Ljava/lang/String;

    .line 2425155
    move-object v0, v2

    .line 2425156
    invoke-interface {v1}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    .line 2425157
    iget-object v3, v0, LX/H4g;->a:LX/H4h;

    iput-object v2, v3, LX/H4h;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2425158
    move-object v0, v0

    .line 2425159
    invoke-interface {v1}, LX/9uc;->y()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    .line 2425160
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->d()LX/5sY;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->d()LX/5sY;

    move-result-object v3

    invoke-interface {v3}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2425161
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->d()LX/5sY;

    move-result-object v3

    invoke-interface {v3}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2425162
    :goto_0
    move-object v2, v3

    .line 2425163
    iget-object v3, v0, LX/H4g;->a:LX/H4h;

    iput-object v2, v3, LX/H4h;->e:Landroid/net/Uri;

    .line 2425164
    move-object v0, v0

    .line 2425165
    invoke-interface {v1}, LX/9uc;->y()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    .line 2425166
    iget-object v3, v0, LX/H4g;->a:LX/H4h;

    iput-object v2, v3, LX/H4h;->j:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2425167
    move-object v0, v0

    .line 2425168
    invoke-interface {v1}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v2

    .line 2425169
    iget-object v3, v0, LX/H4g;->a:LX/H4h;

    iput-object v2, v3, LX/H4h;->g:Ljava/lang/String;

    .line 2425170
    move-object v0, v0

    .line 2425171
    iget-object v2, v0, LX/H4g;->a:LX/H4h;

    iput-object p3, v2, LX/H4h;->h:LX/2km;

    .line 2425172
    move-object v0, v0

    .line 2425173
    iget-object v2, v0, LX/H4g;->a:LX/H4h;

    iput-object p2, v2, LX/H4h;->i:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2425174
    move-object v0, v0

    .line 2425175
    invoke-interface {v1}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    .line 2425176
    iget-object v2, v0, LX/H4g;->a:LX/H4h;

    iput-object v1, v2, LX/H4h;->c:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 2425177
    iget-object v2, v0, LX/H4g;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2425178
    move-object v0, v0

    .line 2425179
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2425180
    return-object v0

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionImageBlockUnitComponentPartDefinition;
    .locals 6

    .prologue
    .line 2425182
    const-class v1, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionImageBlockUnitComponentPartDefinition;

    monitor-enter v1

    .line 2425183
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionImageBlockUnitComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2425184
    sput-object v2, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionImageBlockUnitComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2425185
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2425186
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2425187
    new-instance p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionImageBlockUnitComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v4

    check-cast v4, LX/2d1;

    invoke-static {v0}, LX/H4i;->a(LX/0QB;)LX/H4i;

    move-result-object v5

    check-cast v5, LX/H4i;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionImageBlockUnitComponentPartDefinition;-><init>(Landroid/content/Context;LX/2d1;LX/H4i;)V

    .line 2425188
    move-object v0, p0

    .line 2425189
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2425190
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionImageBlockUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2425191
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2425192
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private h()Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Z"
        }
    .end annotation

    .prologue
    .line 2425181
    iget-object v0, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionImageBlockUnitComponentPartDefinition;->e:LX/2d1;

    invoke-virtual {v0}, LX/2d1;->b()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2425131
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionImageBlockUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2425132
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionImageBlockUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 2425130
    invoke-direct {p0}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionImageBlockUnitComponentPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final synthetic a(LX/1Pn;)Z
    .locals 1

    .prologue
    .line 2425129
    invoke-direct {p0}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionImageBlockUnitComponentPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2425126
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2425127
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2425128
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aV()LX/5sY;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aV()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2425125
    const/4 v0, 0x0

    return-object v0
.end method
