.class public Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketDividerComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/3Cj;",
        "LX/3U5;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H5P;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2424904
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2424905
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2424906
    iput-object v0, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketDividerComponentPartDefinition;->d:LX/0Ot;

    .line 2424907
    return-void
.end method

.method private a(LX/1De;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2424908
    iget-object v0, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketDividerComponentPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H5P;

    const/4 v1, 0x0

    .line 2424909
    new-instance v2, LX/H5O;

    invoke-direct {v2, v0}, LX/H5O;-><init>(LX/H5P;)V

    .line 2424910
    sget-object p0, LX/H5P;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/H5N;

    .line 2424911
    if-nez p0, :cond_0

    .line 2424912
    new-instance p0, LX/H5N;

    invoke-direct {p0}, LX/H5N;-><init>()V

    .line 2424913
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/H5N;->a$redex0(LX/H5N;LX/1De;IILX/H5O;)V

    .line 2424914
    move-object v2, p0

    .line 2424915
    move-object v1, v2

    .line 2424916
    move-object v0, v1

    .line 2424917
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketDividerComponentPartDefinition;
    .locals 4

    .prologue
    .line 2424918
    const-class v1, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketDividerComponentPartDefinition;

    monitor-enter v1

    .line 2424919
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketDividerComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2424920
    sput-object v2, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketDividerComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2424921
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2424922
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2424923
    new-instance p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketDividerComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketDividerComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2424924
    const/16 v3, 0x2ad9

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 2424925
    iput-object v3, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketDividerComponentPartDefinition;->d:LX/0Ot;

    .line 2424926
    move-object v0, p0

    .line 2424927
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2424928
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketDividerComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2424929
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2424930
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2424931
    invoke-direct {p0, p1}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketDividerComponentPartDefinition;->a(LX/1De;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2424932
    invoke-direct {p0, p1}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketDividerComponentPartDefinition;->a(LX/1De;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2424933
    const/4 v0, 0x1

    return v0
.end method
