.class public Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketFooterComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/3Ch;",
        "LX/3U5;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H5T;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2424967
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2424968
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2424969
    iput-object v0, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketFooterComponentPartDefinition;->d:LX/0Ot;

    .line 2424970
    return-void
.end method

.method private a(LX/1De;LX/3Ch;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/3Ch;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2424934
    iget-object v0, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketFooterComponentPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H5T;

    const/4 v1, 0x0

    .line 2424935
    new-instance v2, LX/H5S;

    invoke-direct {v2, v0}, LX/H5S;-><init>(LX/H5T;)V

    .line 2424936
    sget-object p0, LX/H5T;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/H5R;

    .line 2424937
    if-nez p0, :cond_0

    .line 2424938
    new-instance p0, LX/H5R;

    invoke-direct {p0}, LX/H5R;-><init>()V

    .line 2424939
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/H5R;->a$redex0(LX/H5R;LX/1De;IILX/H5S;)V

    .line 2424940
    move-object v2, p0

    .line 2424941
    move-object v1, v2

    .line 2424942
    move-object v0, v1

    .line 2424943
    iget v1, p2, LX/3Ch;->b:I

    move v1, v1

    .line 2424944
    iget-object v2, v0, LX/H5R;->a:LX/H5S;

    iput v1, v2, LX/H5S;->a:I

    .line 2424945
    iget-object v2, v0, LX/H5R;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2424946
    move-object v0, v0

    .line 2424947
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketFooterComponentPartDefinition;
    .locals 4

    .prologue
    .line 2424954
    const-class v1, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketFooterComponentPartDefinition;

    monitor-enter v1

    .line 2424955
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketFooterComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2424956
    sput-object v2, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketFooterComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2424957
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2424958
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2424959
    new-instance p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketFooterComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketFooterComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2424960
    const/16 v3, 0x2adb

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 2424961
    iput-object v3, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketFooterComponentPartDefinition;->d:LX/0Ot;

    .line 2424962
    move-object v0, p0

    .line 2424963
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2424964
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketFooterComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2424965
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2424966
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2424953
    check-cast p2, LX/3Ch;

    invoke-direct {p0, p1, p2}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketFooterComponentPartDefinition;->a(LX/1De;LX/3Ch;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2424952
    check-cast p2, LX/3Ch;

    invoke-direct {p0, p1, p2}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketFooterComponentPartDefinition;->a(LX/1De;LX/3Ch;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2424948
    check-cast p1, LX/3Ch;

    .line 2424949
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->FRIEND_REQUESTS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    .line 2424950
    iget-object v1, p1, LX/3Ch;->a:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    move-object v1, v1

    .line 2424951
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
