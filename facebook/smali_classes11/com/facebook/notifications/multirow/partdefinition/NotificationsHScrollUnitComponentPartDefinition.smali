.class public Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/2ko;",
        ":",
        "LX/2kl;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/H5n;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/E4S;",
            ">;"
        }
    .end annotation
.end field

.field private static h:LX/0Xm;


# instance fields
.field private final b:LX/2dq;

.field public final c:LX/1vi;

.field private final d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation
.end field

.field public final e:LX/1s9;

.field private final f:LX/DqU;

.field public final g:LX/1Qx;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2425120
    new-instance v0, LX/H5l;

    invoke-direct {v0}, LX/H5l;-><init>()V

    sput-object v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/2dq;LX/1vi;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/1s9;LX/DqU;LX/1Qx;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2425112
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2425113
    iput-object p1, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->b:LX/2dq;

    .line 2425114
    iput-object p2, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->c:LX/1vi;

    .line 2425115
    iput-object p3, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2425116
    iput-object p4, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->e:LX/1s9;

    .line 2425117
    iput-object p5, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->f:LX/DqU;

    .line 2425118
    iput-object p6, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->g:LX/1Qx;

    .line 2425119
    return-void
.end method

.method private a(LX/0Px;LX/E2b;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/2eJ;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/9uc;",
            ">;",
            "LX/E2b;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/2eJ",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation

    .prologue
    .line 2425059
    new-instance v0, LX/H5m;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/H5m;-><init>(Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;LX/0Px;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;LX/E2b;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;
    .locals 10

    .prologue
    .line 2425101
    const-class v1, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;

    monitor-enter v1

    .line 2425102
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2425103
    sput-object v2, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2425104
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2425105
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2425106
    new-instance v3, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v4

    check-cast v4, LX/2dq;

    invoke-static {v0}, LX/1vi;->a(LX/0QB;)LX/1vi;

    move-result-object v5

    check-cast v5, LX/1vi;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, LX/1s9;->a(LX/0QB;)LX/1s9;

    move-result-object v7

    check-cast v7, LX/1s9;

    invoke-static {v0}, LX/DqU;->a(LX/0QB;)LX/DqU;

    move-result-object v8

    check-cast v8, LX/DqU;

    invoke-static {v0}, LX/1Qu;->a(LX/0QB;)LX/1Qx;

    move-result-object v9

    check-cast v9, LX/1Qx;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;-><init>(LX/2dq;LX/1vi;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/1s9;LX/DqU;LX/1Qx;)V

    .line 2425107
    move-object v0, v3

    .line 2425108
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2425109
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2425110
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2425111
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2425100
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2425076
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 2425077
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    new-array v1, v5, [I

    const v2, 0x7f01073e

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2425078
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v6

    .line 2425079
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2425080
    new-instance v3, LX/E2b;

    .line 2425081
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2425082
    invoke-direct {v3, v0}, LX/E2b;-><init>(Ljava/lang/String;)V

    .line 2425083
    invoke-static {p2}, LX/Cfu;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v0

    .line 2425084
    if-nez v0, :cond_0

    .line 2425085
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2425086
    :cond_0
    move-object v4, v0

    .line 2425087
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1625

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v0

    .line 2425088
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v1

    if-le v1, v5, :cond_1

    iget-object v1, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->b:LX/2dq;

    int-to-float v0, v0

    const/high16 v2, 0x41000000    # 8.0f

    add-float/2addr v0, v2

    sget-object v2, LX/2eF;->a:LX/1Ua;

    invoke-virtual {v1, v0, v2, v5}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v1

    :goto_0
    move-object v0, p3

    .line 2425089
    check-cast v0, LX/1Pr;

    invoke-interface {v0, v3, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2c;

    .line 2425090
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v2

    .line 2425091
    iput v2, v0, LX/E2c;->c:I

    .line 2425092
    iget-object v7, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    move-object v2, p3

    check-cast v2, LX/1Pr;

    invoke-interface {v2, v3, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/E2c;

    .line 2425093
    iget v5, v2, LX/E2c;->d:I

    move v2, v5

    .line 2425094
    invoke-direct {p0, v4, v3, p2, p3}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->a(LX/0Px;LX/E2b;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/2eJ;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/graphql/model/BaseFeedUnit;->g()Ljava/lang/String;

    move-result-object v4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v7, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2425095
    new-instance v0, LX/Cg2;

    .line 2425096
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2425097
    invoke-direct {v0, v1}, LX/Cg2;-><init>(Ljava/lang/String;)V

    .line 2425098
    new-instance v1, LX/H5n;

    check-cast p3, LX/2ko;

    invoke-interface {p3, p2}, LX/2ko;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-direct {v1, v2, v6, v0}, LX/H5n;-><init>(Landroid/graphics/drawable/Drawable;ILX/Cg2;)V

    return-object v1

    .line 2425099
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->b:LX/2dq;

    sget-object v1, LX/2eF;->a:LX/1Ua;

    invoke-virtual {v0, v1}, LX/2dq;->a(LX/1Ua;)LX/2eF;

    move-result-object v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x6af8abfa

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2425069
    check-cast p2, LX/H5n;

    check-cast p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    const/4 v2, 0x0

    .line 2425070
    iget-object v1, p2, LX/H5n;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {p4, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2425071
    iget v1, p2, LX/H5n;->b:I

    invoke-virtual {p4, v1, v2, v2, v2}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->setPadding(IIII)V

    .line 2425072
    iget-object v1, p2, LX/H5n;->c:LX/Cg2;

    .line 2425073
    iput-object p4, v1, LX/Cg2;->b:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2425074
    iget-object v1, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->c:LX/1vi;

    iget-object v2, p2, LX/H5n;->c:LX/Cg2;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2425075
    const/16 v1, 0x1f

    const v2, 0x5ded8a25

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2425065
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2425066
    const-string v0, "SUCCESS"

    iget-object v1, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->f:LX/DqU;

    invoke-virtual {v1, p1}, LX/DqU;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2425067
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2425068
    instance-of v0, v0, LX/9ud;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2425060
    check-cast p2, LX/H5n;

    .line 2425061
    iget-object v0, p2, LX/H5n;->c:LX/Cg2;

    const/4 v1, 0x0

    .line 2425062
    iput-object v1, v0, LX/Cg2;->b:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2425063
    iget-object v0, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->c:LX/1vi;

    iget-object v1, p2, LX/H5n;->c:LX/Cg2;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2425064
    return-void
.end method
