.class public Lcom/facebook/notifications/multirow/partdefinition/NotificationsActionListComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/H5G;

.field private final e:LX/2d1;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2d1;LX/H5G;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2424888
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2424889
    iput-object p3, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsActionListComponentPartDefinition;->d:LX/H5G;

    .line 2424890
    iput-object p2, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsActionListComponentPartDefinition;->e:LX/2d1;

    .line 2424891
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<",
            "LX/H4i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2424852
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2424853
    iget-object v1, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsActionListComponentPartDefinition;->d:LX/H5G;

    const/4 v2, 0x0

    .line 2424854
    new-instance v3, LX/H5F;

    invoke-direct {v3, v1}, LX/H5F;-><init>(LX/H5G;)V

    .line 2424855
    iget-object p0, v1, LX/H5G;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/H5E;

    .line 2424856
    if-nez p0, :cond_0

    .line 2424857
    new-instance p0, LX/H5E;

    invoke-direct {p0, v1}, LX/H5E;-><init>(LX/H5G;)V

    .line 2424858
    :cond_0
    invoke-static {p0, p1, v2, v2, v3}, LX/H5E;->a$redex0(LX/H5E;LX/1De;IILX/H5F;)V

    .line 2424859
    move-object v3, p0

    .line 2424860
    move-object v2, v3

    .line 2424861
    move-object v1, v2

    .line 2424862
    invoke-interface {v0}, LX/9uc;->l()LX/0Px;

    move-result-object v2

    .line 2424863
    iget-object v3, v1, LX/H5E;->a:LX/H5F;

    iput-object v2, v3, LX/H5F;->a:LX/0Px;

    .line 2424864
    iget-object v3, v1, LX/H5E;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v3, p0}, Ljava/util/BitSet;->set(I)V

    .line 2424865
    move-object v1, v1

    .line 2424866
    iget-object v2, v1, LX/H5E;->a:LX/H5F;

    iput-object p3, v2, LX/H5F;->d:LX/2km;

    .line 2424867
    iget-object v2, v1, LX/H5E;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2424868
    move-object v1, v1

    .line 2424869
    iget-object v2, v1, LX/H5E;->a:LX/H5F;

    iput-object p2, v2, LX/H5F;->e:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2424870
    iget-object v2, v1, LX/H5E;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2424871
    move-object v1, v1

    .line 2424872
    invoke-interface {v0}, LX/9uc;->aJ()Z

    move-result v2

    .line 2424873
    iget-object v3, v1, LX/H5E;->a:LX/H5F;

    iput-boolean v2, v3, LX/H5F;->b:Z

    .line 2424874
    move-object v1, v1

    .line 2424875
    invoke-interface {v0}, LX/9uc;->aG()Z

    move-result v0

    .line 2424876
    iget-object v2, v1, LX/H5E;->a:LX/H5F;

    iput-boolean v0, v2, LX/H5F;->c:Z

    .line 2424877
    move-object v0, v1

    .line 2424878
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2424879
    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/multirow/partdefinition/NotificationsActionListComponentPartDefinition;
    .locals 6

    .prologue
    .line 2424893
    const-class v1, Lcom/facebook/notifications/multirow/partdefinition/NotificationsActionListComponentPartDefinition;

    monitor-enter v1

    .line 2424894
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsActionListComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2424895
    sput-object v2, Lcom/facebook/notifications/multirow/partdefinition/NotificationsActionListComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2424896
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2424897
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2424898
    new-instance p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsActionListComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v4

    check-cast v4, LX/2d1;

    invoke-static {v0}, LX/H5G;->a(LX/0QB;)LX/H5G;

    move-result-object v5

    check-cast v5, LX/H5G;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsActionListComponentPartDefinition;-><init>(Landroid/content/Context;LX/2d1;LX/H5G;)V

    .line 2424899
    move-object v0, p0

    .line 2424900
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2424901
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsActionListComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2424902
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2424903
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private h()Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Z"
        }
    .end annotation

    .prologue
    .line 2424892
    iget-object v0, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsActionListComponentPartDefinition;->e:LX/2d1;

    invoke-virtual {v0}, LX/2d1;->b()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2424886
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsActionListComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2424887
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsActionListComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 2424885
    invoke-direct {p0}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsActionListComponentPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final synthetic a(LX/1Pn;)Z
    .locals 1

    .prologue
    .line 2424884
    invoke-direct {p0}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsActionListComponentPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2424881
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2424882
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2424883
    invoke-interface {v0}, LX/9uc;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2424880
    const/4 v0, 0x0

    return-object v0
.end method
