.class public Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/2kk;",
        ":",
        "LX/2kl;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static i:LX/0Xm;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/3Cm;

.field private final d:LX/3D1;

.field private final e:LX/2jO;

.field private final f:LX/11S;

.field private final g:LX/1rU;

.field private final h:LX/2ks;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2424234
    const-class v0, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/3Cm;LX/2ks;LX/3D1;LX/2jO;LX/11S;LX/1rU;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/3Cm;",
            "LX/2ks;",
            "LX/3D1;",
            "LX/2jO;",
            "LX/11S;",
            "LX/1rU;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2424235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2424236
    iput-object p3, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->h:LX/2ks;

    .line 2424237
    iput-object p1, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->b:LX/0Or;

    .line 2424238
    iput-object p2, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->c:LX/3Cm;

    .line 2424239
    iput-object p4, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->d:LX/3D1;

    .line 2424240
    iput-object p5, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->e:LX/2jO;

    .line 2424241
    iput-object p6, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->f:LX/11S;

    .line 2424242
    iput-object p7, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->g:LX/1rU;

    .line 2424243
    return-void
.end method

.method public static a(Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;LX/1De;LX/2nq;ZLX/1Ad;)LX/1Dh;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2424244
    const/4 v1, 0x0

    .line 2424245
    invoke-interface {p2}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {p2}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2424246
    invoke-interface {p2}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->a()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;

    .line 2424247
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->c()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->e()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2424248
    invoke-interface {p2}, LX/2nq;->k()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->e()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2424249
    :goto_0
    move-object v2, v0

    .line 2424250
    invoke-interface {p2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->af()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->af()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLIcon;->j()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2424251
    :goto_1
    if-eqz p3, :cond_2

    const v0, 0x7f0e05ee

    .line 2424252
    :goto_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x1

    const v5, 0x7f0b0a7d

    invoke-interface {v3, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v4

    invoke-virtual {p4, v1}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v1

    sget-object v5, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v5}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v4, 0x7f0b0a74

    invoke-interface {v1, v4}, LX/1Di;->i(I)LX/1Di;

    move-result-object v1

    const v4, 0x7f0b0a74

    invoke-interface {v1, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    const/4 v4, 0x5

    const v5, 0x7f0b0a72

    invoke-interface {v1, v4, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1, v7, v0}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v3

    invoke-interface {p2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v4

    invoke-static {p0, v4, v5}, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->a(Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-interface {v1, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    .line 2424253
    invoke-static {p2}, LX/BE6;->a(LX/2nq;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 2424254
    invoke-static {p1, v7, v0}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, " "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f081150

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-interface {v1, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-static {p1, v7, v0}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    .line 2424255
    const v2, 0x972d635

    const/4 v4, 0x0

    invoke-static {p1, v2, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 2424256
    invoke-interface {v0, v2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-interface {v3, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2424257
    :cond_0
    return-object v1

    .line 2424258
    :cond_1
    const/4 v0, 0x0

    move-object v1, v0

    goto/16 :goto_1

    .line 2424259
    :cond_2
    const v0, 0x7f0e05ef

    goto/16 :goto_2

    .line 2424260
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->c()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;
    .locals 11

    .prologue
    .line 2424261
    const-class v1, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;

    monitor-enter v1

    .line 2424262
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2424263
    sput-object v2, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2424264
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2424265
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2424266
    new-instance v3, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;

    const/16 v4, 0x509

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/3Cm;->a(LX/0QB;)LX/3Cm;

    move-result-object v5

    check-cast v5, LX/3Cm;

    const-class v6, LX/2ks;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/2ks;

    invoke-static {v0}, LX/3D1;->a(LX/0QB;)LX/3D1;

    move-result-object v7

    check-cast v7, LX/3D1;

    invoke-static {v0}, LX/2jO;->a(LX/0QB;)LX/2jO;

    move-result-object v8

    check-cast v8, LX/2jO;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v9

    check-cast v9, LX/11S;

    invoke-static {v0}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v10

    check-cast v10, LX/1rU;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;-><init>(LX/0Or;LX/3Cm;LX/2ks;LX/3D1;LX/2jO;LX/11S;LX/1rU;)V

    .line 2424267
    move-object v0, v3

    .line 2424268
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2424269
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2424270
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2424271
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2424272
    iget-object v0, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->f:LX/11S;

    sget-object v1, LX/1lB;->NOTIFICATIONS_STREAM_RELATIVE_STYLE:LX/1lB;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    invoke-interface {v0, v1, v2, v3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/2nq;LX/1Pn;)Z
    .locals 6
    .param p2    # LX/2nq;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pn;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/2nq;",
            "TE;)Z"
        }
    .end annotation

    .prologue
    .line 2424273
    iget-object v0, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->d:LX/3D1;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->e:LX/2jO;

    invoke-interface {p2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/2jO;->a(Ljava/lang/String;Ljava/lang/String;)LX/3D3;

    move-result-object v4

    check-cast p3, LX/2kk;

    invoke-interface {p2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p3, v1}, LX/2kk;->e_(Ljava/lang/String;)I

    move-result v5

    iget-object v1, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->g:LX/1rU;

    invoke-virtual {v1}, LX/1rU;->c()Z

    move-object v1, p2

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, LX/3D1;->a(LX/2nq;Landroid/content/Context;Landroid/view/View;LX/3D3;I)V

    .line 2424274
    const/4 v0, 0x1

    return v0
.end method
