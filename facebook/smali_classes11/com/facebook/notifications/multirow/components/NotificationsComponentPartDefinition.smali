.class public Lcom/facebook/notifications/multirow/components/NotificationsComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/2kk;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/2nq;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/H5g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/H5g",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/H5g;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2424231
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2424232
    iput-object p2, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentPartDefinition;->d:LX/H5g;

    .line 2424233
    return-void
.end method

.method private a(LX/1De;LX/2nq;LX/1Pn;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/2nq;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2424214
    iget-object v0, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentPartDefinition;->d:LX/H5g;

    const/4 v1, 0x0

    .line 2424215
    new-instance v2, LX/H5f;

    invoke-direct {v2, v0}, LX/H5f;-><init>(LX/H5g;)V

    .line 2424216
    iget-object p0, v0, LX/H5g;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/H5e;

    .line 2424217
    if-nez p0, :cond_0

    .line 2424218
    new-instance p0, LX/H5e;

    invoke-direct {p0, v0}, LX/H5e;-><init>(LX/H5g;)V

    .line 2424219
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/H5e;->a$redex0(LX/H5e;LX/1De;IILX/H5f;)V

    .line 2424220
    move-object v2, p0

    .line 2424221
    move-object v1, v2

    .line 2424222
    move-object v0, v1

    .line 2424223
    iget-object v1, v0, LX/H5e;->a:LX/H5f;

    iput-object p2, v1, LX/H5f;->a:LX/2nq;

    .line 2424224
    iget-object v1, v0, LX/H5e;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2424225
    move-object v0, v0

    .line 2424226
    iget-object v1, v0, LX/H5e;->a:LX/H5f;

    iput-object p3, v1, LX/H5f;->b:LX/1Pn;

    .line 2424227
    iget-object v1, v0, LX/H5e;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2424228
    move-object v0, v0

    .line 2424229
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/multirow/components/NotificationsComponentPartDefinition;
    .locals 5

    .prologue
    .line 2424202
    const-class v1, Lcom/facebook/notifications/multirow/components/NotificationsComponentPartDefinition;

    monitor-enter v1

    .line 2424203
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/multirow/components/NotificationsComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2424204
    sput-object v2, Lcom/facebook/notifications/multirow/components/NotificationsComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2424205
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2424206
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2424207
    new-instance p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/H5g;->a(LX/0QB;)LX/H5g;

    move-result-object v4

    check-cast v4, LX/H5g;

    invoke-direct {p0, v3, v4}, Lcom/facebook/notifications/multirow/components/NotificationsComponentPartDefinition;-><init>(Landroid/content/Context;LX/H5g;)V

    .line 2424208
    move-object v0, p0

    .line 2424209
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2424210
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/multirow/components/NotificationsComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2424211
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2424212
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2424230
    check-cast p2, LX/2nq;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/notifications/multirow/components/NotificationsComponentPartDefinition;->a(LX/1De;LX/2nq;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2424213
    check-cast p2, LX/2nq;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/notifications/multirow/components/NotificationsComponentPartDefinition;->a(LX/1De;LX/2nq;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2424200
    check-cast p1, LX/2nq;

    .line 2424201
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2424198
    check-cast p1, LX/2nq;

    .line 2424199
    new-instance v0, LX/H5M;

    invoke-direct {v0, p0, p1}, LX/H5M;-><init>(Lcom/facebook/notifications/multirow/components/NotificationsComponentPartDefinition;LX/2nq;)V

    return-object v0
.end method
