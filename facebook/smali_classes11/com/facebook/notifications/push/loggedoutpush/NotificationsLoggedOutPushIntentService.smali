.class public Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;
.super LX/1ZN;
.source ""


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/8D3;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/17Y;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2425353
    const-class v0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 2425354
    return-void
.end method

.method private static a(Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;Landroid/content/Context;LX/0Or;LX/8D3;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/8D3;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/17Y;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2425352
    iput-object p1, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;->c:LX/8D3;

    iput-object p4, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;->d:Lcom/facebook/content/SecureContextHelper;

    iput-object p5, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;->e:LX/17Y;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;

    const-class v1, Landroid/content/Context;

    invoke-interface {v5, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v2, 0x15e7

    invoke-static {v5, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {v5}, LX/8D3;->a(LX/0QB;)LX/8D3;

    move-result-object v3

    check-cast v3, LX/8D3;

    invoke-static {v5}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v5}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v5

    check-cast v5, LX/17Y;

    invoke-static/range {v0 .. v5}, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;->a(Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;Landroid/content/Context;LX/0Or;LX/8D3;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V

    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/high16 v4, 0x10000000

    .line 2425355
    const-string v0, "landing_experience"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    move-result-object v1

    .line 2425356
    iget-object v0, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2425357
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;->a:Landroid/content/Context;

    const-class v2, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2425358
    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 2425359
    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2425360
    iget-object v1, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2425361
    :goto_0
    return-void

    .line 2425362
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;->e:LX/17Y;

    iget-object v2, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;->a:Landroid/content/Context;

    sget-object v3, LX/0ax;->fl:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2425363
    invoke-virtual {v2, p1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 2425364
    const-string v3, "logged_in_user_id"

    iget-object v0, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2425365
    const-string v0, "after_login_redirect_to_notifications_extra"

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2425366
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->DIRECT_LOGIN:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    if-ne v1, v0, :cond_1

    .line 2425367
    const-string v0, "user_id_to_log_in"

    const-string v1, "uid"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2425368
    :cond_1
    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2425369
    iget-object v0, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;->a:Landroid/content/Context;

    invoke-interface {v0, v2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x2af9a2cf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2425345
    if-nez p1, :cond_0

    .line 2425346
    const/16 v0, 0x25

    const v2, -0x4de3c944

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2425347
    :goto_0
    return-void

    .line 2425348
    :cond_0
    iget-object v2, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;->c:LX/8D3;

    const-string v0, "uid"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "ndid"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "landing_experience"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2425349
    iget-object v6, v2, LX/8D3;->a:LX/0Zb;

    const-string v7, "logged_out_push_click"

    invoke-static {v7}, LX/8D3;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "uid"

    invoke-virtual {v7, v8, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "ndid"

    invoke-virtual {v7, v8, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "landing_experience"

    invoke-virtual {v7, v8, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "logged_in_user_id"

    invoke-virtual {v7, v8, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v6, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2425350
    invoke-direct {p0, p1}, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;->b(Landroid/content/Intent;)V

    .line 2425351
    const v0, 0x7f851f1c

    invoke-static {v0, v1}, LX/02F;->d(II)V

    goto :goto_0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x16431ab7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2425342
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 2425343
    invoke-static {p0, p0}, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2425344
    const/16 v1, 0x25

    const v2, 0x2fb3417d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
