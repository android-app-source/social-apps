.class public Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/GvB;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:LX/H5p;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public r:LX/H5u;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public s:LX/8D3;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2425426
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;LX/GvB;LX/H5p;LX/H5u;LX/8D3;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;",
            "Lcom/facebook/auth/login/ipc/LaunchAuthActivityUtil;",
            "LX/H5p;",
            "LX/H5u;",
            "LX/8D3;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2425380
    iput-object p1, p0, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;->p:LX/GvB;

    iput-object p2, p0, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;->q:LX/H5p;

    iput-object p3, p0, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;->r:LX/H5u;

    iput-object p4, p0, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;->s:LX/8D3;

    iput-object p5, p0, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;->t:LX/0Or;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;

    invoke-static {v5}, LX/GvB;->b(LX/0QB;)LX/GvB;

    move-result-object v1

    check-cast v1, LX/GvB;

    new-instance v3, LX/H5p;

    invoke-direct {v3}, LX/H5p;-><init>()V

    invoke-static {v5}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v2, v3, LX/H5p;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object v2, v3

    check-cast v2, LX/H5p;

    invoke-static {v5}, LX/H5u;->a(LX/0QB;)LX/H5u;

    move-result-object v3

    check-cast v3, LX/H5u;

    invoke-static {v5}, LX/8D3;->a(LX/0QB;)LX/8D3;

    move-result-object v4

    check-cast v4, LX/8D3;

    const/16 v6, 0x15e7

    invoke-static {v5, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;->a(Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;LX/GvB;LX/H5p;LX/H5u;LX/8D3;LX/0Or;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;)V
    .locals 8

    .prologue
    .line 2425418
    invoke-virtual {p0}, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2425419
    iget-object v1, p0, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;->q:LX/H5p;

    const-string v2, "uid"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ndid"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;->c(Landroid/content/Intent;)Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    move-result-object v4

    iget-object v0, p0, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;->t:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2425420
    iget-object v5, v1, LX/H5p;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    sget-object v6, LX/0hM;->S:LX/0Tn;

    invoke-interface {v5, v6, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v5

    sget-object v6, LX/0hM;->T:LX/0Tn;

    invoke-interface {v5, v6, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v5

    sget-object v6, LX/0hM;->U:LX/0Tn;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v5

    sget-object v6, LX/0hM;->V:LX/0Tn;

    invoke-interface {v5, v6, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v5

    sget-object v6, LX/0hM;->R:LX/0Tn;

    const/4 v7, 0x1

    invoke-interface {v5, v6, v7}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v5

    .line 2425421
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->DIRECT_LOGIN:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    if-eq v4, v6, :cond_0

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->INTERSTITIAL:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    if-ne v4, v6, :cond_1

    .line 2425422
    :cond_0
    sget-object v6, LX/2ss;->d:LX/0Tn;

    invoke-interface {v5, v6, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 2425423
    :cond_1
    invoke-interface {v5}, LX/0hN;->commit()V

    .line 2425424
    iget-object v0, p0, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;->p:LX/GvB;

    invoke-virtual {v0, p0}, LX/GvB;->a(Landroid/content/Context;)V

    .line 2425425
    return-void
.end method

.method private static c(Landroid/content/Intent;)Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;
    .locals 1

    .prologue
    .line 2425417
    const-string v0, "landing_experience"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2425381
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2425382
    invoke-static {p0, p0}, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2425383
    invoke-virtual {p0}, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2425384
    invoke-static {v0}, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;->c(Landroid/content/Intent;)Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->INTERSTITIAL:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    if-eq v1, v2, :cond_0

    .line 2425385
    invoke-static {p0}, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;->a$redex0(Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;)V

    .line 2425386
    :goto_0
    return-void

    .line 2425387
    :cond_0
    const-string v1, "uid"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2425388
    const-string v2, "landing_interstitial_text"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2425389
    new-instance v3, LX/1De;

    invoke-virtual {p0}, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/1De;-><init>(Landroid/content/Context;)V

    .line 2425390
    new-instance v4, Lcom/facebook/components/ComponentView;

    invoke-direct {v4, v3}, Lcom/facebook/components/ComponentView;-><init>(LX/1De;)V

    .line 2425391
    iget-object v5, p0, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;->r:LX/H5u;

    const/4 v6, 0x0

    .line 2425392
    new-instance v7, LX/H5t;

    invoke-direct {v7, v5}, LX/H5t;-><init>(LX/H5u;)V

    .line 2425393
    sget-object p1, LX/H5u;->a:LX/0Zi;

    invoke-virtual {p1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/H5s;

    .line 2425394
    if-nez p1, :cond_1

    .line 2425395
    new-instance p1, LX/H5s;

    invoke-direct {p1}, LX/H5s;-><init>()V

    .line 2425396
    :cond_1
    invoke-static {p1, v3, v6, v6, v7}, LX/H5s;->a$redex0(LX/H5s;LX/1De;IILX/H5t;)V

    .line 2425397
    move-object v7, p1

    .line 2425398
    move-object v6, v7

    .line 2425399
    move-object v5, v6

    .line 2425400
    iget-object v6, v5, LX/H5s;->a:LX/H5t;

    iput-object v1, v6, LX/H5t;->a:Ljava/lang/String;

    .line 2425401
    iget-object v6, v5, LX/H5s;->d:Ljava/util/BitSet;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2425402
    move-object v1, v5

    .line 2425403
    iget-object v5, v1, LX/H5s;->a:LX/H5t;

    iput-object v2, v5, LX/H5t;->b:Ljava/lang/String;

    .line 2425404
    iget-object v5, v1, LX/H5s;->d:Ljava/util/BitSet;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 2425405
    move-object v1, v1

    .line 2425406
    new-instance v2, LX/H5r;

    invoke-direct {v2, p0, v0}, LX/H5r;-><init>(Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;Landroid/content/Intent;)V

    .line 2425407
    iget-object v0, v1, LX/H5s;->a:LX/H5t;

    iput-object v2, v0, LX/H5t;->c:Landroid/view/View$OnClickListener;

    .line 2425408
    iget-object v0, v1, LX/H5s;->d:Ljava/util/BitSet;

    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Ljava/util/BitSet;->set(I)V

    .line 2425409
    move-object v0, v1

    .line 2425410
    new-instance v1, LX/H5q;

    invoke-direct {v1, p0}, LX/H5q;-><init>(Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;)V

    .line 2425411
    iget-object v2, v0, LX/H5s;->a:LX/H5t;

    iput-object v1, v2, LX/H5t;->d:Landroid/view/View$OnClickListener;

    .line 2425412
    iget-object v2, v0, LX/H5s;->d:Ljava/util/BitSet;

    const/4 v5, 0x3

    invoke-virtual {v2, v5}, Ljava/util/BitSet;->set(I)V

    .line 2425413
    move-object v0, v0

    .line 2425414
    invoke-static {v3, v0}, LX/1dV;->a(LX/1De;LX/1X5;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    .line 2425415
    invoke-virtual {v4, v0}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 2425416
    invoke-virtual {p0, v4}, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialActivity;->setContentView(Landroid/view/View;)V

    goto :goto_0
.end method
