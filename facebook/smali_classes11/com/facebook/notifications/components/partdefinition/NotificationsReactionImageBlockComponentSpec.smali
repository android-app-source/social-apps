.class public Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile d:Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;


# instance fields
.field private final b:LX/E1f;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2423372
    const-class v0, Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/E1f;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/E1f;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2423390
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2423391
    iput-object p1, p0, Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;->b:LX/E1f;

    .line 2423392
    iput-object p2, p0, Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;->c:LX/0Or;

    .line 2423393
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;
    .locals 5

    .prologue
    .line 2423394
    sget-object v0, Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;->d:Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;

    if-nez v0, :cond_1

    .line 2423395
    const-class v1, Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;

    monitor-enter v1

    .line 2423396
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;->d:Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2423397
    if-eqz v2, :cond_0

    .line 2423398
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2423399
    new-instance v4, Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;

    invoke-static {v0}, LX/E1f;->a(LX/0QB;)LX/E1f;

    move-result-object v3

    check-cast v3, LX/E1f;

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;-><init>(LX/E1f;LX/0Or;)V

    .line 2423400
    move-object v0, v4

    .line 2423401
    sput-object v0, Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;->d:Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2423402
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2423403
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2423404
    :cond_1
    sget-object v0, Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;->d:Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;

    return-object v0

    .line 2423405
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2423406
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 9
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/2km;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            "Ljava/lang/String;",
            "TE;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2423373
    if-eqz p2, :cond_0

    if-eqz p4, :cond_0

    if-nez p5, :cond_1

    .line 2423374
    :cond_0
    :goto_0
    return-void

    .line 2423375
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;->b:LX/E1f;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v1

    if-nez v1, :cond_3

    const/4 v3, 0x0

    :goto_1
    move-object v1, p4

    check-cast v1, LX/2kp;

    invoke-interface {v1}, LX/2kp;->t()LX/2jY;

    move-result-object v1

    .line 2423376
    iget-object v4, v1, LX/2jY;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2423377
    move-object v1, p4

    check-cast v1, LX/2kp;

    invoke-interface {v1}, LX/2kp;->t()LX/2jY;

    move-result-object v1

    .line 2423378
    iget-object v5, v1, LX/2jY;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2423379
    iget-object v1, p5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v6, v1

    .line 2423380
    iget-object v1, p5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v7, v1

    .line 2423381
    move-object v1, p4

    check-cast v1, LX/2kn;

    invoke-interface {v1}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v8

    move-object v1, p2

    invoke-virtual/range {v0 .. v8}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)LX/Cfl;

    move-result-object v0

    .line 2423382
    if-eqz p5, :cond_2

    if-eqz v0, :cond_2

    iget-object v1, v0, LX/Cfl;->d:Landroid/content/Intent;

    if-eqz v1, :cond_2

    .line 2423383
    invoke-static {p5}, LX/E1q;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/2nq;

    move-result-object v1

    .line 2423384
    if-eqz v1, :cond_2

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2423385
    iget-object v2, v0, LX/Cfl;->d:Landroid/content/Intent;

    const-string v3, "notification_id"

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v1}, LX/BCw;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2423386
    :cond_2
    iget-object v1, p5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2423387
    iget-object v2, p5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2423388
    invoke-interface {p4, v1, v2, p3, v0}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    goto :goto_0

    .line 2423389
    :cond_3
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method
