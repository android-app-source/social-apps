.class public Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final k:Ljava/lang/String;


# instance fields
.field public a:LX/ADE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/GNY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Or;
    .annotation runtime Lcom/facebook/base/activity/FragmentBaseActivity;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/1e6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/GGW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

.field public m:Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;

.field public n:Landroid/widget/LinearLayout;

.field private o:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public p:LX/62O;

.field private q:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

.field public final r:LX/1DI;

.field private s:LX/GNc;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2346430
    const-class v0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2346426
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2346427
    new-instance v0, LX/GNh;

    invoke-direct {v0, p0}, LX/GNh;-><init>(Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;)V

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->r:LX/1DI;

    .line 2346428
    new-instance v0, LX/GNd;

    invoke-direct {v0, p0}, LX/GNd;-><init>(Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;)V

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->s:LX/GNc;

    .line 2346429
    return-void
.end method

.method public static a$redex0(Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;Z)V
    .locals 3

    .prologue
    .line 2346415
    sget-object v0, LX/0ax;->cL:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2346416
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 2346417
    const-string v1, "refresh_feed"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2346418
    const-string v1, "jump_to_top"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2346419
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2346420
    const-string v2, "tabbar_target_intent"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2346421
    const/high16 v0, 0x4000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2346422
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2346423
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2346424
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2346425
    return-void
.end method

.method private d()V
    .locals 5

    .prologue
    .line 2346402
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_ads_experience_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2346403
    if-nez v0, :cond_0

    .line 2346404
    invoke-static {p0}, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->k(Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;)V

    .line 2346405
    :goto_0
    return-void

    .line 2346406
    :cond_0
    iget-object v1, p0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->a:LX/ADE;

    const/4 v2, 0x0

    new-instance v3, LX/GNe;

    invoke-direct {v3, p0}, LX/GNe;-><init>(Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;)V

    .line 2346407
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2346408
    const-string v4, "no callback"

    invoke-static {v3, v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2346409
    new-instance v4, LX/ADF;

    invoke-direct {v4}, LX/ADF;-><init>()V

    move-object v4, v4

    .line 2346410
    const-string p0, "experienceID"

    invoke-virtual {v4, p0, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2346411
    const-string p0, "scale"

    invoke-virtual {v4, p0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2346412
    iget-object p0, v1, LX/ADE;->a:LX/0tX;

    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    .line 2346413
    iget-object p0, v1, LX/ADE;->b:Ljava/util/concurrent/Executor;

    invoke-static {v4, v3, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2346414
    goto :goto_0
.end method

.method public static e(Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;)V
    .locals 4

    .prologue
    .line 2346399
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->p:LX/62O;

    invoke-virtual {v0}, LX/62O;->a()V

    .line 2346400
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->d:LX/1Ck;

    iget-object v1, p0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->l:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->j()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/GNf;

    invoke-direct {v2, p0}, LX/GNf;-><init>(Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;)V

    new-instance v3, LX/GNg;

    invoke-direct {v3, p0}, LX/GNg;-><init>(Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2346401
    return-void
.end method

.method public static k(Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;)V
    .locals 3

    .prologue
    .line 2346397
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f083769

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2346398
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2346391
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2346392
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    new-instance v5, LX/ADE;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-direct {v5, v3, v4}, LX/ADE;-><init>(LX/0tX;Ljava/util/concurrent/Executor;)V

    move-object v3, v5

    check-cast v3, LX/ADE;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/GNY;->c(LX/0QB;)LX/GNY;

    move-result-object v5

    check-cast v5, LX/GNY;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    const/16 v9, 0xb

    invoke-static {v0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/1e6;->b(LX/0QB;)LX/1e6;

    move-result-object v10

    check-cast v10, LX/1e6;

    invoke-static {v0}, LX/GGW;->a(LX/0QB;)LX/GGW;

    move-result-object v11

    check-cast v11, LX/GGW;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v3, v2, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->a:LX/ADE;

    iput-object v4, v2, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->b:LX/0tX;

    iput-object v5, v2, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->c:LX/GNY;

    iput-object v6, v2, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->d:LX/1Ck;

    iput-object v7, v2, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->e:LX/03V;

    iput-object v8, v2, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->f:Lcom/facebook/content/SecureContextHelper;

    iput-object v9, v2, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->g:LX/0Or;

    iput-object v10, v2, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->h:LX/1e6;

    iput-object v11, v2, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->i:LX/GGW;

    iput-object v0, v2, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->j:Ljava/util/concurrent/Executor;

    .line 2346393
    if-eqz p1, :cond_0

    .line 2346394
    const-string v0, "loading_indicator_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->q:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    .line 2346395
    :cond_0
    new-instance v0, LX/62O;

    iget-object v1, p0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->q:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    iget-object v2, p0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->r:LX/1DI;

    invoke-direct {v0, v1, v2}, LX/62O;-><init>(Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;LX/1DI;)V

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->p:LX/62O;

    .line 2346396
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x77a07eb

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2346378
    const v1, 0x7f0300c4

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x13e53166

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2346388
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2346389
    const-string v0, "loading_indicator_state"

    iget-object v1, p0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->q:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2346390
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2346379
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2346380
    const v0, 0x7f0d0500

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->m:Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;

    .line 2346381
    const v0, 0x7f0d0502

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->n:Landroid/widget/LinearLayout;

    .line 2346382
    const v0, 0x7f0d0501

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->o:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2346383
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->d()V

    .line 2346384
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->m:Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;

    iget-object v1, p0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->s:LX/GNc;

    .line 2346385
    iput-object v1, v0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->b:LX/GNc;

    .line 2346386
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->p:LX/62O;

    iget-object v1, p0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->o:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v1}, LX/62O;->a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;)V

    .line 2346387
    return-void
.end method
