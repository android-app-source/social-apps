.class public Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/GNc;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private f:Landroid/widget/Button;

.field private g:Landroid/widget/Button;

.field private h:Landroid/widget/Button;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2346443
    const-class v0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2346481
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2346482
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2346479
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2346480
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2346476
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2346477
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->a()V

    .line 2346478
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2346467
    const v0, 0x7f0300c5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2346468
    const v0, 0x7f0d0504

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->c:Landroid/widget/TextView;

    .line 2346469
    const v0, 0x7f0d0505

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->d:Landroid/widget/TextView;

    .line 2346470
    const v0, 0x7f0d0503

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2346471
    const v0, 0x7f0d0506

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->g:Landroid/widget/Button;

    .line 2346472
    const v0, 0x7f0d0507

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->h:Landroid/widget/Button;

    .line 2346473
    const v0, 0x7f0d0508

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->f:Landroid/widget/Button;

    .line 2346474
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->b()V

    .line 2346475
    return-void
.end method

.method private b()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 2346483
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->g:Landroid/widget/Button;

    new-instance v1, LX/GNi;

    invoke-direct {v1, p0}, LX/GNi;-><init>(Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2346484
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->h:Landroid/widget/Button;

    new-instance v1, LX/GNj;

    invoke-direct {v1, p0}, LX/GNj;-><init>(Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2346485
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->f:Landroid/widget/Button;

    new-instance v1, LX/GNk;

    invoke-direct {v1, p0}, LX/GNk;-><init>(Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2346486
    return-void
.end method


# virtual methods
.method public setAdsInjectIntroViewListener(LX/GNc;)V
    .locals 0

    .prologue
    .line 2346465
    iput-object p1, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->b:LX/GNc;

    .line 2346466
    return-void
.end method

.method public setConfirmationTextForActiveSharedAds(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2346455
    invoke-virtual {p0}, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083763

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v4

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2346456
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2346457
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e0bbd

    invoke-direct {v0, v2, v3}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v0, v4, v2, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2346458
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2346459
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083765

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2346460
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2346461
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->g:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 2346462
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->h:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 2346463
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->f:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 2346464
    return-void
.end method

.method public setConfirmationTextForInactiveSharedAds(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 2346446
    invoke-virtual {p0}, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083764

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v5

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2346447
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2346448
    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e0bbd

    invoke-direct {v2, v3, v4}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, p1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, p1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v0, v4

    invoke-virtual {v1, v2, v3, v0, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2346449
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2346450
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->d:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2346451
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->g:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 2346452
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->h:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 2346453
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->f:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 2346454
    return-void
.end method

.method public setProfilePicture(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 2346444
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2346445
    return-void
.end method
