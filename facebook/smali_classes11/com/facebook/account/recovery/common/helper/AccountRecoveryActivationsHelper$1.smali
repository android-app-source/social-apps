.class public final Lcom/facebook/account/recovery/common/helper/AccountRecoveryActivationsHelper$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/4hA;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Tn;

.field public final synthetic b:LX/2t2;


# direct methods
.method public constructor <init>(LX/2t2;LX/0Tn;)V
    .locals 0

    .prologue
    .line 2326278
    iput-object p1, p0, Lcom/facebook/account/recovery/common/helper/AccountRecoveryActivationsHelper$1;->b:LX/2t2;

    iput-object p2, p0, Lcom/facebook/account/recovery/common/helper/AccountRecoveryActivationsHelper$1;->a:LX/0Tn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2326279
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2326280
    check-cast p1, LX/4hA;

    .line 2326281
    iget-object v0, p0, Lcom/facebook/account/recovery/common/helper/AccountRecoveryActivationsHelper$1;->b:LX/2t2;

    iget-object v1, p1, LX/4hA;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/account/recovery/common/helper/AccountRecoveryActivationsHelper$1;->b:LX/2t2;

    iget-object v2, v2, LX/2t2;->d:LX/0dC;

    invoke-virtual {v2}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/account/recovery/common/helper/AccountRecoveryActivationsHelper$1;->a:LX/0Tn;

    .line 2326282
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2326283
    :cond_0
    :goto_0
    return-void

    .line 2326284
    :cond_1
    new-instance v4, Lcom/facebook/account/recovery/common/protocol/AccountRecoveryActivationsParams;

    invoke-direct {v4, v1, v2}, Lcom/facebook/account/recovery/common/protocol/AccountRecoveryActivationsParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2326285
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2326286
    const-string v5, "accountRecoveryAppActivationsParamsKey"

    invoke-virtual {v6, v5, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2326287
    iget-object v4, v0, LX/2t2;->e:LX/0aG;

    const-string v5, "account_recovery_app_activations"

    sget-object v7, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v8, LX/2t2;

    invoke-static {v8}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v8

    const v9, -0x1adf754f

    invoke-static/range {v4 .. v9}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    move-result-object v4

    .line 2326288
    new-instance v5, LX/GB0;

    invoke-direct {v5, v0, v3}, LX/GB0;-><init>(LX/2t2;LX/0Tn;)V

    iget-object v6, v0, LX/2t2;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
