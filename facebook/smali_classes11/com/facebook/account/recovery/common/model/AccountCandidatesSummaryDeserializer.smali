.class public Lcom/facebook/account/recovery/common/model/AccountCandidatesSummaryDeserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# static fields
.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/common/json/FbJsonField;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2326590
    const-class v0, Lcom/facebook/account/recovery/common/model/AccountCandidatesSummary;

    new-instance v1, Lcom/facebook/account/recovery/common/model/AccountCandidatesSummaryDeserializer;

    invoke-direct {v1}, Lcom/facebook/account/recovery/common/model/AccountCandidatesSummaryDeserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2326591
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2326592
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    .line 2326593
    const-class v0, Lcom/facebook/account/recovery/common/model/AccountCandidatesSummary;

    invoke-virtual {p0, v0}, Lcom/facebook/common/json/FbJsonDeserializer;->init(Ljava/lang/Class;)V

    .line 2326594
    return-void
.end method


# virtual methods
.method public final getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    .locals 3

    .prologue
    .line 2326595
    const-class v1, Lcom/facebook/account/recovery/common/model/AccountCandidatesSummaryDeserializer;

    monitor-enter v1

    .line 2326596
    :try_start_0
    sget-object v0, Lcom/facebook/account/recovery/common/model/AccountCandidatesSummaryDeserializer;->a:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 2326597
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/facebook/account/recovery/common/model/AccountCandidatesSummaryDeserializer;->a:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2326598
    :cond_0
    const/4 v0, -0x1

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    :goto_0
    packed-switch v0, :pswitch_data_1

    .line 2326599
    invoke-super {p0, p1}, Lcom/facebook/common/json/FbJsonDeserializer;->getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    monitor-exit v1

    .line 2326600
    :goto_1
    return-object v0

    .line 2326601
    :cond_2
    sget-object v0, Lcom/facebook/account/recovery/common/model/AccountCandidatesSummaryDeserializer;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/json/FbJsonField;

    .line 2326602
    if-eqz v0, :cond_0

    .line 2326603
    monitor-exit v1

    goto :goto_1

    .line 2326604
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2326605
    :pswitch_0
    :try_start_3
    const-string v2, "total_count"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2326606
    :pswitch_1
    const-class v0, Lcom/facebook/account/recovery/common/model/AccountCandidatesSummary;

    const-string v2, "totalCount"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Field;)Lcom/facebook/common/json/FbJsonField;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 2326607
    sget-object v2, Lcom/facebook/account/recovery/common/model/AccountCandidatesSummaryDeserializer;->a:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2326608
    monitor-exit v1

    goto :goto_1

    .line 2326609
    :catch_0
    move-exception v0

    .line 2326610
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch -0x184df3ac
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
