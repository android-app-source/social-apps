.class public Lcom/facebook/account/recovery/common/model/AccountCandidatesSummary;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/account/recovery/common/model/AccountCandidatesSummaryDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/account/recovery/common/model/AccountCandidatesSummary;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private totalCount:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "total_count"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2326586
    const-class v0, Lcom/facebook/account/recovery/common/model/AccountCandidatesSummaryDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2326585
    new-instance v0, LX/GB9;

    invoke-direct {v0}, LX/GB9;-><init>()V

    sput-object v0, Lcom/facebook/account/recovery/common/model/AccountCandidatesSummary;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2326582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2326583
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/account/recovery/common/model/AccountCandidatesSummary;->totalCount:I

    .line 2326584
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2326587
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2326588
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/account/recovery/common/model/AccountCandidatesSummary;->totalCount:I

    .line 2326589
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2326581
    iget v0, p0, Lcom/facebook/account/recovery/common/model/AccountCandidatesSummary;->totalCount:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2326580
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2326578
    iget v0, p0, Lcom/facebook/account/recovery/common/model/AccountCandidatesSummary;->totalCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2326579
    return-void
.end method
