.class public Lcom/facebook/account/recovery/common/model/AccountCandidateContactPointList;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/account/recovery/common/model/AccountCandidateContactPointListDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/account/recovery/common/model/AccountCandidateContactPointList;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private candidateContactPoints:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "data"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/account/recovery/common/model/AccountCandidateContactPoint;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2326459
    const-class v0, Lcom/facebook/account/recovery/common/model/AccountCandidateContactPointListDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2326458
    new-instance v0, LX/GB6;

    invoke-direct {v0}, LX/GB6;-><init>()V

    sput-object v0, Lcom/facebook/account/recovery/common/model/AccountCandidateContactPointList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2326446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2326447
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/common/model/AccountCandidateContactPointList;->candidateContactPoints:Ljava/util/List;

    .line 2326448
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2326455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2326456
    sget-object v0, Lcom/facebook/account/recovery/common/model/AccountCandidateContactPoint;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/common/model/AccountCandidateContactPointList;->candidateContactPoints:Ljava/util/List;

    .line 2326457
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/account/recovery/common/model/AccountCandidateContactPoint;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2326452
    iget-object v0, p0, Lcom/facebook/account/recovery/common/model/AccountCandidateContactPointList;->candidateContactPoints:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2326453
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2326454
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/account/recovery/common/model/AccountCandidateContactPointList;->candidateContactPoints:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2326451
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2326449
    iget-object v0, p0, Lcom/facebook/account/recovery/common/model/AccountCandidateContactPointList;->candidateContactPoints:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2326450
    return-void
.end method
