.class public final Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod_ResultDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAccountCandidates:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "data"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/account/recovery/common/model/AccountCandidateModel;",
            ">;"
        }
    .end annotation
.end field

.field private mSummary:Lcom/facebook/account/recovery/common/model/AccountCandidatesSummary;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "summary"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2326687
    const-class v0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod_ResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2326686
    new-instance v0, LX/GBF;

    invoke-direct {v0}, LX/GBF;-><init>()V

    sput-object v0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2326682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2326683
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;->mAccountCandidates:Ljava/util/List;

    .line 2326684
    new-instance v0, Lcom/facebook/account/recovery/common/model/AccountCandidatesSummary;

    invoke-direct {v0}, Lcom/facebook/account/recovery/common/model/AccountCandidatesSummary;-><init>()V

    iput-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;->mSummary:Lcom/facebook/account/recovery/common/model/AccountCandidatesSummary;

    .line 2326685
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2326679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2326680
    sget-object v0, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;->mAccountCandidates:Ljava/util/List;

    .line 2326681
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2326678
    iget-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;->mSummary:Lcom/facebook/account/recovery/common/model/AccountCandidatesSummary;

    invoke-virtual {v0}, Lcom/facebook/account/recovery/common/model/AccountCandidatesSummary;->a()I

    move-result v0

    return v0
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/account/recovery/common/model/AccountCandidateModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2326675
    iget-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;->mAccountCandidates:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2326676
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2326677
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;->mAccountCandidates:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2326671
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2326672
    iget-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;->mAccountCandidates:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2326673
    iget-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;->mSummary:Lcom/facebook/account/recovery/common/model/AccountCandidatesSummary;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2326674
    return-void
.end method
