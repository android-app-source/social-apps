.class public Lcom/facebook/account/recovery/common/protocol/AccountRecoveryActivationsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/account/recovery/common/protocol/AccountRecoveryActivationsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2326667
    new-instance v0, LX/GBE;

    invoke-direct {v0}, LX/GBE;-><init>()V

    sput-object v0, Lcom/facebook/account/recovery/common/protocol/AccountRecoveryActivationsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2326659
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2326660
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoveryActivationsParams;->a:Ljava/lang/String;

    .line 2326661
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoveryActivationsParams;->b:Ljava/lang/String;

    .line 2326662
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2326663
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2326664
    iput-object p1, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoveryActivationsParams;->a:Ljava/lang/String;

    .line 2326665
    iput-object p2, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoveryActivationsParams;->b:Ljava/lang/String;

    .line 2326666
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2326658
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2326655
    iget-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoveryActivationsParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2326656
    iget-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoveryActivationsParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2326657
    return-void
.end method
