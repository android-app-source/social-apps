.class public Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2326691
    const-class v0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;

    sput-object v0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->a:Ljava/lang/Class;

    .line 2326692
    new-instance v0, LX/GBG;

    invoke-direct {v0}, LX/GBG;-><init>()V

    sput-object v0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2326693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2326694
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->b:Ljava/lang/String;

    .line 2326695
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->c:Ljava/lang/String;

    .line 2326696
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->d:Ljava/lang/String;

    .line 2326697
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->e:Ljava/lang/String;

    .line 2326698
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->f:Ljava/lang/String;

    .line 2326699
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2326700
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2326701
    iput-object p1, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->b:Ljava/lang/String;

    .line 2326702
    iput-object p2, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->c:Ljava/lang/String;

    .line 2326703
    iput-object p3, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->d:Ljava/lang/String;

    .line 2326704
    iput-object p4, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->e:Ljava/lang/String;

    .line 2326705
    iput-object p5, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->f:Ljava/lang/String;

    .line 2326706
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2326707
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2326708
    iget-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2326709
    iget-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2326710
    iget-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2326711
    iget-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2326712
    iget-object v0, p0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2326713
    return-void
.end method
