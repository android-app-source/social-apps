.class public Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/2Ne;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/widget/TextView;

.field public c:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public d:Landroid/widget/TextView;

.field public e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field public g:Ljava/lang/String;

.field public h:LX/2Ng;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2327898
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2327895
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2327896
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;

    invoke-static {v0}, LX/2Ne;->a(LX/0QB;)LX/2Ne;

    move-result-object v0

    check-cast v0, LX/2Ne;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->a:LX/2Ne;

    .line 2327897
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/16 v2, 0x2a

    const v3, 0x7f86c6ca

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2327851
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/app/Activity;)V

    .line 2327852
    :try_start_0
    move-object v0, p1

    check-cast v0, LX/2Ng;

    move-object v1, v0

    iput-object v1, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->h:LX/2Ng;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2327853
    const v1, -0x39dd2c23

    invoke-static {v1, v2}, LX/02F;->f(II)V

    return-void

    .line 2327854
    :catch_0
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " must implement OnConfirmationCodeValidatedListener"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    const v3, -0x4ff4c966

    invoke-static {v3, v2}, LX/02F;->f(II)V

    throw v1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x10c10662

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327894
    const v1, 0x7f03001b

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x3ea8803c

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7d4c574b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327889
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2327890
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2327891
    if-nez v1, :cond_0

    .line 2327892
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x6e6c6aa0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2327893
    :cond_0
    const v2, 0x7f083521

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 2327855
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2327856
    const v0, 0x7f0d036e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->b:Landroid/widget/TextView;

    .line 2327857
    const v0, 0x7f0d036f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->c:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2327858
    const v0, 0x7f0d0372

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->d:Landroid/widget/TextView;

    .line 2327859
    const v0, 0x7f0d0374

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->e:Landroid/widget/TextView;

    .line 2327860
    const v0, 0x7f0d0373

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->f:Landroid/widget/TextView;

    .line 2327861
    const v0, 0x7f0d0370

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2327862
    const v0, 0x7f0d0371

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2327863
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->b:Landroid/widget/TextView;

    const v1, 0x7f083547

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2327864
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2327865
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->f:Landroid/widget/TextView;

    const v1, 0x7f083549

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2327866
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2327867
    if-eqz v0, :cond_0

    .line 2327868
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2327869
    const-string v1, "account_profile"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

    .line 2327870
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2327871
    const-string v2, "auto_identify"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 2327872
    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->c:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2327873
    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->c:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->c()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2327874
    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->c:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->d()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2327875
    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->c:Lcom/facebook/fbui/widget/contentview/ContentView;

    const/4 p1, 0x0

    invoke-virtual {v2, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2327876
    if-eqz v1, :cond_1

    .line 2327877
    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->e:Landroid/widget/TextView;

    const/4 p1, 0x0

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2327878
    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->e:Landroid/widget/TextView;

    new-instance p1, LX/GBx;

    invoke-direct {p1, p0}, LX/GBx;-><init>(Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;)V

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2327879
    :goto_0
    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->d:Landroid/widget/TextView;

    const p1, 0x7f083548

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(I)V

    .line 2327880
    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->d:Landroid/widget/TextView;

    new-instance p1, LX/GBy;

    invoke-direct {p1, p0, v0}, LX/GBy;-><init>(Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;Lcom/facebook/account/recovery/common/model/AccountCandidateModel;)V

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2327881
    invoke-virtual {v0}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->g:Ljava/lang/String;

    .line 2327882
    :cond_0
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->a:LX/2Ne;

    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->g:Ljava/lang/String;

    .line 2327883
    iget-object v2, v0, LX/2Ne;->a:LX/0Zb;

    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p1, LX/GB4;->FDR_VIEWED:LX/GB4;

    invoke-virtual {p1}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "account_recovery"

    .line 2327884
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2327885
    move-object p0, p0

    .line 2327886
    const-string p1, "crypted_id"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const/4 p1, 0x1

    invoke-interface {v2, p0, p1}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2327887
    return-void

    .line 2327888
    :cond_1
    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;->e:Landroid/widget/TextView;

    const/16 p1, 0x8

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
