.class public Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/GAv;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final l:[Ljava/lang/String;


# instance fields
.field public A:Landroid/widget/Button;

.field private B:Landroid/view/View;

.field private C:Landroid/widget/TextView;

.field private D:Landroid/widget/TextView;

.field private E:Landroid/widget/Button;

.field public F:Z

.field public G:LX/GBg;

.field public H:Ljava/lang/String;

.field public I:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public J:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public K:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public L:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public M:Z

.field public N:Z

.field private O:Z

.field public P:LX/GaP;

.field public Q:LX/GaP;

.field public R:J

.field public S:J

.field public T:Z

.field private U:Z

.field private V:Z

.field public a:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/2Ne;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/GAz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation runtime Lcom/facebook/account/recovery/annotations/IsEmailListedBeforeSmsEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/GaQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/2Dt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/GB2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/2Ng;

.field public n:Landroid/view/View;

.field private o:Landroid/widget/TextView;

.field private p:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public q:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

.field public r:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

.field public s:Landroid/widget/TextView;

.field private t:Landroid/widget/TextView;

.field public u:Landroid/view/View;

.field public v:Landroid/view/ViewStub;

.field public w:Landroid/widget/TextView;

.field public x:Landroid/view/View;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/ui/search/SearchEditText;

.field public z:Landroid/widget/Button;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2327343
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.READ_SMS"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->l:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2327312
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2327313
    sget-object v0, LX/GBg;->SMS:LX/GBg;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->G:LX/GBg;

    return-void
.end method

.method public static A(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V
    .locals 4

    .prologue
    .line 2327344
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->G:LX/GBg;

    sget-object v1, LX/GBg;->SMS:LX/GBg;

    if-ne v0, v1, :cond_0

    .line 2327345
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->d:LX/2Ne;

    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->H:Ljava/lang/String;

    .line 2327346
    iget-object v2, v0, LX/2Ne;->a:LX/0Zb;

    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p0, LX/GB4;->SENT_CODE_SMS:LX/GB4;

    invoke-virtual {p0}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "account_recovery"

    .line 2327347
    iput-object p0, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2327348
    move-object v3, v3

    .line 2327349
    const-string p0, "crypted_id"

    invoke-virtual {v3, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const/4 p0, 0x1

    invoke-interface {v2, v3, p0}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2327350
    :goto_0
    return-void

    .line 2327351
    :cond_0
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->d:LX/2Ne;

    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->H:Ljava/lang/String;

    .line 2327352
    iget-object v2, v0, LX/2Ne;->a:LX/0Zb;

    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p0, LX/GB4;->SENT_CODE_EMAIL:LX/GB4;

    invoke-virtual {p0}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "account_recovery"

    .line 2327353
    iput-object p0, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2327354
    move-object v3, v3

    .line 2327355
    const-string p0, "crypted_id"

    invoke-virtual {v3, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const/4 p0, 0x1

    invoke-interface {v2, v3, p0}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2327356
    goto :goto_0
.end method

.method public static B(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 2327357
    iput-wide v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->R:J

    .line 2327358
    iput-wide v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->S:J

    .line 2327359
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->T:Z

    .line 2327360
    return-void
.end method

.method private a(Lcom/facebook/account/recovery/common/model/AccountCandidateModel;)V
    .locals 2

    .prologue
    .line 2327361
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->s:Landroid/widget/TextView;

    new-instance v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;-><init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;Lcom/facebook/account/recovery/common/model/AccountCandidateModel;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2327362
    return-void
.end method

.method private static a(Ljava/util/List;Lcom/facebook/fbui/widget/contentview/CheckedContentView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/fbui/widget/contentview/CheckedContentView;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 2327363
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2327364
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setVisibility(I)V

    .line 2327365
    :cond_0
    :goto_0
    return-void

    .line 2327366
    :cond_1
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2327367
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 2327368
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2327369
    const v0, 0x7f0e0143

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaTextAppearance(I)V

    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2327445
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v2}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2327446
    const v0, 0x7f0d0371

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->q:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 2327447
    const v0, 0x7f0d0370

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->r:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 2327448
    sget-object v0, LX/GBg;->EMAIL:LX/GBg;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->G:LX/GBg;

    .line 2327449
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->r:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 2327450
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->r:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const v1, 0x7f0209a0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setCheckMarkDrawable(I)V

    .line 2327451
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->q:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 2327452
    :goto_0
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->q:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const v1, 0x7f083523

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(I)V

    .line 2327453
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->r:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const v1, 0x7f083524

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(I)V

    .line 2327454
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-eq v0, v1, :cond_0

    .line 2327455
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v2}, LX/03R;->asBoolean(Z)Z

    move-result v0

    .line 2327456
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->d:LX/2Ne;

    .line 2327457
    iget-object v2, v1, LX/2Ne;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Cy;

    sget-object p0, LX/GB4;->EMAIL_LISTED_BEFORE_SMS:LX/GB4;

    invoke-virtual {p0}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0, v0}, LX/2Cy;->a(Ljava/lang/String;Z)V

    .line 2327458
    :cond_0
    return-void

    .line 2327459
    :cond_1
    const v0, 0x7f0d0370

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->q:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 2327460
    const v0, 0x7f0d0371

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->r:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    goto :goto_0
.end method

.method public static b(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;LX/GBh;)V
    .locals 8

    .prologue
    .line 2327370
    invoke-static {p0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->z(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327371
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2327372
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->G:LX/GBg;

    sget-object v1, LX/GBg;->SMS:LX/GBg;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->K:Ljava/util/List;

    .line 2327373
    :goto_0
    new-instance v1, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySendConfirmationCodeMethod$Params;

    iget-object v3, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->H:Ljava/lang/String;

    invoke-direct {v1, v3, v0}, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySendConfirmationCodeMethod$Params;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 2327374
    const-string v0, "accountRecoverySendCodeParamsKey"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2327375
    iget-object v6, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->b:LX/1Ck;

    const-string v7, "send_code_method_tag"

    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->a:LX/0aG;

    const-string v1, "account_recovery_send_code"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v4, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x5a19d4a8

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2327376
    sget-object v1, LX/GBh;->RESEND_BUTTON:LX/GBh;

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->j:LX/2Dt;

    invoke-virtual {v1}, LX/2Dt;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2327377
    new-instance v1, LX/GBT;

    invoke-direct {v1, p0}, LX/GBT;-><init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327378
    :goto_1
    move-object v1, v1

    .line 2327379
    invoke-virtual {v6, v7, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2327380
    invoke-static {p0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->w$redex0(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327381
    return-void

    .line 2327382
    :cond_0
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->I:Ljava/util/List;

    goto :goto_0

    :cond_1
    new-instance v1, LX/GBU;

    invoke-direct {v1, p0}, LX/GBU;-><init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    goto :goto_1
.end method

.method public static b(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;Ljava/lang/String;)V
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 2327414
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->N:Z

    .line 2327415
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->Q:LX/GaP;

    if-eqz v0, :cond_0

    .line 2327416
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->Q:LX/GaP;

    invoke-virtual {v0}, LX/GaP;->d()V

    .line 2327417
    iput-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->Q:LX/GaP;

    .line 2327418
    :cond_0
    invoke-static {p0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->z(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327419
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->P:LX/GaP;

    if-eqz v0, :cond_1

    .line 2327420
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->P:LX/GaP;

    invoke-virtual {v0}, LX/GaP;->d()V

    .line 2327421
    iput-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->P:LX/GaP;

    .line 2327422
    :cond_1
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->x:Landroid/view/View;

    if-nez v0, :cond_2

    .line 2327423
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->w:Landroid/widget/TextView;

    const v1, 0x7f08352c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2327424
    :goto_0
    const/4 v7, 0x0

    .line 2327425
    iget-wide v2, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->S:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 2327426
    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->d:LX/2Ne;

    iget-object v3, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->H:Ljava/lang/String;

    iget-wide v4, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->S:J

    iget-boolean v6, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->T:Z

    .line 2327427
    iget-object v8, v2, LX/2Ne;->a:LX/0Zb;

    new-instance v9, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v10, LX/GB4;->CODE_SUBMITTED:LX/GB4;

    invoke-virtual {v10}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v10, "account_recovery"

    .line 2327428
    iput-object v10, v9, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2327429
    move-object v9, v9

    .line 2327430
    const-string v10, "crypted_id"

    invoke-virtual {v9, v10, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "sms_wait_time"

    invoke-virtual {v9, v10, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "paused"

    invoke-virtual {v9, v10, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const/4 v10, 0x1

    invoke-interface {v8, v9, v10}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2327431
    :goto_1
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2327432
    new-instance v2, Lcom/facebook/account/recovery/common/protocol/AccountRecoveryValidateCodeMethod$Params;

    iget-object v3, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->H:Ljava/lang/String;

    const-string v5, ""

    invoke-direct {v2, v3, p1, v5, v7}, Lcom/facebook/account/recovery/common/protocol/AccountRecoveryValidateCodeMethod$Params;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2327433
    const-string v3, "accountRecoveryValidateCodeParamsKey"

    invoke-virtual {v4, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2327434
    iput-boolean v7, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->F:Z

    .line 2327435
    iget-object v8, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->b:LX/1Ck;

    const/4 v9, 0x0

    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->a:LX/0aG;

    const-string v3, "account_recovery_validate_code"

    sget-object v5, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v6, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-static {v6}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    const v7, -0xa247e4c

    invoke-static/range {v2 .. v7}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    move-result-object v2

    new-instance v3, LX/GBX;

    invoke-direct {v3, p0, p1}, LX/GBX;-><init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;Ljava/lang/String;)V

    invoke-virtual {v8, v9, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2327436
    return-void

    .line 2327437
    :cond_2
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->b(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;Z)V

    .line 2327438
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->y:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/ui/search/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2327439
    :cond_3
    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->d:LX/2Ne;

    iget-object v3, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->H:Ljava/lang/String;

    .line 2327440
    iget-object v4, v2, LX/2Ne;->a:LX/0Zb;

    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v6, LX/GB4;->CODE_SUBMITTED:LX/GB4;

    invoke-virtual {v6}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v6, "account_recovery"

    .line 2327441
    iput-object v6, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2327442
    move-object v5, v5

    .line 2327443
    const-string v6, "crypted_id"

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v4, v5, v6}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2327444
    goto :goto_1
.end method

.method public static b(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/16 v1, 0x8

    .line 2327404
    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->x:Landroid/view/View;

    if-nez v2, :cond_0

    .line 2327405
    :goto_0
    return-void

    .line 2327406
    :cond_0
    if-eqz p1, :cond_2

    move v2, v1

    .line 2327407
    :goto_1
    if-eqz p1, :cond_3

    .line 2327408
    :goto_2
    iget-object v3, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->B:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2327409
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->A:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 2327410
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->z:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 2327411
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->E:Landroid/widget/Button;

    iget-boolean v3, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->U:Z

    if-eqz v3, :cond_1

    move v1, v2

    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v2, v0

    .line 2327412
    goto :goto_1

    :cond_3
    move v0, v1

    .line 2327413
    goto :goto_2
.end method

.method public static l(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x2710

    .line 2327399
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, LX/GaQ;->a(Ljava/lang/Long;Ljava/lang/Long;)LX/GaP;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->Q:LX/GaP;

    .line 2327400
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->Q:LX/GaP;

    new-instance v1, LX/GBb;

    invoke-direct {v1, p0}, LX/GBb;-><init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327401
    iput-object v1, v0, LX/GaP;->d:LX/GAx;

    .line 2327402
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->Q:LX/GaP;

    invoke-virtual {v0}, LX/GaP;->c()V

    .line 2327403
    return-void
.end method

.method public static m(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2327387
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->Q:LX/GaP;

    if-eqz v0, :cond_0

    .line 2327388
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->Q:LX/GaP;

    invoke-virtual {v0}, LX/GaP;->d()V

    .line 2327389
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->Q:LX/GaP;

    .line 2327390
    :cond_0
    const/4 v0, 0x1

    .line 2327391
    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->j:LX/2Dt;

    invoke-virtual {v2}, LX/2Dt;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->N:Z

    if-eqz v2, :cond_1

    .line 2327392
    sget-object v2, LX/GBh;->BACKGROUND_SEND:LX/GBh;

    invoke-static {p0, v2}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->b(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;LX/GBh;)V

    .line 2327393
    iget-boolean v2, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->M:Z

    if-eqz v2, :cond_1

    .line 2327394
    invoke-static {p0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->l(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    move v0, v1

    .line 2327395
    :cond_1
    if-eqz v0, :cond_2

    .line 2327396
    invoke-static {p0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->o$redex0(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327397
    :cond_2
    iput-boolean v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->N:Z

    .line 2327398
    return-void
.end method

.method public static o$redex0(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V
    .locals 2

    .prologue
    .line 2327383
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->v:Landroid/view/ViewStub;

    if-eqz v0, :cond_0

    .line 2327384
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->v:Landroid/view/ViewStub;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 2327385
    :cond_0
    invoke-static {p0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->q(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327386
    return-void
.end method

.method public static q(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V
    .locals 6

    .prologue
    const/16 v2, 0x8

    .line 2327314
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->n:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2327315
    const v0, 0x7f0d036b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->c(I)LX/0am;

    move-result-object v0

    .line 2327316
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2327317
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->x:Landroid/view/View;

    .line 2327318
    :goto_0
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->x:Landroid/view/View;

    const v1, 0x7f0d0363

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/search/SearchEditText;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->y:Lcom/facebook/ui/search/SearchEditText;

    .line 2327319
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->x:Landroid/view/View;

    const v1, 0x7f0d0364

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->z:Landroid/widget/Button;

    .line 2327320
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->x:Landroid/view/View;

    const v1, 0x7f0d0366

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->A:Landroid/widget/Button;

    .line 2327321
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->x:Landroid/view/View;

    const v1, 0x7f0d0365

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->B:Landroid/view/View;

    .line 2327322
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->x:Landroid/view/View;

    const v1, 0x7f0d0362

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->C:Landroid/widget/TextView;

    .line 2327323
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->x:Landroid/view/View;

    const v1, 0x7f0d0360

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->D:Landroid/widget/TextView;

    .line 2327324
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->x:Landroid/view/View;

    const v1, 0x7f0d0367

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->E:Landroid/widget/Button;

    .line 2327325
    invoke-static {p0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->r(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327326
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->y:Lcom/facebook/ui/search/SearchEditText;

    new-instance v1, LX/GBe;

    invoke-direct {v1, p0}, LX/GBe;-><init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327327
    iput-object v1, v0, Lcom/facebook/ui/search/SearchEditText;->f:LX/7HK;

    .line 2327328
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->y:Lcom/facebook/ui/search/SearchEditText;

    new-instance v1, LX/GBf;

    invoke-direct {v1, p0}, LX/GBf;-><init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2327329
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->z:Landroid/widget/Button;

    new-instance v1, LX/GBP;

    invoke-direct {v1, p0}, LX/GBP;-><init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2327330
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->A:Landroid/widget/Button;

    new-instance v1, LX/GBQ;

    invoke-direct {v1, p0}, LX/GBQ;-><init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2327331
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->d:LX/2Ne;

    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->H:Ljava/lang/String;

    .line 2327332
    iget-object v3, v0, LX/2Ne;->a:LX/0Zb;

    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v5, LX/GB4;->CODE_ENTRY_VIEWED:LX/GB4;

    invoke-virtual {v5}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "account_recovery"

    .line 2327333
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2327334
    move-object v4, v4

    .line 2327335
    const-string v5, "crypted_id"

    invoke-virtual {v4, v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2327336
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2327337
    :cond_0
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->E:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 2327338
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->U:Z

    .line 2327339
    :goto_1
    return-void

    .line 2327340
    :cond_1
    invoke-static {p0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->s$redex0(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327341
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->E:Landroid/widget/Button;

    new-instance v1, LX/GBd;

    invoke-direct {v1, p0}, LX/GBd;-><init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 2327342
    :cond_2
    const v0, 0x7f0d036c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->x:Landroid/view/View;

    goto/16 :goto_0
.end method

.method public static r(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2327170
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    .line 2327171
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->G:LX/GBg;

    sget-object v1, LX/GBg;->SMS:LX/GBg;

    if-ne v0, v1, :cond_1

    .line 2327172
    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->L:Ljava/util/List;

    .line 2327173
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->C:Landroid/widget/TextView;

    const v1, 0x7f083527

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2327174
    const v1, 0x7f083527

    .line 2327175
    const v0, 0x7f083528

    move-object v3, v2

    move v2, v0

    .line 2327176
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2327177
    iget-object v4, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->D:Landroid/widget/TextView;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2327178
    const v0, 0x7f0d0361

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2327179
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v6, :cond_2

    .line 2327180
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2327181
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2327182
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->C:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 2327183
    :cond_0
    :goto_1
    return-void

    .line 2327184
    :cond_1
    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->J:Ljava/util/List;

    .line 2327185
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->C:Landroid/widget/TextView;

    const v1, 0x7f08352f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2327186
    const v1, 0x7f08352f

    .line 2327187
    const v0, 0x7f083530

    move-object v3, v2

    move v2, v0

    goto :goto_0

    .line 2327188
    :cond_2
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2327189
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->C:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method

.method public static s$redex0(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V
    .locals 2

    .prologue
    .line 2327190
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->G:LX/GBg;

    sget-object v1, LX/GBg;->SMS:LX/GBg;

    if-ne v0, v1, :cond_0

    .line 2327191
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->E:Landroid/widget/Button;

    const v1, 0x7f083532

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 2327192
    :goto_0
    return-void

    .line 2327193
    :cond_0
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->E:Landroid/widget/Button;

    const v1, 0x7f083531

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0
.end method

.method public static w$redex0(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V
    .locals 3

    .prologue
    .line 2327308
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->G:LX/GBg;

    sget-object v1, LX/GBg;->SMS:LX/GBg;

    if-ne v0, v1, :cond_0

    .line 2327309
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->i:LX/0i4;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    .line 2327310
    sget-object v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->l:[Ljava/lang/String;

    new-instance v2, LX/GBW;

    invoke-direct {v2, p0}, LX/GBW;-><init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 2327311
    :cond_0
    return-void
.end method

.method public static z(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V
    .locals 2

    .prologue
    .line 2327194
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->e:LX/GAz;

    .line 2327195
    iget-object v1, v0, LX/GAz;->d:LX/GaP;

    invoke-virtual {v1}, LX/GaP;->d()V

    .line 2327196
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->M:Z

    .line 2327197
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 2327198
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2327199
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v3, p0

    check-cast v3, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v6

    check-cast v6, LX/0kL;

    invoke-static {v0}, LX/2Ne;->a(LX/0QB;)LX/2Ne;

    move-result-object v7

    check-cast v7, LX/2Ne;

    new-instance v12, LX/GAz;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {v0}, LX/2UC;->b(LX/0QB;)LX/2UC;

    move-result-object v9

    check-cast v9, LX/2UC;

    const-class v10, LX/GaQ;

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/GaQ;

    invoke-static {v0}, LX/2Dt;->b(LX/0QB;)LX/2Dt;

    move-result-object v11

    check-cast v11, LX/2Dt;

    invoke-direct {v12, v8, v9, v10, v11}, LX/GAz;-><init>(LX/0SG;LX/2UC;LX/GaQ;LX/2Dt;)V

    move-object v8, v12

    check-cast v8, LX/GAz;

    const/16 v9, 0x2f3

    invoke-static {v0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const-class v10, LX/GaQ;

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/GaQ;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v11

    check-cast v11, LX/0SG;

    const-class v12, LX/0i4;

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/0i4;

    invoke-static {v0}, LX/2Dt;->b(LX/0QB;)LX/2Dt;

    move-result-object p1

    check-cast p1, LX/2Dt;

    invoke-static {v0}, LX/GB2;->b(LX/0QB;)LX/GB2;

    move-result-object v0

    check-cast v0, LX/GB2;

    iput-object v4, v3, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->a:LX/0aG;

    iput-object v5, v3, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->b:LX/1Ck;

    iput-object v6, v3, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->c:LX/0kL;

    iput-object v7, v3, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->d:LX/2Ne;

    iput-object v8, v3, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->e:LX/GAz;

    iput-object v9, v3, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->f:LX/0Or;

    iput-object v10, v3, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->g:LX/GaQ;

    iput-object v11, v3, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->h:LX/0SG;

    iput-object v12, v3, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->i:LX/0i4;

    iput-object p1, v3, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->j:LX/2Dt;

    iput-object v0, v3, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->k:LX/GB2;

    .line 2327200
    iput-boolean v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->M:Z

    .line 2327201
    iput-boolean v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->N:Z

    .line 2327202
    iput-boolean v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->O:Z

    .line 2327203
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->U:Z

    .line 2327204
    iput-boolean v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->V:Z

    .line 2327205
    invoke-static {p0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->B(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327206
    return-void
.end method

.method public final a(Lcom/facebook/account/recovery/common/model/AccountCandidateModel;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2327207
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->p:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {p1}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2327208
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->p:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {p1}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2327209
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->p:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {p1}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2327210
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->p:Lcom/facebook/fbui/widget/contentview/ContentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2327211
    if-eqz p2, :cond_1

    .line 2327212
    iput-boolean v3, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->V:Z

    .line 2327213
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->t:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2327214
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->t:Landroid/widget/TextView;

    new-instance v1, LX/GBY;

    invoke-direct {v1, p0}, LX/GBY;-><init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2327215
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->H:Ljava/lang/String;

    .line 2327216
    invoke-virtual {p1}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->k()V

    .line 2327217
    invoke-virtual {p1}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->g()LX/0Px;

    move-result-object v0

    .line 2327218
    invoke-virtual {p1}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->h()LX/0Px;

    move-result-object v1

    .line 2327219
    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->q:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-static {v0, v2}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->a(Ljava/util/List;Lcom/facebook/fbui/widget/contentview/CheckedContentView;)V

    .line 2327220
    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->r:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-static {v1, v2}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->a(Ljava/util/List;Lcom/facebook/fbui/widget/contentview/CheckedContentView;)V

    .line 2327221
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2327222
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->a(Lcom/facebook/account/recovery/common/model/AccountCandidateModel;)V

    .line 2327223
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2327224
    sget-object v0, LX/GBg;->EMAIL:LX/GBg;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->G:LX/GBg;

    .line 2327225
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->r:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 2327226
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->r:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const v1, 0x7f0207d5

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setCheckMarkDrawable(I)V

    .line 2327227
    :goto_1
    return-void

    .line 2327228
    :cond_1
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->t:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 2327229
    :cond_2
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->q:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const v1, 0x7f0207d5

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setCheckMarkDrawable(I)V

    goto :goto_1

    .line 2327230
    :cond_3
    invoke-direct {p0, p1}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->a(Lcom/facebook/account/recovery/common/model/AccountCandidateModel;)V

    .line 2327231
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->q:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    new-instance v1, LX/GBZ;

    invoke-direct {v1, p0}, LX/GBZ;-><init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2327232
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->r:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    new-instance v1, LX/GBa;

    invoke-direct {v1, p0}, LX/GBa;-><init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2327233
    goto :goto_1
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 2327234
    iget-boolean v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->V:Z

    if-eqz v0, :cond_0

    .line 2327235
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->d:LX/2Ne;

    .line 2327236
    iget-object v1, v0, LX/2Ne;->a:LX/0Zb;

    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p0, LX/GB4;->AUTO_IDENTIFY_REJECTED:LX/GB4;

    invoke-virtual {p0}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v2, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "account_recovery"

    .line 2327237
    iput-object p0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2327238
    move-object v2, v2

    .line 2327239
    const/4 p0, 0x1

    invoke-interface {v1, v2, p0}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2327240
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/16 v2, 0x2a

    const v3, -0x53b430c7

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2327241
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/app/Activity;)V

    .line 2327242
    :try_start_0
    move-object v0, p1

    check-cast v0, LX/2Ng;

    move-object v1, v0

    iput-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->m:LX/2Ng;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2327243
    const v1, -0x613dc424

    invoke-static {v1, v2}, LX/02F;->f(II)V

    return-void

    .line 2327244
    :catch_0
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " must implement OnConfirmationCodeValidatedListener"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    const v3, -0x1fc2ce1a

    invoke-static {v3, v2}, LX/02F;->f(II)V

    throw v1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x5873385a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327245
    const v1, 0x7f03001b

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x58f0e06c

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3493c9b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327167
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->b:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2327168
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2327169
    const/16 v1, 0x2b

    const v2, -0x16e445a6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v0, 0x2a

    const v1, 0x13c707b0

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327246
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->y:Lcom/facebook/ui/search/SearchEditText;

    if-eqz v1, :cond_0

    .line 2327247
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->y:Lcom/facebook/ui/search/SearchEditText;

    .line 2327248
    invoke-static {v1}, Lcom/facebook/ui/search/SearchEditText;->i(Lcom/facebook/ui/search/SearchEditText;)V

    .line 2327249
    :cond_0
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->Q:LX/GaP;

    if-eqz v1, :cond_1

    .line 2327250
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->Q:LX/GaP;

    invoke-virtual {v1}, LX/GaP;->d()V

    .line 2327251
    :cond_1
    iget-boolean v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->M:Z

    if-eqz v1, :cond_2

    .line 2327252
    invoke-static {p0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->z(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327253
    iput-boolean v6, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->O:Z

    .line 2327254
    :cond_2
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->P:LX/GaP;

    if-eqz v1, :cond_3

    .line 2327255
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->P:LX/GaP;

    invoke-virtual {v1}, LX/GaP;->d()V

    .line 2327256
    :cond_3
    iget-wide v2, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->R:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    .line 2327257
    iput-boolean v6, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->T:Z

    .line 2327258
    :cond_4
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2327259
    const/16 v1, 0x2b

    const v2, 0x79c9b342

    invoke-static {v7, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x626c9a1a

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327260
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2327261
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->Q:LX/GaP;

    if-eqz v1, :cond_3

    .line 2327262
    iput-object v3, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->Q:LX/GaP;

    .line 2327263
    invoke-static {p0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->m(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327264
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->O:Z

    if-eqz v1, :cond_2

    .line 2327265
    iget-object v4, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->G:LX/GBg;

    sget-object v5, LX/GBg;->SMS:LX/GBg;

    if-ne v4, v5, :cond_1

    .line 2327266
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->M:Z

    .line 2327267
    iget-object v4, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->e:LX/GAz;

    .line 2327268
    iget-object v6, v4, LX/GAz;->d:LX/GaP;

    .line 2327269
    iget-object v7, v6, LX/GaP;->a:Landroid/os/CountDownTimer;

    if-eqz v7, :cond_5

    const/4 v7, 0x1

    :goto_1
    move v6, v7

    .line 2327270
    if-nez v6, :cond_1

    iget-object v6, v4, LX/GAz;->e:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    if-eqz v6, :cond_1

    iget-wide v6, v4, LX/GAz;->f:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-gtz v6, :cond_4

    .line 2327271
    :cond_1
    :goto_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->O:Z

    .line 2327272
    :cond_2
    const v1, 0x7575ae90

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 2327273
    :cond_3
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->P:LX/GaP;

    if-eqz v1, :cond_0

    .line 2327274
    iput-object v3, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->P:LX/GaP;

    .line 2327275
    invoke-static {p0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->o$redex0(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    goto :goto_0

    .line 2327276
    :cond_4
    iget-wide v6, v4, LX/GAz;->f:J

    invoke-static {v4, v6, v7}, LX/GAz;->a(LX/GAz;J)V

    goto :goto_2

    :cond_5
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2de1695e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327277
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2327278
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2327279
    if-nez v1, :cond_0

    .line 2327280
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x3239657c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2327281
    :cond_0
    const v2, 0x7f083521

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2327282
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2327283
    const v0, 0x7f0d036d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->n:Landroid/view/View;

    .line 2327284
    const v0, 0x7f0d036e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->o:Landroid/widget/TextView;

    .line 2327285
    const v0, 0x7f0d036f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->p:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2327286
    invoke-direct {p0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->b()V

    .line 2327287
    const v0, 0x7f0d0372

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->s:Landroid/widget/TextView;

    .line 2327288
    const v0, 0x7f0d0374

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->t:Landroid/widget/TextView;

    .line 2327289
    const v0, 0x7f0d0375

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->u:Landroid/view/View;

    .line 2327290
    iput-boolean v2, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->F:Z

    .line 2327291
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->o:Landroid/widget/TextView;

    const v1, 0x7f083522

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2327292
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->p:Lcom/facebook/fbui/widget/contentview/ContentView;

    sget-object v1, LX/6VF;->MEDIUM:LX/6VF;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2327293
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->q:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2327294
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->r:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2327295
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2327296
    if-eqz v0, :cond_0

    .line 2327297
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2327298
    const-string v1, "account_profile"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

    .line 2327299
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2327300
    const-string v2, "auto_identify"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 2327301
    invoke-virtual {p0, v0, v1}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->a(Lcom/facebook/account/recovery/common/model/AccountCandidateModel;Z)V

    .line 2327302
    :cond_0
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->d:LX/2Ne;

    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->H:Ljava/lang/String;

    .line 2327303
    iget-object v2, v0, LX/2Ne;->a:LX/0Zb;

    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p1, LX/GB4;->INITIATE_VIEWED:LX/GB4;

    invoke-virtual {p1}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "account_recovery"

    .line 2327304
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2327305
    move-object p0, p0

    .line 2327306
    const-string p1, "crypted_id"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const/4 p1, 0x1

    invoke-interface {v2, p0, p1}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2327307
    return-void
.end method
