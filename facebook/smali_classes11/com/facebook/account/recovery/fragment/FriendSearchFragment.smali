.class public Lcom/facebook/account/recovery/fragment/FriendSearchFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/2Ne;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/ui/search/SearchEditText;

.field public c:Landroid/widget/Button;

.field public d:Landroid/view/View;

.field public e:LX/2Ni;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2327915
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2327916
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;

    invoke-static {p0}, LX/2Ne;->a(LX/0QB;)LX/2Ne;

    move-result-object p0

    check-cast p0, LX/2Ne;

    iput-object p0, p1, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->a:LX/2Ne;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2327954
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2327955
    const-class v0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;

    invoke-static {v0, p0}, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2327956
    new-instance v0, LX/4nE;

    invoke-direct {v0}, LX/4nE;-><init>()V

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 2327957
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/16 v2, 0x2a

    const v3, 0x3347978d

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2327949
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/app/Activity;)V

    .line 2327950
    :try_start_0
    move-object v0, p1

    check-cast v0, LX/2Ni;

    move-object v1, v0

    iput-object v1, p0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->e:LX/2Ni;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2327951
    const v1, 0x277b7769

    invoke-static {v1, v2}, LX/02F;->f(II)V

    return-void

    .line 2327952
    :catch_0
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "must implement FriendSearchListener"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    const v3, 0x589919a9

    invoke-static {v3, v2}, LX/02F;->f(II)V

    throw v1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6ec124fd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327953
    const v1, 0x7f0306ef

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x5b2e2eed

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x629f0367

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327945
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->b:Lcom/facebook/ui/search/SearchEditText;

    .line 2327946
    invoke-static {v1}, Lcom/facebook/ui/search/SearchEditText;->i(Lcom/facebook/ui/search/SearchEditText;)V

    .line 2327947
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2327948
    const/16 v1, 0x2b

    const v2, 0x159e23bf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xc996553

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327942
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2327943
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->b:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->b()Z

    .line 2327944
    const/16 v1, 0x2b

    const v2, 0x699d4c04

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4da5a711    # 3.47398688E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327937
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2327938
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2327939
    if-nez v1, :cond_0

    .line 2327940
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x5a528d92

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2327941
    :cond_0
    const v2, 0x7f08350b

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2327917
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2327918
    const v0, 0x7f0d0380

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/search/SearchEditText;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->b:Lcom/facebook/ui/search/SearchEditText;

    .line 2327919
    const v0, 0x7f0d0385

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->c:Landroid/widget/Button;

    .line 2327920
    const v0, 0x7f0d0387

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->d:Landroid/view/View;

    .line 2327921
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2327922
    if-eqz v0, :cond_0

    .line 2327923
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2327924
    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->f:Ljava/lang/String;

    .line 2327925
    :cond_0
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->a:LX/2Ne;

    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->f:Ljava/lang/String;

    .line 2327926
    iget-object v2, v0, LX/2Ne;->a:LX/0Zb;

    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p2, LX/GB4;->FRIEND_SEARCH_VIEWED:LX/GB4;

    invoke-virtual {p2}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p2, "account_recovery"

    .line 2327927
    iput-object p2, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2327928
    move-object p1, p1

    .line 2327929
    const-string p2, "account_name"

    invoke-virtual {p1, p2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const/4 p2, 0x1

    invoke-interface {v2, p1, p2}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2327930
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->b:Lcom/facebook/ui/search/SearchEditText;

    .line 2327931
    new-instance v1, LX/GC1;

    invoke-direct {v1, p0}, LX/GC1;-><init>(Lcom/facebook/account/recovery/fragment/FriendSearchFragment;)V

    move-object v1, v1

    .line 2327932
    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2327933
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->b:Lcom/facebook/ui/search/SearchEditText;

    new-instance v1, LX/GBz;

    invoke-direct {v1, p0}, LX/GBz;-><init>(Lcom/facebook/account/recovery/fragment/FriendSearchFragment;)V

    .line 2327934
    iput-object v1, v0, Lcom/facebook/ui/search/SearchEditText;->f:LX/7HK;

    .line 2327935
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->c:Landroid/widget/Button;

    new-instance v1, LX/GC0;

    invoke-direct {v1, p0}, LX/GC0;-><init>(Lcom/facebook/account/recovery/fragment/FriendSearchFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2327936
    return-void
.end method
