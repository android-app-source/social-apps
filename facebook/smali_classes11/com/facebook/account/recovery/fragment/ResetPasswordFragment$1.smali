.class public final Lcom/facebook/account/recovery/fragment/ResetPasswordFragment$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7HK;


# instance fields
.field public final synthetic a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;)V
    .locals 0

    .prologue
    .line 2328008
    iput-object p1, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment$1;->a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    const/16 v3, 0x8

    .line 2328009
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment$1;->a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->e:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2328010
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2328011
    :goto_0
    return-void

    .line 2328012
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x6

    if-ge v1, v2, :cond_1

    .line 2328013
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment$1;->a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment$1;->a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    const v2, 0x7f083544

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2328014
    invoke-static {v0, v1}, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->b$redex0(Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;Ljava/lang/String;)V

    .line 2328015
    goto :goto_0

    .line 2328016
    :cond_1
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment$1;->a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    .line 2328017
    iget-object v4, v1, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->c:LX/2Ne;

    iget-object v5, v1, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->i:Ljava/lang/String;

    .line 2328018
    iget-object v6, v4, LX/2Ne;->a:LX/0Zb;

    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v8, LX/GB4;->CHANGE_PASSWORD_SUBMITTED:LX/GB4;

    invoke-virtual {v8}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v8, "account_recovery"

    .line 2328019
    iput-object v8, v7, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2328020
    move-object v7, v7

    .line 2328021
    const-string v8, "crypted_id"

    invoke-virtual {v7, v8, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v6, v7, v8}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2328022
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2328023
    new-instance v4, Lcom/facebook/account/recovery/common/protocol/AccountRecoveryValidateCodeMethod$Params;

    iget-object v5, v1, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->i:Ljava/lang/String;

    iget-object v7, v1, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->j:Ljava/lang/String;

    iget-boolean v8, v1, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->k:Z

    invoke-direct {v4, v5, v7, v0, v8}, Lcom/facebook/account/recovery/common/protocol/AccountRecoveryValidateCodeMethod$Params;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2328024
    const-string v5, "accountRecoveryValidateCodeParamsKey"

    invoke-virtual {v6, v5, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2328025
    iget-object v10, v1, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->b:LX/1Ck;

    const/4 v11, 0x0

    iget-object v4, v1, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->a:LX/0aG;

    const-string v5, "account_recovery_validate_code"

    sget-object v7, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v8, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    invoke-static {v8}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v8

    const v9, -0x482034be

    invoke-static/range {v4 .. v9}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    move-result-object v4

    new-instance v5, LX/GC7;

    invoke-direct {v5, v1, v0}, LX/GC7;-><init>(Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;Ljava/lang/String;)V

    invoke-virtual {v10, v11, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2328026
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment$1;->a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2328027
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment$1;->a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->g:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 2328028
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment$1;->a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->h:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method
