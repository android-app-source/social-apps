.class public Lcom/facebook/account/recovery/fragment/AccountSearchFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/GAv;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final o:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:Ljava/lang/String;

.field public B:Ljava/lang/String;

.field public C:LX/03R;

.field public D:LX/GBv;

.field public E:Z

.field public F:Ljava/lang/String;

.field public G:Ljava/lang/String;

.field public H:Z

.field public b:LX/GCA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2Ne;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/F9o;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation runtime Lcom/facebook/account/recovery/annotations/IsCaptchaEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/2Dt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Uh;
    .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/2Dr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0dC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Landroid/widget/FrameLayout;

.field public q:LX/2Nh;

.field public r:Lcom/facebook/ui/search/SearchEditText;

.field public s:Landroid/widget/TextView;

.field public t:Landroid/widget/TextView;

.field public u:Landroid/widget/Button;

.field public v:Landroid/view/View;

.field public w:Landroid/view/View;

.field public x:Lcom/facebook/widget/listview/BetterListView;

.field public y:Landroid/widget/TextView;

.field public z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2327797
    const-class v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    sput-object v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->o:Ljava/lang/Class;

    .line 2327798
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.GET_ACCOUNTS"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.READ_CONTACTS"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "android.permission.READ_PHONE_STATE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2327829
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2327830
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->z:Ljava/lang/String;

    .line 2327831
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->A:Ljava/lang/String;

    .line 2327832
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->C:LX/03R;

    .line 2327833
    sget-object v0, LX/GBv;->CONTACT_POINT:LX/GBv;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->D:LX/GBv;

    .line 2327834
    iput-boolean v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->E:Z

    .line 2327835
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->F:Ljava/lang/String;

    .line 2327836
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->G:Ljava/lang/String;

    .line 2327837
    iput-boolean v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->H:Z

    return-void
.end method

.method public static a$redex0(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;LX/GBv;)V
    .locals 2

    .prologue
    .line 2327817
    iput-object p1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->D:LX/GBv;

    .line 2327818
    sget-object v0, LX/GBl;->a:[I

    invoke-virtual {p1}, LX/GBv;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2327819
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2327820
    :pswitch_0
    invoke-static {p0}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->d(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    .line 2327821
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->r:Lcom/facebook/ui/search/SearchEditText;

    const v1, 0x7f08350f

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setHint(I)V

    .line 2327822
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->s:Landroid/widget/TextView;

    const v1, 0x7f08350c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2327823
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->y:Landroid/widget/TextView;

    const v1, 0x7f083510

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2327824
    :goto_0
    return-void

    .line 2327825
    :pswitch_1
    invoke-static {p0}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->d(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    .line 2327826
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->r:Lcom/facebook/ui/search/SearchEditText;

    const v1, 0x7f08350e

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setHint(I)V

    .line 2327827
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->s:Landroid/widget/TextView;

    const v1, 0x7f08350d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2327828
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->y:Landroid/widget/TextView;

    const v1, 0x7f083511

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static c(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 2327799
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->b:LX/GCA;

    invoke-virtual {v0}, LX/GCA;->a()V

    .line 2327800
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2327801
    new-instance v0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;

    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->z:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->A:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->B:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->n:LX/0dC;

    invoke-virtual {v4}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v5

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2327802
    const-string v1, "accountRecoverySearchAccountParamsKey"

    invoke-virtual {v6, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2327803
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->A:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2327804
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->c:LX/2Ne;

    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->D:LX/GBv;

    invoke-virtual {v1}, LX/GBv;->name()Ljava/lang/String;

    move-result-object v1

    .line 2327805
    iget-object v2, v0, LX/2Ne;->a:LX/0Zb;

    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/GB4;->SEARCH_SENT:LX/GB4;

    invoke-virtual {v4}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "account_recovery"

    .line 2327806
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2327807
    move-object v3, v3

    .line 2327808
    const-string v4, "search_type"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2327809
    :goto_0
    iget-object v7, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->e:LX/1Ck;

    sget-object v8, LX/GBw;->ACCOUNT_SEARCH:LX/GBw;

    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->d:LX/0aG;

    const-string v1, "account_recovery_search_account"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v2, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x42110960

    move-object v2, v6

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    new-instance v1, LX/GBu;

    invoke-direct {v1, p0}, LX/GBu;-><init>(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    invoke-virtual {v7, v8, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2327810
    return-void

    .line 2327811
    :cond_0
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->c:LX/2Ne;

    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->z:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->D:LX/GBv;

    invoke-virtual {v2}, LX/GBv;->name()Ljava/lang/String;

    move-result-object v2

    .line 2327812
    iget-object v3, v0, LX/2Ne;->a:LX/0Zb;

    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v5, LX/GB4;->FRIEND_SEARCH_SENT:LX/GB4;

    invoke-virtual {v5}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "account_recovery"

    .line 2327813
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2327814
    move-object v4, v4

    .line 2327815
    const-string v5, "account_name"

    invoke-virtual {v4, v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "search_type"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2327816
    goto :goto_0
.end method

.method public static d(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2327789
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->r:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->a()V

    .line 2327790
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->r:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->b()Z

    .line 2327791
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2327792
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2327793
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->u:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 2327794
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->w:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2327795
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->b:LX/GCA;

    invoke-virtual {v0}, LX/GCA;->a()V

    .line 2327796
    return-void
.end method

.method public static m(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V
    .locals 3

    .prologue
    .line 2327787
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->l:LX/2Dr;

    const-string v1, "account_search"

    invoke-virtual {v0, v1}, LX/2Dr;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/GBt;

    invoke-direct {v1, p0}, LX/GBt;-><init>(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->m:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2327788
    return-void
.end method

.method public static n(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V
    .locals 4

    .prologue
    .line 2327760
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->C:LX/03R;

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 2327761
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->c:LX/2Ne;

    .line 2327762
    iget-object v1, v0, LX/2Ne;->a:LX/0Zb;

    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/GB4;->AUTO_IDENTIFY_FAILED:LX/GB4;

    invoke-virtual {v3}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "account_recovery"

    .line 2327763
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2327764
    move-object v2, v2

    .line 2327765
    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2327766
    invoke-static {p0}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->s(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    .line 2327767
    :goto_0
    return-void

    .line 2327768
    :cond_0
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2327769
    iget-boolean v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->H:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->j:LX/2Dt;

    invoke-virtual {v1}, LX/2Dt;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2327770
    const v1, 0x7f08003f

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2327771
    :goto_1
    move-object v1, v1

    .line 2327772
    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    .line 2327773
    iget-boolean v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->H:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->j:LX/2Dt;

    invoke-virtual {v1}, LX/2Dt;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2327774
    const v1, 0x7f083514

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2327775
    :goto_2
    move-object v1, v1

    .line 2327776
    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    .line 2327777
    iget-boolean v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->H:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->j:LX/2Dt;

    invoke-virtual {v1}, LX/2Dt;->e()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2327778
    const v1, 0x7f080016

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2327779
    :goto_3
    move-object v1, v1

    .line 2327780
    new-instance v2, LX/GBi;

    invoke-direct {v2, p0}, LX/GBi;-><init>(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2327781
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->w:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const v1, 0x7f08351c

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 2327782
    :cond_2
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->D:LX/GBv;

    sget-object v2, LX/GBv;->CONTACT_POINT:LX/GBv;

    if-ne v1, v2, :cond_3

    .line 2327783
    const v1, 0x7f08351e

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 2327784
    :cond_3
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->D:LX/GBv;

    sget-object v2, LX/GBv;->NAME:LX/GBv;

    if-ne v1, v2, :cond_4

    .line 2327785
    const v1, 0x7f08351f

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 2327786
    :cond_4
    const v1, 0x7f08351d

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_5
    const v1, 0x7f083520

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3
.end method

.method public static s(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2327752
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->p:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2327753
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2327754
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2327755
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->w:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2327756
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2327757
    sget-object v0, LX/03R;->NO:LX/03R;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->C:LX/03R;

    .line 2327758
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->B:Ljava/lang/String;

    .line 2327759
    return-void
.end method

.method public static v(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V
    .locals 4

    .prologue
    .line 2327838
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->c:LX/2Ne;

    .line 2327839
    iget-object v1, v0, LX/2Ne;->a:LX/0Zb;

    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/GB4;->AUTO_IDENTIFY_STARTED:LX/GB4;

    invoke-virtual {v3}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "account_recovery"

    .line 2327840
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2327841
    move-object v2, v2

    .line 2327842
    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2327843
    invoke-static {p0}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->m(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    .line 2327844
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    .line 2327749
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2327750
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    new-instance v4, LX/GCA;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {v4, v3}, LX/GCA;-><init>(Landroid/content/res/Resources;)V

    move-object v3, v4

    check-cast v3, LX/GCA;

    invoke-static {v0}, LX/2Ne;->a(LX/0QB;)LX/2Ne;

    move-result-object v4

    check-cast v4, LX/2Ne;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v5

    check-cast v5, LX/0aG;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {v0}, LX/F9o;->a(LX/0QB;)LX/F9o;

    move-result-object v7

    check-cast v7, LX/F9o;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/Executor;

    const/16 v9, 0x2f2

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const-class v10, LX/0i4;

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/0i4;

    invoke-static {v0}, LX/2Dt;->b(LX/0QB;)LX/2Dt;

    move-result-object v11

    check-cast v11, LX/2Dt;

    invoke-static {v0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-static {v0}, LX/2Dr;->b(LX/0QB;)LX/2Dr;

    move-result-object v13

    check-cast v13, LX/2Dr;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v0

    check-cast v0, LX/0dC;

    iput-object v3, v2, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->b:LX/GCA;

    iput-object v4, v2, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->c:LX/2Ne;

    iput-object v5, v2, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->d:LX/0aG;

    iput-object v6, v2, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->e:LX/1Ck;

    iput-object v7, v2, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->f:LX/F9o;

    iput-object v8, v2, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->g:Ljava/util/concurrent/Executor;

    iput-object v9, v2, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->h:LX/0Ot;

    iput-object v10, v2, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->i:LX/0i4;

    iput-object v11, v2, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->j:LX/2Dt;

    iput-object v12, v2, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->k:LX/0Uh;

    iput-object v13, v2, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->l:LX/2Dr;

    iput-object p1, v2, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->m:Ljava/util/concurrent/ExecutorService;

    iput-object v0, v2, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->n:LX/0dC;

    .line 2327751
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 2327744
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->b:LX/GCA;

    invoke-virtual {v0}, LX/GCA;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2327745
    invoke-static {p0}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->d(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    .line 2327746
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->c:LX/2Ne;

    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->D:LX/GBv;

    invoke-virtual {v1}, LX/GBv;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Ne;->b(Ljava/lang/String;)V

    .line 2327747
    const/4 v0, 0x1

    .line 2327748
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/16 v2, 0x2a

    const v3, -0x4ad6350d

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2327740
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/app/Activity;)V

    .line 2327741
    :try_start_0
    move-object v0, p1

    check-cast v0, LX/2Nh;

    move-object v1, v0

    iput-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->q:LX/2Nh;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2327742
    const v1, 0x6bef16fe

    invoke-static {v1, v2}, LX/02F;->f(II)V

    return-void

    .line 2327743
    :catch_0
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " must implement AccountSearchListener"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    const v3, 0x547f049e

    invoke-static {v3, v2}, LX/02F;->f(II)V

    throw v1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x7cea602a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327739
    const v1, 0x7f03001f

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x7c255b1d

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7764d631

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327736
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->e:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2327737
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2327738
    const/16 v1, 0x2b

    const v2, -0x19c79da3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3dd4c224

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327651
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->r:Lcom/facebook/ui/search/SearchEditText;

    .line 2327652
    invoke-static {v1}, Lcom/facebook/ui/search/SearchEditText;->i(Lcom/facebook/ui/search/SearchEditText;)V

    .line 2327653
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2327654
    const/16 v1, 0x2b

    const v2, -0x6061a0a8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6556efef

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327733
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2327734
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->r:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->b()Z

    .line 2327735
    const/16 v1, 0x2b

    const v2, 0x10b39105

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6be974f9    # -7.600483E-27f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327728
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2327729
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2327730
    if-nez v1, :cond_0

    .line 2327731
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x14005399

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2327732
    :cond_0
    const v2, 0x7f08350b

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2327655
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2327656
    const v0, 0x7f0d037f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->p:Landroid/widget/FrameLayout;

    .line 2327657
    const v0, 0x7f0d0380

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/search/SearchEditText;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->r:Lcom/facebook/ui/search/SearchEditText;

    .line 2327658
    const v0, 0x7f0d0381

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->v:Landroid/view/View;

    .line 2327659
    const v0, 0x7f0d0382

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->x:Lcom/facebook/widget/listview/BetterListView;

    .line 2327660
    const v0, 0x7f0d0384

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->s:Landroid/widget/TextView;

    .line 2327661
    const v0, 0x7f0d0388

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->t:Landroid/widget/TextView;

    .line 2327662
    const v0, 0x7f0d0385

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->u:Landroid/widget/Button;

    .line 2327663
    const v0, 0x7f0d0387

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->w:Landroid/view/View;

    .line 2327664
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->x:Lcom/facebook/widget/listview/BetterListView;

    const v1, 0x7f0d0383

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 2327665
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->x:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->b:LX/GCA;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2327666
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->x:Lcom/facebook/widget/listview/BetterListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setStickyHeaderEnabled(Z)V

    .line 2327667
    const v0, 0x7f0d0386

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->y:Landroid/widget/TextView;

    .line 2327668
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->D:LX/GBv;

    invoke-static {p0, v0}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->a$redex0(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;LX/GBv;)V

    .line 2327669
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2327670
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2327671
    if-eqz v1, :cond_0

    .line 2327672
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2327673
    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->z:Ljava/lang/String;

    .line 2327674
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2327675
    const-string v1, "friend_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->A:Ljava/lang/String;

    .line 2327676
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2327677
    const-string v1, "account_search_use_query_now"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2327678
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2327679
    const-string v2, "from_login_pw_error"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->E:Z

    .line 2327680
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2327681
    const-string v2, "login_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->F:Ljava/lang/String;

    .line 2327682
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2327683
    const-string v2, "cuid"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->G:Ljava/lang/String;

    .line 2327684
    :cond_0
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->r:Lcom/facebook/ui/search/SearchEditText;

    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->z:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/ui/search/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2327685
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->y:Landroid/widget/TextView;

    new-instance v2, LX/GBm;

    invoke-direct {v2, p0}, LX/GBm;-><init>(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2327686
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->r:Lcom/facebook/ui/search/SearchEditText;

    .line 2327687
    new-instance v2, LX/GBs;

    invoke-direct {v2, p0}, LX/GBs;-><init>(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    move-object v2, v2

    .line 2327688
    invoke-virtual {v1, v2}, Lcom/facebook/ui/search/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2327689
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->r:Lcom/facebook/ui/search/SearchEditText;

    new-instance v2, LX/GBn;

    invoke-direct {v2, p0}, LX/GBn;-><init>(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    .line 2327690
    iput-object v2, v1, Lcom/facebook/ui/search/SearchEditText;->f:LX/7HK;

    .line 2327691
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->u:Landroid/widget/Button;

    new-instance v2, LX/GBo;

    invoke-direct {v2, p0}, LX/GBo;-><init>(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2327692
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->v:Landroid/view/View;

    new-instance v2, LX/GBp;

    invoke-direct {v2, p0}, LX/GBp;-><init>(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2327693
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->x:Lcom/facebook/widget/listview/BetterListView;

    new-instance v2, LX/GBq;

    invoke-direct {v2, p0}, LX/GBq;-><init>(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2327694
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->x:Lcom/facebook/widget/listview/BetterListView;

    new-instance v2, LX/GBr;

    invoke-direct {v2, p0}, LX/GBr;-><init>(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2327695
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2327696
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->r:Lcom/facebook/ui/search/SearchEditText;

    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->r:Lcom/facebook/ui/search/SearchEditText;

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/ui/search/SearchEditText;->onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z

    .line 2327697
    :goto_0
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->c:LX/2Ne;

    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->D:LX/GBv;

    invoke-virtual {v1}, LX/GBv;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Ne;->b(Ljava/lang/String;)V

    .line 2327698
    return-void

    .line 2327699
    :cond_1
    const/4 v1, 0x0

    .line 2327700
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->C:LX/03R;

    sget-object v2, LX/03R;->NO:LX/03R;

    if-ne v0, v2, :cond_2

    .line 2327701
    :goto_1
    goto :goto_0

    .line 2327702
    :cond_2
    sget-object v0, LX/03R;->YES:LX/03R;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->C:LX/03R;

    .line 2327703
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 2327704
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->w:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2327705
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2327706
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->p:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2327707
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2327708
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2327709
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->B:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2327710
    invoke-static {p0}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->v(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    goto :goto_1

    .line 2327711
    :cond_3
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->k:LX/0Uh;

    const/16 v2, 0x1f

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->k:LX/0Uh;

    const/16 v2, 0x1a

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->E:Z

    if-eqz v0, :cond_5

    :cond_4
    const/4 v0, 0x1

    .line 2327712
    :goto_2
    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->F:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2327713
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->F:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->z:Ljava/lang/String;

    .line 2327714
    invoke-static {p0}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->v(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    goto :goto_1

    :cond_5
    move v0, v1

    .line 2327715
    goto :goto_2

    .line 2327716
    :cond_6
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->k:LX/0Uh;

    const/16 v2, 0x19

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->G:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-boolean v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->E:Z

    if-eqz v0, :cond_7

    .line 2327717
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2327718
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2327719
    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->G:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2327720
    const-string v2, "cuid"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2327721
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->B:Ljava/lang/String;

    .line 2327722
    invoke-static {p0}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->v(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 2327723
    :catch_0
    move-exception v0

    .line 2327724
    sget-object v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->o:Ljava/lang/Class;

    const-string v2, "jsonEncode Cuid failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2327725
    :cond_7
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->i:LX/0i4;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    .line 2327726
    sget-object v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->a:[Ljava/lang/String;

    new-instance v2, LX/GBk;

    invoke-direct {v2, p0}, LX/GBk;-><init>(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 2327727
    goto/16 :goto_1
.end method
