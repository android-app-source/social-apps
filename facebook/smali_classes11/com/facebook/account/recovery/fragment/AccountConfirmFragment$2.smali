.class public final Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

.field public final synthetic b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;Lcom/facebook/account/recovery/common/model/AccountCandidateModel;)V
    .locals 0

    .prologue
    .line 2327053
    iput-object p1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iput-object p2, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;->a:Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    const v0, 0x1a160428

    invoke-static {v3, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327054
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;->a:Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

    invoke-virtual {v2}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->i()LX/0Px;

    move-result-object v2

    .line 2327055
    iput-object v2, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->I:Ljava/util/List;

    .line 2327056
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;->a:Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

    invoke-virtual {v2}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->h()LX/0Px;

    move-result-object v2

    .line 2327057
    iput-object v2, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->J:Ljava/util/List;

    .line 2327058
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;->a:Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

    invoke-virtual {v2}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->j()LX/0Px;

    move-result-object v2

    .line 2327059
    iput-object v2, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->K:Ljava/util/List;

    .line 2327060
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v2, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;->a:Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

    invoke-virtual {v2}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->g()LX/0Px;

    move-result-object v2

    .line 2327061
    iput-object v2, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->L:Ljava/util/List;

    .line 2327062
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-static {v1}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->A(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327063
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-static {v1}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->B(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327064
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    .line 2327065
    iget-object v5, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->j:LX/2Dt;

    invoke-virtual {v5}, LX/2Dt;->c()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->k:LX/GB2;

    invoke-virtual {v5}, LX/GB2;->b()Z

    move-result v5

    if-nez v5, :cond_3

    .line 2327066
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->j:LX/2Dt;

    invoke-virtual {v1}, LX/2Dt;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-boolean v1, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->N:Z

    if-nez v1, :cond_2

    .line 2327067
    :cond_1
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    const/16 v5, 0x8

    const/4 v2, 0x0

    .line 2327068
    if-eqz v4, :cond_4

    move v6, v5

    .line 2327069
    :goto_1
    if-eqz v4, :cond_5

    .line 2327070
    :goto_2
    iget-object v5, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->u:Landroid/view/View;

    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2327071
    iget-object v2, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->s:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2327072
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    sget-object v2, LX/GBh;->CP_CHOOSE_SCREEN:LX/GBh;

    invoke-static {v1, v2}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->b(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;LX/GBh;)V

    .line 2327073
    :cond_2
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment$2;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    .line 2327074
    iget-object v5, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->n:Landroid/view/View;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2327075
    iget-boolean v5, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->N:Z

    if-eqz v5, :cond_6

    .line 2327076
    const v5, 0x7f0d0369

    invoke-virtual {v1, v5}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewStub;

    iput-object v5, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->v:Landroid/view/ViewStub;

    .line 2327077
    iget-object v5, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->v:Landroid/view/ViewStub;

    invoke-virtual {v5}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2327078
    const v5, 0x7f0d035f

    invoke-virtual {v1, v5}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->w:Landroid/widget/TextView;

    .line 2327079
    iget-object v5, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->w:Landroid/widget/TextView;

    const v6, 0x7f08352e

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 2327080
    invoke-static {v1}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->l(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327081
    :goto_3
    const v1, -0x8fb8a2a

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2327082
    :cond_3
    const/4 v5, 0x1

    iput-boolean v5, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->N:Z

    .line 2327083
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 2327084
    iget-object v5, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->k:LX/GB2;

    iget-object v6, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->H:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/GB2;->a(Ljava/lang/String;)Lcom/facebook/account/recovery/common/protocol/OpenIDConnectAccountRecoveryMethodParams;

    move-result-object v5

    .line 2327085
    const-string v6, "openIDConnectAccountRecoveryParamsKey"

    invoke-virtual {v7, v6, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2327086
    iget-object v11, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->b:LX/1Ck;

    const-string v12, "open_id_method_tag"

    iget-object v5, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->a:LX/0aG;

    const-string v6, "openid_connect_account_recovery"

    sget-object v8, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v9, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-static {v9}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v9

    const v10, 0x55f0b7b9

    invoke-static/range {v5 .. v10}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v5

    invoke-interface {v5}, LX/1MF;->start()LX/1ML;

    move-result-object v5

    new-instance v6, LX/GBV;

    invoke-direct {v6, v1}, LX/GBV;-><init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    invoke-virtual {v11, v12, v5, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_0

    :cond_4
    move v6, v2

    .line 2327087
    goto/16 :goto_1

    :cond_5
    move v2, v5

    .line 2327088
    goto/16 :goto_2

    .line 2327089
    :cond_6
    iget-boolean v5, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->M:Z

    if-eqz v5, :cond_7

    .line 2327090
    const v7, 0x7f0d0369

    invoke-virtual {v1, v7}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewStub;

    iput-object v7, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->v:Landroid/view/ViewStub;

    .line 2327091
    iget-object v7, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->v:Landroid/view/ViewStub;

    invoke-virtual {v7}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2327092
    const v7, 0x7f0d035f

    invoke-virtual {v1, v7}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->w:Landroid/widget/TextView;

    .line 2327093
    iget-object v7, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->w:Landroid/widget/TextView;

    const v8, 0x7f083529

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 2327094
    const-wide/16 v7, 0x2710

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const-wide/16 v9, 0x3e8

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-static {v7, v8}, LX/GaQ;->a(Ljava/lang/Long;Ljava/lang/Long;)LX/GaP;

    move-result-object v7

    iput-object v7, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->P:LX/GaP;

    .line 2327095
    iget-object v7, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->P:LX/GaP;

    new-instance v8, LX/GBc;

    invoke-direct {v8, v1}, LX/GBc;-><init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327096
    iput-object v8, v7, LX/GaP;->d:LX/GAx;

    .line 2327097
    iget-object v7, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->P:LX/GaP;

    invoke-virtual {v7}, LX/GaP;->c()V

    .line 2327098
    goto/16 :goto_3

    .line 2327099
    :cond_7
    invoke-static {v1}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->q(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    goto/16 :goto_3
.end method
