.class public Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2Ne;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/2Dt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/ui/search/SearchEditText;

.field public f:Landroid/widget/TextView;

.field public g:Landroid/widget/Button;

.field public h:Landroid/view/View;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Z

.field public l:LX/2Nk;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2328121
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2328122
    return-void
.end method

.method public static b$redex0(Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 2328115
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2328116
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2328117
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2328118
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->g:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 2328119
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->h:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2328120
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2328073
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2328074
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v2

    check-cast v2, LX/0aG;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/2Ne;->a(LX/0QB;)LX/2Ne;

    move-result-object p1

    check-cast p1, LX/2Ne;

    invoke-static {v0}, LX/2Dt;->b(LX/0QB;)LX/2Dt;

    move-result-object v0

    check-cast v0, LX/2Dt;

    iput-object v2, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->a:LX/0aG;

    iput-object v3, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->b:LX/1Ck;

    iput-object p1, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->c:LX/2Ne;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->d:LX/2Dt;

    .line 2328075
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/16 v2, 0x2a

    const v3, -0x2af816c9

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2328111
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/app/Activity;)V

    .line 2328112
    :try_start_0
    move-object v0, p1

    check-cast v0, LX/2Nk;

    move-object v1, v0

    iput-object v1, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->l:LX/2Nk;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2328113
    const v1, -0x57e36395

    invoke-static {v1, v2}, LX/02F;->f(II)V

    return-void

    .line 2328114
    :catch_0
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " must implement OnPasswordResetListener"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    const v3, -0x7704196b

    invoke-static {v3, v2}, LX/02F;->f(II)V

    throw v1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0xe4dad8c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2328110
    const v1, 0x7f0311d2

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x7d60c619

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3a7e75cf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2328106
    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->e:Lcom/facebook/ui/search/SearchEditText;

    .line 2328107
    invoke-static {v1}, Lcom/facebook/ui/search/SearchEditText;->i(Lcom/facebook/ui/search/SearchEditText;)V

    .line 2328108
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2328109
    const/16 v1, 0x2b

    const v2, -0x49af4764

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xaeafbeb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2328101
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2328102
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2328103
    if-nez v1, :cond_0

    .line 2328104
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x4b2388c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2328105
    :cond_0
    const v2, 0x7f08353f

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2328076
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2328077
    const v0, 0x7f0d29e3

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/search/SearchEditText;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->e:Lcom/facebook/ui/search/SearchEditText;

    .line 2328078
    const v0, 0x7f0d29e4

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->f:Landroid/widget/TextView;

    .line 2328079
    const v0, 0x7f0d29e5

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->g:Landroid/widget/Button;

    .line 2328080
    const v0, 0x7f0d29e6

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->h:Landroid/view/View;

    .line 2328081
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2328082
    if-eqz v0, :cond_0

    .line 2328083
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2328084
    const-string v1, "account_secret_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->i:Ljava/lang/String;

    .line 2328085
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2328086
    const-string v1, "account_confirmation_code"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->j:Ljava/lang/String;

    .line 2328087
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2328088
    const-string v1, "account_logout"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->k:Z

    .line 2328089
    :cond_0
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->e:Lcom/facebook/ui/search/SearchEditText;

    .line 2328090
    new-instance v1, LX/GC6;

    invoke-direct {v1, p0}, LX/GC6;-><init>(Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;)V

    move-object v1, v1

    .line 2328091
    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2328092
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->e:Lcom/facebook/ui/search/SearchEditText;

    new-instance v1, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment$1;

    invoke-direct {v1, p0}, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment$1;-><init>(Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;)V

    .line 2328093
    iput-object v1, v0, Lcom/facebook/ui/search/SearchEditText;->f:LX/7HK;

    .line 2328094
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->g:Landroid/widget/Button;

    new-instance v1, LX/GC5;

    invoke-direct {v1, p0}, LX/GC5;-><init>(Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2328095
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->c:LX/2Ne;

    iget-object v1, p0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->i:Ljava/lang/String;

    .line 2328096
    iget-object p0, v0, LX/2Ne;->a:LX/0Zb;

    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p2, LX/GB4;->CHANGE_PASSWORD_VIEWED:LX/GB4;

    invoke-virtual {p2}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p2, "account_recovery"

    .line 2328097
    iput-object p2, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2328098
    move-object p1, p1

    .line 2328099
    const-string p2, "crypted_id"

    invoke-virtual {p1, p2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const/4 p2, 0x1

    invoke-interface {p0, p1, p2}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2328100
    return-void
.end method
