.class public Lcom/facebook/account/recovery/fragment/LogoutFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/2Ne;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2Nj;

.field public c:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

.field public d:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

.field public e:Landroid/widget/TextView;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2327979
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2327980
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2327981
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2327982
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/account/recovery/fragment/LogoutFragment;

    invoke-static {v0}, LX/2Ne;->a(LX/0QB;)LX/2Ne;

    move-result-object v0

    check-cast v0, LX/2Ne;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/LogoutFragment;->a:LX/2Ne;

    .line 2327983
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/16 v2, 0x2a

    const v3, -0x639f0716

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2327984
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/app/Activity;)V

    .line 2327985
    :try_start_0
    move-object v0, p1

    check-cast v0, LX/2Nj;

    move-object v1, v0

    iput-object v1, p0, Lcom/facebook/account/recovery/fragment/LogoutFragment;->b:LX/2Nj;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2327986
    const v1, 0x5c009f8b

    invoke-static {v1, v2}, LX/02F;->f(II)V

    return-void

    .line 2327987
    :catch_0
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " must implement LogoutListener"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    const v3, 0x38a1d984

    invoke-static {v3, v2}, LX/02F;->f(II)V

    throw v1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0xe21010d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327988
    const v1, 0x7f030a5b

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x4c940252    # 7.7599376E7f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x67a10400

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327989
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2327990
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2327991
    if-nez v1, :cond_0

    .line 2327992
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x2e5d74f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2327993
    :cond_0
    const v2, 0x7f083538

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2327994
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2327995
    const v0, 0x7f0d1a5a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/LogoutFragment;->c:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 2327996
    const v0, 0x7f0d1a59

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/LogoutFragment;->d:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 2327997
    const v0, 0x7f0d0372

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/LogoutFragment;->e:Landroid/widget/TextView;

    .line 2327998
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/LogoutFragment;->c:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2327999
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/LogoutFragment;->d:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2328000
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2328001
    if-eqz v0, :cond_0

    .line 2328002
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2328003
    const-string v1, "account_secret_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/account/recovery/fragment/LogoutFragment;->f:Ljava/lang/String;

    .line 2328004
    :cond_0
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/LogoutFragment;->c:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    new-instance v1, LX/GC2;

    invoke-direct {v1, p0}, LX/GC2;-><init>(Lcom/facebook/account/recovery/fragment/LogoutFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2328005
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/LogoutFragment;->d:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    new-instance v1, LX/GC3;

    invoke-direct {v1, p0}, LX/GC3;-><init>(Lcom/facebook/account/recovery/fragment/LogoutFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2328006
    iget-object v0, p0, Lcom/facebook/account/recovery/fragment/LogoutFragment;->e:Landroid/widget/TextView;

    new-instance v1, LX/GC4;

    invoke-direct {v1, p0}, LX/GC4;-><init>(Lcom/facebook/account/recovery/fragment/LogoutFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2328007
    return-void
.end method
