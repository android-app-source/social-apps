.class public Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;
.super Lcom/facebook/widget/SwitchCompat;
.source ""


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2365877
    invoke-direct {p0, p1}, Lcom/facebook/widget/SwitchCompat;-><init>(Landroid/content/Context;)V

    .line 2365878
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;->a:Z

    .line 2365879
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2365880
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/SwitchCompat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2365881
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;->a:Z

    .line 2365882
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2365883
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/SwitchCompat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2365884
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;->a:Z

    .line 2365885
    return-void
.end method


# virtual methods
.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x25443b12

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2365886
    iget-boolean v0, p0, Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;->a:Z

    if-eqz v0, :cond_0

    .line 2365887
    invoke-super {p0, p1}, Lcom/facebook/widget/SwitchCompat;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x339b988f

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2365888
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    const v2, 0x7ee11197

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setSwitchingEnabled(Z)V
    .locals 0

    .prologue
    .line 2365889
    iput-boolean p1, p0, Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;->a:Z

    .line 2365890
    return-void
.end method
