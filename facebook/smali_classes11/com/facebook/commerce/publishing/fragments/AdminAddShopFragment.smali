.class public Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# static fields
.field public static final g:Ljava/lang/String;


# instance fields
.field public a:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/GXy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/GXt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/8Fe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7j6;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:J

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:LX/GYC;

.field public m:Landroid/view/ViewGroup;

.field public n:Landroid/view/ViewGroup;

.field public o:Landroid/view/ViewGroup;

.field public p:Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;

.field public q:Lcom/facebook/widget/text/BetterButton;

.field public r:LX/4At;

.field public s:LX/6WS;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2364912
    const-class v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2364906
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2364907
    iput-boolean v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->i:Z

    .line 2364908
    iput-boolean v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->j:Z

    .line 2364909
    iput-boolean v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->k:Z

    .line 2364910
    sget-object v0, LX/GYC;->SET_UP_SHOP:LX/GYC;

    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->l:LX/GYC;

    .line 2364911
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 14

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;

    invoke-static {v6}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    new-instance v8, LX/GXy;

    invoke-static {v6}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    invoke-static {v6}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v10

    check-cast v10, LX/0TD;

    invoke-static {v6}, LX/7j4;->b(LX/0QB;)LX/7j4;

    move-result-object v11

    check-cast v11, LX/7j4;

    invoke-static {v6}, LX/1mR;->a(LX/0QB;)LX/1mR;

    move-result-object v12

    check-cast v12, LX/1mR;

    invoke-static {v6}, LX/7jB;->a(LX/0QB;)LX/7jB;

    move-result-object v13

    check-cast v13, LX/7jB;

    invoke-direct/range {v8 .. v13}, LX/GXy;-><init>(LX/0tX;LX/0TD;LX/7j4;LX/1mR;LX/7jB;)V

    move-object v2, v8

    check-cast v2, LX/GXy;

    invoke-static {v6}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v3

    check-cast v3, LX/0kL;

    invoke-static {v6}, LX/GXt;->a(LX/0QB;)LX/GXt;

    move-result-object v4

    check-cast v4, LX/GXt;

    invoke-static {v6}, LX/8Fe;->a(LX/0QB;)LX/8Fe;

    move-result-object v5

    check-cast v5, LX/8Fe;

    const/16 v7, 0x18c5

    invoke-static {v6, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    iput-object v1, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->a:LX/1Ck;

    iput-object v2, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->b:LX/GXy;

    iput-object v3, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->c:LX/0kL;

    iput-object v4, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->d:LX/GXt;

    iput-object v5, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->e:LX/8Fe;

    iput-object v6, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->f:LX/0Ot;

    return-void
.end method

.method public static m(Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x12c

    .line 2364899
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->d:LX/GXt;

    sget-object v1, LX/GXq;->ACTN_PRIVATE_MESSAGE_BACK_CLICK:LX/GXq;

    iget-wide v2, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->h:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/GXt;->a(LX/GXq;Ljava/lang/String;)V

    .line 2364900
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->m:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    .line 2364901
    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->n:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 2364902
    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->o:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 2364903
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->o:Landroid/view/ViewGroup;

    new-instance v1, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment$5;

    invoke-direct {v1, p0}, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment$5;-><init>(Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/view/ViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2364904
    sget-object v0, LX/GYC;->ANIMATING:LX/GYC;

    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->l:LX/GYC;

    .line 2364905
    return-void
.end method

.method public static n(Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;)V
    .locals 14

    .prologue
    .line 2364854
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->t:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2364855
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->c:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0829b8

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2364856
    :goto_0
    return-void

    .line 2364857
    :cond_0
    iget-object v3, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->r:LX/4At;

    invoke-virtual {v3}, LX/4At;->beginShowingProgress()V

    .line 2364858
    iget-object v3, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->a:LX/1Ck;

    sget-object v4, LX/GYD;->CREATE_SHOP_MUTATION:LX/GYD;

    iget-object v5, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->b:LX/GXy;

    iget-wide v7, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->h:J

    iget-object v6, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->t:Ljava/lang/String;

    .line 2364859
    new-instance v9, LX/7jp;

    invoke-direct {v9}, LX/7jp;-><init>()V

    move-object v9, v9

    .line 2364860
    const-string v10, "input"

    new-instance v11, LX/4DV;

    invoke-direct {v11}, LX/4DV;-><init>()V

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    .line 2364861
    const-string v13, "page_id"

    invoke-virtual {v11, v13, v12}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2364862
    move-object v11, v11

    .line 2364863
    const-string v12, "currency"

    invoke-virtual {v11, v12, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2364864
    move-object v11, v11

    .line 2364865
    invoke-virtual {v9, v10, v11}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v9

    const-string v10, "COMMERCE_SMALL_IMAGE_SIZE"

    const/16 v11, 0x32

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v9

    const-string v10, "COMMERCE_MEDIUM_IMAGE_SIZE"

    const/16 v11, 0xe6

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v9

    check-cast v9, LX/7jp;

    invoke-static {v9}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v9

    .line 2364866
    iget-object v10, v5, LX/GXy;->a:LX/0tX;

    invoke-virtual {v10, v9}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    invoke-static {v9}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    .line 2364867
    new-instance v10, LX/GXv;

    invoke-direct {v10, v5}, LX/GXv;-><init>(LX/GXy;)V

    invoke-static {v9, v10}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2364868
    move-object v5, v9

    .line 2364869
    new-instance v6, LX/GY7;

    invoke-direct {v6, p0}, LX/GY7;-><init>(Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;)V

    invoke-virtual {v3, v4, v5, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2364870
    goto :goto_0
.end method


# virtual methods
.method public final S_()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2364896
    sget-object v1, LX/GY4;->a:[I

    iget-object v2, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->l:LX/GYC;

    invoke-virtual {v2}, LX/GYC;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2364897
    const/4 v0, 0x0

    :goto_0
    :pswitch_0
    return v0

    .line 2364898
    :pswitch_1
    invoke-static {p0}, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->m(Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2364913
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2364914
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2364915
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2364916
    sget-object v1, LX/GXu;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->h:J

    .line 2364917
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->e:LX/8Fe;

    invoke-virtual {v0}, LX/8Fe;->a()V

    .line 2364918
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->e:LX/8Fe;

    iget-wide v2, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8Fe;->a(Ljava/lang/String;)V

    .line 2364919
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x361bd7d9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2364895
    const v1, 0x7f0300b3

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x2d44148d

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, 0x38aeb032

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2364890
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2364891
    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->e:LX/8Fe;

    invoke-virtual {v1}, LX/8Fe;->b()V

    .line 2364892
    iget-boolean v1, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->j:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->k:Z

    if-nez v1, :cond_0

    .line 2364893
    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->d:LX/GXt;

    sget-object v2, LX/GXq;->ACTN_CANCEL_CURRENCY_SELECTION:LX/GXq;

    iget-wide v4, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->h:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/GXt;->a(LX/GXq;Ljava/lang/String;)V

    .line 2364894
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x32735606

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 12

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x57e5c7f3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2364880
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2364881
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2364882
    if-eqz v1, :cond_0

    .line 2364883
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2364884
    const v2, 0x7f0829b3

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2364885
    :cond_0
    iget-object v4, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->s:LX/6WS;

    if-eqz v4, :cond_1

    .line 2364886
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x59a5a35c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2364887
    :cond_1
    iget-object v4, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->a:LX/1Ck;

    sget-object v5, LX/GYD;->FETCH_ADD_SHOP_FIELDS:LX/GYD;

    iget-object v6, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->b:LX/GXy;

    iget-wide v8, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->h:J

    .line 2364888
    iget-object v7, v6, LX/GXy;->b:LX/0TD;

    new-instance v10, LX/GXw;

    invoke-direct {v10, v6, v8, v9}, LX/GXw;-><init>(LX/GXy;J)V

    invoke-interface {v7, v10}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v6, v7

    .line 2364889
    new-instance v7, LX/GY5;

    invoke-direct {v7, p0}, LX/GY5;-><init>(Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;)V

    invoke-virtual {v4, v5, v6, v7}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2effe7d8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2364877
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2364878
    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->a:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2364879
    const/16 v1, 0x2b

    const v2, 0x62f31170

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2364871
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2364872
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2364873
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    .line 2364874
    :cond_0
    const v0, 0x7f0d04df

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->m:Landroid/view/ViewGroup;

    .line 2364875
    new-instance v0, LX/4At;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0829bb

    invoke-direct {v0, v1, v2}, LX/4At;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->r:LX/4At;

    .line 2364876
    return-void
.end method
