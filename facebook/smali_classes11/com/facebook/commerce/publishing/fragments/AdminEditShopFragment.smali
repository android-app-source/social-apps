.class public Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# static fields
.field public static final j:Ljava/lang/String;


# instance fields
.field public a:LX/7j6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/GY1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/GXZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/7jB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/7ka;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/8Fe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:Landroid/support/v7/widget/RecyclerView;

.field private l:Landroid/widget/ProgressBar;

.field public m:LX/4At;

.field public n:J

.field private o:Z

.field private p:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

.field public q:LX/GXY;

.field public final r:LX/1B1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2365119
    const-class v0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->j:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2365116
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2365117
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->r:LX/1B1;

    .line 2365118
    return-void
.end method

.method public static a$redex0(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2365111
    iget-object v3, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->l:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2365112
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->k:Landroid/support/v7/widget/RecyclerView;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2365113
    return-void

    :cond_0
    move v0, v2

    .line 2365114
    goto :goto_0

    :cond_1
    move v2, v1

    .line 2365115
    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;ZI)V
    .locals 7

    .prologue
    .line 2365103
    invoke-static {p0, p1}, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->a$redex0(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;Z)V

    .line 2365104
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->b:LX/1Ck;

    sget-object v1, LX/GYM;->FETCH_COMMERCE_STORE_QUERY:LX/GYM;

    iget-object v2, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->c:LX/GY1;

    iget-wide v4, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->n:J

    .line 2365105
    new-instance v3, LX/7kJ;

    invoke-direct {v3}, LX/7kJ;-><init>()V

    move-object v3, v3

    .line 2365106
    const-string v6, "page_id"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, v6, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v6, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object p1

    invoke-virtual {v3, v6, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v3

    const-string v6, "COMMERCE_MEDIUM_IMAGE_SIZE"

    const/16 p1, 0xe6

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v3, v6, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v6, "COMMERCE_SMALL_IMAGE_SIZE"

    const/16 p1, 0x32

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v3, v6, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/7kJ;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 2365107
    sget-object v6, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v3, v6}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    .line 2365108
    iget-object v6, v2, LX/GY1;->a:LX/0tX;

    invoke-virtual {v6, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    invoke-static {v3}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v2, v3

    .line 2365109
    new-instance v3, LX/GYI;

    invoke-direct {v3, p0, p2}, LX/GYI;-><init>(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;I)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2365110
    return-void
.end method

.method public static b$redex0(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;Z)V
    .locals 3

    .prologue
    .line 2365097
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->q:LX/GXY;

    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->g:LX/7ka;

    invoke-virtual {v1}, LX/7ka;->a()LX/0Px;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->g:LX/7ka;

    invoke-virtual {v2}, LX/7ka;->b()LX/0P1;

    move-result-object v2

    .line 2365098
    iput-object v1, v0, LX/GXY;->b:LX/0Px;

    .line 2365099
    iput-object v2, v0, LX/GXY;->c:LX/0P1;

    .line 2365100
    if-eqz p1, :cond_0

    iget-object p0, v0, LX/GXY;->d:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    if-eqz p0, :cond_0

    .line 2365101
    iget-object p0, v0, LX/GXY;->d:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2365102
    :cond_0
    return-void
.end method

.method public static l(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;)V
    .locals 4

    .prologue
    .line 2365090
    invoke-static {p0}, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->m(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;)V

    .line 2365091
    iget-boolean v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->o:Z

    if-eqz v0, :cond_0

    .line 2365092
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2365093
    :goto_0
    return-void

    .line 2365094
    :cond_0
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->a:LX/7j6;

    iget-wide v2, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->n:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 2365095
    const/4 p0, 0x0

    invoke-virtual {v0, v1, v2, v3, p0}, LX/7j6;->a(Ljava/lang/String;Ljava/lang/String;ZLX/7iP;)V

    .line 2365096
    goto :goto_0
.end method

.method private static m(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;)V
    .locals 7

    .prologue
    .line 2365071
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->q:LX/GXY;

    .line 2365072
    iget-object v1, v0, LX/GXY;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/GXY;->e:Ljava/lang/String;

    invoke-static {v0}, LX/GXY;->i(LX/GXY;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2365073
    if-eqz v0, :cond_0

    .line 2365074
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->c:LX/GY1;

    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->q:LX/GXY;

    .line 2365075
    iget-object v2, v1, LX/GXY;->a:Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    if-eqz v2, :cond_2

    iget-object v2, v1, LX/GXY;->a:Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;->k()Ljava/lang/String;

    move-result-object v2

    :goto_1
    move-object v1, v2

    .line 2365076
    iget-object v2, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->q:LX/GXY;

    invoke-virtual {v2}, LX/GXY;->e()Ljava/lang/String;

    move-result-object v2

    .line 2365077
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2365078
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2365079
    new-instance v3, LX/7jx;

    invoke-direct {v3}, LX/7jx;-><init>()V

    move-object v3, v3

    .line 2365080
    const-string v4, "input"

    new-instance v5, LX/4DZ;

    invoke-direct {v5}, LX/4DZ;-><init>()V

    .line 2365081
    const-string v6, "id"

    invoke-virtual {v5, v6, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2365082
    move-object v5, v5

    .line 2365083
    const-string v6, "intro_summary"

    invoke-virtual {v5, v6, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2365084
    move-object v5, v5

    .line 2365085
    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v3

    const-string v4, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v3

    const-string v4, "COMMERCE_SMALL_IMAGE_SIZE"

    const/16 v5, 0x32

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "COMMERCE_MEDIUM_IMAGE_SIZE"

    const/16 v5, 0xe6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/7jx;

    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 2365086
    iget-object v4, v0, LX/GY1;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    invoke-static {v3}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2365087
    new-instance v4, LX/GY0;

    invoke-direct {v4, v0}, LX/GY0;-><init>(LX/GY1;)V

    invoke-static {v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2365088
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->e:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0829a4

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2365089
    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static o(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;)Z
    .locals 3

    .prologue
    .line 2365120
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->h:LX/8Fe;

    .line 2365121
    iget-object v1, v0, LX/8Fe;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v0, v1

    .line 2365122
    if-eqz v0, :cond_0

    .line 2365123
    const/4 v0, 0x1

    .line 2365124
    :goto_0
    return v0

    .line 2365125
    :cond_0
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->i:LX/03V;

    const-class v1, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "No Page ViewerContext available."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2365126
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 2365069
    invoke-static {p0}, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->m(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;)V

    .line 2365070
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2365043
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2365044
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    invoke-static {v0}, LX/7j6;->a(LX/0QB;)LX/7j6;

    move-result-object v3

    check-cast v3, LX/7j6;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    new-instance v8, LX/GY1;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/1mR;->a(LX/0QB;)LX/1mR;

    move-result-object v6

    check-cast v6, LX/1mR;

    invoke-static {v0}, LX/7jB;->a(LX/0QB;)LX/7jB;

    move-result-object v7

    check-cast v7, LX/7jB;

    invoke-direct {v8, v5, v6, v7}, LX/GY1;-><init>(LX/0tX;LX/1mR;LX/7jB;)V

    move-object v5, v8

    check-cast v5, LX/GY1;

    const-class v6, LX/GXZ;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/GXZ;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v7

    check-cast v7, LX/0kL;

    invoke-static {v0}, LX/7jB;->a(LX/0QB;)LX/7jB;

    move-result-object v8

    check-cast v8, LX/7jB;

    invoke-static {v0}, LX/7ka;->a(LX/0QB;)LX/7ka;

    move-result-object v9

    check-cast v9, LX/7ka;

    invoke-static {v0}, LX/8Fe;->a(LX/0QB;)LX/8Fe;

    move-result-object v10

    check-cast v10, LX/8Fe;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v3, v2, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->a:LX/7j6;

    iput-object v4, v2, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->b:LX/1Ck;

    iput-object v5, v2, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->c:LX/GY1;

    iput-object v6, v2, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->d:LX/GXZ;

    iput-object v7, v2, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->e:LX/0kL;

    iput-object v8, v2, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->f:LX/7jB;

    iput-object v9, v2, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->g:LX/7ka;

    iput-object v10, v2, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->h:LX/8Fe;

    iput-object v0, v2, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->i:LX/03V;

    .line 2365045
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2365046
    sget-object v1, LX/GXu;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->n:J

    .line 2365047
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2365048
    const-string v1, "extra_finish_on_launch_view_shop"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->o:Z

    .line 2365049
    new-instance v0, LX/GXY;

    invoke-direct {v0}, LX/GXY;-><init>()V

    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->q:LX/GXY;

    .line 2365050
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->d:LX/GXZ;

    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->q:LX/GXY;

    .line 2365051
    new-instance v4, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    invoke-static {v0}, Lcom/facebook/commerce/core/ui/ProductItemViewBinder;->a(LX/0QB;)Lcom/facebook/commerce/core/ui/ProductItemViewBinder;

    move-result-object v7

    check-cast v7, Lcom/facebook/commerce/core/ui/ProductItemViewBinder;

    .line 2365052
    new-instance v8, LX/GZ1;

    invoke-direct {v8}, LX/GZ1;-><init>()V

    .line 2365053
    move-object v8, v8

    .line 2365054
    move-object v8, v8

    .line 2365055
    check-cast v8, LX/GZ1;

    move-object v9, v1

    invoke-direct/range {v4 .. v9}, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/facebook/commerce/core/ui/ProductItemViewBinder;LX/GZ1;LX/GXY;)V

    .line 2365056
    move-object v0, v4

    .line 2365057
    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->p:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    .line 2365058
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->p:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    .line 2365059
    iput-object p0, v0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->k:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    .line 2365060
    if-eqz p1, :cond_0

    .line 2365061
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->q:LX/GXY;

    .line 2365062
    const-string v1, "admin_shop_adapter_data_provider_state_root"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .line 2365063
    const-string v2, "admin_shop_adapter_data_provider_state_root.edited_intro_summary"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/GXY;->e:Ljava/lang/String;

    .line 2365064
    :cond_0
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->r:LX/1B1;

    new-instance v1, LX/GYG;

    invoke-direct {v1, p0}, LX/GYG;-><init>(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2365065
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->r:LX/1B1;

    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->f:LX/7jB;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 2365066
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->h:LX/8Fe;

    invoke-virtual {v0}, LX/8Fe;->a()V

    .line 2365067
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->h:LX/8Fe;

    iget-wide v2, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->n:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8Fe;->a(Ljava/lang/String;)V

    .line 2365068
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x6de4f92d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2365042
    const v1, 0x7f0300b6

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x465dd55e

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2eb27bf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2365038
    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->r:LX/1B1;

    iget-object v2, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->f:LX/7jB;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2365039
    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->h:LX/8Fe;

    invoke-virtual {v1}, LX/8Fe;->b()V

    .line 2365040
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2365041
    const/16 v1, 0x2b

    const v2, 0x76dfcfb6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2365032
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2365033
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->q:LX/GXY;

    .line 2365034
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2365035
    const-string v2, "admin_shop_adapter_data_provider_state_root.edited_intro_summary"

    iget-object p0, v0, LX/GXY;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2365036
    const-string v2, "admin_shop_adapter_data_provider_state_root"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2365037
    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/16 v0, 0x2a

    const v1, 0x7272e5dc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2365019
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2365020
    invoke-static {p0, v2}, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->b$redex0(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;Z)V

    .line 2365021
    const/4 v1, 0x0

    invoke-static {p0, v2, v1}, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->a$redex0(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;ZI)V

    .line 2365022
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2365023
    if-eqz v1, :cond_0

    .line 2365024
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2365025
    const v2, 0x7f0814c6

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2365026
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    const v4, 0x7f0829b4

    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2365027
    iput-object v4, v2, LX/108;->g:Ljava/lang/String;

    .line 2365028
    move-object v2, v2

    .line 2365029
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2365030
    new-instance v2, LX/GYF;

    invoke-direct {v2, p0}, LX/GYF;-><init>(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;)V

    invoke-interface {v1, v2}, LX/1ZF;->a(LX/63W;)V

    .line 2365031
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x4053d428

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1fd6dcef

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2365016
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2365017
    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->b:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2365018
    const/16 v1, 0x2b

    const v2, -0xf930523

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2365010
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2365011
    const v0, 0x7f0d04e7

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->k:Landroid/support/v7/widget/RecyclerView;

    .line 2365012
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->k:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->p:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2365013
    const v0, 0x7f0d04de

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->l:Landroid/widget/ProgressBar;

    .line 2365014
    new-instance v0, LX/4At;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0829bb

    invoke-direct {v0, v1, v2}, LX/4At;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->m:LX/4At;

    .line 2365015
    return-void
.end method
