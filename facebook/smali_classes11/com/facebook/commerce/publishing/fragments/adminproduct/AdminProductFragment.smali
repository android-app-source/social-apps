.class public Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public A:Z

.field public B:Z

.field public C:LX/GYi;

.field public D:Z

.field public E:Landroid/view/ViewGroup;

.field public F:Landroid/widget/ProgressBar;

.field public G:Lcom/facebook/commerce/core/ui/NoticeView;

.field public H:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public I:Lcom/facebook/resources/ui/FbEditText;

.field public J:Lcom/facebook/resources/ui/FbEditText;

.field public K:Lcom/facebook/resources/ui/FbEditText;

.field public L:Lcom/facebook/fbui/glyph/GlyphView;

.field public M:Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;

.field public N:Landroid/view/View;

.field public O:Lcom/facebook/commerce/core/ui/NoticeView;

.field public P:Lcom/facebook/widget/text/BetterTextView;

.field public Q:Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;

.field public R:LX/GXS;

.field public S:LX/4At;

.field public final T:LX/1B1;

.field public U:LX/Bc4;

.field public V:LX/GYQ;

.field private W:LX/GYo;

.field public X:Ljava/lang/String;

.field public final Y:Landroid/view/View$OnClickListener;

.field public a:LX/GXn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1EZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/8LV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/7kY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/7j4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/7ka;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/GXt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/7j9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/7jB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/GZ4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/Bc5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/GYR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/GYp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public t:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public u:Z

.field public v:J

.field public w:Ljava/util/Currency;

.field public x:LX/7ja;

.field public y:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsInterfaces$AdminCommerceProductItem$OrderedImages;",
            ">;"
        }
    .end annotation
.end field

.field public z:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2365585
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2365586
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->B:Z

    .line 2365587
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->T:LX/1B1;

    .line 2365588
    new-instance v0, LX/GYV;

    invoke-direct {v0, p0}, LX/GYV;-><init>(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V

    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->Y:Landroid/view/View$OnClickListener;

    .line 2365589
    return-void
.end method

.method public static B(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V
    .locals 3

    .prologue
    .line 2365590
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2365591
    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->I:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2365592
    return-void
.end method

.method public static C(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)Z
    .locals 1

    .prologue
    .line 2365593
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static E(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2365594
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->e:LX/7j4;

    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->w:Ljava/util/Currency;

    iget-object v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->J:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/7j4;->a(Ljava/util/Currency;Ljava/lang/String;)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/GXq;)V
    .locals 4

    .prologue
    .line 2365609
    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->h:LX/GXt;

    iget-wide v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->v:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-interface {v0}, LX/7ja;->e()Ljava/lang/String;

    move-result-object v0

    .line 2365610
    :goto_0
    invoke-static {p1, v2}, LX/GXt;->b(LX/GXq;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2365611
    sget-object p0, LX/GXr;->TARGET_ID:LX/GXr;

    iget-object p0, p0, LX/GXr;->value:Ljava/lang/String;

    invoke-virtual {v3, p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2365612
    iget-object p0, v1, LX/GXt;->a:LX/0Zb;

    invoke-interface {p0, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2365613
    return-void

    .line 2365614
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private static a(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;LX/GXn;LX/1EZ;LX/8LV;LX/7kY;LX/7j4;LX/7ka;LX/1Ck;LX/GXt;LX/7j9;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/0kL;LX/7jB;LX/03V;LX/GZ4;LX/Bc5;LX/GYR;LX/GYp;)V
    .locals 1

    .prologue
    .line 2365595
    iput-object p1, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->a:LX/GXn;

    iput-object p2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->b:LX/1EZ;

    iput-object p3, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->c:LX/8LV;

    iput-object p4, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->d:LX/7kY;

    iput-object p5, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->e:LX/7j4;

    iput-object p6, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->f:LX/7ka;

    iput-object p7, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->g:LX/1Ck;

    iput-object p8, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->h:LX/GXt;

    iput-object p9, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->i:LX/7j9;

    iput-object p10, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->j:Lcom/facebook/content/SecureContextHelper;

    iput-object p11, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->k:LX/17Y;

    iput-object p12, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->l:LX/0kL;

    iput-object p13, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->m:LX/7jB;

    iput-object p14, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->n:LX/03V;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->o:LX/GZ4;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->p:LX/Bc5;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->q:LX/GYR;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->r:LX/GYp;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 22

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v20

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;

    const-class v3, LX/GXn;

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/GXn;

    invoke-static/range {v20 .. v20}, LX/1EZ;->a(LX/0QB;)LX/1EZ;

    move-result-object v4

    check-cast v4, LX/1EZ;

    invoke-static/range {v20 .. v20}, LX/8LV;->a(LX/0QB;)LX/8LV;

    move-result-object v5

    check-cast v5, LX/8LV;

    invoke-static/range {v20 .. v20}, LX/7kY;->a(LX/0QB;)LX/7kY;

    move-result-object v6

    check-cast v6, LX/7kY;

    invoke-static/range {v20 .. v20}, LX/7j4;->a(LX/0QB;)LX/7j4;

    move-result-object v7

    check-cast v7, LX/7j4;

    invoke-static/range {v20 .. v20}, LX/7ka;->a(LX/0QB;)LX/7ka;

    move-result-object v8

    check-cast v8, LX/7ka;

    invoke-static/range {v20 .. v20}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v9

    check-cast v9, LX/1Ck;

    invoke-static/range {v20 .. v20}, LX/GXt;->a(LX/0QB;)LX/GXt;

    move-result-object v10

    check-cast v10, LX/GXt;

    invoke-static/range {v20 .. v20}, LX/7j9;->a(LX/0QB;)LX/7j9;

    move-result-object v11

    check-cast v11, LX/7j9;

    invoke-static/range {v20 .. v20}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v12

    check-cast v12, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v20 .. v20}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v13

    check-cast v13, LX/17Y;

    invoke-static/range {v20 .. v20}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v14

    check-cast v14, LX/0kL;

    invoke-static/range {v20 .. v20}, LX/7jB;->a(LX/0QB;)LX/7jB;

    move-result-object v15

    check-cast v15, LX/7jB;

    invoke-static/range {v20 .. v20}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v16

    check-cast v16, LX/03V;

    invoke-static/range {v20 .. v20}, LX/GZ4;->a(LX/0QB;)LX/GZ4;

    move-result-object v17

    check-cast v17, LX/GZ4;

    const-class v18, LX/Bc5;

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v18

    check-cast v18, LX/Bc5;

    const-class v19, LX/GYR;

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v19

    check-cast v19, LX/GYR;

    const-class v21, LX/GYp;

    invoke-interface/range {v20 .. v21}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v20

    check-cast v20, LX/GYp;

    invoke-static/range {v2 .. v20}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->a(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;LX/GXn;LX/1EZ;LX/8LV;LX/7kY;LX/7j4;LX/7ka;LX/1Ck;LX/GXt;LX/7j9;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/0kL;LX/7jB;LX/03V;LX/GZ4;LX/Bc5;LX/GYR;LX/GYp;)V

    return-void
.end method

.method public static a(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;LX/0Px;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2365596
    iget-object v4, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->y:LX/0Px;

    .line 2365597
    if-eqz v4, :cond_0

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2365598
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v2, v3

    .line 2365599
    :cond_1
    :goto_0
    return v2

    .line 2365600
    :cond_2
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v1

    if-eq v0, v1, :cond_3

    move v2, v3

    .line 2365601
    goto :goto_0

    :cond_3
    move v1, v2

    .line 2365602
    :goto_1
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2365603
    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$OrderedImagesModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$OrderedImagesModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 2365604
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v5, v0}, LX/GZ3;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    move v2, v3

    .line 2365605
    goto :goto_0

    .line 2365606
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;I)V
    .locals 2

    .prologue
    .line 2365607
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2365608
    return-void
.end method

.method public static a$redex0(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;LX/7ja;LX/7jD;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2365551
    iput-boolean v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->B:Z

    .line 2365552
    invoke-static {p0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->B(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V

    .line 2365553
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2365554
    const-string v2, "result_optimistic_product"

    invoke-static {v1, v2, p1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2365555
    const-string v2, "result_mutation_method"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2365556
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, -0x1

    invoke-virtual {v2, v3, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2365557
    sget-object v1, LX/7jD;->CREATE:LX/7jD;

    if-ne p2, v1, :cond_1

    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->U:LX/Bc4;

    .line 2365558
    iget-boolean v2, v1, LX/Bc4;->a:Z

    move v1, v2

    .line 2365559
    if-eqz v1, :cond_1

    .line 2365560
    :goto_0
    if-eqz v0, :cond_2

    .line 2365561
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->U:LX/Bc4;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, LX/GYU;

    invoke-direct {v2, p0}, LX/GYU;-><init>(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V

    .line 2365562
    invoke-static {v0}, LX/Bc4;->b(LX/Bc4;)V

    .line 2365563
    invoke-static {v0}, LX/Bc4;->b(LX/Bc4;)V

    .line 2365564
    iget-object v3, v0, LX/Bc4;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2365565
    invoke-static {v1}, LX/Bc2;->a(Landroid/app/Activity;)LX/31Y;

    move-result-object v3

    .line 2365566
    const p1, 0x7f0829cb

    invoke-virtual {v3, p1}, LX/0ju;->a(I)LX/0ju;

    move-result-object p1

    const p2, 0x7f0829cc

    invoke-virtual {p1, p2}, LX/0ju;->b(I)LX/0ju;

    move-result-object p1

    const p2, 0x7f080016

    new-instance p0, LX/Bc3;

    invoke-direct {p0, v0}, LX/Bc3;-><init>(LX/Bc4;)V

    invoke-virtual {p1, p2, p0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2365567
    if-eqz v2, :cond_0

    .line 2365568
    invoke-virtual {v3, v2}, LX/0ju;->a(Landroid/content/DialogInterface$OnDismissListener;)LX/0ju;

    .line 2365569
    :cond_0
    invoke-virtual {v3}, LX/0ju;->a()LX/2EJ;

    move-result-object v3

    move-object v3, v3

    .line 2365570
    invoke-virtual {v3}, LX/2EJ;->show()V

    .line 2365571
    :goto_1
    return-void

    .line 2365572
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2365573
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;Z)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2365574
    iget-object v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->J:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2365575
    if-eqz p1, :cond_0

    .line 2365576
    :goto_0
    return v0

    .line 2365577
    :cond_0
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->J:Lcom/facebook/resources/ui/FbEditText;

    const v2, 0x7f08299a

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbEditText;->setError(Ljava/lang/CharSequence;)V

    move v0, v1

    .line 2365578
    goto :goto_0

    .line 2365579
    :cond_1
    invoke-static {p0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->E(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v2

    .line 2365580
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;->a()I

    move-result v3

    if-gez v3, :cond_3

    .line 2365581
    :cond_2
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->J:Lcom/facebook/resources/ui/FbEditText;

    const v2, 0x7f08299b

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbEditText;->setError(Ljava/lang/CharSequence;)V

    move v0, v1

    .line 2365582
    goto :goto_0

    .line 2365583
    :cond_3
    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->J:Lcom/facebook/resources/ui/FbEditText;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbEditText;->setError(Ljava/lang/CharSequence;)V

    .line 2365584
    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->J:Lcom/facebook/resources/ui/FbEditText;

    invoke-static {v2}, LX/7j4;->a(Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static l(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V
    .locals 4

    .prologue
    .line 2365298
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2365299
    if-eqz v0, :cond_0

    .line 2365300
    invoke-static {p0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f082995

    :goto_0
    invoke-interface {v0, v1}, LX/1ZF;->x_(I)V

    .line 2365301
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2365302
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0829a3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2365303
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2365304
    move-object v1, v1

    .line 2365305
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2365306
    new-instance v1, LX/GYf;

    invoke-direct {v1, p0}, LX/GYf;-><init>(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    .line 2365307
    :cond_0
    return-void

    .line 2365308
    :cond_1
    const v1, 0x7f082996

    goto :goto_0
.end method

.method public static q(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2365309
    invoke-static {p0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-interface {v0}, LX/7ja;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->M:Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;

    invoke-virtual {v0}, Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 2365310
    :goto_0
    iget v3, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->z:I

    const/16 v4, 0xa

    if-lt v3, v4, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    .line 2365311
    :cond_1
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->M:Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;

    .line 2365312
    iput-boolean v2, v0, Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;->a:Z

    .line 2365313
    return-void

    :cond_2
    move v0, v2

    .line 2365314
    goto :goto_0
.end method

.method public static t(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V
    .locals 14

    .prologue
    .line 2365315
    const/4 v0, 0x0

    .line 2365316
    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->I:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2365317
    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->I:Lcom/facebook/resources/ui/FbEditText;

    const v2, 0x7f082998

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->setError(Ljava/lang/CharSequence;)V

    .line 2365318
    :cond_0
    :goto_0
    move v0, v0

    .line 2365319
    if-nez v0, :cond_1

    .line 2365320
    :goto_1
    return-void

    .line 2365321
    :cond_1
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2365322
    new-instance v2, LX/GYh;

    invoke-direct {v2}, LX/GYh;-><init>()V

    .line 2365323
    new-instance v3, LX/7jY;

    invoke-direct {v3}, LX/7jY;-><init>()V

    .line 2365324
    invoke-static {p0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->E(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v4

    .line 2365325
    if-nez v4, :cond_b

    .line 2365326
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->n:LX/03V;

    const-string v1, "ProductTools"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not parse price: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->J:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with currency "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->w:Ljava/util/Currency;

    invoke-virtual {v3}, Ljava/util/Currency;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2365327
    const/4 v0, 0x0

    .line 2365328
    :goto_2
    move-object v0, v0

    .line 2365329
    if-nez v0, :cond_2

    .line 2365330
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->l:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0814c5

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    goto :goto_1

    .line 2365331
    :cond_2
    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v13, 0x0

    .line 2365332
    iget-object v3, v0, LX/GYh;->a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    if-eqz v3, :cond_4

    iget-object v3, v0, LX/GYh;->a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    .line 2365333
    iget-object v4, v3, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->a:Ljava/lang/String;

    if-nez v4, :cond_3

    iget-object v4, v3, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->b:Ljava/lang/String;

    if-nez v4, :cond_3

    iget-object v4, v3, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->c:Ljava/lang/String;

    if-nez v4, :cond_3

    iget-object v4, v3, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->d:Ljava/lang/String;

    if-nez v4, :cond_3

    iget-object v4, v3, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->e:Ljava/lang/String;

    if-nez v4, :cond_3

    iget-object v4, v3, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->f:Ljava/lang/String;

    if-nez v4, :cond_3

    iget-object v4, v3, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->g:Ljava/lang/Integer;

    if-nez v4, :cond_3

    iget-object v4, v3, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->h:Ljava/lang/String;

    if-nez v4, :cond_3

    iget-object v4, v3, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->i:LX/0Px;

    if-nez v4, :cond_3

    iget-object v4, v3, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->j:LX/03R;

    sget-object v5, LX/03R;->UNSET:LX/03R;

    if-ne v4, v5, :cond_3

    iget-object v4, v3, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->k:LX/03R;

    sget-object v5, LX/03R;->UNSET:LX/03R;

    if-eq v4, v5, :cond_1f

    :cond_3
    const/4 v4, 0x1

    :goto_3
    move v3, v4

    .line 2365334
    if-nez v3, :cond_5

    :cond_4
    invoke-virtual {v0}, LX/GYh;->b()Z

    move-result v3

    if-eqz v3, :cond_1e

    :cond_5
    const/4 v3, 0x1

    :goto_4
    move v3, v3

    .line 2365335
    if-nez v3, :cond_15

    .line 2365336
    :goto_5
    move-object v1, v11

    .line 2365337
    if-nez v1, :cond_6

    .line 2365338
    invoke-static {p0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->B(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V

    .line 2365339
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 2365340
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2365341
    goto/16 :goto_1

    .line 2365342
    :cond_6
    iget-boolean v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->A:Z

    if-eqz v0, :cond_7

    .line 2365343
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->S:LX/4At;

    invoke-virtual {v0}, LX/4At;->beginShowingProgress()V

    goto/16 :goto_1

    .line 2365344
    :cond_7
    invoke-static {p0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, LX/7jD;->CREATE:LX/7jD;

    :goto_6
    invoke-static {p0, v1, v0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->a$redex0(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;LX/7ja;LX/7jD;)V

    goto/16 :goto_1

    :cond_8
    sget-object v0, LX/7jD;->EDIT:LX/7jD;

    goto :goto_6

    .line 2365345
    :cond_9
    invoke-static {p0, v0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->a$redex0(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2365346
    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->R:LX/GXS;

    .line 2365347
    iget-object v2, v1, LX/GXS;->a:Ljava/util/List;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 2365348
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2365349
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f08299e

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 2365350
    :cond_a
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2365351
    :cond_b
    iget-object v5, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->I:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v5}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2365352
    iget-object v6, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->K:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v6}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2365353
    iget-object v7, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->R:LX/GXS;

    invoke-virtual {v7, v0}, LX/GXS;->a(Z)LX/0Px;

    move-result-object v7

    .line 2365354
    iget-object v8, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->M:Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;

    invoke-virtual {v8}, Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;->isChecked()Z

    move-result v8

    .line 2365355
    iget-object v9, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->V:LX/GYQ;

    .line 2365356
    iget-boolean v10, v9, LX/GYQ;->c:Z

    if-eqz v10, :cond_14

    .line 2365357
    iget-object v10, v9, LX/GYQ;->a:Landroid/widget/CompoundButton;

    invoke-virtual {v10}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v10

    invoke-static {v10}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v10

    .line 2365358
    :goto_7
    move-object v9, v10

    .line 2365359
    iget-object v10, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->R:LX/GXS;

    invoke-virtual {v10}, LX/GXS;->b()LX/0Px;

    move-result-object v10

    iput-object v10, v2, LX/GYh;->b:LX/0Px;

    .line 2365360
    invoke-static {p0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 2365361
    iget-object v10, v2, LX/GYh;->b:LX/0Px;

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v10

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v11

    if-ne v10, v11, :cond_c

    :goto_8
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2365362
    iput-object v5, v3, LX/7jY;->d:Ljava/lang/String;

    .line 2365363
    move-object v0, v3

    .line 2365364
    iput-object v6, v0, LX/7jY;->e:Ljava/lang/String;

    .line 2365365
    move-object v0, v0

    .line 2365366
    invoke-virtual {v4}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2365367
    iput-object v1, v0, LX/7jY;->g:Ljava/lang/Integer;

    .line 2365368
    move-object v0, v0

    .line 2365369
    iput-object v7, v0, LX/7jY;->i:LX/0Px;

    .line 2365370
    move-object v0, v0

    .line 2365371
    invoke-static {v8}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    .line 2365372
    iput-object v1, v0, LX/7jY;->j:LX/03R;

    .line 2365373
    move-object v0, v0

    .line 2365374
    iput-object v9, v0, LX/7jY;->k:LX/03R;

    .line 2365375
    move-object v0, v0

    .line 2365376
    invoke-virtual {v0}, LX/7jY;->a()Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    move-result-object v0

    iput-object v0, v2, LX/GYh;->a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    move-object v0, v2

    .line 2365377
    goto/16 :goto_2

    :cond_c
    move v0, v1

    .line 2365378
    goto :goto_8

    .line 2365379
    :cond_d
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-interface {v0}, LX/7ja;->ev_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5}, LX/GZ3;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 2365380
    iput-object v5, v3, LX/7jY;->d:Ljava/lang/String;

    .line 2365381
    :cond_e
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-interface {v0}, LX/7ja;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6}, LX/GZ3;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 2365382
    iput-object v6, v3, LX/7jY;->e:Ljava/lang/String;

    .line 2365383
    :cond_f
    invoke-static {p0, v7}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->a(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 2365384
    iput-object v7, v3, LX/7jY;->i:LX/0Px;

    .line 2365385
    :cond_10
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-interface {v0}, LX/7ja;->b()Z

    move-result v0

    if-eq v0, v8, :cond_11

    .line 2365386
    invoke-static {v8}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    .line 2365387
    iput-object v0, v3, LX/7jY;->j:LX/03R;

    .line 2365388
    :cond_11
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-interface {v0}, LX/7ja;->k()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-interface {v0}, LX/7ja;->k()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;->a()I

    move-result v0

    invoke-virtual {v4}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;->a()I

    move-result v1

    if-eq v0, v1, :cond_13

    .line 2365389
    :cond_12
    invoke-virtual {v4}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2365390
    iput-object v0, v3, LX/7jY;->g:Ljava/lang/Integer;

    .line 2365391
    :cond_13
    invoke-virtual {v3}, LX/7jY;->a()Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    move-result-object v0

    iput-object v0, v2, LX/GYh;->a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    move-object v0, v2

    .line 2365392
    goto/16 :goto_2

    :cond_14
    sget-object v10, LX/03R;->UNSET:LX/03R;

    goto/16 :goto_7

    .line 2365393
    :cond_15
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v10

    .line 2365394
    invoke-static {p0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)Z

    move-result v3

    if-eqz v3, :cond_18

    move-object v9, v10

    .line 2365395
    :goto_9
    iget-boolean v3, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->A:Z

    if-eqz v3, :cond_16

    .line 2365396
    iput-object v9, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->X:Ljava/lang/String;

    .line 2365397
    :cond_16
    invoke-static {p0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)Z

    move-result v3

    if-nez v3, :cond_19

    iget-object v3, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-interface {v3}, LX/7ja;->e()Ljava/lang/String;

    move-result-object v3

    :goto_a
    iget-object v4, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->I:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v4}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->K:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v5}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->M:Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;

    invoke-virtual {v6}, Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;->isChecked()Z

    move-result v6

    invoke-static {p0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->E(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v7

    iget-object v8, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->R:LX/GXS;

    invoke-virtual {v8, v13}, LX/GXS;->a(I)Ljava/lang/String;

    move-result-object v8

    invoke-static/range {v3 .. v8}, LX/7jZ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;Ljava/lang/String;)LX/7ja;

    move-result-object v4

    .line 2365398
    iget-object v3, v0, LX/GYh;->a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    invoke-static {v3}, LX/7jY;->a(Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;)LX/7jY;

    move-result-object v3

    iget-object v5, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    if-eqz v5, :cond_17

    iget-object v5, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-interface {v5}, LX/7ja;->e()Ljava/lang/String;

    move-result-object v11

    .line 2365399
    :cond_17
    iput-object v11, v3, LX/7jY;->a:Ljava/lang/String;

    .line 2365400
    move-object v3, v3

    .line 2365401
    iget-object v5, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2365402
    iget-object v6, v5, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2365403
    iput-object v5, v3, LX/7jY;->b:Ljava/lang/String;

    .line 2365404
    move-object v3, v3

    .line 2365405
    iget-wide v5, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->v:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    .line 2365406
    iput-object v5, v3, LX/7jY;->c:Ljava/lang/String;

    .line 2365407
    move-object v3, v3

    .line 2365408
    iget-object v5, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->w:Ljava/util/Currency;

    invoke-virtual {v5}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v5

    .line 2365409
    iput-object v5, v3, LX/7jY;->f:Ljava/lang/String;

    .line 2365410
    move-object v3, v3

    .line 2365411
    invoke-virtual {v3, v4, v9}, LX/7jY;->a(LX/7ja;Ljava/lang/String;)LX/7jY;

    move-result-object v3

    iget-object v5, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2365412
    iput-object v5, v3, LX/7jY;->n:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2365413
    move-object v3, v3

    .line 2365414
    invoke-virtual {v3}, LX/7jY;->a()Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    move-result-object v3

    iput-object v3, v0, LX/GYh;->a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    .line 2365415
    iget-object v3, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->f:LX/7ka;

    invoke-virtual {v3, v4, v9}, LX/7ka;->a(LX/7ja;Ljava/lang/String;)V

    .line 2365416
    invoke-virtual {v0}, LX/GYh;->b()Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 2365417
    iget-object v3, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->b:LX/1EZ;

    iget-object v5, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->c:LX/8LV;

    iget-object v6, v0, LX/GYh;->a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    iget-object v7, v0, LX/GYh;->b:LX/0Px;

    iget-object v8, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v5, v6, v7, v10, v8}, LX/8LV;->a(Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;LX/0Px;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    :goto_b
    move-object v11, v4

    .line 2365418
    goto/16 :goto_5

    .line 2365419
    :cond_18
    iget-object v3, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-interface {v3}, LX/7ja;->e()Ljava/lang/String;

    move-result-object v3

    move-object v9, v3

    goto/16 :goto_9

    :cond_19
    move-object v3, v11

    .line 2365420
    goto/16 :goto_a

    .line 2365421
    :cond_1a
    invoke-static {p0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)Z

    move-result v3

    if-nez v3, :cond_1d

    move v3, v12

    :goto_c
    invoke-static {v3}, LX/0PB;->checkState(Z)V

    .line 2365422
    iget-object v3, v0, LX/GYh;->b:LX/0Px;

    if-eqz v3, :cond_1b

    iget-object v3, v0, LX/GYh;->b:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1c

    :cond_1b
    move v13, v12

    :cond_1c
    invoke-static {v13}, LX/0PB;->checkArgument(Z)V

    .line 2365423
    iget-object v3, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->d:LX/7kY;

    iget-object v5, v0, LX/GYh;->a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    invoke-virtual {v3, v5}, LX/7kY;->a(Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;)V

    goto :goto_b

    :cond_1d
    move v3, v13

    .line 2365424
    goto :goto_c

    :cond_1e
    const/4 v3, 0x0

    goto/16 :goto_4

    :cond_1f
    const/4 v4, 0x0

    goto/16 :goto_3
.end method


# virtual methods
.method public final S_()Z
    .locals 7

    .prologue
    .line 2365425
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2365426
    iget-object v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C:LX/GYi;

    if-nez v2, :cond_2

    .line 2365427
    :cond_0
    :goto_0
    move v0, v0

    .line 2365428
    if-nez v0, :cond_1

    .line 2365429
    const/4 v0, 0x0

    .line 2365430
    :goto_1
    return v0

    .line 2365431
    :cond_1
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0829c2

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f0829c1

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f0829a5

    new-instance v2, LX/GYT;

    invoke-direct {v2, p0}, LX/GYT;-><init>(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f0829a6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2365432
    const/4 v0, 0x1

    goto :goto_1

    .line 2365433
    :cond_2
    iget-object v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->I:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2365434
    iget-object v3, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->J:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2365435
    iget-object v4, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->K:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v4}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2365436
    iget-object v5, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C:LX/GYi;

    iget-boolean v5, v5, LX/GYi;->d:Z

    iget-object v6, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->M:Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;

    invoke-virtual {v6}, Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;->isChecked()Z

    move-result v6

    if-ne v5, v6, :cond_3

    iget-object v5, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C:LX/GYi;

    iget-object v5, v5, LX/GYi;->a:Ljava/lang/String;

    invoke-static {v5, v2}, LX/GZ3;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C:LX/GYi;

    iget-object v2, v2, LX/GYi;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/GZ3;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C:LX/GYi;

    iget-object v2, v2, LX/GYi;->c:Ljava/lang/String;

    invoke-static {v2, v4}, LX/GZ3;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->R:LX/GXS;

    invoke-virtual {v2, v1}, LX/GXS;->a(Z)LX/0Px;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->a(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;LX/0Px;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto/16 :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2365437
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2365438
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2365439
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2365440
    const/4 v3, 0x0

    .line 2365441
    const-string v2, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2365442
    const-string v2, "extra_wait_for_mutation_finish"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->A:Z

    .line 2365443
    const-string v2, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->v:J

    .line 2365444
    iget-wide v4, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->v:J

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 2365445
    const-string v2, "extra_requires_initial_fetch"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->u:Z

    .line 2365446
    const-string v2, "extra_product_item_id_to_fetch"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->t:LX/0am;

    .line 2365447
    iget-boolean v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->u:Z

    if-eqz v2, :cond_4

    .line 2365448
    :goto_1
    iget-boolean v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->u:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    sget-object v0, LX/GXq;->ACTN_ADD_PRODUCT_CLICK:LX/GXq;

    .line 2365449
    :goto_2
    invoke-direct {p0, v0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->a(LX/GXq;)V

    .line 2365450
    iget-boolean v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->A:Z

    if-eqz v0, :cond_1

    .line 2365451
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->T:LX/1B1;

    new-instance v1, LX/GYS;

    invoke-direct {v1, p0}, LX/GYS;-><init>(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2365452
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->T:LX/1B1;

    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->m:LX/7jB;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 2365453
    :cond_1
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->p:LX/Bc5;

    iget-boolean v1, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->D:Z

    .line 2365454
    new-instance v2, LX/Bc4;

    const/16 v3, 0x18e1

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-direct {v2, v1, v3}, LX/Bc4;-><init>(ZLX/0Ot;)V

    .line 2365455
    move-object v0, v2

    .line 2365456
    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->U:LX/Bc4;

    .line 2365457
    invoke-static {p0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)Z

    move-result v0

    .line 2365458
    new-instance v1, LX/GYQ;

    invoke-direct {v1, v0}, LX/GYQ;-><init>(Z)V

    .line 2365459
    move-object v0, v1

    .line 2365460
    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->V:LX/GYQ;

    .line 2365461
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->r:LX/GYp;

    .line 2365462
    new-instance v2, LX/GYo;

    .line 2365463
    new-instance v3, LX/GYq;

    invoke-direct {v3}, LX/GYq;-><init>()V

    .line 2365464
    move-object v3, v3

    .line 2365465
    move-object v3, v3

    .line 2365466
    check-cast v3, LX/GYq;

    const-class v4, LX/GYx;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/GYx;

    const-class v4, LX/GZ0;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/GZ0;

    const-class v4, LX/GYu;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/GYu;

    const/16 v4, 0x12b1

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v4, 0x259

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    move-object v4, p0

    invoke-direct/range {v2 .. v9}, LX/GYo;-><init>(LX/GYq;Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;LX/GYx;LX/GZ0;LX/GYu;LX/0Ot;LX/0Ot;)V

    .line 2365467
    move-object v0, v2

    .line 2365468
    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->W:LX/GYo;

    .line 2365469
    return-void

    .line 2365470
    :cond_2
    sget-object v0, LX/GXq;->ACTN_EDIT_PRODUCT_CLICK:LX/GXq;

    goto :goto_2

    :cond_3
    move v2, v3

    .line 2365471
    goto/16 :goto_0

    .line 2365472
    :cond_4
    const-string v2, "extra_currency"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Ljava/util/Currency;

    iput-object v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->w:Ljava/util/Currency;

    .line 2365473
    const-string v2, "extra_admin_product_item"

    invoke-static {v0, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7ja;

    iput-object v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    .line 2365474
    const-string v2, "extra_featured_products_count"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->z:I

    .line 2365475
    const-string v2, "extra_has_empty_catalog"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->D:Z

    .line 2365476
    iget-object v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2365477
    iget-object v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2365478
    iget-boolean v3, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v2, v3

    .line 2365479
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 2365480
    iget-object v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->w:Ljava/util/Currency;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p3    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2365481
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2365482
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->o:LX/GZ4;

    invoke-virtual {v0, p1, p2, p3}, LX/GZ4;->a(IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2365483
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->Q:Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;

    if-eqz v0, :cond_0

    .line 2365484
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->Q:Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2365485
    :cond_0
    :goto_0
    return-void

    .line 2365486
    :cond_1
    const/16 v0, 0x9cd

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    .line 2365487
    const-string v0, "extra_media_items"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2365488
    const-string v0, "extra_media_items"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2365489
    if-eqz v0, :cond_0

    .line 2365490
    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->R:LX/GXS;

    .line 2365491
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2365492
    iget-object v2, v1, LX/GXS;->a:Ljava/util/List;

    .line 2365493
    if-nez v0, :cond_4

    .line 2365494
    const/4 p0, 0x0

    .line 2365495
    :goto_1
    move-object p0, p0

    .line 2365496
    invoke-interface {v2, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2365497
    :cond_2
    iget-object v2, v1, LX/GXS;->b:LX/1OM;

    if-eqz v2, :cond_3

    .line 2365498
    iget-object v2, v1, LX/GXS;->b:LX/1OM;

    invoke-virtual {v2}, LX/1OM;->notifyDataSetChanged()V

    .line 2365499
    :cond_3
    goto :goto_0

    .line 2365500
    :cond_4
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 2365501
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/ipc/media/MediaItem;

    .line 2365502
    new-instance p3, LX/GXR;

    invoke-direct {p3, p0}, LX/GXR;-><init>(Ljava/lang/Object;)V

    invoke-interface {p1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    move-object p0, p1

    .line 2365503
    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x2254f24d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2365504
    const v1, 0x7f0300b2

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x6f0c1577

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x5e2da267

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2365505
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2365506
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->W:LX/GYo;

    invoke-virtual {v0}, LX/GYo;->a()V

    .line 2365507
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->T:LX/1B1;

    iget-object v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->m:LX/7jB;

    invoke-virtual {v0, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2365508
    iget-boolean v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->B:Z

    if-nez v0, :cond_0

    .line 2365509
    invoke-static {p0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/GXq;->ACTN_CANCEL_ADD_PRODUCT:LX/GXq;

    .line 2365510
    :goto_0
    invoke-direct {p0, v0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->a(LX/GXq;)V

    .line 2365511
    :cond_0
    const v0, 0x5f432f80

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2365512
    :cond_1
    sget-object v0, LX/GXq;->ACTN_CANCEL_EDIT_PRODUCT:LX/GXq;

    goto :goto_0
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x33166537

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2365513
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2365514
    iget-boolean v1, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->u:Z

    if-nez v1, :cond_0

    .line 2365515
    invoke-static {p0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->l(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V

    .line 2365516
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x3ba5a784

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3ea30db1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2365548
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2365549
    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->g:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2365550
    const/16 v1, 0x2b

    const v2, -0x1615e6ca

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2365517
    const v0, 0x7f0d04d1

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->E:Landroid/view/ViewGroup;

    .line 2365518
    const v0, 0x7f0d04de

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->F:Landroid/widget/ProgressBar;

    .line 2365519
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->W:LX/GYo;

    iget-boolean v1, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->u:Z

    iget-wide v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->v:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->t:LX/0am;

    iget-object v4, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v4}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    .line 2365520
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2365521
    new-instance p0, Ljava/util/LinkedList;

    invoke-direct {p0}, Ljava/util/LinkedList;-><init>()V

    .line 2365522
    if-eqz v1, :cond_0

    .line 2365523
    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2365524
    iget-object p1, v0, LX/GYo;->e:LX/GZ0;

    iget-object v5, v0, LX/GYo;->g:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iget-object p2, v0, LX/GYo;->j:LX/GYk;

    .line 2365525
    new-instance v3, LX/GYz;

    invoke-static {p1}, LX/GY2;->a(LX/0QB;)LX/GY2;

    move-result-object v1

    check-cast v1, LX/GY2;

    invoke-direct {v3, v1, v5, v6, p2}, LX/GYz;-><init>(LX/GY2;LX/1Ck;Ljava/lang/String;LX/GYk;)V

    .line 2365526
    move-object v5, v3

    .line 2365527
    invoke-interface {p0, v5}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2365528
    :cond_0
    :goto_0
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v5

    if-nez v5, :cond_1

    .line 2365529
    iget-object v5, v0, LX/GYo;->f:LX/GYu;

    iget-object v6, v0, LX/GYo;->k:LX/GYk;

    .line 2365530
    new-instance p2, LX/GYt;

    invoke-static {v5}, LX/8Fe;->a(LX/0QB;)LX/8Fe;

    move-result-object p1

    check-cast p1, LX/8Fe;

    invoke-direct {p2, p1, v2, v6}, LX/GYt;-><init>(LX/8Fe;Ljava/lang/String;LX/GYk;)V

    .line 2365531
    move-object v5, p2

    .line 2365532
    invoke-interface {p0, v5}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2365533
    :cond_1
    move-object v5, p0

    .line 2365534
    iget-object v6, v0, LX/GYo;->b:LX/GYq;

    .line 2365535
    iget-boolean p0, v6, LX/GYq;->b:Z

    if-eqz p0, :cond_4

    .line 2365536
    const/4 p0, 0x0

    .line 2365537
    :goto_1
    move v5, p0

    .line 2365538
    if-nez v5, :cond_2

    .line 2365539
    invoke-static {v0}, LX/GYo;->b(LX/GYo;)V

    .line 2365540
    :cond_2
    return-void

    .line 2365541
    :cond_3
    iget-object v6, v0, LX/GYo;->d:LX/GYx;

    iget-object v5, v0, LX/GYo;->g:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1Ck;

    iget-object p1, v0, LX/GYo;->i:LX/GYk;

    .line 2365542
    new-instance v1, LX/GYw;

    invoke-static {v6}, LX/GY2;->a(LX/0QB;)LX/GY2;

    move-result-object p2

    check-cast p2, LX/GY2;

    invoke-direct {v1, p2, v5, v2, p1}, LX/GYw;-><init>(LX/GY2;LX/1Ck;Ljava/lang/String;LX/GYk;)V

    .line 2365543
    move-object v5, v1

    .line 2365544
    invoke-interface {p0, v5}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2365545
    :cond_4
    const/4 p0, 0x1

    iput-boolean p0, v6, LX/GYq;->b:Z

    .line 2365546
    iput-object v5, v6, LX/GYq;->a:Ljava/util/Queue;

    .line 2365547
    invoke-virtual {v6}, LX/GYq;->a()Z

    move-result p0

    goto :goto_1
.end method
