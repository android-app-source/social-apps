.class public final Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment$10$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/7jK;

.field public final synthetic b:LX/GYS;


# direct methods
.method public constructor <init>(LX/GYS;LX/7jK;)V
    .locals 0

    .prologue
    .line 2365190
    iput-object p1, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment$10$1;->b:LX/GYS;

    iput-object p2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment$10$1;->a:LX/7jK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2365191
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment$10$1;->b:LX/GYS;

    iget-object v0, v0, LX/GYS;->a:Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;

    iget-object v0, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->S:LX/4At;

    if-eqz v0, :cond_0

    .line 2365192
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment$10$1;->b:LX/GYS;

    iget-object v0, v0, LX/GYS;->a:Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;

    iget-object v0, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->S:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2365193
    :cond_0
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment$10$1;->a:LX/7jK;

    .line 2365194
    iget-object v1, v0, LX/7jF;->a:LX/7jE;

    move-object v0, v1

    .line 2365195
    sget-object v1, LX/7jE;->SUCCESS:LX/7jE;

    if-ne v0, v1, :cond_2

    .line 2365196
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment$10$1;->b:LX/GYS;

    iget-object v0, v0, LX/GYS;->a:Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;

    iget-object v1, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment$10$1;->a:LX/7jK;

    .line 2365197
    iget-object v2, v1, LX/7jF;->a:LX/7jE;

    move-object v2, v2

    .line 2365198
    sget-object v3, LX/7jE;->SUCCESS:LX/7jE;

    if-ne v2, v3, :cond_3

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 2365199
    iget-object v2, v1, LX/7jK;->a:Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    move-object v1, v2

    .line 2365200
    iget-object v2, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment$10$1;->a:LX/7jK;

    .line 2365201
    iget-object v3, v2, LX/7jF;->b:LX/7jD;

    move-object v2, v3

    .line 2365202
    invoke-static {v0, v1, v2}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->a$redex0(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;LX/7ja;LX/7jD;)V

    .line 2365203
    :cond_1
    :goto_1
    return-void

    .line 2365204
    :cond_2
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment$10$1;->a:LX/7jK;

    .line 2365205
    iget-object v1, v0, LX/7jF;->a:LX/7jE;

    move-object v0, v1

    .line 2365206
    sget-object v1, LX/7jE;->FAILED:LX/7jE;

    if-ne v0, v1, :cond_1

    .line 2365207
    iget-object v0, p0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment$10$1;->b:LX/GYS;

    iget-object v0, v0, LX/GYS;->a:Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;

    iget-object v0, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->l:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0814c5

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_1

    .line 2365208
    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method
