.class public Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;
.super Lcom/facebook/widget/text/BetterEditTextView;
.source ""


# instance fields
.field private final b:Landroid/graphics/drawable/Drawable;

.field private final c:Landroid/content/res/ColorStateList;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2364443
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2364444
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2364420
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2364421
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2364430
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/text/BetterEditTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2364431
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;->d:Z

    .line 2364432
    invoke-direct {p0}, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;->getDropdownIconAndTextColorStateList()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;->c:Landroid/content/res/ColorStateList;

    .line 2364433
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;->c:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v0}, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 2364434
    invoke-direct {p0}, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;->getDropdownIconWithIntrinsicBounds()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;->b:Landroid/graphics/drawable/Drawable;

    .line 2364435
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {p0, v1, v1, v0, v1}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2364436
    invoke-direct {p0}, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;->b()V

    .line 2364437
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;->d:Z

    .line 2364438
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 2364439
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;->c:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    .line 2364440
    if-eq v0, v2, :cond_0

    .line 2364441
    iget-object v1, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;->b:Landroid/graphics/drawable/Drawable;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v0, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2364442
    :cond_0
    return-void
.end method

.method private getDropdownIconAndTextColorStateList()Landroid/content/res/ColorStateList;
    .locals 2

    .prologue
    .line 2364429
    invoke-virtual {p0}, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a09d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method private getDropdownIconWithIntrinsicBounds()Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2364426
    invoke-virtual {p0}, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0219ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 2364427
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2364428
    return-object v0
.end method


# virtual methods
.method public final drawableStateChanged()V
    .locals 1

    .prologue
    .line 2364422
    invoke-super {p0}, Lcom/facebook/widget/text/BetterEditTextView;->drawableStateChanged()V

    .line 2364423
    iget-boolean v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;->d:Z

    if-nez v0, :cond_0

    .line 2364424
    :goto_0
    return-void

    .line 2364425
    :cond_0
    invoke-direct {p0}, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;->b()V

    goto :goto_0
.end method
