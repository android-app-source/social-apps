.class public Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/GZ3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/fbui/glyph/GlyphView;

.field private c:Lcom/facebook/widget/text/BetterEditTextView;

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field public e:LX/GXT;

.field private final f:Landroid/view/View$OnClickListener;

.field private final g:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2364482
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2364483
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2364480
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2364481
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2364455
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2364456
    new-instance v0, LX/GXh;

    invoke-direct {v0, p0}, LX/GXh;-><init>(Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;)V

    iput-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->f:Landroid/view/View$OnClickListener;

    .line 2364457
    new-instance v0, LX/GXi;

    invoke-direct {v0, p0}, LX/GXi;-><init>(Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;)V

    iput-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->g:Landroid/text/TextWatcher;

    .line 2364458
    const-class v0, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;

    invoke-static {v0, p0}, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2364459
    const v0, 0x7f0300bf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2364460
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->setOrientation(I)V

    .line 2364461
    const v0, 0x7f0d04fa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2364462
    const v0, 0x7f0d04fb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->c:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2364463
    const v0, 0x7f0d04fc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2364464
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->c:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v1, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->g:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2364465
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2364466
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;

    new-instance p1, LX/GZ3;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-direct {p1, v1, v2}, LX/GZ3;-><init>(LX/0wM;Landroid/content/res/Resources;)V

    move-object v0, p1

    check-cast v0, LX/GZ3;

    iput-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->a:LX/GZ3;

    return-void
.end method

.method public static c(Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;I)V
    .locals 5

    .prologue
    .line 2364474
    invoke-virtual {p0}, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0063

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 2364475
    const-string v1, "%d/%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2364476
    iget-object v1, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2364477
    iget-object v1, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->d:Lcom/facebook/widget/text/BetterTextView;

    if-lez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2364478
    return-void

    .line 2364479
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2364471
    invoke-virtual {p0}, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0829af

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2364472
    iget-object v1, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->c:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterEditTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 2364473
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2364469
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->c:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2364470
    return-void
.end method

.method public setIntroSummaryTextChangedListener(LX/GXT;)V
    .locals 0

    .prologue
    .line 2364467
    iput-object p1, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->e:LX/GXT;

    .line 2364468
    return-void
.end method
