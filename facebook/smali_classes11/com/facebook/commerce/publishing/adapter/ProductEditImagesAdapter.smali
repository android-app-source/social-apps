.class public Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:I

.field public c:Landroid/view/LayoutInflater;

.field public d:LX/GXl;

.field public e:Landroid/view/View$OnClickListener;

.field public f:LX/GXS;

.field public g:LX/GYY;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2364520
    const-class v0, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;

    const-string v1, "commerce_product_edit_images_adapter"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/GXS;Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/GXS;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/view/View$OnClickListener;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2364521
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2364522
    const/16 v0, 0xa

    iput v0, p0, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->b:I

    .line 2364523
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->c:Landroid/view/LayoutInflater;

    .line 2364524
    iput-object p2, p0, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->f:LX/GXS;

    .line 2364525
    iput-object p3, p0, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->e:Landroid/view/View$OnClickListener;

    .line 2364526
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 2364527
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 2364528
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->d:LX/GXl;

    if-eqz v0, :cond_1

    .line 2364529
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->d:LX/GXl;

    .line 2364530
    :goto_0
    move-object v0, v0

    .line 2364531
    :goto_1
    return-object v0

    .line 2364532
    :cond_0
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f03103a

    const/4 p2, 0x0

    invoke-virtual {v0, v1, p1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2364533
    new-instance v1, LX/GZ2;

    sget-object p2, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, v0, p2}, LX/GZ2;-><init>(Landroid/view/View;Lcom/facebook/common/callercontext/CallerContext;)V

    move-object v0, v1

    .line 2364534
    goto :goto_1

    .line 2364535
    :cond_1
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f031031

    const/4 p2, 0x0

    invoke-virtual {v0, v1, p1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2364536
    iget-object v1, p0, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2364537
    new-instance v1, LX/GXl;

    invoke-direct {v1, v0}, LX/GXl;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->d:LX/GXl;

    .line 2364538
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->d:LX/GXl;

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2364539
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2364540
    :goto_0
    return-void

    .line 2364541
    :cond_0
    check-cast p1, LX/GZ2;

    .line 2364542
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->f:LX/GXS;

    .line 2364543
    iget-object v1, v0, LX/GXS;->a:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/GXR;

    move-object v0, v1

    .line 2364544
    iget-object v1, p0, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->f:LX/GXS;

    invoke-virtual {v1, p2}, LX/GXS;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 2364545
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2364546
    iget-object v2, p1, LX/GZ2;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object p2, p1, LX/GZ2;->o:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2364547
    new-instance v1, LX/GXj;

    invoke-direct {v1, p0, p1}, LX/GXj;-><init>(Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;LX/GZ2;)V

    .line 2364548
    iget-object v2, p1, LX/GZ2;->l:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2364549
    iget-object v1, v0, LX/GXR;->b:LX/GXQ;

    move-object v1, v1

    .line 2364550
    sget-object v2, LX/GXQ;->MEDIA_ITEM:LX/GXQ;

    if-ne v1, v2, :cond_1

    .line 2364551
    new-instance v1, LX/GXk;

    invoke-direct {v1, p0, p1, v0}, LX/GXk;-><init>(Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;LX/GZ2;LX/GXR;)V

    invoke-virtual {p1, v1}, LX/GZ2;->b(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2364552
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/GZ2;->b(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2364553
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->f:LX/GXS;

    invoke-virtual {v0}, LX/GXS;->a()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2364554
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->f:LX/GXS;

    invoke-virtual {v0}, LX/GXS;->a()I

    move-result v0

    iget v1, p0, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->b:I

    if-ge v0, v1, :cond_0

    .line 2364555
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->f:LX/GXS;

    invoke-virtual {v0}, LX/GXS;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 2364556
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->f:LX/GXS;

    invoke-virtual {v0}, LX/GXS;->a()I

    move-result v0

    goto :goto_0
.end method
