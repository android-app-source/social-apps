.class public Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "Lcom/facebook/commerce/publishing/adapter/SectionedAdapterHelper$Host;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:Landroid/view/LayoutInflater;

.field private final d:Lcom/facebook/commerce/core/ui/ProductItemViewBinder;

.field private final e:LX/GZ1;

.field public final f:LX/GXp;

.field public final g:LX/3wu;

.field public final h:LX/GXY;

.field private final i:LX/GXW;

.field public final j:LX/GXX;

.field public k:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

.field private final l:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2364204
    const-class v0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/facebook/commerce/core/ui/ProductItemViewBinder;LX/GZ1;LX/GXY;)V
    .locals 3
    .param p5    # LX/GXY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2364205
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2364206
    new-instance v0, LX/GXV;

    invoke-direct {v0, p0}, LX/GXV;-><init>(Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;)V

    iput-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->l:Landroid/view/View$OnClickListener;

    .line 2364207
    iput-object p1, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->b:Landroid/content/Context;

    .line 2364208
    iput-object p2, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->c:Landroid/view/LayoutInflater;

    .line 2364209
    iput-object p3, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->d:Lcom/facebook/commerce/core/ui/ProductItemViewBinder;

    .line 2364210
    iput-object p4, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->e:LX/GZ1;

    .line 2364211
    iput-object p5, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->h:LX/GXY;

    .line 2364212
    new-instance v0, LX/GXp;

    invoke-direct {v0, p0}, LX/GXp;-><init>(Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;)V

    iput-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->f:LX/GXp;

    .line 2364213
    new-instance v0, LX/3wu;

    const/4 v1, 0x2

    invoke-direct {v0, p1, v1}, LX/3wu;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->g:LX/3wu;

    .line 2364214
    new-instance v0, LX/GXW;

    iget-object v1, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/GXW;-><init>(Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->i:LX/GXW;

    .line 2364215
    new-instance v0, LX/GXX;

    invoke-direct {v0, p0}, LX/GXX;-><init>(Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;)V

    iput-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->j:LX/GXX;

    .line 2364216
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->f:LX/GXp;

    .line 2364217
    new-instance v1, LX/GXo;

    invoke-direct {v1, v0}, LX/GXo;-><init>(LX/GXp;)V

    move-object v0, v1

    .line 2364218
    invoke-virtual {p0, v0}, LX/1OM;->a(LX/1OD;)V

    .line 2364219
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->h:LX/GXY;

    .line 2364220
    iput-object p0, v0, LX/GXY;->d:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    .line 2364221
    iget-object v1, v0, LX/GXY;->d:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    if-eqz v1, :cond_0

    .line 2364222
    iget-object v1, v0, LX/GXY;->d:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 2364223
    :cond_0
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->j:LX/GXX;

    const/4 v1, 0x1

    .line 2364224
    iput-boolean v1, v0, LX/3wr;->b:Z

    .line 2364225
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->g:LX/3wu;

    iget-object v1, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->j:LX/GXX;

    .line 2364226
    iput-object v1, v0, LX/3wu;->h:LX/3wr;

    .line 2364227
    return-void
.end method

.method public static f(Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;)Z
    .locals 1

    .prologue
    .line 2364228
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->h:LX/GXY;

    invoke-virtual {v0}, LX/GXY;->g()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2364229
    const/4 v0, 0x0

    .line 2364230
    iget-object v1, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->c:Landroid/view/LayoutInflater;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2364231
    sget v2, LX/GXg;->a:I

    if-ne p2, v2, :cond_1

    .line 2364232
    new-instance v0, LX/GXd;

    invoke-direct {v0, v1}, LX/GXd;-><init>(Landroid/view/View;)V

    .line 2364233
    :cond_0
    :goto_0
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2364234
    iget-object v1, v0, LX/1a1;->a:Landroid/view/View;

    iget-object v2, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->l:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2364235
    return-object v0

    .line 2364236
    :cond_1
    sget v2, LX/GXg;->d:I

    if-ne p2, v2, :cond_2

    .line 2364237
    new-instance v0, LX/GXf;

    iget-object v2, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->k:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    invoke-direct {v0, v1, v2}, LX/GXf;-><init>(Landroid/view/View;Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;)V

    goto :goto_0

    .line 2364238
    :cond_2
    sget v2, LX/GXg;->e:I

    if-ne p2, v2, :cond_3

    .line 2364239
    new-instance v0, LX/GXc;

    invoke-direct {v0, v1}, LX/GXc;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2364240
    :cond_3
    sget v2, LX/GXg;->b:I

    if-ne p2, v2, :cond_4

    .line 2364241
    new-instance v0, LX/GXa;

    invoke-direct {v0, v1}, LX/GXa;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2364242
    :cond_4
    sget v2, LX/GXg;->c:I

    if-ne p2, v2, :cond_5

    .line 2364243
    new-instance v0, LX/GXb;

    iget-object v2, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/GXb;-><init>(Landroid/view/View;I)V

    goto :goto_0

    .line 2364244
    :cond_5
    sget v2, LX/GXg;->f:I

    if-ne p2, v2, :cond_0

    .line 2364245
    new-instance v0, LX/7iz;

    invoke-direct {v0, v1}, LX/7iz;-><init>(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 2364246
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->f:LX/GXp;

    invoke-virtual {v0, p2}, LX/GXp;->a(I)I

    move-result v0

    .line 2364247
    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 2364248
    check-cast p1, LX/GXf;

    .line 2364249
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->h:LX/GXY;

    invoke-virtual {v0}, LX/GXY;->b()Ljava/util/Currency;

    move-result-object v0

    .line 2364250
    if-eqz v0, :cond_0

    .line 2364251
    iget-object v1, p1, LX/GXf;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2364252
    :cond_0
    :goto_0
    return-void

    .line 2364253
    :cond_1
    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 2364254
    check-cast p1, LX/GXc;

    .line 2364255
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0829b2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2364256
    iget-object v1, p1, LX/GXc;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2364257
    goto :goto_0

    .line 2364258
    :cond_2
    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 2364259
    check-cast p1, LX/GXc;

    .line 2364260
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0829b1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2364261
    iget-object v1, p1, LX/GXc;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2364262
    goto :goto_0

    .line 2364263
    :cond_3
    if-nez v0, :cond_4

    .line 2364264
    check-cast p1, LX/GXd;

    .line 2364265
    iget-object v0, p1, LX/GXd;->l:Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;

    iget-object v1, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->h:LX/GXY;

    .line 2364266
    iget-object p2, v1, LX/GXY;->a:Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    if-eqz p2, :cond_6

    iget-object p2, v1, LX/GXY;->a:Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    invoke-virtual {p2}, Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;->n()Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel$PageModel;

    move-result-object p2

    if-eqz p2, :cond_6

    iget-object p2, v1, LX/GXY;->a:Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    invoke-virtual {p2}, Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;->n()Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel$PageModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel$PageModel;->a()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_6

    .line 2364267
    iget-object p2, v1, LX/GXY;->a:Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    invoke-virtual {p2}, Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;->n()Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel$PageModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel$PageModel;->a()Ljava/lang/String;

    move-result-object p2

    .line 2364268
    :goto_1
    move-object v1, p2

    .line 2364269
    invoke-virtual {v0, v1}, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->a(Ljava/lang/String;)V

    .line 2364270
    iget-object v0, p1, LX/GXd;->l:Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;

    iget-object v1, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->h:LX/GXY;

    invoke-virtual {v1}, LX/GXY;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->b(Ljava/lang/String;)V

    .line 2364271
    iget-object v0, p1, LX/GXd;->l:Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;

    new-instance v1, LX/GXU;

    invoke-direct {v1, p0}, LX/GXU;-><init>(Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;)V

    .line 2364272
    iput-object v1, v0, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->e:LX/GXT;

    .line 2364273
    goto :goto_0

    .line 2364274
    :cond_4
    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 2364275
    check-cast p1, LX/7iz;

    .line 2364276
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->f:LX/GXp;

    invoke-virtual {v0, p2}, LX/GXp;->b(I)I

    move-result v0

    .line 2364277
    iget-object v1, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->h:LX/GXY;

    invoke-virtual {v1, v0}, LX/GXY;->a(I)Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ja;

    .line 2364278
    iget-object v1, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->h:LX/GXY;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2364279
    new-instance v5, LX/7j0;

    invoke-direct {v5}, LX/7j0;-><init>()V

    .line 2364280
    invoke-interface {v0}, LX/7ja;->ev_()Ljava/lang/String;

    move-result-object v2

    .line 2364281
    iput-object v2, v5, LX/7j0;->e:Ljava/lang/String;

    .line 2364282
    move-object v2, v5

    .line 2364283
    invoke-interface {v0}, LX/7ja;->k()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object p0

    invoke-static {p0}, LX/7j4;->a(Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;)Ljava/lang/String;

    move-result-object p0

    .line 2364284
    iput-object p0, v2, LX/7j0;->f:Ljava/lang/String;

    .line 2364285
    move-object p0, v2

    .line 2364286
    invoke-interface {v0}, LX/7ja;->c()Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    move-result-object v2

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;->PRODUCT_REJECTED:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    if-ne v2, p2, :cond_7

    move v2, v3

    .line 2364287
    :goto_2
    iput-boolean v2, p0, LX/7j0;->b:Z

    .line 2364288
    move-object v2, p0

    .line 2364289
    invoke-interface {v0}, LX/7ja;->c()Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    move-result-object p0

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;->PRODUCT_IN_REVIEW:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    if-ne p0, p2, :cond_8

    .line 2364290
    :goto_3
    iput-boolean v3, v2, LX/7j0;->c:Z

    .line 2364291
    move-object v2, v2

    .line 2364292
    invoke-interface {v0}, LX/7ja;->b()Z

    move-result v3

    .line 2364293
    iput-boolean v3, v2, LX/7j0;->d:Z

    .line 2364294
    move-object v2, v2

    .line 2364295
    invoke-virtual {v1, v0}, LX/GXY;->b(LX/7ja;)Z

    move-result v3

    .line 2364296
    iput-boolean v3, v2, LX/7j0;->a:Z

    .line 2364297
    const/4 v3, 0x0

    .line 2364298
    if-eqz v0, :cond_9

    invoke-interface {v0}, LX/7ja;->j()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-interface {v0}, LX/7ja;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    invoke-interface {v0}, LX/7ja;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$ProductImagesLargeModel;

    invoke-virtual {v2}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$ProductImagesLargeModel;->b()Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$ProductImagesLargeModel$ImageModel;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-interface {v0}, LX/7ja;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$ProductImagesLargeModel;

    invoke-virtual {v2}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$ProductImagesLargeModel;->b()Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$ProductImagesLargeModel$ImageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$ProductImagesLargeModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 2364299
    invoke-interface {v0}, LX/7ja;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$ProductImagesLargeModel;

    invoke-virtual {v2}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$ProductImagesLargeModel;->b()Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$ProductImagesLargeModel$ImageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$ProductImagesLargeModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2364300
    :goto_4
    move-object v2, v2

    .line 2364301
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 2364302
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    .line 2364303
    iput-object v2, v5, LX/7j0;->g:LX/0am;

    .line 2364304
    :cond_5
    invoke-virtual {v5}, LX/7j0;->a()LX/7j1;

    move-result-object v2

    move-object v0, v2

    .line 2364305
    invoke-static {p1, v0}, Lcom/facebook/commerce/core/ui/ProductItemViewBinder;->a(LX/7iz;LX/7j1;)V

    .line 2364306
    goto/16 :goto_0

    :cond_6
    const/4 p2, 0x0

    goto/16 :goto_1

    :cond_7
    move v2, v4

    .line 2364307
    goto/16 :goto_2

    :cond_8
    move v3, v4

    goto :goto_3

    :cond_9
    const/4 v2, 0x0

    goto :goto_4
.end method

.method public final a_(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 2364308
    invoke-super {p0, p1}, LX/1OM;->a_(Landroid/support/v7/widget/RecyclerView;)V

    .line 2364309
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->g:LX/3wu;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2364310
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->i:LX/GXW;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2364311
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2364312
    iget-object v1, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->f:LX/GXp;

    invoke-virtual {v1, p1}, LX/GXp;->a(I)I

    move-result v1

    .line 2364313
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 2364314
    sget v0, LX/GXg;->b:I

    .line 2364315
    :goto_0
    return v0

    .line 2364316
    :cond_0
    if-ne v1, v0, :cond_1

    .line 2364317
    sget v0, LX/GXg;->e:I

    goto :goto_0

    .line 2364318
    :cond_1
    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .line 2364319
    sget v0, LX/GXg;->e:I

    goto :goto_0

    .line 2364320
    :cond_2
    if-nez v1, :cond_3

    .line 2364321
    sget v0, LX/GXg;->a:I

    goto :goto_0

    .line 2364322
    :cond_3
    const/4 v2, 0x4

    if-ne v1, v2, :cond_4

    .line 2364323
    sget v0, LX/GXg;->f:I

    goto :goto_0

    .line 2364324
    :cond_4
    const/4 v2, 0x3

    if-ne v1, v2, :cond_5

    .line 2364325
    sget v0, LX/GXg;->c:I

    goto :goto_0

    .line 2364326
    :cond_5
    const/4 v2, 0x6

    if-ne v1, v2, :cond_6

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2364327
    sget v0, LX/GXg;->d:I

    goto :goto_0

    .line 2364328
    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2364329
    iget-object v0, p0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->f:LX/GXp;

    .line 2364330
    invoke-static {v0}, LX/GXp;->d(LX/GXp;)V

    .line 2364331
    iget-object p0, v0, LX/GXp;->b:Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result p0

    move v0, p0

    .line 2364332
    return v0
.end method
