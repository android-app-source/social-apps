.class public final Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2361350
    const-class v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    new-instance v1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2361351
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2361349
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 2361243
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2361244
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2361245
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2361246
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 2361247
    if-eqz v2, :cond_0

    .line 2361248
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361249
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2361250
    :cond_0
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 2361251
    if-eqz v2, :cond_1

    .line 2361252
    const-string v2, "commerce_checkout_style"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361253
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2361254
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2361255
    if-eqz v2, :cond_5

    .line 2361256
    const-string v3, "commerce_insights"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361257
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2361258
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2361259
    if-eqz v3, :cond_2

    .line 2361260
    const-string v4, "message_sends"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361261
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2361262
    :cond_2
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2361263
    if-eqz v3, :cond_3

    .line 2361264
    const-string v4, "organic_impressions"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361265
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2361266
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2361267
    if-eqz v3, :cond_4

    .line 2361268
    const-string v4, "paid_impressions"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361269
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2361270
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2361271
    :cond_5
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2361272
    if-eqz v2, :cond_a

    .line 2361273
    const-string v3, "commerce_merchant_settings"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361274
    const/4 p0, 0x1

    const/4 v3, 0x0

    .line 2361275
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2361276
    invoke-virtual {v1, v2, v3, v3}, LX/15i;->a(III)I

    move-result v3

    .line 2361277
    if-eqz v3, :cond_6

    .line 2361278
    const-string v4, "payment_provider"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361279
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 2361280
    :cond_6
    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result v3

    .line 2361281
    if-eqz v3, :cond_7

    .line 2361282
    const-string v3, "shipping_and_returns_policy"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361283
    invoke-virtual {v1, v2, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v3

    invoke-static {v3, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2361284
    :cond_7
    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, LX/15i;->b(II)Z

    move-result v3

    .line 2361285
    if-eqz v3, :cond_8

    .line 2361286
    const-string v4, "show_edit_interface"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361287
    invoke-virtual {p1, v3}, LX/0nX;->a(Z)V

    .line 2361288
    :cond_8
    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2361289
    if-eqz v3, :cond_9

    .line 2361290
    const-string v4, "terms_and_policies_uri"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361291
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2361292
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2361293
    :cond_a
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 2361294
    if-eqz v2, :cond_b

    .line 2361295
    const-string v2, "commerce_product_visibility"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361296
    invoke-virtual {v1, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2361297
    :cond_b
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2361298
    if-eqz v2, :cond_c

    .line 2361299
    const-string v3, "commerce_ui_detail_sections"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361300
    invoke-static {v1, v2, p1, p2}, LX/GWQ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2361301
    :cond_c
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2361302
    if-eqz v2, :cond_d

    .line 2361303
    const-string v3, "description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361304
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2361305
    :cond_d
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2361306
    if-eqz v2, :cond_e

    .line 2361307
    const-string v3, "external_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361308
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2361309
    :cond_e
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2361310
    if-eqz v2, :cond_f

    .line 2361311
    const-string v3, "group"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361312
    invoke-static {v1, v2, p1, p2}, LX/GWV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2361313
    :cond_f
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2361314
    if-eqz v2, :cond_10

    .line 2361315
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361316
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2361317
    :cond_10
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2361318
    if-eqz v2, :cond_11

    .line 2361319
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361320
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2361321
    :cond_11
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2361322
    if-eqz v2, :cond_12

    .line 2361323
    const-string v3, "page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361324
    invoke-static {v1, v2, p1, p2}, LX/GWX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2361325
    :cond_12
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2361326
    if-eqz v2, :cond_13

    .line 2361327
    const-string v3, "product_promotions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361328
    invoke-static {v1, v2, p1, p2}, LX/GWY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2361329
    :cond_13
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2361330
    if-eqz v2, :cond_16

    .line 2361331
    const-string v3, "recommended_product_items"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361332
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2361333
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2361334
    if-eqz v3, :cond_15

    .line 2361335
    const-string v4, "nodes"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361336
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2361337
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v2

    if-ge v4, v2, :cond_14

    .line 2361338
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v2

    invoke-static {v1, v2, p1, p2}, LX/GWZ;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2361339
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2361340
    :cond_14
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2361341
    :cond_15
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2361342
    :cond_16
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2361343
    if-eqz v2, :cond_17

    .line 2361344
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361345
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2361346
    :cond_17
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2361347
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2361348
    check-cast p1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$Serializer;->a(Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
