.class public final Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1dbd9a3f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2361758
    const-class v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2361728
    const-class v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2361729
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2361730
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2361731
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2361732
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2361733
    return-void
.end method

.method private a(Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;)V
    .locals 3
    .param p1    # Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2361734
    iput-object p1, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->m:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    .line 2361735
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2361736
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2361737
    if-eqz v0, :cond_0

    .line 2361738
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 2361739
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2361740
    iput-object p1, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->n:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    .line 2361741
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2361742
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2361743
    if-eqz v0, :cond_0

    .line 2361744
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 2361745
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 2361746
    iput-boolean p1, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->e:Z

    .line 2361747
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2361748
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2361749
    if-eqz v0, :cond_0

    .line 2361750
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 2361751
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 2361752
    iput-boolean p1, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->i:Z

    .line 2361753
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2361754
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2361755
    if-eqz v0, :cond_0

    .line 2361756
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 2361757
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 2361759
    iput-boolean p1, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->j:Z

    .line 2361760
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2361761
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2361762
    if-eqz v0, :cond_0

    .line 2361763
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 2361764
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 2361672
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2361673
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2361674
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2361675
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->o()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2361676
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->p()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2361677
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2361678
    const/16 v5, 0xb

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2361679
    const/4 v5, 0x0

    iget-boolean v6, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->e:Z

    invoke-virtual {p1, v5, v6}, LX/186;->a(IZ)V

    .line 2361680
    const/4 v5, 0x1

    iget-boolean v6, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->f:Z

    invoke-virtual {p1, v5, v6}, LX/186;->a(IZ)V

    .line 2361681
    const/4 v5, 0x2

    iget-boolean v6, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->g:Z

    invoke-virtual {p1, v5, v6}, LX/186;->a(IZ)V

    .line 2361682
    const/4 v5, 0x3

    iget-boolean v6, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->h:Z

    invoke-virtual {p1, v5, v6}, LX/186;->a(IZ)V

    .line 2361683
    const/4 v5, 0x4

    iget-boolean v6, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->i:Z

    invoke-virtual {p1, v5, v6}, LX/186;->a(IZ)V

    .line 2361684
    const/4 v5, 0x5

    iget-boolean v6, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->j:Z

    invoke-virtual {p1, v5, v6}, LX/186;->a(IZ)V

    .line 2361685
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2361686
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2361687
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2361688
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2361689
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2361690
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2361691
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2361807
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2361808
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->o()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2361809
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->o()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    .line 2361810
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->o()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2361811
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;

    .line 2361812
    iput-object v0, v1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->m:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    .line 2361813
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->p()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2361814
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->p()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    .line 2361815
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->p()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2361816
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;

    .line 2361817
    iput-object v0, v1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->n:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    .line 2361818
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2361819
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2361805
    new-instance v0, LX/GWM;

    invoke-direct {v0, p1}, LX/GWM;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2361806
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2361797
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2361798
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->e:Z

    .line 2361799
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->f:Z

    .line 2361800
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->g:Z

    .line 2361801
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->h:Z

    .line 2361802
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->i:Z

    .line 2361803
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->j:Z

    .line 2361804
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2361765
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2361766
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->b()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2361767
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2361768
    iput v2, p2, LX/18L;->c:I

    .line 2361769
    :goto_0
    return-void

    .line 2361770
    :cond_0
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2361771
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->mz_()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2361772
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2361773
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2361774
    :cond_1
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2361775
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->mA_()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2361776
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2361777
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2361778
    :cond_2
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2361779
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->o()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    move-result-object v0

    .line 2361780
    if-eqz v0, :cond_5

    .line 2361781
    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2361782
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2361783
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 2361784
    :cond_3
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2361785
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->p()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 2361786
    if-eqz v0, :cond_5

    .line 2361787
    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2361788
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2361789
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 2361790
    :cond_4
    const-string v0, "top_level_comments.total_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2361791
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->p()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 2361792
    if-eqz v0, :cond_5

    .line 2361793
    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2361794
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2361795
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 2361796
    :cond_5
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2361692
    const-string v0, "likers"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2361693
    check-cast p2, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    invoke-direct {p0, p2}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->a(Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;)V

    .line 2361694
    :cond_0
    :goto_0
    return-void

    .line 2361695
    :cond_1
    const-string v0, "top_level_comments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2361696
    check-cast p2, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    invoke-direct {p0, p2}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->a(Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 2361697
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2361698
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->a(Z)V

    .line 2361699
    :cond_0
    :goto_0
    return-void

    .line 2361700
    :cond_1
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2361701
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->b(Z)V

    goto :goto_0

    .line 2361702
    :cond_2
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2361703
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->c(Z)V

    goto :goto_0

    .line 2361704
    :cond_3
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2361705
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->o()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    move-result-object v0

    .line 2361706
    if-eqz v0, :cond_0

    .line 2361707
    if-eqz p3, :cond_4

    .line 2361708
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    .line 2361709
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;->a(I)V

    .line 2361710
    iput-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->m:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    goto :goto_0

    .line 2361711
    :cond_4
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;->a(I)V

    goto :goto_0

    .line 2361712
    :cond_5
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2361713
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->p()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 2361714
    if-eqz v0, :cond_0

    .line 2361715
    if-eqz p3, :cond_6

    .line 2361716
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    .line 2361717
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;->a(I)V

    .line 2361718
    iput-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->n:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    goto :goto_0

    .line 2361719
    :cond_6
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;->a(I)V

    goto :goto_0

    .line 2361720
    :cond_7
    const-string v0, "top_level_comments.total_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2361721
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->p()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 2361722
    if-eqz v0, :cond_0

    .line 2361723
    if-eqz p3, :cond_8

    .line 2361724
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    .line 2361725
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;->b(I)V

    .line 2361726
    iput-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->n:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    goto/16 :goto_0

    .line 2361727
    :cond_8
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;->b(I)V

    goto/16 :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2361643
    new-instance v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;

    invoke-direct {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;-><init>()V

    .line 2361644
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2361645
    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2361646
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2361647
    iget-boolean v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->e:Z

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 2361648
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2361649
    iget-boolean v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->f:Z

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 2361650
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2361651
    iget-boolean v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->g:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2361652
    const v0, 0x388752fb

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 2361653
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2361654
    iget-boolean v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->h:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2361655
    const v0, -0x78fb05b

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2361656
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->k:Ljava/lang/String;

    .line 2361657
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2361658
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->l:Ljava/lang/String;

    .line 2361659
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic l()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2361660
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->o()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic m()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2361661
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->p()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    return-object v0
.end method

.method public final mA_()Z
    .locals 2

    .prologue
    .line 2361662
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2361663
    iget-boolean v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->j:Z

    return v0
.end method

.method public final mz_()Z
    .locals 2

    .prologue
    .line 2361664
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2361665
    iget-boolean v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->i:Z

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2361666
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->o:Ljava/lang/String;

    .line 2361667
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2361668
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->m:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->m:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    .line 2361669
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->m:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    return-object v0
.end method

.method public final p()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2361670
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->n:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->n:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    .line 2361671
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->n:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    return-object v0
.end method
