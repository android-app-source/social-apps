.class public final Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4efc89c3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2361239
    const-class v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2361238
    const-class v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2361236
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2361237
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2361226
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2361227
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->j()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2361228
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2361229
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x234b16d

    invoke-static {v3, v2, v4}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2361230
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2361231
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2361232
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2361233
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2361234
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2361235
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2361211
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2361212
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->j()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2361213
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->j()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    .line 2361214
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->j()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2361215
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;

    .line 2361216
    iput-object v0, v1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->e:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    .line 2361217
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2361218
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x234b16d

    invoke-static {v2, v0, v3}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2361219
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2361220
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;

    .line 2361221
    iput v3, v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->g:I

    move-object v1, v0

    .line 2361222
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2361223
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2361224
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 2361225
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2361210
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2361240
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2361241
    const/4 v0, 0x2

    const v1, -0x234b16d

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->g:I

    .line 2361242
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2361207
    new-instance v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;-><init>()V

    .line 2361208
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2361209
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2361206
    const v0, 0x230e415e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2361205
    const v0, 0xa7c5482

    return v0
.end method

.method public final j()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2361203
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->e:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->e:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    .line 2361204
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->e:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2361201
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->f:Ljava/lang/String;

    .line 2361202
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2361199
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2361200
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
