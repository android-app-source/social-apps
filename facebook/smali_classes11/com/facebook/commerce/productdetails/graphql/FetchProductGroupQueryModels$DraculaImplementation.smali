.class public final Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2360755
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2360756
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2360599
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2360600
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 2360688
    if-nez p1, :cond_0

    .line 2360689
    :goto_0
    return v0

    .line 2360690
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2360691
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2360692
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2360693
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2360694
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2360695
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2360696
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2360697
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2360698
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2360699
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2360700
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 2360701
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 2360702
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2360703
    :sswitch_1
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2360704
    invoke-virtual {p0, p1, v6}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v2

    .line 2360705
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 2360706
    invoke-virtual {p0, p1, v7}, LX/15i;->b(II)Z

    move-result v3

    .line 2360707
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2360708
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2360709
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2360710
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2360711
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 2360712
    invoke-virtual {p3, v7, v3}, LX/186;->a(IZ)V

    .line 2360713
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 2360714
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2360715
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2360716
    const v2, 0x6ace6eb7

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v1

    .line 2360717
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2360718
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2360719
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2360720
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    move-result-object v1

    .line 2360721
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2360722
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2360723
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2360724
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2360725
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2360726
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2360727
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2360728
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2360729
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2360730
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2360731
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2360732
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2360733
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2360734
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2360735
    :sswitch_6
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2360736
    const v2, -0x23642ae7

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v1

    .line 2360737
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2360738
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2360739
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2360740
    :sswitch_7
    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    move-result-object v1

    .line 2360741
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2360742
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2360743
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2360744
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2360745
    :sswitch_8
    const-class v1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 2360746
    invoke-static {p3, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 2360747
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2360748
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2360749
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2360750
    :sswitch_9
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2360751
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2360752
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2360753
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2360754
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x59976632 -> :sswitch_4
        -0x4f141f07 -> :sswitch_2
        -0x23642ae7 -> :sswitch_7
        -0x234b16d -> :sswitch_9
        0x252a5612 -> :sswitch_8
        0x273f6519 -> :sswitch_6
        0x471ba6fe -> :sswitch_1
        0x6ace6eb7 -> :sswitch_3
        0x6eda260f -> :sswitch_0
        0x7f2cedd2 -> :sswitch_5
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2360679
    if-nez p0, :cond_0

    move v0, v1

    .line 2360680
    :goto_0
    return v0

    .line 2360681
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 2360682
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2360683
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2360684
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 2360685
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2360686
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2360687
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 2360672
    const/4 v7, 0x0

    .line 2360673
    const/4 v1, 0x0

    .line 2360674
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 2360675
    invoke-static {v2, v3, v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2360676
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 2360677
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2360678
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2360671
    new-instance v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2360667
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 2360668
    if-eqz v0, :cond_0

    .line 2360669
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2360670
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2360658
    sparse-switch p2, :sswitch_data_0

    .line 2360659
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2360660
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2360661
    const v1, 0x6ace6eb7

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2360662
    :goto_0
    :sswitch_1
    return-void

    .line 2360663
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2360664
    const v1, -0x23642ae7

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2360665
    :sswitch_3
    const-class v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 2360666
    invoke-static {v0, p3}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x59976632 -> :sswitch_1
        -0x4f141f07 -> :sswitch_0
        -0x23642ae7 -> :sswitch_1
        -0x234b16d -> :sswitch_1
        0x252a5612 -> :sswitch_3
        0x273f6519 -> :sswitch_2
        0x471ba6fe -> :sswitch_1
        0x6ace6eb7 -> :sswitch_1
        0x6eda260f -> :sswitch_1
        0x7f2cedd2 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2360648
    if-nez p1, :cond_0

    move v0, v1

    .line 2360649
    :goto_0
    return v0

    .line 2360650
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 2360651
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2360652
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2360653
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 2360654
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 2360655
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2360656
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2360657
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 2360641
    if-eqz p1, :cond_0

    .line 2360642
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 2360643
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2360644
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 2360645
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2360646
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2360647
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2360635
    if-eqz p1, :cond_0

    .line 2360636
    invoke-static {p0, p1, p2}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;

    move-result-object v1

    .line 2360637
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;

    .line 2360638
    if-eq v0, v1, :cond_0

    .line 2360639
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2360640
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2360634
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2360632
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2360633
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2360627
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2360628
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2360629
    :cond_0
    iput-object p1, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2360630
    iput p2, p0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->b:I

    .line 2360631
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2360626
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2360625
    new-instance v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2360622
    iget v0, p0, LX/1vt;->c:I

    .line 2360623
    move v0, v0

    .line 2360624
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2360619
    iget v0, p0, LX/1vt;->c:I

    .line 2360620
    move v0, v0

    .line 2360621
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2360616
    iget v0, p0, LX/1vt;->b:I

    .line 2360617
    move v0, v0

    .line 2360618
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2360613
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2360614
    move-object v0, v0

    .line 2360615
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2360604
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2360605
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2360606
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2360607
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2360608
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2360609
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2360610
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2360611
    invoke-static {v3, v9, v2}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2360612
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2360601
    iget v0, p0, LX/1vt;->c:I

    .line 2360602
    move v0, v0

    .line 2360603
    return v0
.end method
