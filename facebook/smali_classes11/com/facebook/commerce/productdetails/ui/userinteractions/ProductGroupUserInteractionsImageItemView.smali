.class public Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public d:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2362973
    const-class v0, Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2362974
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2362975
    const-class v0, Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;

    invoke-static {v0, p0}, Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2362976
    const v0, 0x7f031034

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2362977
    const v0, 0x7f0d26db

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2362978
    const v0, 0x7f0d26dc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2362979
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v1, p0, Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2362980
    iget-object v1, p0, Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;->a:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v1

    iget-object p1, p0, Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;->a:LX/0hB;

    invoke-virtual {p1}, LX/0hB;->d()I

    move-result p1

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    sget p1, LX/GXD;->a:I

    mul-int/2addr v1, p1

    div-int/lit8 v1, v1, 0x64

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 2362981
    iget-object v1, p0, Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;->a:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v1

    iget-object p1, p0, Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;->a:LX/0hB;

    invoke-virtual {p1}, LX/0hB;->d()I

    move-result p1

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    sget p1, LX/GXD;->a:I

    mul-int/2addr v1, p1

    div-int/lit8 v1, v1, 0x64

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 2362982
    iget-object v1, p0, Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2362983
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object p0

    check-cast p0, LX/0hB;

    iput-object p0, p1, Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;->a:LX/0hB;

    return-void
.end method
