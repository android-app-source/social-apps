.class public Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Lcom/facebook/widget/text/BetterTextView;

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:Lcom/facebook/resources/ui/FbButton;

.field private f:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2362670
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2362671
    invoke-direct {p0}, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->a()V

    .line 2362672
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2362667
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2362668
    invoke-direct {p0}, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->a()V

    .line 2362669
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2362673
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2362674
    invoke-direct {p0}, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->a()V

    .line 2362675
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2362654
    const v0, 0x7f03103b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2362655
    const v0, 0x7f0d0432

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2362656
    const v0, 0x7f0d0434

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2362657
    const v0, 0x7f0d0433

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2362658
    const v0, 0x7f0d0435

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2362659
    const v0, 0x7f0d0393

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->e:Lcom/facebook/resources/ui/FbButton;

    .line 2362660
    const v0, 0x7f0d2702

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->f:Landroid/widget/FrameLayout;

    .line 2362661
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2362662
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2362663
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2362664
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2362665
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2362666
    return-void
.end method

.method public setFirstDataLabel(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2362652
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2362653
    return-void
.end method

.method public setFirstDataValue(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2362646
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2362647
    return-void
.end method

.method public setSecondDataLabel(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2362650
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2362651
    return-void
.end method

.method public setSecondDataValue(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2362648
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2362649
    return-void
.end method
