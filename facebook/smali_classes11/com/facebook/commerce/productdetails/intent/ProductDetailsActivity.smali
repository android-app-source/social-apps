.class public Lcom/facebook/commerce/productdetails/intent/ProductDetailsActivity;
.super Lcom/facebook/fbreact/fragment/ReactActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2362609
    invoke-direct {p0}, Lcom/facebook/fbreact/fragment/ReactActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final d(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 2362610
    const-string v0, "productID"

    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/intent/ProductDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "product_item_id"

    const-wide/16 v4, -0x1

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2362611
    const-string v0, "refID"

    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/intent/ProductDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "product_ref_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2362612
    const-string v0, "refType"

    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/intent/ProductDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "product_ref_type"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2362613
    return-object p1
.end method
