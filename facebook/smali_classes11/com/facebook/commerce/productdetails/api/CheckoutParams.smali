.class public Lcom/facebook/commerce/productdetails/api/CheckoutParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/commerce/productdetails/api/CheckoutParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:J

.field public final d:LX/7iP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2360328
    new-instance v0, LX/GW3;

    invoke-direct {v0}, LX/GW3;-><init>()V

    sput-object v0, Lcom/facebook/commerce/productdetails/api/CheckoutParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2360329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2360330
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/api/CheckoutParams;->a:Ljava/lang/String;

    .line 2360331
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/commerce/productdetails/api/CheckoutParams;->b:I

    .line 2360332
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/commerce/productdetails/api/CheckoutParams;->c:J

    .line 2360333
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/7iR;->a(I)LX/7iP;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/api/CheckoutParams;->d:LX/7iP;

    .line 2360334
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/Long;LX/7iP;)V
    .locals 2

    .prologue
    .line 2360335
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2360336
    iput-object p1, p0, Lcom/facebook/commerce/productdetails/api/CheckoutParams;->a:Ljava/lang/String;

    .line 2360337
    iput p2, p0, Lcom/facebook/commerce/productdetails/api/CheckoutParams;->b:I

    .line 2360338
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, Lcom/facebook/commerce/productdetails/api/CheckoutParams;->c:J

    .line 2360339
    iput-object p4, p0, Lcom/facebook/commerce/productdetails/api/CheckoutParams;->d:LX/7iP;

    .line 2360340
    return-void

    .line 2360341
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2360342
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2360343
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/api/CheckoutParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2360344
    iget v0, p0, Lcom/facebook/commerce/productdetails/api/CheckoutParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2360345
    iget-wide v0, p0, Lcom/facebook/commerce/productdetails/api/CheckoutParams;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2360346
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/api/CheckoutParams;->d:LX/7iP;

    invoke-static {v0}, LX/7iR;->a(LX/7iP;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2360347
    return-void
.end method
