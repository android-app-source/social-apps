.class public Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fg;


# instance fields
.field public a:LX/7iU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1g7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/GVv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0rq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/7ic;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/8Fe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7j6;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:J

.field private n:LX/7iP;

.field public o:Landroid/view/ViewGroup;

.field public p:Lcom/facebook/widget/listview/BetterListView;

.field private q:LX/8FZ;

.field public r:LX/7iT;

.field private final s:LX/7id;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7id",
            "<",
            "LX/7ia;",
            ">;"
        }
    .end annotation
.end field

.field private final t:LX/7id;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7id",
            "<",
            "LX/7ib;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2360467
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2360468
    new-instance v0, LX/GWB;

    invoke-direct {v0, p0}, LX/GWB;-><init>(Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;)V

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->s:LX/7id;

    .line 2360469
    new-instance v0, LX/GWC;

    invoke-direct {v0, p0}, LX/GWC;-><init>(Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;)V

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->t:LX/7id;

    .line 2360470
    return-void
.end method

.method public static a$redex0(Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)V
    .locals 10

    .prologue
    .line 2360471
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->c:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->s()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2360472
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2360473
    if-eqz p1, :cond_5

    .line 2360474
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2360475
    if-eqz v0, :cond_4

    move v0, v1

    :goto_0
    if-eqz v0, :cond_6

    .line 2360476
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->l()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2360477
    const/4 v4, 0x2

    invoke-virtual {v3, v0, v4}, LX/15i;->h(II)Z

    move-result v0

    :goto_1
    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->t()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->t()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->l()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    :goto_2
    move v0, v1

    .line 2360478
    if-nez v0, :cond_3

    .line 2360479
    :cond_0
    :goto_3
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->f:LX/GVv;

    const v5, 0x6ace6eb7

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 2360480
    if-nez p1, :cond_8

    move v2, v1

    :goto_4
    if-eqz v2, :cond_a

    :cond_1
    :goto_5
    if-eqz v1, :cond_c

    .line 2360481
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2360482
    iput-object v1, v0, LX/GVv;->d:LX/0Px;

    .line 2360483
    const v1, 0x17be5bc0

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2360484
    :cond_2
    :goto_6
    return-void

    .line 2360485
    :cond_3
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2360486
    if-eqz v0, :cond_0

    .line 2360487
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const v2, 0x7f0837c6

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2360488
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2360489
    move-object v1, v1

    .line 2360490
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2360491
    new-instance v1, LX/GWA;

    invoke-direct {v1, p0, p1}, LX/GWA;-><init>(Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)V

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_1

    :cond_7
    move v1, v2

    goto :goto_2

    .line 2360492
    :cond_8
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->n()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2360493
    if-nez v2, :cond_9

    move v2, v1

    goto :goto_4

    :cond_9
    move v2, v3

    goto :goto_4

    .line 2360494
    :cond_a
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->n()LX/1vs;

    move-result-object v2

    iget-object v4, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-static {v4, v2, v3, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    .line 2360495
    if-eqz v2, :cond_b

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_7
    if-eqz v2, :cond_1

    move v1, v3

    goto :goto_5

    :cond_b
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto :goto_7

    .line 2360496
    :cond_c
    iget-object v1, v0, LX/GVv;->e:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    if-eq v1, p1, :cond_2

    .line 2360497
    iput-object p1, v0, LX/GVv;->e:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    .line 2360498
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2360499
    iget-object v1, v0, LX/GVv;->e:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->n()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-static {v2, v1, v3, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v1

    if-eqz v1, :cond_e

    invoke-static {v1}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v1

    :goto_8
    invoke-virtual {v1}, LX/3Sa;->e()LX/3Sh;

    move-result-object v5

    :cond_d
    :goto_9
    invoke-interface {v5}, LX/2sN;->a()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-interface {v5}, LX/2sN;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v6, v1, LX/1vs;->b:I

    .line 2360500
    iget-object v1, v0, LX/GVv;->b:Ljava/util/EnumMap;

    const-class v7, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    invoke-virtual {v2, v6, v3, v7, v8}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 2360501
    iget-object v1, v0, LX/GVv;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    sget-object v7, LX/GVv;->a:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Unsupported section type: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v9, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    invoke-virtual {v2, v6, v3, v9, p0}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v7, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    .line 2360502
    :cond_e
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v1

    goto :goto_8

    .line 2360503
    :cond_f
    iget-object v1, v0, LX/GVv;->b:Ljava/util/EnumMap;

    const-class v7, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    invoke-virtual {v2, v6, v3, v7, v8}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/GVw;

    .line 2360504
    iget-object v2, v0, LX/GVv;->e:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    invoke-interface {v1, v2}, LX/GVw;->a(Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 2360505
    new-instance v2, LX/DaN;

    iget-object v6, v0, LX/GVv;->e:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    invoke-direct {v2, v1, v6}, LX/DaN;-><init>(LX/DML;Ljava/lang/Object;)V

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_9

    .line 2360506
    :cond_10
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/GVv;->d:LX/0Px;

    .line 2360507
    const v1, 0x7ceb6c4b

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto/16 :goto_6
.end method

.method private d()V
    .locals 4

    .prologue
    .line 2360508
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->d:LX/1Ck;

    const-string v1, "fetch_product_group"

    new-instance v2, LX/GW7;

    invoke-direct {v2, p0}, LX/GW7;-><init>(Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;)V

    new-instance v3, LX/GW9;

    invoke-direct {v3, p0}, LX/GW9;-><init>(Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2360509
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    .line 2360510
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2360511
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    invoke-static {p1}, LX/7iU;->a(LX/0QB;)LX/7iU;

    move-result-object v3

    check-cast v3, LX/7iU;

    const-class v4, LX/1g7;

    invoke-interface {p1, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/1g7;

    invoke-static {p1}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v5

    check-cast v5, LX/0zG;

    invoke-static {p1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {p1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    new-instance v8, LX/GVv;

    const/16 v9, 0x259

    invoke-static {p1, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct {v8, v9}, LX/GVv;-><init>(LX/0Ot;)V

    move-object v8, v8

    check-cast v8, LX/GVv;

    invoke-static {p1}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v9

    check-cast v9, LX/0rq;

    invoke-static {p1}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v10

    check-cast v10, LX/0hB;

    invoke-static {p1}, LX/7ic;->a(LX/0QB;)LX/7ic;

    move-result-object v11

    check-cast v11, LX/7ic;

    invoke-static {p1}, LX/8Fe;->a(LX/0QB;)LX/8Fe;

    move-result-object v12

    check-cast v12, LX/8Fe;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v13

    check-cast v13, LX/03V;

    const/16 v0, 0x18c5

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->a:LX/7iU;

    iput-object v4, v2, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->b:LX/1g7;

    iput-object v5, v2, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->c:LX/0zG;

    iput-object v6, v2, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->d:LX/1Ck;

    iput-object v7, v2, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->e:LX/0tX;

    iput-object v8, v2, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->f:LX/GVv;

    iput-object v9, v2, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->g:LX/0rq;

    iput-object v10, v2, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->h:LX/0hB;

    iput-object v11, v2, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->i:LX/7ic;

    iput-object v12, v2, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->j:LX/8Fe;

    iput-object v13, v2, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->k:LX/03V;

    iput-object p1, v2, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->l:LX/0Ot;

    .line 2360512
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->a:LX/7iU;

    .line 2360513
    iget-object v1, v0, LX/7iU;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x6c0003

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2360514
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 2360515
    const-string v0, "product_item_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->m:J

    .line 2360516
    iget-wide v2, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->m:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid item id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->m:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2360517
    const-string v0, "product_ref_type"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/7iP;

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->n:LX/7iP;

    .line 2360518
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->n:LX/7iP;

    if-nez v0, :cond_0

    .line 2360519
    sget-object v0, LX/7iP;->UNKNOWN:LX/7iP;

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->n:LX/7iP;

    .line 2360520
    :cond_0
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->f:LX/GVv;

    iget-object v1, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->n:LX/7iP;

    .line 2360521
    iput-object v1, v0, LX/GVv;->f:LX/7iP;

    .line 2360522
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->j:LX/8Fe;

    invoke-virtual {v0}, LX/8Fe;->a()V

    .line 2360523
    new-instance v0, LX/8FZ;

    invoke-direct {v0}, LX/8FZ;-><init>()V

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->q:LX/8FZ;

    .line 2360524
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->q:LX/8FZ;

    .line 2360525
    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 2360526
    return-void

    .line 2360527
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final mJ_()V
    .locals 0

    .prologue
    .line 2360528
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2360529
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2360530
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const/16 v0, 0x2426

    if-ne p1, v0, :cond_0

    .line 2360531
    const-string v0, "result_mutation_method"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/7jD;

    .line 2360532
    if-nez v0, :cond_1

    .line 2360533
    :cond_0
    :goto_0
    return-void

    .line 2360534
    :cond_1
    sget-object v1, LX/7jD;->DELETE:LX/7jD;

    if-ne v0, v1, :cond_2

    .line 2360535
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 2360536
    :cond_2
    sget-object v1, LX/7jD;->CREATE:LX/7jD;

    if-eq v0, v1, :cond_3

    sget-object v1, LX/7jD;->EDIT:LX/7jD;

    if-ne v0, v1, :cond_0

    .line 2360537
    :cond_3
    invoke-direct {p0}, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->d()V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x2

    const/16 v0, 0x2a

    const v1, 0xf8edafb

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2360538
    const v0, 0x7f031032

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->o:Landroid/view/ViewGroup;

    .line 2360539
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->b:LX/1g7;

    sget-object v2, LX/7iM;->VIEW_PRODUCT_DETAILS:LX/7iM;

    sget-object v3, LX/7iN;->COMMERCE_DETAILS_PAGE:LX/7iN;

    iget-object v4, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->n:LX/7iP;

    iget-object v5, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->n:LX/7iP;

    invoke-static {v5}, LX/7iR;->a(LX/7iP;)I

    move-result v5

    int-to-long v6, v5

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v2, v3, v4, v5}, LX/1g7;->a(LX/7iM;LX/7iN;LX/7iP;Ljava/lang/Long;)LX/7iT;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->r:LX/7iT;

    .line 2360540
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->r:LX/7iT;

    iget-wide v2, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->m:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/7iT;->c(Ljava/lang/Long;)V

    .line 2360541
    const/4 v3, 0x0

    .line 2360542
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->o:Landroid/view/ViewGroup;

    const v2, 0x102000a

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->p:Lcom/facebook/widget/listview/BetterListView;

    .line 2360543
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->p:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setDividerHeight(I)V

    .line 2360544
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->p:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->b()V

    .line 2360545
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->p:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setVerticalScrollBarEnabled(Z)V

    .line 2360546
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->p:Lcom/facebook/widget/listview/BetterListView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setBroadcastInteractionChanges(Z)V

    .line 2360547
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->p:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->o:Landroid/view/ViewGroup;

    const v3, 0x1020004

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 2360548
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->p:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->f:LX/GVv;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2360549
    invoke-direct {p0}, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->d()V

    .line 2360550
    iget-object v0, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->o:Landroid/view/ViewGroup;

    const/16 v2, 0x2b

    const v3, 0x771a257e

    invoke-static {v8, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x21b140c3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2360551
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2360552
    iget-object v1, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->j:LX/8Fe;

    invoke-virtual {v1}, LX/8Fe;->b()V

    .line 2360553
    const/16 v1, 0x2b

    const v2, -0x2b068ee5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5e406003

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2360554
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2360555
    iget-object v1, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->d:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2360556
    const/16 v1, 0x2b

    const v2, 0x65e33ea3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xe4582b5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2360557
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2360558
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2360559
    if-eqz v1, :cond_0

    .line 2360560
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2360561
    :cond_0
    iget-object v1, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->r:LX/7iT;

    invoke-virtual {v1}, LX/7iT;->a()V

    .line 2360562
    iget-object v1, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->i:LX/7ic;

    iget-object v2, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->s:LX/7id;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2360563
    iget-object v1, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->i:LX/7ic;

    iget-object v2, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->t:LX/7id;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2360564
    const/16 v1, 0x2b

    const v2, 0x325e4d4d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x494b5fc3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2360565
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2360566
    iget-object v1, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->r:LX/7iT;

    invoke-virtual {v1}, LX/7iT;->b()V

    .line 2360567
    iget-object v1, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->i:LX/7ic;

    iget-object v2, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->s:LX/7id;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2360568
    iget-object v1, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->i:LX/7ic;

    iget-object v2, p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->t:LX/7id;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2360569
    const/16 v1, 0x2b

    const v2, -0x46f39d70

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
