.class public Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static a:LX/0Zb;

.field private static final h:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/7j6;

.field public c:I

.field public d:LX/GaB;

.field public e:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

.field public f:LX/GaH;

.field public final g:LX/7iP;

.field public i:LX/2uF;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2366095
    const-class v0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;

    const-string v1, "commerce_product_adapter"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->h:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/7j6;LX/0Zb;LX/7iP;)V
    .locals 0

    .prologue
    .line 2366049
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2366050
    iput-object p1, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->b:LX/7j6;

    .line 2366051
    sput-object p2, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->a:LX/0Zb;

    .line 2366052
    iput-object p3, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->g:LX/7iP;

    .line 2366053
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2366094
    int-to-long v0, p1

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 7

    .prologue
    .line 2366082
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 2366083
    iget-object v1, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->d:LX/GaB;

    if-eqz v1, :cond_1

    .line 2366084
    iget-object v1, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->d:LX/GaB;

    .line 2366085
    :goto_0
    move-object v0, v1

    .line 2366086
    :goto_1
    return-object v0

    .line 2366087
    :cond_0
    new-instance v0, Lcom/facebook/commerce/core/ui/ProductItemWithPriceOverlayView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/commerce/core/ui/ProductItemWithPriceOverlayView;-><init>(Landroid/content/Context;)V

    .line 2366088
    new-instance v1, LX/Ga9;

    iget-object v2, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->b:LX/7j6;

    sget-object v3, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->a:LX/0Zb;

    iget-object v4, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->g:LX/7iP;

    invoke-direct {v1, v0, v2, v3, v4}, LX/Ga9;-><init>(Lcom/facebook/commerce/core/ui/ProductItemWithPriceOverlayView;LX/7j6;LX/0Zb;LX/7iP;)V

    move-object v0, v1

    .line 2366089
    goto :goto_1

    .line 2366090
    :cond_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2366091
    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f031042

    const/4 v4, 0x0

    invoke-virtual {v1, v2, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/text/BetterTextView;

    .line 2366092
    new-instance v1, LX/GaB;

    iget-object v4, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->b:LX/7j6;

    iget-object v5, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->e:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    sget-object v6, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->a:LX/0Zb;

    invoke-direct/range {v1 .. v6}, LX/GaB;-><init>(Lcom/facebook/widget/text/BetterTextView;Landroid/content/Context;LX/7j6;Lcom/facebook/commerce/core/intent/MerchantInfoViewData;LX/0Zb;)V

    iput-object v1, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->d:LX/GaB;

    .line 2366093
    iget-object v1, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->d:LX/GaB;

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2366063
    instance-of v0, p1, LX/Ga9;

    if-eqz v0, :cond_7

    .line 2366064
    check-cast p1, LX/Ga9;

    .line 2366065
    iget-object v0, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->i:LX/2uF;

    if-eqz v0, :cond_2

    .line 2366066
    iget-object v0, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->i:LX/2uF;

    invoke-virtual {v0, p2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2366067
    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    .line 2366068
    iget-object v0, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->i:LX/2uF;

    invoke-virtual {v0, p2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2366069
    const-class v4, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_6

    .line 2366070
    iget-object v0, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->i:LX/2uF;

    invoke-virtual {v0, p2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2366071
    const-class v4, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->m()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel$ProductCatalogImageModel;

    move-result-object v0

    if-eqz v0, :cond_5

    :goto_2
    if-eqz v1, :cond_0

    .line 2366072
    iget-object v0, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->i:LX/2uF;

    invoke-virtual {v0, p2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    .line 2366073
    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->m()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel$ProductCatalogImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel$ProductCatalogImageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->j()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->l()Ljava/lang/String;

    move-result-object v0

    .line 2366074
    iget-object v3, p1, LX/Ga9;->l:Lcom/facebook/commerce/core/ui/ProductItemWithPriceOverlayView;

    invoke-virtual {v3, v1, v2}, Lcom/facebook/commerce/core/ui/ProductItemWithPriceOverlayView;->a(Landroid/net/Uri;Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;)V

    .line 2366075
    iget-object v3, p1, LX/Ga9;->m:LX/GZP;

    .line 2366076
    iput-object v0, v3, LX/GZP;->a:Ljava/lang/String;

    .line 2366077
    :cond_0
    :goto_3
    return-void

    :cond_1
    move v0, v2

    .line 2366078
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_2

    :cond_6
    move v1, v2

    goto :goto_2

    .line 2366079
    :cond_7
    instance-of v0, p1, LX/GaB;

    if-eqz v0, :cond_0

    .line 2366080
    check-cast p1, LX/GaB;

    .line 2366081
    iget-object v0, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->f:LX/GaH;

    iget v1, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->c:I

    invoke-virtual {p1, v0, v1}, LX/GaB;->a(LX/GaH;I)V

    goto :goto_3
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2366057
    if-nez p1, :cond_0

    .line 2366058
    const/4 v0, 0x0

    .line 2366059
    :goto_0
    return v0

    .line 2366060
    :cond_0
    iget-object v0, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->i:LX/2uF;

    invoke-virtual {v0}, LX/39O;->c()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 2366061
    const/4 v0, 0x2

    goto :goto_0

    .line 2366062
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2366054
    iget v0, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->c:I

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    .line 2366055
    iget-object v0, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->i:LX/2uF;

    invoke-virtual {v0}, LX/39O;->c()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 2366056
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->i:LX/2uF;

    invoke-virtual {v0}, LX/39O;->c()I

    move-result v0

    goto :goto_0
.end method
