.class public final Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2367541
    const-class v0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;

    new-instance v1, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2367542
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2367543
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 2367544
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2367545
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0xa

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 2367546
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2367547
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 2367548
    if-eqz v2, :cond_0

    .line 2367549
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2367550
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2367551
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2367552
    if-eqz v2, :cond_1

    .line 2367553
    const-string v3, "can_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2367554
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2367555
    :cond_1
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 2367556
    if-eqz v2, :cond_2

    .line 2367557
    const-string v2, "category_names"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2367558
    invoke-virtual {v1, v0, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2367559
    :cond_2
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 2367560
    if-eqz v2, :cond_3

    .line 2367561
    const-string v2, "category_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2367562
    invoke-virtual {v1, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2367563
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2367564
    if-eqz v2, :cond_9

    .line 2367565
    const-string v3, "commerce_store"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2367566
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2367567
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2367568
    if-eqz v3, :cond_5

    .line 2367569
    const-string v4, "commerce_collections"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2367570
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2367571
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_4

    .line 2367572
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    invoke-static {v1, v5, p1, p2}, LX/8vK;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2367573
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2367574
    :cond_4
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2367575
    :cond_5
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2367576
    if-eqz v3, :cond_6

    .line 2367577
    const-string v4, "commerce_merchant_settings"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2367578
    invoke-static {v1, v3, p1}, LX/7ip;->a(LX/15i;ILX/0nX;)V

    .line 2367579
    :cond_6
    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2367580
    if-eqz v3, :cond_7

    .line 2367581
    const-string v4, "intro_summary"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2367582
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2367583
    :cond_7
    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2367584
    if-eqz v3, :cond_8

    .line 2367585
    const-string v4, "url"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2367586
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2367587
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2367588
    :cond_9
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2367589
    if-eqz v2, :cond_a

    .line 2367590
    const-string v3, "does_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2367591
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2367592
    :cond_a
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2367593
    if-eqz v2, :cond_b

    .line 2367594
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2367595
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2367596
    :cond_b
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2367597
    if-eqz v2, :cond_c

    .line 2367598
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2367599
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2367600
    :cond_c
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2367601
    if-eqz v2, :cond_d

    .line 2367602
    const-string v3, "page_likers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2367603
    invoke-static {v1, v2, p1}, LX/7ii;->a(LX/15i;ILX/0nX;)V

    .line 2367604
    :cond_d
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2367605
    if-eqz v2, :cond_e

    .line 2367606
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2367607
    invoke-static {v1, v2, p1}, LX/7ij;->a(LX/15i;ILX/0nX;)V

    .line 2367608
    :cond_e
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2367609
    if-eqz v2, :cond_f

    .line 2367610
    const-string v2, "viewer_profile_permissions"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2367611
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2367612
    :cond_f
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2367613
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2367614
    check-cast p1, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel$Serializer;->a(Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
