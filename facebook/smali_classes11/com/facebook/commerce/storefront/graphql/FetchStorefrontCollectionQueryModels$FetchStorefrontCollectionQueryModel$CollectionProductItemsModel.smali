.class public final Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4a8b3f0b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2367322
    const-class v0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2367323
    const-class v0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2367324
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2367325
    return-void
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2367326
    iget-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2367327
    iget-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2367328
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2367329
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2367330
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2367331
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2367332
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2367333
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2367334
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2367335
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2367336
    iget-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->e:Ljava/util/List;

    .line 2367337
    iget-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2367338
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2367339
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2367340
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2367341
    if-eqz v1, :cond_2

    .line 2367342
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;

    .line 2367343
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 2367344
    :goto_0
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2367345
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2367346
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2367347
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;

    .line 2367348
    iput-object v0, v1, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2367349
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2367350
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2367351
    new-instance v0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;

    invoke-direct {v0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;-><init>()V

    .line 2367352
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2367353
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2367354
    const v0, 0x391b0173

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2367355
    const v0, -0x5ba9a32d

    return v0
.end method
