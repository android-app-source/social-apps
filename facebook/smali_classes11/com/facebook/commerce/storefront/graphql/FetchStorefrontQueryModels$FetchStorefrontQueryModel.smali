.class public final Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4ee7bbb3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Z

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2367643
    const-class v0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2367644
    const-class v0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2367645
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2367646
    return-void
.end method

.method private q()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2367647
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2367648
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2367649
    :cond_0
    iget-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/enums/GraphQLPageCategoryType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2367650
    iget-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->h:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->h:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    .line 2367651
    iget-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->h:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 2367652
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2367653
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->q()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2367654
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 2367655
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->r()Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 2367656
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x729b7dec

    invoke-static {v4, v3, v5}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2367657
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2367658
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->m()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2367659
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->n()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v8, 0x64e68067

    invoke-static {v7, v6, v8}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2367660
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->o()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    const v9, -0xb5e5073

    invoke-static {v8, v7, v9}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2367661
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->p()LX/0Px;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/util/List;)I

    move-result v8

    .line 2367662
    const/16 v9, 0xb

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2367663
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 2367664
    const/4 v0, 0x1

    iget-boolean v9, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->f:Z

    invoke-virtual {p1, v0, v9}, LX/186;->a(IZ)V

    .line 2367665
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2367666
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2367667
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2367668
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->j:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2367669
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2367670
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2367671
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2367672
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2367673
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2367674
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2367675
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2367676
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2367677
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2367678
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x729b7dec

    invoke-static {v2, v0, v3}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2367679
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2367680
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;

    .line 2367681
    iput v3, v0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->i:I

    move-object v1, v0

    .line 2367682
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2367683
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x64e68067

    invoke-static {v2, v0, v3}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2367684
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2367685
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;

    .line 2367686
    iput v3, v0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->m:I

    move-object v1, v0

    .line 2367687
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->o()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2367688
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->o()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0xb5e5073

    invoke-static {v2, v0, v3}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2367689
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->o()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2367690
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;

    .line 2367691
    iput v3, v0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->n:I

    move-object v1, v0

    .line 2367692
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2367693
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 2367694
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 2367695
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 2367696
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_3
    move-object p0, v1

    .line 2367697
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2367698
    new-instance v0, LX/GZv;

    invoke-direct {v0, p1}, LX/GZv;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2367699
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2367636
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2367637
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->f:Z

    .line 2367638
    const/4 v0, 0x4

    const v1, -0x729b7dec

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->i:I

    .line 2367639
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->j:Z

    .line 2367640
    const/16 v0, 0x8

    const v1, 0x64e68067

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->m:I

    .line 2367641
    const/16 v0, 0x9

    const v1, -0xb5e5073

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->n:I

    .line 2367642
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2367615
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2367616
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2367700
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2367617
    new-instance v0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;

    invoke-direct {v0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;-><init>()V

    .line 2367618
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2367619
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2367620
    const v0, 0x5d301c0f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2367621
    const v0, 0x252222

    return v0
.end method

.method public final j()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2367622
    iget-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->g:Ljava/util/List;

    .line 2367623
    iget-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCommerceStore"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2367624
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2367625
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2367626
    iget-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k:Ljava/lang/String;

    .line 2367627
    iget-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2367628
    iget-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->l:Ljava/lang/String;

    .line 2367629
    iget-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final n()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageLikers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2367630
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2367631
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->m:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final o()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 2367632
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2367633
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->n:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final p()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2367634
    iget-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->o:Ljava/util/List;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->o:Ljava/util/List;

    .line 2367635
    iget-object v0, p0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->o:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
