.class public Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/GZf;


# static fields
.field private static final o:Ljava/lang/String;


# instance fields
.field public A:I

.field public B:I

.field public C:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

.field public D:LX/E8s;

.field private E:LX/E8t;

.field public F:Landroid/view/View;

.field public G:I

.field private H:I

.field public a:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/GZS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1g7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/7iU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/7j6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/Ga8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/GZR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/GZL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private q:LX/7iT;

.field public r:Ljava/lang/Long;

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Z

.field private u:Z

.field private v:LX/Ga3;

.field public w:LX/GZH;

.field public x:LX/7iP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2367043
    const-class v0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->o:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2367039
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2367040
    iput-boolean v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->t:Z

    .line 2367041
    iput-boolean v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->u:Z

    .line 2367042
    iput-boolean v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->z:Z

    return-void
.end method

.method private static a(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;Lcom/facebook/commerce/core/intent/MerchantInfoViewData;)LX/GaD;
    .locals 2

    .prologue
    .line 2367021
    new-instance v0, LX/GaD;

    invoke-direct {v0}, LX/GaD;-><init>()V

    .line 2367022
    invoke-static {p1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, v0, LX/GaD;->h:LX/0am;

    .line 2367023
    move-object v0, v0

    .line 2367024
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->s:Ljava/lang/String;

    .line 2367025
    invoke-static {v1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object p1

    iput-object p1, v0, LX/GaD;->j:LX/0am;

    .line 2367026
    move-object v0, v0

    .line 2367027
    iget-object v1, p2, Lcom/facebook/commerce/core/intent/MerchantInfoViewData;->f:Ljava/lang/String;

    move-object v1, v1

    .line 2367028
    invoke-static {v1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object p1

    iput-object p1, v0, LX/GaD;->i:LX/0am;

    .line 2367029
    move-object v0, v0

    .line 2367030
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->i:LX/Ga8;

    .line 2367031
    invoke-static {v1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object p1

    iput-object p1, v0, LX/GaD;->g:LX/0am;

    .line 2367032
    move-object v0, v0

    .line 2367033
    iget-boolean v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->z:Z

    if-eqz v1, :cond_0

    .line 2367034
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/GaD;->a(Lcom/facebook/commerce/core/intent/MerchantInfoViewData;)LX/GaD;

    .line 2367035
    iget v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->H:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 2367036
    iput-object v1, v0, LX/GaD;->f:LX/0am;

    .line 2367037
    :goto_0
    return-object v0

    .line 2367038
    :cond_0
    invoke-virtual {v0, p2}, LX/GaD;->a(Lcom/facebook/commerce/core/intent/MerchantInfoViewData;)LX/GaD;

    goto :goto_0
.end method

.method public static a(JLjava/lang/String;ZZLX/7iP;Ljava/lang/Long;)Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2367010
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2367011
    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {v2, v0, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2367012
    const-string v0, "arg_init_product_id"

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2367013
    const-string v0, "extra_finish_on_launch_edit_shop"

    invoke-virtual {v2, v0, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2367014
    const-string v0, "extra_is_inside_page_surface_tab"

    invoke-virtual {v2, v0, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2367015
    const-string v0, "product_ref_type"

    invoke-virtual {v2, v0, p5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2367016
    const-string v3, "product_ref_id"

    if-eqz p6, :cond_0

    invoke-virtual {p6}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2367017
    new-instance v0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;

    invoke-direct {v0}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;-><init>()V

    .line 2367018
    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2367019
    return-object v0

    .line 2367020
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;JLcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;Lcom/facebook/commerce/core/intent/MerchantInfoViewData;LX/15i;I)V
    .locals 13
    .param p1    # J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/commerce/core/intent/MerchantInfoViewData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2367006
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->j:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 2367007
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->k:LX/GZR;

    invoke-virtual {v0, p1, p2}, LX/GZR;->a(J)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v10

    .line 2367008
    iget-object v11, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->c:LX/1Ck;

    sget-object v12, LX/GZe;->COLLECTION_FETCH:LX/GZe;

    new-instance v0, LX/GZd;

    move-object v1, p0

    move-wide v4, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move/from16 v9, p6

    invoke-direct/range {v0 .. v9}, LX/GZd;-><init>(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;JJLcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;Lcom/facebook/commerce/core/intent/MerchantInfoViewData;LX/15i;I)V

    invoke-virtual {v11, v12, v10, v0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2367009
    return-void
.end method

.method private static a(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;LX/0zG;LX/GZS;LX/1Ck;LX/03V;LX/0Zb;LX/1g7;LX/7iU;LX/7j6;LX/Ga8;LX/0SG;LX/GZR;Ljava/util/concurrent/ExecutorService;LX/GZL;LX/03V;)V
    .locals 0

    .prologue
    .line 2367005
    iput-object p1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->a:LX/0zG;

    iput-object p2, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->b:LX/GZS;

    iput-object p3, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->c:LX/1Ck;

    iput-object p4, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->d:LX/03V;

    iput-object p5, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->e:LX/0Zb;

    iput-object p6, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->f:LX/1g7;

    iput-object p7, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->g:LX/7iU;

    iput-object p8, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->h:LX/7j6;

    iput-object p9, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->i:LX/Ga8;

    iput-object p10, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->j:LX/0SG;

    iput-object p11, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->k:LX/GZR;

    iput-object p12, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->l:Ljava/util/concurrent/ExecutorService;

    iput-object p13, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->m:LX/GZL;

    iput-object p14, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->n:LX/03V;

    return-void
.end method

.method private static a(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;LX/GZ5;LX/GaE;)V
    .locals 1

    .prologue
    .line 2366999
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->w:LX/GZH;

    .line 2367000
    iput-object p1, v0, LX/GZH;->c:LX/GZ5;

    .line 2367001
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->w:LX/GZH;

    .line 2367002
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/GaE;

    iput-object p1, v0, LX/GZH;->d:LX/GaE;

    .line 2367003
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->w:LX/GZH;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2367004
    return-void
.end method

.method private static a(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;Lcom/facebook/commerce/core/intent/MerchantInfoViewData;Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;JLX/15i;I)V
    .locals 8
    .param p4    # J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "setupAndBindCollectionRecyclerView"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2366981
    invoke-static {p0, p2, p1}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->a(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;Lcom/facebook/commerce/core/intent/MerchantInfoViewData;)LX/GaD;

    move-result-object v0

    .line 2366982
    invoke-static {p6, p7}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->a(LX/15i;I)Z

    move-result v1

    .line 2366983
    iput-boolean v1, v0, LX/GaD;->b:Z

    .line 2366984
    iget-boolean v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->z:Z

    if-eqz v1, :cond_0

    .line 2366985
    invoke-static {p6, p7}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->d(LX/15i;I)Z

    move-result v1

    .line 2366986
    iput-boolean v1, v0, LX/GaD;->e:Z

    .line 2366987
    :cond_0
    invoke-virtual {v0}, LX/GaD;->a()LX/GaE;

    move-result-object v1

    .line 2366988
    iget-object v2, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->m:LX/GZL;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    invoke-static {p6, p7}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->b(LX/15i;I)Z

    move-result v4

    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->x:LX/7iP;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->x:LX/7iP;

    :goto_0
    invoke-virtual {v2, v3, v4, v0}, LX/GZL;->a(Landroid/content/Context;ZLX/7iP;)LX/GZK;

    move-result-object v0

    .line 2366989
    iput-object p3, v0, LX/GZK;->g:Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;

    .line 2366990
    invoke-virtual {p3}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    iput v6, v0, LX/GZK;->i:I

    .line 2366991
    iput-wide p4, v0, LX/GZK;->h:J

    .line 2366992
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2366993
    iget-object v2, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->v:LX/Ga3;

    if-nez v2, :cond_1

    .line 2366994
    new-instance v2, LX/Ga3;

    iget-object v3, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->w:LX/GZH;

    invoke-virtual {v3}, LX/GZH;->d()I

    move-result v3

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, LX/Ga3;-><init>(II)V

    iput-object v2, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->v:LX/Ga3;

    .line 2366995
    iget-object v2, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->v:LX/Ga3;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2366996
    :cond_1
    invoke-static {p0, v0, v1}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->a(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;LX/GZ5;LX/GaE;)V

    .line 2366997
    return-void

    .line 2366998
    :cond_2
    sget-object v0, LX/7iP;->UNKNOWN:LX/7iP;

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 15

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v14

    move-object v0, p0

    check-cast v0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;

    invoke-static {v14}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v1

    check-cast v1, LX/0zG;

    invoke-static {v14}, LX/GZS;->b(LX/0QB;)LX/GZS;

    move-result-object v2

    check-cast v2, LX/GZS;

    invoke-static {v14}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v14}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v14}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    const-class v6, LX/1g7;

    invoke-interface {v14, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/1g7;

    invoke-static {v14}, LX/7iU;->a(LX/0QB;)LX/7iU;

    move-result-object v7

    check-cast v7, LX/7iU;

    invoke-static {v14}, LX/7j6;->a(LX/0QB;)LX/7j6;

    move-result-object v8

    check-cast v8, LX/7j6;

    invoke-static {v14}, LX/Ga8;->b(LX/0QB;)LX/Ga8;

    move-result-object v9

    check-cast v9, LX/Ga8;

    invoke-static {v14}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-static {v14}, LX/GZR;->b(LX/0QB;)LX/GZR;

    move-result-object v11

    check-cast v11, LX/GZR;

    invoke-static {v14}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/ExecutorService;

    const-class v13, LX/GZL;

    invoke-interface {v14, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/GZL;

    invoke-static {v14}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v14

    check-cast v14, LX/03V;

    invoke-static/range {v0 .. v14}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->a(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;LX/0zG;LX/GZS;LX/1Ck;LX/03V;LX/0Zb;LX/1g7;LX/7iU;LX/7j6;LX/Ga8;LX/0SG;LX/GZR;Ljava/util/concurrent/ExecutorService;LX/GZL;LX/03V;)V

    return-void
.end method

.method private static a(LX/15i;I)Z
    .locals 1
    .param p0    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2366980
    if-eqz p1, :cond_0

    invoke-static {p0, p1}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->b(LX/15i;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;JJLcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel;Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;Lcom/facebook/commerce/core/intent/MerchantInfoViewData;LX/15i;I)V
    .locals 9
    .param p5    # Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/commerce/core/intent/MerchantInfoViewData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2366975
    invoke-virtual {p5}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel;->a()Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2366976
    :goto_0
    return-void

    .line 2366977
    :cond_0
    invoke-virtual {p5}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel;->a()Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->j:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    sub-long v3, v0, p1

    const/4 v5, 0x0

    move-wide v0, p3

    invoke-static/range {v0 .. v5}, LX/7iS;->a(JIJZ)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2366978
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->e:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2366979
    invoke-virtual {p5}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel;->a()Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;

    move-result-object v3

    move-object v0, p0

    move-object/from16 v1, p7

    move-object v2, p6

    move-wide v4, p3

    move-object/from16 v6, p8

    move/from16 v7, p9

    invoke-static/range {v0 .. v7}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->a(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;Lcom/facebook/commerce/core/intent/MerchantInfoViewData;Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;JLX/15i;I)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 2366891
    if-nez p1, :cond_0

    .line 2366892
    :goto_0
    return-void

    .line 2366893
    :cond_0
    new-instance v0, LX/8A4;

    invoke-virtual {p1}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->p()LX/0Px;

    move-result-object v2

    invoke-direct {v0, v2}, LX/8A4;-><init>(Ljava/util/List;)V

    sget-object v2, LX/8A3;->EDIT_PROFILE:LX/8A3;

    invoke-virtual {v0, v2}, LX/8A4;->a(LX/8A3;)Z

    move-result v3

    .line 2366894
    invoke-virtual {p1}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_4

    move v0, v1

    .line 2366895
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const-class v6, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v5, v2, v4, v6}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v2

    .line 2366896
    if-eqz v2, :cond_5

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 2366897
    :goto_2
    new-instance v5, LX/GZc;

    invoke-direct {v5}, LX/GZc;-><init>()V

    invoke-static {v2, v5}, LX/0Ph;->e(Ljava/lang/Iterable;LX/0Rl;)Z

    move-result v5

    move v2, v5

    .line 2366898
    if-nez v3, :cond_2

    if-nez v0, :cond_1

    if-eqz v2, :cond_2

    .line 2366899
    :cond_1
    iget-object v2, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->n:LX/03V;

    sget-object v5, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->o:Ljava/lang/String;

    const-string v6, "Storefront cannot be null or empty for non-admins"

    invoke-virtual {v2, v5, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2366900
    :cond_2
    if-eqz v0, :cond_6

    if-eqz v3, :cond_6

    move v2, v1

    .line 2366901
    :goto_3
    invoke-static {p1}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->e(Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;)Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    move-result-object v5

    .line 2366902
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 2366903
    if-eqz p1, :cond_f

    .line 2366904
    invoke-virtual {p1}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2366905
    if-eqz v0, :cond_e

    move v0, v3

    :goto_4
    if-eqz v0, :cond_11

    .line 2366906
    invoke-virtual {p1}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v7, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2366907
    const-class v8, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;

    invoke-virtual {v7, v0, v3, v8}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_10

    move v0, v3

    :goto_5
    if-eqz v0, :cond_12

    .line 2366908
    invoke-virtual {p1}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2366909
    const-class v7, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;

    invoke-virtual {v6, v0, v3, v7}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;

    .line 2366910
    iget-object v3, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->i:LX/Ga8;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;->k()Z

    move-result v7

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;->l()I

    move-result v0

    invoke-static {v0}, LX/7j5;->a(I)Z

    move-result v0

    invoke-virtual {v3, v6, v7, v0}, LX/Ga8;->a(Ljava/lang/String;ZZ)V

    .line 2366911
    :goto_6
    invoke-virtual {p1}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2366912
    iget-boolean v6, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->z:Z

    if-eqz v6, :cond_13

    .line 2366913
    :cond_3
    :goto_7
    const/4 v3, 0x0

    .line 2366914
    invoke-virtual {p1}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_16

    .line 2366915
    invoke-virtual {p1}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v7, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v6, v0, v3, v7}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2366916
    if-eqz v0, :cond_14

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_8
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v6, 0x2

    if-ne v0, v6, :cond_15

    const/4 v0, 0x1

    :goto_9
    move v0, v0

    .line 2366917
    if-eqz v0, :cond_9

    .line 2366918
    invoke-virtual {p1}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v2, v0, v4, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2366919
    if-eqz v0, :cond_7

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_a
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2366920
    invoke-virtual {p1}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v6, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v1, v0, v4, v6}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2366921
    invoke-virtual {p1}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v1

    iget-object v6, v1, LX/1vs;->a:LX/15i;

    iget v7, v1, LX/1vs;->b:I

    .line 2366922
    if-eqz v0, :cond_8

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_b
    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 2366923
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    const/4 p1, 0x2

    if-ne v1, p1, :cond_17

    move v1, v4

    :goto_c
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2366924
    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v1}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->j()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_19

    .line 2366925
    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v1}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->j()LX/1vs;

    move-result-object v1

    iget-object p1, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2366926
    invoke-virtual {p1, v1, v8}, LX/15i;->j(II)I

    move-result v1

    if-lez v1, :cond_18

    .line 2366927
    :goto_d
    if-eqz v4, :cond_1a

    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    :goto_e
    move-object v4, v1

    .line 2366928
    move-object v1, p0

    invoke-static/range {v1 .. v7}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->a(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;JLcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;Lcom/facebook/commerce/core/intent/MerchantInfoViewData;LX/15i;I)V

    goto/16 :goto_0

    :cond_4
    move v0, v4

    .line 2366929
    goto/16 :goto_1

    .line 2366930
    :cond_5
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2366931
    goto/16 :goto_2

    :cond_6
    move v2, v4

    .line 2366932
    goto/16 :goto_3

    .line 2366933
    :cond_7
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2366934
    goto :goto_a

    .line 2366935
    :cond_8
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2366936
    goto :goto_b

    .line 2366937
    :cond_9
    invoke-virtual {p1}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_b

    .line 2366938
    invoke-virtual {p1}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v1, v0, v4, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2366939
    if-eqz v0, :cond_a

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_f
    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    .line 2366940
    :goto_10
    invoke-virtual {p1}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2366941
    invoke-static {p0, v0, v5}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->a(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;Lcom/facebook/commerce/core/intent/MerchantInfoViewData;)LX/GaD;

    move-result-object v0

    .line 2366942
    iput-boolean v2, v0, LX/GaD;->a:Z

    .line 2366943
    move-object v0, v0

    .line 2366944
    invoke-static {v3, v1}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->a(LX/15i;I)Z

    move-result v1

    .line 2366945
    iput-boolean v1, v0, LX/GaD;->b:Z

    .line 2366946
    move-object v0, v0

    .line 2366947
    invoke-virtual {v0}, LX/GaD;->a()LX/GaE;

    move-result-object v1

    .line 2366948
    new-instance v2, LX/GZA;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->x:LX/7iP;

    invoke-direct {v2, v0, v5, v3}, LX/GZA;-><init>(Landroid/content/Context;Lcom/facebook/commerce/core/intent/MerchantInfoViewData;LX/7iP;)V

    .line 2366949
    invoke-virtual {p1}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_d

    .line 2366950
    invoke-virtual {p1}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v5, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v3, v0, v4, v5}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2366951
    if-eqz v0, :cond_c

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 2366952
    :goto_11
    iput-object v0, v2, LX/GZA;->c:Ljava/util/List;

    .line 2366953
    invoke-static {p0, v2, v1}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->a(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;LX/GZ5;LX/GaE;)V

    goto/16 :goto_0

    .line 2366954
    :cond_a
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2366955
    goto :goto_f

    :cond_b
    const/4 v0, 0x0

    goto :goto_10

    .line 2366956
    :cond_c
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2366957
    goto :goto_11

    .line 2366958
    :cond_d
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2366959
    goto :goto_11

    :cond_e
    move v0, v6

    .line 2366960
    goto/16 :goto_4

    :cond_f
    move v0, v6

    goto/16 :goto_4

    :cond_10
    move v0, v6

    goto/16 :goto_5

    :cond_11
    move v0, v6

    goto/16 :goto_5

    .line 2366961
    :cond_12
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->i:LX/Ga8;

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v6, v6}, LX/Ga8;->a(Ljava/lang/String;ZZ)V

    goto/16 :goto_6

    .line 2366962
    :cond_13
    const-class v6, LX/1ZF;

    invoke-virtual {p0, v6}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1ZF;

    .line 2366963
    if-eqz v6, :cond_3

    invoke-static {v3, v0}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->d(LX/15i;I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2366964
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v7

    const v8, 0x7f0814d1

    invoke-virtual {p0, v8}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2366965
    iput-object v8, v7, LX/108;->g:Ljava/lang/String;

    .line 2366966
    move-object v7, v7

    .line 2366967
    invoke-virtual {v7}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v7

    .line 2366968
    invoke-interface {v6, v7}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2366969
    new-instance v7, LX/GZb;

    invoke-direct {v7, p0}, LX/GZb;-><init>(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;)V

    invoke-interface {v6, v7}, LX/1ZF;->a(LX/63W;)V

    goto/16 :goto_7

    .line 2366970
    :cond_14
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2366971
    goto/16 :goto_8

    :cond_15
    move v0, v3

    goto/16 :goto_9

    :cond_16
    move v0, v3

    goto/16 :goto_9

    :cond_17
    move v1, v8

    .line 2366972
    goto/16 :goto_c

    :cond_18
    move v4, v8

    .line 2366973
    goto/16 :goto_d

    :cond_19
    move v4, v8

    goto/16 :goto_d

    .line 2366974
    :cond_1a
    const/4 v1, 0x0

    goto/16 :goto_e
.end method

.method private static b(LX/15i;I)Z
    .locals 3
    .param p0    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 2366890
    if-eqz p1, :cond_0

    const-class v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;->l()I

    move-result v2

    const-class v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;->m()Z

    move-result v0

    invoke-static {v2, v0}, LX/7j5;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/15i;I)Z
    .locals 2
    .param p0    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 2366889
    if-eqz p1, :cond_0

    const-class v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;)Lcom/facebook/commerce/core/intent/MerchantInfoViewData;
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2367044
    const-string v1, ""

    .line 2367045
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2367046
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->m()Ljava/lang/String;

    move-result-object v1

    .line 2367047
    :cond_0
    const-string v2, ""

    .line 2367048
    if-eqz p0, :cond_6

    .line 2367049
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->o()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2367050
    if-eqz v0, :cond_5

    move v0, v6

    :goto_0
    if-eqz v0, :cond_8

    .line 2367051
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->o()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2367052
    invoke-virtual {v3, v0, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    move v0, v6

    :goto_1
    if-eqz v0, :cond_1

    .line 2367053
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->o()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 2367054
    :cond_1
    const-string v3, ""

    .line 2367055
    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2367056
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v3, v0

    .line 2367057
    :cond_2
    const/4 v4, -0x1

    .line 2367058
    if-eqz p0, :cond_a

    .line 2367059
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2367060
    if-eqz v0, :cond_9

    move v0, v6

    :goto_2
    if-eqz v0, :cond_3

    .line 2367061
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v4, v0, v7}, LX/15i;->j(II)I

    move-result v4

    .line 2367062
    :cond_3
    const-string v5, ""

    .line 2367063
    if-eqz p0, :cond_4

    .line 2367064
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->l()Ljava/lang/String;

    move-result-object v5

    .line 2367065
    :cond_4
    const-string v8, ""

    .line 2367066
    if-eqz p0, :cond_c

    .line 2367067
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2367068
    if-eqz v0, :cond_b

    move v0, v6

    :goto_3
    if-eqz v0, :cond_e

    .line 2367069
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v9, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2367070
    invoke-virtual {v9, v0, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_d

    :goto_4
    if-eqz v6, :cond_f

    .line 2367071
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v6, v0, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    .line 2367072
    :goto_5
    new-instance v0, Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/commerce/core/intent/MerchantInfoViewData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_5
    move v0, v7

    .line 2367073
    goto/16 :goto_0

    :cond_6
    move v0, v7

    goto/16 :goto_0

    :cond_7
    move v0, v7

    goto/16 :goto_1

    :cond_8
    move v0, v7

    goto/16 :goto_1

    :cond_9
    move v0, v7

    .line 2367074
    goto :goto_2

    :cond_a
    move v0, v7

    goto :goto_2

    :cond_b
    move v0, v7

    .line 2367075
    goto :goto_3

    :cond_c
    move v0, v7

    goto :goto_3

    :cond_d
    move v6, v7

    goto :goto_4

    :cond_e
    move v6, v7

    goto :goto_4

    :cond_f
    move-object v6, v8

    goto :goto_5
.end method

.method private e()V
    .locals 6

    .prologue
    .line 2366769
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->w:LX/GZH;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/GZH;->b(Z)V

    .line 2366770
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->b:LX/GZS;

    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->r:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 2366771
    new-instance v1, LX/GZt;

    invoke-direct {v1}, LX/GZt;-><init>()V

    move-object v1, v1

    .line 2366772
    const-string v4, "page_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2366773
    const-string v4, "collection_count"

    const/16 v5, 0xa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2366774
    const-string v4, "COMMERCE_SMALL_IMAGE_SIZE"

    const/16 v5, 0x32

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2366775
    const-string v4, "COMMERCE_LARGE_IMAGE_SIZE"

    const/16 v5, 0x2d0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2366776
    const-string v4, "COMMERCE_MEDIUM_IMAGE_SIZE"

    const/16 v5, 0xe6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2366777
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2366778
    iget-object v4, v0, LX/GZS;->a:LX/0tX;

    invoke-virtual {v4, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    move-object v0, v1

    .line 2366779
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->c:LX/1Ck;

    sget-object v2, LX/GZe;->STOREFRONT_FETCH:LX/GZe;

    new-instance v3, LX/GZa;

    invoke-direct {v3, p0}, LX/GZa;-><init>(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2366780
    return-void
.end method

.method public static k(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;)V
    .locals 4

    .prologue
    .line 2366817
    iget-boolean v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->u:Z

    if-eqz v0, :cond_0

    .line 2366818
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2366819
    :goto_0
    return-void

    .line 2366820
    :cond_0
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->h:LX/7j6;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->r:Ljava/lang/Long;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/7j6;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public static m(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;)V
    .locals 3

    .prologue
    .line 2366821
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->D:LX/E8s;

    if-nez v0, :cond_2

    .line 2366822
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->E:LX/E8t;

    if-nez v0, :cond_1

    .line 2366823
    new-instance v0, LX/E8s;

    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->D:LX/E8s;

    .line 2366824
    :goto_0
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->D:LX/E8s;

    const/4 v1, 0x1

    .line 2366825
    iput-boolean v1, v0, LX/E8s;->c:Z

    .line 2366826
    :cond_0
    :goto_1
    return-void

    .line 2366827
    :cond_1
    new-instance v0, LX/E8s;

    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->E:LX/E8t;

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->D:LX/E8s;

    .line 2366828
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->E:LX/E8t;

    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->D:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    goto :goto_0

    .line 2366829
    :cond_2
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->E:LX/E8t;

    if-eqz v0, :cond_0

    .line 2366830
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->D:LX/E8s;

    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->E:LX/E8t;

    .line 2366831
    iput-object v1, v0, LX/E8s;->a:Landroid/view/View;

    .line 2366832
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->E:LX/E8t;

    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->D:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    .line 2366833
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->D:LX/E8s;

    invoke-virtual {v0}, LX/E8s;->requestLayout()V

    goto :goto_1
.end method


# virtual methods
.method public final E_(I)V
    .locals 4

    .prologue
    .line 2366834
    iput p1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->B:I

    .line 2366835
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->F:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2366836
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->F:Landroid/view/View;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->B:I

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2366837
    :cond_0
    return-void
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2366838
    const-string v0, "page_store_front_fragment"

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2366839
    iput p1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->A:I

    .line 2366840
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->A:I

    invoke-static {v0, v1}, LX/8FX;->a(Landroid/view/ViewGroup;I)V

    .line 2366841
    return-void
.end method

.method public final a(LX/E8t;)V
    .locals 0

    .prologue
    .line 2366842
    iput-object p1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->E:LX/E8t;

    .line 2366843
    invoke-static {p0}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->m(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;)V

    .line 2366844
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    .line 2366845
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2366846
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2366847
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->g:LX/7iU;

    .line 2366848
    iget-object v2, v0, LX/7iU;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x6c0002

    invoke-interface {v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2366849
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v0

    .line 2366850
    const-string v0, "com.facebook.katana.profile.id"

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->r:Ljava/lang/Long;

    .line 2366851
    const-string v0, "arg_init_product_id"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->s:Ljava/lang/String;

    .line 2366852
    const-string v0, "extra_finish_on_launch_edit_shop"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->u:Z

    .line 2366853
    const-string v0, "extra_is_inside_page_surface_tab"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->z:Z

    .line 2366854
    const-string v0, "product_ref_type"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/7iP;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->x:LX/7iP;

    .line 2366855
    const-string v0, "product_ref_id"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2366856
    cmp-long v0, v2, v6

    if-eqz v0, :cond_0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->y:Ljava/lang/Long;

    .line 2366857
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0062

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->H:I

    .line 2366858
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->r:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v2, v6

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid page id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->r:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2366859
    return-void

    .line 2366860
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2366861
    goto :goto_1
.end method

.method public final a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2366862
    iput-object p1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->C:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    .line 2366863
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 2366864
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->e()V

    .line 2366865
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x3d53efc1

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2366781
    const v0, 0x7f0313cd

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2366782
    iget-object v3, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->f:LX/1g7;

    sget-object v4, LX/7iM;->VIEW_PRODUCT_STOREFRONT:LX/7iM;

    sget-object v5, LX/7iN;->COMMERCE_STOREFRONT:LX/7iN;

    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->x:LX/7iP;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->x:LX/7iP;

    :goto_0
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->y:Ljava/lang/Long;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->y:Ljava/lang/Long;

    :goto_1
    invoke-virtual {v3, v4, v5, v0, v1}, LX/1g7;->a(LX/7iM;LX/7iN;LX/7iP;Ljava/lang/Long;)LX/7iT;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->q:LX/7iT;

    .line 2366783
    new-instance v0, LX/GZH;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v3, LX/GaD;

    invoke-direct {v3}, LX/GaD;-><init>()V

    invoke-virtual {v3}, LX/GaD;->a()LX/GaE;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->x:LX/7iP;

    invoke-direct {v0, v1, v3, v4, v5}, LX/GZH;-><init>(Landroid/content/Context;LX/GaE;LX/GZ5;LX/7iP;)V

    iput-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->w:LX/GZH;

    .line 2366784
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->w:LX/GZH;

    new-instance v1, LX/GZX;

    invoke-direct {v1, p0}, LX/GZX;-><init>(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;)V

    .line 2366785
    iput-object v1, v0, LX/GZH;->h:Landroid/view/View$OnClickListener;

    .line 2366786
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->w:LX/GZH;

    new-instance v1, LX/GZY;

    invoke-direct {v1, p0}, LX/GZY;-><init>(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;)V

    .line 2366787
    iput-object v1, v0, LX/GZH;->g:Landroid/view/View$OnClickListener;

    .line 2366788
    iget-boolean v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->z:Z

    if-eqz v0, :cond_0

    .line 2366789
    invoke-static {p0}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->m(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;)V

    .line 2366790
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->F:Landroid/view/View;

    .line 2366791
    iget v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->B:I

    invoke-virtual {p0, v0}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->E_(I)V

    .line 2366792
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/GZZ;

    invoke-direct {v1, p0}, LX/GZZ;-><init>(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2366793
    :cond_0
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->w:LX/GZH;

    .line 2366794
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->D:LX/E8s;

    if-eqz v1, :cond_2

    .line 2366795
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->D:LX/E8s;

    invoke-virtual {v1}, LX/E8s;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2366796
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->D:LX/E8s;

    invoke-virtual {v1}, LX/E8s;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->D:LX/E8s;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2366797
    :cond_1
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->D:LX/E8s;

    .line 2366798
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    iput-object v3, v0, LX/GZH;->e:Landroid/view/View;

    .line 2366799
    :cond_2
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->F:Landroid/view/View;

    if-eqz v1, :cond_4

    .line 2366800
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->F:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2366801
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->F:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->F:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2366802
    :cond_3
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->F:Landroid/view/View;

    .line 2366803
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    iput-object v3, v0, LX/GZH;->f:Landroid/view/View;

    .line 2366804
    :cond_4
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->w:LX/GZH;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2366805
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->w:LX/GZH;

    .line 2366806
    new-instance v3, LX/3wu;

    iget-object v4, v1, LX/GZH;->a:Landroid/content/Context;

    const/4 v5, 0x2

    const/4 p1, 0x1

    const/4 p2, 0x0

    invoke-direct {v3, v4, v5, p1, p2}, LX/3wu;-><init>(Landroid/content/Context;IIZ)V

    .line 2366807
    new-instance v4, LX/GZB;

    invoke-direct {v4, v1}, LX/GZB;-><init>(LX/GZH;)V

    .line 2366808
    iput-object v4, v3, LX/3wu;->h:LX/3wr;

    .line 2366809
    move-object v1, v3

    .line 2366810
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2366811
    iget v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->A:I

    invoke-virtual {p0, v0}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->a(I)V

    .line 2366812
    iget-boolean v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->z:Z

    if-nez v0, :cond_5

    .line 2366813
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setBackgroundResource(I)V

    .line 2366814
    :cond_5
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->e()V

    .line 2366815
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const v1, -0xa1a32b

    invoke-static {v1, v2}, LX/02F;->f(II)V

    return-object v0

    .line 2366816
    :cond_6
    sget-object v0, LX/7iP;->PAGE:LX/7iP;

    goto/16 :goto_0

    :cond_7
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->r:Ljava/lang/Long;

    goto/16 :goto_1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6f113c45

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2366866
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2366867
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->c:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2366868
    iput-object v2, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->w:LX/GZH;

    .line 2366869
    iput-object v2, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->v:LX/Ga3;

    .line 2366870
    const/16 v1, 0x2b

    const v2, 0x53ceeed

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6c72472

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2366871
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2366872
    iget-boolean v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->t:Z

    if-eqz v1, :cond_0

    .line 2366873
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->e()V

    .line 2366874
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x203d609f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3597ca2c    # -3804533.0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2366875
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2366876
    iget-boolean v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->z:Z

    if-eqz v1, :cond_0

    .line 2366877
    :goto_0
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->q:LX/7iT;

    invoke-virtual {v1}, LX/7iT;->a()V

    .line 2366878
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->q:LX/7iT;

    sget-object v2, LX/7iO;->PAGE_STOREFRONT_ENTRY_GRID:LX/7iO;

    .line 2366879
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 2366880
    sget-object v5, LX/7iL;->EVENT:LX/7iL;

    iget-object v5, v5, LX/7iL;->value:Ljava/lang/String;

    sget-object v6, LX/7iQ;->DID_OPEN_STOREFRONT_FROM_PAGE_HEADER:LX/7iQ;

    iget-object v6, v6, LX/7iQ;->value:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2366881
    sget-object v5, LX/7iL;->SECTION_TYPE:LX/7iL;

    iget-object v5, v5, LX/7iL;->value:Ljava/lang/String;

    iget-object v6, v2, LX/7iO;->value:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2366882
    sget-object v5, LX/7iL;->LOGGING_EVENT_TIME:LX/7iL;

    iget-object v5, v5, LX/7iL;->value:Ljava/lang/String;

    iget-object v6, v1, LX/7iT;->s:LX/0So;

    invoke-interface {v6}, LX/0So;->now()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2366883
    iget-object v5, v1, LX/7iT;->q:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2366884
    const/16 v1, 0x2b

    const v2, 0x17edbb6b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2366885
    :cond_0
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->a:LX/0zG;

    invoke-interface {v1}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0h5;

    const v2, 0x7f0814cb

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4296d837

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2366886
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2366887
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->q:LX/7iT;

    invoke-virtual {v1}, LX/7iT;->b()V

    .line 2366888
    const/16 v1, 0x2b

    const v2, 0x7cdebd3d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
