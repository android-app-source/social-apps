.class public Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fg;


# instance fields
.field public a:LX/7iU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/GZR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1g7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/7j6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/GZ9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:J

.field public l:LX/1ZF;

.field private m:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private n:Landroid/support/v7/widget/RecyclerView;

.field private o:LX/GZ8;

.field private p:I

.field private q:LX/7iT;

.field private final r:I

.field private s:Z

.field public t:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2366662
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2366663
    const/4 v0, 0x2

    iput v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->r:I

    return-void
.end method

.method public static a$redex0(Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;J)V
    .locals 7

    .prologue
    .line 2366655
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2366656
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->v:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->t:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    if-nez v3, :cond_2

    move v3, v1

    :goto_1
    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->u:Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel;

    if-eqz v0, :cond_3

    :goto_2
    move v0, v1

    .line 2366657
    if-eqz v0, :cond_0

    .line 2366658
    iget-wide v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->k:J

    iget-object v2, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->u:Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel;

    invoke-virtual {v2}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel;->a()Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->h:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    sub-long v3, v4, p1

    iget-boolean v5, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->s:Z

    invoke-static/range {v0 .. v5}, LX/7iS;->a(JIJZ)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2366659
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->f:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2366660
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->l()V

    .line 2366661
    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method private static d(Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;)LX/7iP;
    .locals 1

    .prologue
    .line 2366654
    iget-boolean v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->s:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/7iP;->SHARE:LX/7iP;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/7iP;->PAGE:LX/7iP;

    goto :goto_0
.end method

.method private k()V
    .locals 14

    .prologue
    .line 2366638
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 2366639
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->d:LX/GZR;

    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->v:Ljava/lang/String;

    iget-wide v2, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->k:J

    new-instance v4, LX/GZU;

    invoke-direct {v4, p0, v6, v7}, LX/GZU;-><init>(Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;J)V

    new-instance v5, LX/GZV;

    invoke-direct {v5, p0, v6, v7}, LX/GZV;-><init>(Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;J)V

    .line 2366640
    new-instance v9, LX/0v6;

    const-string v8, "StorefrontCollection"

    invoke-direct {v9, v8}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 2366641
    const/4 v8, 0x0

    .line 2366642
    if-eqz v1, :cond_0

    .line 2366643
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 2366644
    new-instance v8, LX/GZk;

    invoke-direct {v8}, LX/GZk;-><init>()V

    move-object v8, v8

    .line 2366645
    const-string v12, "merchant_page_id"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v8, v12, v13}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2366646
    invoke-static {v8}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v8

    sget-object v12, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v8, v12}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v8

    sget-object v12, LX/0zS;->c:LX/0zS;

    invoke-virtual {v8, v12}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v8

    move-object v8, v8

    .line 2366647
    invoke-virtual {v9, v8}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    .line 2366648
    :cond_0
    invoke-static {v2, v3}, LX/GZR;->c(J)LX/0zO;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v10

    .line 2366649
    if-eqz v8, :cond_1

    .line 2366650
    new-instance v11, LX/GZQ;

    invoke-direct {v11, v0}, LX/GZQ;-><init>(LX/GZR;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v12

    invoke-static {v8, v11, v12}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    iget-object v11, v0, LX/GZR;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v8, v4, v11}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2366651
    :cond_1
    iget-object v8, v0, LX/GZR;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v10, v5, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2366652
    iget-object v8, v0, LX/GZR;->a:LX/0tX;

    invoke-virtual {v8, v9}, LX/0tX;->a(LX/0v6;)V

    .line 2366653
    return-void
.end method

.method private l()V
    .locals 7

    .prologue
    .line 2366619
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->j:LX/GZ9;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-wide v2, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->k:J

    iget-boolean v4, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->s:Z

    iget-object v5, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->t:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    invoke-static {p0}, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->d(Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;)LX/7iP;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, LX/GZ9;->a(Landroid/content/Context;JZLcom/facebook/commerce/core/intent/MerchantInfoViewData;LX/7iP;)LX/GZ8;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->o:LX/GZ8;

    .line 2366620
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->o:LX/GZ8;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2366621
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->n:Landroid/support/v7/widget/RecyclerView;

    .line 2366622
    new-instance v1, LX/3wu;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, LX/3wu;-><init>(Landroid/content/Context;IIZ)V

    .line 2366623
    new-instance v2, LX/GZT;

    invoke-direct {v2, p0}, LX/GZT;-><init>(Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;)V

    .line 2366624
    iput-object v2, v1, LX/3wu;->h:LX/3wr;

    .line 2366625
    move-object v1, v1

    .line 2366626
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2366627
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->p:I

    .line 2366628
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->t:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    invoke-static {v0}, LX/GaI;->a(Lcom/facebook/commerce/core/intent/MerchantInfoViewData;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2366629
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->p:I

    .line 2366630
    :cond_0
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->n:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/Ga3;

    iget v2, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->p:I

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, LX/Ga3;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2366631
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->o:LX/GZ8;

    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->u:Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel;

    invoke-virtual {v1}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel;->a()Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;

    move-result-object v1

    .line 2366632
    iput-object v1, v0, LX/GZ8;->k:Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;

    .line 2366633
    invoke-virtual {v1}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    iput v2, v0, LX/GZ8;->l:I

    .line 2366634
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2366635
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->m:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2366636
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->m:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2366637
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2366608
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2366609
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;

    invoke-static {p1}, LX/7iU;->a(LX/0QB;)LX/7iU;

    move-result-object v3

    check-cast v3, LX/7iU;

    invoke-static {p1}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v4

    check-cast v4, LX/0zG;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p1}, LX/GZR;->b(LX/0QB;)LX/GZR;

    move-result-object v6

    check-cast v6, LX/GZR;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v8

    check-cast v8, LX/0Zb;

    const-class v9, LX/1g7;

    invoke-interface {p1, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/1g7;

    invoke-static {p1}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-static {p1}, LX/7j6;->a(LX/0QB;)LX/7j6;

    move-result-object v11

    check-cast v11, LX/7j6;

    const-class v0, LX/GZ9;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/GZ9;

    iput-object v3, v2, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->a:LX/7iU;

    iput-object v4, v2, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->b:LX/0zG;

    iput-object v5, v2, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->c:LX/03V;

    iput-object v6, v2, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->d:LX/GZR;

    iput-object v7, v2, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->e:Lcom/facebook/content/SecureContextHelper;

    iput-object v8, v2, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->f:LX/0Zb;

    iput-object v9, v2, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->g:LX/1g7;

    iput-object v10, v2, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->h:LX/0SG;

    iput-object v11, v2, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->i:LX/7j6;

    iput-object p1, v2, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->j:LX/GZ9;

    .line 2366610
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->a:LX/7iU;

    .line 2366611
    iget-object v1, v0, LX/7iU;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x6c0001

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2366612
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2366613
    const-string v1, "collection_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->k:J

    .line 2366614
    const-string v1, "is_adunit"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->s:Z

    .line 2366615
    const-string v1, "merchant_page_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->v:Ljava/lang/String;

    .line 2366616
    iget-wide v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->k:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid collection id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->k:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2366617
    return-void

    .line 2366618
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final mJ_()V
    .locals 0

    .prologue
    .line 2366582
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x4da73883    # 3.50687328E8f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2366593
    const v0, 0x7f0302b8

    invoke-virtual {p1, v0, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 2366594
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->v:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    .line 2366595
    sget-object v1, LX/4vu;->DEFAULTS:Ljava/util/Map;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2366596
    move-object v0, v1

    .line 2366597
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2366598
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->g:LX/1g7;

    sget-object v4, LX/7iM;->VIEW_PRODUCT_COLLECTION:LX/7iM;

    sget-object v5, LX/7iN;->COMMERCE_STOREFRONT:LX/7iN;

    invoke-static {p0}, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->d(Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;)LX/7iP;

    move-result-object v6

    invoke-virtual {v1, v4, v5, v6, v0}, LX/1g7;->a(LX/7iM;LX/7iN;LX/7iP;Ljava/lang/Long;)LX/7iT;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->q:LX/7iT;

    .line 2366599
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->q:LX/7iT;

    iget-wide v4, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->k:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2366600
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, LX/7iT;->l:Ljava/lang/String;

    .line 2366601
    const v0, 0x7f0d05b0

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->m:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2366602
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->m:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2366603
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->m:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v7}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2366604
    const v0, 0x7f0d09a2

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->n:Landroid/support/v7/widget/RecyclerView;

    .line 2366605
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->k()V

    .line 2366606
    const v0, -0x5c8728f3

    invoke-static {v0, v2}, LX/02F;->f(II)V

    return-object v3

    .line 2366607
    :cond_0
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->v:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x38d3d5f1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2366586
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2366587
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->b:LX/0zG;

    invoke-interface {v1}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0h5;

    const v2, 0x7f0814cb

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2366588
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    iput-object v1, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->l:LX/1ZF;

    .line 2366589
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->l:LX/1ZF;

    if-nez v1, :cond_0

    .line 2366590
    :goto_0
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->q:LX/7iT;

    invoke-virtual {v1}, LX/7iT;->a()V

    .line 2366591
    const/16 v1, 0x2b

    const v2, 0x6f626b9d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2366592
    :cond_0
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->l:LX/1ZF;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    goto :goto_0
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4ab917bd    # 6065118.5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2366583
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2366584
    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->q:LX/7iT;

    invoke-virtual {v1}, LX/7iT;->b()V

    .line 2366585
    const/16 v1, 0x2b

    const v2, -0x5b7a0db7    # -5.809E-17f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
