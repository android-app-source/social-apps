.class public Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/GZf;


# static fields
.field public static final s:[I


# instance fields
.field public a:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/widget/ScrollingAwareScrollView;

.field private d:Landroid/widget/LinearLayout;

.field private e:J

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Z

.field public k:LX/E8s;

.field private l:LX/E8t;

.field public m:Landroid/view/View;

.field public n:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

.field private o:Z

.field public p:I

.field private q:I

.field private r:I

.field public t:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2367218
    const/4 v0, 0x2

    new-array v0, v0, [I

    sput-object v0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->s:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2367211
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2367212
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->f:Ljava/lang/String;

    .line 2367213
    iput-boolean v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->g:Z

    .line 2367214
    iput-boolean v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->j:Z

    .line 2367215
    iput-boolean v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->o:Z

    .line 2367216
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->r:I

    .line 2367217
    iput-boolean v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->t:Z

    return-void
.end method

.method public static a(JLjava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Z)Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2367200
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2367201
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2367202
    const-string v1, "arg_init_product_id"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2367203
    const-string v1, "extra_finish_on_launch_edit_shop"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2367204
    const-string v1, "extra_is_inside_page_surface_tab"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2367205
    const-string v1, "product_ref_type"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2367206
    const-string v1, "product_ref_id"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2367207
    const-string v1, "hide_page_header"

    invoke-virtual {v0, v1, p7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2367208
    new-instance v1, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    invoke-direct {v1}, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;-><init>()V

    .line 2367209
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2367210
    return-object v1
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    invoke-static {p0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v1

    check-cast v1, LX/0zG;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object p0

    check-cast p0, LX/0hB;

    iput-object v1, p1, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->a:LX/0zG;

    iput-object p0, p1, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->b:LX/0hB;

    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2367189
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->l:LX/E8t;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->d:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 2367190
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->k:LX/E8s;

    if-eqz v0, :cond_0

    .line 2367191
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->d:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->k:LX/E8s;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 2367192
    :cond_0
    new-instance v0, LX/E8s;

    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->l:LX/E8t;

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->k:LX/E8s;

    .line 2367193
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->k:LX/E8s;

    const/4 v1, 0x1

    .line 2367194
    iput-boolean v1, v0, LX/E8s;->c:Z

    .line 2367195
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->d:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->k:LX/E8s;

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 2367196
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->l:LX/E8t;

    iget-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->k:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    .line 2367197
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->c:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/ScrollingAwareScrollView;->setBackgroundResource(I)V

    .line 2367198
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 2367199
    :cond_1
    return-void
.end method

.method public static d(Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;I)V
    .locals 4

    .prologue
    .line 2367185
    iput p1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->r:I

    .line 2367186
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->m:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2367187
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->m:Landroid/view/View;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->r:I

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2367188
    :cond_0
    return-void
.end method


# virtual methods
.method public final E_(I)V
    .locals 1

    .prologue
    .line 2367182
    iget v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->r:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->r:I

    if-le p1, v0, :cond_0

    .line 2367183
    :goto_0
    return-void

    .line 2367184
    :cond_0
    invoke-static {p0, p1}, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->d(Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;I)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2367178
    const-string v0, "page_store_front_fragment"

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2367219
    iput p1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->q:I

    .line 2367220
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->c:Lcom/facebook/widget/ScrollingAwareScrollView;

    iget v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->q:I

    invoke-static {v0, v1}, LX/8FX;->a(Landroid/view/ViewGroup;I)V

    .line 2367221
    return-void
.end method

.method public final a(LX/E8t;)V
    .locals 0

    .prologue
    .line 2367179
    iput-object p1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->l:LX/E8t;

    .line 2367180
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->c()V

    .line 2367181
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 2367165
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2367166
    const-class v0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    invoke-static {v0, p0}, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2367167
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2367168
    const-string v1, "com.facebook.katana.profile.id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->e:J

    .line 2367169
    const-string v1, "arg_init_product_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->f:Ljava/lang/String;

    .line 2367170
    const-string v1, "0"

    iget-object v2, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2367171
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->f:Ljava/lang/String;

    .line 2367172
    :cond_0
    const-string v1, "extra_finish_on_launch_edit_shop"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->g:Z

    .line 2367173
    const-string v1, "extra_is_inside_page_surface_tab"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->o:Z

    .line 2367174
    const-string v1, "product_ref_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->h:Ljava/lang/String;

    .line 2367175
    const-string v1, "product_ref_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->i:Ljava/lang/String;

    .line 2367176
    const-string v1, "hide_page_header"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->j:Z

    .line 2367177
    return-void
.end method

.method public final a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2367163
    iput-object p1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->n:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    .line 2367164
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 2367162
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x48cb8969

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2367131
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 2367132
    const v0, 0x7f0313d2

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/ScrollingAwareScrollView;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->c:Lcom/facebook/widget/ScrollingAwareScrollView;

    .line 2367133
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->c:Lcom/facebook/widget/ScrollingAwareScrollView;

    const v2, 0x7f0d2db5

    invoke-virtual {v0, v2}, Lcom/facebook/widget/ScrollingAwareScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->d:Landroid/widget/LinearLayout;

    .line 2367134
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2367135
    const-string v0, "pageID"

    iget-wide v4, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->e:J

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2367136
    const-string v0, "refID"

    iget-object v3, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->i:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2367137
    const-string v0, "refType"

    iget-object v3, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->h:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2367138
    const-string v3, "hidePageHeader"

    iget-boolean v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->j:Z

    if-eqz v0, :cond_1

    const-string v0, "1"

    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2367139
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2367140
    const-string v0, "selectedProductID"

    iget-object v3, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->f:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2367141
    :cond_0
    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v0

    const-string v3, "/shops_store_rn_front"

    .line 2367142
    iput-object v3, v0, LX/98r;->a:Ljava/lang/String;

    .line 2367143
    move-object v0, v0

    .line 2367144
    const-string v3, "ShopsStoreFrontRoute"

    .line 2367145
    iput-object v3, v0, LX/98r;->b:Ljava/lang/String;

    .line 2367146
    move-object v0, v0

    .line 2367147
    iput-object v2, v0, LX/98r;->f:Landroid/os/Bundle;

    .line 2367148
    move-object v0, v0

    .line 2367149
    const/4 v2, 0x1

    .line 2367150
    iput v2, v0, LX/98r;->h:I

    .line 2367151
    move-object v0, v0

    .line 2367152
    invoke-virtual {v0}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 2367153
    invoke-static {v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->b(Landroid/os/Bundle;)Lcom/facebook/fbreact/fragment/FbReactFragment;

    move-result-object v0

    .line 2367154
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v2, v2

    .line 2367155
    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    .line 2367156
    const v3, 0x7f0d2db6

    invoke-virtual {v2, v3, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 2367157
    invoke-virtual {v2}, LX/0hH;->b()I

    .line 2367158
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->c:Lcom/facebook/widget/ScrollingAwareScrollView;

    const v2, 0x7f0d03c4

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->m:Landroid/view/View;

    .line 2367159
    iget v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->r:I

    invoke-virtual {p0, v0}, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->E_(I)V

    .line 2367160
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->c:Lcom/facebook/widget/ScrollingAwareScrollView;

    const v2, -0x6ee2b69d

    invoke-static {v2, v1}, LX/02F;->f(II)V

    return-object v0

    .line 2367161
    :cond_1
    const-string v0, "0"

    goto :goto_0
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6199ea7e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2367128
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2367129
    iget v1, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->r:I

    invoke-virtual {p0, v1}, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->E_(I)V

    .line 2367130
    const/16 v1, 0x2b

    const v2, 0x5adb70f6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x22c9c6d0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2367124
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2367125
    iget-boolean v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->o:Z

    if-nez v0, :cond_0

    .line 2367126
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->a:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    const v2, 0x7f0814cb

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2367127
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x494f4397

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2367115
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2367116
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->c:Lcom/facebook/widget/ScrollingAwareScrollView;

    new-instance v1, LX/GZh;

    invoke-direct {v1, p0}, LX/GZh;-><init>(Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ScrollingAwareScrollView;->a(LX/4oV;)V

    .line 2367117
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->m:Landroid/view/View;

    new-instance v1, LX/GZi;

    invoke-direct {v1, p0}, LX/GZi;-><init>(Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 2367118
    iget-boolean v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->o:Z

    if-eqz v0, :cond_0

    .line 2367119
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->c:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/ScrollingAwareScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 2367120
    iget-object v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 2367121
    :cond_0
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->c()V

    .line 2367122
    iget v0, p0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->q:I

    invoke-virtual {p0, v0}, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->a(I)V

    .line 2367123
    return-void
.end method
