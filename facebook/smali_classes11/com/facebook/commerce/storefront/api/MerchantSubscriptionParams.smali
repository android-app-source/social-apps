.class public Lcom/facebook/commerce/storefront/api/MerchantSubscriptionParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/commerce/storefront/api/MerchantSubscriptionParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2366421
    new-instance v0, LX/GZM;

    invoke-direct {v0}, LX/GZM;-><init>()V

    sput-object v0, Lcom/facebook/commerce/storefront/api/MerchantSubscriptionParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2366422
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2366423
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/storefront/api/MerchantSubscriptionParams;->a:Ljava/lang/String;

    .line 2366424
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/storefront/api/MerchantSubscriptionParams;->b:Ljava/lang/String;

    .line 2366425
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 2366426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2366427
    iput-object p1, p0, Lcom/facebook/commerce/storefront/api/MerchantSubscriptionParams;->a:Ljava/lang/String;

    .line 2366428
    if-eqz p2, :cond_0

    const-string v0, "SUBSCRIBED"

    :goto_0
    iput-object v0, p0, Lcom/facebook/commerce/storefront/api/MerchantSubscriptionParams;->b:Ljava/lang/String;

    .line 2366429
    return-void

    .line 2366430
    :cond_0
    const-string v0, "NONE"

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2366431
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2366432
    iget-object v0, p0, Lcom/facebook/commerce/storefront/api/MerchantSubscriptionParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2366433
    iget-object v0, p0, Lcom/facebook/commerce/storefront/api/MerchantSubscriptionParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2366434
    return-void
.end method
