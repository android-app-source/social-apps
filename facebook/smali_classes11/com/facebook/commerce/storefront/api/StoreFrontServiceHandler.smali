.class public Lcom/facebook/commerce/storefront/api/StoreFrontServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/GZN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2366474
    const-class v0, Lcom/facebook/commerce/storefront/api/StoreFrontServiceHandler;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/commerce/storefront/api/StoreFrontServiceHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/GZN;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2366475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2366476
    iput-object p1, p0, Lcom/facebook/commerce/storefront/api/StoreFrontServiceHandler;->c:LX/0Ot;

    .line 2366477
    iput-object p2, p0, Lcom/facebook/commerce/storefront/api/StoreFrontServiceHandler;->b:LX/0Ot;

    .line 2366478
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/commerce/storefront/api/StoreFrontServiceHandler;
    .locals 5

    .prologue
    .line 2366479
    const-class v1, Lcom/facebook/commerce/storefront/api/StoreFrontServiceHandler;

    monitor-enter v1

    .line 2366480
    :try_start_0
    sget-object v0, Lcom/facebook/commerce/storefront/api/StoreFrontServiceHandler;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2366481
    sput-object v2, Lcom/facebook/commerce/storefront/api/StoreFrontServiceHandler;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2366482
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2366483
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2366484
    new-instance v3, Lcom/facebook/commerce/storefront/api/StoreFrontServiceHandler;

    const/16 v4, 0x18e4

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0xb83

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, Lcom/facebook/commerce/storefront/api/StoreFrontServiceHandler;-><init>(LX/0Ot;LX/0Ot;)V

    .line 2366485
    move-object v0, v3

    .line 2366486
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2366487
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/commerce/storefront/api/StoreFrontServiceHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2366488
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2366489
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2366490
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v0

    .line 2366491
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2366492
    const-string v1, "update_merchant_subscription_status"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2366493
    iget-object v0, p0, Lcom/facebook/commerce/storefront/api/StoreFrontServiceHandler;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    .line 2366494
    if-nez v2, :cond_0

    .line 2366495
    const-string v0, "Null Parameters"

    invoke-static {v0}, LX/1nY;->valueOf(Ljava/lang/String;)LX/1nY;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2366496
    :goto_0
    return-object v0

    .line 2366497
    :cond_0
    iget-object v1, p0, Lcom/facebook/commerce/storefront/api/StoreFrontServiceHandler;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    const-string v3, "merchantSubscriptionParams"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/commerce/storefront/api/MerchantSubscriptionParams;

    sget-object v3, Lcom/facebook/commerce/storefront/api/StoreFrontServiceHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2366498
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2366499
    :cond_1
    new-instance v1, LX/4B3;

    invoke-direct {v1, v0}, LX/4B3;-><init>(Ljava/lang/String;)V

    throw v1
.end method
