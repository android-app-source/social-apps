.class public Lcom/facebook/commerce/storefront/ui/GridProductItemView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public c:Lcom/facebook/resources/ui/FbTextView;

.field public d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Lcom/facebook/resources/ui/FbButton;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2367911
    const-class v0, Lcom/facebook/commerce/storefront/ui/GridProductItemView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/commerce/storefront/ui/GridProductItemView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 2367900
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2367901
    if-eqz p2, :cond_1

    const v0, 0x7f0302ad

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2367902
    const v0, 0x7f0d098c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/ui/GridProductItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2367903
    const v0, 0x7f0d098f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/ui/GridProductItemView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2367904
    const v0, 0x7f0d098e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/ui/GridProductItemView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2367905
    const v0, 0x7f0d0990

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/ui/GridProductItemView;->e:Lcom/facebook/resources/ui/FbButton;

    .line 2367906
    if-nez p2, :cond_0

    .line 2367907
    iget-object v0, p0, Lcom/facebook/commerce/storefront/ui/GridProductItemView;->e:Lcom/facebook/resources/ui/FbButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2367908
    :cond_0
    iget-object v0, p0, Lcom/facebook/commerce/storefront/ui/GridProductItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2367909
    return-void

    .line 2367910
    :cond_1
    const v0, 0x7f0302ae

    goto :goto_0
.end method
