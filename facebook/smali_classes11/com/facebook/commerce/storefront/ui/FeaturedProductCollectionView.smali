.class public Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/7j6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/GZI;

.field private e:Lcom/facebook/widget/ListViewFriendlyViewPager;

.field private f:Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2367889
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2367890
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->a()V

    .line 2367891
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2367892
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2367893
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->a()V

    .line 2367894
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2367886
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2367887
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->a()V

    .line 2367888
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 2367877
    const-class v0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;

    invoke-static {v0, p0}, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2367878
    const v0, 0x7f0313cf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2367879
    const v0, 0x7f0d2dad

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/ListViewFriendlyViewPager;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->e:Lcom/facebook/widget/ListViewFriendlyViewPager;

    .line 2367880
    const v0, 0x7f0d2dae

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->f:Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;

    .line 2367881
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->b()V

    .line 2367882
    new-instance v0, LX/GZI;

    iget-object v1, p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->a:LX/7j6;

    iget-object v2, p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->b:LX/03V;

    iget-object v3, p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->c:LX/0Zb;

    invoke-direct {v0, v1, v2, v3}, LX/GZI;-><init>(LX/7j6;LX/03V;LX/0Zb;)V

    iput-object v0, p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->d:LX/GZI;

    .line 2367883
    iget-object v0, p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->e:Lcom/facebook/widget/ListViewFriendlyViewPager;

    iget-object v1, p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->d:LX/GZI;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2367884
    iget-object v0, p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->f:Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;

    iget-object v1, p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->e:Lcom/facebook/widget/ListViewFriendlyViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2367885
    return-void
.end method

.method private static a(Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;LX/7j6;LX/03V;LX/0Zb;)V
    .locals 0

    .prologue
    .line 2367876
    iput-object p1, p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->a:LX/7j6;

    iput-object p2, p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->b:LX/03V;

    iput-object p3, p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->c:LX/0Zb;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;

    invoke-static {v2}, LX/7j6;->a(LX/0QB;)LX/7j6;

    move-result-object v0

    check-cast v0, LX/7j6;

    invoke-static {v2}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {v2}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->a(Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;LX/7j6;LX/03V;LX/0Zb;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2367869
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 2367870
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 2367871
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2367872
    iget v0, v1, Landroid/graphics/Point;->x:I

    .line 2367873
    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 2367874
    iget-object v1, p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->e:Lcom/facebook/widget/ListViewFriendlyViewPager;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/facebook/widget/CustomViewPager;->b(IZ)V

    .line 2367875
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2367863
    iget-object v0, p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->d:LX/GZI;

    .line 2367864
    iput-object p1, v0, LX/GZI;->d:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    .line 2367865
    invoke-virtual {v0}, LX/0gG;->kV_()V

    .line 2367866
    iget-boolean v0, p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->g:Z

    if-nez v0, :cond_0

    .line 2367867
    invoke-virtual {p0, p2}, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->setProductItem(Ljava/lang/String;)V

    .line 2367868
    :cond_0
    return-void
.end method

.method public setProductItem(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2367855
    iget-object v0, p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->d:LX/GZI;

    invoke-virtual {v0, p1}, LX/GZI;->a(Ljava/lang/String;)I

    move-result v0

    .line 2367856
    if-ltz v0, :cond_0

    .line 2367857
    iget-object v1, p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->e:Lcom/facebook/widget/ListViewFriendlyViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2367858
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->g:Z

    .line 2367859
    :cond_0
    return-void
.end method

.method public setRefType(LX/7iP;)V
    .locals 1

    .prologue
    .line 2367860
    iget-object v0, p0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->d:LX/GZI;

    .line 2367861
    iput-object p1, v0, LX/GZI;->e:LX/7iP;

    .line 2367862
    return-void
.end method
