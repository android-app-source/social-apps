.class public Lcom/facebook/commerce/storefront/ui/StorefrontLargeHScrollCorexItem;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public c:Lcom/facebook/resources/ui/FbTextView;

.field public d:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2368092
    const-class v0, Lcom/facebook/commerce/storefront/ui/StorefrontLargeHScrollCorexItem;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/commerce/storefront/ui/StorefrontLargeHScrollCorexItem;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2368093
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2368094
    const v0, 0x7f0313d0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2368095
    const v0, 0x7f0d2daf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/ui/StorefrontLargeHScrollCorexItem;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2368096
    const v0, 0x7f0d2db3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/ui/StorefrontLargeHScrollCorexItem;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2368097
    const v0, 0x7f0d2db2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/ui/StorefrontLargeHScrollCorexItem;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2368098
    iget-object v0, p0, Lcom/facebook/commerce/storefront/ui/StorefrontLargeHScrollCorexItem;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2368099
    return-void
.end method
