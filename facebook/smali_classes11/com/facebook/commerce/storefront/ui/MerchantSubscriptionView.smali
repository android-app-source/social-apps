.class public Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/Ga6;


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Ga8;

.field private c:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2367954
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2367955
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->b()V

    .line 2367956
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2367924
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2367925
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->b()V

    .line 2367926
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2367951
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2367952
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->b()V

    .line 2367953
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->a:LX/0wM;

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2367946
    const v0, 0x7f030ac8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2367947
    const-class v0, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;

    invoke-static {v0, p0}, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2367948
    const v0, 0x7f0d1b83

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2367949
    iget-object v0, p0, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->c:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/Ga5;

    invoke-direct {v1, p0}, LX/Ga5;-><init>(Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2367950
    return-void
.end method

.method private e()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 2367929
    iget-object v0, p0, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->b:LX/Ga8;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->b:LX/Ga8;

    invoke-virtual {v0}, LX/Ga8;->c()Z

    move-result v0

    move v3, v0

    .line 2367930
    :goto_0
    if-eqz v3, :cond_1

    const v0, 0x7f0207d6

    .line 2367931
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz v3, :cond_2

    const v1, 0x7f0a00d2

    :goto_2
    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 2367932
    iget-object v1, p0, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->b:LX/Ga8;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->b:LX/Ga8;

    .line 2367933
    iget-boolean v2, v1, LX/Ga8;->e:Z

    move v1, v2

    .line 2367934
    :goto_3
    if-eqz v1, :cond_5

    .line 2367935
    if-eqz v3, :cond_4

    const v1, 0x7f0814cc

    .line 2367936
    :goto_4
    iget-object v2, p0, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v3, p0, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->a:LX/0wM;

    invoke-virtual {v3, v0, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v2, v0, v5, v5, v5}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2367937
    iget-object v0, p0, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2367938
    iget-object v0, p0, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2367939
    return-void

    :cond_0
    move v3, v2

    .line 2367940
    goto :goto_0

    .line 2367941
    :cond_1
    const v0, 0x7f020872

    goto :goto_1

    .line 2367942
    :cond_2
    const v1, 0x7f0a00e6

    goto :goto_2

    :cond_3
    move v1, v2

    .line 2367943
    goto :goto_3

    .line 2367944
    :cond_4
    const v1, 0x7f0814cd

    goto :goto_4

    .line 2367945
    :cond_5
    if-eqz v3, :cond_6

    const v1, 0x7f0814ce

    goto :goto_4

    :cond_6
    const v1, 0x7f0814cf

    goto :goto_4
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2367927
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->e()V

    .line 2367928
    return-void
.end method

.method public setViewController(LX/Ga8;)V
    .locals 2

    .prologue
    .line 2367916
    iget-object v0, p0, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->b:LX/Ga8;

    if-eqz v0, :cond_0

    .line 2367917
    iget-object v0, p0, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->b:LX/Ga8;

    const/4 v1, 0x0

    .line 2367918
    iput-object v1, v0, LX/Ga8;->f:LX/Ga6;

    .line 2367919
    :cond_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ga8;

    iput-object v0, p0, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->b:LX/Ga8;

    .line 2367920
    iget-object v0, p0, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->b:LX/Ga8;

    .line 2367921
    iput-object p0, v0, LX/Ga8;->f:LX/Ga6;

    .line 2367922
    invoke-direct {p0}, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->e()V

    .line 2367923
    return-void
.end method
