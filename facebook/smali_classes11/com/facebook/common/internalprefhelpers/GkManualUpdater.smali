.class public Lcom/facebook/common/internalprefhelpers/GkManualUpdater;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/0SG;

.field private final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/2ZJ;

.field private final e:Lcom/facebook/gk/internal/GkSessionlessFetcher;

.field private final f:LX/28W;

.field private final g:LX/0Sh;

.field private final h:LX/14U;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2368188
    const-class v0, Lcom/facebook/common/internalprefhelpers/GkManualUpdater;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/common/internalprefhelpers/GkManualUpdater;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2ZJ;Lcom/facebook/gk/internal/GkSessionlessFetcher;LX/28W;LX/0Sh;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2368189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2368190
    iput-object p1, p0, Lcom/facebook/common/internalprefhelpers/GkManualUpdater;->b:LX/0SG;

    .line 2368191
    iput-object p2, p0, Lcom/facebook/common/internalprefhelpers/GkManualUpdater;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2368192
    iput-object p3, p0, Lcom/facebook/common/internalprefhelpers/GkManualUpdater;->d:LX/2ZJ;

    .line 2368193
    iput-object p4, p0, Lcom/facebook/common/internalprefhelpers/GkManualUpdater;->e:Lcom/facebook/gk/internal/GkSessionlessFetcher;

    .line 2368194
    iput-object p5, p0, Lcom/facebook/common/internalprefhelpers/GkManualUpdater;->f:LX/28W;

    .line 2368195
    iput-object p6, p0, Lcom/facebook/common/internalprefhelpers/GkManualUpdater;->g:LX/0Sh;

    .line 2368196
    new-instance v0, LX/14U;

    invoke-direct {v0}, LX/14U;-><init>()V

    iput-object v0, p0, Lcom/facebook/common/internalprefhelpers/GkManualUpdater;->h:LX/14U;

    .line 2368197
    iget-object v0, p0, Lcom/facebook/common/internalprefhelpers/GkManualUpdater;->h:LX/14U;

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2368198
    iput-object v1, v0, LX/14U;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2368199
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 5

    .prologue
    .line 2368200
    iget-object v0, p0, Lcom/facebook/common/internalprefhelpers/GkManualUpdater;->g:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 2368201
    sget-object v0, LX/3g1;->c:LX/0Tn;

    iget-object v1, p0, Lcom/facebook/common/internalprefhelpers/GkManualUpdater;->d:LX/2ZJ;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2368202
    iget-object v1, p0, Lcom/facebook/common/internalprefhelpers/GkManualUpdater;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/common/internalprefhelpers/GkManualUpdater;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v1, v0, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2368203
    iget-object v0, p0, Lcom/facebook/common/internalprefhelpers/GkManualUpdater;->d:LX/2ZJ;

    invoke-virtual {v0}, LX/2ZJ;->c()LX/2ZE;

    move-result-object v0

    .line 2368204
    iget-object v1, p0, Lcom/facebook/common/internalprefhelpers/GkManualUpdater;->f:LX/28W;

    const-string v2, "manualGkRefresh"

    sget-object v3, Lcom/facebook/common/internalprefhelpers/GkManualUpdater;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iget-object v4, p0, Lcom/facebook/common/internalprefhelpers/GkManualUpdater;->h:LX/14U;

    invoke-virtual {v1, v2, v3, v0, v4}, LX/28W;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/List;LX/14U;)V

    .line 2368205
    if-eqz p1, :cond_0

    .line 2368206
    iget-object v0, p0, Lcom/facebook/common/internalprefhelpers/GkManualUpdater;->e:Lcom/facebook/gk/internal/GkSessionlessFetcher;

    invoke-virtual {v0}, Lcom/facebook/gk/internal/GkSessionlessFetcher;->a()Z

    move-result v0

    .line 2368207
    if-nez v0, :cond_0

    .line 2368208
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot fetch sessionless gatekeepers: SingleMethodRunner failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2368209
    :cond_0
    return-void
.end method
