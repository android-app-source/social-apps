.class public final Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/2ZK;

.field public final synthetic c:Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;ILX/2ZK;)V
    .locals 0

    .prologue
    .line 2368153
    iput-object p1, p0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment$3;->c:Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;

    iput p2, p0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment$3;->a:I

    iput-object p3, p0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment$3;->b:LX/2ZK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2368154
    iget-object v0, p0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment$3;->c:Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;

    iget v1, p0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment$3;->a:I

    iget-object v2, p0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment$3;->b:LX/2ZK;

    .line 2368155
    iget-object v3, v0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v3, :cond_1

    .line 2368156
    :cond_0
    :goto_0
    return-void

    .line 2368157
    :cond_1
    iget-object v3, v0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v3, v3

    .line 2368158
    check-cast v3, Landroid/app/ProgressDialog;

    .line 2368159
    if-eqz v3, :cond_0

    .line 2368160
    invoke-virtual {v3, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 2368161
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x10

    invoke-static {v4, v5}, LX/0YN;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    .line 2368162
    new-instance v5, Ljava/lang/StringBuilder;

    const-string p0, "Fetching "

    invoke-direct {v5, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
