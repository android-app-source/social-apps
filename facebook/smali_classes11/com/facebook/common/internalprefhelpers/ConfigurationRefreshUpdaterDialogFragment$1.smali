.class public final Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;)V
    .locals 0

    .prologue
    .line 2368126
    iput-object p1, p0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment$1;->a:Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2368127
    iget-object v0, p0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment$1;->a:Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;

    .line 2368128
    const/4 v1, 0x0

    .line 2368129
    iget-object v2, v0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->n:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2ZK;

    .line 2368130
    iget-object v4, v0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v4, :cond_1

    .line 2368131
    invoke-interface {v1}, LX/2ZK;->b()LX/2ZE;

    move-result-object v4

    .line 2368132
    if-eqz v4, :cond_0

    .line 2368133
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2368134
    iget-object v5, v0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->p:Ljava/util/concurrent/Executor;

    new-instance v6, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment$3;

    invoke-direct {v6, v0, v2, v1}, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment$3;-><init>(Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;ILX/2ZK;)V

    const p0, -0x177392d3

    invoke-static {v5, v6, p0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2368135
    new-instance v1, LX/14U;

    invoke-direct {v1}, LX/14U;-><init>()V

    .line 2368136
    sget-object v5, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2368137
    iput-object v5, v1, LX/14U;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2368138
    iget-object v5, v0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->m:LX/28W;

    const-string v6, "handleFetchConfiguration"

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-static {p0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p0

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-virtual {v5, v6, p0, v4, v1}, LX/28W;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/List;LX/14U;)V

    .line 2368139
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    .line 2368140
    goto :goto_0

    .line 2368141
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method
