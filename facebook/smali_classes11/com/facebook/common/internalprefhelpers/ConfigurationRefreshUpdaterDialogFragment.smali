.class public Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final r:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public m:LX/28W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/2ZK;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2368187
    const-class v0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;

    sput-object v0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->r:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2368186
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2368177
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 2368178
    invoke-static {v0}, LX/4md;->a(Landroid/app/Dialog;)V

    .line 2368179
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 2368180
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 2368181
    iget-object v1, p0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->n:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 2368182
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 2368183
    const-string v1, "Fetching Configuration"

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 2368184
    const-string v1, "Starting fetch"

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 2368185
    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x301d3f7c

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2368171
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2368172
    if-nez p1, :cond_0

    .line 2368173
    iget-object v1, p0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->o:LX/0TD;

    new-instance v2, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment$1;

    invoke-direct {v2, p0}, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment$1;-><init>(Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2368174
    iget-object v1, p0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/GaK;

    invoke-direct {v2, p0}, LX/GaK;-><init>(Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;)V

    iget-object p1, p0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->p:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2368175
    :goto_0
    const v1, -0x4420947a

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 2368176
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x722ac3e0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2368168
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2368169
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    move-object v4, p0

    check-cast v4, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;

    invoke-static {v1}, LX/28W;->a(LX/0QB;)LX/28W;

    move-result-object v5

    check-cast v5, LX/28W;

    invoke-static {v1}, LX/3g3;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v6

    invoke-static {v1}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, LX/0TD;

    invoke-static {v1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/Executor;

    invoke-static {v1}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v1

    check-cast v1, LX/0kL;

    iput-object v5, v4, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->m:LX/28W;

    iput-object v6, v4, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->n:Ljava/util/Set;

    iput-object v7, v4, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->o:LX/0TD;

    iput-object p1, v4, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->p:Ljava/util/concurrent/Executor;

    iput-object v1, v4, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->q:LX/0kL;

    .line 2368170
    const/16 v1, 0x2b

    const v2, 0x4c9acbb7    # 8.115756E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 2368163
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 2368164
    iget-object v0, p0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2368165
    iget-object v0, p0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2368166
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2368167
    :cond_0
    return-void
.end method
