.class public final Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2217010
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2217011
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2217008
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2217009
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 2217087
    if-nez p1, :cond_0

    .line 2217088
    :goto_0
    return v0

    .line 2217089
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2217090
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2217091
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2217092
    const v2, 0x3a8661ad

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v1

    .line 2217093
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2217094
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2217095
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2217096
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2217097
    const v2, -0x3f19b646

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2217098
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v2

    .line 2217099
    const v3, -0x44b7bcb1

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2217100
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2217101
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2217102
    invoke-virtual {p0, p1, v8}, LX/15i;->p(II)I

    move-result v4

    .line 2217103
    const v5, -0x9e7252

    invoke-static {p0, v4, v5, p3}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v4

    .line 2217104
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2217105
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2217106
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 2217107
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 2217108
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 2217109
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2217110
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2217111
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2217112
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2217113
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2217114
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2217115
    :sswitch_3
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2217116
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2217117
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2217118
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2217119
    :sswitch_4
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2217120
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v2

    .line 2217121
    const v3, -0x2cb05235

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v2

    .line 2217122
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2217123
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2217124
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 2217125
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2217126
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2217127
    const v2, -0x73215571

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2217128
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2217129
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2217130
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2217131
    :sswitch_6
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v0

    .line 2217132
    const v1, -0x582b29cd

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2217133
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2217134
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 2217135
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2217136
    :sswitch_7
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2217137
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2217138
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2217139
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2217140
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x73215571 -> :sswitch_6
        -0x5ffd0669 -> :sswitch_0
        -0x582b29cd -> :sswitch_7
        -0x44b7bcb1 -> :sswitch_3
        -0x3f19b646 -> :sswitch_2
        -0x2cb05235 -> :sswitch_5
        -0x9e7252 -> :sswitch_4
        0x3a8661ad -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2217086
    new-instance v0, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 2217069
    sparse-switch p2, :sswitch_data_0

    .line 2217070
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2217071
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2217072
    const v1, 0x3a8661ad

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2217073
    :goto_0
    :sswitch_1
    return-void

    .line 2217074
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2217075
    const v1, -0x3f19b646

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2217076
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 2217077
    const v1, -0x44b7bcb1

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2217078
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2217079
    const v1, -0x9e7252

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2217080
    :sswitch_3
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 2217081
    const v1, -0x2cb05235

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2217082
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2217083
    const v1, -0x73215571

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2217084
    :sswitch_5
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 2217085
    const v1, -0x582b29cd

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x73215571 -> :sswitch_5
        -0x5ffd0669 -> :sswitch_0
        -0x582b29cd -> :sswitch_1
        -0x44b7bcb1 -> :sswitch_1
        -0x3f19b646 -> :sswitch_1
        -0x2cb05235 -> :sswitch_4
        -0x9e7252 -> :sswitch_3
        0x3a8661ad -> :sswitch_2
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2217059
    if-nez p1, :cond_0

    move v0, v1

    .line 2217060
    :goto_0
    return v0

    .line 2217061
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 2217062
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2217063
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2217064
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 2217065
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 2217066
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2217067
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2217068
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 2217052
    if-eqz p1, :cond_0

    .line 2217053
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 2217054
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2217055
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 2217056
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2217057
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2217058
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2217046
    if-eqz p1, :cond_0

    .line 2217047
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;

    move-result-object v1

    .line 2217048
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;

    .line 2217049
    if-eq v0, v1, :cond_0

    .line 2217050
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2217051
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2217045
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2217043
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2217044
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2217038
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2217039
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2217040
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2217041
    iput p2, p0, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->b:I

    .line 2217042
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2217037
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2217036
    new-instance v0, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2217033
    iget v0, p0, LX/1vt;->c:I

    .line 2217034
    move v0, v0

    .line 2217035
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2217030
    iget v0, p0, LX/1vt;->c:I

    .line 2217031
    move v0, v0

    .line 2217032
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2217027
    iget v0, p0, LX/1vt;->b:I

    .line 2217028
    move v0, v0

    .line 2217029
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2217024
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2217025
    move-object v0, v0

    .line 2217026
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2217015
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2217016
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2217017
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2217018
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2217019
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2217020
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2217021
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2217022
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2217023
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2217012
    iget v0, p0, LX/1vt;->c:I

    .line 2217013
    move v0, v0

    .line 2217014
    return v0
.end method
