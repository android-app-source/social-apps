.class public final Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$UnreadThreadQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$UnreadThreadQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2217238
    const-class v0, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$UnreadThreadQueryModel;

    new-instance v1, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$UnreadThreadQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$UnreadThreadQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2217239
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2217241
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$UnreadThreadQueryModel;LX/0nX;LX/0my;)V
    .locals 7

    .prologue
    .line 2217242
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2217243
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2217244
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2217245
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2217246
    if-eqz v2, :cond_f

    .line 2217247
    const-string v3, "message_threads"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2217248
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2217249
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2217250
    if-eqz v3, :cond_e

    .line 2217251
    const-string v4, "nodes"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2217252
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2217253
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_d

    .line 2217254
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    .line 2217255
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2217256
    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 2217257
    if-eqz v6, :cond_1

    .line 2217258
    const-string p0, "image"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2217259
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2217260
    const/4 p0, 0x0

    invoke-virtual {v1, v6, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2217261
    if-eqz p0, :cond_0

    .line 2217262
    const-string v0, "uri"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2217263
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2217264
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2217265
    :cond_1
    const/4 v6, 0x1

    invoke-virtual {v1, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 2217266
    if-eqz v6, :cond_3

    .line 2217267
    const-string p0, "messages"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2217268
    const/4 p0, 0x0

    .line 2217269
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2217270
    invoke-virtual {v1, v6, p0, p0}, LX/15i;->a(III)I

    move-result p0

    .line 2217271
    if-eqz p0, :cond_2

    .line 2217272
    const-string v0, "unread_count"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2217273
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 2217274
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2217275
    :cond_3
    const/4 v6, 0x2

    invoke-virtual {v1, v5, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 2217276
    if-eqz v6, :cond_4

    .line 2217277
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2217278
    invoke-virtual {p1, v6}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2217279
    :cond_4
    const/4 v6, 0x3

    invoke-virtual {v1, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 2217280
    if-eqz v6, :cond_c

    .line 2217281
    const-string p0, "other_participants"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2217282
    const/4 p0, 0x0

    .line 2217283
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2217284
    invoke-virtual {v1, v6, p0, p0}, LX/15i;->a(III)I

    move-result p0

    .line 2217285
    if-eqz p0, :cond_5

    .line 2217286
    const-string v0, "count"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2217287
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 2217288
    :cond_5
    const/4 p0, 0x1

    invoke-virtual {v1, v6, p0}, LX/15i;->g(II)I

    move-result p0

    .line 2217289
    if-eqz p0, :cond_b

    .line 2217290
    const-string v0, "nodes"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2217291
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2217292
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, p0}, LX/15i;->c(I)I

    move-result v2

    if-ge v0, v2, :cond_a

    .line 2217293
    invoke-virtual {v1, p0, v0}, LX/15i;->q(II)I

    move-result v2

    .line 2217294
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2217295
    const/4 v5, 0x0

    invoke-virtual {v1, v2, v5}, LX/15i;->g(II)I

    move-result v5

    .line 2217296
    if-eqz v5, :cond_9

    .line 2217297
    const-string v6, "messaging_actor"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2217298
    const/4 v2, 0x0

    .line 2217299
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2217300
    invoke-virtual {v1, v5, v2}, LX/15i;->g(II)I

    move-result v6

    .line 2217301
    if-eqz v6, :cond_6

    .line 2217302
    const-string v6, "__type__"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2217303
    invoke-static {v1, v5, v2, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2217304
    :cond_6
    const/4 v6, 0x1

    invoke-virtual {v1, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 2217305
    if-eqz v6, :cond_8

    .line 2217306
    const-string v2, "profile_picture"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2217307
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2217308
    const/4 v2, 0x0

    invoke-virtual {v1, v6, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2217309
    if-eqz v2, :cond_7

    .line 2217310
    const-string v5, "uri"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2217311
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2217312
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2217313
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2217314
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2217315
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2217316
    :cond_a
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2217317
    :cond_b
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2217318
    :cond_c
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2217319
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 2217320
    :cond_d
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2217321
    :cond_e
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2217322
    :cond_f
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2217323
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2217240
    check-cast p1, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$UnreadThreadQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$UnreadThreadQueryModel$Serializer;->a(Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$UnreadThreadQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
