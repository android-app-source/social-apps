.class public final Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$UnreadThreadQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2217141
    const-class v0, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$UnreadThreadQueryModel;

    new-instance v1, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$UnreadThreadQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$UnreadThreadQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2217142
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2217143
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 2217144
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2217145
    const/4 v2, 0x0

    .line 2217146
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2217147
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2217148
    :goto_0
    move v1, v2

    .line 2217149
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2217150
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2217151
    new-instance v1, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$UnreadThreadQueryModel;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$UnreadThreadQueryModel;-><init>()V

    .line 2217152
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2217153
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2217154
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2217155
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2217156
    :cond_0
    return-object v1

    .line 2217157
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2217158
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2217159
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2217160
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2217161
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2217162
    const-string v4, "message_threads"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2217163
    const/4 v3, 0x0

    .line 2217164
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 2217165
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2217166
    :goto_2
    move v1, v3

    .line 2217167
    goto :goto_1

    .line 2217168
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2217169
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2217170
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 2217171
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2217172
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 2217173
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2217174
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2217175
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 2217176
    const-string v5, "nodes"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2217177
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2217178
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_7

    .line 2217179
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_7

    .line 2217180
    const/4 v5, 0x0

    .line 2217181
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_10

    .line 2217182
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2217183
    :goto_5
    move v4, v5

    .line 2217184
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2217185
    :cond_7
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 2217186
    goto :goto_3

    .line 2217187
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 2217188
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2217189
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    goto :goto_3

    .line 2217190
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2217191
    :cond_b
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_f

    .line 2217192
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2217193
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2217194
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_b

    if-eqz v9, :cond_b

    .line 2217195
    const-string v10, "image"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 2217196
    const/4 v9, 0x0

    .line 2217197
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v10, :cond_14

    .line 2217198
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2217199
    :goto_7
    move v8, v9

    .line 2217200
    goto :goto_6

    .line 2217201
    :cond_c
    const-string v10, "messages"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 2217202
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2217203
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v11, :cond_19

    .line 2217204
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2217205
    :goto_8
    move v7, v9

    .line 2217206
    goto :goto_6

    .line 2217207
    :cond_d
    const-string v10, "name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 2217208
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_6

    .line 2217209
    :cond_e
    const-string v10, "other_participants"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 2217210
    invoke-static {p1, v0}, LX/FFN;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_6

    .line 2217211
    :cond_f
    const/4 v9, 0x4

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 2217212
    invoke-virtual {v0, v5, v8}, LX/186;->b(II)V

    .line 2217213
    const/4 v5, 0x1

    invoke-virtual {v0, v5, v7}, LX/186;->b(II)V

    .line 2217214
    const/4 v5, 0x2

    invoke-virtual {v0, v5, v6}, LX/186;->b(II)V

    .line 2217215
    const/4 v5, 0x3

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 2217216
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto/16 :goto_5

    :cond_10
    move v4, v5

    move v6, v5

    move v7, v5

    move v8, v5

    goto/16 :goto_6

    .line 2217217
    :cond_11
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2217218
    :cond_12
    :goto_9
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_13

    .line 2217219
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2217220
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2217221
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_12

    if-eqz v10, :cond_12

    .line 2217222
    const-string v11, "uri"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_11

    .line 2217223
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_9

    .line 2217224
    :cond_13
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 2217225
    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 2217226
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    goto/16 :goto_7

    :cond_14
    move v8, v9

    goto :goto_9

    .line 2217227
    :cond_15
    :goto_a
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_17

    .line 2217228
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 2217229
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2217230
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_15

    if-eqz v12, :cond_15

    .line 2217231
    const-string p0, "unread_count"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_16

    .line 2217232
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v7

    move v11, v7

    move v7, v10

    goto :goto_a

    .line 2217233
    :cond_16
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_a

    .line 2217234
    :cond_17
    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 2217235
    if-eqz v7, :cond_18

    .line 2217236
    invoke-virtual {v0, v9, v11, v9}, LX/186;->a(III)V

    .line 2217237
    :cond_18
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    goto/16 :goto_8

    :cond_19
    move v7, v9

    move v11, v9

    goto :goto_a
.end method
