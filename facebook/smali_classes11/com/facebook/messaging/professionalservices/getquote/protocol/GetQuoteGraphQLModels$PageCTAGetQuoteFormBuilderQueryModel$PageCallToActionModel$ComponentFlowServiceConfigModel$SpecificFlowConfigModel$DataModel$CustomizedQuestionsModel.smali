.class public final Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xc4b140f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2413107
    const-class v0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2413106
    const-class v0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2413088
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2413089
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2413098
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2413099
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel;->a()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2413100
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2413101
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2413102
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2413103
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2413104
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2413105
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2413095
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2413096
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2413097
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2413108
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel;->e:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel;->e:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    .line 2413109
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel;->e:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2413092
    new-instance v0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel;

    invoke-direct {v0}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel;-><init>()V

    .line 2413093
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2413094
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2413090
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel;->f:Ljava/lang/String;

    .line 2413091
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2413087
    const v0, 0x628ff9e9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2413086
    const v0, 0x4ad9ae3a    # 7132957.0f

    return v0
.end method
