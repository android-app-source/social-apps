.class public final Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x516249dd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2412859
    const-class v0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2412860
    const-class v0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2412861
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2412862
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2412863
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2412864
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2412865
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2412866
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2412867
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2412868
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2412869
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2412870
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2412871
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2412872
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2412873
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2412874
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 2412875
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2412876
    new-instance v0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel;

    invoke-direct {v0}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel;-><init>()V

    .line 2412877
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2412878
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2412879
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel;->f:Ljava/lang/String;

    .line 2412880
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2412881
    const v0, 0x7295085e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2412882
    const v0, -0x373bbdaf

    return v0
.end method
