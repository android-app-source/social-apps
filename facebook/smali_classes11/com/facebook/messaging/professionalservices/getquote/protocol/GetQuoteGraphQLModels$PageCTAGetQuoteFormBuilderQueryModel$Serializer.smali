.class public final Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2413336
    const-class v0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel;

    new-instance v1, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2413337
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2413335
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2413306
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2413307
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v3, 0x0

    .line 2413308
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2413309
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 2413310
    if-eqz v2, :cond_0

    .line 2413311
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2413312
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2413313
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2413314
    if-eqz v2, :cond_2

    .line 2413315
    const-string v3, "component_flow_apps"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2413316
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2413317
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_1

    .line 2413318
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1, p2}, LX/H09;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2413319
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2413320
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2413321
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2413322
    if-eqz v2, :cond_3

    .line 2413323
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2413324
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2413325
    :cond_3
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2413326
    if-eqz v2, :cond_4

    .line 2413327
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2413328
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2413329
    :cond_4
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2413330
    if-eqz v2, :cond_5

    .line 2413331
    const-string v3, "page_call_to_action"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2413332
    invoke-static {v1, v2, p1, p2}, LX/H0E;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2413333
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2413334
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2413305
    check-cast p1, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$Serializer;->a(Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
