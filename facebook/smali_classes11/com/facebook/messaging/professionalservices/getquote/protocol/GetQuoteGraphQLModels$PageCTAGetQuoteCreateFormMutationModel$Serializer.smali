.class public final Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteCreateFormMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteCreateFormMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2412801
    const-class v0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteCreateFormMutationModel;

    new-instance v1, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteCreateFormMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteCreateFormMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2412802
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2412803
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteCreateFormMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2412804
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2412805
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2412806
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2412807
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2412808
    if-eqz v2, :cond_0

    .line 2412809
    const-string p0, "page_cta"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2412810
    invoke-static {v1, v2, p1}, LX/H06;->a(LX/15i;ILX/0nX;)V

    .line 2412811
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2412812
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2412813
    check-cast p1, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteCreateFormMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteCreateFormMutationModel$Serializer;->a(Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteCreateFormMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
