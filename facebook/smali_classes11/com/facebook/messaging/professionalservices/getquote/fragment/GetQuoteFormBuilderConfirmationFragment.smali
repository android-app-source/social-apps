.class public Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/H0I;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/Gze;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public e:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

.field private f:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2412263
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;

    invoke-static {p0}, LX/H0I;->b(LX/0QB;)LX/H0I;

    move-result-object v1

    check-cast v1, LX/H0I;

    invoke-static {p0}, LX/Gze;->b(LX/0QB;)LX/Gze;

    move-result-object v2

    check-cast v2, LX/Gze;

    invoke-static {p0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    iput-object v1, p1, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;->b:LX/H0I;

    iput-object v2, p1, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;->c:LX/Gze;

    iput-object p0, p1, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;Z)V
    .locals 2

    .prologue
    .line 2412286
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;->f:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 2412287
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;->f:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2412288
    :cond_0
    return-void

    .line 2412289
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2412290
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2412291
    const-class v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2412292
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2412293
    const-string v1, "arg_page_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;->d:Ljava/lang/String;

    .line 2412294
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2412295
    const-string v1, "arg_form_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;->e:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    .line 2412296
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x311b5f02

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2412285
    const v1, 0x7f03079d

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x266fefc1

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x302c0a1a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2412274
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2412275
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2412276
    if-eqz v1, :cond_0

    .line 2412277
    const v2, 0x7f081510

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2412278
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f081513

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2412279
    iput-object v4, v2, LX/108;->g:Ljava/lang/String;

    .line 2412280
    move-object v2, v2

    .line 2412281
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2412282
    new-instance v2, LX/Gzh;

    invoke-direct {v2, p0}, LX/Gzh;-><init>(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;)V

    invoke-interface {v1, v2}, LX/1ZF;->a(LX/63W;)V

    .line 2412283
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2412284
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x39a6d859

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2412264
    const v0, 0x7f0d145b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;->f:Landroid/widget/ProgressBar;

    .line 2412265
    const v0, 0x7f0d1459

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2412266
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2412267
    const-string v2, "arg_get_quote_description"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2412268
    const v0, 0x7f0d145a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;

    .line 2412269
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;->e:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    .line 2412270
    iget-object v2, v1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->b:Ljava/lang/String;

    move-object v1, v2

    .line 2412271
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;->setFormTitle(Ljava/lang/String;)V

    .line 2412272
    new-instance v1, LX/Gzg;

    invoke-direct {v1, p0}, LX/Gzg;-><init>(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;->setEditFormCtaClickListener(Landroid/view/View$OnClickListener;)V

    .line 2412273
    return-void
.end method
