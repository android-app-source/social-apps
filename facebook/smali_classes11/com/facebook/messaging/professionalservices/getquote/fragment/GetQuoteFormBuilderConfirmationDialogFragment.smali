.class public Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationDialogFragment;
.super Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;
.source ""


# instance fields
.field public m:LX/Gzk;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2412202
    invoke-direct {p0}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;-><init>()V

    .line 2412203
    return-void
.end method

.method private l()Lcom/facebook/messaging/dialog/ConfirmActionParams;
    .locals 4

    .prologue
    .line 2412204
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2412205
    const-string v1, "arg_confirmation_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2412206
    sget-object v1, LX/Gzf;->DISCARD_FORM_CHANGES:LX/Gzf;

    invoke-virtual {v1}, LX/Gzf;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2412207
    new-instance v0, LX/6dy;

    const-string v1, ""

    const v2, 0x7f08152a

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/6dy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x7f081528

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2412208
    iput-object v1, v0, LX/6dy;->d:Ljava/lang/String;

    .line 2412209
    move-object v0, v0

    .line 2412210
    const v1, 0x7f081529

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2412211
    iput-object v1, v0, LX/6dy;->e:Ljava/lang/String;

    .line 2412212
    move-object v0, v0

    .line 2412213
    invoke-virtual {v0}, LX/6dy;->a()Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v0

    return-object v0

    .line 2412214
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid type:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2412215
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationDialogFragment;->m:LX/Gzk;

    if-eqz v0, :cond_0

    .line 2412216
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationDialogFragment;->m:LX/Gzk;

    .line 2412217
    iget-object v1, v0, LX/Gzk;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    invoke-static {v1}, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->c$redex0(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2412218
    :cond_0
    :goto_0
    return-void

    .line 2412219
    :cond_1
    iget-object v1, v0, LX/Gzk;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    const/4 p0, 0x0

    .line 2412220
    iput-boolean p0, v1, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->m:Z

    .line 2412221
    iget-object v1, v0, LX/Gzk;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    invoke-static {v1}, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->m(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;)V

    goto :goto_0
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 2412222
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationDialogFragment;->m:LX/Gzk;

    if-eqz v0, :cond_0

    .line 2412223
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationDialogFragment;->m:LX/Gzk;

    .line 2412224
    iget-object v1, v0, LX/Gzk;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    const/4 p0, 0x0

    .line 2412225
    iput-boolean p0, v1, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->m:Z

    .line 2412226
    iget-object v1, v0, LX/Gzk;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2412227
    iget-object v1, v0, LX/Gzk;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->onBackPressed()V

    .line 2412228
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x43adbb80

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2412229
    invoke-super {p0, p1}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2412230
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationDialogFragment;->l()Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v1

    .line 2412231
    iput-object v1, p0, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->m:Lcom/facebook/messaging/dialog/ConfirmActionParams;

    .line 2412232
    const/16 v1, 0x2b

    const v2, -0x446baba3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
