.class public Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public a:LX/Gzy;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/1Ck;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/H0I;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/H0t;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0kL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/Gze;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/H0P;

.field private i:Landroid/widget/ProgressBar;

.field private j:Landroid/support/v7/widget/RecyclerView;

.field public k:Ljava/lang/String;

.field public l:Z

.field public m:Z

.field public n:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2412451
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2412452
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->m:Z

    return-void
.end method

.method public static a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;
    .locals 3

    .prologue
    .line 2412443
    new-instance v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    invoke-direct {v0}, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;-><init>()V

    .line 2412444
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2412445
    const-string v2, "arg_page_id"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2412446
    const-string v2, "arg_is_edit_flow"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2412447
    const-string v2, "arg_get_quote_cta_label"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2412448
    const-string v2, "arg_get_quote_description"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2412449
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2412450
    return-object v0
.end method

.method public static a(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;Z)V
    .locals 2

    .prologue
    .line 2412439
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->i:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 2412440
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->i:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2412441
    :cond_0
    return-void

    .line 2412442
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static c$redex0(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2412422
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->h:LX/H0P;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->n:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    invoke-virtual {v0, v1}, LX/H0P;->b(Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V
    :try_end_0
    .catch LX/H0L; {:try_start_0 .. :try_end_0} :catch_0

    .line 2412423
    :try_start_1
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->h:LX/H0P;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->n:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    invoke-virtual {v0, v1}, LX/H0P;->c(Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V
    :try_end_1
    .catch LX/H0M; {:try_start_1 .. :try_end_1} :catch_1

    .line 2412424
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 2412425
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2412426
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->f:LX/0kL;

    new-instance v3, LX/27k;

    const v4, 0x7f08152d

    invoke-direct {v3, v4}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v3}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2412427
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->j:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    .line 2412428
    iget-object v3, v1, LX/H0L;->mAdapterPositions:LX/0Px;

    move-object v1, v3

    .line 2412429
    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2412430
    invoke-virtual {v0, v1, v2}, LX/1P1;->d(II)V

    move v0, v2

    .line 2412431
    goto :goto_0

    .line 2412432
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 2412433
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->f:LX/0kL;

    new-instance v3, LX/27k;

    const v4, 0x7f08152e

    invoke-direct {v3, v4}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v3}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2412434
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->j:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    .line 2412435
    iget-object v3, v1, LX/H0M;->mAdapterPositions:LX/0Px;

    move-object v1, v3

    .line 2412436
    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2412437
    invoke-virtual {v0, v1, v2}, LX/1P1;->d(II)V

    move v0, v2

    .line 2412438
    goto :goto_0
.end method

.method public static m(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;)V
    .locals 6

    .prologue
    .line 2412418
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->a(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;Z)V

    .line 2412419
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->n:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->f()Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    move-result-object v4

    .line 2412420
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->c:LX/H0I;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->k:Ljava/lang/String;

    new-instance v5, LX/Gzo;

    invoke-direct {v5, p0, v4}, LX/Gzo;-><init>(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V

    invoke-virtual/range {v0 .. v5}, LX/H0I;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/professionalservices/getquote/model/FormData;LX/Gzi;)V

    .line 2412421
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 4

    .prologue
    .line 2412366
    iget-boolean v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->n:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->n:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->o:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->m:Z

    if-eqz v0, :cond_0

    .line 2412367
    sget-object v0, LX/Gzf;->DISCARD_FORM_CHANGES:LX/Gzf;

    .line 2412368
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2412369
    const-string v2, "arg_confirmation_type"

    invoke-virtual {v0}, LX/Gzf;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2412370
    new-instance v2, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationDialogFragment;

    invoke-direct {v2}, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationDialogFragment;-><init>()V

    .line 2412371
    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2412372
    move-object v0, v2

    .line 2412373
    new-instance v1, LX/Gzk;

    invoke-direct {v1, p0}, LX/Gzk;-><init>(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;)V

    .line 2412374
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationDialogFragment;->m:LX/Gzk;

    .line 2412375
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    const-string v2, "get_quote_form_builder_confirmation_dialog_fragment_tag"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2412376
    const/4 v0, 0x1

    .line 2412377
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2412406
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2412407
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    new-instance v4, LX/Gzy;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    const/16 v5, 0x259

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-direct {v4, v3, v5}, LX/Gzy;-><init>(LX/0tX;LX/0Ot;)V

    move-object v3, v4

    check-cast v3, LX/Gzy;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {v0}, LX/H0I;->b(LX/0QB;)LX/H0I;

    move-result-object v5

    check-cast v5, LX/H0I;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v0}, LX/H0t;->a(LX/0QB;)LX/H0t;

    move-result-object v7

    check-cast v7, LX/H0t;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v8

    check-cast v8, LX/0kL;

    invoke-static {v0}, LX/Gze;->b(LX/0QB;)LX/Gze;

    move-result-object v0

    check-cast v0, LX/Gze;

    iput-object v3, v2, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->a:LX/Gzy;

    iput-object v4, v2, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->b:LX/1Ck;

    iput-object v5, v2, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->c:LX/H0I;

    iput-object v6, v2, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->d:Ljava/lang/String;

    iput-object v7, v2, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->e:LX/H0t;

    iput-object v8, v2, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->f:LX/0kL;

    iput-object v0, v2, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->g:LX/Gze;

    .line 2412408
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2412409
    const-string v1, "arg_page_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->k:Ljava/lang/String;

    .line 2412410
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->k:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2412411
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2412412
    const-string v1, "arg_is_edit_flow"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->l:Z

    .line 2412413
    new-instance v0, LX/H0P;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-boolean v2, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->l:Z

    invoke-direct {v0, v1, v2}, LX/H0P;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->h:LX/H0P;

    .line 2412414
    if-eqz p1, :cond_0

    .line 2412415
    const-string v0, "state_form_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->n:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    .line 2412416
    const-string v0, "state_original_form_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->o:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    .line 2412417
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x516e1881

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2412395
    const v0, 0x7f0307a4

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2412396
    const v0, 0x7f0d145b

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->i:Landroid/widget/ProgressBar;

    .line 2412397
    const v0, 0x7f0d1464

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->j:Landroid/support/v7/widget/RecyclerView;

    .line 2412398
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->j:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    const/4 v4, 0x1

    invoke-direct {v3, v4, v5}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2412399
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->j:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->h:LX/H0P;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2412400
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->n:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    if-nez v0, :cond_0

    .line 2412401
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->a(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;Z)V

    .line 2412402
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->b:LX/1Ck;

    const-string v3, "fetch_form_builder_data"

    new-instance v4, LX/Gzm;

    invoke-direct {v4, p0}, LX/Gzm;-><init>(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;)V

    new-instance v5, LX/Gzn;

    invoke-direct {v5, p0}, LX/Gzn;-><init>(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;)V

    invoke-virtual {v0, v3, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2412403
    :goto_0
    const v0, -0x17f94cf4

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-object v2

    .line 2412404
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->h:LX/H0P;

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->n:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    invoke-virtual {v0, v3}, LX/H0P;->a(Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V

    .line 2412405
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->h:LX/H0P;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2412391
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2412392
    const-string v0, "state_form_data"

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->n:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2412393
    const-string v0, "state_original_form_data"

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->o:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2412394
    return-void
.end method

.method public final onStart()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x8df0221

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2412378
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2412379
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2412380
    if-eqz v1, :cond_0

    .line 2412381
    iget-boolean v2, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->l:Z

    if-eqz v2, :cond_1

    const v2, 0x7f081511

    :goto_0
    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2412382
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v4

    iget-boolean v2, p0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->l:Z

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f081513

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2412383
    :goto_1
    iput-object v2, v4, LX/108;->g:Ljava/lang/String;

    .line 2412384
    move-object v2, v4

    .line 2412385
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2412386
    new-instance v2, LX/Gzl;

    invoke-direct {v2, p0}, LX/Gzl;-><init>(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;)V

    invoke-interface {v1, v2}, LX/1ZF;->a(LX/63W;)V

    .line 2412387
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2412388
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x6c66dc5d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2412389
    :cond_1
    const v2, 0x7f08150f

    goto :goto_0

    .line 2412390
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f081512

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method
