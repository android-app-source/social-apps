.class public Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2414426
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2414427
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;->a()V

    .line 2414428
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2414429
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2414430
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;->a()V

    .line 2414431
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2414432
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2414433
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;->a()V

    .line 2414434
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2414435
    const v0, 0x7f03079e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2414436
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;->setOrientation(I)V

    .line 2414437
    const v0, 0x7f0d145c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;->a:Landroid/widget/TextView;

    .line 2414438
    const v0, 0x7f0d145d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;->b:Landroid/view/View;

    .line 2414439
    return-void
.end method


# virtual methods
.method public setEditFormCtaClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2414440
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2414441
    return-void
.end method

.method public setFormTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2414442
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2414443
    return-void
.end method
