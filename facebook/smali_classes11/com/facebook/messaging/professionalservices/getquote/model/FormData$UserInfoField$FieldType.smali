.class public final enum Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

.field public static final enum ADDRESS:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum EMAIL:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

.field public static final enum NAME:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

.field public static final enum PHONE:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2412520
    new-instance v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    const-string v1, "NAME"

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;->NAME:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    .line 2412521
    new-instance v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    const-string v1, "EMAIL"

    invoke-direct {v0, v1, v3}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;->EMAIL:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    .line 2412522
    new-instance v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    const-string v1, "PHONE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;->PHONE:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    .line 2412523
    new-instance v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    const-string v1, "ADDRESS"

    invoke-direct {v0, v1, v5}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;->ADDRESS:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    .line 2412524
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    sget-object v1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;->NAME:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;->EMAIL:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;->PHONE:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;->ADDRESS:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;->$VALUES:[Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    .line 2412525
    new-instance v0, LX/Gzw;

    invoke-direct {v0}, LX/Gzw;-><init>()V

    sput-object v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2412526
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;
    .locals 1

    .prologue
    .line 2412527
    const-class v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;
    .locals 1

    .prologue
    .line 2412528
    sget-object v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;->$VALUES:[Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2412529
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2412530
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2412531
    return-void
.end method
